package com.example.floricultura;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import com.example.sim.MAINACTIVITY;
import com.example.sim.MyApp;
import com.example.sim.adapters.DatosLista;
import com.example.sim.adapters.ListAdapter;
import com.example.sim.adapters.ListAdapter3;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.Dempleado;
import com.example.sim.data.Dguardados;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

public class LISTADOAUSENCIAS extends ActionBarActivity {
	

private ListView ListadoAusencias;
private Button btnBack;
private Button btnInicio;
ArrayList<DatosLista> listado = new ArrayList<DatosLista>();
private MyApp appState;	
private DBAdapter db;
private int CodigoEmpleado;
private Ringtone ringtone;
private String Hoy;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_listadoausencias);
		
		Bundle extras = getIntent().getExtras();
		CodigoEmpleado = extras.getInt("CodigoEmpleado");
		
		 appState = ((MyApp)getApplicationContext());   
		    db = appState.getDb();
		    db.open();
		    Calendar c = Calendar.getInstance(); 
			SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
			Hoy = fdate.format(c.getTime());
		
		ListadoAusencias = (ListView)findViewById(R.id.ListadoAusencia);
		btnBack = (Button)findViewById(R.id.btnBack);
		btnInicio = (Button)findViewById(R.id.btnInicio);
		
		ListadoAusencias = (ListView)findViewById(R.id.ListadoAusencia);
		listado = getDatos();
		
		ListAdapter3 adapter = new ListAdapter3(this,R.layout.filas_ausencias, listado);
		ListadoAusencias.setAdapter(adapter);
		
		
		btnBack.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		btnInicio.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intento = new Intent(LISTADOAUSENCIAS.this,MAINACTIVITY.class);
				startActivity(intento);
			}
		});
		
	}
	public ArrayList<DatosLista> getDatos(){
		ArrayList<DatosLista> listadoTemp = new ArrayList<DatosLista>();
		listadoTemp.add(new DatosLista(" Nombre\nEmpleado","  Hora","  Opcion"));
		
		
		 Cursor Casignados= null ;
		 Casignados = this.db.getDguardados("Supervisor=" + CodigoEmpleado + " AND ActividadFinalizada = 0 " + " AND Fecha = '"+ Hoy +"'");
		 
		 if(Casignados.getCount()>0){
			 for(int i=0 ; i<Casignados.getCount();i++){	    
				 
				 Dguardados Dtemp = this.db.getDguardadosFromCursor(Casignados, i);
				 
				 Cursor Codigoempleado = this.db.getDempledo("CodigoEmpleado=" + Dtemp.getCodigoEmpleado().toString().trim());
				 Dempleado DatosEmpleado = this.db.getDempleadoFromCursor(Codigoempleado, 0);
				 if(DatosEmpleado != null){				 
					 listadoTemp.add(new DatosLista("    "+DatosEmpleado.getNombre().toString().trim(),"       " + Dtemp.getHoraIncio()," " +Dtemp.getActividad()));
				 }else{
					 listadoTemp.add(new DatosLista("    "+" NOMBRE NO ENCONTRADO ","       " + Dtemp.getHoraIncio()," " +Dtemp.getActividad())); 
				 }
				 
				 
				 Codigoempleado.close(); 
		     } 
		 }

		 Casignados.close();
		
		return listadoTemp;
	}
public void alertDialogMensaje(String message1, String mesage2){
		
		
		try {
		    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
		    ringtone.play();
		    
		    
		} catch (Exception e) {
		    e.printStackTrace();
		}
		
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(message1);
		alertDialog.setMessage(mesage2);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int which) {
		// here you can add functions
			ringtone.stop();
		}
		});
		alertDialog.show();
	}
}
