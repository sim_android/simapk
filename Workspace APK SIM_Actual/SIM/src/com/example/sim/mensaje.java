package com.example.sim;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;



import com.example.floricultura.R;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.Dguardados;
import com.example.sim.data.JsonFiles;
import com.example.sim.data.Mdepartamento;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

public class mensaje extends ActionBarActivity{
private MyApp appState;	
private DBAdapter db;
Ringtone ringtone;
EditText txtMensajeEnviar;
Button btnAtras;
int click = 0;
int click2=3;
Button uno;
Button dos;
Button tres;
Button cuatro;
Button cinco;
Button seis;
Button siete;
Button ocho;
Button nueve;
Button cero;
Button a;
Button b;
Button c;
Button d;
Button e;
Button f;
Button g;
Button h;
Button i;
Button j;
Button k;
Button l;
Button m;
Button n;
Button o;
Button p;
Button q;
Button r;
Button s;
Button t;
Button u;
Button v;
Button w;
Button x;
Button y;
Button z;
Button espacio;
Button borrar;
Button saltoLinea;
String[] datos = null;
Button signos;
int selection = 0;
String dato;
int cargar = 0;
int CodigoEmpleado;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.enviar_mensaje);
	
		Bundle extras = getIntent().getExtras();
		CodigoEmpleado = extras.getInt("CodigoEmpleado");
		
		txtMensajeEnviar = (EditText)findViewById(R.id.txtMensajeEnviar);
		btnAtras = (Button)findViewById(R.id.btnNuevo);
		a = (Button)findViewById(R.id.LetraA);
		b = (Button)findViewById(R.id.LetraB);
		c = (Button)findViewById(R.id.letraC);
		d = (Button)findViewById(R.id.letraD);
		e = (Button)findViewById(R.id.letraE);
		f = (Button)findViewById(R.id.letraF);
		g = (Button)findViewById(R.id.letraG);
		h = (Button)findViewById(R.id.letraH);
		i = (Button)findViewById(R.id.letraI);
		j = (Button)findViewById(R.id.letraJ);
		k = (Button)findViewById(R.id.letraK);
		l = (Button)findViewById(R.id.letraL);
		m = (Button)findViewById(R.id.letraM);
		n = (Button)findViewById(R.id.letraN);
		o = (Button)findViewById(R.id.letraO);
		p = (Button)findViewById(R.id.letraP);
		q = (Button)findViewById(R.id.letraQ);
		r = (Button)findViewById(R.id.letraR);
		s = (Button)findViewById(R.id.letraS);
		t = (Button)findViewById(R.id.letraT);
		u = (Button)findViewById(R.id.letraU);
		v = (Button)findViewById(R.id.letraV);
		w = (Button)findViewById(R.id.letraW);
		x = (Button)findViewById(R.id.letraX);
		y = (Button)findViewById(R.id.letraY);
		z = (Button)findViewById(R.id.letraZ);
		
		
		txtMensajeEnviar.requestFocus();
		
		
		
		appState = ((MyApp)getApplicationContext());   
	    db = appState.getDb();
	    db.open();
	    
	    
	    btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
	    	public void onClick(View v) {
            	
            	onBackPressed();
            	//finish();
            	
            }
        });
	}
	public void hideSoftKeyboard() {
	    if(getCurrentFocus()!=null) {
	        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
	        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),   InputMethodManager.HIDE_NOT_ALWAYS);
	    }
	}	
public void enviarMensaje(View view){

	switch(view.getId()){
	case R.id.LetraA:
		if (click==0){
			agregarNumero("a");
		}else if (click==1){
			agregarNumero("A");
		}else if(click2==2){
			agregarNumero("@");
		}
		break;
	case R.id.LetraB:
		if (click==0){
			agregarNumero("b");
		}else if (click==1){
			agregarNumero("B");
		}else if(click2==2){
			agregarNumero(";");
		}
		
			break;
	case R.id.letraC:
		if (click==0){
			agregarNumero("c");
		}else if (click==1){
			agregarNumero("C");
		}else if(click2==2){
			agregarNumero("\"");
		}
		break;
	case R.id.letraD:
		if (click==0){
			agregarNumero("d");
		}else if (click==1){
			agregarNumero("D");
		}else if(click2==2){
			agregarNumero("$");
		}
		break;
	case R.id.letraE:
		if (click==0){
			agregarNumero("e");
		}else if (click==1){
			agregarNumero("E");
		}else if(click2==2){
			agregarNumero("�");
		}
		break;
	case R.id.letraF:
		if (click==0){
			agregarNumero("f");
		}else if (click==1){
			agregarNumero("F");
		}else if(click2==2){
			agregarNumero("/");
		}
		break;
	case R.id.letraG:
		if (click==0){
			agregarNumero("g");
		}else if (click==1){
			agregarNumero("G");
		}else if(click2==2){
			agregarNumero("^");
		}
		break;
	case R.id.letraH:
		if (click==0){
			agregarNumero("h");
		}else if (click==1){
			agregarNumero("H");
		}else if(click2==2){
			agregarNumero("&");
		}
		break;
	case R.id.letraI:
		if (click==0){
			agregarNumero("i");
		}else if (click==1){
			agregarNumero("I");
		}else if(click2==2){
			agregarNumero(">");
		}
		break;	
	case R.id.letraJ:
		if (click==0){
			agregarNumero("j");
		}else if (click==1){
			agregarNumero("J");
		}else if(click2==2){
			agregarNumero("*");
		}
		break;
	case R.id.letraK:
		if (click==0){
			agregarNumero("k");
		}else if (click==1){
			agregarNumero("K");
		}else if(click2==2){
			agregarNumero("(");
		}
		break;
	case R.id.letraL:
		if (click==0){
			agregarNumero("l");
		}else if (click==1){
			agregarNumero("L");
		}else if(click2==2){
			agregarNumero(")");
		}
		break;
	case R.id.letraM:
		if (click==0){
			agregarNumero("m");
		}else if (click==1){
			agregarNumero("M");
		}else if(click2==2){
			agregarNumero("?");
		}
		break;
	case R.id.letraN:
		if (click==0){
			agregarNumero("n");
		}else if (click==1){
			agregarNumero("N");
		}else if(click2==2){
			agregarNumero("!");
		}
		break;
	case R.id.letraO:
		if (click==0){
			agregarNumero("o");
		}else if (click==1){
			agregarNumero("O");
		}else if(click2==2){
			agregarNumero("<");
		}
		break;
	case R.id.letraP:
		if (click==0){
			agregarNumero("p");
		}else if (click==1){
			agregarNumero("P");
		}else if(click2==2){
			agregarNumero("[");
		}
		break;
	case R.id.letraQ:
		if (click==0){
			agregarNumero("q");
		}else if (click==1){
			agregarNumero("Q");
		}else if(click2==2){
			agregarNumero("+");
		}
		break;
	case R.id.letraR:
		if (click==0){
			agregarNumero("r");
		}else if (click==1){
			agregarNumero("R");
		}else if(click2==2){
			agregarNumero("=");
		}
		break;
	case R.id.letraS:
		if (click==0){
			agregarNumero("s");
		}else if (click==1){
			agregarNumero("S");
		}else if(click2==2){
			agregarNumero("#");
		}
		break;
	case R.id.letraT:
		if (click==0){
			agregarNumero("t");
		}else if (click==1){
			agregarNumero("T");
		}else if(click2==2){
			agregarNumero("%");
		}
		break;
	case R.id.letraU:
		if (click==0){
			agregarNumero("u");
		}else if (click==1){
			agregarNumero("U");
		}else if(click2==2){
			agregarNumero("]");
		}
		break;
	case R.id.letraV:
		if (click==0){
			agregarNumero("v");
		}else if (click==1){
			agregarNumero("V");
		}else if(click2==2){
			agregarNumero(":");
		}
		break;
	case R.id.letraW:
		if (click==0){
			agregarNumero("w");
		}else if (click==1){
			agregarNumero("W");
		}else if(click2==2){
			agregarNumero("x");
		}
		break;
	case R.id.letraX:
		if (click==0){
			agregarNumero("x");
		}else if (click==1){
			agregarNumero("X");
		}else if(click2==2){
			agregarNumero("'");
		}
		break;
	case R.id.letraY:
		if (click==0){
			agregarNumero("y");
		}else if (click==1){
			agregarNumero("Y");
		}else if(click2==2){
			agregarNumero("_");
		}
		break;
	case R.id.letraZ:
		if (click==0){
			agregarNumero("z");
		}else if (click==1){
			agregarNumero("Z");
		}else if(click2==2){
			agregarNumero("-");
		}
		break;
	case R.id.espacio:
		agregarNumero(" ");
		break;
	case R.id.borrar:
		String mensaje = txtMensajeEnviar.getText().toString();
		if (mensaje.equals("")){
			txtMensajeEnviar.setText("");
		}else{
			String ncadena=mensaje.substring(0,mensaje.length()-1);
			txtMensajeEnviar.setText(ncadena);
			int d = txtMensajeEnviar.length();
			txtMensajeEnviar.setSelection(d);
		}
	
		
		break;
	case R.id.saltoLinea:
		String salto = "\n";
		txtMensajeEnviar.setText(txtMensajeEnviar.getText() + salto);
		int d = txtMensajeEnviar.length();
		txtMensajeEnviar.setSelection(d);	
		d++;
		break;
	case R.id.UNO:
		agregarNumero("1");
		break;
	case R.id.DOS:
		agregarNumero("2");
		break;
	case R.id.TRES:
		agregarNumero("3");
		break;
	case R.id.CUATRO:
		agregarNumero("4");
		break;
	case R.id.CINCO:
		agregarNumero("5");	
		break;
	case R.id.SEIS:
		agregarNumero("6");
		break;
	case R.id.SIETE:
		agregarNumero("7");
		break;
	case R.id.OCHO:
		agregarNumero("8");
		break;
	case R.id.NUEVE:
		agregarNumero("9");
		break;
	case R.id.CERO:
		agregarNumero("0");
		break;
	case R.id.coma:
		agregarNumero(",");
		break;
	case R.id.punto:
		agregarNumero(".");
		break;
		
	case R.id.buttonGuarda:
		String obtenerMensaje = txtMensajeEnviar.getText().toString();
		if(obtenerMensaje.equals("")){
			alertDialogMensaje("Error","Debe escribir su Mensaje");	
		}
		else{
		Dguardados dtemp = new Dguardados();
		Calendar c = Calendar.getInstance(); 
		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
		String fecha = fdate.format(c.getTime());
		SimpleDateFormat ftime = new SimpleDateFormat("HH:mm:ss");
		String horaFin = ftime.format(c.getTime());
		dtemp.setFecha(fecha);
		dtemp.setHoraFin(horaFin);
		ObjectMapper mapper = new ObjectMapper();
		try {
			// se refiere al nombre que se coloca en el bloc de notas
			String nameJson ="Mensaje de: " + CodigoEmpleado + "_"+dtemp.getFecha() + "_"+dtemp.getHoraFin();
			
			nameJson=nameJson.replace(":", "-");
			mapper.writeValue(new File( Environment.getExternalStorageDirectory()+"/SIM/mensajes/" + nameJson+ ".json"), obtenerMensaje);
			
			
			JsonFiles archivo = new JsonFiles();
			archivo.setName(nameJson+ ".json");
			archivo.setNameFolder("mensajes/");
			archivo.setUpload(0);
			db.insertarJsonFile(archivo);
			
			if(!appState.getSubiendoArchivos()){
				appState.setSubiendoArchivos(true);
				new UptoDropboxMensajes().execute(getApplicationContext());
				alertDialogMensaje("Mensaje","Su mensaje fue enviado con Exito"); 
			}	
		
			
		/*	
			if(!appState.getSubiendoArchivos()){
				appState.setSubiendoArchivos(true);
				new UptoDropbox().execute(getApplicationContext());
			}
			
		*/	
			
			txtMensajeEnviar.setText("");
		} 
		catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
	break;
	}

}
public void mayusculas(View view){
	switch(view.getId()){
	case R.id.mayus:
		if(click==0){
			a.setText("A");
			b.setText("B");
			c.setText("C");
			d.setText("D");
			e.setText("E");
			f.setText("F");
			g.setText("G");
			h.setText("H");
			i.setText("I");
			j.setText("J");
			k.setText("K");
			l.setText("L");
			m.setText("M");
			n.setText("N");
			o.setText("O");
			p.setText("P");
			q.setText("Q");
			r.setText("R");
			s.setText("S");
			t.setText("T");
			u.setText("U");
			v.setText("V");
			w.setText("W");
			x.setText("X");
			y.setText("Y");
			z.setText("Z");
			click=1;		
		}
		else
		if (click==1)
		{
			a.setText("a");
			b.setText("b");
			c.setText("c");
			d.setText("d");
			e.setText("e");
			f.setText("f");
			g.setText("g");
			h.setText("h");
			i.setText("i");
			j.setText("j");
			k.setText("k");
			l.setText("l");
			m.setText("m");
			n.setText("n");
			o.setText("o");
			p.setText("p");
			q.setText("q");
			r.setText("r");
			s.setText("s");
			t.setText("t");
			u.setText("u");
			v.setText("v");
			w.setText("w");
			x.setText("x");
			y.setText("y");
			z.setText("z");
			click=0;
		}
		break;
		
	}
}
public void agregarNumero(String n){
	int i=0;	
	if(n.contains("aa")){
		
		if(txtMensajeEnviar.isFocused()){
			txtMensajeEnviar.setText("");
		}
	}else{
		
		if(txtMensajeEnviar.isFocused()){
			
			txtMensajeEnviar.setText(txtMensajeEnviar.getText() + n);
			int d = txtMensajeEnviar.length();
			txtMensajeEnviar.setSelection(d);	
			d++;
			
			
	}	
		
}
}
				public void getListado(){
	int j = 0;
	 String[] listado = new String[]{"?","!"};
	
	 if(listado.length>0){
		datos = new String[]{};
		 for(int i=0 ; i<listado.length;i++){	         	
			 //Mdepartamento d = db.getMdepartamentoFromCursor(Deptos, i);  				 
			 datos[j] = listado[i];
			 j++;
	     } 
	 }
}
public void signos(View view){

	switch(view.getId()){
	case R.id.signos:
		
		if(click2==3){
			a.setText("@");
			b.setText(";");
			c.setText("\"");
			d.setText("$");
			e.setText("�");
			f.setText("/");
			g.setText("^");
			h.setText("&");
			i.setText(">");
			j.setText("*");
			k.setText("(");
			l.setText(")");
			m.setText("?");
			n.setText("!");
			o.setText("<");
			p.setText("[");
			q.setText("+");
			r.setText("=");
			s.setText("#");
			t.setText("%");
			u.setText("]");
			v.setText(":");
			w.setText("x");
			x.setText("'");
			y.setText("_");
			z.setText("-");
			click=4;
			click2=2;
		}
		else
		if (click2==2)
		{
			a.setText("a");
			b.setText("b");
			c.setText("c");
			d.setText("d");
			e.setText("e");
			f.setText("f");
			g.setText("g");
			h.setText("h");
			i.setText("i");
			j.setText("j");
			k.setText("k");
			l.setText("l");
			m.setText("m");
			n.setText("n");
			o.setText("o");
			p.setText("p");
			q.setText("q");
			r.setText("r");
			s.setText("s");
			t.setText("t");
			u.setText("u");
			v.setText("v");
			w.setText("w");
			x.setText("x");
			y.setText("y");
			z.setText("z");
			click=0;
			click2=3;
		}
		break;
		
	}
}
public void alertDialogMensaje(String message1, String mesage2){
	
	try {
	    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
	    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
	    ringtone.play();	    	    
	} catch (Exception e) {
	    e.printStackTrace();
	}
	AlertDialog alertDialog = new AlertDialog.Builder(this).create();
	alertDialog.setTitle(message1);
	alertDialog.setMessage(mesage2);
	alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
	public void onClick(DialogInterface dialog, int which) {
	// here you can add functions
		ringtone.stop();
	}
	});
	alertDialog.show();
}

}
