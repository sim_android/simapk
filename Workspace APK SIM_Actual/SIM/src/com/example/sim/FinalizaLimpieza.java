package com.example.sim;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.floricultura.R;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.Dguardados;
import com.example.sim.data.JsonFiles;
import com.example.sim.data.Opciones;
import com.example.sim.data.Usuario;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class FinalizaLimpieza extends ActionBarActivity {
	
	private MyApp appState;	
	private DBAdapter db;
	int CodigoModulo;
	int Codigodepartamento;
	int CodigoEmpleado;
	int CodigoOpcion;
	String NombreOpcion;
	EditText empleado;
	int finalizar;
	Ringtone ringtone;
	EditText EditPeso;
	EditText Supervisor;
	int Supervisa;
    

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_finaliza_limpieza);
		
		Bundle extras = getIntent().getExtras();
		CodigoModulo = extras.getInt("CodigoModulo");
	    CodigoEmpleado = extras.getInt("CodigoEmpleado");
	    Codigodepartamento = extras.getInt("Codigodepartamento");
	    CodigoOpcion = extras.getInt("CodigoOpcion");
	    NombreOpcion = extras.getString("NombreOpcion");
	    
	    
	    TextView TNombreCaptura = (TextView)findViewById(R.id.OpcionCaptura);
	    TextView TCodigoEmpleado= (TextView)findViewById(R.id.CodigoEmpleado);
	    TextView TCodigoOpcion= (TextView)findViewById(R.id.codigoOpcion);
	    empleado  = (EditText)findViewById(R.id.editEmpleado);
	    Supervisor = (EditText)findViewById(R.id.editSupervisor);
	    Button btnNuevo = (Button) findViewById(R.id.btnNuevo);
	    Button btnBack = (Button) findViewById(R.id.btnBack);
	    Button btnListado = (Button) findViewById(R.id.btnListado);
	    
	    
	    TextView TextPeso= (TextView)findViewById(R.id.TextPeso);
	    EditPeso = (EditText)findViewById(R.id.EditPeso);
	    
	    TNombreCaptura.setText(NombreOpcion+ "");
	    TCodigoEmpleado.setText(CodigoEmpleado + "");
	    TCodigoOpcion.setText(CodigoOpcion + "");
	    

	    
	    //se valida la opcion finalizar actividad
	    
	    
	    appState = ((MyApp)getApplicationContext());   
	    db = appState.getDb();
	    db.open();
	    
	    Cursor CursorOpciones= null ;
	    CursorOpciones = db.getOpciones("Opcion=" + CodigoOpcion);
	    
	    if(CursorOpciones.getCount()>0){
	    	Opciones op = db.getOpcionesFromCursor(CursorOpciones, 0);
	    	finalizar = op.getFinalizaActividad();
	    }
	    CursorOpciones.close();
	    
	    if(finalizar>0){
	    	//validacionesGroup.setVisibility(View.VISIBLE);
	    }
	    

			    //Se verfica el input de empleado al persionar enter
			    empleado.setOnKeyListener(new View.OnKeyListener() {
		            public boolean onKey(View v, int keyCode, KeyEvent event) {
		                // If the event is a key-down event on the "enter" button
		                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
		                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
		                	int valida = VerificarEmpleado(empleado.getText().toString().trim());
		                	if(valida == 1){
		                	 Supervisor.requestFocus();
		                	}else{
		                		alertDialogMensaje("Empleado", "Empleado Incorrecto");
		                	}return true; 
		            }
		                return false;
		            }
		        });
			    	
			    Supervisor.setOnKeyListener(new View.OnKeyListener() {
		            public boolean onKey(View v, int keyCode, KeyEvent event) {
		                // If the event is a key-down event on the "enter" button
		                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
		                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
		                	 int verifica =   VerificarSupervisor(Supervisor.getText().toString().trim());
		                	 if(verifica ==  1){
		                		Cursor CursorGuardados =	db.getDguardados("CodigoEmpleado=" + Integer.parseInt(empleado.getText().toString().trim())+ " AND ActividadFinalizada=0" );
		                		if(CursorGuardados.getCount()>0){
		                			Dguardados dtemp =  (Dguardados)db.getDguardadosFromCursor(CursorGuardados, 0);
		                			
		                			
		                			//-----------------------------------------------------------------------------------
		                			String correlativo="";
		                			    	//almaceno el empleado en una variable string
		                			    	String getEmpleado = empleado.getText().toString().trim();
		                			    	// guardo la variable que tiene empleado en un array
		                			    	String[]empl = new String[]{getEmpleado};
		                			    	Cursor obtenerCorrelativo = db.getCorrelativo(empl);
		                			    	//recorro dicho arreglo para obtener el campo
		                			    	if (obtenerCorrelativo.moveToFirst()){
		                			    		do{
		                			    			correlativo = obtenerCorrelativo.getString(0);
		                			    		}while(obtenerCorrelativo.moveToNext());
		                			    	}
		                			//-------------------------------------------------------------------------
		                			
		                			
		                    		Calendar c = Calendar.getInstance(); 
		                    		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
		                    		String fecha = fdate.format(c.getTime());
		                    		SimpleDateFormat ftime = new SimpleDateFormat("HH:mm:ss");
		                    		String horaFin = ftime.format(c.getTime());
		                    		
		                    		dtemp.setFecha(fecha);
		                			dtemp.setHoraFin(horaFin);
		                			dtemp.setSupervisor(CodigoEmpleado);
		                			dtemp.setCorrelativoBolsa(Integer.toString(Supervisa));
		                			dtemp.setActividadFinalizada(1);
		                			dtemp.setCorrelativoEmpleado(correlativo);
		                			String sm =  dtemp.getSiembra().toString().trim();
		                			//String hp =  dtemp.getAzucarera().toString().trim();
		                			
		                			dtemp.setSiembra(sm);
		                			//dtemp.setAzucarera(hp);
		                			if(EditPeso.getText().toString().trim().length()>0)
		                				dtemp.setFlores(EditPeso.getText().toString().trim());	
		                			
		                			db.updateDguardados(dtemp);
		                			
		                			empleado.setText("");
		                			EditPeso.setText("");
		                			Supervisor.setText("");
		                			EditPeso.requestFocus();
		                    		
		                    		ObjectMapper mapper = new ObjectMapper();
		                    		try {
		                    			String nameJson ="LM_" + dtemp.getCodigoEmpleado() + "_"+dtemp.getFecha() + "_"+dtemp.getHoraFin();
		                    			nameJson=nameJson.replace(":", "-");
										mapper.writeValue(new File( Environment.getExternalStorageDirectory()+"/SIM/JsonDescarga/Finalizados/" + nameJson+ ".json"), dtemp);
										
										
										JsonFiles archivo = new JsonFiles();
										archivo.setName(nameJson+ ".json");
										archivo.setNameFolder("Finalizados/");
										archivo.setUpload(0);
										db.insertarJsonFile(archivo);
										
										if(!appState.getSubiendoArchivos()){
											appState.setSubiendoArchivos(true);
											new UptoDropboxFin().execute(getApplicationContext());
										}
										
										
									} catch (JsonGenerationException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									} catch (JsonMappingException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									} catch (IOException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
		                    		
		                		}else{
		                			alertDialogMensaje("Invalido", "Usuario invalido");
		                    		empleado.setText("");
		                    		Supervisor.setText("");
		                    		empleado.requestFocus();
		                		}
		                		CursorGuardados.close();
		                	 }else{
		                	 //alertDialogMensaje("Invalido", "Supervisor invalido");
		                	 Supervisor.setText("");
		                	 Supervisor.requestFocus();
		                	 }
		                 return true;
		                }
		                return false;
		            }
		        });
	    
	    
	    
        EditPeso.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	// se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                		empleado.requestFocus();
	                return true;
                }
                return false;
            }
        });
        
	  
        
        btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
 
            	onBackPressed();
            }
        });
        
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	empleado.setText("");
    			EditPeso.setText("");
    			EditPeso.requestFocus();
            	
            }
        });
        
        btnListado.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Intent intent = new Intent(getApplicationContext(), LISTADOLIMPIEZA.class);
           	    intent.putExtra("CodigoEmpleado", CodigoEmpleado );
           	    startActivity(intent);
            	
            }
        });

		
	}
		
	public void hideSoftKeyboard() {
	    if(getCurrentFocus()!=null) {
	        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
	        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),   InputMethodManager.HIDE_NOT_ALWAYS);
	    }
	}
	
	//retornara un valor dependiendo del estado del empleado
			// 0 = no existe
			// 1 = existe pero esta asignado
			// 2 = existe y no esta asignado
	public int VerificarEmpleado(String empleado){
				int valor = 0;
				Cursor CursorDacceso =	db.getDacceso("CodigoEmpleado='" + empleado + "'");
				if(CursorDacceso != null  && CursorDacceso.getCount()>0){
					Cursor CursorGuardados =	db.getDguardados("CodigoEmpleado=" + empleado + " AND ActividadFinalizada=0");
					if(CursorGuardados.getCount()>0){
						valor = 1;
					}else{
						valor = 2;
					}
					CursorGuardados.close();	
				}
				CursorDacceso.close();
				return valor;
			}

	public int VerificarSupervisor(String sup){
		int val = 0;
		 Cursor CursorUsuario = db.getUsuario("Usuario='" + sup + "'");
		 if(CursorUsuario.getCount()>0){
			 Usuario u = db.getUsuarioFromCursor(CursorUsuario, 0);
			 try {
			 Log.d("USUARIO",u.getUsuario() + ", "+u.getCodigoEmpleado());
			 } catch(Exception e) {
				 alertDialogMensaje("ERROR", "NO EXISTE USUARIO PARA ESTE SUPERVISOR");
			 }
			 Supervisa =  u.getCodigoEmpleado();
			 val = 1;
		} else {
			alertDialogMensaje("ERROR", "NO EXISTE USUARIO PARA ESTE SUPERVISOR");
		}
		 CursorUsuario.close();
		 return val;
	}
	
	public void alertDialogMensaje(String message1, String mesage2){
		
		
		try {
		    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
		    ringtone.play();
		    
		    
		} catch (Exception e) {
		    e.printStackTrace();
		}
		
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(message1);
		alertDialog.setMessage(mesage2);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int which) {
		// here you can add functions
			ringtone.stop();
		}
		});
		alertDialog.show();
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.finalizar_actividad, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	
	public void ButtonOnClick(View v) {
	    switch (v.getId()) {
	      case R.id.buttonborra:
		        agregarNumero("1");
	        break;
	      case R.id.btnDos:
	    	  agregarNumero("2");
	        break;
	      case R.id.btnTres:
		        agregarNumero("3");
	        break;
	      case R.id.btnCuatro:
	    	  agregarNumero("4");
	        break;
	      case R.id.btnCinco:
		        agregarNumero("5");
	        break;
	      case R.id.btnSeis:
	    	  agregarNumero("6");
	        break;
	      case R.id.btnSiete:
	    	  agregarNumero("7");
	        break;
	      case R.id.btnOcho:
		        agregarNumero("8");
	        break;
	      case R.id.btnNueve:
	    	  agregarNumero("9");
	        break;
	      case R.id.button0:
		        agregarNumero("0");
	        break;
	      case R.id.btnCero:
	    	  agregarNumero("b");
	        break;
	      case R.id.Button11:
	    	  agregarNumero(".");
	        break;
	      }
	     
	}
	
	
	public void agregarNumero(String n){
		
		if(n.contains("b")){	
			if(EditPeso.isFocused()){
				EditPeso.setText("");
			}
			
		}else{
			if(EditPeso.isFocused()){
			EditPeso.setText(EditPeso.getText() + n);
			}
		}
		

	}



}

	
