package com.example.sim;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Vibrator;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.floricultura.R;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.Dbolsas;
import com.example.sim.data.Dguardados;
import com.example.sim.data.Filtros;
import com.example.sim.data.JsonFiles;
import com.example.sim.data.Semillas;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CapturaGeneral extends ActionBarActivity {
	
	//Variables globales
	private MyApp appState;	
	private DBAdapter db;
	int CodigoModulo;
	int Codigodepartamento;
	int CodigoEmpleado;
	int CodigoOpcion;
	int OpcionPolinizacion;
	int OpcionSuccion;
	int ValorPolinizacion = 0;
	int ValorSuccion=0;
	int TotalEmpleadosAsignados;
	int OpcionCosecha;
	boolean validoDescriptivo;
	boolean confirma = true;
	boolean confirma2 = true;
	String NombreOpcion;
	String Val_Bolsa;
	String azu;
	String bot;
	String bro;
	String SiembraF = "";
	LinearLayout validacionesGroup;
	TextView Material;
	TextView Material1;
	TextView TCantidadAsignados;
	EditText azucar;
	EditText bote;
	EditText brocha;
	EditText descriptivo;
	EditText empleado;
	TextView TextviewAzucarera;
	TextView TextviewBrocha;
	TextView TextviewBote; 
	Vibrator mVibrator;
	Ringtone ringtone;
	CheckBox checkbox1;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_captura_general);
		
		
		//se obtienen lo datos de la actividad antetior por medio del intent
		Bundle extras = getIntent().getExtras();
		CodigoModulo = extras.getInt("CodigoModulo");
	    CodigoEmpleado = extras.getInt("CodigoEmpleado");
	    Codigodepartamento = extras.getInt("Codigodepartamento");
	    CodigoOpcion = extras.getInt("CodigoOpcion");
	    NombreOpcion = extras.getString("NombreOpcion");
  
	    //hacemos referencia a todos los objetos de nuestra vista
	    TextView TNombreCaptura = (TextView)findViewById(R.id.OpcionCaptura);
	    TextView TCodigoEmpleado= (TextView)findViewById(R.id.CodigoEmpleado);
	    TextView TCodigoOpcion= (TextView)findViewById(R.id.codigoOpcion);
	    TextviewAzucarera= (TextView)findViewById(R.id.textViewFlores);
	    TextviewBrocha= (TextView)findViewById(R.id.textViewBrocha);
	    TextviewBote= (TextView)findViewById(R.id.textViewBote);
	    
	    
	    
	    TCantidadAsignados = (TextView)findViewById(R.id.textEmpleadosAsginados);
	    validacionesGroup = (LinearLayout)findViewById(R.id.layoutValidaciones);
	    descriptivo = (EditText)findViewById(R.id.editTextDescriptivo);
	    Material = (TextView)findViewById(R.id.textMaterial);
	    Material1 = (TextView)findViewById(R.id.textMaterial1);
	    azucar  = (EditText)findViewById(R.id.editAzucar);
	    bote  = (EditText)findViewById(R.id.editBote);
	    brocha  = (EditText)findViewById(R.id.editBrocha);
	    empleado  = (EditText)findViewById(R.id.editEmpleado);
	    checkbox1 = (CheckBox)findViewById(R.id.checkBox10);
	    Button buttonNuevo = (Button) findViewById(R.id.btnNuevo);
        Button btnBack = (Button) findViewById(R.id.btnBack);
        Button btnInicio = (Button) findViewById(R.id.btnInicio);
	    
	    
	    //hacemos las inicializaciones necesarias a nuestra vista
	    TNombreCaptura.setText(NombreOpcion+ "");
	    TCodigoEmpleado.setText(CodigoEmpleado + "");
	    TCodigoOpcion.setText(CodigoOpcion + "");
	    if(NombreOpcion.contains("POLINIZACION"))
	    {
	    	OpcionPolinizacion = 1;
	    } else if(NombreOpcion.contains("COSECHAS")){
	    	OpcionCosecha = 1; 
	    } else if(NombreOpcion.contains("SUCCION")){
		    OpcionSuccion = 1;
	    }
	    validoDescriptivo = false;
	    confirma = false;
	    confirma2 = false;
	    validacionesGroup.setVisibility(View.INVISIBLE);
	    descriptivo.requestFocus();
	    checkbox1.setVisibility(View.INVISIBLE);
	  
	    // Agrega CheckBox a La vista si opcion es seleccionada y agrega el texto que corresponde   
	 /*   if (OpcionSuccion == 1){
	    		checkbox1.setVisibility(View.VISIBLE);
	    } else if(OpcionCosecha == 1){
	    			checkbox1.setVisibility(View.VISIBLE);
	    			checkbox1.setText("VARIAS BOLSAS ");
	    }else{
	    	checkbox1.setVisibility(View.INVISIBLE);
	    }
	   //fin agrega checkbox
	    */
	    
	    //incializamos la BD asi como tambien obtenemos la referencia al singleton
	    appState = ((MyApp)getApplicationContext());   
	    db = appState.getDb();
	    db.open();
	    
	    verificarAsginados();
	    	    
	    //al recibir el enter se verifica si el descriptivo es correcto y lo manda al siguiente input sino le muestra un mensaje al usuario
        descriptivo.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                		VerificarDatos(descriptivo.getText().toString().trim());
	                	if (validoDescriptivo == true){
	                		if(OpcionCosecha == 1){
	                			azucar.requestFocus();
	                			descriptivo.setEnabled(false);
	                		//agregado valor polinizaci�n 0, 1, 2 y 3
	                		}else if(OpcionPolinizacion == 1 && ValorPolinizacion == 0){
	                			bote.requestFocus();
	                			descriptivo.setEnabled(false);
	                		}else if(OpcionPolinizacion == 1 && ValorPolinizacion == 1){
	                			azucar.requestFocus();
	                			descriptivo.setEnabled(false);
	                		}else if(OpcionPolinizacion == 1 && ValorPolinizacion == 2){
	                			bote.requestFocus();
	                			descriptivo.setEnabled(false);
	                		}else if(OpcionPolinizacion == 1 && ValorPolinizacion == 3){
	                			azucar.requestFocus();
	                			descriptivo.setEnabled(false);
	                		// fin agregado valor polinizacion   			
	                		}else if(OpcionPolinizacion == 1 && ValorPolinizacion == 4) {
	                			brocha.requestFocus();
	                			descriptivo.setEnabled(false);
	                		}else if (OpcionSuccion==1){
	                			descriptivo.setEnabled(false);
	                			azucar.setEnabled(true);
	                			azucar.requestFocus();	                		
	                		}else {
	                			descriptivo.setEnabled(false);
	                			empleado.requestFocus();
	                		}
	                	}else{
		    	    		alertDialogMensaje("Descriptivo", "Descriptivo Invalido");
		    	    		descriptivo.setEnabled(true);
		    	    		descriptivo.setText("");
		    	    		descriptivo.requestFocus();
	                	}
	                	return true;  
                }     
                return false;
            }  
        });
               
	    //Se verfica el input azucarera al persionar enter
        azucar.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	// se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                	azu = azucar.getText().toString().trim();
                	azu = azu.replace("'","-");
                	azucar.setText(azu);
                	
               	if (OpcionCosecha == 1){
               			Material1.setText("");
                		VerificarDatosbolsa(azucar.getText().toString().trim());
                		String des = descriptivo.getText().toString().trim();
                		String az = Material1.getText().toString().trim();
                		//String az = Val_Bolsa;
                		
                		if(des.equals(az)){
                			bote.setEnabled(true);
                			bote.requestFocus();              			
                			azucar.setEnabled(false);
                		}else{
                			
                			if(OpcionCosecha == 1){
                				alertDialogMensaje("Bolsa", "Valor de Bolsa es invalido");
                				//CODIGO AGREGADO
                			}else if (OpcionSuccion == 1){
                				alertDialogMensaje("Filtro", "Valor de Filtro es invalido");
                				//FIN CODIGO AGREGADO
                			}else{
                				alertDialogMensaje("Azucarera", "Valor de Azucarera es invalido");
                			}
                			azucar.setEnabled(true);
                			azucar.setText("");
                			azucar.requestFocus();
                		}
               		} else if(OpcionPolinizacion == 1 && ValorPolinizacion == 3){
               			String  Descrip = azucar.getText().toString().trim();
               			validarDescriptivo(Descrip);
               			if(validoDescriptivo == true){
               				brocha.requestFocus();
               				azucar.setEnabled(false);
               			} else {
               				alertDialogMensaje("Error"," Descriptivo es Invalido ");
               				azucar.setText("");
               				azucar.requestFocus();
               			}
                	}else{
                	
            		String des = descriptivo.getText().toString().trim();
            		String az = azucar.getText().toString().trim();
            		//String az = VerificarFiltro(azucar.getText().toString().trim());
            		//String az = Val_Bolsa;
            		
            		if(des.equals(az)){
            			          		
            			bote.requestFocus();
            			azucar.setEnabled(false);  
            		}else{
            			
            			if(OpcionCosecha == 1){
            				alertDialogMensaje("Bolsa", "Valor de Bolsa #1 es invalido");
            				//CODIGO AGREGADO
            			}else if (OpcionSuccion == 1){
            				alertDialogMensaje("Filtro", "Valor de Filtro es invalido");
            				//FIN CODIGO AGREGADO
            			}else{
            				alertDialogMensaje("Azucarera", "Valor de Azucarera es invalido");
            			}
            			azucar.setEnabled(true);
            			azucar.setText("");
            			azucar.requestFocus();
            		}
                	}
            		
            		
	                return true;
                }
                return false;
            }
        });
           
	    //Se verfica el input azucarera al persionar enter
        bote.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	// se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                	bot = bote.getText().toString().trim();
                	bot = bot.replace("'","-");
                	bote.setText(bot);
                	String bot = bote.getText().toString().trim();
            		String mat = Material.getText().toString().trim();
                if (OpcionCosecha == 1){	
                	Material1.setText("");
                	VerificarDatosbolsa(bote.getText().toString().trim());            		            		
        			String des = descriptivo.getText().toString().trim();
        			String bol = Material1.getText().toString().trim();
        			if(des.equals(bol)){	
        				if(OpcionCosecha == 1){ 
        					String b1 = azucar.getText().toString().trim();
        					String b2 = bote.getText().toString().trim();
        					if(!b1.equals(b2)){
        						bote.setBackgroundColor(Color.RED);
        					}
        					brocha.requestFocus();  
        					bote.setEnabled(false);
        				}
        				}else{ 
        				alertDialogMensaje("Mensaje", "Valor de Bolsa #2 es invalido");
        				bote.setText("");
        				bote.requestFocus();
        				}
                   }else if(OpcionCosecha == 0 && bot.equals(mat)){
                	   	brocha.setEnabled(true);
            			brocha.requestFocus();	
            		 if(OpcionSuccion ==1){
            				empleado.requestFocus();
        //agrega else if opcion polinizacion para que el focus salte al campo Empleado
            			}else if (OpcionPolinizacion == 1 && ValorPolinizacion == 0){
            			empleado.requestFocus();
        
            			}else if (OpcionPolinizacion == 1 && ValorPolinizacion == 2){
            				empleado.requestFocus();
            			}
        // Else general si  tipo de polinizaci�n no es 0 � 2    			
            			else{
            				brocha.requestFocus();            				
            			}
            			//FIN CODIGO AGREGADO
            			bote.setEnabled(false);
            		}else{
            			//CODIGO AGREGADO
            			if(OpcionSuccion == 1){
            				alertDialogMensaje("Pipeta", "Valor de Pipeta es invalido");
            			}else{
            				alertDialogMensaje("Bote", "Valor de Bote es invalido");
            				bote.setText("");
            				bote.requestFocus();
            			}
            			//alertDialogMensaje("Bote", "Valor de Bote es invalido");
            			bote.setText("");
            			bote.requestFocus();
            		}
	                return true;
                }
                return false;
            }
        });
        
	    //Se verfica el input azucarera al persionar enter
        brocha.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	// se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                	bro = brocha.getText().toString().trim();
                	bro = bro.replace("'","-");
                	brocha.setText(bro);
                	String bro = brocha.getText().toString().trim();
            		String mat = Material.getText().toString().trim();
            		
            	if (OpcionCosecha == 1){
            		Material1.setText("");
            	 	VerificarDatosbolsa(brocha.getText().toString().trim());
            		String bol = Material1.getText().toString().trim();
            		String des = descriptivo.getText().toString().trim();
            	if (des.equals(bol)){
            		String b1 = azucar.getText().toString().trim();
            		String b2 = bote.getText().toString().trim();
            		String b3 = brocha.getText().toString().trim();
            		if(b3.equals(b1)){
            			brocha.setBackgroundColor(Color.WHITE);                                                                                                                                                                                                                                                       
            		}else if(b3.equals(b2)){
            			brocha.setBackgroundColor(Color.RED);
            		} else {
            			brocha.setBackgroundColor(Color.GREEN);
            		}
    				brocha.setEnabled(false);
            		empleado.requestFocus();
            		}else {
            		alertDialogMensaje("Mensaje", "Valor de Bolsa #3 es Invalido");
            		brocha.setText("");
            		brocha.requestFocus();
    				}
            	}else if(bro.equals(mat)){
            			empleado.requestFocus();
            			brocha.setEnabled(false);
            	}else{
            			alertDialogMensaje("Brocha", "Valor de Brocha es invalido");
            			brocha.setText("");
            			brocha.requestFocus();
            		}
            	
	                return true;
                }
                return false;
            }
        });
             
	    //Se verfica el input empleado al persionar enter
        empleado.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	// se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                	
                	// agrega c�digo de validaci�n 24/11/2014
                if(OpcionCosecha == 1 ){
                	validoDescriptivo = false;
                	validarDescriptivo(descriptivo.getText().toString().trim());
                	if (validoDescriptivo == true){
                	Material1.setText("");
                	ValidaCampos(azucar.getText().toString().trim());	
                	if(confirma2 == false){
                		alertDialogMensaje("Mensaje", "Valor de Bolsa #1 es invalido");
                		azucar.setEnabled(true);
                		azucar.setText("");
            			bote.setText("");
            			brocha.setText("");
            			empleado.setText("");
            			azucar.requestFocus();
                	} else{
                		confirma2 = false;
                		ValidaCampos(bote.getText().toString().trim());
                		if(confirma2 == false){
                    		alertDialogMensaje("Mensaje", "Valor de Bolsa #2 es invalido");
                    		bote.setEnabled(true);
                    		bote.setText("");
                			brocha.setText("");
                			empleado.setText(""); 
                			bote.requestFocus();
                    	}  else {
                    		confirma2 = false;
                    		ValidaCampos(brocha.getText().toString().trim());
                    		if(confirma2 == false){
                        		alertDialogMensaje("Mensaje", "Valor de Bolsa #3 es invalido");
                        		brocha.setEnabled(true);
                        		brocha.setText("");
                    			empleado.setText("");
                    			brocha.requestFocus();
                        	} else{
                        		GuardarDatos();
                	            return true; 
                        	}
                    	}
                	} 
                	}else {
                		alertDialogMensaje("ERROR","ERROR EN EL DESCRIPTIVO");
	                	validacionesGroup.setVisibility(View.INVISIBLE);
	                	Material.setText(" ");
	                	descriptivo.setText("");
	                	azucar.setText("");  
	                	bote.setText("");  
	                	brocha.setText("");  
	   				 	empleado.setText(""); 
	   				 	validoDescriptivo = false;
	   				 	ValorPolinizacion = 0; 
	   				 	descriptivo.setEnabled(true);
	   				 	azucar.setEnabled(true);
	   				 	bote.setEnabled(true);
	   				 	brocha.setEnabled(true);
	                	descriptivo.requestFocus();
                		}    
                	}else{
                		validoDescriptivo = false;
                		VerificarDatos(descriptivo.getText().toString().trim());
	                	if (validoDescriptivo == true){
                		GuardarDatos();
	                	}else{
	                	alertDialogMensaje("ERROR","ERROR EN EL DESCRIPTIVO");
	                	validacionesGroup.setVisibility(View.INVISIBLE);
	                	Material.setText(" ");
	                	descriptivo.setText("");
	                	azucar.setText("");  
	                	bote.setText("");  
	                	brocha.setText("");  
	   				 	empleado.setText(""); 
	   				 	validoDescriptivo = false;
	   				 	ValorPolinizacion = 0; 
	   				 	descriptivo.setEnabled(true);
	   				 	azucar.setEnabled(true);
	   				 	bote.setEnabled(true);
	   				 	brocha.setEnabled(true);
	   				 	azucar.setBackgroundColor(Color.WHITE);
	   				 	bote.setBackgroundColor(Color.WHITE);
	   				 	brocha.setBackgroundColor(Color.WHITE);
	                	descriptivo.requestFocus();
	                	}
                } 
                return true; 
            }
            
            return false;
        } 
    });	 
                     
	    //se limpian los campos y variables para una nueva captura
	    buttonNuevo.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	        	//-----------------------------------------------------------------------------------
        		String correlativo="";
        		    	//almaceno el empleado en una variable string
        		    	String getEmpleado = empleado.getText().toString().trim();
        		    	// guardo la variable que tiene empleado en un array
        		    	String[]empl = new String[]{getEmpleado};
        		    	Cursor obtenerCorrelativo = db.getCorrelativo(empl);
        		    	//recorro dicho arreglo para obtener el campo
        		    	if (obtenerCorrelativo.moveToFirst()){
        		    		do{
        		    			correlativo = obtenerCorrelativo.getString(0);
        		    		}while(obtenerCorrelativo.moveToNext());
        		    	}
        		    	
	        	 validacionesGroup.setVisibility(View.INVISIBLE);
				 Material.setText(" ");
				 descriptivo.setText("");
				 azucar.setText("");  
				 bote.setText("");  
				 brocha.setText("");  
				 empleado.setText(""); 
				 validoDescriptivo = false;
				 ValorPolinizacion = 0; 
				 descriptivo.setEnabled(true);
				 azucar.setEnabled(true);
				 bote.setEnabled(true);
				 brocha.setEnabled(true);
				 azucar.setBackgroundColor(Color.WHITE);
				 bote.setBackgroundColor(Color.WHITE);
				 brocha.setBackgroundColor(Color.WHITE);
				 descriptivo.requestFocus();
				 
				 
	        }
	    });
        
        btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
 
            	onBackPressed();
            }
        });
                
        btnInicio.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Intent intent = new Intent(getApplicationContext(), MAINACTIVITY.class);
           	 	startActivity(intent);
            }
        });
          
	}
	
	public void verificarAsginados(){
		Cursor CursorGuardados =	db.getDguardados("Actividad=" + CodigoOpcion + " AND ActividadFinalizada=0" );
		TotalEmpleadosAsignados = CursorGuardados.getCount();
		TCantidadAsignados.setText(TotalEmpleadosAsignados + "");
		CursorGuardados.close();
	}
	
	//retornara un valor dependiendo del estado del empleado
	// 0 = no existe
	// 1 = existe pero esta asignado
	// 2 = existe y no esta asignado
	public int VerificarEmpleado(String empleado){
		int valor = 0;
		Cursor CursorDacceso =	db.getDacceso("CodigoEmpleado='" + empleado + "'");
		if(CursorDacceso != null  && CursorDacceso.getCount()>0){
			Cursor CursorGuardados =	db.getDguardados("CodigoEmpleado=" + empleado + " AND ActividadFinalizada=0");
			if(CursorGuardados.getCount()>0){
				valor = 1;
			}else{
				valor = 2;
			}
			CursorGuardados.close();
		}
		CursorDacceso.close();
		return valor;
	}
	
	public void alertDialogMensaje(String message1, String mesage2){
		
		//mVibrator.vibrate(300);
		
		try {
		    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
		    ringtone.play();
		    
		    
		} catch (Exception e) {
		    e.printStackTrace();
		}
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(message1);
		alertDialog.setMessage(mesage2);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int which) {
		// here you can add functions
			ringtone.stop();
		}
		});
		alertDialog.show();
	}
		
	public void VerificarDatos(String dato){	
		 Cursor CursorSemillas = db.getSemillas("Siembra='" + dato + "'");
		 if(CursorSemillas.getCount()>0){
			 Semillas s = db.getSemillasFromCursor(CursorSemillas, 0);
			 Log.d("SEMILLA",s.getSiembra() + ", "+s.getMaterial() + " ," + s.getPolinizacion());
			 
			 if(OpcionCosecha==1){
				 //si se entra a captura con la opcion de cosechas
					validacionesGroup.setVisibility(View.VISIBLE);
					azucar.setVisibility(View.VISIBLE);
					azucar.setText("");
					TextviewAzucarera.setText("Bolsa 1");
					bote.setVisibility(View.VISIBLE);
					bote.setText("");
					TextviewBote.setText("Bolsa 2");
					brocha.setVisibility(View.VISIBLE);
					brocha.setText("");
					TextviewBrocha.setText("Bolsa 3");
					azucar.requestFocus();
 					
				
				 
				 /*
				//agarege codigo para las bolsas de cosecha
				//Cursor CursorBolsas = db.getDbolsas("Codigo='" + dato + "'");
				Cursor CursorBolsas = db.getDbolsas("Codigo='" + azucar.getText().toString().trim() + "'");
				if (CursorBolsas.getCount()>0){
					Dbolsas bl = db.getDbolsasFromCursor(CursorBolsas,0);
					
					validoDescriptivo = true;
					 Material.setText(bl.getCodigo());
				}
				//Fin codigo para las bolsas de cosecha
				*/
				//CODIGO AGREGADO
				
			 }else if(OpcionSuccion == 1){ 
				 if (checkbox1.isChecked() == true){ 
					 validacionesGroup.setVisibility(View.VISIBLE);
					 TextviewAzucarera.setText("");
					 azucar.setVisibility(View.INVISIBLE);
					 TextviewBote.setText("Pipeta");
					 bote.setEnabled(true);
					 bote.setVisibility(View.VISIBLE);
					 bote.requestFocus();
					 brocha.setVisibility(View.INVISIBLE);
					 TextviewBrocha.setText("");;
				 } else{
					 validacionesGroup.setVisibility(View.VISIBLE);
					 azucar.setEnabled(true);
					 azucar.setVisibility(View.VISIBLE);
					 TextviewAzucarera.setText("Filtro");
					 TextviewBote.setText("Pipeta");
					 bote.setEnabled(true);
					 bote.setVisibility(View.VISIBLE);
					 brocha.setVisibility(View.INVISIBLE);
					 TextviewBrocha.setText("");				 }
					
				 //FIN CODIGO AGREGADO
				
			 }else if(s.getPolinizacion() == 0 && OpcionPolinizacion==1) {
				 validacionesGroup.setVisibility(View.VISIBLE);
				 azucar.setVisibility(View.INVISIBLE);
				 bote.setVisibility(View.VISIBLE);
				 brocha.setVisibility(View.INVISIBLE);
				 TextviewAzucarera.setText("");
				 TextviewBrocha.setText("");
				 TextviewBote.setText("Bote");
				 ValorPolinizacion=0;
			 
			 }else if(s.getPolinizacion() == 1 && OpcionPolinizacion==1){
				 validacionesGroup.setVisibility(View.VISIBLE);				 
				 azucar.setVisibility(View.VISIBLE);
				 bote.setVisibility(View.VISIBLE);
				 brocha.setVisibility(View.VISIBLE);
				 TextviewAzucarera.setText("Azucarera");
				 TextviewBrocha.setText("Brocha");
				 TextviewBote.setText("Bote");
				 ValorPolinizacion=1;
			 }else if(s.getPolinizacion() == 2 && OpcionPolinizacion==1){
				 validacionesGroup.setVisibility(View.VISIBLE);				 
				 azucar.setVisibility(View.INVISIBLE);
				 bote.setVisibility(View.VISIBLE);
				 brocha.setVisibility(View.INVISIBLE);
				 TextviewAzucarera.setText("");
				 TextviewBrocha.setText("");
				 TextviewBote.setText("Pincel");
				 ValorPolinizacion=2;
			 } else if(s.getPolinizacion() == 3 && OpcionPolinizacion==1){
				 validacionesGroup.setVisibility(View.VISIBLE);				 
				 //azucar.setVisibility(View.INVISIBLE);
				 bote.setVisibility(View.INVISIBLE);
				 //brocha.setVisibility(View.INVISIBLE);
				 TextviewAzucarera.setText(" Macho ");
				 TextviewBrocha.setText(" Brocha ");
				 TextviewBote.setText("");
				 ValorPolinizacion=3;
			 } else if (s.getPolinizacion() == 4 && OpcionPolinizacion==1){
				 validacionesGroup.setVisibility(View.VISIBLE);				 
				 azucar.setVisibility(View.INVISIBLE);
				 bote.setVisibility(View.INVISIBLE);
				 brocha.setVisibility(View.VISIBLE);
				 TextviewAzucarera.setText("");
				 TextviewBrocha.setText("Brocha");
				 TextviewBote.setText("");
				 ValorPolinizacion=4;
			 }
			 validoDescriptivo = true;
			 Material.setText(s.getMaterial());
			 
		 }else{
			 validacionesGroup.setVisibility(View.INVISIBLE);
			 Material.setText("");
			 ValorPolinizacion=0;
			 validoDescriptivo = false;
		 }
		 CursorSemillas.close();
	}
	
	public void ValidaCampos(String vali){
		Material1.setText("");
 		VerificarDatosbolsa(vali);
		String bolv = Material1.getText().toString().trim();
		String desv = descriptivo.getText().toString().trim();
		if(desv.equals(bolv)){
		  confirma2 = true;
		}
		else{
			confirma2 = false;
		}
	
	}
	
	public String VerificarFiltro(String dato) {
		Cursor CursorFiltros = db.getDFiltros ("Id='" + dato + "'");
		 if(CursorFiltros != null  && CursorFiltros.getCount()>0){
				 Filtros f = db.getDFiltrosFromCursor(CursorFiltros, 0);
				 Log.d("FILTROS",f.getNoSiembra() + ", "+f.getMaterial() + " ," + f.getFechaSuccion());
				 SiembraF = f.getNoSiembra();
		 }else{
			 SiembraF = "";		 
		 }
		 CursorFiltros.close();
		return SiembraF;
	}
	
	public void validarDescriptivo(String dato){
		Cursor CursorSemillas = db.getSemillas("Siembra='" + dato + "'");
		 if(CursorSemillas.getCount()>0){
			 Semillas s = db.getSemillasFromCursor(CursorSemillas, 0);
			 Log.d("SEMILLA",s.getSiembra() + ", "+s.getMaterial() + " ," + s.getPolinizacion());
			 
			 validoDescriptivo = true;
			 Material.setText(s.getMaterial());
		 }
		 CursorSemillas.close();
	}
	
	public void GuardarDatos(){
		String empleadoEscaneado = empleado.getText().toString().trim();
    	int existe = VerificarEmpleado( empleadoEscaneado );
        
    	if(existe == 2){
        		Dguardados dtemp = new Dguardados();
        		dtemp.setCodigoEmpleado(Integer.parseInt(empleadoEscaneado));
        		dtemp.setCodigoDepto(Codigodepartamento);
        		dtemp.setSiembra(descriptivo.getText().toString().trim());
        		
        		//-----------------------------------------------------------------------------------
        		String correlativo="";
        		    	//almaceno el empleado en una variable string
        		    	String getEmpleado = empleado.getText().toString().trim();
        		    	// guardo la variable que tiene empleado en un array
        		    	String[]empl = new String[]{getEmpleado};
        		    	Cursor obtenerCorrelativo = db.getCorrelativo(empl);
        		    	//recorro dicho arreglo para obtener el campo
        		    	if (obtenerCorrelativo.moveToFirst()){
        		    		do{
        		    			correlativo = obtenerCorrelativo.getString(0);
        		    		}while(obtenerCorrelativo.moveToNext());
        		    	}
        		//-------------------------------------------------------------------------
        		
        		Calendar c = Calendar.getInstance(); 
        		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
        		String fecha = fdate.format(c.getTime());
        		SimpleDateFormat ftime = new SimpleDateFormat("HH:mm:ss");
        		String horaInicio = ftime.format(c.getTime());
        		dtemp.setFecha(fecha);
        		dtemp.setHoraIncio(horaInicio);
        		dtemp.setActividad(CodigoOpcion);
        		dtemp.setSupervisor(CodigoEmpleado);
        		dtemp.setCorrelativoBolsa(azucar.getText().toString().trim());
        		dtemp.setActividadFinalizada(0);
        		dtemp.setCorrelativoEmpleado(correlativo);
        		
        		//Guarda Bolsas Adicionales
        		dtemp.setCorrerlativoBolsa2(bote.getText().toString().trim());
        		dtemp.setCorrerlativoBolsa3(brocha.getText().toString().trim());
        		//Fin graba bolsas adicionales
        		
        		
        	
        		Log.d("Dguardados", dtemp.toString().trim());
        		
        		db.insertarDguardados(dtemp);
        		
        		//generacion archivo json
        		ObjectMapper mapper = new ObjectMapper();
        		try {
        			String nameJson ="G_" + dtemp.getCodigoEmpleado() + "_"+dtemp.getFecha() + "_"+dtemp.getHoraIncio();
        			nameJson=nameJson.replace(":", "-");	
					mapper.writeValue(new File( Environment.getExternalStorageDirectory()+"/SIM/JsonDescarga/" + nameJson+ ".json"), dtemp);
					
					JsonFiles archivo = new JsonFiles();
					archivo.setName(nameJson+ ".json");
					archivo.setNameFolder("SinFinalizar/");
					archivo.setUpload(0);
					db.insertarJsonFile(archivo);
					
					if(!appState.getSubiendoArchivos()){
						appState.setSubiendoArchivos(true);
						new UptoDropbox().execute(getApplicationContext());
					}
					
        		
        		} catch (JsonGenerationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JsonMappingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        		
        		
        		empleado.setText("");  
        		azucar.setText("");
        		bote.setText("");
        		brocha.setText("");
        		azucar.setBackgroundColor(Color.WHITE);
				bote.setBackgroundColor(Color.WHITE);
				brocha.setBackgroundColor(Color.WHITE);
        		azucar.setEnabled(true);		
        		bote.setEnabled(true);
        		brocha.setEnabled(true);               		
        		VerificarDatos(descriptivo.getText().toString().trim());
            	if (validoDescriptivo == true){
            		if(OpcionCosecha == 1){
            			azucar.requestFocus();
        
            		}else if(OpcionPolinizacion == 1 && ValorPolinizacion == 0){
            			bote.requestFocus();
            		}else if(OpcionPolinizacion == 1 && ValorPolinizacion == 1){
            			azucar.requestFocus();
            		}else if(OpcionPolinizacion == 1 && ValorPolinizacion == 2){
            			bote.requestFocus();  			
            		}else if(OpcionPolinizacion == 1 && ValorPolinizacion == 4){
            			brocha.requestFocus();
            		}else if (OpcionSuccion==1){
            			azucar.setEnabled(true);
            			azucar.requestFocus();
            		}else{
            			empleado.requestFocus();
            		}
            	}
        		
        		TotalEmpleadosAsignados++;
        		TCantidadAsignados.setText(TotalEmpleadosAsignados + "");
        		
        		
        	}else if(existe == 1){
        		alertDialogMensaje("Asignado", "Este usuario ya ha sido asignado");
        		empleado.setText("");
        		empleado.requestFocus();
        		//VerificarDatos(descriptivo.getText().toString().trim());
        	}else if(existe == 0){
        		alertDialogMensaje("Marcaje", "No existe marcaje para este usuario");
        		empleado.setText("");
        		empleado.requestFocus();
        		 //VerificarDatos(descriptivo.getText().toString().trim());	
    	}
	}
	
	public void VerificarDatosbolsa(String dato){	
		
		 Cursor CursorSemillas = db.getDbolsas("Codigo='" + dato + "'");
		 if(CursorSemillas.getCount()>0){
			 Dbolsas s = db.getDbolsasFromCursor(CursorSemillas, 0);
			 Log.d("SEMILLA",s.getSiembra() + ", "+s.getMaterial() + " ," + s.getCodigo());
			 
			 if(OpcionCosecha==1){
				 //si se entra a captura con la opcion de cosechas
				validacionesGroup.setVisibility(View.VISIBLE);		
				bote.setVisibility(View.VISIBLE);
				brocha.setVisibility(View.VISIBLE);
				TextviewAzucarera.setText("Bolsa 1");
				TextviewBote.setText("Bolsa 2");
				TextviewBrocha.setText("Bolsa 3");
				//CODIGO AGREGADO
			 }else if(OpcionSuccion == 1){
				 validacionesGroup.setVisibility(View.VISIBLE);
				 TextviewAzucarera.setText("Filtro");
				 TextviewBote.setText("Pipeta");;
				 brocha.setVisibility(View.INVISIBLE);
				 TextviewBrocha.setText("");;
				 //FIN CODIGO AGREGADO
						
			 }else{
				 validacionesGroup.setVisibility(View.INVISIBLE);
				 ValorPolinizacion=0;
			 }
			 //validoDescriptivo = true;
			 Material1.setText(s.getSiembra());		 		 
		 }else{
			 validacionesGroup.setVisibility(View.VISIBLE);
			 ValorPolinizacion=0;
			 validoDescriptivo = false;
			// alertDialogMensaje("Mensaje", "EL valor es invalido");
			 
		 }
		 CursorSemillas.close();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.captura_general, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}


/*
	String empleadoEscaneado = empleado.getText().toString().trim();
int existe = VerificarEmpleado( empleadoEscaneado );

if(existe == 2){
		Dguardados dtemp = new Dguardados();
		dtemp.setCodigoEmpleado(Integer.parseInt(empleadoEscaneado));
		dtemp.setCodigoDepto(Codigodepartamento);
		dtemp.setSiembra(Integer.parseInt(descriptivo.getText().toString().trim()));
		
		Calendar c = Calendar.getInstance(); 
		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
		String fecha = fdate.format(c.getTime());
		SimpleDateFormat ftime = new SimpleDateFormat("HH:mm:ss");
		String horaInicio = ftime.format(c.getTime());
		//CorrelativoBolsa = azucar.getText().toString().trim();
		
		
		//String fecha = c.get(Calendar.DAY_OF_MONTH) + "-" + (c.get(Calendar.MONTH)+1) + "-"+c.get(Calendar.YEAR);
		dtemp.setFecha(fecha);
		//String horaInicio = c.get(Calendar.HOUR) + ":" +c.get(Calendar.MINUTE) + ":"+c.get(Calendar.SECOND);
		dtemp.setHoraIncio(horaInicio);
		dtemp.setActividad(CodigoOpcion);
		dtemp.setSupervisor(CodigoEmpleado);
		dtemp.setCorrerlativoBolsa(azucar.getText().toString().trim());
		dtemp.setActividadFinalizada(0);
		
		//Guarda Bolsas Adicionales
		dtemp.setCorrerlativoBolsa2(bote.getText().toString().trim());
		dtemp.setCorrerlativoBolsa3(brocha.getText().toString().trim());
		//Fin graba bolsas adicionales
		
		
	
		Log.d("Dguardados", dtemp.toString().trim());
		
		db.insertarDguardados(dtemp);
		
		//generacion archivo json
		ObjectMapper mapper = new ObjectMapper();
		try {
			String nameJson ="G_" + dtemp.getCodigoEmpleado() + "_"+dtemp.getFecha() + "_"+dtemp.getHoraIncio();
			nameJson=nameJson.replace(":", "-");	
			mapper.writeValue(new File( Environment.getExternalStorageDirectory()+"/Floricultura/JsonDescarga/" + nameJson+ ".json"), dtemp);
			
			JsonFiles archivo = new JsonFiles();
			archivo.setName(nameJson+ ".json");
			archivo.setNameFolder("SinFinalizar/");
			archivo.setUpload(0);
			db.insertarJsonFile(archivo);
			
			if(!appState.getSubiendoArchivos()){
				appState.setSubiendoArchivos(true);
				new UptoDropbox().execute(getApplicationContext());
			}
			
		
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		empleado.setText("");  
		azucar.setText("");
		bote.setText("");
		brocha.setText("");
		azucar.setEnabled(true);		
		bote.setEnabled(true);
		brocha.setEnabled(true);               		
		VerificarDatos(descriptivo.getText().toString().trim());
    	if (validoDescriptivo == true){
    		if(OpcionCosecha == 1){
    			azucar.requestFocus();

    		}else if(OpcionPolinizacion == 1 && ValorPolinizacion == 0){
    			bote.requestFocus();
    		}else if(OpcionPolinizacion == 1 && ValorPolinizacion == 1){
    			azucar.requestFocus();
    		}else if(OpcionPolinizacion == 1 && ValorPolinizacion == 2){
    			bote.requestFocus();  			
    		}else if (OpcionSuccion==1){
    			azucar.setEnabled(true);
    			azucar.requestFocus();
    		}else{
    			empleado.requestFocus();
    		}
    	}
		
		TotalEmpleadosAsignados++;
		TCantidadAsignados.setText(TotalEmpleadosAsignados + "");
		
		
	}else if(existe == 1){
		alertDialogMensaje("Asignado", "Este usuario ya ha sido asignado");
		empleado.setText("");
		empleado.requestFocus();
		//VerificarDatos(descriptivo.getText().toString().trim());
	}else if(existe == 0){
		alertDialogMensaje("Marcaje", "No existe marcaje para este usuario");
		empleado.setText("");
		empleado.requestFocus();
		 //VerificarDatos(descriptivo.getText().toString().trim());	
} 
    return true; 
}

return false;
} 
});


//listener para verificar el campo descriptivo
descriptivo.addTextChangedListener(new TextWatcher() {

public void afterTextChanged(Editable s) {

   	Log.d("TEXTOAPP", descriptivo.getText().toString().trim());
   	VerificarDatos(descriptivo.getText().toString().trim());
}

public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

public void onTextChanged(CharSequence s, int start, int before, int count) {}
});

*/
