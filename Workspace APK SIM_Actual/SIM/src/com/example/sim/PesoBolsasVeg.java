package com.example.sim;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.floricultura.R;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.EtiquetasVegetativo;
import com.example.sim.data.JSONReader;
import com.example.sim.data.JsonFiles;
import com.example.sim.data.RangoEtiqueta;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class PesoBolsasVeg extends ActionBarActivity {
	
	private MyApp appState;	
	private DBAdapter db;
	int CodigoModulo;
	int Codigodepartamento;
	int CodigoEmpleado;
	int CodigoOpcion;
	int EsCorte;
	int TotalEmpleadosAsignados;
	int TotalEtiquetasAsignadas;
	boolean validoInvernadero;
	String NombreOpcion;
	EditText peso;
	EditText etiqueta;
	TextView TCantidadAsignados;
	Ringtone ringtone;
	RangoEtiqueta rangos;
	


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_peso_bolsasveg);
		
		
		Bundle extras = getIntent().getExtras();
		CodigoModulo = extras.getInt("CodigoModulo");
	    CodigoEmpleado = extras.getInt("CodigoEmpleado");
	    Codigodepartamento = extras.getInt("Codigodepartamento");
	    CodigoOpcion = extras.getInt("CodigoOpcion");
	    NombreOpcion = extras.getString("NombreOpcion");
	    final Spinner sp = (Spinner) findViewById(R.id.spclima);
	    final Spinner sp1 = (Spinner) findViewById(R.id.spTipo);
	    final Spinner sp2 = (Spinner) findViewById(R.id.spcalidad);
	    
	    TextView TNombreCaptura = (TextView)findViewById(R.id.OpcionCaptura);
	    TextView TCodigoEmpleado= (TextView)findViewById(R.id.CodigoEmpleado);
	    TextView TCodigoOpcion= (TextView)findViewById(R.id.codigoOpcion);
	    TextView textViewEtiqueta= (TextView)findViewById(R.id.textViewEtiqueta);
	    TCantidadAsignados = (TextView)findViewById(R.id.textEmpleadosAsginados);
	    peso  = (EditText)findViewById(R.id.editPesoBolsa);
	    etiqueta  = (EditText)findViewById(R.id.editEtiqueta);
	    final String[] dias = {"SOLEADO","NUBLADO","LLUVIOSO"};
        final ArrayAdapter<String> adap = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item, dias);
        sp.setAdapter(adap);
        final String[] tipo = {"BOLSA","ESQUEJE"};
        final ArrayAdapter<String> adapt = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item, tipo);
        sp1.setAdapter(adapt);
        final String[] tipoCalidad = {"1","2"};
        final ArrayAdapter<String> adapte = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item, tipoCalidad);
        sp2.setAdapter(adapte);
	    
	    Button bntGuardar = (Button) findViewById(R.id.buttonGuarda);
	    Button btnBack = (Button) findViewById(R.id.btnBack);
	    Button btnNuevo = (Button) findViewById(R.id.btnNuevo);
	    
	    appState = ((MyApp)getApplicationContext());   
	    db = appState.getDb();
	    db.open();
	    
	    rangos = new RangoEtiqueta();
	    
	    
	    TNombreCaptura.setText(NombreOpcion+ "");
	    TCodigoEmpleado.setText(CodigoEmpleado + "");
	    TCodigoOpcion.setText(CodigoOpcion + "");
	   
	    obtenerRangoEtq();
	    
	    verificarAsginados();
	    
	    //se limpian los campos y variables para una nueva captura
	    btnNuevo.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	        	etiqueta.setText("");
	        	peso.setText("");
	        	etiqueta.setEnabled(true);
	        	etiqueta.requestFocus(); 	
	        }
	    });
	    
	    
	   	etiqueta.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                	String etq = etiqueta.getText().toString().trim();
                if (ValidarEtiqueta(Integer.parseInt(etq)) == true){	
                	etiqueta.setEnabled(false);
                	peso.requestFocus();
                	}else {
                		etiqueta.setText("");
                		etiqueta.requestFocus();
                	}
                return true;
                }
                return false;
            }
        });
	    
	   	bntGuardar.setOnClickListener(new View.OnClickListener() {
	            public void onClick(View v) {
                	String val = ("");
                	String et = etiqueta.getText().toString().trim();
                	String Ps = peso.getText().toString().trim();
                	if(et.equals(val)){
                	 etiqueta.requestFocus();
                	 etiqueta.setText("");
                	 peso.setText("");
               		 alertDialogMensaje("Mensaje", "Error Falta ETIQUETA");	
                	}else if(Ps.equals(val) || Ps.equals(val)){
                		 peso.setText("");
                  		 alertDialogMensaje("Mensaje", "Error debe ingresar PESO");
                  		 peso.requestFocus();
                	}else{
                		if (ValidarEtiqueta(Integer.parseInt(et)) == true){                			
                    		EtiquetasVegetativo EVtemp = new EtiquetasVegetativo();
                    		
                    		String empleadoEscaneado = "9999";
                    		EVtemp.setCodigoEmpleado(Integer.parseInt(sp2.getSelectedItem().toString().trim()));
                    		EVtemp.setEtiqueta(etiqueta.getText().toString().trim());
                    		EVtemp.setFinalizado(0);
                    		EVtemp.setInvernadero(sp.getSelectedItem().toString().trim());  
                    		EVtemp.setTipo(sp1.getSelectedItem().toString().trim());
                    		Calendar c = Calendar.getInstance(); 
                    		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
                    		String fecha = fdate.format(c.getTime());
                    		SimpleDateFormat ftime = new SimpleDateFormat("HH:mm:ss");
                    		String horaInicio = ftime.format(c.getTime());      		
	                		EVtemp.setFecha(fecha);
	                		EVtemp.setHora(horaInicio);
	                		EVtemp.setSupervisor(CodigoEmpleado);
	                		EVtemp.setActividad(CodigoOpcion);
	                		EVtemp.setPeso(peso.getText().toString().trim());
	                		ObjectMapper mapper = new ObjectMapper();
                    		mapper = new ObjectMapper();
                    		try {
                    			String nameJson = "Pv_" +empleadoEscaneado + "_"+EVtemp.getFecha() + "_"+EVtemp.getHora() + "_"+etiqueta.getText().toString().trim();
                    			nameJson=nameJson.replace(":", "-");
								mapper.writeValue(new File( Environment.getExternalStorageDirectory()+"/SIM/JsonDescarga/" + nameJson+ ".json"), EVtemp);	
								JsonFiles archivo = new JsonFiles();
								archivo.setName(nameJson+ ".json");
								archivo.setNameFolder("SinFinalizar/");
								archivo.setUpload(0);
								db.insertarJsonFile(archivo);
								
								if(!appState.getSubiendoArchivos()){
									appState.setSubiendoArchivos(true);
									new UptoDropbox().execute(getApplicationContext());
								}
								
							} catch (JsonGenerationException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (JsonMappingException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
	                		                 		
                    		db.insertarEtiquetas(EVtemp);
                    		peso.setEnabled(true);
                    		peso.setText("");
                    		peso.requestFocus();
                    		TotalEtiquetasAsignadas++;
	                		TCantidadAsignados.setText(TotalEtiquetasAsignadas + "");
                    		
                		}else{
                			etiqueta.setText("");
                			peso.setText("");
                    		etiqueta.requestFocus();                    		
                		}
                	}
	            }
        });
	    
       
        btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
 
            	onBackPressed();
            }
        });
        
        

	}
	
	
	public void verificarAsginados(){
		Cursor CursorGuardados =	db.getDguardados("Actividad=" + CodigoOpcion + " AND ActividadFinalizada=0" );
		TotalEmpleadosAsignados = CursorGuardados.getCount();
		TCantidadAsignados.setText(TotalEmpleadosAsignados + "");
		CursorGuardados.close();
	}
	
	public void EtiquetasAsginadas(){
		//Cursor cursorEtiquetaG =	db.getEtiquetas("Actividad=" + CodigoOpcion + " AND CodigoEmpleado=" + empleado +"");
		Cursor cursorEtiquetaG =	db.getEtiquetas("Actividad");
		TotalEtiquetasAsignadas = cursorEtiquetaG.getCount();
		TCantidadAsignados.setText(TotalEtiquetasAsignadas + "");
		cursorEtiquetaG.close();
	}
		
	public void obtenerRangoEtiqueta(){
		
		JSONReader _reader = appState.getReader();
		ArrayList<RangoEtiqueta> r = (ArrayList<RangoEtiqueta>) ( _reader.ReadFromSDCard(6, "SIM/JsonCarga/D_Vegetativo.json"));
		if(r.size()>0){
			this.rangos = r.get(0);
		}
		
	}
	
	public void alertDialogMensaje(String message1, String mesage2){
		

		
		try {
		    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
		    ringtone.play();
		    
		    
		} catch (Exception e) {
		    e.printStackTrace();
		}
		
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(message1);
		alertDialog.setMessage(mesage2);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int which) {
		// here you can add functions
			ringtone.stop();
		}
		});
		alertDialog.show();
	}
		
	
	public void obtenerRangoEtq(){
		
		JSONReader _reader = appState.getReader();
		ArrayList<RangoEtiqueta> r = (ArrayList<RangoEtiqueta>) ( _reader.ReadFromSDCard(6, "SIM/JsonCarga/D_Vegetativo.json"));
		if(r.size()>0){
			this.rangos = r.get(0);
		}
		
	}
	
	public boolean ValidarEtiqueta(int dato){
		boolean valido = false;
		
		//se hace la consulta para validar si la etiqueta ya fue ingresada en el sistema
		Cursor cursorEtiqueta = db.getEtiquetas("Etiqueta=" + dato + "'");
		if(cursorEtiqueta.getCount() > 0 ){
			valido = false;
			alertDialogMensaje("Error Etiqueta", "Etiqueta YA FUE ESCANEADA");
		}else{
			//se valida el rango de la etiqueta
			int valorEtiqueta = dato;
			Log.d("valorEtiqueta",valorEtiqueta + "");
			Log.d("valorEtiqueta", this.rangos.getInicio() + "" + this.rangos.getFin());
			
			if(valorEtiqueta > this.rangos.getInicio() && valorEtiqueta<this.rangos.getFin()){
				valido = true;
			}else{
				alertDialogMensaje("Error Etiqueta", "Valor de Etiqueta NO ENCONTRADO \n\n Favor descargar D_VEGETATIVO");
			}
			
		}
		cursorEtiqueta.close();
		return valido;
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.captura_vegetativo, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void ButtonOnClick(View v) {
	    switch (v.getId()) {
	      case R.id.buttonborra:
		        agregarNumero("1");
	        break;
	      case R.id.btnDos:
	    	  agregarNumero("2");
	        break;
	      case R.id.btnTres:
		        agregarNumero("3");
	        break;
	      case R.id.btnCuatro:
	    	  agregarNumero("4");
	        break;
	      case R.id.btnCinco:
		        agregarNumero("5");
	        break;
	      case R.id.btnSeis:
	    	  agregarNumero("6");
	        break;
	      case R.id.btnSiete:
	    	  agregarNumero("7");
	        break;
	      case R.id.btnOcho:
		        agregarNumero("8");
	        break;
	      case R.id.btnNueve:
	    	  agregarNumero("9");
	        break;
	      case R.id.button0:
		        agregarNumero("0");
	        break;
	      case R.id.btnCero:
	    	  agregarNumero("b");
	        break;
	      case R.id.Button11:
	    	  agregarNumero(".");
	        break;
	      }
	     
	}
	
	public void agregarNumero(String n){
		
		if(n.contains("b")){
			
			if(peso.isFocused()){
				peso.setText("");			
			}
			
		}else{
			if(peso.isFocused()){
				peso.setText(peso.getText() + n);
			}
		}
		

	}

}