package com.example.sim;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Vibrator;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.floricultura.R;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.Dazucareras;
import com.example.sim.data.Dbolsas;
import com.example.sim.data.Dguardados;
import com.example.sim.data.Filtros;
import com.example.sim.data.JsonFiles;
import com.example.sim.data.Opciones;
import com.example.sim.data.Semillas;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Reportest extends ActionBarActivity {
	
	//Variables globales
	private MyApp appState;	
	private DBAdapter db;
	int CodigoModulo;
	int Codigodepartamento;
	int CodigoEmpleado;
	int CodigoOpcion;
	int OpcionPolinizacion;
	int OpcionSuccion;
	int ValorPolinizacion = 0;
	int ValorSuccion=0;
	int TotalEmpleadosAsignados;
	int OpcionCosecha;
	boolean validoDescriptivo;
	boolean confirma = true;
	boolean confirma2 = true;
	String NombreOpcion;
	String Val_Bolsa;
	//String SiembraF = "";
	LinearLayout validacionesGroup;
	String Material;
	//TextView Material1;
	TextView TCantidadAsignados;
	EditText Parametro1;
	EditText Parametro2;
	EditText Parametro3;
	EditText descriptivo;
	EditText empleado;
	TextView TextP1;
	TextView TextP2;
	TextView TextP3; 
	Vibrator mVibrator;
	Ringtone ringtone;
	CheckBox checkbox1;
	String Fecha = "";
	int Nparametros = 0;
	int TipoValP1 = 1;  // Validaci�n por defecto 1 = Siembra
	int TipoValP2 = 1;  // Validaci�n con 2 =  Material;  3 = Bolsa
	int TipoValP3 = 1;  // Validaci�n con 4 =  Azucarera;  5 = Filtro
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_reportest);
		
		
		//se obtienen lo datos de la actividad antetior por medio del intent
		Bundle extras = getIntent().getExtras();
		CodigoModulo = extras.getInt("CodigoModulo");
	    CodigoEmpleado = extras.getInt("CodigoEmpleado");
	    Codigodepartamento = extras.getInt("Codigodepartamento");
	    CodigoOpcion = extras.getInt("CodigoOpcion");
	    NombreOpcion = extras.getString("NombreOpcion");
  
	    //hacemos referencia a todos los objetos de nuestra vista
	    TextView TNombreCaptura = (TextView)findViewById(R.id.OpcionCaptura);
	    TextView TCodigoEmpleado= (TextView)findViewById(R.id.CodigoEmpleado);
	    TextView TCodigoOpcion= (TextView)findViewById(R.id.codigoOpcion);
	    TextP1 = (TextView)findViewById(R.id.TextP1);
	    TextP2 = (TextView)findViewById(R.id.TextP2);
	    TextP3 = (TextView)findViewById(R.id.TextP3);
	    
	    
	    
	    TCantidadAsignados = (TextView)findViewById(R.id.textEmpleadosAsginados);
	    validacionesGroup = (LinearLayout)findViewById(R.id.layoutValidaciones);
	    descriptivo = (EditText)findViewById(R.id.editTextDescriptivo);
	   // Material = (TextView)findViewById(R.id.textMaterial);
	   // Material1 = (TextView)findViewById(R.id.textMaterial1);
	    Parametro1  = (EditText)findViewById(R.id.editP1);
	    Parametro2  = (EditText)findViewById(R.id.editP2);
	    Parametro3  = (EditText)findViewById(R.id.editP3);
	    empleado  = (EditText)findViewById(R.id.editEmpleado);
	    checkbox1 = (CheckBox)findViewById(R.id.checkBox10);
	    Button buttonNuevo = (Button) findViewById(R.id.btnNuevo);
        Button btnBack = (Button) findViewById(R.id.btnBack);
        Button btnInicio = (Button) findViewById(R.id.btnInicio);
	    
	    
	    //hacemos las inicializaciones necesarias a nuestra vista
	    TNombreCaptura.setText(NombreOpcion+ "");
	    TCodigoEmpleado.setText(CodigoEmpleado + "");
	    TCodigoOpcion.setText(CodigoOpcion + "");
	    
	    /*if(NombreOpcion.contains("POLINIZACION")){
	    	OpcionPolinizacion = 1;
	    } else if(NombreOpcion.contains("COSECHAS")){
	    	OpcionCosecha = 1; 
	    } else if(NombreOpcion.contains("SUCCION")){
		    OpcionSuccion = 1;
	    }*/
	    
	    validoDescriptivo = false;
	    confirma = false;
	    confirma2 = false;
	    //validacionesGroup.setVisibility(View.INVISIBLE);
	    descriptivo.requestFocus();
	    empleado.setEnabled(false);
	    checkbox1.setVisibility(View.INVISIBLE);
	    TextP1.setVisibility(View.INVISIBLE);
	    TextP2.setVisibility(View.INVISIBLE);
	    TextP3.setVisibility(View.INVISIBLE);
	    Parametro1.setVisibility(View.INVISIBLE);
	    Parametro2.setVisibility(View.INVISIBLE);
	    Parametro3.setVisibility(View.INVISIBLE);
	    Parametro1.setEnabled(false);
	    Parametro2.setEnabled(false);
	    Parametro3.setEnabled(false);
	    Parametro1.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
	    Parametro1.setSingleLine(false);   // se utiliza para que el EditText no haga un Tab Automatico.
	    Parametro2.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
	    Parametro2.setSingleLine(false);   // se utiliza para que el EditText no haga un Tab Automatico.
	    Parametro3.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
	    Parametro3.setSingleLine(false);   // se utiliza para que el EditText no haga un Tab Automatico.
	    
	  
	    // Agrega CheckBox a La vista si opcion es seleccionada y agrega el texto que corresponde   
	 /*   if (OpcionSuccion == 1){
	    		checkbox1.setVisibility(View.VISIBLE);
	    } else if(OpcionCosecha == 1){
	    			checkbox1.setVisibility(View.VISIBLE);
	    			checkbox1.setText("VARIAS BOLSAS ");
	    }else{
	    	checkbox1.setVisibility(View.INVISIBLE);
	    }
	   //fin agrega checkbox
	    */
	    
	    //incializamos la BD asi como tambien obtenemos la referencia al singleton
	    appState = ((MyApp)getApplicationContext());   
	    db = appState.getDb();
	    db.open();
	    Calendar c = Calendar.getInstance(); 
		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
		Fecha = fdate.format(c.getTime());
	    VerificarParametros();
	    verificarAsginados();
	    	    
	    //al recibir el enter se verifica si el descriptivo es correcto y lo manda al siguiente input sino le muestra un mensaje al usuario
        descriptivo.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                		VerificarDatos(descriptivo.getText().toString().trim());
	                	if (validoDescriptivo == true){
	                		descriptivo.setEnabled(false);
	                		if(Nparametros > 0){
	                			Parametro1.setEnabled(true);
	                			Parametro1.requestFocus();
	                		} else {
	                			empleado.requestFocus();
	                			empleado.setEnabled(true);
	                		}
	                	}else{
		    	    		alertDialogMensaje("Descriptivo", "Descriptivo Invalido");
		    	    		descriptivo.setEnabled(true);
		    	    		descriptivo.setText("");
		    	    		descriptivo.requestFocus();
	                	}
	                	return true;  
                }     
                return false;
            }  
        });
               
	    //Se verfica el input azucarera al persionar enter
        Parametro1.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	// se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                	String texto = TextP1.getText().toString().trim().toUpperCase();
                	boolean valor = false;
                	if(TipoValP1 == 1){
                		 valor = VerificarSiembra(Parametro1.getText().toString().trim());
                	} else if(TipoValP1 == 2) {
                		 valor = VerificarMaterial(Parametro1.getText().toString().trim());
                	} else if(TipoValP1 == 3){
                		 valor = VerificarBolsa(Parametro1.getText().toString().trim());
                	} else if(TipoValP1 == 4){
               		 	 valor = VerificarAzucarera(Parametro1.getText().toString().trim());
                	} else if(TipoValP1 == 5){
                		 valor = VerificarFiltro(Parametro1.getText().toString().trim());
                	}
                	
                	if(valor == true && Nparametros > 1){
            			Parametro2.setEnabled(true);
            			Parametro2.requestFocus();
            			Parametro1.setEnabled(false);
            		}else if (valor == true && Nparametros < 2){
            			empleado.requestFocus();
            			empleado.setEnabled(true);
            			Parametro1.setEnabled(false);
            		}else{
            			alertDialogMensaje("DATO INCORRECTO","VALOR " + texto + "\n ES INCORRECTO");
            			Parametro1.setEnabled(true);
            			Parametro1.setText("");
            			Parametro1.requestFocus();
            		}
	                return true;
                }
                return false;
            }
        });
           
	    //Se verfica el input azucarera al persionar enter
        Parametro2.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	// se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                	String texto = TextP2.getText().toString().trim().toUpperCase();
                	boolean valor = false;
                	if(TipoValP2 == 1){
                		valor = VerificarSiembra(Parametro2.getText().toString().trim());
                	} else if(TipoValP2 == 2) {
                		valor = VerificarMaterial(Parametro2.getText().toString().trim());
                	} else if(TipoValP2 == 3){
                		 valor = VerificarBolsa(Parametro2.getText().toString().trim());
                	} else if(TipoValP2 == 4){
                  		 valor = VerificarAzucarera(Parametro2.getText().toString().trim());
                	} else if(TipoValP2 == 5){
               		 	 valor = VerificarFiltro(Parametro2.getText().toString().trim());
                	}
                	if(valor == true && Nparametros > 2){
            			Parametro3.setEnabled(true);
            			Parametro3.requestFocus();
            			Parametro2.setEnabled(false);
            		}else if (valor == true && Nparametros < 3){
            			empleado.requestFocus();
            			empleado.setEnabled(true);
            			Parametro2.setEnabled(false);
            		}else{
            			alertDialogMensaje("DATO INCORRECTO","VALOR " + texto + "\n ES INCORRECTO");
            			Parametro2.setEnabled(true);
            			Parametro2.setText("");
            			Parametro2.requestFocus();
            		}
	                return true;
                }
                return false;
            }
        });
        
	    //Se verfica el input azucarera al persionar enter
        Parametro3.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	// se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                	String texto = TextP3.getText().toString().trim().toUpperCase();
                	boolean valor = false;
                	if(TipoValP3 == 1){
                		valor = VerificarSiembra(Parametro3.getText().toString().trim());
                	}else if (TipoValP3 == 2) {
                		valor = VerificarMaterial(Parametro3.getText().toString().trim());
                	}else if (TipoValP3 == 3) {
                		valor = VerificarBolsa(Parametro3.getText().toString().trim());
                	} else if(TipoValP3 == 4){
                  		 valor = VerificarAzucarera(Parametro3.getText().toString().trim());
                	} else if(TipoValP3 == 5){
                		valor = VerificarFiltro(Parametro3.getText().toString().trim());
                	}
                	if(valor == true ){
            			empleado.requestFocus();
            			empleado.setEnabled(true);
            			Parametro3.setEnabled(false);
            		}else{
            			alertDialogMensaje("DATO INCORRECTO","VALOR " + texto + "\n ES INCORRECTO");
            			Parametro3.setEnabled(true);
            			Parametro3.setText("");
            			Parametro3.requestFocus();
            		}
	                return true;
                }
                return false;
            }
        });
             
	    //Se verfica el input empleado al persionar enter
        empleado.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	// se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                	GuardarDatos();
                	return true; 
                }
            return false;
        } 
    });	 
                     
	    //se limpian los campos y variables para una nueva captura
	    buttonNuevo.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	        	 // validacionesGroup.setVisibility(View.INVISIBLE);
				 Material = "";
				 descriptivo.setText("");
				 Parametro1.setText("");  
				 Parametro2.setText("");
				 Parametro3.setText("");
				 Parametro1.setEnabled(false);
				 Parametro2.setEnabled(false);
				 Parametro3.setEnabled(false);
				 empleado.setText(""); 
				 empleado.setEnabled(false );
				 validoDescriptivo = false;
				// ValorPolinizacion = 0; 
				 descriptivo.setEnabled(true);
				descriptivo.requestFocus();
	        }
	    });
        
        btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
             	onBackPressed();
            }
        });
                
        btnInicio.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Intent intent = new Intent(getApplicationContext(), MAINACTIVITY.class);
           	 	startActivity(intent);
            }
        });
          
	}
	
	// Tipo Validacion = 1
	public boolean VerificarSiembra(String dato){
		boolean resultado = false;
		String Nsiembra = descriptivo.getText().toString().trim();
		if(dato.equals(Nsiembra)){
			resultado = true;
		} else {
			resultado = false;
		}
		return resultado;
	}
	
	
	// Tipo Validacion = 2
	public boolean VerificarMaterial(String dato){
		boolean resultado = false;
		if(dato.equals(Material)){
			resultado = true;
		}else{
			resultado = false;
		}
		return resultado;
	}
	
	
	// Tipo Validacion = 3
	public boolean VerificarBolsa(String dato){
		boolean resultado = false;
		String Nsiembra = descriptivo.getText().toString().trim();
		Cursor CursorBolsa = db.getDbolsas("Codigo='" + dato + "'");
		 if(CursorBolsa.getCount()>0){
			 Dbolsas s = db.getDbolsasFromCursor(CursorBolsa, 0);
			 String Siem = s.getSiembra().toString().trim();
			 if(Siem.equals(Nsiembra)){
				 resultado = true;
			 } else {
				 resultado = false;
			 }
		 } 
		 CursorBolsa.close();
		return resultado;
	}
	
	// Tipo Validacion = 4
	public boolean VerificarAzucarera(String dato){		
		boolean resultado = false;
		String Nsiembra = descriptivo.getText().toString().trim();
		 Cursor CursorAzucarera = db.getDazucareras("Codigo='" + dato + "'");
		 if(CursorAzucarera != null  && CursorAzucarera.getCount()>0){
			 Dazucareras az = db.getDazucarerasFromCursor(CursorAzucarera, 0);		 
			 String Siem = az.getSiembra();
			 if(Siem.equals(Nsiembra)){
			 resultado = true;
			 }
		 } 
		 CursorAzucarera.close();
	return resultado;
	}
	
	// Tipo Validacion = 5
	public boolean VerificarFiltro(String dato) {
		boolean resultado = false;
		String Nsiembra = descriptivo.getText().toString().trim();
		Cursor CursorFiltros = db.getDFiltros ("Id='" + dato + "'");
		if(CursorFiltros != null  && CursorFiltros.getCount()>0){
			Filtros f = db.getDFiltrosFromCursor(CursorFiltros, 0);
			 String SiembraF = f.getNoSiembra().toString().trim();
			 String FechaF = f.getFechaSuccion().toString().trim();
			 if(SiembraF.equals(Nsiembra) && FechaF.equals(Fecha)){
				 resultado = true;
			 }
		 }
		CursorFiltros.close();
		return resultado;
	}
			
	public void verificarAsginados(){
		Cursor CursorGuardados =	db.getDguardados("Actividad=" + CodigoOpcion + " AND ActividadFinalizada=0" );
		TotalEmpleadosAsignados = CursorGuardados.getCount();
		TCantidadAsignados.setText(TotalEmpleadosAsignados + "");	
		CursorGuardados.close();
	}
	
	//retornara un valor dependiendo del estado del empleado
	// 0 = no existe
	// 1 = existe pero esta asignado
	// 2 = existe y no esta asignado
	
	public int VerificarEmpleado(String empleado){
		int valor = 0;
		Cursor CursorDacceso =	db.getDacceso("CodigoEmpleado='" + empleado + "'");
		if(CursorDacceso != null  && CursorDacceso.getCount()>0){
			Cursor CursorGuardados =	db.getDguardados("CodigoEmpleado=" + empleado + " AND ActividadFinalizada=0");
			if(CursorGuardados.getCount()>0){
				valor = 1;
			}else{
				valor = 2;
			}
		}
		CursorDacceso.close();
		return valor;
	}
	
	
	public void alertDialogMensaje(String message1, String mesage2){
		
		//mVibrator.vibrate(300);
		
		try {
		    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
		    ringtone.play();
		    
		    
		} catch (Exception e) {
		    e.printStackTrace();
		}
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(message1);
		alertDialog.setMessage(mesage2);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int which) {
		// here you can add functions
			ringtone.stop();
		}
		});
		alertDialog.show();
	}
		
	public void VerificarDatos(String dato){	
		 Cursor CursorSemillas = db.getSemillas("Siembra='" + dato + "'");
		 if(CursorSemillas.getCount()>0){
			 Semillas s = db.getSemillasFromCursor(CursorSemillas, 0);
			 Log.d("SEMILLA",s.getSiembra() + ", "+s.getMaterial() + " ," + s.getPolinizacion());
		
			 /*
			 if(OpcionCosecha==1){
				 //si se entra a captura con la opcion de cosechas
					validacionesGroup.setVisibility(View.VISIBLE);
					azucar.setVisibility(View.VISIBLE);
					azucar.setText("");
					TextviewAzucarera.setText("Bolsa 1");
					bote.setVisibility(View.VISIBLE);
					bote.setText("");
					TextviewBote.setText("Bolsa 2");
					brocha.setVisibility(View.VISIBLE);
					brocha.setText("");
					TextviewBrocha.setText("Bolsa 3");
					azucar.requestFocus();
 					
				
				 
				 /*
				//agarege codigo para las bolsas de cosecha
				//Cursor CursorBolsas = db.getDbolsas("Codigo='" + dato + "'");
				Cursor CursorBolsas = db.getDbolsas("Codigo='" + azucar.getText().toString().trim() + "'");
				if (CursorBolsas.getCount()>0){
					Dbolsas bl = db.getDbolsasFromCursor(CursorBolsas,0);
					
					validoDescriptivo = true;
					 Material.setText(bl.getCodigo());
				}
				//Fin codigo para las bolsas de cosecha
				*/
				//CODIGO AGREGADO
				
		/*	 }else if(OpcionSuccion == 1){ 
				 if (checkbox1.isChecked() == true){ 
					 validacionesGroup.setVisibility(View.VISIBLE);
					 TextviewAzucarera.setText("");
					 azucar.setVisibility(View.INVISIBLE);
					 TextviewBote.setText("Pipeta");
					 bote.setEnabled(true);
					 bote.setVisibility(View.VISIBLE);
					 bote.requestFocus();
					 brocha.setVisibility(View.INVISIBLE);
					 TextviewBrocha.setText("");;
				 } else{
					 validacionesGroup.setVisibility(View.VISIBLE);
					 azucar.setEnabled(true);
					 azucar.setVisibility(View.VISIBLE);
					 TextviewAzucarera.setText("Filtro");
					 TextviewBote.setText("Pipeta");
					 bote.setEnabled(true);
					 bote.setVisibility(View.VISIBLE);
					 brocha.setVisibility(View.INVISIBLE);
					 TextviewBrocha.setText("");				 }
					
				 //FIN CODIGO AGREGADO
				
			 }else if(s.getPolinizacion() == 0 && OpcionPolinizacion==1) {
				 validacionesGroup.setVisibility(View.VISIBLE);
				 azucar.setVisibility(View.INVISIBLE);
				 //bote.setVisibility(View.INVISIBLE);
				 brocha.setVisibility(View.INVISIBLE);
				 TextviewAzucarera.setText("");
				 TextviewBrocha.setText("");
				 TextviewBote.setText("Bote");
				 ValorPolinizacion=0;
			 
			 }else if(s.getPolinizacion() == 1 && OpcionPolinizacion==1){
				 validacionesGroup.setVisibility(View.VISIBLE);				 
				 azucar.setVisibility(View.VISIBLE);
				 bote.setVisibility(View.VISIBLE);
				 brocha.setVisibility(View.VISIBLE);
				 TextviewAzucarera.setText("Azucarera");
				 TextviewBrocha.setText("Brocha");
				 TextviewBote.setText("Bote");
				 ValorPolinizacion=1;
			 }else if(s.getPolinizacion() == 2 && OpcionPolinizacion==1){
				 validacionesGroup.setVisibility(View.VISIBLE);				 
				 azucar.setVisibility(View.INVISIBLE);
				 //bote.setVisibility(View.INVISIBLE);
				 brocha.setVisibility(View.INVISIBLE);
				 TextviewAzucarera.setText("");
				 TextviewBrocha.setText("");
				 TextviewBote.setText("Pincel");
				 ValorPolinizacion=2;
			 } else if(s.getPolinizacion() == 3 && OpcionPolinizacion==1){
				 validacionesGroup.setVisibility(View.VISIBLE);				 
				 //azucar.setVisibility(View.INVISIBLE);
				 bote.setVisibility(View.INVISIBLE);
				 //brocha.setVisibility(View.INVISIBLE);
				 TextviewAzucarera.setText(" Macho ");
				 TextviewBrocha.setText(" Brocha ");
				 TextviewBote.setText("");
				 ValorPolinizacion=3;
			 }
			 
			 */
			  
			 validoDescriptivo = true;
			 Material = s.getMaterial().toString().trim();
			 
		 }else{
			 //validacionesGroup.setVisibility(View.INVISIBLE);
			 Material =  "";
			 ValorPolinizacion=0;
			 validoDescriptivo = false;
		 }
		 CursorSemillas.close();
	}
	
		

	
	/*
	public void validarDescriptivo(String dato){
		Cursor CursorSemillas = db.getSemillas("Siembra='" + dato + "'");
		 if(CursorSemillas.getCount()>0){
			 Semillas s = db.getSemillasFromCursor(CursorSemillas, 0);
			 Log.d("SEMILLA",s.getSiembra() + ", "+s.getMaterial() + " ," + s.getPolinizacion());
			 
			 validoDescriptivo = true;
			 Material.setText(s.getMaterial());
		 }
	}
	*/
	
	public void VerificarParametros() {
		int contador = 0;
		Cursor COpciones = db.getOpciones("Opcion = '" + CodigoOpcion + "'");
		if(COpciones.getCount()>0){
			Opciones op = db.getOpcionesFromCursor(COpciones, 0);
			if(!op.getPureza1().substring(0, 1).equals("0")){
				contador = 1;
				if(!op.getPureza2().substring(0, 1).equals("0")){
					contador = 2;
					if(!op.getPureza3().substring(0, 1).equals("0")){
						contador = 3;
					}
				}
			} 
			Nparametros = contador;			
			if(Nparametros == 3){
				TextP1.setVisibility(View.VISIBLE);
				Parametro1.setVisibility(View.VISIBLE);
				TextP2.setVisibility(View.VISIBLE);
				Parametro2.setVisibility(View.VISIBLE);
				TextP3.setVisibility(View.VISIBLE);
				Parametro3.setVisibility(View.VISIBLE);
				TextP1.setText(op.getPureza1().substring(1));
				TipoValP1 = Integer.parseInt(op.getPureza1().substring(0, 1));
				Parametro1.setText("");
				TextP2.setText(op.getPureza2().substring(1));
				TipoValP2 = Integer.parseInt(op.getPureza2().substring(0, 1));
				Parametro2.setText("");
				TextP3.setText(op.getPureza3().substring(1));
				TipoValP3 = Integer.parseInt(op.getPureza3().substring(0, 1));
				Parametro3.setText("");
			} else if (Nparametros == 2){
				TextP1.setVisibility(View.VISIBLE);
				Parametro1.setVisibility(View.VISIBLE);
				TextP2.setVisibility(View.VISIBLE);
				Parametro2.setVisibility(View.VISIBLE);
				TextP1.setText(op.getPureza1().substring(1));
				TipoValP1 = Integer.parseInt(op.getPureza1().substring(0, 1));
				Parametro1.setText("");
				TextP2.setText(op.getPureza2().substring(1));
				TipoValP2 = Integer.parseInt(op.getPureza2().substring(0, 1));
				Parametro2.setText("");
			} else if (Nparametros == 1){
				TextP1.setVisibility(View.VISIBLE);
				Parametro1.setVisibility(View.VISIBLE);
				TextP1.setText(op.getPureza1().substring(1));
				TipoValP1 = Integer.parseInt(op.getPureza1().substring(0, 1));
				Parametro1.setText("");
			} 
		} else {
			alertDialogMensaje("ERROR EN PARAMETROS"," ESTA OPCION NO TIENE \n PARAMETROS DE PUREZA DEFINIDOS ");
		}
		COpciones.close();
	}
	
	public void GuardarDatos(){
		String empleadoEscaneado = empleado.getText().toString().trim();
    	int existe = VerificarEmpleado( empleadoEscaneado );
        
    	if(existe == 2){
        		Dguardados dtemp = new Dguardados();
        		dtemp.setCodigoEmpleado(Integer.parseInt(empleadoEscaneado));
        		dtemp.setCodigoDepto(Codigodepartamento);
        		dtemp.setSiembra(descriptivo.getText().toString().trim());
        		
        		//-----------------------------------------------------------------------------------
        		String correlativo="";
        		    	//almaceno el empleado en una variable string
        		    	String getEmpleado = empleado.getText().toString().trim();
        		    	// guardo la variable que tiene empleado en un array
        		    	String[]empl = new String[]{getEmpleado};
        		    	Cursor obtenerCorrelativo = db.getCorrelativo(empl);
        		    	//recorro dicho arreglo para obtener el campo
        		    	if (obtenerCorrelativo.moveToFirst()){
        		    		do{
        		    			correlativo = obtenerCorrelativo.getString(0);
        		    		}while(obtenerCorrelativo.moveToNext());
        		    	}
        		//-------------------------------------------------------------------------
        		
        		Calendar c = Calendar.getInstance(); 
        		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
        		String fecha = fdate.format(c.getTime());
        		SimpleDateFormat ftime = new SimpleDateFormat("HH:mm:ss");
        		String horaInicio = ftime.format(c.getTime());
        		dtemp.setFecha(fecha);
        		dtemp.setHoraIncio(horaInicio);
        		dtemp.setActividad(CodigoOpcion);
        		dtemp.setSupervisor(CodigoEmpleado);
        		dtemp.setCorrelativoBolsa(Parametro1.getText().toString().trim());
        		dtemp.setCorrerlativoBolsa2(Parametro2.getText().toString().trim());
        		dtemp.setCorrerlativoBolsa3(Parametro3.getText().toString().trim());
        		dtemp.setActividadFinalizada(0);
        		dtemp.setCorrelativoEmpleado(correlativo);
        		        		        		        	
        		Log.d("Dguardados", dtemp.toString().trim());
        		     		
        		db.insertarDguardados(dtemp);
        		
        		//generacion archivo json
        		ObjectMapper mapper = new ObjectMapper();
        		try {
        			String nameJson ="G_" + dtemp.getCodigoEmpleado() + "_"+dtemp.getFecha() + "_"+dtemp.getHoraIncio();
        			nameJson=nameJson.replace(":", "-");	
					mapper.writeValue(new File( Environment.getExternalStorageDirectory()+"/SIM/JsonDescarga/" + nameJson+ ".json"), dtemp);
					
					JsonFiles archivo = new JsonFiles();
					archivo.setName(nameJson+ ".json");
					archivo.setNameFolder("SinFinalizar/");
					archivo.setUpload(0);
					db.insertarJsonFile(archivo);
					
					if(!appState.getSubiendoArchivos()){
						appState.setSubiendoArchivos(true);
						new UptoDropbox().execute(getApplicationContext());
					}
					
        		
        		} catch (JsonGenerationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JsonMappingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        		
        		
        		empleado.setText("");  
        		Parametro1.setText("");
        		Parametro2.setText("");
        		Parametro3.setText("");
        		Parametro1.setEnabled(false);		
        		Parametro2.setEnabled(false);
        		Parametro3.setEnabled(false); 
        		empleado.setEnabled(false);
        		VerificarDatos(descriptivo.getText().toString().trim());
            	if (validoDescriptivo == true){
            		descriptivo.setEnabled(false);
            		if(Nparametros > 0){
            			Parametro1.setEnabled(true);
            			Parametro1.requestFocus();
            		} else {
            			empleado.requestFocus();
            			empleado.setEnabled(true);
            		}
            	}
        		
        		TotalEmpleadosAsignados++;
        		TCantidadAsignados.setText(TotalEmpleadosAsignados + "");
        		
        		
        	}else if(existe == 1){
        		alertDialogMensaje("Asignado", "Este usuario ya ha sido asignado");
        		empleado.setText("");
        		empleado.requestFocus();
        		//VerificarDatos(descriptivo.getText().toString().trim());
        	}else if(existe == 0){
        		alertDialogMensaje("Marcaje", "No existe marcaje para este usuario");
        		empleado.setText("");
        		empleado.requestFocus();
        		 //VerificarDatos(descriptivo.getText().toString().trim());	
    	}
	}
	/*
	public void VerificarDatosbolsa(String dato){	
		
		 Cursor CursorSemillas = db.getDbolsas("Codigo='" + dato + "'");
		 if(CursorSemillas.getCount()>0){
			 Dbolsas s = db.getDbolsasFromCursor(CursorSemillas, 0);
			 Log.d("SEMILLA",s.getSiembra() + ", "+s.getMaterial() + " ," + s.getCodigo());
			 
			 if(OpcionCosecha==1){
				 //si se entra a captura con la opcion de cosechas
				validacionesGroup.setVisibility(View.VISIBLE);		
				bote.setVisibility(View.VISIBLE);
				brocha.setVisibility(View.VISIBLE);
				TextviewAzucarera.setText("Bolsa 1");
				TextviewBote.setText("Bolsa 2");
				TextviewBrocha.setText("Bolsa 3");
				//CODIGO AGREGADO
			 }else if(OpcionSuccion == 1){
				 validacionesGroup.setVisibility(View.VISIBLE);
				 TextviewAzucarera.setText("Filtro");
				 TextviewBote.setText("Pipeta");;
				 brocha.setVisibility(View.INVISIBLE);
				 TextviewBrocha.setText("");;
				 //FIN CODIGO AGREGADO
						
			 }else{
				 validacionesGroup.setVisibility(View.INVISIBLE);
				 ValorPolinizacion=0;
			 }
			 //validoDescriptivo = true;
			 Material1.setText(s.getSiembra());		 		 
		 }else{
			 validacionesGroup.setVisibility(View.VISIBLE);
			 ValorPolinizacion=0;
			 validoDescriptivo = false;
			// alertDialogMensaje("Mensaje", "EL valor es invalido");
			 
		 }
	}
	
	*/
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.captura_general, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
	/*
	
	//Variables globales
		private SQLiteDatabase database;
		private DBHelper dbHelper;
		private MyApp appState;	
		private DBAdapter db;
		int CodigoModulo;
		int Codigodepartamento;
		int CodigoEmpleado;
		int CodigoOpcion;
		long Pos;
		String Act;
		String Ccosto;
		String NombreOpcion;
		GridView gridview;
		GridView gridEnc;
		TableLayout tablatest;
		Vibrator mVibrator;
		Ringtone ringtone;
		String Fechade;
		private String val;
		public static int TIPO_USUARIO = 1;
		static final String TABLE_USUARIO = "usuario";
		private static final String DATABASE_NAME = "floricultural.db";
		
		
		public synchronized void open() throws SQLException {
			try {
				database = dbHelper.getWritableDatabase();
			} catch (SQLiteException ex) {
				try {
					Log.e("error", "DB abierta RO " + ex.getMessage());
					database = dbHelper.getReadableDatabase();				
				} catch (SQLiteException ex1) {
					Log.e("error", "Imposible abrir la base de datos " + ex1.getMessage());
				}
			}			
	}
			
		
		@SuppressLint("NewApi") @Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.activity_reportest);
			
			// para manejar la excepcion de permisos en el API de Android
	    	StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	        StrictMode.setThreadPolicy(policy);

			//se obtienen lo datos de la actividad antetior por medio del intent
			Bundle extras = getIntent().getExtras();
			CodigoModulo = extras.getInt("CodigoModulo");
		    CodigoEmpleado = extras.getInt("CodigoEmpleado");
		    Codigodepartamento = extras.getInt("Codigodepartamento");
		    CodigoOpcion = extras.getInt("CodigoOpcion");
		    NombreOpcion = extras.getString("NombreOpcion");
		    Act = extras.getString("Act");
		    Ccosto = extras.getString("Ccosto");
		    Fechade = extras.getString("Fechade");
		    
		    //hacemos referencia a todos los objetos de nuestra vista
		    TextView TNombreCaptura = (TextView)findViewById(R.id.OpcionCaptura);
		    TextView TCodigoEmpleado= (TextView)findViewById(R.id.CodigoEmpleado);
		    TextView TCodigoOpcion= (TextView)findViewById(R.id.codigoOpcion);
	        Button btnBack = (Button) findViewById(R.id.btnBack);
	        Button btnOK = (Button) findViewById(R.id.btnAsistencia);
	        Button btnfechade = (Button) findViewById(R.id.buttonfechade);
	        final EditText editFechade = (EditText) findViewById(R.id.editFechaDe);
	        editFechade.setEnabled(false);
	        tablatest = (TableLayout) findViewById(R.id.TablaTest);
	        gridEnc = (GridView) findViewById(R.id.gridEnc);
	        gridview = (GridView) findViewById(R.id.gridDetalle);
	        
	        final ArrayList<String> datos = new ArrayList<String>();
	        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, datos);
	        final ArrayList<String> encabezado = new ArrayList<String>();
	        final ArrayAdapter<String> adapEnc = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, encabezado);
	        
		    //hacemos las inicializaciones necesarias a nuestra vista
		    TNombreCaptura.setText(NombreOpcion+ "");
		    TCodigoEmpleado.setText(CodigoEmpleado + "");
		    TCodigoOpcion.setText(CodigoOpcion + "");
		    
		    //incializamos la BD asi como tambien obtenemos la referencia al singleton
		    appState = ((MyApp)getApplicationContext());   
		    db = appState.getDb();
		    db.open();
		    
		
		    final String Pb = "";
        	final String es = " | ";
        	final String Fecha = editFechade.getText().toString().trim();
        	tablatest.removeAllViews();
        	
        	
        	
    	
        	
        	
        	btnBack.setOnClickListener(new View.OnClickListener() {
	            public void onClick(View v) {
	 
	            	onBackPressed();
	            }
	        });
        	
        	 btnOK.setOnClickListener(new View.OnClickListener() {
 	            public void onClick(View v) {
        	try {
            	String user = "sim";
            	String pasword = "sim";
            	 Connection connection = null;
            	 ResultSet rs = null;
            	 Statement statement = null;
            	 
    				Class.forName("com.ibm.as400.access.AS400JDBCDriver");
    				connection = DriverManager.getConnection("jdbc:as400://190.190.190.10", user, pasword);	
    				statement = connection.createStatement();			     
    				rs = statement.executeQuery("select u.clave, ifnull(r.codigodepto,0) CodigoDepto, ifnull(u.codigoempleado,0) CodigoEmpleado, u.tipo, u.usuario from siscfgf.usuario u " +
    						"left join sisrrhhf.mempleado r on u.codigoempleado = r.correlativo");
    				
    				while (rs.next()) {
    					ContentValues newUsuario = new ContentValues();
    					newUsuario.put("Usuario", rs.getString(1));
    					newUsuario.put("Clave", rs.getString(2));
    					newUsuario.put("Tipo", rs.getString(3));
    					newUsuario.put("CodigoEmpleado", rs.getString(4));		
    					newUsuario.put("CodigoDepto", rs.getString(5));	
    					try {
    						database.insert(TABLE_USUARIO, null, newUsuario);
    					} catch (Exception e){
    						Log.e("InsertErrorDBApder", e.toString().trim());
    					}
    				connection.close();	
        	}
        	}
			catch (Exception e) {
				Toast toast = Toast.makeText(getApplicationContext(), "Verificar Conexi�n.....", Toast.LENGTH_SHORT);
            	toast.show();
				e.printStackTrace();
        } 
        	}
        	 });
        	 
        	 
        	/*if(Fecha.equals(Pb)){
        		alertDialogMensaje("ERROR","PORFAVOR SELECCIONE UNA FECHA");
        		editFechade.setText("");
        		editFechade.requestFocus();
        	}else{*/
        	
        	//2563
        /*	
        	try {
        	String user = "sim";
        	String pasword = "sim";
        	 Connection connection = null;
        	 ResultSet rs = null;
        	 Statement statement = null;
        	 
				Class.forName("com.ibm.as400.access.AS400JDBCDriver");
				connection = DriverManager.getConnection("jdbc:as400://190.190.190.10", user, pasword);	
				statement = connection.createStatement();			     
				rs = statement.executeQuery("select FechaSuccion, Siembra, Filtros, ifnull(Estado,'Solicitud') " +
						"from sisappf.app_solicitudFiltros where Fecha = '2016-09-14' " +
						"and CorrelativoContador = '350'");
				
				//Agregar encabezados de columnas	
				 TableRow Erow1 = new TableRow(Reportest.this);
				 TextView E1 = new TextView(Reportest.this);
				 TextView E2 = new TextView(Reportest.this);
				 TextView E3 = new TextView(Reportest.this);
				 TextView E4 = new TextView(Reportest.this);
				 TextView E5 = new TextView(Reportest.this);
				 TextView E6 = new TextView(Reportest.this);
				 TextView E7 = new TextView(Reportest.this);
				 TextView E8 = new TextView(Reportest.this); 
				 TextView E9 = new TextView(Reportest.this); 
				 TextView E10 = new TextView(Reportest.this); 
				 TextView E11 = new TextView(Reportest.this); 
				 TextView E12 = new TextView(Reportest.this); 
				 TextView E13 = new TextView(Reportest.this);
				 TextView E14 = new TextView(Reportest.this);
				 TextView E15 = new TextView(Reportest.this);
				 TextView E16 = new TextView(Reportest.this);
				 TextView E17 = new TextView(Reportest.this);
				 TextView E18 = new TextView(Reportest.this);
				 TextView E19 = new TextView(Reportest.this);
				 TextView E20 = new TextView(Reportest.this);
				 TextView E21 = new TextView(Reportest.this);
				 TextView E22 = new TextView(Reportest.this);
				 E1.setTextSize(10);
				 E1.setTextColor(Color.BLACK);
				 E2.setTextSize(10);
				 E2.setTextColor(Color.BLACK);
				 E3.setTextSize(10);
				 E3.setTextColor(Color.BLACK);
				 E4.setTextSize(10);
				 E4.setTextColor(Color.BLACK);
				 E5.setTextSize(10);
				 E5.setTextColor(Color.BLACK);
				 E6.setTextSize(10);
				 E6.setTextColor(Color.BLACK);
				 E7.setTextSize(10);
				 E7.setTextColor(Color.BLACK);
				 E8.setTextSize(20);
				 E8.setTextColor(Color.BLACK);
				 E9.setTextSize(20);
				 E9.setTextColor(Color.BLACK);
				 E10.setTextSize(20);
				 E10.setTextColor(Color.BLACK);
				 E11.setTextSize(20);
				 E11.setTextColor(Color.BLACK);
				 E12.setTextSize(20);
				 E12.setTextColor(Color.BLACK);
				 E13.setTextSize(20);
				 E13.setTextColor(Color.BLACK);
				 E14.setTextSize(10);
				 E14.setTextColor(Color.BLACK);
				 E15.setTextSize(10);
				 E15.setTextColor(Color.BLACK);
				 E16.setTextSize(10);
				 E16.setTextColor(Color.BLACK);
				 E17.setTextSize(10);
				 E17.setTextColor(Color.BLACK);
				 E18.setTextSize(20);
				 E18.setTextColor(Color.BLACK);
				 E19.setTextSize(20);
				 E19.setTextColor(Color.BLACK);
				 E20.setTextSize(20);
				 E20.setTextColor(Color.BLACK);
				 E21.setTextSize(20);
				 E21.setTextColor(Color.BLACK);
				 String Cn1 = rs.getMetaData().getColumnName(1);
				 String Cn2 = rs.getMetaData().getColumnName(2);
				 String Cn3 = rs.getMetaData().getColumnName(3);
				 String Cn4 = rs.getMetaData().getColumnName(4);
				 String Cn5 = rs.getMetaData().getColumnName(5);
				 String Cn6 = rs.getMetaData().getColumnName(6);
				 String Cn7 = rs.getMetaData().getColumnName(7);
				 String Cn8 = rs.getMetaData().getColumnName(8);
				 String Cn9 = rs.getMetaData().getColumnName(9);
				 String Cn10 = rs.getMetaData().getColumnName(10);
				 String Cn11 = rs.getMetaData().getColumnName(11);
				 E1.setText(Cn1);
				 E2.setText(Cn2);
				 E3.setText(Cn3);
				 E4.setText(Cn4);
				 E5.setText(Cn5);
				 E6.setText(Cn6);
				 E7.setText(Cn7);
				 E8.setText(es);
				 E9.setText(es);
				 E10.setText(es);
				 E11.setText(es);
				 E12.setText(es);
				 E13.setText(es);
				 E14.setText(Cn8);
				 E15.setText(Cn9);
				 E16.setText(Cn10);
				 E17.setText(Cn11);
				 E18.setText(es);
				 E19.setText(es);
				 E20.setText(es);
				 E21.setText(es);
				 Erow1.addView(E1);
				 Erow1.addView(E8);
				 Erow1.addView(E2);
				 Erow1.addView(E9);
				 Erow1.addView(E3);
				 Erow1.addView(E10);
				 Erow1.addView(E4);
				 Erow1.addView(E11);
				 Erow1.addView(E5);
				 Erow1.addView(E12);
				 Erow1.addView(E6);
				 Erow1.addView(E13);
				 Erow1.addView(E7);
				 Erow1.addView(E18);
				 Erow1.addView(E14);
				 Erow1.addView(E19);
				 Erow1.addView(E15);
				 Erow1.addView(E20);
				 Erow1.addView(E16);
				 Erow1.addView(E21);
				 Erow1.addView(E17);
				 E1.setGravity(Gravity.CENTER_HORIZONTAL);
				 E2.setGravity(Gravity.CENTER_HORIZONTAL);
				 E3.setGravity(Gravity.CENTER_HORIZONTAL);
				 E4.setGravity(Gravity.CENTER_HORIZONTAL);
				 E5.setGravity(Gravity.CENTER_HORIZONTAL);
				 E6.setGravity(Gravity.CENTER_HORIZONTAL);
				 E7.setGravity(Gravity.CENTER_HORIZONTAL);
				 E8.setGravity(Gravity.CENTER_HORIZONTAL);
				 E9.setGravity(Gravity.CENTER_HORIZONTAL);
				 E10.setGravity(Gravity.CENTER_HORIZONTAL);
				 E11.setGravity(Gravity.CENTER_HORIZONTAL);
				 E12.setGravity(Gravity.CENTER_HORIZONTAL);
				 E13.setGravity(Gravity.CENTER_HORIZONTAL);
				 E14.setGravity(Gravity.CENTER_HORIZONTAL);
				 E15.setGravity(Gravity.CENTER_HORIZONTAL);
				 E16.setGravity(Gravity.CENTER_HORIZONTAL);
				 E17.setGravity(Gravity.CENTER_HORIZONTAL);
				 E18.setGravity(Gravity.CENTER_HORIZONTAL);
				 E19.setGravity(Gravity.CENTER_HORIZONTAL);
				 E20.setGravity(Gravity.CENTER_HORIZONTAL);
				 E21.setGravity(Gravity.CENTER_HORIZONTAL);
				 Erow1.setGravity(Gravity.CENTER);
				 Erow1.setBackgroundColor(Color.GREEN);
				 tablatest.addView(Erow1);	
				// finaliza agregar encabezados de columnas 
				int sz = rs.getFetchSize();
				int cont = 0;
				while (rs.next()) {
					 String d1 = rs.getString(1).trim();
					 String d2 = rs.getString(2).trim();
					 String d3 = rs.getString(3).trim();
					 String d4 = rs.getString(4).trim();
				     String d5 = rs.getString(5).trim();
					 String d6 = rs.getString(6).trim();
					 String d7 = rs.getString(7).trim();
					 String d8 = rs.getString(8).trim();
					 String d9 = rs.getString(9).trim();
					 String d10 = rs.getString(10).trim();
					 String d11 = rs.getString(11).trim();
					 String c1 = comprobar(d1);
					 String c2 = comprobar(d2);
					 String c3 = comprobar(d3);
					 String c4 = comprobar(d4);
					 String c5 = comprobar(d5);
					 String c6 = comprobar(d6);
					 String c7 = comprobar(d7);
					 String c8 = comprobar(d8);
					 String c9 = comprobar(d9);
					 String c10 = comprobar(d10);
					 String c11 = comprobar(d11);
					 for (int i = 0; i == sz; i++)
					 {	
						 cont = cont +1;
						 TableRow row1 = new TableRow(Reportest.this);
						 TextView t1 = new TextView(Reportest.this);
						 TextView t2 = new TextView(Reportest.this);
						 TextView t3 = new TextView(Reportest.this);
						 TextView t4 = new TextView(Reportest.this);
						 TextView t5 = new TextView(Reportest.this);
						 TextView t6 = new TextView(Reportest.this);
						 TextView t7 = new TextView(Reportest.this);
						 TextView t8 = new TextView(Reportest.this); 
						 TextView t9 = new TextView(Reportest.this); 
						 TextView t10 = new TextView(Reportest.this); 
						 TextView t11 = new TextView(Reportest.this); 
						 TextView t12 = new TextView(Reportest.this); 
						 TextView t13 = new TextView(Reportest.this);
						 TextView t14 = new TextView(Reportest.this);
						 TextView t15 = new TextView(Reportest.this);
						 TextView t16 = new TextView(Reportest.this);
						 TextView t17 = new TextView(Reportest.this);
						 TextView t18 = new TextView(Reportest.this);
						 TextView t19 = new TextView(Reportest.this);
						 TextView t20 = new TextView(Reportest.this);
						 TextView t21 = new TextView(Reportest.this);
						 t1.setClickable(true);
						 t2.setClickable(true);
						 t3.setClickable(true);
						 t4.setClickable(true);
						 t5.setClickable(true);
						 t6.setClickable(true);
						 t7.setClickable(true);
						 t8.setClickable(true);
						 t9.setClickable(true);
						 t10.setClickable(true);
						 t11.setClickable(true);
						 t12.setClickable(true);
						 t13.setClickable(true);
						 t14.setClickable(true);
						 t15.setClickable(true);
						 t16.setClickable(true);
						 t17.setClickable(true);
						 t18.setClickable(true);
						 t19.setClickable(true);
						 t20.setClickable(true);
						 t21.setClickable(true);
						 t1.setTextSize(10);
						 t1.setTextColor(Color.BLACK);
						 t2.setTextSize(10);
						 t2.setTextColor(Color.BLACK);
						 t3.setTextSize(10);
						 t3.setTextColor(Color.BLACK);
						 t4.setTextSize(10);
						 t4.setTextColor(Color.BLACK);
						 t5.setTextSize(10);
						 t5.setTextColor(Color.BLACK);
						 t6.setTextSize(10);
						 t6.setTextColor(Color.BLACK);
						 t7.setTextSize(10);
						 t7.setTextColor(Color.BLACK);
						 t8.setTextSize(10);
						 t8.setTextColor(Color.BLACK);
						 t9.setTextSize(10);
						 t9.setTextColor(Color.BLACK);
						 t10.setTextSize(10);
						 t10.setTextColor(Color.BLACK);
						 t11.setTextSize(10);
						 t11.setTextColor(Color.BLACK);
						 t12.setTextSize(20);
						 t12.setTextColor(Color.BLACK);
						 t13.setTextSize(20);
						 t13.setTextColor(Color.BLACK);
						 t14.setTextSize(20);
						 t14.setTextColor(Color.BLACK);
						 t15.setTextSize(20);
						 t15.setTextColor(Color.BLACK);
						 t16.setTextSize(20);
						 t16.setTextColor(Color.BLACK);
						 t17.setTextSize(20);
						 t17.setTextColor(Color.BLACK);
						 t18.setTextSize(20);
						 t18.setTextColor(Color.BLACK);
						 t19.setTextSize(20);
						 t19.setTextColor(Color.BLACK);
						 t20.setTextSize(20);
						 t20.setTextColor(Color.BLACK);
						 t21.setTextSize(20);
						 t21.setTextColor(Color.BLACK);
						 t1.setGravity(Gravity.CENTER_HORIZONTAL);
						 t2.setGravity(Gravity.CENTER_HORIZONTAL);
						 //t3.setWidth(350);
						 t4.setGravity(Gravity.CENTER_HORIZONTAL);
						 t5.setGravity(Gravity.CENTER_HORIZONTAL);
						 t6.setGravity(Gravity.CENTER_HORIZONTAL);
						 t6.setWidth(100);
						 t7.setGravity(Gravity.CENTER_HORIZONTAL);
						 t8.setGravity(Gravity.CENTER_HORIZONTAL);
						 t9.setGravity(Gravity.CENTER_HORIZONTAL);
						 t10.setGravity(Gravity.CENTER_HORIZONTAL);
						 t11.setGravity(Gravity.CENTER_HORIZONTAL);
						 t12.setGravity(Gravity.CENTER_HORIZONTAL);
						 t13.setGravity(Gravity.CENTER_HORIZONTAL);
						 t14.setGravity(Gravity.CENTER_HORIZONTAL);
						 t15.setGravity(Gravity.CENTER_HORIZONTAL);
						 t16.setGravity(Gravity.CENTER_HORIZONTAL);
						 t17.setGravity(Gravity.CENTER_HORIZONTAL);
						 t18.setGravity(Gravity.CENTER_HORIZONTAL);
						 t19.setGravity(Gravity.CENTER_HORIZONTAL);
						 t20.setGravity(Gravity.CENTER_HORIZONTAL);
						 t21.setGravity(Gravity.CENTER_HORIZONTAL);
						 row1.setGravity(Gravity.CENTER);
						 row1.setClickable(true);
						 if(cont%2 == 0){
						 row1.setBackgroundColor(Color.LTGRAY);
						 }else {
						 row1.setBackgroundColor(Color.WHITE);	 
						 }
					 t1.setText(c1);
					 t2.setText(c2);
					 t3.setText(c3);
					 t4.setText(c4);
					 t5.setText(c5);
					 t6.setText(c6);
					 t7.setText(c7);
					 t8.setText(c8);
					 t9.setText(c9);
					 t10.setText(c10);
					 t11.setText(c11);
					 t12.setText(es);
					 t13.setText(es);
					 t14.setText(es);
					 t15.setText(es);
					 t16.setText(es);
					 t17.setText(es);
					 t18.setText(es);
					 t19.setText(es);
					 t20.setText(es);
					 t21.setText(es);
					 row1.addView(t1);
					 row1.addView(t12);
					 row1.addView(t2);
					 row1.addView(t13);
					 row1.addView(t3);
					 row1.addView(t14);
					 row1.addView(t4);
					 row1.addView(t15);
					 row1.addView(t5);
					 row1.addView(t16);
					 row1.addView(t6);
					 row1.addView(t17);
					 row1.addView(t7);
					 row1.addView(t18);
					 row1.addView(t8);
					 row1.addView(t19);
					 row1.addView(t9);
					 row1.addView(t20);
					 row1.addView(t10);
					 row1.addView(t21);
					 row1.addView(t11);
					 tablatest.addView(row1);
					 tablatest.setClickable(true);
					 }
					 
				}
				connection.close();	
        	}
			catch (Exception e) {
				Toast toast = Toast.makeText(getApplicationContext(), "Verificar Conexi�n.....", Toast.LENGTH_SHORT);
            	toast.show();
				e.printStackTrace();
        } 
		    
        */
        	
        	/*
		    try {
            	String user = "sim";
            	String pasword = "sim";
            	 Connection connection = null;
            	 ResultSet rs = null;
            	 Statement statement = null;
            	 
					Class.forName("com.ibm.as400.access.AS400JDBCDriver");
					connection = DriverManager.getConnection("jdbc:as400://190.190.190.10", user, pasword);	
					statement = connection.createStatement();			     
					rs = statement.executeQuery("SELECT G.CORRELATIVO, G.ACTIVIDAD,SOM.DESCRIPCION,G.SIEMBRA,G.CODIGOEMPLEADO CODIGO, E.NOMBRE|| ' '|| E.APELLIDO NOMBRE,G.HORAINICIO H_INICIO,G.HORAFIN H_FIN,SIE.PLANTAS,G.PLANTAS P_TRABAJADAS,SIE.INVERNADERO INV " +
							"FROM SISAPPF.APP_TMP_GENERAL g LEFT JOIN SISCFGF.SEGURIDAD_OPCION_MODULOASP SOM ON G.ACTIVIDAD = SOM.OPCION " +
							"LEFT JOIN SISSEMF.MOV_DEFSIEMBRAE SIE  ON G.SIEMBRA = SIE.NOSIEMBRA LEFT JOIN SISRRHHF.MEMPLEADO E ON '0' || G.CODIGOEMPLEADO = E.EMPRESA || E.CODIGOEMPLEADO " +
							"where G.FECHA = '"+Fechade+"' AND  G.actividadfinalizada = '1' AND SOM.CONTROL_PLANTAS = 'SI' AND G.SIEMBRA = '"+Ccosto+"' AND G.ACTIVIDAD = '"+Act+"' " +
							"ORDER BY G.ACTIVIDAD ");
	
				    //Agregar encabezados de columnas	
					String Cn1 = rs.getMetaData().getColumnName(1);
					encabezado.add(Cn1);
					gridEnc.setAdapter(adapEnc);
					String Cn2 = rs.getMetaData().getColumnName(2);
					encabezado.add(Cn2);
					gridEnc.setAdapter(adapEnc);
					String Cn3 = rs.getMetaData().getColumnName(3);
					encabezado.add(Cn3);
					gridEnc.setAdapter(adapEnc);
					String Cn4 = rs.getMetaData().getColumnName(4);
					encabezado.add(Cn4);
					gridEnc.setAdapter(adapEnc);
					String Cn5 = rs.getMetaData().getColumnName(5);
					encabezado.add(Cn5);
					gridEnc.setAdapter(adapEnc);
					String Cn6 = rs.getMetaData().getColumnName(6);
					encabezado.add(Cn6);
					gridEnc.setAdapter(adapEnc);
					String Cn7 = rs.getMetaData().getColumnName(7);
					encabezado.add(Cn7);
					gridEnc.setAdapter(adapEnc);
					String Cn8 = rs.getMetaData().getColumnName(8);
					encabezado.add(Cn8);
					gridEnc.setAdapter(adapEnc);
					String Cn9 = rs.getMetaData().getColumnName(9);
					encabezado.add(Cn9);
					gridEnc.setAdapter(adapEnc);
					String Cn10 = rs.getMetaData().getColumnName(10);
					encabezado.add(Cn10);
					gridEnc.setAdapter(adapEnc);
					String Cn11 = rs.getMetaData().getColumnName(11);
					encabezado.add(Cn11);
					gridEnc.setAdapter(adapEnc);
					
					// finaliza agregar encabezados de columnas	
					while (rs.next()) {
					//agrega filas de columnas
						 String d1 = rs.getString(1).trim();
						 String d2 = rs.getString(2).trim();
						 String d3 = rs.getString(3).trim();
						 String d4 = rs.getString(4).trim();
					     String d5 = rs.getString(5).trim();
						 String d6 = rs.getString(6).trim();
						 String d7 = rs.getString(7).trim();
						 String d8 = rs.getString(8).trim();
						 String d9 = rs.getString(9).trim();
						 String d10 = rs.getString(10).trim();
						 String d11 = rs.getString(11).trim();
						 String c1 = comprobar(d1);
						 String c2 = comprobar(d2);
						 String c3 = comprobar(d3);
						 String c4 = comprobar(d4);
						 String c5 = comprobar(d5);
						 String c6 = comprobar(d6);
						 String c7 = comprobar(d7);
						 String c8 = comprobar(d8);
						 String c9 = comprobar(d9);
						 String c10 = comprobar(d10);
						 String c11 = comprobar(d11);
						 datos.add(c1);
						 gridview.setAdapter(adapter);
						 datos.add(c2);
						 gridview.setAdapter(adapter);
						 datos.add(c3);
						 gridview.setAdapter(adapter);
						 datos.add(c4);
						 gridview.setAdapter(adapter);
						 datos.add(c5);
						 gridview.setAdapter(adapter);
						 datos.add(c6);
						 gridview.setAdapter(adapter);
						 datos.add(c7);
						 gridview.setAdapter(adapter);
						 datos.add(c8);
						 gridview.setAdapter(adapter);
						 datos.add(c9);
						 gridview.setAdapter(adapter);
						 datos.add(c10);
						 gridview.setAdapter(adapter);
						 datos.add(c11);
						 gridview.setAdapter(adapter);
						 //.setTextSize(12);
					}
					connection.close();	
            	}
				catch (Exception e) {
					Toast toast = Toast.makeText(getApplicationContext(), "Verificar Conexi�n.....", Toast.LENGTH_SHORT);
	            	toast.show();
					e.printStackTrace();
            } 
		   
		    
		    
		    btnfechade.setOnClickListener(new View.OnClickListener() {
	            public void onClick(View v) {
	            	Calendar mcurrentDate=Calendar.getInstance();
	                int mYear = mcurrentDate.get(Calendar.YEAR);
	                int mMonth = mcurrentDate.get(Calendar.MONTH);
	                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

	                DatePickerDialog mDatePicker=new DatePickerDialog(Reportest.this, new OnDateSetListener() {                  
	                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
	                    	int datodia = selectedday;
	                    	String dia = format(datodia);
	                    	int datomes = selectedmonth + 1;
	                    	String mes = format(datomes);
	                    	String datoanio = Integer.toString(selectedyear);
	                    	String dato = datoanio + "-" + mes + "-" +dia;
	                    	editFechade.setText(dato);
	                    }
	                }
	                ,mYear, mMonth, mDay);
	                mDatePicker.setTitle("Seleccionar Fecha");                
	                mDatePicker.show();  
	            }
	            
	        });
	    
		    btnOK.setOnClickListener(new View.OnClickListener() {
	            public void onClick(View v) {
	            	datos.clear();
	            	adapter.clear();
	            	encabezado.clear();
	            	adapEnc.clear();
	            	tablatest.removeAllViews();
	            	Fechade = editFechade.getText().toString().trim();
	            	String Pb = "";
	            	
	            	if(Fechade.equals(Pb)){
	            		alertDialogMensaje("ERROR","PORFAVOR SELECCIONE UNA FECHA");
	            	}
	            	else 
	            	{
	            	try {
	            	
	            	String user = "sim";
	            	String pasword = "sim";
	            	 Connection connection = null;
	            	 ResultSet rs = null;
	            	 Statement statement = null;
	            	 
						Class.forName("com.ibm.as400.access.AS400JDBCDriver");
						connection = DriverManager.getConnection("jdbc:as400://190.190.190.10", user, pasword);	
						statement = connection.createStatement();			     
						rs = statement.executeQuery("select FechaSuccion, Siembra, Filtros, ifnull(Estado,'Solicitud') Estado " +
								"from sisappf.app_solicitudFiltros " +
								"where FechaSuccion = '"+Fechade+"' and CorrelativoContador = '"+CodigoEmpleado+"'");
		
					    //Agregar encabezados de columnas	
						 TableRow Erow1 = new TableRow(Reportest.this);
						 TextView E1 = new TextView(Reportest.this);
						 TextView E2 = new TextView(Reportest.this);
						 TextView E3 = new TextView(Reportest.this);
						 TextView E4 = new TextView(Reportest.this);
						 TextView E5 = new TextView(Reportest.this);
						 TextView E6 = new TextView(Reportest.this);
						 TextView E7 = new TextView(Reportest.this);
						 TextView E8 = new TextView(Reportest.this); 
						 E1.setTextSize(20);
						 E1.setTextColor(Color.BLACK);
						 E2.setTextSize(25);
						 E2.setTextColor(Color.BLACK);
						 E3.setTextSize(20);
						 E3.setTextColor(Color.BLACK);
						 E4.setTextSize(25);
						 E4.setTextColor(Color.BLACK);
						 E5.setTextSize(20);
						 E5.setTextColor(Color.BLACK);
						 E6.setTextSize(25);
						 E6.setTextColor(Color.BLACK);
						 E7.setTextSize(20);
						 E7.setTextColor(Color.BLACK);
						 E8.setTextSize(25);
						 E8.setTextColor(Color.BLACK);
						 String Cn1 = rs.getMetaData().getColumnName(1);
						 String Cn2 = rs.getMetaData().getColumnName(2);
						 String Cn3 = rs.getMetaData().getColumnName(3);
						 String Cn4 = rs.getMetaData().getColumnName(4);
						 E1.setText(Cn1);
						 E3.setText(Cn2);
						 E5.setText(Cn3);
						 E7.setText(Cn4);
						 E2.setText(es);
						 E4.setText(es);
						 E6.setText(es);
						 E8.setText(es);
						 Erow1.addView(E1);
						 Erow1.addView(E2);
						 Erow1.addView(E3);
						 Erow1.addView(E4);
						 Erow1.addView(E5);
						 Erow1.addView(E6);
						 Erow1.addView(E7);
						 //Erow1.addView(E8);
						 E1.setGravity(Gravity.CENTER_HORIZONTAL);
						 E2.setGravity(Gravity.CENTER_HORIZONTAL);
						 E3.setGravity(Gravity.CENTER_HORIZONTAL);
						 E4.setGravity(Gravity.CENTER_HORIZONTAL);
						 E5.setGravity(Gravity.CENTER_HORIZONTAL);
						 E6.setGravity(Gravity.CENTER_HORIZONTAL);
						 E7.setGravity(Gravity.CENTER_HORIZONTAL);
						 E8.setGravity(Gravity.CENTER_HORIZONTAL);
						 Erow1.setGravity(Gravity.CENTER);
						 Erow1.setBackgroundColor(Color.GREEN);
						 tablatest.addView(Erow1);	
						// finaliza agregar encabezados de columnas	
						 int sz = rs.getFetchSize();
							int cont = 0;
							while (rs.next()) {
								 String d1 = rs.getString(1).trim();
								 String d2 = rs.getString(2).trim();
								 String d3 = rs.getString(3).trim();
								 String d4 = rs.getString(4).trim();
								 String c1 = comprobar(d1);
								 String c2 = comprobar(d2);
								 String c3 = comprobar(d3);
								 String c4 = comprobar(d4);
								 for (int i = 0; i == sz; i++)
								 {	
									 cont = cont +1;
									 TableRow row1 = new TableRow(Reportest.this);
									 TextView t1 = new TextView(Reportest.this);
									 TextView t2 = new TextView(Reportest.this);
									 TextView t3 = new TextView(Reportest.this);
									 TextView t4 = new TextView(Reportest.this);
									 TextView t5 = new TextView(Reportest.this);
									 TextView t6 = new TextView(Reportest.this);
									 TextView t7 = new TextView(Reportest.this);
									 TextView t8 = new TextView(Reportest.this); 
									 t1.setTextSize(20);
									 t1.setTextColor(Color.BLACK);
									 t2.setTextSize(25);
									 t2.setTextColor(Color.BLACK);
									 t3.setTextSize(20);
									 t3.setTextColor(Color.BLACK);
									 t4.setTextSize(25);
									 t4.setTextColor(Color.BLACK);
									 t5.setTextSize(20);
									 t5.setTextColor(Color.BLACK);
									 t6.setTextSize(25);
									 t6.setTextColor(Color.BLACK);
									 t7.setTextSize(20);
									 t7.setTextColor(Color.BLACK);
									 t8.setTextSize(25);
									 t8.setTextColor(Color.BLACK);
									 t1.setGravity(Gravity.CENTER_HORIZONTAL);
									 t2.setGravity(Gravity.CENTER_HORIZONTAL);
									 //t7.setMaxWidth(100);
									 t4.setGravity(Gravity.CENTER_HORIZONTAL);
									 t5.setGravity(Gravity.CENTER_HORIZONTAL);
									 t6.setGravity(Gravity.CENTER_HORIZONTAL);
									 t7.setGravity(Gravity.CENTER_HORIZONTAL);
									 t8.setGravity(Gravity.CENTER_HORIZONTAL);
									 row1.setGravity(Gravity.CENTER);
									 row1.setClickable(true);
									 if(cont%2 == 0){
									 row1.setBackgroundColor(Color.LTGRAY);
									 }else {
									 row1.setBackgroundColor(Color.WHITE);	 
									 }
								 t1.setText(c1);
								 t2.setText(es);
								 t3.setText(c2);
								 t4.setText(es);
								 t5.setText(c3);
								 t6.setText(es);
								 t7.setText(c4);
								 t8.setText(es);
								 row1.addView(t1);
								 row1.addView(t2);
								 row1.addView(t3);
								 row1.addView(t4);
								 row1.addView(t5);
								 row1.addView(t6);
								 row1.addView(t7);
								 //row1.addView(t8);
								 tablatest.addView(row1);
								 tablatest.setClickable(true);
							}
							}
						connection.close();	
	            	}
					catch (Exception e) {
						Toast toast = Toast.makeText(getApplicationContext(), "Verificar Conexi�n.....", Toast.LENGTH_SHORT);
		            	toast.show();
						e.printStackTrace();
	            } 
	           } 	
	         } 
	        });	
		/*    
		    gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
	            @Override
	            public void onItemClick(AdapterView<?> parent, final View view, int position, long id){
	            			Pos =  parent.getItemIdAtPosition(position);
	            			int ifila = (int)(Pos/7);
	            			int icelda = (ifila*7)+1; 
	            			int idia = (icelda)-1;
	            			String Invpv = (String) parent.getItemAtPosition(icelda);
	            			Inv = Invpv.replace("-","");
	            			DiaCorte = (String) parent.getItemAtPosition(idia);
		    	        	Intent intent = new Intent(getApplicationContext(), DetalleCorte.class);
		    	        	intent.putExtra("Inv", Inv);
		    	        	intent.putExtra("Fechade", Fechade);
		    	        	intent.putExtra("Fechaal", Fechaal);
		    	        	intent.putExtra("dia1", dia1);
		    	        	intent.putExtra("mes1", mes1);
		    	        	intent.putExtra("a�o1", a�o1);
		    	        	intent.putExtra("Fechade1", Fechade1);
		    	        	intent.putExtra("Fechaal1", Fechaal1);
		    	        	intent.putExtra("a�o2", a�o2);
		    	        	intent.putExtra("dia2", dia2);
		    	        	intent.putExtra("mes2", mes2);
		    	        	intent.putExtra("Wk", Wk);
		    	        	intent.putExtra("DiaCorte", DiaCorte);
		    	        	startActivity(intent);	    	    
		    	            }
		    	    });
		    	      */
/*
		}
		
		public void alertDialogMensaje(String message1, String mesage2){
		
			try {
			    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
			    ringtone.play();	    	    
			} catch (Exception e) {
			    e.printStackTrace();
			}
			AlertDialog alertDialog = new AlertDialog.Builder(this).create();
			alertDialog.setTitle(message1);
			alertDialog.setMessage(mesage2);
			alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
			// here you can add functions
				ringtone.stop();
			}
			});
			alertDialog.show();
		}
		
		public String comprobar(String cad){
			String Pb = "AP";
			if(cad.equals(Pb)){
				cad = "Procesado";
			}
			return cad;	
		}
		
		public String format (int f){
			val = "";
			if(f<10){
			val = "0"+Integer.toString(f);	
			}
			else{
			val = Integer.toString(f);
			}
			return val;
		}
				
		@Override
		public boolean onCreateOptionsMenu(Menu menu) {

			// Inflate the menu; this adds items to the action bar if it is present.
			getMenuInflater().inflate(R.menu.soporteit, menu);
			return true;
		}

		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			// Handle action bar item clicks here. The action bar will
			// automatically handle clicks on the Home/Up button, so long
			// as you specify a parent activity in AndroidManifest.xml.
			int id = item.getItemId();
			if (id == R.id.action_settings) {
				return true;
			}
			return super.onOptionsItemSelected(item);
		}
	}  
*/