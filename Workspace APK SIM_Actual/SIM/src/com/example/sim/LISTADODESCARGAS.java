package com.example.sim;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import com.dropbox.core.DbxException;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.FileMetadata;
import com.example.floricultura.R;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.DFamilias;
import com.example.sim.data.DUbicaciones;
import com.example.sim.data.Dacceso;
import com.example.sim.data.Dazucareras;
import com.example.sim.data.Dbolsas;
import com.example.sim.data.Dempleado;
import com.example.sim.data.Dinvernaderos;
import com.example.sim.data.Filtros;
import com.example.sim.data.JSONReader;
import com.example.sim.data.Mdepartamento;
import com.example.sim.data.Modulos;
import com.example.sim.data.Opciones;
import com.example.sim.data.QCPolen;
import com.example.sim.data.Semillas;
import com.example.sim.data.SemillasAcond;
import com.example.sim.data.SiembraVegetativo;
import com.example.sim.data.Usuario;

@SuppressLint("NewApi") 
public class LISTADODESCARGAS extends ActionBarActivity {
	
	
	private ProgressDialog loadingDialogDropbox;
	private final String DIR_APP = "/SIM/";
	private ProgressDialog loadingDialog;
	private MyApp appState;	
	private DBAdapter db;
	Ringtone ringtone;
	ArrayList<String> listadoJson = new ArrayList<String>();
	private DbxClientV2 dbxClientV2;
	
	
	String user ="";
	String password = "";
	String URL = "";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_listado_descargas);
		
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		
		
		Button btnSemillas = (Button) findViewById(R.id.btnSemillas);
		Button btnVegetativo = (Button) findViewById(R.id.btnVegetativo);
		Button btnIT = (Button) findViewById(R.id.btnIT);
		final CheckBox Acces = (CheckBox) findViewById(R.id.checkAnterior);
		final CheckBox Sem = (CheckBox) findViewById(R.id.checkBox2);
		final CheckBox Mod = (CheckBox) findViewById(R.id.checkBox3);
		final CheckBox Opc = (CheckBox) findViewById(R.id.checkBox4);
		final CheckBox Usr = (CheckBox) findViewById(R.id.checkBox5);
		final CheckBox Veg = (CheckBox) findViewById(R.id.checkBox6);
		final CheckBox Emp = (CheckBox) findViewById(R.id.checkBox7);
		final CheckBox Bol = (CheckBox) findViewById(R.id.checkBox8);
		final CheckBox Ubi = (CheckBox) findViewById(R.id.checkBox9);
		final CheckBox SiemV = (CheckBox) findViewById(R.id.checkBox10);
		final CheckBox Fil = (CheckBox) findViewById(R.id.checkBox11);
		final CheckBox SemAcond = (CheckBox) findViewById(R.id.checkBox12);
		final CheckBox QCPol = (CheckBox) findViewById(R.id.checkBox13);
		appState = ((MyApp)getApplicationContext());   
        db = appState.getDb();
        db.open();        
        dbxClientV2 = appState.getDropBoxClient();
        
        //Cuando se Indica hacer el Check en cada CheckBox se agrega al Listado de Descargas el Json Correspondiente.
        // Agregue Funci�n del Bot�n Semillas
        btnSemillas.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	if(Acces.isChecked() == false){
            		Acces.setChecked(true);
            		listadoJson.add("D_Acceso.json");
            		Sem.setChecked(true);
            		listadoJson.add("D_Semillas.json");
            		Emp.setChecked(true);
            		listadoJson.add("D_Empleados.json");
            		Bol.setChecked(true);
            		listadoJson.add("D_Bolsas.json");
	            	listadoJson.add("D_Azucareras.json");
            		Fil.setChecked(true);	
            		listadoJson.add("D_Filtros.json");
            		QCPol.setChecked(true);
            		listadoJson.add("DQC_Polen.json");
            	}else {
            		Acces.setChecked(false);
            		listadoJson.remove("D_Acceso.json");
            		Sem.setChecked(false);
            		listadoJson.remove("D_Semillas.json");
            		Emp.setChecked(false);
            		listadoJson.remove("D_Empleados.json");
            		Bol.setChecked(false);
            		listadoJson.remove("D_Bolsas.json");
	            	listadoJson.remove("D_Azucareras.json");
            		Fil.setChecked(false);
            		listadoJson.remove("D_Filtros.json");
            		QCPol.setChecked(false);
            		listadoJson.remove("DQC_Polen.json");
            	}
            }
        });
        
        // Funci�n de Bot�n Vegetativo
        btnVegetativo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	if(Acces.isChecked() == false){
            		Acces.setChecked(true);
            		listadoJson.add("D_Acceso.json");
            		Veg.setChecked(true);
            		listadoJson.add("D_Vegetativo.json");
            		listadoJson.add("DFamilias.json");
            		Emp.setChecked(true);
            		listadoJson.add("D_Empleados.json");
            		Ubi.setChecked(true);
            		listadoJson.add("D_Invernaderos.json");
            		listadoJson.add("DUbicaciones.json");
            		SiemV.setChecked(true);
            		listadoJson.add("D_SiembrasVeg.json");
            	}else{
            		Acces.setChecked(false);
            		listadoJson.remove("D_Acceso.json");
            		Veg.setChecked(false);
            		listadoJson.remove("D_Vegetativo.json");
            		listadoJson.remove("DFamilias.json");
            		Emp.setChecked(false);
            		listadoJson.remove("D_Empleados.json");
            		Ubi.setChecked(false);
            		listadoJson.remove("D_Invernaderos.json");
            		listadoJson.remove("DUbicaciones.json");
            		SiemV.setChecked(false);
            		listadoJson.remove("D_SiembrasVeg.json");
            	}
            }
        });
        		
        Button btnBack = (Button) findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
 
            	onBackPressed();
            }
        });
        
        btnIT.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            		new ActualizaAPP().execute();
            }
        });
     
        Button btnDescarga = (Button) findViewById(R.id.cargarDatos);
        btnDescarga.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	if(appState.connectionAvailable() == true){
            	new DescargarDatos().execute();
            	} else {
            		alertDialogMensaje("ERROR DE CONEXION","NO HAY CONEXION WIFI");
            	
            	}
            	
            }
        });
   
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.listado_descargas, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
		
	public void onCheckboxClicked(View view) {
	    boolean checked = ((CheckBox) view).isChecked();
	    boolean contains = false;
	    // Check which checkbox was clicked
	    switch(view.getId()) { 
	        case R.id.checkAnterior:
	        	contains = listadoJson.contains("D_Acceso.json");
	            if (checked){
	            	if (contains == false)
	            	listadoJson.add("D_Acceso.json");
	            }else{
	            	if (contains == true)
	            	listadoJson.remove("D_Acceso.json");
	            }
	            break;
	            
	        case R.id.checkBox2:	            
	        	contains = listadoJson.contains("D_Semillas.json");
	            if (checked){
	            	if (contains == false)
	            	listadoJson.add("D_Semillas.json");
	            }else{
	            	if (contains == true)
	            	listadoJson.remove("D_Semillas.json");
	            }
	            break;
	            
	        case R.id.checkBox3:
	        	contains = listadoJson.contains("Modulos.json");
	            if (checked){
	            	if (contains == false)
	            	listadoJson.add("Modulos.json");
	            	listadoJson.add("DDepartamento.json");
	            }else{
	            	if (contains == true)
	            	listadoJson.remove("Modulos.json");
	            	listadoJson.remove("DDepartamento.json");
	            }
	            break;
	            
	        case R.id.checkBox4:
	        	contains = listadoJson.contains(" .json");
	            if (checked){
	            	if (contains == false)
	            	listadoJson.add("Opciones.json");
	            }else{
	            	if (contains == true)
	            	listadoJson.remove("Opciones.json");
	            }
	            break;
	            
	        case R.id.checkBox5:
	        	contains = listadoJson.contains("Usuarios.json");
	            if (checked){
	            	if (contains == false)
	            	listadoJson.add("Usuarios.json");
	            }else{
	            	if (contains == true)
	            	listadoJson.remove("Usuarios.json");
	            }
	            break;
	            
	        case R.id.checkBox6:
	        	contains = listadoJson.contains("D_Vegetativo.json");
	            if (checked){
	            	if (contains == false)
	            	listadoJson.add("D_Vegetativo.json");
	            	listadoJson.add("DFamilias.json");
	            }else{
	            	if (contains == true)
	            	listadoJson.remove("D_Vegetativo.json");
	            	listadoJson.remove("DFamilias.json");
	            }
	            break;
	            
	        case R.id.checkBox7:
	        	contains = listadoJson.contains("D_Empleados.json");
	            if (checked){
	            	if (contains == false)
	            	listadoJson.add("D_Empleados.json");
	            }else{
	            	if (contains == true)
	            	listadoJson.remove("D_Empleados.json");
	            }
	            break;
	            
	        case R.id.checkBox8:
	        	contains = listadoJson.contains("D_Bolsas.json");
	            if (checked){
	            	if (contains == false)
	            	listadoJson.add("D_Bolsas.json");
	            	listadoJson.add("D_Azucareras.json");
	            }else{
	            	if (contains == true)
	            	listadoJson.remove("D_Bolsas.json");
	            	listadoJson.remove("D_Azcucareras.json");
	            }
	            break;
	            
	        case R.id.checkBox9:
	        	contains = listadoJson.contains("D_Invernaderos.json");
	            if (checked){
	            	if (contains == false)
	            	listadoJson.add("D_Invernaderos.json");
	            	listadoJson.add("DUbicaciones.json");
	            }else{
	            	if (contains == true)
	            	listadoJson.remove("D_Invernaderos.json");
	            	listadoJson.remove("DUbicaciones.json");
	            }
	            break;     
	        case R.id.checkBox10:
	        	contains = listadoJson.contains("D_SiembrasVeg.json");
	            if (checked){
	            	if (contains == false)
	            	listadoJson.add("D_SiembrasVeg.json");
	            }else{
	            	if (contains == true)
	            	listadoJson.remove("D_SiembrasVeg.json");
	            }
	            break;   
	        case R.id.checkBox11:
	        	contains = listadoJson.contains("D_Filtros.json");
	            if (checked){
	            	if (contains == false)
	            	listadoJson.add("D_Filtros.json");
	            }else{
	            	if (contains == true)
	            	listadoJson.remove("D_Filtros.json");
	            }
	            break; 
	        case R.id.checkBox12:
	        	contains = listadoJson.contains("D_SemillasAcond.json");
	            if (checked){
	            	if (contains == false)
	            	listadoJson.add("D_SemillasAcond.json");
	            }else{
	            	if (contains == true)
	            	listadoJson.remove("D_SemillasAcond.json");
	            }
	            break;
	        case R.id.checkBox13:
	        	contains = listadoJson.contains("DQC_Polen.json");
	            if (checked){
	            	if (contains == false)
	            	listadoJson.add("DQC_Polen.json");
	            }else{
	            	if (contains == true)
	            	listadoJson.remove("DQC_Polen.json");
	            }
	            break;
	    }
	}
	
    private class DescargarDatos extends AsyncTask<Void, Integer, Boolean> {   	
    	
        @Override
	        protected void onPreExecute() {
	        	loadingDialogDropbox = ProgressDialog.show(LISTADODESCARGAS.this,"", "Descargando Datos.....", true, false);
	        }
    	
        @Override 
        protected Boolean doInBackground(Void... params) {
        	
        	
        	for(int i =0;i<listadoJson.size();i++){
        			Log.d("descargando ", listadoJson.get(i) + "");
        		
		        	String nameJson =listadoJson.get(i);
		        	File file = new File( Environment.getExternalStorageDirectory()+"/SIM/JsonCarga/" + nameJson);
		        	FileOutputStream outputStream;
		        	String directorioDropbox = DIR_APP + "JsonCarga/" + nameJson;
		        	try {

						outputStream = new FileOutputStream(file);
						//IVAN: esta es la nueva forma de descargar datos de Dropbox
						FileMetadata fileMetadata = dbxClientV2.files().download(directorioDropbox).download(outputStream);
						Log.i("DbExampleLog", "The file's rev is: " + fileMetadata.getRev());
						mHandler.sendEmptyMessage(0);
						
					} catch (DbxException e) {
						e.printStackTrace();
					}  catch (IllegalStateException e) {
						mHandler.sendEmptyMessage(-1);
						e.printStackTrace();
						return false;
					} catch (IOException e) {
						e.printStackTrace();
					}
	        	}    	
		     return true;
        }
     

        @Override
        protected void onPostExecute(Boolean result) {
        	if (result == true){
        	loadingDialogDropbox.dismiss();
        	new GuardarDatos().execute();
        	} 
        }
    }

    private class ActualizaAPP extends AsyncTask<Void, Integer, Boolean> {   	
    	
        @Override
        protected void onPreExecute() {
        	loadingDialogDropbox = ProgressDialog.show(LISTADODESCARGAS.this,"", "Descargando Actualizacion.....", true, false);
        }
    	
        @Override 
        protected Boolean doInBackground(Void... params) {
        	
        				String name = "SIM.apk";
        				File file = new File( Environment.getExternalStorageDirectory()+"/SIM/" + name);
        				FileOutputStream outputStream;
        				String directorio = DIR_APP +  name;
        				try {

        					outputStream = new FileOutputStream(file);
        					//IVAN: esta es la nueva forma de descargar datos de Dropbox
        					FileMetadata fileMetadata = dbxClientV2.files().download(directorio).download(outputStream);
        					Log.i("DbExampleLog", "The file's rev is: " + fileMetadata.getRev());
        					mHandler.sendEmptyMessage(0);

        					//IVAN: los exceptions cambiarion en el nuevo API, creo que con estos es suficiente
        				} catch (DbxException e) {
        					e.printStackTrace();
        				}  catch (IllegalStateException e) {
        					mHandler.sendEmptyMessage(-1);
        					e.printStackTrace();
        					return false;
        				} catch (IOException e) {
        					e.printStackTrace();
        				}
            return true;
        }
     
        @Override
        protected void onPostExecute(Boolean result) {
        	if (result == true){
        	loadingDialogDropbox.dismiss();     	
        	 final Intent intent = new Intent(Intent.ACTION_VIEW);
			 intent.setDataAndType(Uri.fromFile(new File(Environment.getExternalStorageDirectory()+"/SIM/" + "SIM.apk")),"application/vnd.android.package-archive");
			 intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			 startActivity(intent);
        	}
        }
    }
    
    public void InsertarDatosPrueba2(){
    	
    	JSONReader _reader = appState.getReader();
    	for(int j =0;j<listadoJson.size();j++){
    		if(listadoJson.get(j).equals("D_Acceso.json")){
    			db.ReiniciarTabla(db.TIPO_DACCESO);
    	    	ArrayList<Dacceso> d = (ArrayList<Dacceso>) ( _reader.ReadFromSDCard(5, "SIM/JsonCarga/D_Acceso.json"));
    	    	for(int i =0; i<d.size() ; i++){
    	    		db.insertarDacceso(d.get(i));}
    		}else if(listadoJson.get(j).equals("D_Semillas.json")){
    			db.ReiniciarTabla(db.TIPO_SEMILLAS);
    	    	ArrayList<Semillas> s = (ArrayList<Semillas>) ( _reader.ReadFromSDCard(2, "SIM/JsonCarga/D_Semillas.json"));
    	    	for(int i =0; i<s.size() ; i++){
    	    		db.insertarSemillas(s.get(i));}
    		}else if(listadoJson.get(j).equals("Modulos.json")){
    			db.ReiniciarTabla(db.TIPO_MODULOS);
    	    	ArrayList<Modulos> m = (ArrayList<Modulos>) ( _reader.ReadFromSDCard(3, "SIM/JsonCarga/Modulos.json"));
    	    	for(int i =0; i<m.size() ; i++){
    	    		db.insertarModulos(m.get(i));}	
    		}else if(listadoJson.get(j).equals("Opciones.json")){
    			db.ReiniciarTabla(db.TIPO_OPCIONES);
    	    	ArrayList<Opciones> o = (ArrayList<Opciones>) ( _reader.ReadFromSDCard(4, "SIM/JsonCarga/Opciones.json"));
    	    	for(int i =0; i<o.size() ; i++){
    	    		db.insertarOpciones(o.get(i));}
    	    	
    		}else if(listadoJson.get(j).equals("Usuarios.json")){
    			db.ReiniciarTabla(db.TIPO_USUARIO);
    			ArrayList<Usuario> u = (ArrayList<Usuario>) ( _reader.ReadFromSDCard(1, "SIM/JsonCarga/Usuarios.json"));
    	    	for(int i =0; i<u.size() ; i++){
    	    		db.insertarUsuario(u.get(i));}   		
    		}
    		else if(listadoJson.get(j).equals("D_Empleados.json")){
    			db.ReiniciarTabla(db.TIPO_DEMPLEADO);
    			ArrayList<Dempleado> d = (ArrayList<Dempleado>) ( _reader.ReadFromSDCard(7, "SIM/JsonCarga/D_Empleados.json"));
    	    	for(int i =0; i<d.size() ; i++){
    	    		db.insertDEmpleado(d.get(i));}   		
    		}
    		else if(listadoJson.get(j).equals("D_Bolsas.json")){
    			db.ReiniciarTabla(db.TIPO_DBOLSAS);
    			ArrayList<Dbolsas> b = (ArrayList<Dbolsas>) ( _reader.ReadFromSDCard(8, "SIM/JsonCarga/D_Bolsas.json"));
    	    	for(int i =0; i<b.size() ; i++){
    	    		db.insertDBolsas(b.get(i));}   		
    		}
    		else if(listadoJson.get(j).equals("D_Invernaderos.json")){
    			db.ReiniciarTabla(db.TIPO_DINVERNADERO);
    			ArrayList<Dinvernaderos> p = (ArrayList<Dinvernaderos>) ( _reader.ReadFromSDCard(9, "SIM/JsonCarga/D_Invernaderos.json"));
    	    	for(int i =0; i<p.size() ; i++){
    	    		db.insertDInvernaderos(p.get(i));}   		
    		}
    		else if(listadoJson.get(j).equals("D_SiembrasVeg.json")){
    			db.ReiniciarTabla(db.TIPO_DSIEMBRAVEGETATIVO);
    			ArrayList<SiembraVegetativo> w = (ArrayList<SiembraVegetativo>) ( _reader.ReadFromSDCard(10, "SIM/JsonCarga/D_SiembrasVeg.json"));
    	    	for(int i =0; i<w.size() ; i++){
    	    		db.insertarSiembraVegetativo(w.get(i));}   		
    		}
    		else if(listadoJson.get(j).equals("D_Azucareras.json")){
    			db.ReiniciarTabla(db.TIPO_DAZUCARERAS);
    			ArrayList<Dazucareras> z = (ArrayList<Dazucareras>) ( _reader.ReadFromSDCard(11, "SIM/JsonCarga/D_Azucareras.json"));
    	    	for(int i =0; i<z.size() ; i++){
    	    		db.insertarDazucareras(z.get(i));}   		
    		}
    		else if(listadoJson.get(j).equals("D_Filtros.json")){
    			db.ReiniciarTabla(db.TIPO_DFILTROS);
    			ArrayList<Filtros> f = (ArrayList<Filtros>) ( _reader.ReadFromSDCard(12, "SIM/JsonCarga/D_Filtros.json"));
    	    	for(int i =0; i<f.size() ; i++){
    	    		db.insertarDFiltros(f.get(i));}   		
    		}
    		else if(listadoJson.get(j).equals("D_SemillasAcond.json")){
    			db.ReiniciarTabla(db.TIPO_SEMILLAS);
    			ArrayList<SemillasAcond> sa = (ArrayList<SemillasAcond>) ( _reader.ReadFromSDCard(13, "SIM/JsonCarga/D_SemillasAcond.json"));
    	    	for(int i =0; i<sa.size() ; i++){
    	    		db.insertarSemillasAcond(sa.get(i));}   		
    		}	
    		else if(listadoJson.get(j).equals("DQC_Polen.json")){
    			db.ReiniciarTabla(db.TIPO_QCPOLEN);
    			ArrayList<QCPolen> q = (ArrayList<QCPolen>) ( _reader.ReadFromSDCard(14, "SIM/JsonCarga/DQC_Polen.json"));
    	    	for(int i =0; i<q.size() ; i++){
    	    		db.insertarQCPolen(q.get(i));}   		
    		}
    		else if(listadoJson.get(j).equals("DFamilias.json")){
    			db.ReiniciarTabla(db.TIPO_DFAMILIAS);
    			ArrayList<DFamilias> f = (ArrayList<DFamilias>) ( _reader.ReadFromSDCard(15, "SIM/JsonCarga/DFamilias.json"));
    	    	for(int i =0; i<f.size() ; i++){
    	    		db.insertarDFamilias(f.get(i));}   		
    		} 
    		else if(listadoJson.get(j).equals("DDepartamento.json")){
    			db.ReiniciarTabla(db.TIPO_MDEPARTAMENTO);
    			ArrayList<Mdepartamento> f = (ArrayList<Mdepartamento>) ( _reader.ReadFromSDCard(16, "SIM/JsonCarga/DDepartamento.json"));
    	    	for(int i =0; i<f.size() ; i++){
    	    		db.insertarMdepartamento(f.get(i));}   		
    		}	
    		else if(listadoJson.get(j).equals("DUbicaciones.json")){
    			db.ReiniciarTabla(db.TIPO_DUBICACIONES);
    			ArrayList<DUbicaciones> u = (ArrayList<DUbicaciones>) ( _reader.ReadFromSDCard(17, "SIM/JsonCarga/DUbicaciones.json"));
    	    	for(int i =0; i<u.size() ; i++){
    	    		db.insertDUbicaciones(u.get(i));}   		
    		}
    	}
    }
    
    private class GuardarDatos extends AsyncTask<Void, Integer, Boolean> {   	
    	
        @Override
        protected void onPreExecute() {
        	loadingDialog = ProgressDialog.show(LISTADODESCARGAS.this,"", "Guardando al dispositivo....", true, false);
        }
    	 
        @Override 
        protected Boolean doInBackground(Void... params) {
        	
        	InsertarDatosPrueba2();
        	
            return true;
        }
             
        @Override
        protected void onPostExecute(Boolean result) {
        	 loadingDialog.dismiss();
        	 appState.exportDatabse("SIM.db");//exporta la base de datos a la SD card		
        	
        	 Intent i = new Intent(getApplicationContext(), MAINACTIVITY.class);
	         startActivity(i);
	         finish();
        }

    }
    
    private Handler mHandler = new Handler() {
    	public void handleMessage(android.os.Message msg) {
    		if(msg.what == -1){ // Error en la Conexi�n o Descarga
    			Toast.makeText(LISTADODESCARGAS.this, "ERROR EN LA CONEXION NO SE HA PODIDO DESCARGAR.....", Toast.LENGTH_SHORT).show();
    			loadingDialogDropbox.dismiss();
    			//startActivity(new Intent(ListadoDescargas.this, ListadoDescargas.class));
    		}
    		else if(msg.what == 0){ // Descargado con Exito
    			Toast.makeText(LISTADODESCARGAS.this, "Descargando exitosamente.....", Toast.LENGTH_SHORT).show();
    		}
    		
    	}
    	
    };
    
    public void insertar(View v){
    	switch(v.getId()){
    	case R.id.insert:
    		try{
				appState = ((MyApp)getApplicationContext());
 			    db = appState.getDb();
 			    db.open();
				 user = "sim";
            	 password = "sim";
            	 Connection connection = null;
            	 ResultSet rs = null;
            	 Statement statement = null;
            	 String Clave="";
            	 String CodigoAPP="";
            	 String CodigoDepto="";
            	 String CodigoEmpleado="";
            	 String Tipo ="";
            	 String Usuario="";
					Class.forName("com.ibm.as400.access.AS400JDBCDriver");
					connection = DriverManager.getConnection("jdbc:as400://190.190.190.10", user, password);
					System.setProperty("http.keepAlive", "false");
				
					statement = connection.createStatement();		
					
					rs = statement.executeQuery("select upper(u.usuario) usuario,upper(u.clave) clave,upper(u.tipo) tipo,case when u.codigoempleado is null then 0 else u.codigoempleado end codigoempleado,case when e.codigodepto is null then 0 else e.codigodepto end CodigoDepto,opciones_app from siscfgf.usuario u left join sisrrhhf.mempleado e on u.codigoempleado = e.correlativo where u.estado = 'AC'");
					while (rs.next()) {
						Clave= rs.getString(2).trim();
						//CodigoAPP = rs.getString(6).trim();
						CodigoDepto = rs.getString(5).trim();
						CodigoEmpleado = rs.getString(4).trim();
						Tipo = rs.getString(3).trim();
						Usuario = rs.getString(1).trim();
						//db.insertUsuarios(Clave, CodigoAPP, CodigoDepto, CodigoEmpleado, Tipo, Usuario);
					}
 		
					
 			   
 			    	//db.insertUsuarios("01010101", "AS", "25", "2550", "AD", "porfi");
					
 			    	Toast toast = Toast.makeText(getApplicationContext(), "Insertado", Toast.LENGTH_SHORT);
	            	toast.show();
 			  //Insertamos el registro en la base de datos
 			
					
					 db.close();
			}
				catch (Exception e) {
					Toast toast = Toast.makeText(getApplicationContext(), "Verificar Conexi�n.....", Toast.LENGTH_SHORT);
	            	toast.show();
					e.printStackTrace();
			}
			    
    		break;
    	}
    }
	    public void alertDialogMensaje(String message1, String mesage2){	
			try {
			    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
			    ringtone.play(); 
			} catch (Exception e) {
			    e.printStackTrace();
			}	
			AlertDialog alertDialog = new AlertDialog.Builder(this).create();
			alertDialog.setTitle(message1);
			alertDialog.setMessage(mesage2);
			alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
			// here you can add functions
				ringtone.stop();
			}
			});
			alertDialog.show();
		}
    
   
}
