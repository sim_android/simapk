package com.example.sim;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.Vibrator;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.floricultura.R;
import com.example.sim.data.DBAdapter;

public class DetalleConsultaPlantas extends ActionBarActivity {
	
	//Variables globales
		private MyApp appState;	
		private DBAdapter db;
		int CodigoModulo;
		int Codigodepartamento;
		int CodigoEmpleado;
		int CodigoOpcion;
		long Pos;
		String Act;
		String Desc;
		String Siembra;
		String Fecha;
		String Inv;
		String Empleado;
		GridView gridview;
		GridView gridEnc;
		Vibrator mVibrator;
		Ringtone ringtone;
		String Fechade;
		TextView NomActividad;
		TextView Invernadero;
		TextView FechaF;
		TextView siem;
		TextView Corre;
		TextView Emplea;
		EditText Plantas;
		String tCorrelativo;
		String tNombre;
		String tApellido;
		String tEmpleado;
		String tPlantas;
		ArrayAdapter<String> adapter;
		ArrayList<String> datos;
		ArrayList<String> encabezado;
		ArrayAdapter<String> adapEnc;
		
		private String val;
		
		@SuppressLint("NewApi") @Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.detalle_consultaplantas);
			
			// para manejar la excepcion de permisos en el API de Android
	    	StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	        StrictMode.setThreadPolicy(policy);

			//se obtienen lo datos de la actividad antetior por medio del intent
			Bundle extras = getIntent().getExtras();
			CodigoModulo = extras.getInt("CodigoModulo");
			Codigodepartamento = extras.getInt("Codigodepartamento");
		    CodigoOpcion = extras.getInt("CodigoOpcion");
		    CodigoEmpleado = extras.getInt("CodigoEmpleado");
		    Act = extras.getString("Act");
		    Desc = extras.getString("Desc");
		    Siembra = extras.getString("Siembra");
		    Inv = extras.getString("Inv");  
		    Fecha = extras.getString("Fecha");
		   
		    //hacemos referencia a todos los objetos de nuestra vista
		    NomActividad = (TextView)findViewById(R.id.TextNombreActividad);
		    TextView TCodigoEmpleado= (TextView)findViewById(R.id.CodigoEmpleado);		
	        Button btnBack = (Button) findViewById(R.id.btnBack);
	        Button btnOK = (Button) findViewById(R.id.btnAsistencia);
	        final EditText editFechade = (EditText) findViewById(R.id.editFechaDe);	        
	        siem = (TextView) findViewById(R.id.TextSiembra);
	        Invernadero = (TextView) findViewById(R.id.TextInv);
	        FechaF = (TextView) findViewById(R.id.TextFecha);
	        Emplea = (TextView) findViewById(R.id.TextEmpleado);
	        Corre = (TextView) findViewById(R.id.TextCorrelativo);
	        Plantas = (EditText) findViewById(R.id.EditPlantas);
	        Plantas.setInputType(InputType.TYPE_CLASS_NUMBER);   //Para Asignarle un tipo de dato 
	        Plantas.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
	        Plantas.setSingleLine(false);   // se utiliza para que el EditText no haga un Tab Automatico.
	        gridEnc = (GridView) findViewById(R.id.gridEnc);
	        gridview = (GridView) findViewById(R.id.gridDetalle);
	       
	        
	        datos = new ArrayList<String>();
	        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, datos);
	        encabezado = new ArrayList<String>();
	        adapEnc = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, encabezado);
	        
		    //hacemos las inicializaciones necesarias a nuestra vista
	        NomActividad.setText(Desc);
		    TCodigoEmpleado.setText(CodigoEmpleado + "");
		    siem.setText(Siembra);
		    Invernadero.setText("In. " + Inv);
		    FechaF.setText(Fecha);
		    
		    //incializamos la BD asi como tambien obtenemos la referencia al singleton
		    appState = ((MyApp)getApplicationContext());   
		    db = appState.getDb();
		    db.open();
		    
		    // Se cargar el Grid con la Funci�n Cargar y se inicializan los valores.
		    Cargar();
         	   	   	  
		    gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
	            @Override
	            public void onItemClick(AdapterView<?> parent, final View view, int position, long id){
	            			Pos =  parent.getItemIdAtPosition(position);
	            			Emplea.setText("");
	            			Plantas.setText("");
	            			Corre.setText("");
	            			int ifila = (int)(Pos/7);
	            			int icorre = (ifila*7); 
	            			int inombre = (ifila*7)+1;
	            			int iapellido = (ifila*7)+2;
	            			int iplantas = (ifila*7)+6;
	            			tCorrelativo = (String) parent.getItemAtPosition(icorre);
	            			tNombre = (String) parent.getItemAtPosition(inombre);
	            			tApellido = (String) parent.getItemAtPosition(iapellido);
	            			tEmpleado =  tNombre + "  " + tApellido;
	            			tPlantas = (String) parent.getItemAtPosition(iplantas);
	            			// Se asignan los valores a las vistas
	            			Emplea.setText(tEmpleado);
	            			Plantas.setText(tPlantas);
	            			Corre.setText(tCorrelativo);
		    	            }
		    	    });
		    	      
		    btnBack.setOnClickListener(new View.OnClickListener() {
	            public void onClick(View v) {
	 
	            	onBackPressed();
	            }
	        });
	    		    		    
		    btnOK.setOnClickListener(new View.OnClickListener() {
	            public void onClick(View v) {
	            	                     	
	            	String Pb = "";
	            	String plan = Plantas.getText().toString().trim();
	            	String Cor = Corre.getText().toString().trim();
	            	
	            	if(plan.equals(Pb)){
	            		alertDialogMensaje("ERROR","DEBE INGRESAR LAS PLANTAS");
	            		Plantas.setText("");
	            		Plantas.requestFocus();           		
	            	}
	            	else 
	            	{
	            	try {
	            	String user = "sim";
	            	String pasword = "sim";
	            	 Connection connection = null;
	            	 int rs ;
	            	 Statement statement = null;
						Class.forName("com.ibm.as400.access.AS400JDBCDriver");
						connection = DriverManager.getConnection("jdbc:as400://190.190.190.10", user, pasword);	
						statement = connection.createStatement();			     
						rs = statement.executeUpdate(
						"UPDATE SISAPPF.APP_TMP_GENERAL SET PLANTAS = '"+ plan +"' WHERE CORRELATIVO = '"+ Cor +"'");
						
						if(rs > 0){
							Toast toast = Toast.makeText(getApplicationContext(), "Cambio Realizado.....", Toast.LENGTH_SHORT);
			            	toast.show();
			            	
			            	Cargar();
						}
						connection.close();	
						
	            	}
					catch (Exception e) {
						Toast toast = Toast.makeText(getApplicationContext(), "Problema de Conexi�n.....", Toast.LENGTH_SHORT);
		            	toast.show();
						e.printStackTrace();
	            } 
	           } 	
	            }
	        });
		  	    	      
		}
		
		public void Cargar(){
			Emplea.setText("");
		    Plantas.setText("");
		    Corre.setText("");
        	datos.clear();
        	adapter.clear();
        	encabezado.clear();
        	adapEnc.clear();
        	
        	try {
        	String user = "sim";
        	String pasword = "sim";
        	 Connection connection = null;
        	 ResultSet rs = null;
        	 Statement statement = null;
        	 
				Class.forName("com.ibm.as400.access.AS400JDBCDriver");
				connection = DriverManager.getConnection("jdbc:as400://190.190.190.10", user, pasword);	
				statement = connection.createStatement();			     
				rs = statement.executeQuery(
				"SELECT G.CORRELATIVO CORRE,G.ACTIVIDAD,SOM.DESCRIPCION,G.SIEMBRA,G.CODIGOEMPLEADO,E.PRIMERNOMBRE NOM,E.PRIMERAPELLIDO APE,G.HORAINICIO INICIO, " +
				"G.HORAFIN FIN,(ifnull(sie.plantas,0) + ifnull(d.desecho,0)) PLANTA,G.PLANTAS TRABAJ,SIE.INVERNADERO FROM SISAPPF.APP_TMP_GENERAL g " +
				"LEFT JOIN SISCFGF.SEGURIDAD_OPCION_MODULOASP SOM ON G.ACTIVIDAD = SOM.OPCION LEFT JOIN SISSEMF.MOV_DEFSIEMBRAE SIE " +
				"ON G.SIEMBRA = SIE.NOSIEMBRA LEFT JOIN SISRRHHF.MEMPLEADO E ON '0' || G.CODIGOEMPLEADO = E.EMPRESA || E.CODIGOEMPLEADO left join " +
				"(select empresa,siembra,sum(plantas) Desecho from sissemf.mov_desechoplantasem " +
				"where fecha > '"+Fecha+"' group by empresa,siembra) d on g.siembra = d.siembra where (G.ESTADO IS NULL OR G.ESTADO = 'CO') AND " +
				"G.FECHA = '"+Fecha+"' AND  G.actividadfinalizada = '1' AND SOM.CONTROL_PLANTAS = 'SI' AND " +
				"G.SIEMBRA = '"+Siembra+"' AND G.ACTIVIDAD = '"+Act+"' ORDER BY G.ACTIVIDAD");

			    //Agregar encabezados de columnas	
				String Cn1 = rs.getMetaData().getColumnName(1);
				encabezado.add(Cn1);
				gridEnc.setAdapter(adapEnc);
				String Cn2 = rs.getMetaData().getColumnName(6);
				encabezado.add(Cn2);
				gridEnc.setAdapter(adapEnc);
				String Cn3 = rs.getMetaData().getColumnName(7);
				encabezado.add(Cn3);
				gridEnc.setAdapter(adapEnc);
				String Cn4 = rs.getMetaData().getColumnName(8);
				encabezado.add(Cn4);
				gridEnc.setAdapter(adapEnc);
				String Cn5 = rs.getMetaData().getColumnName(9);
				encabezado.add(Cn5);
				gridEnc.setAdapter(adapEnc);
				String Cn6 = rs.getMetaData().getColumnName(10);
				encabezado.add(Cn6);
				gridEnc.setAdapter(adapEnc);
				String Cn7 = rs.getMetaData().getColumnName(11);
				encabezado.add(Cn7);
				gridEnc.setAdapter(adapEnc);
				// finaliza agregar encabezados de columnas	
				while (rs.next()) {
				//agrega filas de columnas
					 String d1 = rs.getString(1).trim();
					 String d2 = rs.getString(6).trim();
					 String d3 = rs.getString(7).trim();
					 String d4 = rs.getString(8).trim();
				     String d5 = rs.getString(9).trim();
					 String d6 = rs.getString(10).trim();
					 String d7 = rs.getString(11).trim();
					 String c1 = comprobar(d1);
					 String c2 = comprobar(d2);
					 String c3 = comprobar(d3);
					 String c4 = comprobar(d4);
					 String c5 = comprobar(d5);
					 String c6 = comprobar(d6);
					 String c7 = comprobar(d7);
					 datos.add(c1);
					 gridview.setAdapter(adapter);
					 datos.add(c2);
					 gridview.setAdapter(adapter);
					 datos.add(c3);
					 gridview.setAdapter(adapter);
					 datos.add(c4);
					 gridview.setAdapter(adapter);
					 datos.add(c5);
					 gridview.setAdapter(adapter);
					 datos.add(c6);
					 gridview.setAdapter(adapter);
					 datos.add(c7);
					 gridview.setAdapter(adapter);
				}
				connection.close();	
        	}
			catch (Exception e) {
				Toast toast = Toast.makeText(getApplicationContext(), "Verificar Conexi�n.....", Toast.LENGTH_SHORT);
            	toast.show();
				e.printStackTrace();
			} 
		}
		
		public void alertDialogMensaje(String message1, String mesage2){
		
			try {
			    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
			    ringtone.play();	    	    
			} catch (Exception e) {
			    e.printStackTrace();
			}
			AlertDialog alertDialog = new AlertDialog.Builder(this).create();
			alertDialog.setTitle(message1);
			alertDialog.setMessage(mesage2);
			alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
			// here you can add functions
				ringtone.stop();
			}
			});
			alertDialog.show();
		}
		
		public String comprobar(String cad){
			String Pb = "";
			if(cad.equals(Pb)){
				cad = "---";
			}
			return cad;	
		}
		
		public String format (int f){
			val = "";
			if(f<10){
			val = "0"+Integer.toString(f);	
			}
			else{
			val = Integer.toString(f);
			}
			return val;
		}
				
		@Override
		public boolean onCreateOptionsMenu(Menu menu) {

			// Inflate the menu; this adds items to the action bar if it is present.
			getMenuInflater().inflate(R.menu.soporteit, menu);
			return true;
		}

		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			// Handle action bar item clicks here. The action bar will
			// automatically handle clicks on the Home/Up button, so long
			// as you specify a parent activity in AndroidManifest.xml.
			int id = item.getItemId();
			if (id == R.id.action_settings) {
				return true;
			}
			return super.onOptionsItemSelected(item);
		}
	
		public void ButtonOnClick(View v) {
		    switch (v.getId()) {
		      case R.id.buttonborra:
			      agregarNumero("1");
		        break;
		      case R.id.btnDos:
		    	  agregarNumero("2");
		        break;
		      case R.id.btnTres:
			      agregarNumero("3");
		        break;
		      case R.id.btnCuatro:
		    	  agregarNumero("4");
		        break;
		      case R.id.btnCinco:
			      agregarNumero("5");
		        break;
		      case R.id.btnSeis:
		    	  agregarNumero("6");
		        break;
		      case R.id.btnSiete:
		    	  agregarNumero("7");
		        break;
		      case R.id.btnOcho:
			      agregarNumero("8");
		        break;
		      case R.id.btnNueve:
		    	  agregarNumero("9");
		        break;
		      case R.id.button0:
			      agregarNumero("0");
		        break;
		      case R.id.btnCero:
		    	  agregarNumero("b");
		        break;
		      }
		     
		}
		
		public void agregarNumero(String n){
			
			if(n.contains("b")){
				
				if(Plantas.isFocused()){
					Plantas.setText("");			
				}
				
			}else{
				if(Plantas.isFocused()){
					Plantas.setText(Plantas.getText() + n);
				}
			}
		}
}  
