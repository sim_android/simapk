package com.example.sim;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Vibrator;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.floricultura.R;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.DisponibilidadVegetativo;
import com.example.sim.data.JsonFiles;
import com.example.sim.data.SiembraVegetativo;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CapturaDisponVeg extends ActionBarActivity {
	
	//Variables globales
	private MyApp appState;	
	private DBAdapter db;
	int CodigoModulo;
	int Codigodepartamento;
	int CodigoEmpleado;
	int CodigoOpcion;
	int OpcionPolinizacion;
	int OpcionSuccion;
	int ValorPolinizacion = 0;
	int ValorSuccion=0;
	int TotalEmpleadosAsignados;
	int OpcionCosecha;
	String Invernadero;
	boolean validoDescriptivo;
	String NombreOpcion;
	String Val_Bolsa;
	String pdes;
	String pflor;
	
	LinearLayout validacionesGroup;
	TextView TextVariedad;
	TextView TCantidadAsignados;
	EditText descriptivo;
	EditText empleado;	
	Vibrator mVibrator;
	Ringtone ringtone;

	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_captura_disponveg);
			
		//se obtienen lo datos de la actividad antetior por medio del intent
		Bundle extras = getIntent().getExtras();
		CodigoModulo = extras.getInt("CodigoModulo");
	    CodigoEmpleado = extras.getInt("CodigoEmpleado");
	    Codigodepartamento = extras.getInt("Codigodepartamento");
	    CodigoOpcion = extras.getInt("CodigoOpcion");
	    NombreOpcion = extras.getString("NombreOpcion");
	    OpcionPolinizacion = extras.getInt("OpcionPolinizacion");
	    OpcionCosecha= extras.getInt("OpcionCosecha");	    
	    OpcionSuccion = extras.getInt("OpcionSuccion");
	    
	    //hacemos referencia a todos los objetos de nuestra vista
	    TextView TNombreCaptura = (TextView)findViewById(R.id.OpcionCaptura);
	    TextView TCodigoEmpleado= (TextView)findViewById(R.id.CodigoEmpleado);
	    TextView TCodigoOpcion= (TextView)findViewById(R.id.codigoOpcion);
	    TextVariedad = (TextView)findViewById(R.id.TextVariedad);
	    TCantidadAsignados = (TextView)findViewById(R.id.textEmpleadosAsginados);
	    validacionesGroup = (LinearLayout)findViewById(R.id.layoutValidaciones);
	    descriptivo = (EditText)findViewById(R.id.editTextDescriptivo);
	    empleado  = (EditText)findViewById(R.id.editEmpleado);
	    Button buttonNuevo = (Button) findViewById(R.id.btnNuevo);
        Button btnBack = (Button) findViewById(R.id.btnBack);
	    Button botonGuarda = (Button) findViewById(R.id.buttonGuarda);
	    
	    
	    //hacemos las inicializaciones necesarias a nuestra vista
	    TNombreCaptura.setText(NombreOpcion+ "");
	    TCodigoEmpleado.setText(CodigoEmpleado + "");
	    TCodigoOpcion.setText(CodigoOpcion + "");
	    validoDescriptivo = false;
	    TextVariedad.setVisibility(View.INVISIBLE);
	    validacionesGroup.setVisibility(View.VISIBLE);
	    
	    //incializamos la BD asi como tambien obtenemos la referencia al singleton
	    appState = ((MyApp)getApplicationContext());   
	    db = appState.getDb();
	    db.open();	    
	    
	    //al recibir el enter se verifica si el descriptivo es correcto y lo manda al siguiente input sino le muestra un mensaje al usuario
        descriptivo.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                		VerificarDatos(descriptivo.getText().toString().trim());
	                	if (validoDescriptivo == true){
	                		descriptivo.setEnabled(false);
	                		empleado.requestFocus();
	                	}else{
		    	    		alertDialogMensaje("SIEMBRA", "Numero de Siembra Invalido");
		    	    		descriptivo.setText("");
		    	    		descriptivo.requestFocus();
	                	}
	                  return true;
                }
                return false;
            }
        });
        
        

        botonGuarda.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	        	VerificarDatos(descriptivo.getText().toString().trim());
	        	String p1 = "";
	        	pdes = descriptivo.getText().toString().trim();
	        	pflor = empleado.getText().toString().trim();
	        	if(pdes.equals(p1)){
	        		alertDialogMensaje("Mensaje", "Existen Datos Invalidos");
	        		descriptivo.requestFocus();
	        	} else if(pflor.equals(p1)){
	        		alertDialogMensaje("Mensaje", "Existen Datos Invalidos");
	        		empleado.requestFocus();
	        	} else if(validoDescriptivo == false) {
	        		alertDialogMensaje("Mensaje", "NUMERO DE SIEMBRA INVALIDO");
	        		descriptivo.setText("");
	        		empleado.setText("");
	        		descriptivo.requestFocus();
	        	}
	        	else
	        	{
	        	DisponibilidadVegetativo dtemp = new DisponibilidadVegetativo();	
        		dtemp.setSiembra(descriptivo.getText().toString().trim());
        		dtemp.setPlantas(Integer.parseInt(empleado.getText().toString().trim()));
        		
        		Calendar c = Calendar.getInstance(); 
        		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
        		String fecha = fdate.format(c.getTime());
        		SimpleDateFormat ftime = new SimpleDateFormat("HH:mm:ss");
        		String horaInicio = ftime.format(c.getTime());
        		 		
        		dtemp.setFecha(fecha);
        		dtemp.setHora(horaInicio);
        		dtemp.setActividad(CodigoOpcion);
        		dtemp.setSupervisor(CodigoEmpleado);
        		dtemp.setFinalizado(0);
        		dtemp.setInvernadero(Invernadero);	
    	
        		Log.d("disponibilidadvegetativo", dtemp.toString().trim());
        		
        		db.insetarDisponVeg(dtemp);
        		
        		//generacion archivo json
        		ObjectMapper mapper = new ObjectMapper();
        		try {
        			String nameJson ="Dv_" + dtemp.getSiembra() + "_"+dtemp.getFecha() + "_"+dtemp.getHora();
        			nameJson=nameJson.replace(":", "-");	
					mapper.writeValue(new File( Environment.getExternalStorageDirectory()+"/SIM/JsonDescarga/" + nameJson+ ".json"), dtemp);
					
					JsonFiles archivo = new JsonFiles();
					archivo.setName(nameJson+ ".json");
					archivo.setNameFolder("SinFinalizar/");
					archivo.setUpload(0);
					db.insertarJsonFile(archivo);
					
					if(!appState.getSubiendoArchivos()){
						appState.setSubiendoArchivos(true);
						new UptoDropbox().execute(getApplicationContext());
					}
	
        		} catch (JsonGenerationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JsonMappingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        		
        		empleado.setText("");         	
        		empleado.requestFocus();
        		
        		TotalEmpleadosAsignados++;
        		TCantidadAsignados.setText(TotalEmpleadosAsignados + "");       	 
	        }
	        } 
	    });       
               
	    //se limpian los campos y variables para una nueva captura
	    buttonNuevo.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	        	validacionesGroup.setVisibility(View.VISIBLE); 
				 descriptivo.setText("");
				 empleado.setText(""); 
				 TCantidadAsignados.setText("0");
				 validoDescriptivo = false;
				 TextVariedad.setVisibility(View.INVISIBLE);
				 descriptivo.setEnabled(true);
				 descriptivo.requestFocus();
				 
				 
	        }   
	    });
        
        btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
 
            	onBackPressed();
            }
        
        });
        
	}  
        
	public void alertDialogMensaje(String message1, String mesage2){
		
		//mVibrator.vibrate(300);
		
		try {
		    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
		    ringtone.play();
		    
		    
		} catch (Exception e) {
		    e.printStackTrace();
		}
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(message1);
		alertDialog.setMessage(mesage2);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int which) {
		// here you can add functions
			ringtone.stop();
		}
		});
		alertDialog.show();
	}

	public void VerificarDatos(String dato){	
		 Cursor CursorSiembra = db.getSiembraVegetativo("Siembra='" + dato + "'");
		 if(CursorSiembra.getCount()>0){
			 SiembraVegetativo s = db.getSiembraVegetativoFromCursor(CursorSiembra, 0);
			 Log.d("SIEMBRAVEGETATIVO",s.getSiembra() + ", "+s.getVariedad() + " ," + s.getPlantas() + s.getInvernadero());
			 
			 validoDescriptivo = true;
			 TextVariedad.setText(s.getVariedad());
			 TextVariedad.setVisibility(View.VISIBLE);
			 Invernadero = s.getInvernadero();
			 } else {
				 validoDescriptivo = false;
			 }
		 CursorSiembra.close();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.captura_disponibilidad, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void ButtonOnClick(View v) {
	    switch (v.getId()) {
	      case R.id.buttonborra:
		        agregarNumero("1");
	        break;
	      case R.id.btnDos:
	    	  agregarNumero("2");
	        break;
	      case R.id.btnTres:
		        agregarNumero("3");
	        break;
	      case R.id.btnCuatro:
	    	  agregarNumero("4");
	        break;
	      case R.id.btnCinco:
		        agregarNumero("5");
	        break;
	      case R.id.btnSeis:
	    	  agregarNumero("6");
	        break;
	      case R.id.btnSiete:
	    	  agregarNumero("7");
	        break;
	      case R.id.btnOcho:
		        agregarNumero("8");
	        break;
	      case R.id.btnNueve:
	    	  agregarNumero("9");
	        break;
	      case R.id.button0:
		        agregarNumero("0");
	        break;
	      case R.id.btnCero:
	    	  agregarNumero("b");
	        break;
	      }
	     
	}
	
	public void agregarNumero(String n){
		
		if(n.contains("b")){
			
			if(empleado.isFocused()){
				empleado.setText("");			
			}
			
		}else{
			if(empleado.isFocused()){
				empleado.setText(empleado.getText() + n);
			}
		}
		

	}

}