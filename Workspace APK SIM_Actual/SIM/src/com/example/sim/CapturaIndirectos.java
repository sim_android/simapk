package com.example.sim;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.os.Vibrator;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400Message;
import com.ibm.as400.access.CommandCall;
import com.ibm.as400.jtopenstubs.javabeans.PropertyVetoException;

import com.example.floricultura.R;
import com.example.sim.adapters.DatosLista;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.DUbicaciones;
import com.example.sim.data.Dempleado;
import com.example.sim.data.Dguardados;
import com.example.sim.data.Dinvernaderos;
import com.example.sim.data.EInvestigacion;
import com.example.sim.data.EMantenimiento;
import com.example.sim.data.JsonFiles;
import com.example.sim.data.Mdepartamento;
import com.example.sim.data.Opciones;
import com.example.sim.data.Semillas;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@SuppressLint("NewApi") public class CapturaIndirectos extends ActionBarActivity {
	
	//Variables globales
	private MyApp appState;	
	private DBAdapter db;
	int CodigoModulo;
	int Codigodepartamento;
	int CodigoEmpleado;
	int CodigoOpcion;
	int TotalEmpleadosAsignados;
	String NombreOpcion;
	TextView TCantidadAsignados;
	Vibrator mVibrator;
	Ringtone ringtone;
	String EmpleadoEscaneado;
	String[] datos = null;
	String[] data = null;
	EditText empleado;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_investigacion);
	
		//se obtienen lo datos de la actividad antetior por medio del intent
		Bundle extras = getIntent().getExtras();
		CodigoModulo = extras.getInt("CodigoModulo");
	    CodigoEmpleado = extras.getInt("CodigoEmpleado");
	    Codigodepartamento = extras.getInt("Codigodepartamento");
	    CodigoOpcion = extras.getInt("CodigoOpcion");
	    NombreOpcion = extras.getString("NombreOpcion");
	   
	    //hacemos referencia a todos los objetos de nuestra vista
	    TextView TNombreCaptura = (TextView)findViewById(R.id.OpcionCaptura);
	    TextView TCodigoEmpleado= (TextView)findViewById(R.id.CodigoEmpleado);
	    TextView TCodigoOpcion= (TextView)findViewById(R.id.codigoOpcion);
	    empleado = (EditText)findViewById(R.id.editEmpleado);
	    
	    TCantidadAsignados = (TextView)findViewById(R.id.textEmpleadosAsginados);
	    final Button botonNuevo = (Button) findViewById(R.id.btnNuevo);
        Button btnBack = (Button) findViewById(R.id.btnBack);
	    Button botonListado =  (Button) findViewById(R.id.btnListado);
	    	       
	    //hacemos las inicializaciones necesarias a nuestra vista
	    TNombreCaptura.setText(NombreOpcion+ "");
	    TCodigoEmpleado.setText(CodigoEmpleado + "");
	    TCodigoOpcion.setText(CodigoOpcion + "");
	    Calendar c = Calendar.getInstance(); 
		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
		String fecha = fdate.format(c.getTime());
		empleado.setInputType(InputType.TYPE_CLASS_NUMBER);   //Para Asignarle un tipo de dato 
		empleado.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
		empleado.setSingleLine(false);   // se utiliza para que el EditText no haga un Tab Automatico.
	    if (android.os.Build.VERSION.SDK_INT > 9)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
	        
	  //incializa la BD, tambien obtenemos la referencia al singleton
	    appState = ((MyApp)getApplicationContext());   
	    db = appState.getDb();
	    db.open();
	    getAsignados();
	    
	  // Comandos al Presionar el Boton Mostrar Listado
	    botonListado.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	getListado();
            	//Creando Cuadro de Dialogo para Listado de Personas
        		class DialogoSeleccion extends DialogFragment {
        	        @Override
        	        public Dialog onCreateDialog(Bundle savedInstanceState) {
        	         	  
        	            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        	    
						builder.setTitle("Listado").setItems(data, new DialogInterface.OnClickListener() {
        	                    public void onClick(DialogInterface dialog, int item) {
        	                        Log.i("Empleado", "Opci�n elegida: " + data[item]);        	                 
        	                    }
        	                });
        	            return builder.create(); }
        	    }
        		
        		FragmentManager fragmentManager = getSupportFragmentManager();
        		DialogoSeleccion dialogo = new DialogoSeleccion();
                dialogo.show(fragmentManager, "Alerta");
            }
            
        });
	        
	    // Comandos al Presionar el Boton Atras
	    btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
 
            	onBackPressed();
            }
        
        });
	
	    // Comandos al Presionar el Boton Nuevo
	    botonNuevo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	empleado.setText("");
            	empleado.requestFocus();
            }
        
        });
	    
	    //Se verfica el input empleado al persionar enter
        empleado.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	// se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                	String valida = "";
                	EmpleadoEscaneado = empleado.getText().toString().trim();
                	if (EmpleadoEscaneado.equals(valida)){
                		alertDialogMensaje("ERROR","ERROR EN EL EMPLEADO INGRESADO");
                	}else{
                	GuardarDatos();
                	}
                }
                return false;
                }	
                }) ;
   	       
	}
	
	public void alertDialogMensaje(String message1, String mesage2){
		
		//mVibrator.vibrate(300);
		
		try {
		    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
		    ringtone.play();
		    
		    
		} catch (Exception e) {
		    e.printStackTrace();
		}
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(message1);
		alertDialog.setMessage(mesage2);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int which) {
		// here you can add functions
			ringtone.stop();
		}
		});
		alertDialog.show();
	}
			
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.captura_disponibilidad, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void getListado(){
		int sup = CodigoEmpleado;
		int Opc =  CodigoOpcion;
		Cursor Casignados = null ;
		 Casignados = this.db.getDguardados("Supervisor = " + sup + " AND Actividad= " + Opc +" AND ActividadFinalizada = 0");
		 String tmp = null;
		 int j = 0;
		 data = new String[Casignados.getCount()];
		 if(Casignados.getCount()>0){
			 for(int i=0 ; i<Casignados.getCount();i++){	 
				 Dguardados g = db.getDguardadosFromCursor(Casignados, i);
				 Cursor Codigoempleado = this.db.getDempledo("CodigoEmpleado= " + g.getCodigoEmpleado().toString().trim());
				 Dempleado DE = this.db.getDempleadoFromCursor(Codigoempleado, 0);
				 if(DE != null){
					 tmp =  DE.getNombre().toString().trim() +"         "+ g.getHoraIncio().toString().trim();
				 }else{
					 tmp =  " NOMBRE NO ENCONTRADO " + "         "+ g.getHoraIncio().toString().trim();
				 }
				data[j] = tmp;
				j++;
				Codigoempleado.close();
			 	} 
		 	}
		 Casignados.close();
		}
	
	public void GuardarDatos(){
    	int existe = VerificarEmpleado(EmpleadoEscaneado);
    	if(existe == 2){
    			Dguardados dtemp = new Dguardados();
    			
    			
    			//-----------------------------------------------------------------------------------
    			String correlativo="";
    			    	//almaceno el empleado en una variable string
    			    	String getEmpleado = empleado.getText().toString().trim();
    			    	// guardo la variable que tiene empleado en un array
    			    	String[]empl = new String[]{getEmpleado};
    			    	Cursor obtenerCorrelativo = db.getCorrelativo(empl);
    			    	//recorro dicho arreglo para obtener el campo
    			    	if (obtenerCorrelativo.moveToFirst()){
    			    		do{
    			    			correlativo = obtenerCorrelativo.getString(0);
    			    		}while(obtenerCorrelativo.moveToNext());
    			    	}
    			//-------------------------------------------------------------------------
        		
        		Calendar c = Calendar.getInstance(); 
        		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
        		String fecha = fdate.format(c.getTime());
        		SimpleDateFormat ftime = new SimpleDateFormat("HH:mm:ss");
        		String horaInicio = ftime.format(c.getTime());
        		dtemp.setCodigoDepto(Codigodepartamento);
        		dtemp.setCodigoEmpleado(Integer.parseInt(EmpleadoEscaneado));
        		dtemp.setFecha(fecha);
        		dtemp.setHoraIncio(horaInicio);
        		dtemp.setActividad(CodigoOpcion);
        		dtemp.setSupervisor(CodigoEmpleado);
        		dtemp.setActividadFinalizada(0);
        		dtemp.setCorrelativoEmpleado(correlativo);
       
        		Log.d("CapturaIndirectos", dtemp.toString().trim());    		
        		db.insertarDguardados(dtemp);
        		
        		//generacion archivo json
        		ObjectMapper mapper = new ObjectMapper();
        		try {
        			String nameJson ="G_" + dtemp.getCodigoEmpleado() + "_"+dtemp.getFecha() + "_"+dtemp.getHoraIncio();
        			nameJson=nameJson.replace(":", "-");	
					mapper.writeValue(new File( Environment.getExternalStorageDirectory()+"/SIM/JsonDescarga/" + nameJson+ ".json"), dtemp);
					
					JsonFiles archivo = new JsonFiles();
					archivo.setName(nameJson+ ".json");
					archivo.setNameFolder("SinFinalizar/");
					archivo.setUpload(0);
					db.insertarJsonFile(archivo);
					
					if(!appState.getSubiendoArchivos()){
						appState.setSubiendoArchivos(true);
						new UptoDropbox().execute(getApplicationContext());
					}
					
        		
        		} catch (JsonGenerationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JsonMappingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        	       
        		empleado.setText("");
        		empleado.requestFocus();
        		TotalEmpleadosAsignados++;
        		TCantidadAsignados.setText(TotalEmpleadosAsignados + "");
        		
        		
        	}else if(existe == 1){
        		alertDialogMensaje("Asignado", "Este Empleado ya esta asignado");
        		empleado.setText("");
        		empleado.requestFocus();
        	}else if(existe == 0){
        		alertDialogMensaje("Marcaje", "Empleado no encontrado \n Descargar D_Acceso");
        		empleado.setText("");
        		empleado.requestFocus();	
    	}
	}
	
		//retornara un valor dependiendo del estado del empleado
		// 0 = no existe
		// 1 = existe pero esta asignado
		// 2 = existe y no esta asignado
	public int VerificarEmpleado(String empleado){
		int valor = 0;
		Cursor CursorDacceso =	db.getDacceso("CodigoEmpleado='" + empleado + "'");
		if(CursorDacceso != null  && CursorDacceso.getCount()>0){
		Cursor CursorGuardados =	db.getEInvestigacion("CodigoEmpleado=" + empleado + " AND ActividadFinalizada=0");
		if(CursorGuardados.getCount()>0){
			valor = 1;
			}else{
			valor = 2;
			}
		CursorGuardados.close();
		 }
		 CursorDacceso.close();
		  return valor;
		}
	
	public void getAsignados(){
		int sup = CodigoEmpleado;
		int Opc =  CodigoOpcion;
		Cursor Casignados = null ;
		 Casignados = this.db.getDguardados("Supervisor=" + sup + " AND Actividad=" + Opc +" AND ActividadFinalizada=0");
		 int j = Casignados.getCount();
		TotalEmpleadosAsignados = j;
 		TCantidadAsignados.setText(TotalEmpleadosAsignados + "");
 		Casignados.close();
		}
	
	public String format (int f){
		String val = "";
		if(f<10){
		val = "0"+Integer.toString(f);	
		}
		else{
		val = Integer.toString(f);
		}
		return val;
	}
	
}