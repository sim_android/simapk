package com.example.sim;

import java.io.File;
import java.io.IOException;
import java.sql.Array;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;

import android.R.string;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Color;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.os.Vibrator;
import android.support.v7.app.ActionBarActivity;
import android.text.Layout.Alignment;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;


//import com.ibm.as400.access.AS400;
//import com.ibm.as400.access.AS400Message;
//import com.ibm.as400.access.CommandCall;
//import com.ibm.as400.jtopenstubs.javabeans.PropertyVetoException;

import com.example.floricultura.R;
import com.example.sim.adapters.DatosLista;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.DBHelper;
import com.example.sim.data.Dazucareras;
import com.example.sim.data.Dbolsas;
import com.example.sim.data.Dguardados;
import com.example.sim.data.Filtros;
import com.example.sim.data.JsonFiles;
import com.example.sim.data.Opciones;
import com.example.sim.data.Semillas;
import com.example.sim.data.Usuario;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ControlPureza extends ActionBarActivity {
	
	//Variables globales
	private MyApp appState;	
	private DBAdapter db;
	int CodigoModulo;
	int Codigodepartamento;
	int CodigoEmpleado;
	int CodigoOpcion;
	int OpcionPolinizacion;
	int OpcionSuccion;
	int ValorPolinizacion = 0;
	int ValorSuccion=0;
	int TotalEmpleadosAsignados;
	int OpcionCosecha;
	boolean validoDescriptivo;
	boolean confirma = true;
	boolean confirma2 = true;
	String NombreOpcion;
	String Val_Bolsa;
	//String SiembraF = "";
	LinearLayout validacionesGroup;
	String Material;
	//TextView Material1;
	TextView TCantidadAsignados;
	EditText Parametro1;
	EditText Parametro2;
	EditText Parametro3;
	EditText Parametro4;
	EditText Parametro5;
	EditText descriptivo;
	EditText empleado;
	TextView TextP1;
	TextView TextP2;
	TextView TextP3; 
	TextView TextP4;
	TextView TextP5;
	
	/*
	 * 
	 * en este codigo fuente se hizo la actualizacion de agregar un nuevo
	 * parametro, el cual el codigo fuente anterior no lo tiene
	 * 
	 * el Textp5
	 * 
	 */
	Vibrator mVibrator;
	Ringtone ringtone;
	String Fecha = "";
	int Nparametros = 0;
	int TipoValP1 = 1;  // Validación por defecto 1 = Siembra
	int TipoValP2 = 1;  // Validación con 2 =  Material;  3 = Bolsa
	int TipoValP3 = 1;  // Validación con 4 =  Azucarera;  5 = Filtro
	int TipoValP4 = 1;  //
	int TipoValP5 = 1;  //
	
	
	/*
	 * 
	 * se agregaron 2 tipos de Dato enteros que son las validaciones
	 * 4 y 5 que el codigo fuente anterior no lo tiene
	 * 
	 */
	int cargar = 0;
	
	
	/*
	 * 
	 * La variable entera cargar no se encuentra en el codigo fuente
	 * anterior
	 * 
	 * 
	 */

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.control_pureza);
		
		
		//se obtienen lo datos de la actividad antetior por medio del intent
		Bundle extras = getIntent().getExtras();
		CodigoModulo = extras.getInt("CodigoModulo");
	    CodigoEmpleado = extras.getInt("CodigoEmpleado");
	    Codigodepartamento = extras.getInt("Codigodepartamento");
	    CodigoOpcion = extras.getInt("CodigoOpcion");
	    NombreOpcion = extras.getString("NombreOpcion");
  
	    //hacemos referencia a todos los objetos de nuestra vista
	    TextView TNombreCaptura = (TextView)findViewById(R.id.prueba);
	   TextView TCodigoEmpleado= (TextView)findViewById(R.id.CodigoEmpleado);
	    TextView TCodigoOpcion= (TextView)findViewById(R.id.codigoOpcion);
	    TextP1 = (TextView)findViewById(R.id.TextP1);
	    TextP2 = (TextView)findViewById(R.id.TextP2);
	    TextP3 = (TextView)findViewById(R.id.TextP3);
	    TextP4 = (TextView)findViewById(R.id.TextP4);
	    TextP5 = (TextView)findViewById(R.id.TextP5);
	    
	    /*
	     * 
	     * 
	     * se manda a traer las 2 vistas creadas para poder manipularlas en
	     * este codigo fuente que son TextP4 y TextP5, estas 2 no se
	     * encuentran en el codigo fuente anterior
	     * 
	     * 
	     */
	    
	    
	    
	    TCantidadAsignados = (TextView)findViewById(R.id.textEmpleadosAsginados);
	    validacionesGroup = (LinearLayout)findViewById(R.id.layoutValidaciones);
	    descriptivo = (EditText)findViewById(R.id.editTextDescriptivo);
	   // Material = (TextView)findViewById(R.id.textMaterial);
	   // Material1 = (TextView)findViewById(R.id.textMaterial1);
	    Parametro1  = (EditText)findViewById(R.id.editP1);
	    Parametro2  = (EditText)findViewById(R.id.editP2);
	    Parametro3  = (EditText)findViewById(R.id.editP3);
	    Parametro4  = (EditText)findViewById(R.id.editP6);
	    Parametro5  = (EditText)findViewById(R.id.editP5);
	    
	    /*
	     * 
	     * se mandan a llamar los label creados para poder manipularlos
	     * en este codigo fuente que son Parametro4 y Parametro 5, estos 2 no se
	     * encuentan en el codigo fuente anterior
	     * 
	     * 
	     */
	    
	    empleado  = (EditText)findViewById(R.id.editEmpleado);
	    Button buttonNuevo = (Button) findViewById(R.id.btnNuevo);
        Button btnBack = (Button) findViewById(R.id.btnAtras);
        Button btnInicio = (Button) findViewById(R.id.btnInicio);
	    
	    
	    //hacemos las inicializaciones necesarias a nuestra vista
	    TNombreCaptura.setText(NombreOpcion+ "");
	    TCodigoEmpleado.setText(CodigoEmpleado + "");
	    TCodigoOpcion.setText(CodigoOpcion + "");
	    
	    /*if(NombreOpcion.contains("POLINIZACION")){
	    	OpcionPolinizacion = 1;
	    } else if(NombreOpcion.contains("COSECHAS")){
	    	OpcionCosecha = 1; 
	    } else if(NombreOpcion.contains("SUCCION")){
		    OpcionSuccion = 1;
	    }*/
	    
	    validoDescriptivo = false;
	    confirma = false;
	    confirma2 = false;
	    //validacionesGroup.setVisibility(View.INVISIBLE);
	    descriptivo.requestFocus();
	    empleado.setEnabled(false);	  
	    /*Parametros*/
	    TextP1.setVisibility(View.INVISIBLE);
	    TextP2.setVisibility(View.INVISIBLE);
	    TextP3.setVisibility(View.INVISIBLE);
	    /*
	     * 
	     * 
	     * 
	     */
	    TextP4.setVisibility(View.INVISIBLE);
	    TextP5.setVisibility(View.INVISIBLE);
	    /*
	     * 
	     * 
	     * 
	     * 
	     */
	    Parametro1.setVisibility(View.INVISIBLE);
	    Parametro2.setVisibility(View.INVISIBLE);
	    Parametro3.setVisibility(View.INVISIBLE);
	    /*
	     * 
	     * 
	     * 
	     */
	    Parametro4.setVisibility(View.INVISIBLE);
	    Parametro5.setVisibility(View.INVISIBLE);
	    /*
	     * 
	     * 
	     * 
	     */
	    Parametro1.setEnabled(false);
	    Parametro2.setEnabled(false);
	    Parametro3.setEnabled(false);
	    
	    /*
	     * 
	     * 
	     * 
	     */
	    Parametro4.setEnabled(false);
	    Parametro5.setEnabled(false);
	    /*
	     * 
	     * 
	     * 
	     */
	    Parametro1.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
	    Parametro1.setSingleLine(false);   // se utiliza para que el EditText no haga un Tab Automatico.
	    Parametro2.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
	    Parametro2.setSingleLine(false);   // se utiliza para que el EditText no haga un Tab Automatico.
	    Parametro3.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
	    Parametro3.setSingleLine(false);   // se utiliza para que el EditText no haga un Tab Automatico.
	    /*
	     * 
	     * 
	     * 
	     */
	    Parametro4.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
	    Parametro4.setSingleLine(false);   // se utiliza para que el EditText no haga un Tab Automatico.
	    Parametro5.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
	    Parametro5.setSingleLine(false);   // se utiliza para que el EditText no haga un Tab Automatico.
	    /*
	     * 
	     * 
	     * 
	     */
	  
	    // Agrega CheckBox a La vista si opcion es seleccionada y agrega el texto que corresponde   
	 /*   if (OpcionSuccion == 1){
	    		checkbox1.setVisibility(View.VISIBLE);
	    } else if(OpcionCosecha == 1){
	    			checkbox1.setVisibility(View.VISIBLE);
	    			checkbox1.setText("VARIAS BOLSAS ");
	    }else{
	    	checkbox1.setVisibility(View.INVISIBLE);
	    }
	   //fin agrega checkbox
	    */
	    
	    //incializamos la BD asi como tambien obtenemos la referencia al singleton
	    appState = ((MyApp)getApplicationContext());   
	    db = appState.getDb();
	    db.open();
	    Calendar c = Calendar.getInstance(); 
		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
		Fecha = fdate.format(c.getTime());
	    VerificarParametros();
	    verificarAsginados();
	    	    
	    //al recibir el enter se verifica si el descriptivo es correcto y lo manda al siguiente input sino le muestra un mensaje al usuario
        descriptivo.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                		VerificarDatos(descriptivo.getText().toString().trim());
	                	if (validoDescriptivo == true){
	                		descriptivo.setEnabled(false);
	                		if(Nparametros > 0){
	                			Parametro1.setEnabled(true);
	                			Parametro1.requestFocus();
	                		} else {
	                			empleado.requestFocus();
	                			empleado.setEnabled(true);
	                		}
	                	}else{
		    	    		alertDialogMensaje("Descriptivo", "Descriptivo Invalido");
		    	    		descriptivo.setEnabled(true);
		    	    		descriptivo.setText("");
		    	    		descriptivo.requestFocus();
	                	}
	                	return true;  
                }     
                return false;
            }  
        });
               
	    //Se verfica el input azucarera al persionar enter
        Parametro1.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	// se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                	String texto = TextP1.getText().toString().trim().toUpperCase();
                	boolean valor = false;
                	if(TipoValP1 == 1){
                		 valor = VerificarSiembra(Parametro1.getText().toString().trim());
                	} else if(TipoValP1 == 2) {
                		 valor = VerificarMaterial(Parametro1.getText().toString().trim());
                	} else if(TipoValP1 == 3){
                		 valor = VerificarBolsa(Parametro1.getText().toString().trim());
                	} else if(TipoValP1 == 4){
               		 	 valor = VerificarAzucarera(Parametro1.getText().toString().trim());
                	} else if(TipoValP1 == 5){
                		 valor = VerificarFiltro(Parametro1.getText().toString().trim());
                	}
                	
                	if(valor == true && Nparametros > 1){
            			Parametro2.setEnabled(true);
            			Parametro2.requestFocus();
            			Parametro1.setEnabled(false);
            		}else if (valor == true && Nparametros < 2){
            			empleado.requestFocus();
            			empleado.setEnabled(true);
            			Parametro1.setEnabled(false);
            		}else{
            			alertDialogMensaje("DATO INCORRECTO","VALOR " + texto + "\n ES INCORRECTO");
            			Parametro1.setEnabled(true);
            			Parametro1.setText("");
            			Parametro1.requestFocus();
            		}
	                return true;
                }
                return false;
            }
        });
           
	    //Se verfica el input azucarera al persionar enter
        Parametro2.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	// se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                	String texto = TextP2.getText().toString().trim().toUpperCase();
                	boolean valor = false;
                	if(TipoValP2 == 1){
                		valor = VerificarSiembra(Parametro2.getText().toString().trim());
                	} else if(TipoValP2 == 2) {
                		valor = VerificarMaterial(Parametro2.getText().toString().trim());
                	} else if(TipoValP2 == 3){
                		 valor = VerificarBolsa(Parametro2.getText().toString().trim());
                	} else if(TipoValP2 == 4){
                  		 valor = VerificarAzucarera(Parametro2.getText().toString().trim());
                	} else if(TipoValP2 == 5){
               		 	 valor = VerificarFiltro(Parametro2.getText().toString().trim());
                	}
                	if(valor == true && Nparametros > 2){
            			Parametro3.setEnabled(true);
            			Parametro3.requestFocus();
            			Parametro2.setEnabled(false);
            		}else if (valor == true && Nparametros < 3){
            			empleado.requestFocus();
            			empleado.setEnabled(true);
            			Parametro2.setEnabled(false);
            		}else{
            			alertDialogMensaje("DATO INCORRECTO","VALOR " + texto + "\n ES INCORRECTO");
            			Parametro2.setEnabled(true);
            			Parametro2.setText("");
            			Parametro2.requestFocus();
            		}
	                return true;
                }
                return false;
            }
        });
        
	    //Se verfica el input azucarera al persionar enter
        Parametro3.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	// se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                	String texto = TextP3.getText().toString().trim().toUpperCase();
                	boolean valor = false;
                	if(TipoValP3 == 1){
                		valor = VerificarSiembra(Parametro3.getText().toString().trim());
                	}else if (TipoValP3 == 2) {
                		valor = VerificarMaterial(Parametro3.getText().toString().trim());
                	}else if (TipoValP3 == 3) {
                		valor = VerificarBolsa(Parametro3.getText().toString().trim());
                	} else if(TipoValP3 == 4){
                  		 valor = VerificarAzucarera(Parametro3.getText().toString().trim());
                	} else if(TipoValP3 == 5){
                		valor = VerificarFiltro(Parametro3.getText().toString().trim());
                	}
                	if(valor == true && Nparametros > 3){
            			Parametro4.setEnabled(true);
            			Parametro4.requestFocus();
            			Parametro3.setEnabled(false);
            		}else if (valor == true && Nparametros < 4){
            			empleado.requestFocus();
            			empleado.setEnabled(true);
            			Parametro3.setEnabled(false);
            		}else{
            			alertDialogMensaje("DATO INCORRECTO","VALOR " + texto + "\n ES INCORRECTO");
            			Parametro3.setEnabled(true);
            			Parametro3.setText("");
            			Parametro3.requestFocus();
            		}
	                return true;
                }
                return false;
            }
        });
        //Se verfica el input azucarera al persionar enter
        Parametro4.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	// se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                	String texto = TextP4.getText().toString().trim().toUpperCase();
                	boolean valor = false;
                	if(TipoValP4 == 1){
                		valor = VerificarSiembra(Parametro4.getText().toString().trim());
                	}else if (TipoValP4 == 2) {
                		valor = VerificarMaterial(Parametro4.getText().toString().trim());
                	}else if (TipoValP4 == 3) {
                		valor = VerificarBolsa(Parametro4.getText().toString().trim());
                	} else if(TipoValP4 == 4){
                  		 valor = VerificarAzucarera(Parametro4.getText().toString().trim());
                	} else if(TipoValP4 == 5){
                		valor = VerificarFiltro(Parametro4.getText().toString().trim());
                	}
                	if(valor == true && Nparametros > 4){
            			Parametro5.setEnabled(true);
            			Parametro5.requestFocus();
            			Parametro4.setEnabled(false);
            		}else if (valor == true && Nparametros < 5){
            			empleado.requestFocus();
            			empleado.setEnabled(true);
            			Parametro4.setEnabled(false);
            		}else{
            			alertDialogMensaje("DATO INCORRECTO","VALOR " + texto + "\n ES INCORRECTO");
            			Parametro4.setEnabled(true);
            			Parametro4.setText("");
            			Parametro4.requestFocus();
            		}
	                return true;
                }
                return false;
            }
        });
        
        //Se verfica el input azucarera al persionar enter
        Parametro5.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	// se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                	String texto = TextP5.getText().toString().trim().toUpperCase();
                	boolean valor = false;
                	if(TipoValP5 == 1){
                		valor = VerificarSiembra(Parametro5.getText().toString().trim());
                	}else if (TipoValP5 == 2) {
                		valor = VerificarMaterial(Parametro5.getText().toString().trim());
                	}else if (TipoValP5 == 3) {
                		valor = VerificarBolsa(Parametro5.getText().toString().trim());
                	} else if(TipoValP5 == 4){
                  		 valor = VerificarAzucarera(Parametro5.getText().toString().trim());
                	} else if(TipoValP5 == 5){
                		valor = VerificarFiltro(Parametro5.getText().toString().trim());
                	}
                	if(valor == true ){
            			empleado.requestFocus();
            			empleado.setEnabled(true);
            			Parametro5.setEnabled(false);
            		}else{
            			alertDialogMensaje("DATO INCORRECTO","VALOR " + texto + "\n ES INCORRECTO");
            			Parametro5.setEnabled(true);
            			Parametro5.setText("");
            			Parametro5.requestFocus();
            		}
	                return true;
                }
                return false;
            }
        });
             
	    //Se verfica el input empleado al persionar enter
        empleado.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	// se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                	GuardarDatos();
                	return true; 
                }
            return false;
        } 
    });	 
                     
	    //se limpian los campos y variables para una nueva captura
	    buttonNuevo.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	        	 // validacionesGroup.setVisibility(View.INVISIBLE);
				 Material = "";
				 descriptivo.setText("");
				 
				 Parametro1.setText("");  
				 Parametro2.setText("");
				 Parametro3.setText("");
				 Parametro4.setText("");
				 Parametro5.setText("");
				 
				 Parametro1.setEnabled(false);
				 Parametro2.setEnabled(false);
				 Parametro3.setEnabled(false);
				 Parametro4.setEnabled(false);
				 Parametro5.setEnabled(false);
				 
				 empleado.setText(""); 
				 empleado.setEnabled(false );
				 validoDescriptivo = false;
				// ValorPolinizacion = 0; 
				 descriptivo.setEnabled(true);
				descriptivo.requestFocus();
	        }
	    });
        
        btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
             	onBackPressed();
            }
        });
                
        btnInicio.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Intent intent = new Intent(getApplicationContext(), MAINACTIVITY.class);
           	 	startActivity(intent);
            }
        });
          
	}
	
	// Tipo Validacion = 1
	public boolean VerificarSiembra(String dato){
		boolean resultado = false;
		String Nsiembra = descriptivo.getText().toString().trim();
		if(dato.equals(Nsiembra)){
			resultado = true;
		} else {
			resultado = false;
		}
		return resultado;
	}
	
	
	// Tipo Validacion = 2
	public boolean VerificarMaterial(String dato){
		boolean resultado = false;
		if(dato.equals(Material)){
			resultado = true;
		}else{
			resultado = false;
		}
		return resultado;
	}
	
	
	// Tipo Validacion = 3
	public boolean VerificarBolsa(String dato){
		boolean resultado = false;
		String Nsiembra = descriptivo.getText().toString().trim();
		Cursor CursorBolsa = db.getDbolsas("Codigo='" + dato + "'");
		 if(CursorBolsa.getCount()>0){
			 Dbolsas s = db.getDbolsasFromCursor(CursorBolsa, 0);
			 String Siem = s.getSiembra().toString().trim();
			 if(Siem.equals(Nsiembra)){
				 resultado = true;
			 } else {
				 resultado = false;
			 }
		 }
		 CursorBolsa.close();
		return resultado;
	}
	
	// Tipo Validacion = 4
	public boolean VerificarAzucarera(String dato){		
		boolean resultado = false;
		String Nsiembra = descriptivo.getText().toString().trim();
		 Cursor CursorAzucarera = db.getDazucareras("Codigo='" + dato + "'");
		 if(CursorAzucarera != null  && CursorAzucarera.getCount()>0){
			 Dazucareras az = db.getDazucarerasFromCursor(CursorAzucarera, 0);		 
			 String Siem = az.getSiembra();
			 if(Siem.equals(Nsiembra)){
			 resultado = true;
			 }
		 } 
		 CursorAzucarera.close();
		 return resultado;
	}
	
	// Tipo Validacion = 5
	public boolean VerificarFiltro(String dato) {
		boolean resultado = false;
		String Nsiembra = descriptivo.getText().toString().trim();
		Cursor CursorFiltros = db.getDFiltros ("Id='" + dato + "'");
		if(CursorFiltros != null  && CursorFiltros.getCount()>0){
			Filtros f = db.getDFiltrosFromCursor(CursorFiltros, 0);
			 String SiembraF = f.getNoSiembra().toString().trim();
			 String FechaF = f.getFechaSuccion().toString().trim();
			 if(SiembraF.equals(Nsiembra) && FechaF.equals(Fecha)){
				 resultado = true;
			 }
		 }
		CursorFiltros.close();
		return resultado;
	}
			
	public void verificarAsginados(){
		Cursor CursorGuardados =	db.getDguardados("Actividad=" + CodigoOpcion + " AND ActividadFinalizada=0" );
		TotalEmpleadosAsignados = CursorGuardados.getCount();
		TCantidadAsignados.setText(TotalEmpleadosAsignados + "");	
		CursorGuardados.close(); 
	}
	
	//retornara un valor dependiendo del estado del empleado
	// 0 = no existe
	// 1 = existe pero esta asignado
	// 2 = existe y no esta asignado
	
	public int VerificarEmpleado(String empleado){
		int valor = 0;
		Cursor CursorDacceso =	db.getDacceso("CodigoEmpleado='" + empleado + "'");
		if(CursorDacceso != null  && CursorDacceso.getCount()>0){
			Cursor CursorGuardados =	db.getDguardados("CodigoEmpleado=" + empleado + " AND ActividadFinalizada=0");
			if(CursorGuardados.getCount()>0){
				valor = 1;
			}else{
				valor = 2;
			}
		}
		CursorDacceso.close();
		return valor;
	}
	
	
	public void alertDialogMensaje(String message1, String mesage2){
		
		//mVibrator.vibrate(300);
		
		try {
		    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
		    ringtone.play();
		    
		    
		} catch (Exception e) {
		    e.printStackTrace();
		}
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(message1);
		alertDialog.setMessage(mesage2);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int which) {
		// here you can add functions
			ringtone.stop();
		}
		});
		alertDialog.show();
	}
		
	public void VerificarDatos(String dato){	
		 Cursor CursorSemillas = db.getSemillas("Siembra='" + dato + "'");
		 if(CursorSemillas.getCount()>0){
			 Semillas s = db.getSemillasFromCursor(CursorSemillas, 0);
			 Log.d("SEMILLA",s.getSiembra() + ", "+s.getMaterial() + " ," + s.getPolinizacion());
		
			 /*
			 if(OpcionCosecha==1){
				 //si se entra a captura con la opcion de cosechas
					validacionesGroup.setVisibility(View.VISIBLE);
					azucar.setVisibility(View.VISIBLE);
					azucar.setText("");
					TextviewAzucarera.setText("Bolsa 1");
					bote.setVisibility(View.VISIBLE);
					bote.setText("");
					TextviewBote.setText("Bolsa 2");
					brocha.setVisibility(View.VISIBLE);
					brocha.setText("");
					TextviewBrocha.setText("Bolsa 3");
					azucar.requestFocus();
 					
				
				 
				 /*
				//agarege codigo para las bolsas de cosecha
				//Cursor CursorBolsas = db.getDbolsas("Codigo='" + dato + "'");
				Cursor CursorBolsas = db.getDbolsas("Codigo='" + azucar.getText().toString().trim() + "'");
				if (CursorBolsas.getCount()>0){
					Dbolsas bl = db.getDbolsasFromCursor(CursorBolsas,0);
					
					validoDescriptivo = true;
					 Material.setText(bl.getCodigo());
				}
				//Fin codigo para las bolsas de cosecha
				*/
				//CODIGO AGREGADO
				
		/*	 }else if(OpcionSuccion == 1){ 
				 if (checkbox1.isChecked() == true){ 
					 validacionesGroup.setVisibility(View.VISIBLE);
					 TextviewAzucarera.setText("");
					 azucar.setVisibility(View.INVISIBLE);
					 TextviewBote.setText("Pipeta");
					 bote.setEnabled(true);
					 bote.setVisibility(View.VISIBLE);
					 bote.requestFocus();
					 brocha.setVisibility(View.INVISIBLE);
					 TextviewBrocha.setText("");;
				 } else{
					 validacionesGroup.setVisibility(View.VISIBLE);
					 azucar.setEnabled(true);
					 azucar.setVisibility(View.VISIBLE);
					 TextviewAzucarera.setText("Filtro");
					 TextviewBote.setText("Pipeta");
					 bote.setEnabled(true);
					 bote.setVisibility(View.VISIBLE);
					 brocha.setVisibility(View.INVISIBLE);
					 TextviewBrocha.setText("");				 }
					
				 //FIN CODIGO AGREGADO
				
			 }else if(s.getPolinizacion() == 0 && OpcionPolinizacion==1) {
				 validacionesGroup.setVisibility(View.VISIBLE);
				 azucar.setVisibility(View.INVISIBLE);
				 //bote.setVisibility(View.INVISIBLE);
				 brocha.setVisibility(View.INVISIBLE);
				 TextviewAzucarera.setText("");
				 TextviewBrocha.setText("");
				 TextviewBote.setText("Bote");
				 ValorPolinizacion=0;
			 
			 }else if(s.getPolinizacion() == 1 && OpcionPolinizacion==1){
				 validacionesGroup.setVisibility(View.VISIBLE);				 
				 azucar.setVisibility(View.VISIBLE);
				 bote.setVisibility(View.VISIBLE);
				 brocha.setVisibility(View.VISIBLE);
				 TextviewAzucarera.setText("Azucarera");
				 TextviewBrocha.setText("Brocha");
				 TextviewBote.setText("Bote");
				 ValorPolinizacion=1;
			 }else if(s.getPolinizacion() == 2 && OpcionPolinizacion==1){
				 validacionesGroup.setVisibility(View.VISIBLE);				 
				 azucar.setVisibility(View.INVISIBLE);
				 //bote.setVisibility(View.INVISIBLE);
				 brocha.setVisibility(View.INVISIBLE);
				 TextviewAzucarera.setText("");
				 TextviewBrocha.setText("");
				 TextviewBote.setText("Pincel");
				 ValorPolinizacion=2;
			 } else if(s.getPolinizacion() == 3 && OpcionPolinizacion==1){
				 validacionesGroup.setVisibility(View.VISIBLE);				 
				 //azucar.setVisibility(View.INVISIBLE);
				 bote.setVisibility(View.INVISIBLE);
				 //brocha.setVisibility(View.INVISIBLE);
				 TextviewAzucarera.setText(" Macho ");
				 TextviewBrocha.setText(" Brocha ");
				 TextviewBote.setText("");
				 ValorPolinizacion=3;
			 }
			 
			 */
			  
			 validoDescriptivo = true;
			 Material = s.getMaterial().toString().trim();
			 
		 }else{
			 //validacionesGroup.setVisibility(View.INVISIBLE);
			 Material =  "";
			 ValorPolinizacion=0;
			 validoDescriptivo = false;
		 }
	}
	
		

	
	/*
	public void validarDescriptivo(String dato){
		Cursor CursorSemillas = db.getSemillas("Siembra='" + dato + "'");
		 if(CursorSemillas.getCount()>0){
			 Semillas s = db.getSemillasFromCursor(CursorSemillas, 0);
			 Log.d("SEMILLA",s.getSiembra() + ", "+s.getMaterial() + " ," + s.getPolinizacion());
			 
			 validoDescriptivo = true;
			 Material.setText(s.getMaterial());
		 }
	}
	*/
	
	public void VerificarParametros() {
		int contador = 0;
		Cursor COpciones = db.getOpciones("Opcion = '" + CodigoOpcion + "'");
		try{
		if(COpciones.getCount()>0){
			Opciones op = db.getOpcionesFromCursor(COpciones, 0);
			if(!op.getPureza1().substring(0, 1).equals("0")){
				contador = 1;
				if(!op.getPureza2().substring(0, 1).equals("0")){
					contador = 2;
					if(!op.getPureza3().substring(0, 1).equals("0")){
						contador = 3;
						if(!op.getPureza4().substring(0, 1).equals("0")){
							contador = 4;
							if(!op.getPureza5().substring(0, 1).equals("0")){
								contador = 5;
							}
						}
					}
				}
				COpciones.close();
			} 
			Nparametros = contador;	
		
			if(Nparametros == 5){
				TextP1.setVisibility(View.VISIBLE);
				Parametro1.setVisibility(View.VISIBLE);
				TextP2.setVisibility(View.VISIBLE);
				Parametro2.setVisibility(View.VISIBLE);
				TextP3.setVisibility(View.VISIBLE);
				Parametro3.setVisibility(View.VISIBLE);
				TextP4.setVisibility(View.VISIBLE);
				Parametro4.setVisibility(View.VISIBLE);
				TextP5.setVisibility(View.VISIBLE);
				Parametro5.setVisibility(View.VISIBLE);
				
				TextP1.setText(op.getPureza1().substring(1));
				TipoValP1 = Integer.parseInt(op.getPureza1().substring(0, 1));
				Parametro1.setText("");
				TextP2.setText(op.getPureza2().substring(1));
				TipoValP2 = Integer.parseInt(op.getPureza2().substring(0, 1));
				Parametro2.setText("");
				TextP3.setText(op.getPureza3().substring(1));
				TipoValP3 = Integer.parseInt(op.getPureza3().substring(0, 1));
				Parametro3.setText("");
				TextP4.setText(op.getPureza4().substring(1));
				TipoValP4 = Integer.parseInt(op.getPureza4().substring(0, 1));
				Parametro4.setText("");
				TextP5.setText(op.getPureza5().substring(1));
				TipoValP5 = Integer.parseInt(op.getPureza5().substring(0, 1));
				Parametro5.setText("");
				
				// editar Nparametros cuando sean iguales a 4
			} else if (Nparametros == 4){
				TextP1.setVisibility(View.VISIBLE);
				Parametro1.setVisibility(View.VISIBLE);
				TextP2.setVisibility(View.VISIBLE);
				Parametro2.setVisibility(View.VISIBLE);
				TextP3.setVisibility(View.VISIBLE);
				Parametro3.setVisibility(View.VISIBLE);
				TextP4.setVisibility(View.VISIBLE);
				Parametro4.setVisibility(View.VISIBLE);
				
				TextP1.setText(op.getPureza1().substring(1));
				TipoValP1 = Integer.parseInt(op.getPureza1().substring(0, 1));
				Parametro1.setText("");
				TextP2.setText(op.getPureza2().substring(1));
				TipoValP2 = Integer.parseInt(op.getPureza2().substring(0, 1));
				Parametro2.setText("");
				TextP3.setText(op.getPureza3().substring(1));
				TipoValP3 = Integer.parseInt(op.getPureza3().substring(0, 1));
				Parametro3.setText("");
				TextP4.setText(op.getPureza4().substring(1));
				TipoValP4 = Integer.parseInt(op.getPureza4().substring(0, 1));
				Parametro4.setText("");
			} else if (Nparametros == 3){
				TextP1.setVisibility(View.VISIBLE);
				Parametro1.setVisibility(View.VISIBLE);
				TextP2.setVisibility(View.VISIBLE);
				Parametro2.setVisibility(View.VISIBLE);
				TextP3.setVisibility(View.VISIBLE);
				Parametro3.setVisibility(View.VISIBLE);
				
				TextP1.setText(op.getPureza1().substring(1));
				TipoValP1 = Integer.parseInt(op.getPureza1().substring(0, 1));
				Parametro1.setText("");
				TextP2.setText(op.getPureza2().substring(1));
				TipoValP2 = Integer.parseInt(op.getPureza2().substring(0, 1));
				Parametro2.setText("");
				TextP3.setText(op.getPureza3().substring(1));
				TipoValP3 = Integer.parseInt(op.getPureza3().substring(0, 1));
				Parametro3.setText("");
			} else if (Nparametros == 2){
				TextP1.setVisibility(View.VISIBLE);
				Parametro1.setVisibility(View.VISIBLE);
				TextP2.setVisibility(View.VISIBLE);
				Parametro2.setVisibility(View.VISIBLE);
				
				TextP1.setText(op.getPureza1().substring(1));
				TipoValP1 = Integer.parseInt(op.getPureza1().substring(0, 1));
				Parametro1.setText("");
				TextP2.setText(op.getPureza2().substring(1));
				TipoValP2 = Integer.parseInt(op.getPureza2().substring(0, 1));
				Parametro2.setText("");
				
				
			} else if (Nparametros == 1){
				TextP1.setVisibility(View.VISIBLE);
				Parametro1.setVisibility(View.VISIBLE);
				TextP1.setText(op.getPureza1().substring(1));
				TipoValP1 = Integer.parseInt(op.getPureza1().substring(0, 1));
				Parametro1.setText("");
			} 
		} else {
			alertDialogMensaje("ERROR EN PARAMETROS"," ESTA OPCION NO TIENE \n PARAMETROS DE PUREZA DEFINIDOS ");
		}
		COpciones.close();
		}catch (Exception e) {
		    e.printStackTrace();
		    System.out.println("error");
		}
	}
	
	public void GuardarDatos(){
		String empleadoEscaneado = empleado.getText().toString().trim();
    	int existe = VerificarEmpleado( empleadoEscaneado );
        
    	if(existe == 2){
        		Dguardados dtemp = new Dguardados();
        		dtemp.setCodigoEmpleado(Integer.parseInt(empleadoEscaneado));
        		dtemp.setCodigoDepto(Codigodepartamento);
        		dtemp.setSiembra(descriptivo.getText().toString().trim());
        		
        		//-----------------------------------------------------------------------------------
        		String correlativo="";
        		    	//almaceno el empleado en una variable string
        		    	String getEmpleado = empleado.getText().toString().trim();
        		    	// guardo la variable que tiene empleado en un array
        		    	String[]empl = new String[]{getEmpleado};
        		    	Cursor obtenerCorrelativo = db.getCorrelativo(empl);
        		    	//recorro dicho arreglo para obtener el campo
        		    	if (obtenerCorrelativo.moveToFirst()){
        		    		do{
        		    			correlativo = obtenerCorrelativo.getString(0);
        		    		}while(obtenerCorrelativo.moveToNext());
        		    	}
        		//-------------------------------------------------------------------------
        		
        		Calendar c = Calendar.getInstance(); 
        		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
        		String fecha = fdate.format(c.getTime());
        		SimpleDateFormat ftime = new SimpleDateFormat("HH:mm:ss");
        		String horaInicio = ftime.format(c.getTime());
        		dtemp.setFecha(fecha);
        		dtemp.setHoraIncio(horaInicio);
        		dtemp.setActividad(CodigoOpcion);
        		dtemp.setSupervisor(CodigoEmpleado);
        		dtemp.setCorrelativoBolsa(Parametro1.getText().toString().trim());
        		dtemp.setCorrerlativoBolsa2(Parametro2.getText().toString().trim());
        		dtemp.setCorrerlativoBolsa3(Parametro3.getText().toString().trim());
        		dtemp.setCorrerlativoBolsa4(Parametro4.getText().toString().trim());
        		dtemp.setCorrerlativoBolsa5(Parametro5.getText().toString().trim());
        		dtemp.setActividadFinalizada(1);
        		dtemp.setCorrelativoEmpleado(correlativo);        		        		        	
        		Log.d("Dguardados", dtemp.toString().trim());
        		     		
        		//db.insertarDguardados(dtemp);
        		db.updateDguardados(dtemp);
        		//generacion archivo json
        		ObjectMapper mapper = new ObjectMapper();
        		try {
        			String nameJson ="CP_" + dtemp.getCodigoEmpleado() + "_"+dtemp.getFecha() + "_"+dtemp.getHoraIncio();
        			nameJson=nameJson.replace(":", "-");	
					mapper.writeValue(new File( Environment.getExternalStorageDirectory()+"/SIM/JsonDescarga/" + nameJson+ ".json"), dtemp);
					
					JsonFiles archivo = new JsonFiles();
					archivo.setName(nameJson+ ".json");
					archivo.setNameFolder("SinFinalizar/");

	
						archivo.setUpload(0);
						db.insertarJsonFile(archivo);
					
						if(!appState.getSubiendoArchivos()){
							appState.setSubiendoArchivos(true);
							new UptoDropbox().execute(getApplicationContext());
						}
						
					
										
					
				/*	
					
					
        		*/	
        		} catch (JsonGenerationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JsonMappingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        		
        		
        		empleado.setText(""); 
        		
        		Parametro1.setText("");
        		Parametro2.setText("");
        		Parametro3.setText("");
        		Parametro4.setText("");
        		Parametro5.setText("");
        		
        		Parametro1.setEnabled(false);		
        		Parametro2.setEnabled(false);
        		Parametro3.setEnabled(false); 
        		Parametro4.setEnabled(false); 
        		Parametro5.setEnabled(false); 
        		
        		empleado.setEnabled(false);
        		VerificarDatos(descriptivo.getText().toString().trim());
            	if (validoDescriptivo == true){
            		descriptivo.setEnabled(false);
            		if(Nparametros > 0){
            			Parametro1.setEnabled(true);
            			Parametro1.requestFocus();
            		} else {
            			empleado.requestFocus();
            			empleado.setEnabled(true);
            		}
            	}
        		
        		TotalEmpleadosAsignados++;
        		TCantidadAsignados.setText(TotalEmpleadosAsignados + "");
	
        		
        	}else if(existe == 1){
        		alertDialogMensaje("Asignado", "Este usuario ya ha sido asignado");
        		empleado.setText("");
        		empleado.requestFocus();
        		//VerificarDatos(descriptivo.getText().toString().trim());
        	}else if(existe == 0){
        		alertDialogMensaje("Marcaje", "No existe marcaje para este usuario");
        		empleado.setText("");
        		empleado.requestFocus();
        		 //VerificarDatos(descriptivo.getText().toString().trim());	
    	}
	}
	/*
	public void VerificarDatosbolsa(String dato){	
		
		 Cursor CursorSemillas = db.getDbolsas("Codigo='" + dato + "'");
		 if(CursorSemillas.getCount()>0){
			 Dbolsas s = db.getDbolsasFromCursor(CursorSemillas, 0);
			 Log.d("SEMILLA",s.getSiembra() + ", "+s.getMaterial() + " ," + s.getCodigo());
			 
			 if(OpcionCosecha==1){
				 //si se entra a captura con la opcion de cosechas
				validacionesGroup.setVisibility(View.VISIBLE);		
				bote.setVisibility(View.VISIBLE);
				brocha.setVisibility(View.VISIBLE);
				TextviewAzucarera.setText("Bolsa 1");
				TextviewBote.setText("Bolsa 2");
				TextviewBrocha.setText("Bolsa 3");
				//CODIGO AGREGADO
			 }else if(OpcionSuccion == 1){
				 validacionesGroup.setVisibility(View.VISIBLE);
				 TextviewAzucarera.setText("Filtro");
				 TextviewBote.setText("Pipeta");;
				 brocha.setVisibility(View.INVISIBLE);
				 TextviewBrocha.setText("");;
				 //FIN CODIGO AGREGADO
						
			 }else{
				 validacionesGroup.setVisibility(View.INVISIBLE);
				 ValorPolinizacion=0;
			 }
			 //validoDescriptivo = true;
			 Material1.setText(s.getSiembra());		 		 
		 }else{
			 validacionesGroup.setVisibility(View.VISIBLE);
			 ValorPolinizacion=0;
			 validoDescriptivo = false;
			// alertDialogMensaje("Mensaje", "EL valor es invalido");
			 
		 }
	}
	
	*/
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.captura_general, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}