package com.example.sim;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.Vibrator;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.floricultura.R;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.Modulos;
import com.example.sim.data.Opciones;
import com.example.sim.data.Semillas;

public class CorreccionDisponibilidad extends ActionBarActivity {
	
	//Variables globales
		private MyApp appState;	
		private DBAdapter db;
		int CodigoModulo;
		int Codigodepartamento;
		int CodigoEmpleado;
		int CodigoOpcion;
		long Pos;
		String Siemb = "";
		String Actividad = "";
		String Fecha = "";
		String Desc;
		String NombreOpcion;
		GridView gridview;
		GridView gridEnc;
		Vibrator mVibrator;
		Ringtone ringtone;	
		EditText Siembra;
		EditText Muestras;
		String siem;
		boolean validoDescriptivo;
		private String val;
		ArrayList<String> datos;
		ArrayAdapter<String> adapter;
		ArrayList<String> encabezado;
		ArrayAdapter<String> adapEnc;
		TextView FechaDe;
		TextView TextActividad;
		TextView Correlativo;
		String[] data = null;
		Button btnGuardar;
		Button btnNuevo;
		String tCorrelativo;
		String tMuestras;
		
		@SuppressLint("NewApi") @Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.correccion_disponibilidad);
			
			// para manejar la excepcion de permisos en el API de Android
	    	StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	        StrictMode.setThreadPolicy(policy);

			//se obtienen lo datos de la actividad antetior por medio del intent
			Bundle extras = getIntent().getExtras();
			CodigoModulo = extras.getInt("CodigoModulo");
		    CodigoEmpleado = extras.getInt("CodigoEmpleado");
		    Codigodepartamento = extras.getInt("Codigodepartamento");
		    CodigoOpcion = extras.getInt("CodigoOpcion");
		    NombreOpcion = extras.getString("NombreOpcion");
		    
		    //hacemos referencia a todos los objetos de nuestra vista
		    TextView TNombreCaptura = (TextView)findViewById(R.id.TextNombreActividad);
		    TextView TCodigoEmpleado= (TextView)findViewById(R.id.CodigoEmpleado);		
	        Button btnBack = (Button) findViewById(R.id.btnBack);
	        Button btnOK = (Button) findViewById(R.id.btnOk);
	        btnGuardar = (Button) findViewById(R.id.btnGuardar);
	        btnNuevo = (Button) findViewById(R.id.btnNuevo);
	        Button btnfechade = (Button) findViewById(R.id.buttonfechade);
	        Button botonActividad = (Button) findViewById(R.id.buttonActividad);
	        TextActividad = (TextView) findViewById(R.id.TextActividad);
	        FechaDe = (TextView) findViewById(R.id.TextFecha);
	        Siembra = (EditText) findViewById(R.id.TextSiembra);
	        gridEnc = (GridView) findViewById(R.id.gridEnc);
	        gridview = (GridView) findViewById(R.id.gridDetalle);
	        Muestras = (EditText) findViewById(R.id.EditMuestras);
	        Correlativo = (TextView) findViewById(R.id.TextCorrelativo);
	        Muestras.setInputType(InputType.TYPE_CLASS_NUMBER);   //Para Asignarle un tipo de dato 
	        Muestras.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
	        Muestras.setSingleLine(false);   // se utiliza para que el EditText no haga un Tab Automatico.
	        
	        datos = new ArrayList<String>();
	        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, datos);
	        encabezado = new ArrayList<String>();
	        adapEnc = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, encabezado);
	        
		    //hacemos las inicializaciones necesarias a nuestra vista
		    TNombreCaptura.setText(NombreOpcion+ "");
		    TCodigoEmpleado.setText(CodigoEmpleado + "");
		    btnGuardar.setEnabled(false);
		    
		    //incializamos la BD asi como tambien obtenemos la referencia al singleton
		    appState = ((MyApp)getApplicationContext());   
		    db = appState.getDb();
		    db.open();
		    gridEnc.setVisibility(View.INVISIBLE);
		    gridview.setVisibility(View.INVISIBLE);
		    Siembra.setEnabled(true);

		    btnBack.setOnClickListener(new View.OnClickListener() {
	            public void onClick(View v) {
	 
	            	onBackPressed();
	            }
	        });
		    

		    btnNuevo.setOnClickListener(new View.OnClickListener() {
	            public void onClick(View v) {
	            	btnGuardar.setEnabled(false);
	            	gridEnc.setVisibility(View.INVISIBLE);
	     		    gridview.setVisibility(View.INVISIBLE);
	        		Muestras.setText("");
	        	    Correlativo.setText("");
	            	datos.clear();
	            	adapter.clear();
	            	encabezado.clear();
	            	adapEnc.clear();
	            	Fecha = "";
	            	Siemb = "";
	            	Actividad = "";
	            	TextActividad.setText("");
	            	Siembra.setText("");
	            	FechaDe.setText("");
	            	Siembra.setEnabled(true);
	            	Siembra.requestFocus();
	            }
	        });
		    
		    
		    btnfechade.setOnClickListener(new View.OnClickListener() {
	            public void onClick(View v) {
	            	Calendar mcurrentDate=Calendar.getInstance();
	                int mYear = mcurrentDate.get(Calendar.YEAR);
	                int mMonth = mcurrentDate.get(Calendar.MONTH);
	                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

	                DatePickerDialog mDatePicker=new DatePickerDialog(CorreccionDisponibilidad.this, new OnDateSetListener() {                  
	                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
	                    	int datodia = selectedday;
	                    	String dia = format(datodia);
	                    	int datomes = selectedmonth + 1;
	                    	String mes = format(datomes);
	                    	String datoanio = Integer.toString(selectedyear);
	                    	String dato = datoanio + "-" + mes + "-" +dia;
	                    	FechaDe.setText(dato);
	                    	Fecha = FechaDe.getText().toString().trim();
	                    }
	                }
	                ,mYear, mMonth, mDay);
	                mDatePicker.setTitle("Seleccionar Fecha");                
	                mDatePicker.show();  
	            }
	            
	        });
		    
		  //al recibir el enter se verifica si el descriptivo es correcto, sino le muestra un mensaje al usuario
	        Siembra.setOnKeyListener(new View.OnKeyListener() {
	            public boolean onKey(View v, int keyCode, KeyEvent event) {
	                // se manda a llamar cuando se presione Enter
	                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
	                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
	                	VerificarDatos(Siembra.getText().toString().trim());
	                	if (validoDescriptivo == false){
	                		alertDialogMensaje("Descriptivo", "Descriptivo Invalido \n \n  Revisar Descriptivo");
	    		    	    Siembra.setText("");
	    		    	    Siembra.requestFocus();
	                	} else {
	                		Siemb = Siembra.getText().toString().trim();
	                		Siembra.setEnabled(false);
	                		Muestras.requestFocus();
	                	}
	                return true;  
	               }     
	              return false;
	            }  
	        });
		  
	     // Comandos al presionar el boton Seleccionar Actividad
		    botonActividad.setOnClickListener(new View.OnClickListener() {
		    	 public void onClick(View v) {
		    		 getActividad();
		            	//Creando Cuadro de Dialogo para Listado de Personas
		        		class DialogoSeleccion extends DialogFragment {
		        	        @Override
		        	        public Dialog onCreateDialog(Bundle savedInstanceState) {
		        	         	  
		        	            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		        	    
								builder.setTitle("Actividad").setItems(data, new DialogInterface.OnClickListener() {
		        	                    public void onClick(DialogInterface dialog, int item) {
		        	                        Log.i("Dialogos", "Opci�n elegida: " + data[item]);
		        	                        Actividad = data[item].substring(0,8);
		        	                        TextActividad.setText(data[item].substring(8));
		        	                    }
		        	                });
		        	            return builder.create(); }
		        	    }
		        		
		        		FragmentManager fragmentManager = getSupportFragmentManager();
		        		DialogoSeleccion dialogo = new DialogoSeleccion();
		                dialogo.show(fragmentManager, "Alerta");
		            }
	            
	        });
	        
		    btnOK.setOnClickListener(new View.OnClickListener() {
		    	  public void onClick(View v) {
		    		  
		    		  Siemb = Siembra.getText().toString().trim();
		    		  String Pb = "";
		    		  if(Fecha.equals(Pb)){
		    			  alertDialogMensaje("FECHA", "Debe Seleccionar Una Fecha");  
		    			  FechaDe.setText("");
		    		  } else if(Siemb.equals(Pb)){
		    			  alertDialogMensaje("DESCRIPTIVO", "Debe Ingresar Un Descriptivo");
		    			  Siembra.setText("");
		    			  Siemb = "";
		    			  Siembra.requestFocus();
		    		  } else if(Actividad.equals(Pb)){
		    			  alertDialogMensaje("ACTIVIDAD", "Debe Seleccionar La Actividad");
		    			  TextActividad.setText("");
		    			  Actividad = "";
		    		  } else {
		    			 
		    			  Cargar();
		    		  }
		    		  
		    	  }
		    	
		    });
	        	    
		    gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
	            @Override
	            public void onItemClick(AdapterView<?> parent, final View view, int position, long id){
	            			Pos =  parent.getItemIdAtPosition(position);	
	            			Muestras.setText("");
	            			Correlativo.setText("");
	            			int ifila = (int)(Pos/2);
	            			int icorre = (ifila*2); 
	            			int imuestras = (ifila*2)+1;
	            			tCorrelativo = (String) parent.getItemAtPosition(icorre);
	            			tMuestras = (String) parent.getItemAtPosition(imuestras);
	            			// Se asignan los valores a las vistas
	            			Muestras.setText(tMuestras);
	            			Correlativo.setText(tCorrelativo);
	            			btnGuardar.setEnabled(true);
		    	            }
		    	    });
		     
		    btnGuardar.setOnClickListener(new View.OnClickListener() {
	            public void onClick(View v) {
	            	                     	
	            	String Pb = "";
	            	String Muestra = Muestras.getText().toString().trim();
	            	String ID = Correlativo.getText().toString().trim();
	            	
	            	if(Muestra.equals(Pb)){
	            		alertDialogMensaje("ERROR","DEBE INGRESAR LA MUESTRA");
	            		Muestras.setText("");
	            		Muestras.requestFocus();           		
	            	}
	            	else 
	            	{
	            	try {
	            	String user = "sim";
	            	String pasword = "sim";
	            	 Connection connection = null;
	            	 int rs ;
	            	 Statement statement = null;
						Class.forName("com.ibm.as400.access.AS400JDBCDriver");
						connection = DriverManager.getConnection("jdbc:as400://190.190.190.10", user, pasword);	
						statement = connection.createStatement();			     
						rs = statement.executeUpdate(
						"UPDATE SISAPPF.APP_DISPONIBILIDADFLORES SET FLORES = '"+Muestra+"' WHERE ID = '"+ID+"'");
						
						if(rs > 0){
							Toast toast = Toast.makeText(getApplicationContext(), "Cambio Realizado.....", Toast.LENGTH_SHORT);
			            	toast.show();
			            	
			            	Cargar();
						}
						connection.close();	
						
	            	}
					catch (Exception e) {
						Toast toast = Toast.makeText(getApplicationContext(), "Problema de Conexi�n.....", Toast.LENGTH_SHORT);
		            	toast.show();
						e.printStackTrace();
	            } 
	           } 	
	            }
	        });
		    
		}    
		    		    
	public void Cargar() {
		btnGuardar.setEnabled(false);
		gridEnc.setVisibility(View.VISIBLE);
		gridview.setVisibility(View.VISIBLE);
		Muestras.setText("");
	    Correlativo.setText("");
    	datos.clear();
    	adapter.clear();
    	encabezado.clear();
    	adapEnc.clear();
    	
    	try {
    	String user = "sim";
    	String pasword = "sim";
    	 Connection connection = null;
    	 ResultSet rs = null;
    	 Statement statement = null;
    	 
			Class.forName("com.ibm.as400.access.AS400JDBCDriver");
			connection = DriverManager.getConnection("jdbc:as400://190.190.190.10", user, pasword);	
			statement = connection.createStatement();			     
			rs = statement.executeQuery(
					"SELECT ID,FLORES MUESTRAS FROM SISAPPF.APP_DISPONIBILIDADFLORES " +
					"WHERE FECHA = '"+Fecha+"' AND ACTIVIDAD = '"+Actividad+"' " +
					"AND SIEMBRA = '"+Siemb+"' AND ESTADO = 'AC'");
			
			if(rs.wasNull() == true){
				alertDialogMensaje("ERROR", "NO SE ENCONTRARON DATOS \n \n REVISAR DATOS INGRESADOS");	    						
			}else {
		    //Agregar encabezados de columnas	
			String Cn1 = rs.getMetaData().getColumnName(1);
			encabezado.add(Cn1);
			gridEnc.setAdapter(adapEnc);
			String Cn2 = rs.getMetaData().getColumnName(2);
			encabezado.add(Cn2);
			gridEnc.setAdapter(adapEnc);		    					
			// finaliza agregar encabezados de columnas	
			while (rs.next()) {
			//agrega filas de columnas
				 String d1 = rs.getString(1).trim();
				 String d2 = rs.getString(2).trim();	    						
				 String c1 = comprobar(d1);
				 String c2 = comprobar(d2);	 
				 datos.add(c1);
				 gridview.setAdapter(adapter);
				 datos.add(c2);
				 gridview.setAdapter(adapter);		    						
			}
			connection.close();	
			}
    	}
		catch (Exception e) {
			Toast toast = Toast.makeText(getApplicationContext(), "Verificar Conexion.....", Toast.LENGTH_SHORT);
        	toast.show();
			e.printStackTrace();
		} 		
	}	    
		    
	public String comprobar(String cad){
			String Pb = "";
			if(cad.equals(Pb)){
				cad = "---";
			}
			return cad;	
		}	    
		    
	public void getActividad(){
			 int modulo;
			 Cursor CModulos = this.db.getModulos("CodigoDepto = "+ Codigodepartamento + " AND Descripcion = " + "'DISPONIBILIDAD'");
			 Modulos md = db.getModulosFromCursor(CModulos, 0);
			 modulo = md.getCodigoModulo();
			 Cursor COpciones= null ;
			 COpciones = this.db.getOpciones("CodigoDepto = " + modulo);
			 Integer tmp = null;
			 String tmp2 = null;
			 int j = 0;
			 int z = COpciones.getCount();
			 data = new String[z];
			 if(COpciones.getCount()>0){
				 for(int i=0 ; i<COpciones.getCount();i++){	 
					 Opciones op = db.getOpcionesFromCursor(COpciones, i);
					 tmp = op.getOpcion();
					 tmp2 = op.getDescripcion().toString().trim();
					 if(!tmp2.contains("FINALIZA")){
						 data[j] = tmp + "  " +tmp2;
						 j++;
					 	}
				 	} 
			 	}
			 CModulos.close();
			 COpciones.close();
			}	    
		    
	public void alertDialogMensaje(String message1, String mesage2){
			
			try {
			    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
			    ringtone.play();	    	    
			} catch (Exception e) {
			    e.printStackTrace();
			}
			AlertDialog alertDialog = new AlertDialog.Builder(this).create();
			alertDialog.setTitle(message1);
			alertDialog.setMessage(mesage2);
			alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
			// here you can add functions
				ringtone.stop();
			}
			});
			alertDialog.show();
		}
		
	public String format (int f){
				val = "";
				if(f<10){
				val = "0"+Integer.toString(f);	
				}
				else{
				val = Integer.toString(f);
				}
				return val;
			}
			
	public void VerificarDatos(String dato){	
				 Cursor CursorSemillas = db.getSemillas("Siembra='" + dato + "'");
				 if(CursorSemillas.getCount()>0){
					 Semillas s = db.getSemillasFromCursor(CursorSemillas, 0);
					 Log.d("SEMILLA",s.getSiembra() + ", "+s.getMaterial() + " ," + s.getPolinizacion());
					 validoDescriptivo = true;
					 } else {
						 validoDescriptivo = false;
					 }
				 CursorSemillas.close();
			}
					
			@Override
	public boolean onCreateOptionsMenu(Menu menu) {

				// Inflate the menu; this adds items to the action bar if it is present.
				getMenuInflater().inflate(R.menu.soporteit, menu);
				return true;
			}

			@Override
	public boolean onOptionsItemSelected(MenuItem item) {
				// Handle action bar item clicks here. The action bar will
				// automatically handle clicks on the Home/Up button, so long
				// as you specify a parent activity in AndroidManifest.xml.
				int id = item.getItemId();
				if (id == R.id.action_settings) {
					return true;
				}
				return super.onOptionsItemSelected(item);
			}
			
	public void ButtonOnClick(View v) {
			    switch (v.getId()) {
			      case R.id.enviarMensaje:
				      agregarNumero("1");
			        break;
			      case R.id.btnDos:
			    	  agregarNumero("2");
			        break;
			      case R.id.btnTres:
				      agregarNumero("3");
			        break;
			      case R.id.btnCuatro:
			    	  agregarNumero("4");
			        break;
			      case R.id.btnCinco:
				      agregarNumero("5");
			        break;
			      case R.id.btnSeis:
			    	  agregarNumero("6");
			        break;
			      case R.id.btnSiete:
			    	  agregarNumero("7");
			        break;
			      case R.id.btnOcho:
				      agregarNumero("8");
			        break;
			      case R.id.btnNueve:
			    	  agregarNumero("9");
			        break;
			      case R.id.button0:
				      agregarNumero("0");
			        break;
			      case R.id.buttonBorra:
			    	  agregarNumero("b");
			        break;
			      }
			     
			}
			
	public void agregarNumero(String n){
				
				if(n.contains("b")){
					
					if(Muestras.isFocused()){
						Muestras.setText("");			
					} else if (Siembra.isFocused()){
						Siembra.setText("");
					}
					
				}else{
					if(Muestras.isFocused()){
						Muestras.setText(Muestras.getText() + n);
					}
				}
			}
	
}  
 
		    