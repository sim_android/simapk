package com.example.sim.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Dacceso {
	
	
	private String Empresa;
	private String CodigoEmpleado;
	private String CorrelativoEmpleado;
	
	@JsonProperty("EMPRESA")
	public String getEmpresa() {
		return Empresa;
	}
	public void setEmpresa(String empresa) {
		Empresa = empresa;
	}
	
	@JsonProperty("CODIGOEMPLEADO")
	public String getCodigoEmpleado() {
		return CodigoEmpleado;
	}
	public void setCodigoEmpleado(String codigoEmpleado) {
		CodigoEmpleado = codigoEmpleado;
	}
	
	@JsonProperty("CORRELATIVO")
	public String getCorrelativoEmpleado(){
		return CorrelativoEmpleado;
	}
	public void setCorrelativoEmpleado(String correlativoEmpleado){
		CorrelativoEmpleado = correlativoEmpleado;
	}
	

}
