package com.example.sim.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Usuario {

	private String Usuario;
	private String Clave;
	private String Tipo;
	private Integer CodigoEmpleado;
	private Integer CodigoDepto;
	private String CodigoAPP;
	
	public Usuario()  {
		Usuario = "";
		Clave = "";
		Tipo = "";
		CodigoEmpleado = 0;
		CodigoDepto = 0;
		CodigoAPP = "0";
	}
	
	@JsonProperty("USUARIO")
	public String getUsuario() {
		return Usuario;
	}
	public void setUsuario(String usuario) {
		Usuario = usuario;
	}
	
	@JsonProperty("CLAVE")
	public String getClave() {
		return Clave;
	}
	public void setClave(String clave) {
		Clave = clave;
	}
	
	@JsonProperty("TIPO")
	public String getTipo() {
		return Tipo;
	}
	public void setTipo(String tipo) {
		Tipo = tipo;
	}
	
	@JsonProperty("CODIGOEMPLEADO")
	public int getCodigoEmpleado() {
		return CodigoEmpleado;
	}
	public void setCodigoEmpleado(Integer codigoEmpleado) {
		CodigoEmpleado = codigoEmpleado;
	}
		
	@JsonProperty("CODIGODEPTO")
	public Integer getCodigoDepto() {
		return CodigoDepto;
	}
	public void setCodigoDepto(Integer codigoDepto) {
		CodigoDepto = codigoDepto;
	}
	
	@JsonProperty("OPCIONES_APP")
	public String getCodigoAPP() {
		return CodigoAPP;
	}
	public void setCodigoAPP(String codigoAPP) {
		CodigoAPP = codigoAPP;
	}
	
	
	@Override
	public String toString(){
		
		String Usuario = this.Usuario + " ," +this.Clave + " ," + this.Tipo + " ," +this.CodigoEmpleado+ " ," + this.CodigoDepto+ " ," +this.CodigoAPP;
		
		return Usuario;
		
	}

}
