package com.example.sim.data;

public class Dguardados {
	
		// cambie el tipo INTEGER de Siembra y de FLORES por Un STRING Para probar Limpieza Manual
	private Integer CodigoEmpleado;
	private Integer CodigoDepto;
	private Integer Actividad;
	private String Siembra;
	private String Fecha;
	private String HoraIncio;
	private String HoraFin;
	private Integer Supervisor;
	private Integer ActividadFinalizada;
	private String Invernadero;
	private String Plantas;
	private String Flores;
	private String Bancas;
	private Integer Hora;
	private Integer Min;
	private String CorrelativoBolsa;
	private String CorrelativoBolsa2;
	private String CorrelativoBolsa3;
	private String CorrelativoBolsa4;
	private String CorrelativoBolsa5;
	private String Azucarera;
	private String Filtro;
	private String PesoPolen;
	private String VolumenPolen;
	private String CorrelativoEmpleado;
	private Integer PlantaMacho;
	

	
	public Integer getPlantaMacho(){
		return PlantaMacho;
	}
	public void setPlantaMacho(Integer plantaMacho){
		PlantaMacho = plantaMacho;
	}
	
	public Integer getHora() {
		return Hora;
	}
	public void setHora(Integer hora) {
		Hora = hora;
	}
	public Integer getMin() {
		return Min;
	}
	public void setMin(Integer min) {
		Min = min;
	}
	
	public String getPlantas() {
		return Plantas;
	}
	public void setPlantas(String plantas) {
		Plantas = plantas;
	}
	
	public String getFlores() {
		return Flores;
	}
	public void setFlores(String flores) {
		Flores = flores;
	}
	
	public String getBancas() {
		return Bancas;
	}
	public void setBancas(String bancas) {
		Bancas = bancas;
	}
	
	
	public String getInvernadero() {
		return Invernadero;
	}
	public void setInvernadero(String invernadero) {
		Invernadero = invernadero;
	}
	public Integer getActividadFinalizada() {
		return ActividadFinalizada;
	}
	public void setActividadFinalizada(Integer actividadFinalizada) {
		ActividadFinalizada = actividadFinalizada;
	}
	public Integer getSupervisor() {
		return Supervisor;
	}
	public void setSupervisor(Integer supervisor) {
		Supervisor = supervisor;
	}
	public Integer getCodigoEmpleado() {
		return CodigoEmpleado;
	}
	public void setCodigoEmpleado(Integer codigoEmpleado) {
		CodigoEmpleado = codigoEmpleado;
	}
	public Integer getCodigoDepto() {
		return CodigoDepto;
	}
	public void setCodigoDepto(Integer codigoDepto) {
		CodigoDepto = codigoDepto;
	}
	public Integer getActividad() {
		return Actividad;
	}
	public void setActividad(Integer actividad) {
		Actividad = actividad;
	}
	public String getSiembra() {
		return Siembra;
	}
	public void setSiembra(String siembra) {
		Siembra = siembra;
	}
	public String getFecha() {
		return Fecha;
	}
	public void setFecha(String fecha) {
		this.Fecha = fecha;
	}
	public String getHoraIncio() {
		return HoraIncio;
	}
	public void setHoraIncio(String horaIncio) {
		HoraIncio = horaIncio;
	}
	public String getHoraFin() {
		return HoraFin;
	}
	public void setHoraFin(String horaFin) {
		HoraFin = horaFin;
	}
	
	public String getCorrelativoBolsa() {
		return CorrelativoBolsa;
	}
	public void setCorrelativoBolsa(String correlativobolsa) {
		CorrelativoBolsa = correlativobolsa;
	}
	
	public String getCorrelativoBolsa2() {
		return CorrelativoBolsa2;
	}
	public void setCorrerlativoBolsa2(String correlativobolsa2) {
		CorrelativoBolsa2 = correlativobolsa2;
	}
	
	public String getCorrelativoBolsa3() {
		return CorrelativoBolsa3;
	}
	public void setCorrerlativoBolsa3(String correlativobolsa3) {
		CorrelativoBolsa3 = correlativobolsa3;
	}
	public String getCorrelativoBolsa4() {
		return CorrelativoBolsa4;
	}
	public void setCorrerlativoBolsa4(String correlativobolsa4) {
		CorrelativoBolsa4 = correlativobolsa4;
	}
	public String getCorrelativoBolsa5() {
		return CorrelativoBolsa5;
	}
	public void setCorrerlativoBolsa5(String correlativobolsa5) {
		CorrelativoBolsa5 = correlativobolsa5;
	}
	public String getAzucarera() {
		return Azucarera;
	}
	public void setAzucarera(String azucarera) {
		Azucarera = azucarera;
	}
	
	public String getFiltro() {
		return Filtro;
	}
	public void setFiltro(String filtro) {
		Filtro = filtro;
	}
	
	public String getPesoPolen() {
		return PesoPolen;
	}
	public void setPesoPolen(String pesopolen) {
		PesoPolen = pesopolen;
	}
	
	public String getVolumenPolen() {
		return VolumenPolen;
	}
	public void setVolumenPolen(String volumenpolen) {
		VolumenPolen = volumenpolen;
	}
	public String getCorrelativoEmpleado() {
		return CorrelativoEmpleado;
	}
	public void setCorrelativoEmpleado(String correlativoEmpleado) {
		CorrelativoEmpleado = correlativoEmpleado;
	}
	
	
	
	
	public String toString(){
		String s ="";
				
		s = this.CodigoEmpleado + "\n" + this.CodigoDepto + "\n" + this.Actividad + "\n" + this.Siembra + "\n" + this.Azucarera 
				+"\n" + this.Fecha + "\n" + this.HoraIncio + "\n" + this.HoraFin + "\n" + this.Supervisor + "\n" + this.ActividadFinalizada
				+ "\n" + this.Invernadero + "\n" + this.Plantas +"\n" +this.PlantaMacho + "\n" + this.Flores + "\n" + this.Bancas + "\n" + this.Hora + "\n" + this.Min
				+ "\n" + this.CorrelativoBolsa + "\n" + this.CorrelativoBolsa2 + "\n" + this.CorrelativoBolsa3 + "\n" + this.CorrelativoBolsa4 + "\n" + this.CorrelativoBolsa5 
				+ "\n" + this.Filtro 
				+ "\n" + this.PesoPolen + "\n"  + this.VolumenPolen
				+this.CorrelativoEmpleado + "\n";
		
		return s;
	}
	

}
