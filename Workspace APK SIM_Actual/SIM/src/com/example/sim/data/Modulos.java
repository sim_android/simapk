package com.example.sim.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Modulos {
	
	private Integer CodigoDepto;
	private Integer CodigoModulo;
	private String Descripcion;
	private Integer Prioridad;
	private Integer Familia;
	private String CodigoAPP;
	private String Area;
		
	public Modulos(Integer codigoDepto, Integer codigoModulo,
			String descripcion, Integer prioridad, Integer familia, String codigoAPP, String area) {
		CodigoDepto = codigoDepto;
		CodigoModulo = codigoModulo;
		Descripcion = descripcion;
		Prioridad = prioridad;
		Familia = familia;
		CodigoAPP = codigoAPP;
		Area = area;
		
	}
	
	public Modulos() {
		CodigoDepto = 0;
		CodigoModulo = 0;
		Descripcion = "";
		Prioridad = 0;
		Familia = 0;
		CodigoAPP = "";
		Area = "";
	}
	
	
	
	@JsonProperty("CODIGO_DEPTO")
	public Integer getCodigoDepto() {
		return CodigoDepto;
	}
	public void setCodigoDepto(Integer codigoDepto) {
		CodigoDepto = codigoDepto;
	}
	
	@JsonProperty("CODIGO_MODULO")
	public Integer getCodigoModulo() {
		return CodigoModulo;
	}
	public void setCodigoModulo(Integer codigoModulo) {
		CodigoModulo = codigoModulo;
	}
	
	@JsonProperty("DESCRIPCION")
	public String getDescripcion() {
		return Descripcion;
	}
	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}
	
	@JsonProperty("PRIORIDAD")
	public Integer getPrioridad() {
		return Prioridad;
	}
	public void setPrioridad(Integer prioridad) {
		Prioridad = prioridad;
	}
	
	@JsonProperty("COD_FAMILIA")
	public Integer getFamilia() {
		return Familia;
	}
	public void setFamilia(Integer familia) {
		Familia = familia;
	}
	
	@JsonProperty("OPCIONES_APP")
	public String getCodigoAPP() {
		return CodigoAPP;
	}
	public void setCodigoAPP(String codigoAPP) {
		CodigoAPP = codigoAPP;
	}
	
	@JsonProperty("AREA")
	public String getArea() {
		return Area;
	}
	public void setArea(String area) {
		Area = area;
	}

}
