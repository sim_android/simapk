package com.example.sim.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Dbolsas {
	
	private String Empresa;
	private Integer Codigo;
	private String Material;
	private String Siembra;
	private String FechaCosecha;
	
	@JsonProperty("EMPRESA")
	public String getEmpresa() {
		return Empresa;
	}
	public void setEmpresa(String empresa) {
		Empresa = empresa;
	}

	@JsonProperty("CODIGO")
	public Integer getCodigo() {
		return Codigo;
	}
	public void setCodigo(Integer codigo) {
		Codigo = codigo;
	}
	@JsonProperty("MATERIAL")
	public String getMaterial() {
		return Material;
	}
	public void setMaterial(String material) {
		Material = material;
	}
	@JsonProperty("SIEMBRA")
	public String getSiembra() {
		return Siembra;
	}
	public void setSiembra(String siembra) {
		Siembra = siembra;
	}
	@JsonProperty("FECHACOSECHA")
	public String getFechaCosecha() {
		return FechaCosecha;
	}
	public void setFechaCosecha(String fechacosecha) {
		FechaCosecha = fechacosecha;
	}
		
		
}
