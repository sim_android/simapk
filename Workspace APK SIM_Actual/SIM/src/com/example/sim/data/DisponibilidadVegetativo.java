package com.example.sim.data;

public class DisponibilidadVegetativo {
	
	private String Siembra;
	private Integer Plantas;
	private String Fecha;
	private String Hora;
	private Integer Finalizado;
	private Integer Supervisor;
	private Integer Actividad;
	private String Invernadero;
	
	
	public String getSiembra() {
		return Siembra;
	}
	public void setSiembra(String siembra) {
		this.Siembra = siembra;
	}
	
	public Integer getPlantas() {
		return Plantas;
	}
	public void setPlantas(int i) {
		this.Plantas = i;
	}
	
	public String getFecha() {
		return Fecha;
	}
	public void setFecha(String fecha) {
		this.Fecha = fecha;
	}
		
	public String getHora() {
		return Hora;
	}
	public void setHora(String hora) {
		this.Hora = hora;
	}
	
	public Integer getFinalizado() {
		return Finalizado;
	}
	public void setFinalizado(Integer finalizado) {
		this.Finalizado = finalizado;
	}
	
	public Integer getSupervisor() {
		return Supervisor;
	}
	public void setSupervisor(Integer supervisor) {
		Supervisor = supervisor;
	}
	
	public Integer getActividad() {
		return Actividad;
	}
	public void setActividad(Integer actividad) {
		this.Actividad = actividad;
	}
	
	public String getInvernadero() {
		return Invernadero;
	}
	public void setInvernadero(String invernadero) {
		this.Invernadero = invernadero;
	}	

	public String toString(){
		String Dv ="";
				
		Dv = this.Siembra + "\n" + this.Plantas + "\n" + this.Fecha + "\n" + this.Hora + "\n" + this.Finalizado 
				+"\n" + this.Supervisor + "\n" + this.Actividad + "\n" + this.Invernadero;
		
		return Dv;
	}
	

}
