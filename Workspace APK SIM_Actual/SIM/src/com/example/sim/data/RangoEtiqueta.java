package com.example.sim.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RangoEtiqueta {
	
	private Integer inicio;
	private Integer fin;
	
	@JsonProperty("DEL")
	public Integer getInicio() {
		return inicio;
	}
	public void setInicio(Integer inicio) {
		this.inicio = inicio;
	}
	
	@JsonProperty("AL")
	public Integer getFin() {
		return fin;
	}
	public void setFin(Integer fin) {
		this.fin = fin;
	}
	
	

}
