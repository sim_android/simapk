package com.example.sim.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SiembraVegetativo {

	String Siembra;
	String Variedad;
	Integer Plantas;
	String Invernadero;
	
	
	
	public SiembraVegetativo() {
		Siembra = "";
		Variedad = "";
		Plantas = 0;
	}
	
	public SiembraVegetativo (String siembra, String variedad, Integer plantas) {
		Siembra = siembra;
		Variedad = variedad;
		Plantas = plantas;
	}
	
	@JsonProperty("SIEMBRA")
	public String getSiembra() {
		return Siembra;
	}
	public void setSiembra(String siembra) {
		Siembra = siembra;
	}
	
	@JsonProperty("VARIEDAD")
	public String getVariedad() {
		return Variedad;
	}
	public void setVariedad(String variedad) {
		Variedad = variedad;
	}
	
	@JsonProperty("PLANTAS")
	public Integer getPlantas() {
		return Plantas;
	}
	public void setPlantas(Integer plantas) {
		Plantas = plantas;
	}
	
	@JsonProperty("INVERNADERO")
	public String getInvernadero() {
		return Invernadero;
	}
	public void setInvernadero(String invernadero) {
		Invernadero = invernadero;
	}
}
