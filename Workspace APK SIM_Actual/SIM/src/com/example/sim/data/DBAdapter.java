package com.example.sim.data;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.util.Log;

public class DBAdapter {
	
	private SQLiteDatabase database;
	private DBHelper dbHelper;
	public static int TIPO_USUARIO = 1;
	public static int TIPO_DISPONSEM = 2;
	public static int TIPO_MDEPARTAMENTO = 3;
	public static int TIPO_MODULOS = 4;
	public static int TIPO_OPCIONES = 5;
	public static int TIPO_SEMILLAS = 6;
	public static int TIPO_DACCESO = 7;
	public static int TIPO_DGUARDADOS = 8;
	public static int TIPO_ETIQUETAS = 9;
	public static int TIPO_JSONFILES = 10;
	public static int TIPO_DEMPLEADO = 11;
	public static int TIPO_DBOLSAS = 12;
	public static int TIPO_DINVERNADERO = 13;
	public static int TIPO_DSIEMBRAVEGETATIVO = 14;
	public static int TIPO_DAZUCARERAS = 15;
	public static int TIPO_DISPONVEG = 16;
	public static int TIPO_DFILTROS = 17;
	public static int TIPO_SEMILLASACOND = 18;
	public static int TIPO_QCPOLEN = 19;
	public static int TIPO_DFAMILIAS = 20;
	public static int TIPO_MANTENIMIENTO = 21;
	public static int TIPO_RIEGO = 22;
	public static int TIPO_DUBICACIONES = 23;
	public static int TIPO_INVESTIGACION = 24;
	
	public DBAdapter(Context context) {
		dbHelper = new DBHelper(context);
	}
	
	//abrir una conexion a la base de datos
	public synchronized void open() throws SQLException {
			try {
				database = dbHelper.getWritableDatabase();
			} catch (SQLiteException ex) {
				try {
					Log.e("error", "DB abierta RO " + ex.getMessage());
					database = dbHelper.getReadableDatabase();				
				} catch (SQLiteException ex1) {
					Log.e("error", "Imposible abrir la base de datos " + ex1.getMessage());
				}
			}			
	}
	
	
	public void ReinicarDB(){
		dbHelper.onUpgrade(database, 0, 1);
	}
	
	public void ReiniciarTabla(int tipoTabla){
		
		switch(tipoTabla){
		
		case 1:
			dbHelper.NuevaTabla(database, dbHelper.TABLE_USUARIO);
			break;
		case 2:
			dbHelper.NuevaTabla(database, dbHelper.TABLE_DISPONSEM);
			break;
		case 3:
			dbHelper.NuevaTabla(database, dbHelper.TABLE_MDEPARTAMENTO);
			break;
		case 4:
			dbHelper.NuevaTabla(database, dbHelper.TABLE_MODULOS);
			break;
		case 5:
			dbHelper.NuevaTabla(database, dbHelper.TABLE_OPCIONES);
			break;
		case 6:
			dbHelper.NuevaTabla(database, dbHelper.TABLE_SEMILLAS);
			break;
		case 7:
			dbHelper.NuevaTabla(database, dbHelper.TABLE_DACCESO);
			break;
		case 8:
			dbHelper.NuevaTabla(database, dbHelper.TABLE_DGUARDADOS);
			break;
		case 9:
			dbHelper.NuevaTabla(database, dbHelper.TABLE_ETIQUETAS);
			break;
		case 10:
			dbHelper.NuevaTabla(database, dbHelper.TABLE_JSONFILES);
			break;
		case 11:
			dbHelper.NuevaTabla(database, dbHelper.TABLE_DEMPLEADO);
			break;
		case 12:
			dbHelper.NuevaTabla(database, dbHelper.TABLE_DBOLSAS);
			break;
		case 13:
			dbHelper.NuevaTabla(database, dbHelper.TABLE_DINVERNADERO);
			break;
		case 14:
			dbHelper.NuevaTabla(database, dbHelper.TABLE_DSIEMBRAVEGETATIVO);
			break;
		case 15:
			dbHelper.NuevaTabla(database, dbHelper.TABLE_DAZUCARERAS);
			break;
		case 16:
			dbHelper.NuevaTabla(database, dbHelper.TABLE_DISPONVEG);
			break;
		case 17:
			dbHelper.NuevaTabla(database, dbHelper.TABLE_DFILTROS);
			break;
		case 18:
			dbHelper.NuevaTabla(database, dbHelper.TABLE_SEMILLAS);
			break;
		case 19:
			dbHelper.NuevaTabla(database, dbHelper.TABLE_QCPOLEN);
			break;
		case 20:
			dbHelper.NuevaTabla(database, dbHelper.TABLE_DFAMILIAS);
			break;
		case 21:
			dbHelper.NuevaTabla(database, dbHelper.TABLE_MANTENIMIENTO);
			break;
		case 22:
			dbHelper.NuevaTabla(database, dbHelper.TABLE_RIEGO);
			break;
		case 23:
			dbHelper.NuevaTabla(database, dbHelper.TABLE_DUBICACIONES);
			break;
		case 24:
			dbHelper.NuevaTabla(database, dbHelper.TABLE_INVESTIGACION);
			break;
		default:
			break;
		}
		
	}
	
	//cerrar conexion a la base de datos
	public void close() {
		try {
			if (database.isOpen()) {
				database.close();
			}
			if (dbHelper != null) {
				dbHelper.close();	
			}
		} catch (Exception e){}
	}
	
	//------------------ Acciones a la BD para Elementos -----------------------
			//obtener todos los datos de la tabla  dguardados de acuerdo a una hora inicio y una hora final 
public synchronized Cursor getDGuardadosF(String horaInicio,String horaFin) {
		
		if (!database.isOpen()) {
			this.open();
		}
		String[] columnas = new String[]{"Actividad","Hora","Muestra","Siembra","Supervisor"};
		
		Cursor _data = new MatrixCursor(new String[]{""});
		try {
			//_data = database.query(, getKeysFromClass(DisponibilidadSemillas.class), 
								   //whereClause, null, null, null, null);
			
			//_data = database.query(DBHelper.TABLE_DISPONSEM, columnas, "Fecha=?", whereClause, null, null, null);
			_data = database.rawQuery("SELECT *FROM dguardados where horaIncio=? AND horaFin=?", new String[] {horaInicio, horaFin});
		

		
		} catch (Exception e) {
			Log.e("ErrorIngetElements", e.toString().trim());
		}
			
		return _data;
	}
//------------------ Acciones a la BD para Elementos -----------------------
		//se obtienen el promedio de semillas localmente


public synchronized Cursor getDatos(String[] whereClause) {
	
	if (!database.isOpen()) {
		this.open();
	}
	String[] columnas = new String[]{"Actividad","Hora","Muestra","Siembra","Supervisor"};
	
	Cursor _data = new MatrixCursor(new String[]{""});
	try {
		//_data = database.query(, getKeysFromClass(DisponibilidadSemillas.class), 
							   //whereClause, null, null, null, null);
		
		//_data = database.query(DBHelper.TABLE_DISPONSEM, columnas, "Fecha=?", whereClause, null, null, null);
		_data = database.rawQuery("SELECT o.Descripcion,s.Material,d.Siembra,s.Plantas,avg(d.Muestra) as DISP, (s.Plantas*avg(d.Muestra)) as FLOR_DISP from dispsemillas d inner join semillas s on s.Siembra = d.Siembra inner join opciones o on o.Opcion=d.Actividad where Fecha=? group by o.Descripcion,d.Siembra", whereClause);

	
	} catch (Exception e) {
		Log.e("ErrorIngetElements", e.toString().trim());
	}
		
	return _data;
}
//------------------ Acciones a la BD para Elementos -----------------------
		//inse obtiene los datos de acuerdo a una fecha


public synchronized Cursor getDguardados(String[] whereClause) {
	
	if (!database.isOpen()) {
		this.open();
	}
	Cursor _data = new MatrixCursor(new String[]{""});
	try {
		//_data = database.query(, getKeysFromClass(DisponibilidadSemillas.class), 
							   //whereClause, null, null, null, null);
		
		//_data = database.query(DBHelper.TABLE_DISPONSEM, columnas, "Fecha=?", whereClause, null, null, null);
		//_data = database.rawQuery("SELECT Actividad,Descripcion, avg(Muestra) as Promedio, Siembra from dispsemillas INNER JOIN opciones ON opciones.Opcion = dispsemillas.Actividad where Fecha=? group by Actividad", whereClause);
		_data = database.rawQuery("SELECT Nombre,Siembra,Descripcion as Actividad,HoraIncio,HoraFin,Plantas FROM dguardados inner join opciones ON opciones.Opcion = dguardados.Actividad INNER JOIN dempleado ON dempleado.CodigoEmpleado=dguardados.CodigoEmpleado WHERE	Fecha=? AND ActividadFinalizada=1 group by Descripcion order by Nombre,Siembra desc", whereClause);
		
	} catch (Exception e) {
		Log.e("ErrorIngetElements", e.toString().trim());
	}
		
	return _data;
}



//------------------ Acciones a la BD para Elementos -----------------------
		//inserta objetos tipo Elementos a la BD
			public synchronized long insertarUsuario(Usuario u) {	   		
		ContentValues newUsuario = buildFromObject(u,TIPO_USUARIO);
		
		if (!database.isOpen()) {
			open();
		}		
		long rowId = -1;
		try {
			rowId = database.insert(DBHelper.TABLE_USUARIO, null, newUsuario);
			Log.d("rowId", rowId + "");
		} catch (Exception e){
			Log.e("InsertErrorDBApder", e.toString().trim());
		}
		Log.d("return", rowId + "");
		return rowId;		
	}
			
			
			
			
			//
			//insertar datos a tabla Usuario
		public synchronized long insertUsuarios(String Clave,String CodigoAPP, String CodigoDepto,String CodigoEmpleado, String Tipo,String Usuario) {
			//
			
				if (!database.isOpen()) {
					this.open();
				}
				long rowId = -1;
				try {
					ResultSet rs;
					ContentValues nuevoRegistro = new ContentValues();
					nuevoRegistro.put("Clave", Clave);
					nuevoRegistro.put("CodigoAPP",CodigoAPP);
					nuevoRegistro.put("CodigoDepto",CodigoDepto);
					nuevoRegistro.put("CodigoEmpleado",CodigoEmpleado);
					nuevoRegistro.put("Tipo",Tipo);
					nuevoRegistro.put("Usuario",Usuario);
					

					database.insert("usuario", null, nuevoRegistro);
					//String sql = "insert into " +"usuario"+ " (" +"Clave"+ ", " +"CodigoAPP"+ ","+"CodigoDepto"+ ","+"CodigoEmpleado"+ ","+"Tipo"+","+"Usuario"+ ") values(" +d+")";
					//database.execSQL(sql);
					
					
					//_data = database.rawQuery(sql, null);
					//_data = database.rawQuery("SELECT Nombre,Siembra,Descripcion as Actividad,HoraIncio,HoraFin,Plantas FROM dguardados inner join opciones ON opciones.Opcion = dguardados.Actividad INNER JOIN dempleado ON dempleado.CodigoEmpleado=dguardados.CodigoEmpleado WHERE	Fecha=? AND ActividadFinalizada=1 group by Descripcion order by Nombre,Siembra desc", whereClause);
					
				} catch (Exception e) {
					Log.e("ErrorIngetElements", e.toString().trim());
				}
					
				return rowId;
			}
		
		// insertar para Modulos
		
		public synchronized long insertModulos(String Area,String CodigoAPP, String CodigoDepto,String CodigoModulo, String Descripcion,String Familia,String Prioridad) {
			
			if (!database.isOpen()) {
				this.open();
			}
			long rowId = -1;
			try {
				ContentValues nuevoRegistro = new ContentValues();
				nuevoRegistro.put("Area", Area);
				nuevoRegistro.put("CodigoAPP",CodigoAPP);
				nuevoRegistro.put("CodigoDepto",CodigoDepto);
				nuevoRegistro.put("CodigoModulo",CodigoModulo);
				nuevoRegistro.put("Descripcion",Descripcion);
				nuevoRegistro.put("Familia",Familia);
				nuevoRegistro.put("Prioridad",Prioridad);
				
				database.insert("modulos", null, nuevoRegistro);
				//_data = database.rawQuery(sql, null);
				//_data = database.rawQuery("SELECT Nombre,Siembra,Descripcion as Actividad,HoraIncio,HoraFin,Plantas FROM dguardados inner join opciones ON opciones.Opcion = dguardados.Actividad INNER JOIN dempleado ON dempleado.CodigoEmpleado=dguardados.CodigoEmpleado WHERE	Fecha=? AND ActividadFinalizada=1 group by Descripcion order by Nombre,Siembra desc", whereClause);
				
			} catch (Exception e) {
				Log.e("ErrorIngetElements", e.toString().trim());
			}
				
			return rowId;
		}
		// insertar opciones
		
		public synchronized long insertOpciones(String CodigoAPP, String CodigoDepto, String Descripcion, String FinalizaActividad, String Menu, String Opcion, String Parametro, String Parametro2, String Parametro3, String Parametro4, String Parametro5, String Poliza, String Prioridad, String Pureza1, String Pureza2, String Pureza3,String Pureza4, String Pureza5, String Tipo) {
			
			if (!database.isOpen()) {
				this.open();
			}
			long rowId = -1;
			try {
				ContentValues nuevoRegistro = new ContentValues();
				nuevoRegistro.put("CodigoAPP", CodigoAPP);
				nuevoRegistro.put("CodigoDepto",CodigoDepto);
				nuevoRegistro.put("Descripcion",Descripcion);
				nuevoRegistro.put("FinalizaActividad",FinalizaActividad);
				nuevoRegistro.put("Menu",Menu);
				nuevoRegistro.put("Opcion",Opcion);
				nuevoRegistro.put("Parametro",Parametro);
				nuevoRegistro.put("Parametro2", Parametro2);
				nuevoRegistro.put("Parametro3",Parametro3);
				nuevoRegistro.put("Parametro4",Parametro4);
				nuevoRegistro.put("Parametro5",Parametro5);
				nuevoRegistro.put("Poliza",Poliza);
				nuevoRegistro.put("Prioridad",Prioridad);
				nuevoRegistro.put("Pureza1",Pureza1);
				nuevoRegistro.put("Pureza2",Pureza2);
				nuevoRegistro.put("Pureza3",Pureza3);
				nuevoRegistro.put("Pureza4",Pureza4);
				nuevoRegistro.put("Pureza5",Pureza5);
				nuevoRegistro.put("Tipo",Tipo);
				
				
				database.insert("opciones", null, nuevoRegistro);
				//_data = database.rawQuery(sql, null);
				//_data = database.rawQuery("SELECT Nombre,Siembra,Descripcion as Actividad,HoraIncio,HoraFin,Plantas FROM dguardados inner join opciones ON opciones.Opcion = dguardados.Actividad INNER JOIN dempleado ON dempleado.CodigoEmpleado=dguardados.CodigoEmpleado WHERE	Fecha=? AND ActividadFinalizada=1 group by Descripcion order by Nombre,Siembra desc", whereClause);
				
			} catch (Exception e) {
				Log.e("ErrorIngetElements", e.toString().trim());
			}
				
			return rowId;
		}
		//insertar mdepartamento
public synchronized long insertDepartamento(String CodigoDepto,String DescripcionDepto,String Menu) {
			
			if (!database.isOpen()) {
				this.open();
			}
			long rowId = -1;
			try {
				ContentValues nuevoRegistro = new ContentValues();
				nuevoRegistro.put("CodigoDepto", CodigoDepto);
				nuevoRegistro.put("DescripcionDepto", DescripcionDepto);
				nuevoRegistro.put("Menu", Menu);
				
				
				
				database.insert("mdepartamento", null, nuevoRegistro);
				//_data = database.rawQuery(sql, null);
				//_data = database.rawQuery("SELECT Nombre,Siembra,Descripcion as Actividad,HoraIncio,HoraFin,Plantas FROM dguardados inner join opciones ON opciones.Opcion = dguardados.Actividad INNER JOIN dempleado ON dempleado.CodigoEmpleado=dguardados.CodigoEmpleado WHERE	Fecha=? AND ActividadFinalizada=1 group by Descripcion order by Nombre,Siembra desc", whereClause);
				
			} catch (Exception e) {
				Log.e("ErrorIngetElements", e.toString().trim());
			}
				
			return rowId;
		}
			//insertar para D Acceso
		public synchronized long insertDAceso(String Empresa,String CodigoEmpleado) {
			
			if (!database.isOpen()) {
				this.open();
			}
			long rowId = -1;
			try {
				ContentValues nuevoRegistro = new ContentValues();
				nuevoRegistro.put("CodigoEmpleado",CodigoEmpleado);
				nuevoRegistro.put("Empresa", Empresa);

				database.insert("dacceso", null, nuevoRegistro);
				//_data = database.rawQuery(sql, null);
				//_data = database.rawQuery("SELECT Nombre,Siembra,Descripcion as Actividad,HoraIncio,HoraFin,Plantas FROM dguardados inner join opciones ON opciones.Opcion = dguardados.Actividad INNER JOIN dempleado ON dempleado.CodigoEmpleado=dguardados.CodigoEmpleado WHERE	Fecha=? AND ActividadFinalizada=1 group by Descripcion order by Nombre,Siembra desc", whereClause);
				
			} catch (Exception e) {
				Log.e("ErrorIngetElements", e.toString().trim());
			}
				
			return rowId;
		}
		// insertar semillas
		public synchronized long insertSemillas(String MCosecha,String Material,String Plantas,String Polinizacion,String Postura,String Produccion,String Siembra) {
			
			if (!database.isOpen()) {
				this.open();
			}
			long rowId = -1;
			try {
				ContentValues nuevoRegistro = new ContentValues();
				nuevoRegistro.put("MCosecha", MCosecha);
				nuevoRegistro.put("Material",Material);
				nuevoRegistro.put("Plantas",Plantas);
				nuevoRegistro.put("Polinizacion",Polinizacion);
				nuevoRegistro.put("Postura",Postura);
				nuevoRegistro.put("Produccion",Produccion);
				nuevoRegistro.put("Siembra",Siembra);
				
				
				database.insert("semillas", null, nuevoRegistro);
				//_data = database.rawQuery(sql, null);
				//_data = database.rawQuery("SELECT Nombre,Siembra,Descripcion as Actividad,HoraIncio,HoraFin,Plantas FROM dguardados inner join opciones ON opciones.Opcion = dguardados.Actividad INNER JOIN dempleado ON dempleado.CodigoEmpleado=dguardados.CodigoEmpleado WHERE	Fecha=? AND ActividadFinalizada=1 group by Descripcion order by Nombre,Siembra desc", whereClause);
				
			} catch (Exception e) {
				Log.e("ErrorIngetElements", e.toString().trim());
			}
				
			return rowId;
		}
		//insertar para filtros
		public synchronized long insertFiltros(String FechaSuccion,String Id,String Material,String NoSiembra) {
			
			if (!database.isOpen()) {
				this.open();
			}
			long rowId = -1;
			try {
				ContentValues nuevoRegistro = new ContentValues();
				nuevoRegistro.put("FechaSuccion", FechaSuccion);
				nuevoRegistro.put("Id",Id);
				nuevoRegistro.put("Material",Material);
				nuevoRegistro.put("NoSiembra",NoSiembra);
				
				
				database.insert("filtros", null, nuevoRegistro);
				//_data = database.rawQuery(sql, null);
				//_data = database.rawQuery("SELECT Nombre,Siembra,Descripcion as Actividad,HoraIncio,HoraFin,Plantas FROM dguardados inner join opciones ON opciones.Opcion = dguardados.Actividad INNER JOIN dempleado ON dempleado.CodigoEmpleado=dguardados.CodigoEmpleado WHERE	Fecha=? AND ActividadFinalizada=1 group by Descripcion order by Nombre,Siembra desc", whereClause);
				
			} catch (Exception e) {
				Log.e("ErrorIngetElements", e.toString().trim());
			}
				
			return rowId;
		}
		// INSERTAR QC POLEN
		public synchronized long insertQC(String Calificativo, String Codigo, String Descripcion) {
			
			if (!database.isOpen()) {
				this.open();
			}
			long rowId = -1;
			try {
				ContentValues nuevoRegistro = new ContentValues();
				nuevoRegistro.put("Calificativo", Calificativo);
				nuevoRegistro.put("Codigo",Codigo);
				nuevoRegistro.put("Descripcion",Descripcion);
				database.insert("QCPolen", null, nuevoRegistro);
				
				
				//_data = database.rawQuery(sql, null);
				//_data = database.rawQuery("SELECT Nombre,Siembra,Descripcion as Actividad,HoraIncio,HoraFin,Plantas FROM dguardados inner join opciones ON opciones.Opcion = dguardados.Actividad INNER JOIN dempleado ON dempleado.CodigoEmpleado=dguardados.CodigoEmpleado WHERE	Fecha=? AND ActividadFinalizada=1 group by Descripcion order by Nombre,Siembra desc", whereClause);
				
			} catch (Exception e) {
				Log.e("ErrorIngetElements", e.toString().trim());
			}
				
			return rowId;
		}
		// insertar dempleado
		public synchronized long insertEmpleado(String Empresa,String CodigoEmpleado,String CodigoDepto,String Nombre,String Estado,String Responsable) {
			
			if (!database.isOpen()) {
				this.open();
			}
			long rowId = -1;
			try {
				ContentValues nuevoRegistro = new ContentValues();
				nuevoRegistro.put("Empresa", Empresa);
				nuevoRegistro.put("CodigoEmpleado", CodigoEmpleado);
				nuevoRegistro.put("CodigoDepto", CodigoDepto);
				nuevoRegistro.put("Nombre", Nombre);
				nuevoRegistro.put("Estado", Estado);
				nuevoRegistro.put("Responsable", Responsable);
				database.insert("dempleado", null, nuevoRegistro);
				
				
				//_data = database.rawQuery(sql, null);
				//_data = database.rawQuery("SELECT Nombre,Siembra,Descripcion as Actividad,HoraIncio,HoraFin,Plantas FROM dguardados inner join opciones ON opciones.Opcion = dguardados.Actividad INNER JOIN dempleado ON dempleado.CodigoEmpleado=dguardados.CodigoEmpleado WHERE	Fecha=? AND ActividadFinalizada=1 group by Descripcion order by Nombre,Siembra desc", whereClause);
				
			} catch (Exception e) {
				Log.e("ErrorIngetElements", e.toString().trim());
			}
				
			return rowId;
		}
		
		// insertar para dAzucareras
		public synchronized long insertDAzucarera(String Codigo,String Siembra,String Material) {
			
			if (!database.isOpen()) {
				this.open();
			}
			long rowId = -1;
			try {
				ContentValues nuevoRegistro = new ContentValues();
				nuevoRegistro.put("Codigo", Codigo);
				nuevoRegistro.put("Material",Material);
				nuevoRegistro.put("Siembra",Siembra);
				
				
				database.insert("dazuracreras", null, nuevoRegistro);
				//_data = database.rawQuery(sql, null);
				//_data = database.rawQuery("SELECT Nombre,Siembra,Descripcion as Actividad,HoraIncio,HoraFin,Plantas FROM dguardados inner join opciones ON opciones.Opcion = dguardados.Actividad INNER JOIN dempleado ON dempleado.CodigoEmpleado=dguardados.CodigoEmpleado WHERE	Fecha=? AND ActividadFinalizada=1 group by Descripcion order by Nombre,Siembra desc", whereClause);
				
			} catch (Exception e) {
				Log.e("ErrorIngetElements", e.toString().trim());
			}
				
			return rowId;
		}
		// insertar a dbolsas
		
		public synchronized long insertDBolsas(String Empresa,String Codigo,String Material,String Siembra, String FechaCosecha) {
			
			if (!database.isOpen()) {
				this.open();
			}
			long rowId = -1;
			try {
				ContentValues nuevoRegistro = new ContentValues();
				nuevoRegistro.put("Empresa",Empresa);
				nuevoRegistro.put("Codigo", Codigo);
				nuevoRegistro.put("Material",Material);
				nuevoRegistro.put("Siembra",Siembra);
				nuevoRegistro.put("FechaCosecha",FechaCosecha);
				database.insert("dbolsas", null, nuevoRegistro);
				//_data = database.rawQuery(sql, null);
				//_data = database.rawQuery("SELECT Nombre,Siembra,Descripcion as Actividad,HoraIncio,HoraFin,Plantas FROM dguardados inner join opciones ON opciones.Opcion = dguardados.Actividad INNER JOIN dempleado ON dempleado.CodigoEmpleado=dguardados.CodigoEmpleado WHERE	Fecha=? AND ActividadFinalizada=1 group by Descripcion order by Nombre,Siembra desc", whereClause);
				
			} catch (Exception e) {
				Log.e("ErrorIngetElements", e.toString().trim());
			}
				
			return rowId;
		}
		//
		//modifica elementos en la BD
			public synchronized boolean updateElement(Usuario u) {
		ContentValues updatedUsuario = buildFromObject(u,this.TIPO_USUARIO);		
		if (!database.isOpen()) {
			open();
		}		
		boolean result = false;
		try {
			result = database.update(DBHelper.TABLE_USUARIO, updatedUsuario,  "CodigoEmpleado =" + u.getCodigoEmpleado(), null) > 0;
		} catch (Exception e){}
	    return result;
	}
	
		//elimina elementos en la BD
			public synchronized boolean removeUsario(Usuario u) {
		return removeElement(u.getCodigoEmpleado());
	}
	
			public synchronized boolean removeElement(int id) {
				if (!database.isOpen()) {
					this.open();
				}		
				boolean result = false;
					try {		
						result = database.delete(DBHelper.TABLE_USUARIO, "CodigoEmpleado =" + id, null) > 0;
					} catch (Exception e){}

				return result;
			}
	
		//retorna un curosr con elementos a partir de un where	
			public synchronized Cursor getUsuario(String whereClause) {
		if(!database.isOpen()) {
			this.open();
		}

		Cursor _data = new MatrixCursor(new String[]{""});
		try {
			_data = database.query(DBHelper.TABLE_USUARIO, getKeysFromClass(Usuario.class), 
								   whereClause, null, null, null, null);
		} catch (Exception e) {
			Log.e("ErrorIngetElements", e.toString().trim());
		}
			
		return _data;
	}
			
		//retorna un elemento a partir de un cursor
			public synchronized Usuario getUsuarioFromCursor (Cursor _c, int index) {	 
			Usuario e = (Usuario)getObjectFromCursor(_c,TIPO_USUARIO,index);
			//_c.close();
			return e;
		}
		
//------------------ Acciones a la BD para Mempleado ----------------------
		//inserta objetos tipo Elementos a la BD
			public synchronized long insertarDisponSem(DisponibilidadSemillas u) {	   		
			ContentValues newContent = buildFromObject(u,TIPO_DISPONSEM);
			
			if (!database.isOpen()) {
				open();
			}		
			long rowId = -1;
			try {
				rowId = database.insert(DBHelper.TABLE_DISPONSEM, null, newContent);
			} catch (Exception e){
			}
			Log.d("return", rowId + "");
			return rowId;		
		}
		
		//retorna un curosr con elementos a partir de un where	
			public synchronized Cursor getDispSemillas(String whereClause) {
			if (!database.isOpen()) {
				this.open();
			}

			Cursor _data = new MatrixCursor(new String[]{""});
			try {
				_data = database.query(DBHelper.TABLE_DISPONSEM, getKeysFromClass(DisponibilidadSemillas.class), 
									   whereClause, null, null, null, null);
			} catch (Exception e) {
				Log.e("ErrorIngetElements", e.toString().trim());
			}
				
			return _data;
		}
					
		//retorna un elemento a partir de un cursor
			public synchronized DisponibilidadSemillas getDispSemillasFromCursor (Cursor _c, int index) {	 
				DisponibilidadSemillas e = (DisponibilidadSemillas)getObjectFromCursor(_c,TIPO_DISPONSEM,index);
				//_c.close();
				return e;
			}
						
//------------------ Acciones a la BD para Memdepartemento ------------------
		//inserta objetos tipo Elementos a la BD
			public synchronized long insertarMdepartamento(Mdepartamento u) {	   		
				ContentValues newContent = buildFromObject(u,TIPO_MDEPARTAMENTO);
				
				if (!database.isOpen()) {
					open();
				}		
				long rowId = -1;
				try {
					rowId = database.insert(DBHelper.TABLE_MDEPARTAMENTO, null, newContent);
				} catch (Exception e){
				}
				Log.d("return", rowId + "");
				return rowId;		
			}
			
		//retorna un curosr con elementos a partir de un where	
			public synchronized Cursor getMdepartamento(String whereClause) {
				if (!database.isOpen()) {
					this.open();
				}

				Cursor _data = new MatrixCursor(new String[]{""});
				try {
					_data = database.query(DBHelper.TABLE_MDEPARTAMENTO, getKeysFromClass(Mdepartamento.class), 
										   whereClause, null, null, null, null);
				} catch (Exception e) {
					Log.e("ErrorIngetElements", e.toString().trim());
				}
					
				return _data;
			}
						
		//retorna un elemento a partir de un cursor
			public synchronized Mdepartamento getMdepartamentoFromCursor (Cursor _c, int index) {	 
					Mdepartamento e = (Mdepartamento)getObjectFromCursor(_c,TIPO_MDEPARTAMENTO,index);
					//_c.close();
					return e;
				}
							
//------------------ Acciones a la BD para OPCIONES ------------------
		//inserta objetos tipo Elementos a la BD
			public synchronized long insertarOpciones(Opciones u) {	   		
				ContentValues newContent = buildFromObject(u,TIPO_OPCIONES);
				
				if (!database.isOpen()) {
					open();
				}		
				long rowId = -1;
				try {
					rowId = database.insert(DBHelper.TABLE_OPCIONES, null, newContent);
				} catch (Exception e){
				}
				Log.d("return", rowId + "");
				return rowId;		
			}
			
		//retorna un curosr con elementos a partir de un where	
			public synchronized Cursor getOpciones(String whereClause) {
				if (!database.isOpen()) {
					this.open();
				}
	
				Cursor _data = new MatrixCursor(new String[]{""});
				try {
					_data = database.query(DBHelper.TABLE_OPCIONES, getKeysFromClass(Opciones.class), 
										   whereClause, null, null, null, null);
				} catch (Exception e) {
					Log.e("ErrorIngetElements", e.toString().trim());
				}
					
				return _data;
			}
					
		//retorna un elemento a partir de un cursor
			public synchronized Opciones getOpcionesFromCursor (Cursor _c, int index) {	 
					Opciones e = (Opciones)getObjectFromCursor(_c,TIPO_OPCIONES,index);
					//_c.close();
					return e;
				}
														
//------------------ Acciones a la BD para Modulos ------------------
		//inserta objetos tipo Elementos a la BD
			public synchronized long insertarModulos(Modulos u) {	   		
				ContentValues newContent = buildFromObject(u,TIPO_MODULOS);
				
				if (!database.isOpen()) {
					open();
				}		
				long rowId = -1;
				try {
					rowId = database.insert(DBHelper.TABLE_MODULOS, null, newContent);
				} catch (Exception e){
				}
				Log.d("return", rowId + "");
				return rowId;		
			}
			
		//retorna un curosr con elementos a partir de un where	
			public synchronized Cursor getModulos(String whereClause) {
				if (!database.isOpen()) {
					this.open();
				}
	
				Cursor _data = new MatrixCursor(new String[]{""});
				try {
					_data = database.query(DBHelper.TABLE_MODULOS, getKeysFromClass(Modulos.class), 
										   whereClause, null, null, null, null);

				} catch (Exception e) {
					Log.e("ErrorIngetElements", e.toString().trim());
				}
					
				return _data;
			}
							
		//retorna un elemento a partir de un cursor
			public synchronized Modulos getModulosFromCursor (Cursor _c, int index) {	 
					Modulos e = (Modulos)getObjectFromCursor(_c,TIPO_MODULOS,index);
					//_c.close();
					return e;
				}
							
//------------------ Acciones a la BD para Semillas ------------------
		//inserta objetos tipo Elementos a la BD
			public synchronized long insertarSemillas(Semillas u) {	   		
				ContentValues newContent = buildFromObject(u,TIPO_SEMILLAS);
				
				if (!database.isOpen()) {
					open();
				}		
				long rowId = -1;
				try {
					rowId = database.insert(DBHelper.TABLE_SEMILLAS, null, newContent);
				} catch (Exception e){
				}
				Log.d("return", rowId + "");
				return rowId;		
			}
			
		//retorna un curosr con elementos a partir de un where	
			public synchronized Cursor getSemillas(String whereClause) {
				if (!database.isOpen()) {
					this.open();
				}
	
				Cursor _data = new MatrixCursor(new String[]{""});
				try {
					_data = database.query(DBHelper.TABLE_SEMILLAS, getKeysFromClass(Semillas.class), 
										   whereClause, null, null, null, null);
				} catch (Exception e) {
					Log.e("ErrorIngetElements", e.toString().trim());
				}
					
				return _data;
			}
							
		//retorna un elemento a partir de un cursor
			public synchronized Semillas getSemillasFromCursor (Cursor _c, int index) {	 
					Semillas e = (Semillas)getObjectFromCursor(_c,TIPO_SEMILLAS,index);
					//_c.close();
					return e;
				}
				
//------------------ Acciones a la BD para Dacceso ------------------
		//inserta objetos tipo Elementos a la BD
			public synchronized long insertarDacceso(Dacceso u) {	   		
					ContentValues newContent = buildFromObject(u,TIPO_DACCESO);
					
					if (!database.isOpen()) {
						open();
					}		
					long rowId = -1;
					try {
						rowId = database.insert(DBHelper.TABLE_DACCESO, null, newContent);
					} catch (Exception e){
					}
					Log.d("return", rowId + "");
					return rowId;		
				}
				
		//retorna un curosr con elementos a partir de un where	
			public synchronized Cursor getDacceso(String whereClause) {
					if (!database.isOpen()) {
						this.open();
					}
		
					Cursor _data = new MatrixCursor(new String[]{""});
					try {
						_data = database.query(DBHelper.TABLE_DACCESO, getKeysFromClass(Dacceso.class), 
											   whereClause, null, null, null, null);
					} catch (Exception e) {
						Log.e("ErrorIngetElements", e.toString().trim());
					}
						
					return _data;
				}
			
			//retorna un elemento a partir de un cursor
			public synchronized Dacceso getDaccesoFromCursor (Cursor _c, int index) {	 
				
				Dacceso e = (Dacceso)getObjectFromCursor(_c,TIPO_DACCESO,index);
			//_c.close();
			return e;
		}
							// retornar correlativo desde DAcceso
			
			public synchronized Cursor getCorrelativo(String[] whereClause) {
				
				if (!database.isOpen()) {
					this.open();
				}
				Cursor _data = new MatrixCursor(new String[]{""});
				try {
					_data = database.rawQuery("SELECT CorrelativoEmpleado FROM dacceso WHERE CodigoEmpleado=?", whereClause);
					
				} catch (Exception e) {
					Log.e("ErrorIngetElements", e.toString().trim());
				}
					
				return _data;
			}
			
			
			
						
//------------------ Acciones a la BD para Dguardados ------------------
		//inserta objetos tipo Elementos a la BD
			public synchronized long insertarDguardados(Dguardados u) {	   		
						ContentValues newContent = buildFromObject(u,TIPO_DGUARDADOS);
						
						if (!database.isOpen()) {
							open();
						}		
						long rowId = -1;
						try {
							rowId = database.insert(DBHelper.TABLE_DGUARDADOS, null, newContent);
						} catch (Exception e){
						}
						Log.d("return", rowId + "");
						return rowId;		
					}
					
		//retorna un curosr con elementos a partir de un where	
			public synchronized Cursor getDguardados(String whereClause) {
						if (!database.isOpen()) {
							this.open();
						}
			
						Cursor _data = new MatrixCursor(new String[]{""});
						try {
							_data = database.query(DBHelper.TABLE_DGUARDADOS, getKeysFromClass(Dguardados.class), 
												   whereClause, null, null, null, null);
						} catch (Exception e) {
							Log.e("ErrorIngetElements", e.toString().trim());
						}
							
						return _data;
					}
											
		//retorna un elemento a partir de un cursor
			public synchronized Dguardados getDguardadosFromCursor (Cursor _c, int index) {	 
							Dguardados e = (Dguardados)getObjectFromCursor(_c,TIPO_DGUARDADOS,index);
							//_c.close();
							return e;
						}
						
		//modifica elementos en la BD
			public synchronized boolean updateDguardados(Dguardados u) {
							ContentValues updatedUsuario = buildFromObject(u,this.TIPO_DGUARDADOS);		
							if (!database.isOpen()) {
								open();
							}		
							boolean result = false;
							try {
								result = database.update(DBHelper.TABLE_DGUARDADOS, updatedUsuario,  "CodigoEmpleado =" + u.getCodigoEmpleado() + " AND ActividadFinalizada = 0", null) > 0;
							} catch (Exception e){}
						    return result;
						}
											
//------------------ Acciones a la BD para JSONFILES ------------------
		//inserta objetos tipo Elementos a la BD
			public synchronized long insertarJsonFile(JsonFiles u) {	   		
					ContentValues newContent = buildFromObject(u,TIPO_JSONFILES);
					
					if (!database.isOpen()) {
						open();
					}		
					long rowId = -1;
					try {
						rowId = database.insert(DBHelper.TABLE_JSONFILES, null, newContent);
					} catch (Exception e){
					}
					Log.d("return", rowId + "");
					return rowId;		
				}
				
		//retorna un curosr con elementos a partir de un where	
			public synchronized Cursor getJsonFiles(String whereClause) {
					if (!database.isOpen()) {
						this.open();
					}
		
					Cursor _data = new MatrixCursor(new String[]{""});
					try {
						_data = database.query(DBHelper.TABLE_JSONFILES, getKeysFromClass(JsonFiles.class), 
											   whereClause, null, null, null, null);
					} catch (Exception e) {
						Log.e("ErrorIngetElements", e.toString().trim());
					}
						
					return _data;
				}
									
		//retorna un elemento a partir de un cursor
			public synchronized JsonFiles getJsonFilesFromCursor (Cursor _c, int index) {	 
						JsonFiles e = (JsonFiles)getObjectFromCursor(_c,TIPO_JSONFILES,index);
						//_c.close();
						return e;
					}
					
		//modifica elementos en la BD
			public synchronized boolean updateJsonFiles(JsonFiles u) {
						ContentValues updatedUsuario = buildFromObject(u,this.TIPO_JSONFILES);		
						if (!database.isOpen()) {
							open();
						}		
						boolean result = false;
						try {
							result = database.update(DBHelper.TABLE_JSONFILES, updatedUsuario,  "Name ='" + u.getName() + "'", null) > 0;
						} catch (Exception e){}
					    return result;
					}
						
//------------------ Acciones a la BD para EtiquetasVegetativo ------------------
		//inserta objetos tipo Elementos a la BD
			public synchronized long insertarEtiquetas(EtiquetasVegetativo u) {	   		
						ContentValues newContent = buildFromObject(u,TIPO_ETIQUETAS);
						
						if (!database.isOpen()) {
							open();
						}		
						long rowId = -1;
						try {
							rowId = database.insert(DBHelper.TABLE_ETIQUETAS, null, newContent);
						} catch (Exception e){
						}
						Log.d("return", rowId + "");
						return rowId;		
					}
					
		//retorna un curosr con elementos a partir de un where	
			public synchronized Cursor getEtiquetas(String whereClause) {
						if (!database.isOpen()) {
							this.open();
						}
			
						Cursor _data = new MatrixCursor(new String[]{""});
						try {
							_data = database.query(DBHelper.TABLE_ETIQUETAS, getKeysFromClass(EtiquetasVegetativo.class), 
												   whereClause, null, null, null, "CodigoEmpleado ASC",null );
						} catch (Exception e) {
							Log.e("ErrorIngetElements", e.toString().trim());
						}
							
						return _data;
					}
											
		//retorna un elemento a partir de un cursor
			public synchronized EtiquetasVegetativo getEtiquetasFromCursor (Cursor _c, int index) {	 
							EtiquetasVegetativo e = (EtiquetasVegetativo)getObjectFromCursor(_c,TIPO_ETIQUETAS,index);
							//_c.close();
							return e;
						}
						
		//modifica elementos en la BD
			public synchronized boolean updateEtiquetas(EtiquetasVegetativo u) {
							ContentValues updatedUsuario = buildFromObject(u,this.TIPO_ETIQUETAS);		
							if (!database.isOpen()) {
								open();
							}		
							boolean result = false;
							try {
								result = database.update(DBHelper.TABLE_ETIQUETAS, updatedUsuario,"Etiqueta = '" + u.getEtiqueta().toString().trim() + "' AND Fecha = '"+ u.getFecha().toString().trim() +"'" , null) > 0;
							} catch (Exception e){}
						    return result;
						}
						
//------------------ Acciones a la BD para EMANTENIMIENTO ---------------------
		//inserta objetos tipo Elementos a la BD
			public synchronized long insertarMantenimiento (EMantenimiento m) {	   		
							ContentValues newContent = buildFromObject(m,TIPO_MANTENIMIENTO);
							
							if (!database.isOpen()) {
								open();
							}		
							long rowId = -1;
							try {
								rowId = database.insert(DBHelper.TABLE_MANTENIMIENTO, null, newContent);
							} catch (Exception e){
							}
							Log.d("return", rowId + "");
							return rowId;		
						}
						
		//retorna un curosr con elementos a partir de un where	
			public synchronized Cursor getMantenimiento (String whereClause) {
							if (!database.isOpen()) {
								this.open();
							}
				
							Cursor _data = new MatrixCursor(new String[]{""});
							try {
								_data = database.query(DBHelper.TABLE_MANTENIMIENTO, getKeysFromClass(EMantenimiento.class), 
													   whereClause, null, null, null, null);
							} catch (Exception e) {
								Log.e("ErrorIngetElements", e.toString().trim());
							}
								
							return _data;
						}
														
		//retorna un elemento a partir de un cursor
			public synchronized EMantenimiento getMantenimientoFromCursor (Cursor _c, int index) {	 
								EMantenimiento m = (EMantenimiento)getObjectFromCursor(_c,TIPO_MANTENIMIENTO,index);
									//_c.close();
									return m;
								}
							
		//modifica elementos en la BD
			public synchronized boolean updateMantenimiento(EMantenimiento m) {
									ContentValues updatedUsuario = buildFromObject(m,this.TIPO_MANTENIMIENTO);		
									if (!database.isOpen()) {
										open();
									}		
									boolean result = false;
									try {
										result = database.update(DBHelper.TABLE_MANTENIMIENTO, updatedUsuario,  "CodigoEmpleado =" + m.getCodigoEmpleado() + " AND ActividadFinalizada = 0", null) > 0;
									} catch (Exception e){}
								    return result;
								}
							
//------------------ Acciones a la BD para EtiquetasVegetativo------------------
		//inserta objetos tipo Elementos a la BD
			public synchronized long insertarRiego(ERiego r) {	   		
								ContentValues newContent = buildFromObject(r,TIPO_RIEGO);
								
								if (!database.isOpen()) {
									open();
								}		
								long rowId = -1;
								try {
									rowId = database.insert(DBHelper.TABLE_RIEGO, null, newContent);
								} catch (Exception e){
								}
								Log.d("return", rowId + "");
								return rowId;		
							}
							
		//retorna un curosr con elementos a partir de un where	
			public synchronized Cursor getRiego (String whereClause) {
								if (!database.isOpen()) {
									this.open();
								}
					
								Cursor _data = new MatrixCursor(new String[]{""});
								try {
									_data = database.query(DBHelper.TABLE_RIEGO, getKeysFromClass(ERiego.class), 
														   whereClause, null, null, null, null);
								} catch (Exception e) {
									Log.e("ErrorIngetElements", e.toString().trim());
								}
									
								return _data;
							}
															
		//retorna un elemento a partir de un cursor
			public synchronized ERiego getRiegoFromCursor (Cursor _c, int index) {	 
									ERiego r = (ERiego)getObjectFromCursor(_c,TIPO_RIEGO,index);
									//_c.close();
									return r;
								}
								
		//modifica elementos en la BD
			public synchronized boolean updateRiego(ERiego r) {
										ContentValues updatedUsuario = buildFromObject(r,this.TIPO_RIEGO);		
										if (!database.isOpen()) {
											open();
										}		
										boolean result = false;
										try {
											result = database.update(DBHelper.TABLE_RIEGO, updatedUsuario,  "CodigoEmpleado =" + r.getCodigoEmpleado() + " AND ActividadFinalizada = 0", null) > 0;
										} catch (Exception e){}
									    return result;
									}			
									
//------------------ Acciones a la BD para DEMPLEADO ----------------------
		//inserta objetos tipo Elementos a la BD
			public synchronized long insertDEmpleado(Dempleado d) {	   		
							ContentValues newContent = buildFromObject(d,TIPO_DEMPLEADO);
							
							if (!database.isOpen()) {
								open();
							}		
							long rowId = -1;
							try {
								rowId = database.insert(DBHelper.TABLE_DEMPLEADO, null, newContent);
							} catch (Exception e){
							}
							Log.d("return", rowId + "");
							return rowId;		
						}	
																			
		//retorna un curosr con elementos a partir de un where	
			public synchronized Cursor getDempledo(String whereClause) {
							if (!database.isOpen()) {
								this.open();
							}
				
							Cursor _data = new MatrixCursor(new String[]{""});
							try {
								_data = database.query(DBHelper.TABLE_DEMPLEADO, getKeysFromClass(Dempleado.class), 
													   whereClause, null, null, null, null);
							} catch (Exception e) {
								Log.e("ErrorIngetElements", e.toString().trim());
							}
								
							return _data;
						}
													
		//retorna un elemento a partir de un cursor
			public synchronized Dempleado getDempleadoFromCursor (Cursor _c, int index) {	 
								Dempleado d = (Dempleado)getObjectFromCursor(_c,TIPO_DEMPLEADO,index);
								//_c.close();
								return d;
							}
		
//------------------- agrega DBOLSAS -------------------------------------				
		//inserta objetos tipo Elementos a la BD
			public synchronized long insertDBolsas(Dbolsas d) {	   		
								ContentValues newContent = buildFromObject(d,TIPO_DBOLSAS);
								
								if (!database.isOpen()) {
									open();
								}		
								long rowId = -1;
								try {
									rowId = database.insert(DBHelper.TABLE_DBOLSAS, null, newContent);
								} catch (Exception e){
								}
								Log.d("return", rowId + "");
								return rowId;		
							}	
																					
		//retorna un cursor con elementos a partir de un where	
			public synchronized Cursor getDbolsas(String whereClause) {
								if (!database.isOpen()) {
									this.open();
								}
					
								Cursor _data = new MatrixCursor(new String[]{""});
								try {
									_data = database.query(DBHelper.TABLE_DBOLSAS, getKeysFromClass(Dbolsas.class), 
														   whereClause, null, null, null, null);
								} catch (Exception e) {
									Log.e("ErrorIngetElements", e.toString().trim());
								}
									
								return _data;
							}
														
		//retorna un elemento a partir de un cursor
			public synchronized Dbolsas getDbolsasFromCursor (Cursor _c, int index) {	 
									Dbolsas b = (Dbolsas)getObjectFromCursor(_c,TIPO_DBOLSAS,index);
									//_c.close();
									return b;
								}	
							
//------------------- agrega DUBICACIONES --------------------									
		//inserta objetos tipo Elementos a la BD
			public synchronized long insertDUbicaciones(DUbicaciones u) {	   		
									ContentValues newContent = buildFromObject(u,TIPO_DUBICACIONES);
									
									if (!database.isOpen()) {
										open();
									}		
									long rowId = -1;
									try {
										rowId = database.insert(DBHelper.TABLE_DUBICACIONES, null, newContent);
									} catch (Exception e){
									}
									Log.d("return", rowId + "");
									return rowId;		
								}	
																								
		//retorna un cursor con elementos a partir de un where	
			public synchronized Cursor getDUbicaciones(String whereClause) {
				if (!database.isOpen()) {
					this.open();
				}
			
			Cursor _data = new MatrixCursor(new String[]{""});
				try {
					_data = database.query(DBHelper.TABLE_DUBICACIONES, getKeysFromClass(DUbicaciones.class), 
							whereClause, null, null, null, null);
					} catch (Exception e) {
						Log.e("ErrorIngetElements", e.toString().trim());
					}
					return _data;
					}
																	
		//retorna un elemento a partir de un cursor
			public synchronized DUbicaciones getDUbicacionesFromCursor (Cursor _c, int index) {	 
										DUbicaciones ub = (DUbicaciones)getObjectFromCursor(_c,TIPO_DUBICACIONES,index);
										//_c.close();
										return ub;
									}	

//------------------- agrega DINVERNADERO --------------------									
		//inserta objetos tipo Elementos a la BD
			public synchronized long insertDInvernaderos(Dinvernaderos d) {	   		
										ContentValues newContent = buildFromObject(d,TIPO_DINVERNADERO);
										
										if (!database.isOpen()) {
											open();
										}		
										long rowId = -1;
										try {
											rowId = database.insert(DBHelper.TABLE_DINVERNADERO, null, newContent);
										} catch (Exception e){
										}
										Log.d("return", rowId + "");
										return rowId;		
									}	
																									
		//retorna un cursor con elementos a partir de un where	
			public synchronized Cursor getDInvernaderos(String whereClause) {
										if (!database.isOpen()) {
											this.open();
										}
							
										Cursor _data = new MatrixCursor(new String[]{""});
										try {
											_data = database.query(DBHelper.TABLE_DINVERNADERO, getKeysFromClass(Dinvernaderos.class), 
																   whereClause, null, null, null, null);
										} catch (Exception e) {
											Log.e("ErrorIngetElements", e.toString().trim());
										}
											
										return _data;
									}
																		
		//retorna un elemento a partir de un cursor
			public synchronized Dinvernaderos getDInvernaderosFromCursor (Cursor _c, int index) {	 
											Dinvernaderos i = (Dinvernaderos)getObjectFromCursor(_c,TIPO_DINVERNADERO,index);
											//_c.close();
											return i;
										}	
																		
// ------------------------ Agrega DSIEMBRAVEGETATIVO -----------------------------------					
		//inserta objetos tipo Elementos a la BD
			public synchronized long insertarSiembraVegetativo(SiembraVegetativo u) {	   		
										ContentValues newContent = buildFromObject(u,TIPO_DSIEMBRAVEGETATIVO);
										
										if (!database.isOpen()) {
											open();
										}		
										long rowId = -1;
										try {
											rowId = database.insert(DBHelper.TABLE_DSIEMBRAVEGETATIVO, null, newContent);
										} catch (Exception e){
										}
										Log.d("return", rowId + "");
										return rowId;		
									}
									
		//retorna un curosr con elementos a partir de un where	
			public synchronized Cursor getSiembraVegetativo(String whereClause) {
										if (!database.isOpen()) {
											this.open();
										}
							
										Cursor _data = new MatrixCursor(new String[]{""});
										try {
											_data = database.query(DBHelper.TABLE_DSIEMBRAVEGETATIVO, getKeysFromClass(SiembraVegetativo.class), 
																   whereClause, null, null, null, null);
										} catch (Exception e) {
											Log.e("ErrorIngetElements", e.toString().trim());
										}
											
										return _data;
									}
				
		//retorna un elemento a partir de un cursor
			public synchronized SiembraVegetativo getSiembraVegetativoFromCursor (Cursor _c, int index) {	 
											SiembraVegetativo e = (SiembraVegetativo)getObjectFromCursor(_c,TIPO_DSIEMBRAVEGETATIVO,index);
											//_c.close();
											return e;
										}
										
//------------------- agrega DAZUCARERAS ----------------------------------------				
		//inserta objetos tipo Elementos a la BD
			public synchronized long insertarDazucareras(Dazucareras d) {	   		
					ContentValues newContent = buildFromObject(d,TIPO_DAZUCARERAS);
					if (!database.isOpen()) {
					open();
					}		
					long rowId = -1;
					try {
					rowId = database.insert(DBHelper.TABLE_DAZUCARERAS, null, newContent);
					} catch (Exception e){
					}
					Log.d("return", rowId + "");
					return rowId;		
					}														
											
		//retorna un cursor con elementos a partir de un where	
			public synchronized Cursor getDazucareras(String whereClause) {
						if (!database.isOpen()) {
						this.open();
						}
						
						Cursor _data = new MatrixCursor(new String[]{""});
						try {
							_data = database.query(DBHelper.TABLE_DAZUCARERAS, getKeysFromClass(Dazucareras.class), 
							whereClause, null, null, null, null);
							} catch (Exception e) {
							Log.e("ErrorIngetElements", e.toString().trim());
							}
												
							return _data;
							}
																	
		//retorna un elemento a partir de un cursor
			public synchronized Dazucareras getDazucarerasFromCursor (Cursor _c, int index) {	 
						Dazucareras b = (Dazucareras)getObjectFromCursor(_c,TIPO_DAZUCARERAS,index);
						//_c.close();
						return b;
						}	

//------------------- agrega DFiltros --------------------			
		//inserta objetos tipo Elementos a la BD
			public synchronized long insertarDFiltros(Filtros fil) {	   		
						ContentValues newContent = buildFromObject(fil,TIPO_DFILTROS);
						
						if (!database.isOpen()) {
							open();
							}		
							long rowId = -1;
							try {
							rowId = database.insert(DBHelper.TABLE_DFILTROS, null, newContent);
							} catch (Exception e){
							}
							Log.d("return", rowId + "");
							return rowId;		
							}																		
				
		//retorna un cursor con elementos a partir de un where	
			public synchronized Cursor getDFiltros(String whereClause) {
						if (!database.isOpen()) {
						this.open();
						}
									
						Cursor _data = new MatrixCursor(new String[]{""});
						try {
						_data = database.query(DBHelper.TABLE_DFILTROS, getKeysFromClass(Filtros.class), 
						whereClause, null, null, null, null);
						} catch (Exception e) {
						Log.e("ErrorIngetElements", e.toString().trim());
						}
													
						return _data;
						}
																		
		//retorna un elemento a partir de un cursor
			public synchronized Filtros getDFiltrosFromCursor (Cursor _c, int index) {	 
					Filtros fi = (Filtros)getObjectFromCursor(_c,TIPO_DFILTROS,index);
					return fi;
					}	
				
//------------------- agrega DISPONIBILIDAD VEGETATIVO --------------------	
		//inserta objetos tipo Elementos a la BD
			public synchronized long insetarDisponVeg(DisponibilidadVegetativo d) {	   		
							ContentValues newContent = buildFromObject(d,TIPO_DISPONVEG);		
								if (!database.isOpen()) {
									open();
									}		
									long rowId = -1;
									try {
									rowId = database.insert(DBHelper.TABLE_DISPONVEG, null, newContent);
									} catch (Exception e){
									}
									Log.d("return", rowId + "");
									return rowId;		
									}
											
		//retorna un curosr con elementos a partir de un where	
			public synchronized Cursor getDisponVeg(String whereClause) {
										if (!database.isOpen()) {
											this.open();
												}
									
										Cursor _data = new MatrixCursor(new String[]{""});
										try {
											_data = database.query(DBHelper.TABLE_DISPONVEG, getKeysFromClass(DisponibilidadVegetativo.class), 
												whereClause, null, null, null, null);
												} catch (Exception e) {
													Log.e("ErrorIngetElements", e.toString().trim());
												}		
										return _data;
									}
									
		//retorna un elemento a partir de un cursor
			public synchronized DisponibilidadVegetativo getDisponibildadVegetativoFromCursor (Cursor _c, int index) {	 
								DisponibilidadVegetativo d = (DisponibilidadVegetativo)getObjectFromCursor(_c,TIPO_DISPONVEG,index);
								//_c.close();
								return d;
							}
							
		//modifica elementos en la BD
			public synchronized boolean updateDisponveg(DisponibilidadVegetativo u) {
								ContentValues updatedUsuario = buildFromObject(u,this.TIPO_DISPONVEG);		
								if (!database.isOpen()) {
									open();
								}		
								boolean result = false;
								try {
									result = database.update(DBHelper.TABLE_DISPONVEG, updatedUsuario,  "CodigoEmpleado =" + u.getSupervisor(), null) > 0;
								} catch (Exception e){}
								return result;
							}															
		
//************** Acciones a la BD para SemillasACOND ********************
		//inserta objetos tipo Elementos a la BD
			public synchronized long insertarSemillasAcond(SemillasAcond u) {	   		
					ContentValues newContent = buildFromObject(u,TIPO_SEMILLASACOND);
								if (!database.isOpen()) {
									open();
								}		
								long rowId = -1;
								try {
									rowId = database.insert(DBHelper.TABLE_SEMILLAS, null, newContent);
								} catch (Exception e){
								}
								Log.d("return", rowId + "");
								return rowId;		
							}							
				
		//retorna un curosr con elementos a partir de un where	
			public synchronized Cursor getSemillasAcond(String whereClause) {
								if (!database.isOpen()) {
									this.open();
								}
								Cursor _data = new MatrixCursor(new String[]{""});
								try {
									_data = database.query(DBHelper.TABLE_SEMILLAS, getKeysFromClass(SemillasAcond.class), 
														   whereClause, null, null, null, null);
								} catch (Exception e) {
									Log.e("ErrorIngetElements", e.toString().trim());
								}
									
								return _data;
							}								
							
		//retorna un elemento a partir de un cursor
			public synchronized Semillas getSemillasAcondFromCursor (Cursor _c, int index) {	 
					Semillas e = (Semillas)getObjectFromCursor(_c,TIPO_SEMILLASACOND,index);
									//_c.close();
									return e;
								}													
							
//------------------- agrega QCPOLEN -----------------------------------------------			
		//inserta objetos tipo Elementos a la BD
			public synchronized long insertarQCPolen(QCPolen c) {	   		
						ContentValues newContent = buildFromObject(c,TIPO_QCPOLEN);		
							if (!database.isOpen()) {
								open();
								}		
								long rowId = -1;
								try {
								rowId = database.insert(DBHelper.TABLE_QCPOLEN, null, newContent);
								} catch (Exception e){
								}
								Log.d("return", rowId + "");
								return rowId;		
								}
										
		//retorna un curosr con elementos a partir de un where	
			public synchronized Cursor getQCPolen(String whereClause) {
									if (!database.isOpen()) {
										this.open();
											}
								
									Cursor _data = new MatrixCursor(new String[]{""});
									try {
										_data = database.query(DBHelper.TABLE_QCPOLEN, getKeysFromClass(QCPolen.class), 
											whereClause, null, null, null, null);
											} catch (Exception e) {
												Log.e("ErrorIngetElements", e.toString().trim());
											}		
									return _data;
								}
								
		//retorna un elemento a partir de un cursor
			public synchronized QCPolen getQCPolenFromCursor (Cursor _c, int index) {	 
							QCPolen q = (QCPolen)getObjectFromCursor(_c,TIPO_QCPOLEN,index);
							//_c.close();
							return q;
						}
						
		//modifica elementos en la BD
			public synchronized boolean updateQCPolen(QCPolen q) {
							ContentValues updateQCPolen = buildFromObject(q,this.TIPO_QCPOLEN);		
							if (!database.isOpen()) {
								open();
							}		
							boolean result = false;
							try {
								result = database.update(DBHelper.TABLE_QCPOLEN, updateQCPolen,  "Codigo =" + q.getCodigo(), null) > 0;
							} catch (Exception e){}
							return result;
						}								
							
//------------------- agrega DFAMILIAS -----------------------------------	
		//inserta objetos tipo Elementos a la BD
			public synchronized long insertarDFamilias(DFamilias f) {	   		
							ContentValues newContent = buildFromObject(f,TIPO_DFAMILIAS);		
								if (!database.isOpen()) {
									open();
									}		
									long rowId = -1;
									try {
									rowId = database.insert(DBHelper.TABLE_DFAMILIAS, null, newContent);
									} catch (Exception e){
									}
									Log.d("return", rowId + "");
									return rowId;		
									}
											
		//retorna un curosr con elementos a partir de un where	
			public synchronized Cursor getDFamilias(String whereClause) {
										if (!database.isOpen()) {
											this.open();
												}
									
										Cursor _data = new MatrixCursor(new String[]{""});
										try {
											_data = database.query(DBHelper.TABLE_DFAMILIAS, getKeysFromClass(DFamilias.class), 
												whereClause, null, null, null, null);
												} catch (Exception e) {
													Log.e("ErrorIngetElements", e.toString().trim());
												}		
										return _data;
									}
									
		//retorna un elemento a partir de un cursor
			public synchronized DFamilias getDFamiliasFromCursor (Cursor _c, int index) {	 
								DFamilias f = (DFamilias)getObjectFromCursor(_c,TIPO_DFAMILIAS,index);
								//_c.close();
								return f;
							}
							
		//modifica elementos en la BD
			public synchronized boolean updateDFamilias(DFamilias f) {
								ContentValues updateDFamilias = buildFromObject(f,this.TIPO_DFAMILIAS);		
								if (!database.isOpen()) {
									open();
								}		
								boolean result = false;
								try {
									result = database.update(DBHelper.TABLE_DFAMILIAS, updateDFamilias,  "Codigo =" + f.getCodigo(), null) > 0;
								} catch (Exception e){}
								return result;
							}				

//------------------ Acciones a la BD para EInvestigacion ------------------
		//inserta objetos tipo Elementos a la BD
		public synchronized long insertarEInvestigacion(EInvestigacion i) {	   		
			ContentValues newContent = buildFromObject(i,TIPO_INVESTIGACION);
			
			if (!database.isOpen()) {
				open();
				}		
				long rowId = -1;
				try {
				rowId = database.insert(DBHelper.TABLE_INVESTIGACION, null, newContent);
				} catch (Exception e){
					}
					Log.d("return", rowId + "");
					return rowId;		
					}
						
		//retorna un curosr con elementos a partir de un where	
		public synchronized Cursor getEInvestigacion(String whereClause) {
			if (!database.isOpen()) {
				this.open();
			}
			
			Cursor _data = new MatrixCursor(new String[]{""});
			try {
				_data = database.query(DBHelper.TABLE_INVESTIGACION, getKeysFromClass(EInvestigacion.class), 
						whereClause, null, null, null, null);
			} catch (Exception e) {
				Log.e("ErrorIngetElements", e.toString().trim());
			}
			
			return _data;
		}
		
		//retorna un elemento a partir de un cursor
		public synchronized EInvestigacion getEInvestigacionFromCursor (Cursor _c, int index) {	 
			EInvestigacion i = (EInvestigacion)getObjectFromCursor(_c,TIPO_INVESTIGACION,index);
			//_c.close();
			return i;
		}
		
		//modifica elementos en la BD
		public synchronized boolean updateEInvestigacion(EInvestigacion i) {
			ContentValues updatedUsuario = buildFromObject(i,this.TIPO_INVESTIGACION);		
			if (!database.isOpen()) {
				open();
			}		
			boolean result = false;
			try {
				result = database.update(DBHelper.TABLE_INVESTIGACION, updatedUsuario,  "CodigoEmpleado =" + i.getCodigoEmpleado() + " AND ActividadFinalizada = 0", null) > 0;
			} catch (Exception e){}
			return result;
		}
			
//----------------------------------------- FIN METODOS PARA AGREGAR A TABLAS ---------------------------------------------------						
		
	public synchronized void setLastUpdated(Date d) {
						SimpleDateFormat _format = new SimpleDateFormat("ddMMyyyy'T'HH:mm:ssZ");
						String str_date = _format.format(d);		
						ContentValues newElement = new ContentValues();
						newElement.put("last_updated",str_date);
						
						if (!database.isOpen()) {
							open();
						}
						
						Cursor _data = database.query( DBHelper.TABLE_INFO, null, "id = 1", null, null, null, "");
						try {
							if (_data.moveToFirst()) {		
								database.update(DBHelper.TABLE_INFO, newElement,  "id = 1", null);
							} else {
								database.insert(DBHelper.TABLE_INFO, null, newElement);
							}
						} catch (Exception e){}
						_data.close();
					}
					
	public synchronized Date getLastUpdated(){
				Date last_updated = null;
				
				if (!database.isOpen()) {
					this.open();
				}				
				Cursor _data = database.query( DBHelper.TABLE_INFO, new String[]{"last_updated"}, "id = 1", null, null, null, "");
				if (_data.moveToFirst()) {
					String str_last_updated = _data.getString(_data.getColumnIndex("last_updated"));
					_data.close();
					SimpleDateFormat _format = new SimpleDateFormat("ddMMyyyy'T'HH:mm:ssZ");
			    	try {
						last_updated = _format.parse(str_last_updated);
					} catch (Exception e) {}
				}
				return last_updated;
			}
	
	//elimina todas las tablas de la BD
	public synchronized void deleteAllElements() {
		if (!database.isOpen()) {
			open();
		}		
		try {
			database.delete(DBHelper.TABLE_USUARIO, null, null);


		} catch (Exception e) {}
	}
	
	public String[] getKeysFromClass(Class<?> cl) {
		ArrayList<String> keys = new ArrayList<String>();		
		Field[] fl = cl.getDeclaredFields();
		for (Field f : fl) {
			if (  f.getType().equals(String.class)
				||f.getType().equals(Bitmap.class)
				|| f.getType().equals(Integer.class)
				) {
				keys.add(f.getName());				
			}
		}
		return keys.toArray(new String[keys.size()]);
	}
		
	private ContentValues buildFromObject(Object Ob, int type) {		
	    ContentValues newElement = new ContentValues();
	    	    
		if(type == TIPO_USUARIO){	
			Usuario u = (Usuario)Ob;
			newElement.put("Usuario", u.getUsuario());
			newElement.put("Clave", u.getClave());
			newElement.put("Tipo", u.getTipo());
			newElement.put("CodigoEmpleado", u.getCodigoEmpleado());		
			newElement.put("CodigoDepto", u.getCodigoDepto());
			newElement.put("CodigoAPP", u.getCodigoAPP());
			
		}else if(type == TIPO_DISPONSEM){	
			DisponibilidadSemillas u = (DisponibilidadSemillas)Ob;
			newElement.put("Actividad", u.getActividad());
			newElement.put("Siembra", u.getSiembra());
			newElement.put("Muestra", u.getMuestra());
			newElement.put("Fecha", u.getFecha());
			newElement.put("Hora", u.getHora());
			newElement.put("Supervisor", u.getSupervisor());
	
		}else if(type == TIPO_MDEPARTAMENTO){
			Mdepartamento u = (Mdepartamento)Ob;	
			
			newElement.put("CodigoDepto", u.getCodigoDepto());
			newElement.put("DescripcionDepto", u.getDescripcionDepto());
			newElement.put("MENU", u.getMenu());
			
		}else if(type == TIPO_MODULOS){
			Modulos u = (Modulos)Ob;	
						
			newElement.put("CodigoDepto", u.getCodigoDepto());
			newElement.put("CodigoModulo", u.getCodigoModulo());
			newElement.put("Descripcion", u.getDescripcion());
			newElement.put("Prioridad", u.getPrioridad());
			newElement.put("Familia", u.getFamilia());
			newElement.put("CodigoAPP", u.getCodigoAPP());
			newElement.put("Area", u.getArea());
			
		}else if(type == TIPO_OPCIONES){
			Opciones u = (Opciones)Ob;	
			newElement.put("CodigoDepto", u.getCodigoDepto());
			newElement.put("Opcion", u.getOpcion());
			newElement.put("Descripcion", u.getDescripcion());
			newElement.put("Menu", u.getMenu());
			newElement.put("Prioridad", u.getPrioridad());
			newElement.put("FinalizaActividad", u.getFinalizaActividad());
			newElement.put("Tipo", u.getTipo());
			newElement.put("Parametro", u.getParametro());
			newElement.put("Parametro2", u.getParametro2());
			newElement.put("Parametro3", u.getParametro3());
			newElement.put("Parametro4", u.getParametro4());
			newElement.put("Parametro5", u.getParametro5());
			newElement.put("Poliza", u.getPoliza());
			newElement.put("Pureza1", u.getPureza1());
			newElement.put("Pureza2", u.getPureza2());
			newElement.put("Pureza3", u.getPureza3());
			newElement.put("Pureza4", u.getPureza4());
			newElement.put("Pureza5", u.getPureza5());
			newElement.put("CodigoAPP", u.getCodigoAPP());
			
		}else if(type == TIPO_SEMILLAS){
			Semillas u = (Semillas)Ob;
			newElement.put("Siembra", u.getSiembra());
			newElement.put("Material", u.getMaterial());
			newElement.put("Polinizacion", u.getPolinizacion());
			newElement.put("Produccion", u.getProduccion());
			newElement.put("MCosecha", u.getMCosecha());
			newElement.put("Plantas", u.getPlantas());
			newElement.put("Postura", u.getPostura());
			
		}else if(type == TIPO_DACCESO){
			Dacceso u = (Dacceso)Ob;
			newElement.put("Empresa", u.getEmpresa());
			newElement.put("CodigoEmpleado", u.getCodigoEmpleado());
			newElement.put("CorrelativoEmpleado", u.getCorrelativoEmpleado());

		}else if(type == TIPO_DGUARDADOS){
			Dguardados u = (Dguardados)Ob;
			newElement.put("CodigoEmpleado", u.getCodigoEmpleado());
			newElement.put("CodigoDepto", u.getCodigoDepto());
			newElement.put("Actividad",   u.getActividad());
			newElement.put("Siembra",  u.getSiembra() );
			newElement.put("Azucarera",  u.getAzucarera() );
			newElement.put("Fecha",  u.getFecha() );
			newElement.put("HoraIncio",  u.getHoraIncio() );
			newElement.put("HoraFin",   u.getHoraFin());
			newElement.put("Supervisor",   u.getSupervisor());
			newElement.put("ActividadFinalizada",   u.getActividadFinalizada());
			newElement.put("Invernadero",   u.getInvernadero());
			newElement.put("Plantas",   u.getPlantas());
			newElement.put("PlantaMacho",   u.getPlantaMacho());
			newElement.put("Flores",   u.getFlores());
			newElement.put("Bancas",   u.getBancas());
			newElement.put("Hora", u.getHora());
			newElement.put("Min", u.getMin());
			newElement.put("CorrelativoBolsa", u.getCorrelativoBolsa());
			newElement.put("CorrelativoBolsa2", u.getCorrelativoBolsa2());
			newElement.put("CorrelativoBolsa3", u.getCorrelativoBolsa3());
			newElement.put("CorrelativoBolsa4", u.getCorrelativoBolsa4());
			newElement.put("CorrelativoBolsa5", u.getCorrelativoBolsa5());
			newElement.put("Filtro", u.getFiltro());
			newElement.put("PesoPolen", u.getPesoPolen());
			newElement.put("VolumenPolen", u.getVolumenPolen());
			newElement.put("CorrelativoEmpleado", u.getCorrelativoEmpleado());

		}else if(type == TIPO_ETIQUETAS){
			EtiquetasVegetativo u = (EtiquetasVegetativo)Ob;
			newElement.put("CodigoEmpleado", u.getCodigoEmpleado());
			newElement.put("Etiqueta", u.getEtiqueta());
			newElement.put("Finalizado", u.getFinalizado());
			newElement.put("Invernadero", u.getInvernadero());
			newElement.put("Fecha", u.getFecha());
			newElement.put("Hora", u.getHora());
			newElement.put("Actividad", u.getActividad());
			newElement.put("Supervisor", u.getSupervisor());
			newElement.put("Tipo", u.getTipo());
			newElement.put("Peso", u.getPeso());
		
		}else if(type == TIPO_MANTENIMIENTO){
			EMantenimiento m = (EMantenimiento)Ob;
			newElement.put("CodigoEmpleado", m.getCodigoEmpleado());
			newElement.put("CodigoDepto", m.getCodigoDepto());
			newElement.put("Actividad", m.getActividad());
			newElement.put("Fecha", m.getFecha());
			newElement.put("HoraIncio", m.getHoraIncio());
			newElement.put("HoraFin", m.getHoraFin());
			newElement.put("Supervisor", m.getSupervisor());
			newElement.put("ActividadFinalizada", m.getActividadFinalizada());
			newElement.put("Parametro", m.getParametro());
			newElement.put("Parametro2", m.getParametro2());
			newElement.put("Ubicacion", m.getUbicacion());
			newElement.put("Hora", m.getHora());
			newElement.put("Min", m.getMin());
			newElement.put("CorrelativoEmpleado",m.getCorrelativoEmpleado());
			
		}else if(type == TIPO_RIEGO){
			ERiego r = (ERiego)Ob;
			newElement.put("CodigoEmpleado", r.getCodigoEmpleado());
			newElement.put("CodigoDepto", r.getCodigoDepto());
			newElement.put("Actividad", r.getActividad());
			newElement.put("Fecha", r.getFecha());
			newElement.put("HoraIncio", r.getHoraIncio());
			newElement.put("HoraFin", r.getHoraFin());
			newElement.put("Supervisor", r.getSupervisor());
			newElement.put("ActividadFinalizada", r.getActividadFinalizada());
			newElement.put("Parametro", r.getParametro());
			newElement.put("Siembra", r.getSiembra());
			newElement.put("Ubicacion", r.getUbicacion());
			newElement.put("Hora", r.getHora());
			newElement.put("Min", r.getMin());
			newElement.put("FP", r.getFP());
			newElement.put("CorrelativoEmpleado", r.getCorrelativoEmpleado());
			
		}else if(type == TIPO_INVESTIGACION){
			EInvestigacion i = (EInvestigacion)Ob;
			newElement.put("CodigoEmpleado", i.getCodigoEmpleado());
			newElement.put("CodigoDepto", i.getCodigoDepto());
			newElement.put("Actividad", i.getActividad());
			newElement.put("Fecha", i.getFecha());
			newElement.put("HoraIncio", i.getHoraIncio());
			newElement.put("HoraFin", i.getHoraFin());
			newElement.put("Supervisor", i.getSupervisor());
			newElement.put("ActividadFinalizada", i.getActividadFinalizada());
			newElement.put("Parametro", i.getParametro());
			newElement.put("Parametro2", i.getParametro2());
			newElement.put("Hora", i.getHora());
			newElement.put("Min", i.getMin());
			newElement.put("CorrelativoEmpleado", i.getCorrelativoEmpleado());

		}else if(type == TIPO_JSONFILES){
			JsonFiles u = (JsonFiles)Ob;
			newElement.put("Name", u.getName());
			newElement.put("Upload", u.getUpload());
			newElement.put("NameFolder", u.getNameFolder());
					
		}else if(type == TIPO_DEMPLEADO){
			Dempleado d = (Dempleado)Ob;
			newElement.put("Empresa", d.getEmpresa());
			newElement.put("CodigoEmpleado", d.getCodigoEmpleado());
			newElement.put("CodigoDepto", d.getCodigoDepto());
			newElement.put("Nombre", d.getNombre());
			newElement.put("Estado", d.getEstado());
			newElement.put("Responsable", d.getResponsable());

		}else if(type == TIPO_DBOLSAS){  // -------------------- Agrega Elementos a DBOLSAS -------------------
			Dbolsas b = (Dbolsas)Ob;
			newElement.put("Empresa", b.getEmpresa());
			newElement.put("Codigo", b.getCodigo());
			newElement.put("Material", b.getMaterial());
			newElement.put("Siembra", b.getSiembra());
			newElement.put("FechaCosecha", b.getFechaCosecha());
		
		}else if(type == TIPO_DINVERNADERO){  // -------------------- Agrega Elementos a D_invernadero ---------------------	
			Dinvernaderos in = (Dinvernaderos)Ob;
			newElement.put("Invernadero", in.getInvernadero());
			
		}else if(type == TIPO_DUBICACIONES){  // -------------------- Agrega Elementos a DUBICACIONES ---------------------	
			DUbicaciones ub = (DUbicaciones)Ob;
			newElement.put("Codigo", ub.getCodigo());
			newElement.put("Descripcion", ub.getDescripcion());
			
		}else if(type == TIPO_DSIEMBRAVEGETATIVO){  // -------------------- Agrega Elementos a DSIEMBRASVEG ---------------------	
			SiembraVegetativo si = (SiembraVegetativo)Ob;
			newElement.put("Siembra", si.getSiembra());
			newElement.put("Variedad", si.getVariedad());
			newElement.put("Plantas", si.getPlantas());
			newElement.put("Invernadero", si.getInvernadero());
	
		}else if(type == TIPO_DAZUCARERAS){  // -------------------- Agrega Elementos a DAZUCARERAS -------------------
			Dazucareras az = (Dazucareras)Ob;
			newElement.put("Codigo", az.getCodigo());
			newElement.put("Material", az.getMaterial());
			newElement.put("Siembra", az.getSiembra());
		
		}else if(type == TIPO_DFILTROS){  // -------------------- Agrega Elementos a FILTROS -------------------
			Filtros ff = (Filtros)Ob;
			newElement.put("ID", ff.getId());
			newElement.put("FechaSuccion", ff.getFechaSuccion());
			newElement.put("Material", ff.getMaterial());
			newElement.put("NoSiembra", ff.getNoSiembra());
		
		}else if(type == TIPO_DISPONVEG){  // -------------------- Agrega Elementos a DisponibilidadVegetativo-------------------
			DisponibilidadVegetativo dv = (DisponibilidadVegetativo)Ob;
			newElement.put("Siembra", dv.getSiembra());
			newElement.put("Plantas", dv.getPlantas());
			newElement.put("Hora", dv.getHora());
			newElement.put("Fecha",dv.getFecha());
			newElement.put("Actividad", dv.getActividad());
			newElement.put("Supervisor", dv.getSupervisor());
			newElement.put("Invernadero", dv.getInvernadero());
			newElement.put("Finalizado", dv.getFinalizado());
		
		}else if(type == TIPO_SEMILLASACOND){ // -------------------- Agrega Elementos a DSEMILLASACOND ---------------------	
			SemillasAcond u = (SemillasAcond)Ob;
			newElement.put("Siembra", u.getSiembra());
			newElement.put("Material", u.getMaterial());
			newElement.put("Polinizacion", u.getPolinizacion());
		
		}else if(type == TIPO_QCPOLEN){  // -------------------- Agrega Elementos a QCPOLEN ---------------------	
			QCPolen q = (QCPolen)Ob;
			newElement.put("Codigo", q.getCodigo());
			newElement.put("Descripcion", q.getDescripcion());
			newElement.put("Calificativo", q.getCalificativo());
		
		}else if(type == TIPO_DFAMILIAS){ // -------------------- Agrega Elementos a DFAMILIAS ---------------------	 
			DFamilias f = (DFamilias)Ob;
			newElement.put("Empresa", f.getEmpresa());
			newElement.put("Codigo", f.getCodigo());
			newElement.put("Familia", f.getFamilia());
		}
		
		return newElement;		
	}
	
	//retorna un objeto a partir de un cursor
		public synchronized Object getObjectFromCursor (Cursor _c, int type, int index) {
			Object objecto= null;
			
			if (_c.moveToPosition(index)) {
				Class<?> cl =  getClass();
				
	   			 switch (type) {
				 	case 1:	
				 		cl = Usuario.class;
				 		objecto = new Usuario();
				 		break;
				 	case 2:	
			 		    cl = DisponibilidadSemillas.class;
			 		    objecto = new DisponibilidadSemillas();
			 			break;
				 	case 3:	
			 		    cl = Mdepartamento.class;
			 		    objecto = new Mdepartamento();
			 			break;
				 	case 4:	
			 		    cl = Modulos.class;
			 		    objecto = new Modulos();
			 			break;
				 	case 5:	
			 		    cl = Opciones.class;
			 		    objecto = new Opciones();
			 			break;
				 	case 6:	
			 		    cl = Semillas.class;
			 		    objecto = new Semillas();
			 			break;
				 	case 7:	
			 		    cl = Dacceso.class;
			 		    objecto = new Dacceso();
			 			break;
				 	case 8:	
			 		    cl = Dguardados.class;
			 		    objecto = new Dguardados();
			 			break;
				 	case 9:	
			 		    cl = EtiquetasVegetativo.class;
			 		    objecto = new EtiquetasVegetativo();
			 			break;
				 	case 10:	
			 		    cl = JsonFiles.class;
			 		    objecto = new JsonFiles();
			 			break;
				 	case 11:	
			 		    cl = Dempleado.class;
			 		    objecto = new Dempleado();
			 			break;		 			
				 	case 12:	
			 		    cl = Dbolsas.class;
			 		    objecto = new Dbolsas();
			 			break;		 			
				 	case 13:	
			 		    cl = Dinvernaderos.class;
			 		    objecto = new Dinvernaderos();
			 			break;
				 	case 14:	
			 		    cl = SiembraVegetativo.class;
			 		    objecto = new SiembraVegetativo();
			 			break;
				 	case 15:	
			 		    cl = Dazucareras.class;
			 		    objecto = new Dazucareras();
			 			break;
				 	case 16:	
			 		    cl = DisponibilidadVegetativo.class;
			 		    objecto = new DisponibilidadVegetativo();
			 			break;
				 	case 17:	
			 		    cl = Filtros.class;
			 		    objecto = new Filtros();
			 			break;
				 	case 18:	
			 		    cl = SemillasAcond.class;
			 		    objecto = new SemillasAcond();
			 			break;
				 	case 19:	
			 		    cl = QCPolen.class;
			 		    objecto = new QCPolen();
			 			break;
				 	case 20:	
			 		    cl = DFamilias.class;
			 		    objecto = new DFamilias();
			 			break;
				 	case 21:
				 		cl = EMantenimiento.class;
				 		objecto = new EMantenimiento();
				 		break;
				 	case 22:
				 		cl = ERiego.class;
				 		objecto = new ERiego();
				 		break;
				 	case 23:
				 		cl = DUbicaciones.class;
				 		objecto = new DUbicaciones();
				 		break;
				 	case 24:
				 		cl = EInvestigacion.class;
				 		objecto = new EInvestigacion();
				 		break;
				 	default:
			 			break;
				 }
				
				Field[] fl = cl.getDeclaredFields();
				for (Field f : fl) {				
					try {
						String _name = f.getName();
						if (f.getType().equals(String.class)) {
							String _value = _c.getString(_c.getColumnIndex(_name));
							Method method = (objecto.getClass()).getMethod("set" + _name, String.class);
							method.invoke(objecto,_value);
						} else if (f.getType().equals(Integer.class)) {
							Integer _value = _c.getInt(_c.getColumnIndex(_name));
							Method method = (objecto.getClass()).getMethod("set" + _name, Integer.class);
							method.invoke(objecto,_value); 										
						}
					} catch (SecurityException e) {
					} catch (NoSuchMethodException e) {
					} catch (IllegalArgumentException e) {
					} catch (IllegalAccessException e) {
					} catch (IllegalStateException e) {
					} catch (InvocationTargetException e) {						
					}					
				}
			}
			return objecto;
		}
}
