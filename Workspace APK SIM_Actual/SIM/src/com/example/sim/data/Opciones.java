package com.example.sim.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Opciones {
	
	private Integer CodigoDepto;
	private Integer Opcion;
	private String 	Descripcion;
	private String 	Menu;
	private Integer Prioridad;
	private Integer FinalizaActividad;
	private Integer Tipo;
	private String  Parametro;
	private String  Parametro2;
	private String  Parametro3;
	private String  Parametro4;
	private String  Parametro5;
	private String  Poliza;
	private String  Pureza1;
	private String  Pureza2;
	private String  Pureza3;
	private String  Pureza4;
	private String  Pureza5;
	private String CodigoAPP;

	public Opciones() {
		CodigoDepto = 0;
		Opcion = 0;
		Descripcion = "";
		Menu = "";
		Prioridad = 0;
		FinalizaActividad=0;
		Tipo = 1;
		Parametro = "";
		Parametro2 = "";
		Parametro3 = "";
		Parametro4 = "";
		Parametro5 = "";
		Poliza = "";
		Pureza1 = "";
		Pureza2 = "";
		Pureza3 = "";
		Pureza4 = "";
		Pureza5 = "";
		CodigoAPP = "";
	}
	
	public Opciones(Integer codigoDepto, Integer opcion, String descripcion,
			String menu, Integer prioridad, Integer finalizaActividad, Integer tipo, 
			String parametro, String poliza, String pureza1, String pureza2, String pureza3, String pureza4,String pureza5,
			String parametro2, String parametro3,String parametro4,String parametro5, String codigoApp) {
		CodigoDepto = codigoDepto;
		Opcion = opcion;
		Descripcion = descripcion;
		Menu = menu;
		Prioridad = prioridad;
		FinalizaActividad = finalizaActividad;
		Tipo = tipo;
		Parametro = parametro;
		Parametro2 = parametro2;
		Parametro3 = parametro3;
		Parametro4 = parametro4;
		Parametro5 = parametro5;
		Poliza = poliza;
		Pureza1 = pureza1;
		Pureza2 = pureza2;
		Pureza3 = pureza3;
		Pureza4 = pureza4;
		Pureza5 = pureza5;
		CodigoAPP = codigoApp;
	}

	public Integer getFinalizaActividad() {
		return FinalizaActividad;
	}

	public void setFinalizaActividad(Integer finalizaActividad) {
		FinalizaActividad = finalizaActividad;
	}
	
	@JsonProperty("CODIGO_MODULO")
	public Integer getCodigoDepto() {
		return CodigoDepto;
	}
	
	public void setCodigoDepto(Integer codigoDepto) {
		CodigoDepto = codigoDepto;
	}
	
	@JsonProperty("OPCION")
	public Integer getOpcion() {
		return Opcion;
	}
	public void setOpcion(Integer opcion) {
		Opcion = opcion;
	}
	
	@JsonProperty("DESCRIPCION")
	public String getDescripcion() {
		return Descripcion;
	}
	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}
	
	@JsonProperty("MENU")
	public String getMenu() {
		return Menu;
	}
	public void setMenu(String menu) {
		Menu = menu;
	}
	
	@JsonProperty("PRIORIDAD")
	public Integer getPrioridad() {
		return Prioridad;
	}
	public void setPrioridad(Integer prioridad) {
		Prioridad = prioridad;
	}
	
	@JsonProperty("TIPOOPCION")
	public Integer getTipo(){
		return Tipo;
	}
	
	public void setTipo(Integer tipo) {
		Tipo = tipo;
	}
	
	@JsonProperty("PARAMETRO_EVALUACION")
	public String getParametro() {
		return Parametro;
	}
	public void setParametro(String parametro) {
		Parametro = parametro;
	}	
	@JsonProperty("PARAMETRO_EVALUACION2")
	public String getParametro2() {
		return Parametro2;
	}
	public void setParametro2(String parametro2) {
		Parametro2 = parametro2;
	}	
	@JsonProperty("PARAMETRO_EVALUACION3")
	public String getParametro3() {
		return Parametro3;
	}
	public void setParametro3(String parametro3) {
		Parametro3 = parametro3;
	}	
	
	@JsonProperty("PARAMETRO_EVALUACION4")
	public String getParametro4() {
		return Parametro4;
	}
	public void setParametro4(String parametro4) {
		Parametro4 = parametro4;
	}	
	@JsonProperty("PARAMETRO_EVALUACION5")
	public String getParametro5() {
		return Parametro5;
	}
	public void setParametro5(String parametro5) {
		Parametro5 = parametro5;
	}	
	@JsonProperty("POLIZA")
	public String getPoliza() {
		return Poliza;
	}
	public void setPoliza(String poliza) {
		Poliza = poliza;
	}
	@JsonProperty("PARAMETRO1")
	public String getPureza1() {
		return Pureza1;
	}
	public void setPureza1(String pureza1) {
		Pureza1 = pureza1;
	}
	@JsonProperty("PARAMETRO2")
	public String getPureza2() {
		return Pureza2;
	}
	public void setPureza2(String pureza2) {
		Pureza2 = pureza2;
	}
	@JsonProperty("PARAMETRO3")
	public String getPureza3() {
		return Pureza3;
	}
	public void setPureza3(String pureza3) {
		Pureza3 = pureza3;
	}
	@JsonProperty("PARAMETRO4")
	public String getPureza4() {
		return Pureza4;
	}
	public void setPureza4(String pureza4) {
		Pureza4 = pureza4;
	}
	@JsonProperty("PARAMETRO5")
	public String getPureza5() {
		return Pureza5;
	}
	public void setPureza5(String pureza5) {
		Pureza5 = pureza5;
	}
	@JsonProperty("OPCIONES_APP")
	public String getCodigoAPP() {
		return CodigoAPP;
	}
	public void setCodigoAPP(String codigoAPP) {
		CodigoAPP = codigoAPP;
	}
	
}

