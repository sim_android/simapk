package com.example.sim.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DFamilias {
	
	private String Empresa;
	private Integer Codigo;
	private String Familia;
	
	@JsonProperty("EMPRESA")
	public String getEmpresa() {
		return Empresa;
	}
	public void setEmpresa(String empresa) {
		Empresa = empresa;
	}

	@JsonProperty("CODIGO")
	public Integer getCodigo() {
		return Codigo;
	}
	public void setCodigo(Integer codigo) {
		Codigo = codigo;
	}
	
	@JsonProperty("FAMILIA")
	public String getFamilia() {
		return Familia;
	}
	public void setFamilia(String familia) {
		Familia = familia;
	}
			
}
