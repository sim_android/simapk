package com.example.sim.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Mdepartamento {
	
	private Integer CodigoDepto;
	private String DescripcionDepto;
	private String Menu;
	
	
	public Mdepartamento() {
		CodigoDepto = 0;
		DescripcionDepto = "";
		Menu = "";
	}
	
	public Mdepartamento(Integer codigoDepto, String descripcionDepto,
			String menu) {
		CodigoDepto = codigoDepto;
		DescripcionDepto = descripcionDepto;
		Menu = menu;
	}
	
	@JsonProperty("CODIGODEPTO")
	public Integer getCodigoDepto() {
		return CodigoDepto;
	}
	public void setCodigoDepto(Integer codigoDepto) {
		CodigoDepto = codigoDepto;
	}
	@JsonProperty("DESCRIPCIONDEPTO")
	public String getDescripcionDepto() {
		return DescripcionDepto;
	}
	public void setDescripcionDepto(String descripcionDepto) {
		DescripcionDepto = descripcionDepto;
	}
	@JsonProperty("MENU")
	public String getMenu() {
		return Menu;
	}
	public void setMenu(String menu) {
		Menu = menu;
	}
	

}
