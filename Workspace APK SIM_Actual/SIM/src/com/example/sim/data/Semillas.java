package com.example.sim.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Semillas {
	
	private String Siembra;
	private String Material;
	private Integer Polinizacion;
	private String Produccion;
	private Integer MCosecha;
	private Integer Plantas;
	private Integer Postura;
	
	
	public Semillas() {
		Siembra = "";
		Material = "";
		Polinizacion = 0;
		Produccion = "";
		MCosecha = 0;
		Plantas = 0;
		Postura = 0;
		
	}
	
	public Semillas(String siembra, String material, Integer polinizacion, String produccion, Integer mcosecha,Integer plantas,Integer postura) {
		Siembra = siembra;
		Material = material;
		Polinizacion = polinizacion;
		Produccion = produccion;
		MCosecha = mcosecha;
		Plantas = plantas;
		Postura = postura;
	}
	
	@JsonProperty("SIEMBRA")
	public String getSiembra() {
		return Siembra;
	}
	public void setSiembra(String siembra) {
		Siembra = siembra;
	}
	
	@JsonProperty("MATERIAL")
	public String getMaterial() {
		return Material;
	}
	public void setMaterial(String material) {
		Material = material;
	}
	
	@JsonProperty("POLINIZACION")
	public Integer getPolinizacion() {
		return Polinizacion;
	}
	public void setPolinizacion(Integer polinizacion) {
		Polinizacion = polinizacion;
	}
	
	@JsonProperty("PRODUCCION")
	public String getProduccion() {
		return Produccion;
	}
	public void setProduccion(String produccion) {
		Produccion = produccion;
	}
	
	@JsonProperty("MCOSECHA")
	public Integer getMCosecha() {
		return MCosecha;
	}
	public void setMCosecha(Integer mcosecha) {
		MCosecha = mcosecha;
	}
	
	@JsonProperty("PLANTAS")
	public Integer getPlantas() {
		return Plantas;
	}
	public void setPlantas(Integer plantas) {
		Plantas = plantas;
	}
	
	@JsonProperty("PLANTASXBOLSA")
	public Integer getPostura() {
		return Postura;
	}
	public void setPostura(Integer postura) {
		Postura = postura;
	}

}
