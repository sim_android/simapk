package com.example.sim.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Dazucareras {
	
	private String Codigo;
	private String Siembra;
	private String Material;
	
	//metodos Getter y Setter	
	@JsonProperty("CODIGO")
	public String getCodigo() {
		return Codigo;
	}
	public void setCodigo(String codigo) {
		Codigo = codigo;
	}
	@JsonProperty("MATERIAL")
	public String getMaterial() {
		return Material;
	}
	public void setMaterial(String material) {
		Material = material;
	}
	@JsonProperty("SIEMBRA")
	public String getSiembra() {
		return Siembra;
	}
	public void setSiembra(String siembra) {
		Siembra = siembra;
	}		
}
