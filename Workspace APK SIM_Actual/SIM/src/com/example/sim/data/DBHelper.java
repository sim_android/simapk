package com.example.sim.data;

import java.lang.reflect.Field;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.util.Log;


public class DBHelper extends SQLiteOpenHelper {
	
	private static final int DATABASE_VERSION = 1;
	
	static final String TABLE_USUARIO = "usuario";
	static final String TABLE_DISPONSEM = "dispsemillas";
	static final String TABLE_MDEPARTAMENTO = "mdepartamento";
	static final String TABLE_MODULOS = "modulos";
	static final String TABLE_OPCIONES = "opciones";
	static final String TABLE_SEMILLAS = "semillas";
	static final String TABLE_DGUARDADOS = "dguardados";
	static final String TABLE_DISPONVEG = "disponibilidadvegetativo";
	static final String TABLE_DACCESO = "dacceso";
	static final String TABLE_ETIQUETAS = "etiquetavegetativo";
	static final String TABLE_JSONFILES = "jsonfiles";
	static final String TABLE_INFO = "info";
	static final String TABLE_DEMPLEADO = "dempleado";
	static final String TABLE_DBOLSAS = "dbolsas";
	static final String TABLE_DINVERNADERO = "dinvernadero";
	static final String TABLE_DUBICACIONES = "dubicaciones";
	static final String TABLE_DSIEMBRAVEGETATIVO = "dsiembravegetativo";
	static final String TABLE_DAZUCARERAS = "dazuracreras";
	static final String TABLE_DFILTROS = "filtros";
	static final String TABLE_QCPOLEN = "QCPolen";
	static final String TABLE_DFAMILIAS = "DFamilias";
	static final String TABLE_MANTENIMIENTO = "Mantenimiento";
	static final String TABLE_RIEGO = "Riego";
	static final String TABLE_INVESTIGACION = "Investigacion";
	
	
	
	private static final String DATABASE_NAME = "SIM.db";
	
	public DBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	public DBHelper(Context context, int version) {
		super(context, DATABASE_NAME, null, version);
	}
	
	@Override
	public void onCreate(SQLiteDatabase database) {
		
		/*
		String usuario_sql = "";
		String mempleado_sql = "";
		String mdepartamento_sql = "";
		String modulos_sql = "";
		String opciones_sql = "";
		String semillas_sql = "";
		*/
		
		String usuario_sql = getSqlQueryText(Usuario.class);
		String disponsem_sql = getSqlQueryText(DisponibilidadSemillas.class);
		String mdepartamento_sql = getSqlQueryText(Mdepartamento.class);
		String modulos_sql = getSqlQueryText(Modulos.class);
		String opciones_sql = getSqlQueryText(Opciones.class);
		String semillas_sql = getSqlQueryText(Semillas.class);
		String dacceso_sql = getSqlQueryText(Dacceso.class);
		String dguardados_sql = getSqlQueryText(Dguardados.class);
		String disponveg_sql = getSqlQueryText(DisponibilidadVegetativo.class);
		String etiquetas_sql = getSqlQueryText(EtiquetasVegetativo.class);
		String jsonfiles_sql = getSqlQueryText(JsonFiles.class);
		String dempleado_sql = getSqlQueryText(Dempleado.class);
		String dbolsas_sql = getSqlQueryText(Dbolsas.class);
		String dinvernadero_sql = getSqlQueryText(Dinvernaderos.class);
		String dubicaciones_sql = getSqlQueryText(DUbicaciones.class);
		String dsiembravegetativo_sql = getSqlQueryText(SiembraVegetativo.class);
		String dazucareras_sql = getSqlQueryText(Dazucareras.class);
		String dfiltros_sql = getSqlQueryText(Filtros.class);
		String QcPolen_sql = getSqlQueryText(QCPolen.class);
		String dfamilias_sql = getSqlQueryText(DFamilias.class);
		String mantenimiento_sql = getSqlQueryText(EMantenimiento.class);
		String riego_sql = getSqlQueryText(ERiego.class);
		String investigacion_sql = getSqlQueryText(EInvestigacion.class);
		
		usuario_sql = "CREATE TABLE " + TABLE_USUARIO + " (" + usuario_sql.substring(0, usuario_sql.length()-1) + ")";
		disponsem_sql = "CREATE TABLE " + TABLE_DISPONSEM + " (" + disponsem_sql.substring(0, disponsem_sql.length()-1) + ")";
		mdepartamento_sql = "CREATE TABLE " + TABLE_MDEPARTAMENTO + " (" + mdepartamento_sql.substring(0, mdepartamento_sql.length()-1) + ")";
		modulos_sql = "CREATE TABLE " + TABLE_MODULOS + " (" + modulos_sql.substring(0, modulos_sql.length()-1) + ")";
		opciones_sql = "CREATE TABLE " + TABLE_OPCIONES + " (" + opciones_sql.substring(0, opciones_sql.length()-1) + ")";
		semillas_sql = "CREATE TABLE " + TABLE_SEMILLAS + " (" + semillas_sql.substring(0, semillas_sql.length()-1) + ")";
		dguardados_sql = "CREATE TABLE " + TABLE_DGUARDADOS + " (" + dguardados_sql.substring(0, dguardados_sql.length()-1) + ")";
		disponveg_sql = "CREATE TABLE " + TABLE_DISPONVEG + " (" + disponveg_sql.substring(0, disponveg_sql.length()-1) + ")";
		dacceso_sql = "CREATE TABLE " + TABLE_DACCESO + " (" + dacceso_sql.substring(0, dacceso_sql.length()-1) + ")";
		etiquetas_sql = "CREATE TABLE " + TABLE_ETIQUETAS + " (" + etiquetas_sql.substring(0, etiquetas_sql.length()-1) + ")";
		jsonfiles_sql = "CREATE TABLE " + TABLE_JSONFILES + " (" + jsonfiles_sql.substring(0, jsonfiles_sql.length()-1) + ")";
		String info_sql = "CREATE TABLE " + TABLE_INFO + " (id integer primary key, last_updated text, check_count integer, agencia integer, producto integer, promocion integer, carrera integer)";
		dempleado_sql = "CREATE TABLE " + TABLE_DEMPLEADO + " (" + dempleado_sql.substring(0, dempleado_sql.length()-1) + ")";
		dbolsas_sql = "CREATE TABLE " + TABLE_DBOLSAS + " (" + dbolsas_sql.substring(0, dbolsas_sql.length()-1) + ")";
		dinvernadero_sql = "CREATE TABLE " + TABLE_DINVERNADERO + " (" + dinvernadero_sql.substring(0, dinvernadero_sql.length()-1) + ")";
		dubicaciones_sql = "CREATE TABLE " + TABLE_DUBICACIONES + " (" + dubicaciones_sql.substring(0, dubicaciones_sql.length()-1) + ")";
		dsiembravegetativo_sql = "CREATE TABLE " + TABLE_DSIEMBRAVEGETATIVO + " (" + dsiembravegetativo_sql.substring(0, dsiembravegetativo_sql.length()-1) + ")";
		dazucareras_sql = "CREATE TABLE " + TABLE_DAZUCARERAS + " (" + dazucareras_sql.substring(0, dazucareras_sql.length()-1) + ")";
		dfiltros_sql = "CREATE TABLE " + TABLE_DFILTROS + "(" + dfiltros_sql.substring(0, dfiltros_sql.length()-1) + ")"; 
		QcPolen_sql = "CREATE TABLE " + TABLE_QCPOLEN + "(" + QcPolen_sql.substring(0, QcPolen_sql.length()-1) + ")";
		dfamilias_sql = "CREATE TABLE " + TABLE_DFAMILIAS + "(" + dfamilias_sql.substring(0, dfamilias_sql.length()-1) + ")";
		mantenimiento_sql = "CREATE TABLE " + TABLE_MANTENIMIENTO + "(" + mantenimiento_sql.substring(0, mantenimiento_sql.length()-1) + ")";
		riego_sql = "CREATE TABLE " + TABLE_RIEGO + "(" + riego_sql.substring(0, riego_sql.length()-1) + ")";
		investigacion_sql = "CREATE TABLE " + TABLE_INVESTIGACION + "(" + investigacion_sql.substring(0, investigacion_sql.length()-1) + ")";
		
		database.execSQL(usuario_sql);
		database.execSQL(disponsem_sql);
		database.execSQL(mdepartamento_sql);
		database.execSQL(modulos_sql);
		database.execSQL(opciones_sql);
		database.execSQL(info_sql);
		database.execSQL(semillas_sql);
		database.execSQL(dguardados_sql);
		database.execSQL(disponveg_sql);
		database.execSQL(dacceso_sql);
		database.execSQL(etiquetas_sql);
		database.execSQL(jsonfiles_sql);
		database.execSQL(dempleado_sql);
		database.execSQL(dbolsas_sql);
		database.execSQL(dinvernadero_sql);
		database.execSQL(dubicaciones_sql);
		database.execSQL(dsiembravegetativo_sql);
		database.execSQL(dazucareras_sql);
		database.execSQL(dfiltros_sql);
		database.execSQL(QcPolen_sql);
		database.execSQL(dfamilias_sql);
		database.execSQL(mantenimiento_sql);
		database.execSQL(riego_sql);
		database.execSQL(investigacion_sql);

		Log.d("se han creado las tablas", "se han creado las tablas");
		Log.d("tablaUsuario", usuario_sql);
	}

	
	public String getSqlQueryText( Class<?> clase)
	{
		String query="";
		Field[] fl = clase.getDeclaredFields();
		for (Field f : fl) {
			try {
				String _name = f.getName();
				if (f.getType().equals(Bitmap.class)) {
					query += _name + " blob,";
				} else if (f.getType().equals(Integer.class)) {
					query += _name + " integer,";
				} else if (f.getType().equals(String.class)) {
					if (_name.equals("id")) {
						query += "id integer primary key, type integer,";	
					} else {
						query += _name + " text,";	
					}						
				}
			} catch (SecurityException e) {
			} catch (IllegalArgumentException e) {						
			}
		}
		
		return query;
	}
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(DBHelper.class.getName(),
				"Upgrading database from version " + oldVersion + " to "
						+ newVersion + ", which will destroy all old data");
				
					
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USUARIO);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_DISPONSEM);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_MDEPARTAMENTO);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_MODULOS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_OPCIONES);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SEMILLAS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_INFO);
		//db.execSQL("DROP TABLE IF EXISTS " + TABLE_DGUARDADOS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_DACCESO);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ETIQUETAS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_JSONFILES);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_DBOLSAS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_DINVERNADERO);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_DUBICACIONES);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_DSIEMBRAVEGETATIVO);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_DAZUCARERAS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_DFILTROS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_QCPOLEN);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_DFAMILIAS);
		//db.execSQL("DROP TABLE IF EXISTS " + TABLE_MANTENIMIENTO);
		//db.execSQL("DROP TABLE IF EXISTS " + TABLE_RIEGO);
		//db.execSQL("DROP TABLE IF EXISTS " + TABLE_INVESTIGACION);
		restaurarTablas(db);

	}
	
	
	public void restaurarTablas(SQLiteDatabase database) {
		
		String usuario_sql = getSqlQueryText(Usuario.class);
		String disponsem_sql = getSqlQueryText(DisponibilidadSemillas.class);
		String mdepartamento_sql = getSqlQueryText(Mdepartamento.class);
		String modulos_sql = getSqlQueryText(Modulos.class);
		String opciones_sql = getSqlQueryText(Opciones.class);
		String semillas_sql = getSqlQueryText(Semillas.class);
		String dacceso_sql = getSqlQueryText(Dacceso.class);
		//String dguardados_sql = getSqlQueryText(Dguardados.class);
		String etiquetas_sql = getSqlQueryText(EtiquetasVegetativo.class);
		String jsonfiles_sql = getSqlQueryText(JsonFiles.class);
		String dbolsas_sql = getSqlQueryText(Dbolsas.class);
		String dinvernadero_sql = getSqlQueryText(Dinvernaderos.class);
		String dubicaciones_sql = getSqlQueryText(DUbicaciones.class);
		String dsiembravegetativo_sql = getSqlQueryText(SiembraVegetativo.class);
		String dazucareras_sql = getSqlQueryText(Dazucareras.class);
		String dfiltros_sql = getSqlQueryText(Filtros.class);
		String QcPolen_sql = getSqlQueryText(QCPolen.class);
		String dfamilias_sql = getSqlQueryText(DFamilias.class);
		//String mantenimiento_sql = getSqlQueryText(EMantenimiento.class);
		//String riego_sql = getSqlQueryText(ERiego.class);
		//String investigacion_sql = getSqlQueryText(EInvestigacion.class);
		
		usuario_sql = "CREATE TABLE " + TABLE_USUARIO + " (" + usuario_sql.substring(0, usuario_sql.length()-1) + ")";
		disponsem_sql = "CREATE TABLE " + TABLE_DISPONSEM + " (" + disponsem_sql.substring(0, disponsem_sql.length()-1) + ")";
		mdepartamento_sql = "CREATE TABLE " + TABLE_MDEPARTAMENTO + " (" + mdepartamento_sql.substring(0, mdepartamento_sql.length()-1) + ")";
		modulos_sql = "CREATE TABLE " + TABLE_MODULOS + " (" + modulos_sql.substring(0, modulos_sql.length()-1) + ")";
		opciones_sql = "CREATE TABLE " + TABLE_OPCIONES + " (" + opciones_sql.substring(0, opciones_sql.length()-1) + ")";
		semillas_sql = "CREATE TABLE " + TABLE_SEMILLAS + " (" + semillas_sql.substring(0, semillas_sql.length()-1) + ")";
		//dguardados_sql = "CREATE TABLE " + TABLE_DGUARDADOS + " (" + dguardados_sql.substring(0, dguardados_sql.length()-1) + ")";
		dacceso_sql = "CREATE TABLE " + TABLE_DACCESO + " (" + dacceso_sql.substring(0, dacceso_sql.length()-1) + ")";
		etiquetas_sql = "CREATE TABLE " + TABLE_ETIQUETAS + " (" + etiquetas_sql.substring(0, etiquetas_sql.length()-1) + ")";
		jsonfiles_sql = "CREATE TABLE " + TABLE_JSONFILES + " (" + jsonfiles_sql.substring(0, jsonfiles_sql.length()-1) + ")";
		dbolsas_sql = "CREATE TABLE " + TABLE_DBOLSAS + " (" + dbolsas_sql.substring(0, dbolsas_sql.length()-1) + ")";
		dinvernadero_sql = "CREATE TABLE " + TABLE_DINVERNADERO + " (" + dinvernadero_sql.substring(0, dinvernadero_sql.length()-1) + ")";
		dubicaciones_sql = "CREATE TABLE " + TABLE_DUBICACIONES + " (" + dubicaciones_sql.substring(0, dubicaciones_sql.length()-1) + ")";
		dsiembravegetativo_sql = "CREATE TABLE " + TABLE_DSIEMBRAVEGETATIVO + " (" + dsiembravegetativo_sql.substring(0, dsiembravegetativo_sql.length()-1) + ")";
		dazucareras_sql = "CREATE TABLE " + TABLE_DAZUCARERAS + " (" + dazucareras_sql.substring(0, dazucareras_sql.length()-1) + ")";
		dfiltros_sql = "CREATE TABLE " + TABLE_DFILTROS + " (" + dfiltros_sql.substring(0, dfiltros_sql.length()-1)+ ")";
		QcPolen_sql = "CREATE TABLE " + TABLE_QCPOLEN + " (" + QcPolen_sql.substring(0, QcPolen_sql.length()-1)+ ")";
		dfamilias_sql = "CREATE TABLE " + TABLE_DFAMILIAS + "(" + dfamilias_sql.substring(0, dfamilias_sql.length()-1)+ ")";
		//mantenimiento_sql = "CREATE TABLE " + TABLE_MANTENIMIENTO + "(" + mantenimiento_sql.substring(0, mantenimiento_sql.length()-1)+ ")";
		//riego_sql = "CREATE TABLE " + TABLE_RIEGO + "(" + riego_sql.substring(0, riego_sql.length()-1)+ ")";
		//investigacion_sql = "CREATE TABLE " + TABLE_INVESTIGACION + "(" + investigacion_sql.substring(0, investigacion_sql.length()-1)+ ")";
		String info_sql = "CREATE TABLE " + TABLE_INFO + " (id integer primary key, last_updated text, check_count integer, agencia integer, producto integer, promocion integer, carrera integer)"; 			
		
		database.execSQL(usuario_sql);
		database.execSQL(disponsem_sql);
		database.execSQL(mdepartamento_sql);
		database.execSQL(modulos_sql);
		database.execSQL(opciones_sql);
		database.execSQL(info_sql);
		database.execSQL(semillas_sql);
		//database.execSQL(dguardados_sql);
		database.execSQL(dacceso_sql);
		database.execSQL(etiquetas_sql);
		database.execSQL(jsonfiles_sql);
		database.execSQL(dbolsas_sql);
		database.execSQL(dinvernadero_sql);
		database.execSQL(dubicaciones_sql);
		database.execSQL(dsiembravegetativo_sql);
		database.execSQL(dazucareras_sql);
		database.execSQL(dfiltros_sql);
		database.execSQL(QcPolen_sql);
		database.execSQL(dfamilias_sql);
		//database.execSQL(mantenimiento_sql);
		//database.execSQL(riego_sql);
		//database.execSQL(investigacion_sql);
		
		Log.d("se han creado las tablas", "se han creado las tablas");
		Log.d("tablaUsuario", usuario_sql);
		
	}
	
	
	public void NuevaTabla(SQLiteDatabase database , String tabla){
		
		if(tabla.equals(TABLE_USUARIO)){
			database.execSQL("DROP TABLE IF EXISTS " + TABLE_USUARIO);
			String usuario_sql = getSqlQueryText(Usuario.class);
			usuario_sql = "CREATE TABLE " + TABLE_USUARIO + " (" + usuario_sql.substring(0, usuario_sql.length()-1) + ")";
			database.execSQL(usuario_sql);
			}
		
		else if(tabla.equals(TABLE_DISPONSEM)){
			database.execSQL("DROP TABLE IF EXISTS " + TABLE_DISPONSEM);
			String disponsem_sql = getSqlQueryText(DisponibilidadSemillas.class);
			disponsem_sql = "CREATE TABLE " + TABLE_DISPONSEM + " (" + disponsem_sql.substring(0, disponsem_sql.length()-1) + ")";
			database.execSQL(disponsem_sql);
			}
		
		else if(tabla.equals(TABLE_MDEPARTAMENTO)){
			database.execSQL("DROP TABLE IF EXISTS " + TABLE_MDEPARTAMENTO);
			String mdepartamento_sql = getSqlQueryText(Mdepartamento.class);
			mdepartamento_sql = "CREATE TABLE " + TABLE_MDEPARTAMENTO + " (" + mdepartamento_sql.substring(0, mdepartamento_sql.length()-1) + ")";
			database.execSQL(mdepartamento_sql);
			}
		
		else if(tabla.equals(TABLE_MODULOS)){
			database.execSQL("DROP TABLE IF EXISTS " + TABLE_MODULOS);
			String modulos_sql = getSqlQueryText(Modulos.class);
			modulos_sql = "CREATE TABLE " + TABLE_MODULOS + " (" + modulos_sql.substring(0, modulos_sql.length()-1) + ")";
			database.execSQL(modulos_sql);			
			}
			
		else if(tabla.equals(TABLE_OPCIONES)){
			database.execSQL("DROP TABLE IF EXISTS " + TABLE_OPCIONES);
			String opciones_sql = getSqlQueryText(Opciones.class);
			opciones_sql = "CREATE TABLE " + TABLE_OPCIONES + " (" + opciones_sql.substring(0, opciones_sql.length()-1) + ")";
			database.execSQL(opciones_sql);
			}
		
		else if(tabla.equals(TABLE_SEMILLAS)){
			database.execSQL("DROP TABLE IF EXISTS " + TABLE_SEMILLAS);
			String semillas_sql = getSqlQueryText(Semillas.class);
			semillas_sql = "CREATE TABLE " + TABLE_SEMILLAS + " (" + semillas_sql.substring(0, semillas_sql.length()-1) + ")";
			database.execSQL(semillas_sql);
			}
		
		else if(tabla.equals(TABLE_DGUARDADOS)){
			database.execSQL("DROP TABLE IF EXISTS " + TABLE_DGUARDADOS);
			String dguardados_sql = getSqlQueryText(Dguardados.class);
			dguardados_sql = "CREATE TABLE " + TABLE_DGUARDADOS + " (" + dguardados_sql.substring(0, dguardados_sql.length()-1) + ")";
			database.execSQL(dguardados_sql);	
			}
		
		else if(tabla.equals(TABLE_DISPONVEG)){
			database.execSQL("DROP TABLE IF EXISTS " + TABLE_DISPONVEG);
			String disponveg_sql = getSqlQueryText(DisponibilidadVegetativo.class);
			disponveg_sql = "CREATE TABLE " + TABLE_DISPONVEG + " (" + disponveg_sql.substring(0, disponveg_sql.length()-1) + ")";
			database.execSQL(disponveg_sql);	
			}
			
		else if(tabla.equals(TABLE_DACCESO)){
			database.execSQL("DROP TABLE IF EXISTS " + TABLE_DACCESO);
			String dacceso_sql = getSqlQueryText(Dacceso.class);
			dacceso_sql = "CREATE TABLE " + TABLE_DACCESO + " (" + dacceso_sql.substring(0, dacceso_sql.length()-1) + ")";
			database.execSQL(dacceso_sql);
			}
		
		else if(tabla.equals(TABLE_ETIQUETAS)){
			database.execSQL("DROP TABLE IF EXISTS " + TABLE_ETIQUETAS);
			String etiquetas_sql = getSqlQueryText(EtiquetasVegetativo.class);
			etiquetas_sql = "CREATE TABLE " + TABLE_ETIQUETAS + " (" + etiquetas_sql.substring(0, etiquetas_sql.length()-1) + ")";
			database.execSQL(etiquetas_sql);
			}
			
		else if(tabla.equals(TABLE_JSONFILES)){
			database.execSQL("DROP TABLE IF EXISTS " + TABLE_JSONFILES);
			String jsonfiles_sql = getSqlQueryText(JsonFiles.class);
			jsonfiles_sql = "CREATE TABLE " + TABLE_JSONFILES + " (" + jsonfiles_sql.substring(0, jsonfiles_sql.length()-1) + ")";
			database.execSQL(jsonfiles_sql);
			}
		
		else if(tabla.equals(TABLE_DEMPLEADO)){
			database.execSQL("DROP TABLE IF EXISTS " + TABLE_DEMPLEADO);
			String dempleado_sql = getSqlQueryText(Dempleado.class);
			dempleado_sql = "CREATE TABLE " + TABLE_DEMPLEADO + " (" + dempleado_sql.substring(0, dempleado_sql.length()-1) + ")";
			database.execSQL(dempleado_sql);
			}
	
		else if(tabla.equals(TABLE_DBOLSAS)){
			database.execSQL("DROP TABLE IF EXISTS " + TABLE_DBOLSAS);
			String dbolsas_sql = getSqlQueryText(Dbolsas.class);
			dbolsas_sql = "CREATE TABLE " + TABLE_DBOLSAS + " (" + dbolsas_sql.substring(0, dbolsas_sql.length()-1) + ")";
			database.execSQL(dbolsas_sql);
			}	

		else if(tabla.equals(TABLE_DINVERNADERO)){
			database.execSQL("DROP TABLE IF EXISTS " + TABLE_DINVERNADERO);
			String dinvernadero_sql = getSqlQueryText(Dinvernaderos.class);
			dinvernadero_sql = "CREATE TABLE " + TABLE_DINVERNADERO + " (" + dinvernadero_sql.substring(0, dinvernadero_sql.length()-1) + ")";
			database.execSQL(dinvernadero_sql);
			}
		
		else if(tabla.equals(TABLE_DUBICACIONES)){
			database.execSQL("DROP TABLE IF EXISTS " + TABLE_DUBICACIONES);
			String dubicaciones_sql = getSqlQueryText(DUbicaciones.class);
			dubicaciones_sql = "CREATE TABLE " + TABLE_DUBICACIONES + " (" + dubicaciones_sql.substring(0, dubicaciones_sql.length()-1) + ")";
			database.execSQL(dubicaciones_sql);
			}
		
		else if(tabla.equals(TABLE_DSIEMBRAVEGETATIVO)){
			database.execSQL("DROP TABLE IF EXISTS " + TABLE_DSIEMBRAVEGETATIVO);
			String dsiembravegetativo_sql = getSqlQueryText(SiembraVegetativo.class);
			dsiembravegetativo_sql = "CREATE TABLE " + TABLE_DSIEMBRAVEGETATIVO + " (" + dsiembravegetativo_sql.substring(0, dsiembravegetativo_sql.length()-1) + ")";
			database.execSQL(dsiembravegetativo_sql);
			}
		
		else if(tabla.equals(TABLE_DAZUCARERAS)){
			database.execSQL("DROP TABLE IF EXISTS " + TABLE_DAZUCARERAS);
			String dazucareras_sql = getSqlQueryText(Dazucareras.class);
			dazucareras_sql = "CREATE TABLE " + TABLE_DAZUCARERAS + " (" + dazucareras_sql.substring(0, dazucareras_sql.length()-1) + ")";
			database.execSQL(dazucareras_sql);
			}		
		
		else if(tabla.equals(TABLE_DFILTROS)){
			database.execSQL("DROP TABLE IF EXISTS " + TABLE_DFILTROS);
			String dfiltros_sql = getSqlQueryText(Filtros.class);
			dfiltros_sql = "CREATE TABLE " + TABLE_DFILTROS + " (" + dfiltros_sql.substring(0, dfiltros_sql.length()-1) + ")";
			database.execSQL(dfiltros_sql);
			}	
		
		else if(tabla.equals(TABLE_QCPOLEN)){
			database.execSQL("DROP TABLE IF EXISTS " + TABLE_QCPOLEN);
			String QcPolen_sql = getSqlQueryText(QCPolen.class);
			QcPolen_sql = "CREATE TABLE " + TABLE_QCPOLEN + " (" + QcPolen_sql.substring(0, QcPolen_sql.length()-1) + ")";
			database.execSQL(QcPolen_sql);
			}
		
		else if(tabla.equals(TABLE_DFAMILIAS)){
			database.execSQL("DROP TABLE IF EXISTS " + TABLE_DFAMILIAS);
			String dfamilias_sql = getSqlQueryText(DFamilias.class);
			dfamilias_sql = "CREATE TABLE " + TABLE_DFAMILIAS + " (" + dfamilias_sql.substring(0, dfamilias_sql.length()-1) + ")";
			database.execSQL(dfamilias_sql);
			}
		
		else if(tabla.equals(TABLE_MANTENIMIENTO)){
			database.execSQL("DROP TABLE IF EXISTS " + TABLE_MANTENIMIENTO);
			String mantenimiento_sql = getSqlQueryText(EMantenimiento.class);
			mantenimiento_sql = "CREATE TABLE " + TABLE_MANTENIMIENTO + " (" + mantenimiento_sql.substring(0, mantenimiento_sql.length()-1) + ")";
			database.execSQL(mantenimiento_sql);
			}
		
		else if(tabla.equals(TABLE_RIEGO)){
			database.execSQL("DROP TABLE IF EXISTS " + TABLE_RIEGO);
			String riego_sql = getSqlQueryText(ERiego.class);
			riego_sql = "CREATE TABLE " + TABLE_RIEGO + " (" + riego_sql.substring(0, riego_sql.length()-1) + ")";
			database.execSQL(riego_sql);
			}
		
		else if(tabla.equals(TABLE_INVESTIGACION)){
			database.execSQL("DROP TABLE IF EXISTS " + TABLE_INVESTIGACION);
			String investigacion_sql = getSqlQueryText(EInvestigacion.class);
			investigacion_sql = "CREATE TABLE " + TABLE_INVESTIGACION + " (" + investigacion_sql.substring(0, investigacion_sql.length()-1) + ")";
			database.execSQL(investigacion_sql);
			}
	}

}