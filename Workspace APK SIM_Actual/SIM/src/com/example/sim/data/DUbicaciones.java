package com.example.sim.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DUbicaciones {

	private  String Codigo;
	private  String Descripcion;
	
	@JsonProperty("CODIGOUBICACION")
	public String getCodigo() {
		return Codigo;
	}
	public void setCodigo(String codigo) {
		Codigo = codigo;
	}
	
	@JsonProperty("DESCRIPCIONUBICACION")
	public String getDescripcion() {
		return Descripcion;
	}
	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}

	
}
