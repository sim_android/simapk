package com.example.sim.data;

// Estructura de JSON para Riego (E..Riego)
public class ERiego {
	
	private Integer CodigoEmpleado;
	private Integer CodigoDepto;
	private Integer Actividad;
	private String Fecha;
	private String HoraIncio;
	private String HoraFin;
	private Integer Supervisor;
	private Integer ActividadFinalizada;
	private String Parametro;
	private String Siembra;
	private String Ubicacion;
	private Integer Hora;
	private Integer Min;
	private String H_Manual;
	private String FP;
	private String CorrelativoEmpleado;
	
	public Integer getCodigoEmpleado() {
		return CodigoEmpleado;
	}
	public void setCodigoEmpleado(Integer codigoEmpleado) {
		CodigoEmpleado = codigoEmpleado;
	}
	public Integer getCodigoDepto() {
		return CodigoDepto;
	}
	public void setCodigoDepto(Integer codigoDepto) {
		CodigoDepto = codigoDepto;
	}	
	public Integer getActividad() {
		return Actividad;
	}
	public void setActividad(Integer actividad) {
		Actividad = actividad;
	}	
	public String getFecha() {
		return Fecha;
	}
	public void setFecha(String fecha) {
		this.Fecha = fecha;
	}	
	public String getHoraIncio() {
		return HoraIncio;
	}
	public void setHoraIncio(String horaIncio) {
		HoraIncio = horaIncio;
	}
	public String getHoraFin() {
		return HoraFin;
	}
	public void setHoraFin(String horaFin) {
		HoraFin = horaFin;
	}	
	public Integer getSupervisor() {
		return Supervisor;
	}
	public void setSupervisor(Integer supervisor) {
		Supervisor = supervisor;
	}	
	public Integer getActividadFinalizada() {
		return ActividadFinalizada;
	}
	public void setActividadFinalizada(Integer actividadFinalizada) {
		ActividadFinalizada = actividadFinalizada;
	}	
	public String getParametro() {
		return Parametro;
	}
	public void setParametro(String parametro) {
		Parametro = parametro;
	}	
	public String getSiembra() {
		return Siembra;
	}
	public void setSiembra(String siembra) {
		Siembra = siembra;
	}
	public String getUbicacion() {
		return Ubicacion;
	}
	public void setUbicacion(String ubicacion) {
		Ubicacion = ubicacion;
	}
	public Integer getHora() {
		return Hora;
	}
	public void setHora(Integer hora) {
		Hora = hora;
	}
	public Integer getMin() {
		return Min;
	}
	public void setMin(Integer min) {
		Min = min;
	}		
	public String getH_Manual() {
		return H_Manual;
	}
	public void setH_Manual(String h_manual) {
		H_Manual = h_manual;
	}
	
	public String getFP() {
		return FP;
	}
	public void setFP(String fp) {
		FP = fp;
	}
	public String getCorrelativoEmpleado() {
		return CorrelativoEmpleado;
	}
	public void setCorrelativoEmpleado(String correlativoEmpleado) {
		CorrelativoEmpleado = correlativoEmpleado;
	}
	
	
	public String toString(){
		String s ="";
				
		s = this.CodigoEmpleado + "\n" + this.CodigoDepto + "\n" + this.Actividad + "\n" + this.Fecha + "\n" + this.HoraIncio 
				+"\n" + this.HoraFin + "\n" + this.Supervisor + "\n" + this.ActividadFinalizada + "\n" + this.Parametro + "\n" + this.Siembra
				+ "\n" + this.Ubicacion + "\n" + this.Hora + "\n" + this.Min + "\n" + this.H_Manual + "\n" + this.FP 
		+ "\n" + this.CorrelativoEmpleado;
		
		return s;
	}
	

}

