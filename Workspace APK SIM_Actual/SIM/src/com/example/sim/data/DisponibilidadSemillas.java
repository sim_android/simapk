package com.example.sim.data;

public class DisponibilidadSemillas {
	
	private Integer Actividad;
	private String Siembra;
	private String Muestra;
	private String Fecha;
	private String Hora;
	private Integer Supervisor;

	
	
	public DisponibilidadSemillas() {
		Actividad = 0;
		Siembra = "0";
		Muestra = "0";
		Fecha = "0";
		Hora = "0";
		Supervisor = 0;
	}
	
	public DisponibilidadSemillas(Integer actividad,String siembra,String muestra,String fecha,String hora,Integer supervisor) {
		Actividad = actividad;
		Siembra = siembra;
		Muestra = muestra;
		Fecha = fecha;
		Hora = hora;
		Supervisor = supervisor;
	}
	
	
	public Integer getActividad() {
		return Actividad;
	}
	public void setActividad(Integer actividad) {
		 Actividad = actividad;
	}
	
	public String getSiembra() {
		return Siembra;
	}
	public void setSiembra(String siembra) {
		Siembra = siembra;
	}
	
	public String getMuestra() {
		return Muestra;
	}
	public void setMuestra(String muestra) {
		Muestra = muestra;
	}
	
	public String getFecha() {
		return Fecha;
	}
	public void setFecha(String fecha) {
		Fecha = fecha;
	}
	
	public String getHora() {
		return Hora;
	}
	public void setHora(String hora) {
		Hora = hora;
	}
	
	public Integer getSupervisor() {
		return Supervisor;
	}
	public void setSupervisor(Integer supervisor) {
		 Supervisor = supervisor;
	}
	
	
	
	
}
