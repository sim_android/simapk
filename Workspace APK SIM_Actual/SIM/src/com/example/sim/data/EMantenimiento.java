package com.example.sim.data;

// Estructura de JSON para Mantenimiento (E..Mantenimiento)
public class EMantenimiento {
	
	private Integer CodigoEmpleado;
	private Integer CodigoDepto;
	private Integer Actividad;
	private String Fecha;
	private String HoraIncio;
	private String HoraFin;
	private Integer Supervisor;
	private Integer ActividadFinalizada;
	private String TextoP1;
	private String TextoP2;
	private String TextoP3;
	private String Parametro;
	private String Parametro2;
	private String Parametro3;
	private String Ubicacion;
	private Integer Hora;
	private Integer Min;
	private String CorrelativoEmpleado;
	
	public Integer getCodigoEmpleado() {
		return CodigoEmpleado;
	}
	public void setCodigoEmpleado(Integer codigoEmpleado) {
		CodigoEmpleado = codigoEmpleado;
	}
	public Integer getCodigoDepto() {
		return CodigoDepto;
	}
	public void setCodigoDepto(Integer codigoDepto) {
		CodigoDepto = codigoDepto;
	}	
	public Integer getActividad() {
		return Actividad;
	}
	public void setActividad(Integer actividad) {
		Actividad = actividad;
	}	
	public String getFecha() {
		return Fecha;
	}
	public void setFecha(String fecha) {
		this.Fecha = fecha;
	}	
	public String getHoraIncio() {
		return HoraIncio;
	}
	public void setHoraIncio(String horaIncio) {
		HoraIncio = horaIncio;
	}
	public String getHoraFin() {
		return HoraFin;
	}
	public void setHoraFin(String horaFin) {
		HoraFin = horaFin;
	}	
	public Integer getSupervisor() {
		return Supervisor;
	}
	public void setSupervisor(Integer supervisor) {
		Supervisor = supervisor;
	}	
	public Integer getActividadFinalizada() {
		return ActividadFinalizada;
	}
	public void setActividadFinalizada(Integer actividadFinalizada) {
		ActividadFinalizada = actividadFinalizada;
	}
	public String getTextoP1() {
		return TextoP1;
	}
	public void setTextoP1(String textop1) {
		TextoP1 = textop1;
	}	
	public String getTextoP2() {
		return TextoP2;
	}
	public void setTextoP2(String textop2) {
		TextoP2 = textop2;
	}
	public String getTextoP3() {
		return TextoP3;
	}
	public void setTextoP3(String textop3) {
		TextoP3 = textop3;
	}	
	public String getParametro() {
		return Parametro;
	}
	public void setParametro(String parametro) {
		Parametro = parametro;
	}	
	public String getParametro2() {
		return Parametro2;
	}
	public void setParametro2(String parametro2) {
		Parametro2 = parametro2;
	}
	public String getParametro3() {
		return Parametro3;
	}
	public void setParametro3(String parametro3) {
		Parametro3 = parametro3;
	}
	public String getUbicacion() {
		return Ubicacion;
	}
	public void setUbicacion(String ubicacion) {
		Ubicacion = ubicacion;
	}
	public Integer getHora() {
		return Hora;
	}
	public void setHora(Integer hora) {
		Hora = hora;
	}
	public Integer getMin() {
		return Min;
	}
	public void setMin(Integer min) {
		Min = min;
	}
	public String getCorrelativoEmpleado() {
		return CorrelativoEmpleado;
	}
	public void setCorrelativoEmpleado(String correlativoEmpleado) {
		CorrelativoEmpleado = correlativoEmpleado;
	}
	
  	
	public String toString(){
		String s ="";
				
		s = this.CodigoEmpleado + "\n" + this.CodigoDepto + "\n" + this.Actividad + "\n" + this.Fecha + "\n" + this.HoraIncio 
				+"\n" + this.HoraFin + "\n" + this.Supervisor + "\n" + this.ActividadFinalizada + "\n" + this.Parametro + "\n" + this.Parametro2
				+ "\n" + this.Ubicacion + "\n" + this.Hora + "\n" + this.Min
		
		+ "\n" + this.CorrelativoEmpleado;
		
		return s;
	}
	

}
