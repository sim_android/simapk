package com.example.sim.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class QCPolen {

	private Integer Codigo;
	private String Descripcion;
	private Integer Calificativo;
	
	@JsonProperty("CODIGO")
	public Integer getCodigo() {
		return Codigo;
	}
	public void setCodigo(Integer codigo) {
		Codigo = codigo;
	}
	
	@JsonProperty("DESCRIPCION")
	public String getDescripcion() {
		return Descripcion;
	}
	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}
	
	@JsonProperty("CALIFICATIVO")
	public Integer getCalificativo() {
		return Calificativo;
	}
	public void setCalificativo(Integer calificativo) {
		Calificativo = calificativo;
	}

}
