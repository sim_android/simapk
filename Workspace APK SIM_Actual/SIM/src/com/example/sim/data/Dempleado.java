package com.example.sim.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Dempleado {
	private String Empresa;
	private Integer CodigoEmpleado;
	private Integer CodigoDepto;
	private String Responsable;
	private String Nombre;
	private String Estado;
	
	@JsonProperty("EMPRESA")
	public String getEmpresa() {
		return Empresa;
	}
	public void setEmpresa(String empresa) {
		Empresa = empresa;
	}
	
	@JsonProperty("CODIGOEMPLEADO")
	public Integer getCodigoEmpleado() {
		return CodigoEmpleado;
	}
	public void setCodigoEmpleado(Integer codigoempleado) {
		CodigoEmpleado = codigoempleado;
	}
	
	@JsonProperty("CODIGODEPTO")
	public Integer getCodigoDepto() {
		return CodigoDepto;
	}
	public void setCodigoDepto(Integer codigoDepto) {
		CodigoDepto = codigoDepto;
	}
	
	@JsonProperty("RESPONSABLE")
	public String getResponsable() {
		return Responsable;
	}
	public void setResponsable(String responsable) {
		Responsable = responsable;
	}
	
	@JsonProperty("NOMBRE")
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	@JsonProperty("ESTADO")
	public String getEstado() {
		return Estado;
	}
	public void setEstado(String estado) {
		Estado = estado;
	}
	
}
