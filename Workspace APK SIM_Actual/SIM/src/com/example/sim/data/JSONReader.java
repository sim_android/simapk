package com.example.sim.data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;



public class JSONReader {

	private Context ctx;
	public final static int URL = 1;
	public final static int FILE = 2;
	
	public JSONReader(Context ctx){
		this.ctx = ctx;
	}
	
	/*
	public ArrayList ReadFromSDCard(int clase, String file){
		
		JSONArray _json = null;
		ArrayList _data = new ArrayList();
		try {
			
			_json = readJSONArrayFromInputStream((ctx.getAssets()).open(file));
			//Log.d("JSON:", _json.toString().trim());
			ObjectMapper mapper = new ObjectMapper();

			 switch (clase) {
			 	case 1:	_data = mapper.readValue(_json.toString().trim(), new TypeReference<ArrayList<Pantallas>>(){});
			 			break;
			 	case 2: _data = mapper.readValue(_json.toString().trim(), new TypeReference<ArrayList<Elemento>>(){});
	 					break;
			 	case 3: _data = mapper.readValue(_json.toString().trim(), new TypeReference<ArrayList<Phones>>(){});
					break;
			 	case 4: _data = mapper.readValue(_json.toString().trim(), new TypeReference<ArrayList<Comments>>(){});
					break;
			 	case 5: _data = mapper.readValue(_json.toString().trim(), new TypeReference<ArrayList<Place>>(){});
					break;
			 	case 6: _data = mapper.readValue(_json.toString().trim(), new TypeReference<ArrayList<Subcategories>>(){});
					break;
					
			 }

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return _data;
	}
	*/
	
	public ArrayList ReadFromSDCard(int clase, String file){
		
		JSONArray _json = null;
		ArrayList _data = new ArrayList();
				
		
		try {

			 File dir = Environment.getExternalStorageDirectory();
		     File yourFile = new File(dir,file);
		     
		     if(yourFile.exists())
		     {
		     
			     FileInputStream stream = new FileInputStream(yourFile);  
			     _json = readJSONArrayFromInputStream(stream);
				 ObjectMapper mapper = new ObjectMapper();
				 switch (clase) {
				 	case 1:	_data = mapper.readValue(_json.toString().trim(), new TypeReference<ArrayList<Usuario>>(){});
				 			break;		
				 	case 2:	_data = mapper.readValue(_json.toString().trim(), new TypeReference<ArrayList<Semillas>>(){});
		 					break;	
				 	case 3:	_data = mapper.readValue(_json.toString().trim(), new TypeReference<ArrayList<Modulos>>(){});
 							break;	
				 	case 4:	_data = mapper.readValue(_json.toString().trim(), new TypeReference<ArrayList<Opciones>>(){});
 							break;	
				 	case 5:	_data = mapper.readValue(_json.toString().trim(), new TypeReference<ArrayList<Dacceso>>(){});
 							break;	
				 	case 6:	_data = mapper.readValue(_json.toString().trim(), new TypeReference<ArrayList<RangoEtiqueta>>(){});
							break;
				 	case 7:	_data = mapper.readValue(_json.toString().trim(), new TypeReference<ArrayList<Dempleado>>(){});
				 			break;
				 	case 8:	_data = mapper.readValue(_json.toString().trim(), new TypeReference<ArrayList<Dbolsas>>(){});
				 			break;
				 	case 9:	_data = mapper.readValue(_json.toString().trim(), new TypeReference<ArrayList<Dinvernaderos>>(){});
				 			break;
				 	case 10: _data = mapper.readValue(_json.toString().trim(), new TypeReference<ArrayList<SiembraVegetativo>>(){});
				 			break;
				 	case 11: _data = mapper.readValue(_json.toString().trim(), new TypeReference<ArrayList<Dazucareras>>(){});
				 			break;
				 	case 12: _data = mapper.readValue(_json.toString().trim(), new TypeReference<ArrayList<Filtros>>(){});
				 			break;
				 	case 13: _data = mapper.readValue(_json.toString().trim(), new TypeReference<ArrayList<SemillasAcond>>(){});
		 					break;
				 	case 14: _data = mapper.readValue(_json.toString().trim(), new TypeReference<ArrayList<QCPolen>>(){});
		 					break;
				 	case 15: _data = mapper.readValue(_json.toString().trim(), new TypeReference<ArrayList<DFamilias>>(){});
 							break;
				 	case 16: _data = mapper.readValue(_json.toString().trim(), new TypeReference<ArrayList<Mdepartamento>>(){});
				 			break;
				 	case 17: _data = mapper.readValue(_json.toString().trim(), new TypeReference<ArrayList<DUbicaciones>>(){});
		 					break;
				 }
				 stream.close();
		     }else{
		    	 Log.d("no existe", "no existe");
		     }

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		return _data;
		
	}
	
	public JSONArray readJSONArrayFromInputStream(InputStream is) {		
		String result="";
		JSONArray json = null;
				
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(is,Charset.forName("UTF-8")),8);
			//BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
	        StringBuilder sb = new StringBuilder();
	        String line = null;
	        while ((line = reader.readLine()) != null) {
	        	byte[] utf8 = line.getBytes("UTF-8");
		        line = new String(utf8, "UTF-8");
	        	sb.append(line + "\n");
	        }
	        is.close();
	        result = sb.toString().trim();
	       }catch(Exception e){
	        Log.e("log_tag", "Error converting result "+e.toString().trim());
	    }
		
		try{	
			json = new JSONArray(result);			  
		}catch(JSONException e){
			Log.e("log_tag", "Error parsing data " + e.toString().trim());
		}
		return json;		
		
	}
	
	
}
