package com.example.sim.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Filtros {

	private Integer Id;
	private String FechaSuccion;
	private String Material;
	private String NoSiembra;
	
	
	@JsonProperty("ID")
	public Integer getId() {
		return Id;
	}
	public void setId(Integer id) {
		Id = id;
	}
	@JsonProperty("FECHASUCCION")
	public String getFechaSuccion() {
		return FechaSuccion;
	}
	public void setFechaSuccion(String fecha) {
		FechaSuccion = fecha;
	}
	@JsonProperty("MATERIAL")
	public String getMaterial() {
		return Material;
	}
	public void setMaterial(String material) {
		Material = material;
	}
	@JsonProperty("NOSIEMBRA")
	public String getNoSiembra() {
		return NoSiembra;
	}
	public void setNoSiembra(String siembra) {
		NoSiembra = siembra;
	}
				
}
