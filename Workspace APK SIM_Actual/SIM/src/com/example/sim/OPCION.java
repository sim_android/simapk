package com.example.sim;

import java.util.ArrayList;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.example.floricultura.R;
import com.example.sim.adapters.DatosLista;
import com.example.sim.adapters.ListAdapter;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.Opciones;


public class OPCION extends ActionBarActivity {
	
	private MyApp appState;	
	private DBAdapter db;
	ArrayList<DatosLista> listado = new ArrayList<DatosLista>();
	String id;
	String UsuarioLogueado;
	int CodigoModulo;
	int Codigodepartamento;
	int CodigoEmpleado;
	int CodigoOpcion;
	int CodigoFamilia = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_opcion);
		
		
	    appState = ((MyApp)getApplicationContext());   
	    db = appState.getDb();
	    db.open();
		
		Bundle extras = getIntent().getExtras();
		CodigoModulo = extras.getInt("CodigoModulo");
	    CodigoEmpleado = extras.getInt("CodigoEmpleado");
	    Codigodepartamento = extras.getInt("Codigodepartamento");
	    CodigoFamilia = extras.getInt("CodigoFamilia");
	    UsuarioLogueado = extras.getString("UsuarioLogueado");
	    
	    ListView listadoVista = (ListView)findViewById(R.id.ListOpciones);
		listado = getDatos();
					
		ListAdapter adaptador = new ListAdapter(this, R.layout.filas_lista, listado);
        listadoVista.setAdapter(adaptador);
        
        
        listadoVista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {   
            	DatosLista dato = (DatosLista) parent.getItemAtPosition(position);
            	try{
            	 CodigoOpcion = Integer.parseInt(dato.getCampo1().trim());
            	 String Inicio = getOpcion(""+ CodigoOpcion);
            	 Class newclass = null;
     			 String activityString = "com.example.sim."+ Inicio;    		
  			     newclass = Class.forName(activityString);		   
  			     
            	Intent i = new Intent(getApplicationContext(), newclass);
           	 	i.putExtra("Codigodepartamento", Codigodepartamento);
           	    i.putExtra("CodigoEmpleado", CodigoEmpleado );
           	    i.putExtra("CodigoModulo",  CodigoModulo);
           	    i.putExtra("CodigoFamilia", CodigoFamilia);
           	    i.putExtra("CodigoOpcion", Integer.parseInt(dato.getCampo1().trim()));
           	    i.putExtra("NombreOpcion", dato.getCampo2());
           	    i.putExtra("UsuarioLogueado", UsuarioLogueado);
           	    startActivity(i);
            	
            }catch (Exception e) {
 			e.printStackTrace();
        }
        }   	 
        });
        
        
        Button btnBack = (Button) findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
 
            	onBackPressed();
            }
        });
        
        
        Button btnInicio = (Button) findViewById(R.id.btnInicio);
        btnInicio.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Intent intent = new Intent(getApplicationContext(), MAINACTIVITY.class);
           	 	startActivity(intent);
            }
        });


	}		
	
	// Se obtiene el Menu desde la Opcion que se envia en el parametro
	public String getOpcion(String dato){
		 Cursor Opcion = null ;
		 Opcion = this.db.getOpciones("Opcion = " +dato);
		 String tmp = null;	
		 if(Opcion.getCount()>0){
			 for(int i=0 ; i<Opcion.getCount();i++){	 
				 Opciones op = db.getOpcionesFromCursor(Opcion, i);
				 tmp = op.getMenu();				
			 	} 
		 	}
		 Opcion.close();
		 return tmp;
		}
	
	public ArrayList<DatosLista> getDatos(){
		ArrayList<DatosLista> listaTemp = new ArrayList<DatosLista>();
		listaTemp.add(new DatosLista("Opcion", "Descripcion", ""));
		

		 Cursor COpciones= null ;
		 COpciones = this.db.getOpciones("CodigoDepto=" + CodigoModulo);
		 
		 if(COpciones.getCount()>0){
			 for(int i=0 ; i<COpciones.getCount();i++){	         	
				 Opciones m = db.getOpcionesFromCursor(COpciones, i);  				 
				 listaTemp.add(new DatosLista(m.getOpcion()+ "", m.getDescripcion().trim(), ""));			 
		     } 
		 }
		 COpciones.close();
		return listaTemp;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.opciones, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}


/*
 
            	 if(CodigoModulo == 108 || CodigoModulo == 208){
            		
            		Intent intent = new Intent(getApplicationContext(), Ausencias.class);
	           	 	intent.putExtra("Codigodepartamento", Codigodepartamento);
	           	    intent.putExtra("CodigoEmpleado", CodigoEmpleado );
	           	    intent.putExtra("CodigoModulo",  CodigoModulo);
	           	    intent.putExtra("CodigoOpcion", Integer.parseInt(dato.getCampo1().trim()));
	           	    intent.putExtra("NombreOpcion", dato.getCampo2());
	           	    startActivity(intent);
	           	    
            		} else if(Codigodepartamento == 1){
            			
            			if(dato.getCampo2().contains("LECTURA UNO")) {
            				Intent intent = new Intent(getApplicationContext(), CapturaEtiquetas.class);
    		           	 	intent.putExtra("Codigodepartamento", Codigodepartamento);
    		           	    intent.putExtra("CodigoEmpleado", CodigoEmpleado );
    		           	    intent.putExtra("CodigoModulo",  CodigoModulo);
    		           	    intent.putExtra("CodigoOpcion", Integer.parseInt(dato.getCampo1().trim()));
    		           	    intent.putExtra("NombreOpcion", dato.getCampo2());
    		           	    intent.putExtra("EsCorte", 1);
    		           	startActivity(intent);	
            				
            			}else if(CodigoOpcion == 10919101) {
            				Intent intent = new Intent(getApplicationContext(), CuadreCorte.class);
    		           	 	intent.putExtra("Codigodepartamento", Codigodepartamento);
    		           	    intent.putExtra("CodigoEmpleado", CodigoEmpleado );
    		           	    intent.putExtra("CodigoModulo",  CodigoModulo);
    		           	    intent.putExtra("CodigoOpcion", Integer.parseInt(dato.getCampo1().trim()));
    		           	    intent.putExtra("NombreOpcion", dato.getCampo2());
    		           	    intent.putExtra("EsCorte", 1);
    		           	startActivity(intent);
    		           	
            			}else if(CodigoOpcion == 10919104) {
            				Intent intent = new Intent(getApplicationContext(), ConsultaEtiqueta.class);
    		           	 	intent.putExtra("Codigodepartamento", Codigodepartamento);
    		           	    intent.putExtra("CodigoEmpleado", CodigoEmpleado );
    		           	    intent.putExtra("CodigoModulo",  CodigoModulo);
    		           	    intent.putExtra("CodigoOpcion", Integer.parseInt(dato.getCampo1().trim()));
    		           	    intent.putExtra("NombreOpcion", dato.getCampo2());
    		           	    intent.putExtra("EsCorte", 1);
    		           	startActivity(intent);
    		           	
            			}else if(CodigoOpcion == 10919102) {   
    		           		 	Intent intent = new Intent(getApplicationContext(), ConsultaProyeccion.class);
    		           	 		intent.putExtra("Codigodepartamento", Codigodepartamento);
    		           	 		intent.putExtra("CodigoEmpleado", CodigoEmpleado );
    		           	 		intent.putExtra("CodigoModulo",  CodigoModulo);
    		           	 		intent.putExtra("CodigoOpcion", Integer.parseInt(dato.getCampo1().trim()));
    		           	 		intent.putExtra("NombreOpcion", dato.getCampo2()); 
    		           	 startActivity(intent);
    		       	    
            			}else if(CodigoOpcion == 10919103) {   
		           		 	Intent intent = new Intent(getApplicationContext(), consulta_genesis.class);
		           	 		intent.putExtra("Codigodepartamento", Codigodepartamento);
		           	 		intent.putExtra("CodigoEmpleado", CodigoEmpleado );
		           	 		intent.putExtra("CodigoModulo",  CodigoModulo);
		           	 		intent.putExtra("CodigoOpcion", Integer.parseInt(dato.getCampo1().trim()));
		           	 		intent.putExtra("NombreOpcion", dato.getCampo2()); 
		           	 	startActivity(intent);
		           	 
            			}else if(CodigoModulo == 110 || CodigoModulo == 123) {
            				Intent intent = new Intent(getApplicationContext(), CapturaDisponVeg.class);
    		           	 	intent.putExtra("Codigodepartamento", Codigodepartamento);
    		           	    intent.putExtra("CodigoEmpleado", CodigoEmpleado );
    		           	    intent.putExtra("CodigoModulo",  CodigoModulo);
    		           	    intent.putExtra("CodigoOpcion", Integer.parseInt(dato.getCampo1().trim()));
    		           	    intent.putExtra("NombreOpcion", dato.getCampo2());
    		           	    intent.putExtra("EsCorte", 0);
    		           	    startActivity(intent);	
            				
            			}else if(CodigoOpcion == 19991101) {
            				Intent intent = new Intent(getApplicationContext(), PesoBolsasVeg.class);
    		           	 	intent.putExtra("Codigodepartamento", Codigodepartamento);
    		           	    intent.putExtra("CodigoEmpleado", CodigoEmpleado );
    		           	    intent.putExtra("CodigoModulo",  CodigoModulo);
    		           	    intent.putExtra("CodigoOpcion", Integer.parseInt(dato.getCampo1().trim()));
    		           	    intent.putExtra("NombreOpcion", dato.getCampo2());
    		           	    startActivity(intent);
    		           	    
            			 }else if(dato.getCampo2().contains("FINALIZA ACTIVIDAD")){	
                 			Intent intent = new Intent(getApplicationContext(), FinalizarActividad.class);
     		           	 	intent.putExtra("Codigodepartamento", Codigodepartamento);
     		           	    intent.putExtra("CodigoEmpleado", CodigoEmpleado );
     		           	    intent.putExtra("CodigoModulo",  CodigoModulo);
     		           	    intent.putExtra("CodigoOpcion", Integer.parseInt(dato.getCampo1().trim()));
     		           	    intent.putExtra("NombreOpcion", dato.getCampo2());
     		           	    startActivity(intent);
                 		
            			}else{
            				try{
    	           	    	Intent intent = new Intent(getApplicationContext(), CapturaVegetativo.class);
    		           	 	intent.putExtra("Codigodepartamento", Codigodepartamento);
    		           	    intent.putExtra("CodigoEmpleado", CodigoEmpleado );
    		           	    intent.putExtra("CodigoModulo",  CodigoModulo);
    		           	    intent.putExtra("CodigoOpcion", Integer.parseInt(dato.getCampo1().trim()));
    		           	    intent.putExtra("NombreOpcion", dato.getCampo2());
            				
    			           	 if(dato.getCampo2().contains("ASIGNACION_ETIQUETAS")){
    			           		intent.putExtra("EsCorte", 1); 
    			           	 }else{
    			           		intent.putExtra("EsCorte", 0); 
    			           	 }
            				
    		           	    startActivity(intent);
            				}catch (Exception e) {
                    			e.printStackTrace();
                    		}            	    
            			}
            			
            			
	           	    	
	           	    }else if(Codigodepartamento == 2){
	           	    	
	           	     // para Consulta de Disponibilidad		
		           	  if(CodigoOpcion == 20727199) {   
		           		 	Intent intent = new Intent(getApplicationContext(), ConsultaDisponibilidad.class);
		           	 		intent.putExtra("Codigodepartamento", Codigodepartamento);
		           	 		intent.putExtra("CodigoEmpleado", CodigoEmpleado );
		           	 		intent.putExtra("CodigoModulo",  CodigoModulo);
		           	 		intent.putExtra("CodigoOpcion", Integer.parseInt(dato.getCampo1().trim()));
		           	 		intent.putExtra("NombreOpcion", dato.getCampo2()); 
		           	 		startActivity(intent);
		           	 		
		           	    //Para Disponiblidades
		           	} else if(CodigoModulo == 207 || CodigoModulo == 209 && CodigoOpcion != 20727199){
		           		Intent intent = new Intent(getApplicationContext(), CapturaDisponibilidad.class);
		           	 	intent.putExtra("Codigodepartamento", Codigodepartamento);
		           	    intent.putExtra("CodigoEmpleado", CodigoEmpleado );
		           	    intent.putExtra("CodigoModulo",  CodigoModulo);
		           	    intent.putExtra("CodigoOpcion", Integer.parseInt(dato.getCampo1().trim()));
		           	    intent.putExtra("NombreOpcion", dato.getCampo2()); 
		           	    startActivity(intent);
		           	    
		        	    //Para Propagador
		           	 }else if(CodigoModulo == 206){
			           		Intent intent = new Intent(getApplicationContext(), Propagador.class);
			           	 	intent.putExtra("Codigodepartamento", Codigodepartamento);
			           	    intent.putExtra("CodigoEmpleado", CodigoEmpleado );
			           	    intent.putExtra("CodigoModulo",  CodigoModulo);
			           	    intent.putExtra("CodigoOpcion", Integer.parseInt(dato.getCampo1().trim()));
			           	    intent.putExtra("NombreOpcion", dato.getCampo2()); 
			           	    startActivity(intent);
			           	    
		           	}else if(CodigoOpcion == 20222101 || CodigoOpcion == 21020003){
		           		Intent intent = new Intent(getApplicationContext(), CapturaSuccion.class);
		           	 	intent.putExtra("Codigodepartamento", Codigodepartamento);
		           	    intent.putExtra("CodigoEmpleado", CodigoEmpleado );
		           	    intent.putExtra("CodigoModulo",  CodigoModulo);
		           	    intent.putExtra("CodigoOpcion", Integer.parseInt(dato.getCampo1().trim()));
		           	    intent.putExtra("NombreOpcion", dato.getCampo2()); 
		           	    startActivity(intent);  
			           	    
		           	    // Para pesado Polen
		           	 }else if(dato.getCampo2().contains("PESO POLEN")){
			           		Intent intent = new Intent(getApplicationContext(), CapturaPesoPolen.class);
			           	 	intent.putExtra("Codigodepartamento", Codigodepartamento);
			           	    intent.putExtra("CodigoEmpleado", CodigoEmpleado );
			           	    intent.putExtra("CodigoModulo",  CodigoModulo);
			           	    intent.putExtra("CodigoOpcion", Integer.parseInt(dato.getCampo1().trim()));
			           	    intent.putExtra("NombreOpcion", dato.getCampo2()); 
			           	    startActivity(intent);
			           	    
		           	 } else  if(CodigoOpcion == 20323108) {    
		           		 Intent intent = new Intent(getApplicationContext(), FinalizaProceso.class);
		           	 		intent.putExtra("Codigodepartamento", Codigodepartamento);
		           	 		intent.putExtra("CodigoEmpleado", CodigoEmpleado );
		           	 		intent.putExtra("CodigoModulo",  CodigoModulo);
		           	 		intent.putExtra("CodigoOpcion", Integer.parseInt(dato.getCampo1().trim()));
		           	 		intent.putExtra("NombreOpcion", dato.getCampo2()); 
		           	 		startActivity(intent);
		           	 		
		           	 } else  if(CodigoOpcion == 20323109) {    
		           		 Intent intent = new Intent(getApplicationContext(), FinalizaLimpieza.class);
		           	 		intent.putExtra("Codigodepartamento", Codigodepartamento);
		           	 		intent.putExtra("CodigoEmpleado", CodigoEmpleado );
		           	 		intent.putExtra("CodigoModulo",  CodigoModulo);
		           	 		intent.putExtra("CodigoOpcion", Integer.parseInt(dato.getCampo1().trim()));
		           	 		intent.putExtra("NombreOpcion", dato.getCampo2()); 
		           	 		startActivity(intent);
		           	 
		           	 } else  if(CodigoOpcion == 21272998) {    
		           		 Intent intent = new Intent(getApplicationContext(), FinalizaDehisencia.class);
		           	 		intent.putExtra("Codigodepartamento", Codigodepartamento);
		           	 		intent.putExtra("CodigoEmpleado", CodigoEmpleado );
		           	 		intent.putExtra("CodigoModulo",  CodigoModulo);
		           	 		intent.putExtra("CodigoOpcion", Integer.parseInt(dato.getCampo1().trim()));
		           	 		intent.putExtra("NombreOpcion", dato.getCampo2()); 
		           	 		startActivity(intent);
		           	 		
		           	 } else  if(CodigoModulo == 299 && CodigoOpcion == 29992101) {   
		           		 Intent intent = new Intent(getApplicationContext(), ConsultaPlantas.class);
		           	 		intent.putExtra("Codigodepartamento", Codigodepartamento);
		           	 		intent.putExtra("CodigoEmpleado", CodigoEmpleado );
		           	 		intent.putExtra("CodigoModulo",  CodigoModulo);
		           	 		intent.putExtra("CodigoOpcion", Integer.parseInt(dato.getCampo1().trim()));
		           	 		intent.putExtra("NombreOpcion", dato.getCampo2()); 
		           	 		startActivity(intent);
		         
			           	    
		           	 }else if(CodigoOpcion == 20323107){
			           		Intent intent = new Intent(getApplicationContext(), LimpiezaManual.class);
			           	 	intent.putExtra("Codigodepartamento", Codigodepartamento);
			           	    intent.putExtra("CodigoEmpleado", CodigoEmpleado );
			           	    intent.putExtra("CodigoModulo",  CodigoModulo);
			           	    intent.putExtra("CodigoOpcion", Integer.parseInt(dato.getCampo1().trim()));
			           	    intent.putExtra("NombreOpcion", dato.getCampo2()); 
			           	    startActivity(intent);
			           	    
			         // Actividad para Consulta de Plantas  	    
		           	 }else if(CodigoOpcion == 29829801){
			           		Intent intent = new Intent(getApplicationContext(), ConsultaPlantas.class);
			           	 	intent.putExtra("Codigodepartamento", Codigodepartamento);
			           	    intent.putExtra("CodigoEmpleado", CodigoEmpleado );
			           	    intent.putExtra("CodigoModulo",  CodigoModulo);
			           	    intent.putExtra("CodigoOpcion", Integer.parseInt(dato.getCampo1().trim()));
			           	    intent.putExtra("NombreOpcion", dato.getCampo2()); 
			           	    startActivity(intent);
			       	 
		           	}else if(CodigoOpcion == 29829802){
		           		Intent intent = new Intent(getApplicationContext(), ConsultaExistenciaPolen.class);
		           	 	intent.putExtra("Codigodepartamento", Codigodepartamento);
		           	    intent.putExtra("CodigoEmpleado", CodigoEmpleado );
		           	    intent.putExtra("CodigoModulo",  CodigoModulo);
		           	    intent.putExtra("CodigoOpcion", Integer.parseInt(dato.getCampo1().trim()));
		           	    intent.putExtra("NombreOpcion", dato.getCampo2()); 
		           	    startActivity(intent);
		           	    
			         // Actividad para SOLICITUDES de SEMILLAS  	    
		           	 }else if(CodigoModulo == 297){
			           		Intent intent = new Intent(getApplicationContext(), SolicitudSemillas.class);
			           	 	intent.putExtra("Codigodepartamento", Codigodepartamento);
			           	    intent.putExtra("CodigoEmpleado", CodigoEmpleado );
			           	    intent.putExtra("CodigoModulo",  CodigoModulo);
			           	    intent.putExtra("CodigoOpcion", Integer.parseInt(dato.getCampo1().trim()));
			           	    intent.putExtra("NombreOpcion", dato.getCampo2()); 
			           	    startActivity(intent);
			          
			        //Finalización DIARIA de las actividades de RIEGO SEMILLAS
		           	}else if(CodigoOpcion == 21373998 || CodigoOpcion == 21474998 || CodigoOpcion == 21575998){
		           		Intent intent = new Intent(getApplicationContext(), FinDiaRiegoSem.class);
		           	 	intent.putExtra("Codigodepartamento", Codigodepartamento);
		           	    intent.putExtra("CodigoEmpleado", CodigoEmpleado );
		           	    intent.putExtra("CodigoModulo",  CodigoModulo);
		           	    intent.putExtra("CodigoOpcion", Integer.parseInt(dato.getCampo1().trim()));
		           	    intent.putExtra("NombreOpcion", dato.getCampo2()); 
		           	    startActivity(intent);
		           	
		           	  //Finalización PARCIAL de las actividades de RIEGO SEMILLAS
		           	}else if(CodigoOpcion == 21373997 || CodigoOpcion == 21474997 || CodigoOpcion == 21575997){
		           		Intent intent = new Intent(getApplicationContext(), FinParciaRiegoSem.class);
		           	 	intent.putExtra("Codigodepartamento", Codigodepartamento);
		           	    intent.putExtra("CodigoEmpleado", CodigoEmpleado );
		           	    intent.putExtra("CodigoModulo",  CodigoModulo);
		           	    intent.putExtra("CodigoOpcion", Integer.parseInt(dato.getCampo1().trim()));
		           	    intent.putExtra("NombreOpcion", dato.getCampo2()); 
		           	    startActivity(intent);	    
		           	    
		           	// Asignacion en modulos de RIEGO FUMIGACION Y LABORES DE SOPORTE SEMILLAS
		           	}else if(CodigoModulo == 213 || CodigoModulo == 214 || CodigoModulo == 215){
		       			Intent intent = new Intent(getApplicationContext(), RiegoSemillas.class);
		           	 	intent.putExtra("Codigodepartamento", Codigodepartamento);
		           	    intent.putExtra("CodigoEmpleado", CodigoEmpleado );
		           	    intent.putExtra("CodigoModulo",  CodigoModulo);
		           	    intent.putExtra("CodigoOpcion", Integer.parseInt(dato.getCampo1().trim()));
		           	    intent.putExtra("NombreOpcion", dato.getCampo2()); 
		           	    startActivity(intent);
		           	   
		           	}else if(CodigoOpcion == 20222117){
			           		Intent intent = new Intent(getApplicationContext(), CosechaPolenDehisencia.class);
			           	 	intent.putExtra("Codigodepartamento", Codigodepartamento);
			           	    intent.putExtra("CodigoEmpleado", CodigoEmpleado );
			           	    intent.putExtra("CodigoModulo",  CodigoModulo);
			           	    intent.putExtra("CodigoOpcion", Integer.parseInt(dato.getCampo1().trim()));
			           	    intent.putExtra("NombreOpcion", dato.getCampo2()); 
			           	    startActivity(intent);	       	 
			          
		          	 }else if(CodigoOpcion == 21272005){
			           		Intent intent = new Intent(getApplicationContext(), LimpiezaDehisencia.class);
			           	 	intent.putExtra("Codigodepartamento", Codigodepartamento);
			           	    intent.putExtra("CodigoEmpleado", CodigoEmpleado );
			           	    intent.putExtra("CodigoModulo",  CodigoModulo);
			           	    intent.putExtra("CodigoOpcion", Integer.parseInt(dato.getCampo1().trim()));
			           	    intent.putExtra("NombreOpcion", dato.getCampo2()); 
			           	    startActivity(intent);
		          	 
		          	 }else if(dato.getCampo2().contains("FINALIZA ACTIVIDAD")){	
            			Intent intent = new Intent(getApplicationContext(), FinalizarActividad.class);
		           	 	intent.putExtra("Codigodepartamento", Codigodepartamento);
		           	    intent.putExtra("CodigoEmpleado", CodigoEmpleado );
		           	    intent.putExtra("CodigoModulo",  CodigoModulo);
		           	    intent.putExtra("CodigoOpcion", Integer.parseInt(dato.getCampo1().trim()));
		           	    intent.putExtra("NombreOpcion", dato.getCampo2());
		           	    startActivity(intent);
            	
		           	 }else{
		           		 
		           		//Para pantalla General 
		           		 try {
		           		Intent intent = new Intent(getApplicationContext(), CapturaGeneral.class);
		           	 	intent.putExtra("Codigodepartamento", Codigodepartamento);
		           	    intent.putExtra("CodigoEmpleado", CodigoEmpleado );
		           	    intent.putExtra("CodigoModulo",  CodigoModulo);
		           	    intent.putExtra("CodigoOpcion", Integer.parseInt(dato.getCampo1().trim()));
		           	    intent.putExtra("NombreOpcion", dato.getCampo2());
		           		 
		           	    if(dato.getCampo2().contains("POLINIZACIONES")){
			           		intent.putExtra("OpcionPolinizacion", 1); 
			           	 }else{
			           		intent.putExtra("OpcionPolinizacion", 0); 
			           	 }
		           	    //CODIGO AGREGADO
		           	    if(dato.getCampo2().contains("SUCCION")){
		           	    	intent.putExtra("OpcionSuccion", 1);
		           	    }else{
		           	    	intent.putExtra("OpcionSuccion", 0);
		           	    }
		           	    //FIN CODIGO AGREGADO
		           	    		           	    
		           	    if(dato.getCampo2().contains("COSECHAS")){
			           		intent.putExtra("OpcionCosecha", 1); 
			           	 }else{
			           		intent.putExtra("OpcionCosecha", 0); 
			           	 }
		           	 		           	  	           	 
		           	    startActivity(intent);
		           		}catch (Exception e) {
                			e.printStackTrace();
                		}            	    
		           	 }
		           	    	    	
	           	    } else {
	           	    	if(Codigodepartamento == 3) {
	           	    		Intent intent = new Intent(getApplicationContext(), CapturaMantenimiento.class);
		           	 		intent.putExtra("Codigodepartamento", Codigodepartamento);
		           	 		intent.putExtra("CodigoEmpleado", CodigoEmpleado );
		           	 		intent.putExtra("CodigoModulo",  CodigoModulo);
		           	 		intent.putExtra("CodigoOpcion", Integer.parseInt(dato.getCampo1().trim()));
		           	 		intent.putExtra("NombreOpcion", dato.getCampo2()); 
		           	 		startActivity(intent);			        	
	           	    	}
	           	    	
	           	    	if(CodigoOpcion == 80181101) {   
		           		 	Intent intent = new Intent(getApplicationContext(), CapturaEvaluacion.class);
		           	 		intent.putExtra("Codigodepartamento", Codigodepartamento);
		           	 		intent.putExtra("CodigoEmpleado", CodigoEmpleado );
		           	 		intent.putExtra("CodigoModulo",  CodigoModulo);
		           	 		intent.putExtra("CodigoOpcion", Integer.parseInt(dato.getCampo1().trim()));
		           	 		intent.putExtra("NombreOpcion", dato.getCampo2()); 
		           	 		startActivity(intent);		           	 				           	  
		           	}
	           	    	 
	           }           	
 */
