package com.example.sim;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.Vibrator;
import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.floricultura.R;

public class DetalleProyeccion extends ActionBarActivity {
	
	//Variables globales
	private MyApp appState;
	int valor;
	String NombreOpcion;
	//GridView gridet;
	//GridView gridEnc;
	
	Vibrator mVibrator;
	Ringtone ringtone;
	String Inv;
	String Act;
	String Fecha1;
	String Fecha2;
	String Per;
	
	@SuppressLint("NewApi") @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.detalle_proyeccion);
		
		// para manejar la excepcion de permisos en el API de Android
    	StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

		//se obtienen lo datos de la actividad antetior por medio del intent
		Bundle extras = getIntent().getExtras();
	    NombreOpcion = "Detalle Proyeccion";
	    Act = extras.getString("Act");
	    Inv = extras.getString("Inv");
	    Fecha1 = extras.getString("Fecha1");
	    Fecha2 = extras.getString("Fecha2");
	    Per = extras.getString("Per");
	    
	    //hacemos referencia a todos los objetos de nuestra vista
	    TextView TNombreCaptura = (TextView)findViewById(R.id.OpcionCaptura);
	    TextView Actividad = (TextView) findViewById(R.id.textActividad);
	    TextView Invernadero = (TextView) findViewById(R.id.textInv);
	    TextView Personas = (TextView) findViewById(R.id.textPersonas);
        Button btnBack = (Button) findViewById(R.id.btnBack);
        final TableLayout TablaDetalle = (TableLayout) findViewById(R.id.TablaDetalleProyeccion);
        
 
	    //hacemos las inicializaciones necesarias a nuestra vista
	    TNombreCaptura.setText(NombreOpcion+ ""); 
	    Actividad.setText(Act);
	    Invernadero.setText(Inv);
	    Personas.setText(Per);
	   
        	
        	String es = " | ";
        	TablaDetalle.removeAllViews();
        	DecimalFormat formatea = new DecimalFormat("###,###.##");
        	
    	try {
    		String user = "sim";
    		String pasword = "sim";
    		 Connection connection = null;
    		 ResultSet rs = null;
    		 Statement statement = null;
    		 
    			Class.forName("com.ibm.as400.access.AS400JDBCDriver");
    			connection = DriverManager.getConnection("jdbc:as400://190.190.190.10", user, pasword);	
    			statement = connection.createStatement();			     
    			rs = statement.executeQuery(" Select d.fecha,s.definv,so.descripcion Actividad,s.defban BANC,s.defvar VAR,d.siembra,s.defmat  PLANT,decimal((sum(decimal(d.conteo,10,2)) / count(decimal(d.conteo,10,2))),10,2) PROM, " +
    					"decimal((decimal((sum(decimal(d.conteo,10,2)) / count(decimal(d.conteo,10,2))),10,2) * decimal(s.defmat,10,2)),10,2) DISP, SO.RENDIMIENTOGENERAL REND, " +
    					"decimal(((decimal((sum(decimal(d.conteo,10,2)) / count(decimal(d.conteo,10,2))),10,2) * decimal(s.defmat,10,2)) / decimal(SO.RENDIMIENTOGENERAL,10,2)),10,2) MIN, " +
    					"decimal(decimal(((decimal((sum(decimal(d.conteo,10,2)) / count(decimal(d.conteo,10,2))),10,2) * decimal(s.defmat,10,2)) / decimal(SO.RENDIMIENTOGENERAL,10,2)),10,2) / 60.00,10,2) Horas, " +
    					"decimal(decimal(decimal(((decimal((sum(decimal(d.conteo,10,2)) / count(decimal(d.conteo,10,2))),10,2) * decimal(s.defmat,10,2)) / decimal(SO.RENDIMIENTOGENERAL,10,2)),10,2) / 60.00,10,2) / 8.00,10,2) Personas, " +
    					"decimal(((decimal(SO.RENDIMIENTOGENERAL,10,2) * 60.00) * 8.00),10,2) TAREA,  " +
    					"'AC' Estado from sisappf.app_disponibilidadveg d left join sisvegf.mov_defsiembraveg s on d.siembra = left(s.defsie,4)||s.correlativo left join siscfgf.seguridad_opcion_moduloasp so on d.actividad = so.opcion " +
    					"where s.definv = '"+Inv+"' and d.fecha between '"+Fecha1+"' and '"+Fecha2+"' and so.descripcion = '"+Act+"' " +
    					"group by SO.RENDIMIENTOGENERAL,so.descripcion,d.siembra,d.fecha,d.actividad,s.defvar,s.defban,s.definv,s.defmat order by d.fecha,s.definv,s.defban");
    			//Agregar encabezados de columnas	
    			 TableRow Erow1 = new TableRow(DetalleProyeccion.this);
    			 TextView E1 = new TextView(DetalleProyeccion.this);
    			 TextView E2 = new TextView(DetalleProyeccion.this);
    			 TextView E3 = new TextView(DetalleProyeccion.this);
    			 TextView E4 = new TextView(DetalleProyeccion.this);
    			 TextView E5 = new TextView(DetalleProyeccion.this);
    			 TextView E6 = new TextView(DetalleProyeccion.this);
    			 TextView E7 = new TextView(DetalleProyeccion.this);
    			 TextView E8 = new TextView(DetalleProyeccion.this); 
    			 TextView E9 = new TextView(DetalleProyeccion.this); 
    			 TextView E10 = new TextView(DetalleProyeccion.this); 
    			 TextView E11 = new TextView(DetalleProyeccion.this); 
    			 TextView E12 = new TextView(DetalleProyeccion.this);
    			 TextView E13 = new TextView(DetalleProyeccion.this);
    			 E1.setTextSize(20);
    			 E1.setTextColor(Color.WHITE);
    			 E1.setTypeface(null, Typeface.BOLD);  
    			 E2.setTextSize(20);
    			 E2.setTextColor(Color.WHITE);
    			 E2.setTypeface(null, Typeface.BOLD); 
    			 E3.setTextSize(20);
    			 E3.setTextColor(Color.WHITE);
    			 E3.setTypeface(null, Typeface.BOLD); 
    			 E4.setTextSize(20);
    			 E4.setTextColor(Color.WHITE);
    			 E4.setTypeface(null, Typeface.BOLD); 
    			 E5.setTextSize(20);
    			 E5.setTextColor(Color.WHITE);
    			 E5.setTypeface(null, Typeface.BOLD); 
    			 E6.setTextSize(20);
    			 E6.setTextColor(Color.WHITE);
    			 E6.setTypeface(null, Typeface.BOLD); 
    			 E7.setTextSize(20);
    			 E7.setTextColor(Color.WHITE);
    			 E7.setTypeface(null, Typeface.BOLD); 
    			 E8.setTextSize(20);
    			 E8.setTextColor(Color.BLACK);
    			 E9.setTextSize(20);
    			 E9.setTextColor(Color.BLACK);
    			 E10.setTextSize(20);
    			 E10.setTextColor(Color.BLACK);
    			 E11.setTextSize(20);
    			 E11.setTextColor(Color.BLACK);
    			 E12.setTextSize(20);
    			 E12.setTextColor(Color.BLACK);
    			 E13.setTextSize(20);
    			 E13.setTextColor(Color.BLACK);
    			 String Cn1 = rs.getMetaData().getColumnName(5);
    			 String Cn2 = rs.getMetaData().getColumnName(7);
    			 String Cn3 = rs.getMetaData().getColumnName(8);
    			 String Cn4 = rs.getMetaData().getColumnName(9);
    			 String Cn5 = rs.getMetaData().getColumnName(10);
    			 String Cn6 = rs.getMetaData().getColumnName(13);
    			 String Cn7 = rs.getMetaData().getColumnName(14);
    			 E1.setText(Cn1);
    			 E2.setText(Cn2);
    			 E3.setText(Cn3);
    			 E4.setText(Cn4);
    			 E5.setText(Cn5);
    			 E6.setText(Cn6);
    			 E7.setText(Cn7);
    			 E8.setText(es);
    			 E9.setText(es);
    			 E10.setText(es);
    			 E11.setText(es);
    			 E12.setText(es);
    			 E13.setText(es);
    			 Erow1.addView(E1);
    			 Erow1.addView(E8);
    			 Erow1.addView(E2);
    			 Erow1.addView(E9);
    			 Erow1.addView(E3);
    			 Erow1.addView(E10);
    			 Erow1.addView(E4);
    			 Erow1.addView(E11);
    			 Erow1.addView(E5);
    			 Erow1.addView(E12);
    			 Erow1.addView(E6);
    			 Erow1.addView(E13);
    			 Erow1.addView(E7);
    			 E1.setGravity(Gravity.CENTER_HORIZONTAL);
    			 E2.setGravity(Gravity.CENTER_HORIZONTAL);
    			 E3.setGravity(Gravity.CENTER_HORIZONTAL);
    			 E4.setGravity(Gravity.CENTER_HORIZONTAL);
    			 E5.setGravity(Gravity.CENTER_HORIZONTAL);
    			 E6.setGravity(Gravity.CENTER_HORIZONTAL);
    			 E8.setGravity(Gravity.CENTER_HORIZONTAL);
    			 E9.setGravity(Gravity.CENTER_HORIZONTAL);
    			 E10.setGravity(Gravity.CENTER_HORIZONTAL);
    			 E11.setGravity(Gravity.CENTER_HORIZONTAL);
    			 E12.setGravity(Gravity.CENTER_HORIZONTAL);
    			 Erow1.setGravity(Gravity.CENTER);
    			 Erow1.setBackgroundColor(Color.GREEN);
    			 TablaDetalle.addView(Erow1);
    			// finaliza agregar encabezados de columnas 
    			int sz = rs.getFetchSize();
    			int cont = 0;
    			while (rs.next()) {
    				 String d1 = rs.getString(5).trim();
    				 String d2 = rs.getString(7).trim();
    				 String d3 = rs.getString(8).trim();
    				 String d4 = rs.getString(9).trim();
    			     String d5 = rs.getString(10).trim();
    				 String d6 = rs.getString(13).trim();
    				 String d7 = rs.getString(14).trim();
    				 String c1 = comprobar(d1);
    				 String r2 = comprobar(d2);
    				 String c3 = comprobar(d3);
    				 String r4 = comprobar(d4);
    				 String c5 = comprobar(d5);
    				 String c6 = comprobar(d6);
    				 String r7 = comprobar(d7);
    				 String c2 = formatea.format(Double.parseDouble(r2));
    				 String c4 = formatea.format(Double.parseDouble(r4));
    				 String c7 = formatea.format(Double.parseDouble(r7));
    				 for (int i = 0; i == sz; i++)
    				 {	
    					 cont = cont +1;
    					 TableRow row1 = new TableRow(DetalleProyeccion.this);
    					 TextView t1 = new TextView(DetalleProyeccion.this);
    					 TextView t2 = new TextView(DetalleProyeccion.this);
    					 TextView t3 = new TextView(DetalleProyeccion.this);
    					 TextView t4 = new TextView(DetalleProyeccion.this);
    					 TextView t5 = new TextView(DetalleProyeccion.this);
    					 TextView t6 = new TextView(DetalleProyeccion.this);
    					 TextView t7 = new TextView(DetalleProyeccion.this);
    					 TextView t8 = new TextView(DetalleProyeccion.this); 
    					 TextView t9 = new TextView(DetalleProyeccion.this); 
    					 TextView t10 = new TextView(DetalleProyeccion.this); 
    					 TextView t11 = new TextView(DetalleProyeccion.this); 
    					 TextView t12 = new TextView(DetalleProyeccion.this);
    					 TextView t13 = new TextView(DetalleProyeccion.this);
    					 t1.setTextSize(15);
    					 t1.setTextColor(Color.BLACK);
    					 t2.setTextSize(15);
    					 t2.setTextColor(Color.BLACK);
    					 t3.setTextSize(15);
    					 t3.setTextColor(Color.BLACK);
    					 t4.setTextSize(15);
    					 t4.setTextColor(Color.BLACK);
    					 t5.setTextSize(15);
    					 t5.setTextColor(Color.BLACK);
    					 t6.setTextSize(15);
    					 t6.setTextColor(Color.BLACK);
    					 t7.setTextSize(15);
    					 t7.setTextColor(Color.BLACK);
    					 t8.setTextSize(20);
    					 t8.setTextColor(Color.BLACK);
    					 t9.setTextSize(20);
    					 t9.setTextColor(Color.BLACK);
    					 t10.setTextSize(20);
    					 t10.setTextColor(Color.BLACK);
    					 t11.setTextSize(20);
    					 t11.setTextColor(Color.BLACK);
    					 t12.setTextSize(20);
    					 t12.setTextColor(Color.BLACK);
    					 t13.setTextSize(20);
    					 t13.setTextColor(Color.BLACK);
    					 t1.setGravity(Gravity.CENTER_HORIZONTAL);
    					 t2.setGravity(Gravity.CENTER_HORIZONTAL);
    					 t3.setGravity(Gravity.CENTER_HORIZONTAL);
    					 t4.setGravity(Gravity.CENTER_HORIZONTAL);
    					 t5.setGravity(Gravity.CENTER_HORIZONTAL);
    					 t6.setGravity(Gravity.CENTER_HORIZONTAL);
    					 t7.setGravity(Gravity.CENTER_HORIZONTAL);
    					 t8.setGravity(Gravity.CENTER_HORIZONTAL);
    					 t9.setGravity(Gravity.CENTER_HORIZONTAL);
    					 t10.setGravity(Gravity.CENTER_HORIZONTAL);
    					 t11.setGravity(Gravity.CENTER_HORIZONTAL);
    					 t12.setGravity(Gravity.CENTER_HORIZONTAL);
    					 t13.setGravity(Gravity.CENTER_HORIZONTAL);
    					 row1.setGravity(Gravity.CENTER);
    					 row1.setClickable(true);
    					 if(cont%2 == 0){
    					 row1.setBackgroundColor(Color.LTGRAY);
    					 }else {
    					 row1.setBackgroundColor(Color.WHITE);	 
    					 }
    				 t1.setText(c1);
    				 t2.setText(c2);
    				 t3.setText(c3);
    				 t4.setText(c4);
    				 t5.setText(c5);
    				 t6.setText(c6);
    				 t7.setText(c7);
    				 t8.setText(es);
    				 t9.setText(es);
    				 t10.setText(es);
    				 t11.setText(es);
    				 t12.setText(es);
    				 t13.setText(es);
    				 row1.addView(t1);
    				 row1.addView(t8);
    				 row1.addView(t2);
    				 row1.addView(t9);
    				 row1.addView(t3);
    				 row1.addView(t10);
    				 row1.addView(t4);
    				 row1.addView(t11);
    				 row1.addView(t5);
    				 row1.addView(t12);
    				 row1.addView(t6);
    				 row1.addView(t13);
    				 row1.addView(t7);
    				 TablaDetalle.addView(row1);
    				 TablaDetalle.setClickable(true);
    				 }
    				 
    			}
    			connection.close();	
    		}
    		catch (Exception e) {
    			Toast toast = Toast.makeText(getApplicationContext(), "Verificar Conexi�n.....", Toast.LENGTH_SHORT);
    			toast.show();
    			e.printStackTrace();
    		} 
    		    
	    btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	onBackPressed();
            }
        });   
	}
	    
	public void alertDialogMensaje(String message1, String mesage2){
		
		//mVibrator.vibrate(300);
		
		try {
		    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
		    ringtone.play();
		    
		    
		} catch (Exception e) {
		    e.printStackTrace();
		}
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(message1);
		alertDialog.setMessage(mesage2);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int which) {
		// here you can add functions
			ringtone.stop();
		}
		});
		alertDialog.show();
	}

	public String comprobar(String cad){
		String Pb = "";
		if(cad.equals(Pb)){
			cad = "---";
		}
		return cad;	
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.soporteit, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}  


/*
try {
String user = "sim";
String pasword = "sim";
 Connection connection = null;
 ResultSet rs = null;
 Statement statement = null;
 
	Class.forName("com.ibm.as400.access.AS400JDBCDriver");
	connection = DriverManager.getConnection("jdbc:as400://190.190.190.10", user, pasword);	
	statement = connection.createStatement();
	rs = statement.executeQuery("Select v43.bardoc ORDEN,ifnull(ifnull(l1.invernadero,asig.invernadero),replace(replace(v43.barinv,'-',''),' ','')) Inv," +
			"v43.barvar VARIEDAD,v43.barcns Etiquetas,ifnull((rtrim(asig.codigo_empleado)||'-'||rtrim(e.nombre)||'-'||rtrim(e.apellido)),'---') Empleado " +
			"from sigvegfgu.vege43 v43 left join (Select codigo_empleado,ordencompra,invernadero,cast(etiqueta as integer) Etiqueta " +
			"from sisappf.app_tmp_etiquetas where fecha between '"+Fechade+"' and '"+Fechaal+"' and estado = 'A' and invernadero <> 'EMPAQU' " +
			"order by etiqueta) asig on v43.barcns = asig.Etiqueta left join (Select ordencompra,invernadero,cast(etiqueta as integer) Etiqueta " +
			"from sisappf.app_tmp_etiquetasL1 where fecha  between '"+Fechade+"' and '"+Fechaal+"' and estado = 'A' and invernadero <> 'EMPAQU' " +
			"order by etiqueta) l1 on v43.barcns = l1.Etiqueta left join (Select i.temd01 from sigipvfgu.incv611 i left join " +
			"(Select ordencompra,invernadero,cast(etiqueta as integer) Etiqueta from sisappf.app_tmp_etiquetasL1 where " +
			"fecha between '"+Fechade+"' and '"+Fechaal+"' and estado = 'A' and invernadero <> 'EMPAQU' order by etiqueta) a on i.temd01 = a.etiqueta " +
			"where temd08 between '"+Fechade1+"' and '"+Fechaal1+"' and a.etiqueta is null order by temd01) emp on v43.barcns = emp.temd01 " +
			"left join sisrrhhf.mempleado e on '0'||asig.codigo_empleado = e.empresa||e.codigoempleado left join sisvegf.membarques emb " +
			"on right(v43.bardoc,1) = emb.letraorden where barano = '"+a�o1+"' and barsem = '"+Wk+"' and barot3 <> '2.00' and l1.etiqueta is null " +
			"and emp.temd01 is null and ifnull(ifnull(l1.invernadero,asig.invernadero),replace(replace(v43.barinv,'-',''),' ','')) = '"+Inv+"' AND LEFT(EMB.DIA,3) = '"+DiaCorte+"'");
	
    //Agregar encabezados de columnas	
	String Cn1 = rs.getMetaData().getColumnName(1);
	encabezado.add(Cn1);
	gridEnc.setAdapter(adapEnc);
	String Cn2 = rs.getMetaData().getColumnName(2);
	encabezado.add(Cn2);
	gridEnc.setAdapter(adapEnc);
	String Cn3 = rs.getMetaData().getColumnName(3);
	encabezado.add(Cn3);
	gridEnc.setAdapter(adapEnc);
	String Cn4 = rs.getMetaData().getColumnName(4);
	encabezado.add(Cn4);
	gridEnc.setAdapter(adapEnc);
	String Cn5 = rs.getMetaData().getColumnName(5);
	encabezado.add(Cn5);
	gridEnc.setAdapter(adapEnc);
				
	// finaliza agregar encabezados de columnas	
	while (rs.next()) {
	//agrega filas de columnas
		 String d1 = rs.getString(1).trim();
		 String d2 = rs.getString(2).trim();
		 String d3 = rs.getString(3).trim();
		 String d4 = rs.getString(4).trim();
	     String d5 = rs.getString(5).trim();
		 String c1 = comprobar(d1);
		 String c2 = comprobar(d2);
		 String c3 = comprobar(d3);
		 String c4 = comprobar(d4);
		 String c5 = comprobar(d5);
		 datos.add(c1);
		 gridet.setAdapter(adapter);
		 datos.add(c2);
		 gridet.setAdapter(adapter);
		 datos.add(c3);
		 gridet.setAdapter(adapter);
		 datos.add(c4);
		 gridet.setAdapter(adapter);
		 datos.add(c5);
		 gridet.setAdapter(adapter);
		 
	//finaliza agregar columnas	 
	}
	connection.close();	
}
catch (Exception e) {
	Toast toast = Toast.makeText(getApplicationContext(), "Verificar Conexi�n.....", Toast.LENGTH_SHORT);
	toast.show();
	//alertDialogMensaje("CONEXION","VERIFIQUE SU CONEXION");
	// TODO Auto-generated catch block
	e.printStackTrace();
}  */