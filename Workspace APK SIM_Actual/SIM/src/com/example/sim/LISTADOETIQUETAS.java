package com.example.sim;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.example.floricultura.R;
import com.example.sim.adapters.DatosLista;
import com.example.sim.adapters.ListAdapter;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.Dempleado;
import com.example.sim.data.Dguardados;
import com.example.sim.data.EtiquetasVegetativo;

public class LISTADOETIQUETAS extends ActionBarActivity {
	
	ArrayList<DatosLista> listado = new ArrayList<DatosLista>();
	int CodigoEmpleado;
	int Act;
	private MyApp appState;	
	private DBAdapter db;
	String Hoy;
	String[] data = null;
	String[] data2 = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_listado_actividad);
		
		Bundle extras = getIntent().getExtras();
	    CodigoEmpleado = extras.getInt("CodigoEmpleado");
	    Act = extras.getInt("CodigoOpcion");
	    
	    appState = ((MyApp)getApplicationContext());   
	    db = appState.getDb();
	    db.open();
	    Calendar c = Calendar.getInstance(); 
		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
		Hoy = fdate.format(c.getTime());
	    
		ListView listadoVista = (ListView)findViewById(R.id.ListadoAsignados);
		listado = getDatos();
		
		ListAdapter adaptador = new ListAdapter(this, R.layout.filas_lista, listado);
        listadoVista.setAdapter(adaptador);
        
        Button btnBack = (Button) findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	            
            	onBackPressed();
            	}
        });


	}
	
	
	public ArrayList<DatosLista> getDatos(){
		ArrayList<DatosLista> listaTemp = new ArrayList<DatosLista>();
		
		listaTemp.add(new DatosLista("      Codigo\nEmpleado", "      Etiquetas\nAsignadas", " Opcion"));

		/* Cursor CEtiquetas = this.db.getEtiquetas("Fecha = '"+ Hoy +"'");
		 
		 
		 if(CEtiquetas.getCount()>0){
			 for(int i=0 ; i<CEtiquetas.getCount();i++){	    
				 
				 EtiquetasVegetativo Etemp = this.db.getEtiquetasFromCursor(CEtiquetas, i);
				 
				 Cursor Codigoempleado = this.db.getDempledo("CodigoEmpleado=" + Etemp.getCodigoEmpleado().toString().trim());
				 Dempleado DatosEmpleado = this.db.getDempleadoFromCursor(Codigoempleado, 0);
				 Cursor EtiquetasAsignadas = this.db.getEtiquetas("CodigoEmpleado = '" + Etemp.getCodigoEmpleado().toString().trim() + "' AND Fecha = '"+ Hoy +"'");
				 			 
				 listaTemp.add(new DatosLista("    "+DatosEmpleado.getNombre().toString().trim(),"       " + EtiquetasAsignadas.getCount()," " + Etemp.getActividad()));
				
		     } 
		 }*/

		 	int sup = CodigoEmpleado;
			int Opc =  Act;
			 Cursor Casignados = this.db.getEtiquetas("Supervisor=" + sup + " AND Actividad=" + Opc +" AND Fecha = '"+ Hoy +"'");
			 String tmp = null;
			 int j = 0;
			 data2 = new String[Casignados.getCount()];
			 if(Casignados.getCount()>0){
				 for(int i=0 ; i<Casignados.getCount();i++){	 
					 EtiquetasVegetativo ETQ = db.getEtiquetasFromCursor(Casignados, i);
					 Cursor Codigoempleado = this.db.getDempledo("CodigoEmpleado = " + ETQ.getCodigoEmpleado().toString().trim());
					 Dempleado DE = this.db.getDempleadoFromCursor(Codigoempleado, 0);
					 Cursor EtiquetasAsignadas = this.db.getEtiquetas("CodigoEmpleado = '" + ETQ.getCodigoEmpleado().toString().trim() + "' AND Fecha = '"+ Hoy +"'");
					 if(DE != null){
						 tmp =  DE.getNombre().toString().trim() +"         "+ EtiquetasAsignadas.getCount();
					 }else{
						 tmp =  "NOMBRE NO ENCONTRADO  " +"         "+ EtiquetasAsignadas.getCount();
					 }
					 Codigoempleado.close();
					 EtiquetasAsignadas.close();
					 if(j != 0){
						 if(!tmp.equals(data2[j]) && !tmp.equals(data2[j-1]) ){
							 Log.i("Dialogos", "Opci�n  " + tmp +" - " + data2[j] + " - "+ j );
							 data2[j] = tmp;
							 j++;
					 	} 
					 }else{
						 data2[j] = tmp;
						 j++;
					 }
					 EtiquetasAsignadas.close();
					 Codigoempleado.close();
				 	} 
				 }
			 Casignados.close();
			 
			 int z = 0;
			 data = new String[j];
			 for(int i=0; i<j; i++){
				 data[z] = data2[z];
				 String TMP = data[z];
				 listaTemp.add(new DatosLista("    ", TMP , "    "));
				 z++;
			 }
				
		return listaTemp;
		

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.listado_actividad, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}



}
