package com.example.sim;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Vibrator;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.floricultura.R;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.Dbolsas;
import com.example.sim.data.Dempleado;
import com.example.sim.data.Dguardados;
import com.example.sim.data.Filtros;
import com.example.sim.data.JsonFiles;
import com.example.sim.data.Semillas;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CapturaSuccion extends ActionBarActivity {

	private MyApp appState;	
	private DBAdapter db;
	int CodigoModulo;
	int Codigodepartamento;
	int CodigoEmpleado;
	int CodigoOpcion;
	int TotalEmpleadosAsignados;
	boolean validoDescriptivo = false;
	boolean validoFiltro = false;
	boolean validoPipeta = false;
	int validoasignado = 0;
	TextView TCantidadAsignados;
	String MaterialS;							   // Variables que Terminan con S  almacenan datos de la tabla Semillas
	String MaterialF;                              // Variables que Terminan con F  almacenan datos de la tabla Filtros
	String SiembraF;
	String FechaF;
	String empleadoEscaneado = "";
	Ringtone ringtone;
	EditText Siembra;
	EditText Filtro;
	EditText Pipeta;
	EditText Azucarera;
	EditText empleado;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_captura_succion);
		
	//se obtienen lo datos de la actividad antetior por medio del intent
			Bundle extras = getIntent().getExtras();
			CodigoModulo = extras.getInt("CodigoModulo");
		    CodigoEmpleado = extras.getInt("CodigoEmpleado");
		    Codigodepartamento = extras.getInt("Codigodepartamento");
		    CodigoOpcion = extras.getInt("CodigoOpcion");
		    String NombreOpcion = extras.getString("NombreOpcion");
		    		    
		    //hacemos referencia a todos los objetos de nuestra vista
		    TextView TNombreCaptura = (TextView)findViewById(R.id.OpcionCaptura);
		    TextView TCodigoEmpleado= (TextView)findViewById(R.id.CodigoEmpleado);
		    TextView TCodigoOpcion= (TextView)findViewById(R.id.codigoOpcion);
		    	       
		    TCantidadAsignados = (TextView)findViewById(R.id.textEmpleadosAsginados);
		    Siembra = (EditText)findViewById(R.id.editSiembra);
		   	Filtro  = (EditText)findViewById(R.id.editFiltro);
		    Pipeta  = (EditText)findViewById(R.id.editPipeta);
		    Azucarera = (EditText)findViewById(R.id.editAzucarera);
		    empleado  = (EditText)findViewById(R.id.editEmpleado);
		    Button buttonNuevo = (Button) findViewById(R.id.btnNuevo);
	        Button btnBack = (Button) findViewById(R.id.btnBack);
	        
	        // Se Asigna el tipo de dato a los EditText que unicamente permiten N�meros
	        Siembra.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
	        Siembra.setInputType(InputType.TYPE_CLASS_NUMBER);
	        Siembra.setSingleLine(false);
	        empleado.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
	        empleado.setInputType(InputType.TYPE_CLASS_NUMBER);
		    empleado.setSingleLine(false);
		    Filtro.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
	        Filtro.setInputType(InputType.TYPE_CLASS_NUMBER);
		    Filtro.setSingleLine(false);
		    Azucarera.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
		    Azucarera.setInputType(InputType.TYPE_CLASS_NUMBER);
		    Azucarera.setSingleLine(false);
		   
	        		    
		    //hacemos las inicializaciones necesarias a nuestra vista
		    TNombreCaptura.setText(NombreOpcion+ "");
		    TCodigoEmpleado.setText(CodigoEmpleado + "");
		    TCodigoOpcion.setText(CodigoOpcion + "");
		    validoDescriptivo = false;
		    validoFiltro = false;
		    validoPipeta = false;
		    Calendar c = Calendar.getInstance(); 
    		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
    		final String fecha = fdate.format(c.getTime());
		    Siembra.requestFocus(); 
		   
		    appState = ((MyApp)getApplicationContext());   
		    db = appState.getDb();
		    db.open();
		    
		    verificarAsginados();
		    
		    Siembra.setOnKeyListener(new View.OnKeyListener() {
	            public boolean onKey(View v, int keyCode, KeyEvent event) {
	                // se manda a llamar cuando se presione Enter
	                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
	                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
	                	VerificaSiembra(Siembra.getText().toString().trim());
		                	if (validoDescriptivo == true){
		                		Filtro.requestFocus();
		                		Filtro.setEnabled(true);
		                		Pipeta.setEnabled(true);
		                		Azucarera.setEnabled(true);
		                		empleado.setEnabled(true);
		                		Siembra.setEnabled(false);
		                	}else{
			    	    		alertDialogMensaje("Descriptivo", "Siembra Invalida o No Encontrada \n Favor Descargar D_Semillas");
			    	    		Siembra.setEnabled(true);
			    	    		Siembra.setText("");
			    	    		Filtro.setText("");
			    	    		Pipeta.setText("");
			    	    		Azucarera.setText("");
			    	    		empleado.setText("");
			    	    		Siembra.requestFocus();
		                	}
		                	return true;  
	                	  }     
	                	return false;
	            	}	  
	        	});
		    
		    Filtro.setOnKeyListener(new View.OnKeyListener() {
				public boolean onKey(View v, int keyCode, KeyEvent event) {
					if((event.getAction() == KeyEvent.ACTION_DOWN)&& (keyCode == KeyEvent.KEYCODE_ENTER)){
						VerificarFiltro(Filtro.getText().toString().trim());
						String SiembraS = Siembra.getText().toString().trim();
						if(validoFiltro == true) {
							if(SiembraS.equals(SiembraF)){
								if(MaterialS.equals(MaterialF)){
									if(FechaF.equals(fecha)){
										Pipeta.requestFocus();
										Pipeta.setEnabled(true);
										Azucarera.setEnabled(true);
										empleado.setEnabled(true);
										Filtro.setEnabled(false);
									}else{
										alertDialogMensaje("FECHA SUCCION","La Fecha de Succion No Corresponde \n La fecha de solicitud es: " + FechaF);
										Filtro.setText("");
										Pipeta.setText("");
										Azucarera.setText("");
										empleado.setText("");
										Filtro.setEnabled(true);
										Filtro.requestFocus();
										}
								}else{
									alertDialogMensaje(" MATERIAL ","El Material \n No Corresponde");
									Filtro.setText("");
									Pipeta.setText("");
									Azucarera.setText("");
									empleado.setText("");
									Filtro.setEnabled(true);
									Filtro.requestFocus();
									} 
							} else{
								alertDialogMensaje(" SIEMBRA ","El N�mero de Siembra \n No Corresponde");
								Filtro.setText("");
								Pipeta.setText("");
								Azucarera.setText("");
								empleado.setText("");
								Filtro.setEnabled(true);
								Filtro.requestFocus();
								}
						}else{
							alertDialogMensaje(" FILTRO ","El Filtro NO existe \n Descargar D_Filtros");
							Filtro.setText("");
							Pipeta.setText("");
							Azucarera.setText("");
							empleado.setText("");
							Filtro.setEnabled(true);
							Filtro.requestFocus();
							}
						return true;
					}
					return false;
				}
			});
		    
		    Pipeta.setOnKeyListener(new View.OnKeyListener() {
				public boolean onKey(View v, int keyCode, KeyEvent event) {
					if((event.getAction() == KeyEvent.ACTION_DOWN)&& (keyCode == KeyEvent.KEYCODE_ENTER)){
					String MaterialP = Pipeta.getText().toString().trim();
					if(MaterialP.equals(MaterialF)){
						Azucarera.requestFocus();
						Azucarera.setEnabled(true);
						Pipeta.setEnabled(false);
					} else {
						alertDialogMensaje(" PIPETA ","La Pipeta Escaneada \n NO Corresponde al Material");
						Pipeta.setText("");
						Azucarera.setText("");
						empleado.setText("");
						Pipeta.setEnabled(true);
						Pipeta.requestFocus();
					}
					return true;
					}
					return false;
				}
			});
		    
		    Azucarera.setOnKeyListener(new View.OnKeyListener() {
				public boolean onKey(View v, int keyCode, KeyEvent event) {
					if((event.getAction() == KeyEvent.ACTION_DOWN)&& (keyCode == KeyEvent.KEYCODE_ENTER)){
					String SiembraA = Azucarera.getText().toString().trim();
					if(SiembraA.equals(SiembraF)){
						empleado.requestFocus();
						empleado.setEnabled(true);
						Azucarera.setEnabled(false);
					} else {
						alertDialogMensaje(" Azucarera ","La Azucarera Escaneada \n NO Corresponde a la Siembra");
						Azucarera.setText("");
						empleado.setText("");
						Azucarera.setEnabled(true);
						Azucarera.requestFocus();
					}
					return true;
					}
					return false;
				}
			});
		    
		    empleado.setOnKeyListener(new View.OnKeyListener() {
				public boolean onKey(View v, int keyCode, KeyEvent event) {
					if((event.getAction() == KeyEvent.ACTION_DOWN)&& (keyCode == KeyEvent.KEYCODE_ENTER)){
						empleadoEscaneado = empleado.getText().toString().trim();
						String val = "";
						String Siem = Siembra.getText().toString().trim();
						String Fil = Filtro.getText().toString().trim();
						String Pip = Pipeta.getText().toString().trim();
						String Azu = Azucarera.getText().toString().trim();
						if(!(Siem.equals(val))){
							if(!(Fil.equals(val))){
								if(!(Pip.equals(val))){
									if(!(Azu.equals(val))) {
									VerificarFiltro2(Filtro.getText().toString().trim());
									if(!(validoasignado == 1)){
									GuardarDatos();
									 return true;
									}else{
									empleado.setText("");  
					        		Siembra.setText("");
					        		Filtro.setText("");
					        		Pipeta.setText("");
					        		Siembra.setEnabled(true);		
					        		Siembra.requestFocus();
									}
									}else{
										alertDialogMensaje(" AZUCARERA "," AZUCARERA No puede ser vacio");
										Azucarera.setText("");
										empleado.setText("");
										Azucarera.setEnabled(true);
										Azucarera.requestFocus();
									}
								}else{
									alertDialogMensaje(" PIPETA "," Pipeta No puede ser vacio");
									Pipeta.setText("");
									Azucarera.setText("");
									empleado.setText("");
									Pipeta.setEnabled(true);
									Pipeta.requestFocus();
								}
							}else{
								alertDialogMensaje(" FILTRO "," Filtro No puede ser vacio ");
								Azucarera.setText("");
								Filtro.setText("");
								Pipeta.setText("");
								empleado.setText("");
								Filtro.setEnabled(true);
								Filtro.requestFocus();
								}
						}else{
		    	    		alertDialogMensaje(" DESCRIPTIVO ", " Siembra No puede ser vacio ");
		    	    		Siembra.setEnabled(true);
		    	    		Azucarera.setText("");
		    	    		Siembra.setText("");
		    	    		Filtro.setText("");
		    	    		Pipeta.setText("");
		    	    		empleado.setText("");
		    	    		Siembra.requestFocus();
	                	}
						return true;
					}
					return false;
				}
			});
	
		    btnBack.setOnClickListener(new View.OnClickListener() {
	            public void onClick(View v) {
	            	onBackPressed();
	            }
	        });

		    //se limpian los campos y variables para una nueva captura
		    buttonNuevo.setOnClickListener(new View.OnClickListener() {
		        public void onClick(View v) {
					 Siembra.setText("");
					 Filtro.setText("");
					 Pipeta.setText("");  
					 Azucarera.setText("");
					 empleado.setText("");  
					 validoDescriptivo = false;
					 validoFiltro = false;
					 validoPipeta = false;
					 Siembra.setEnabled(true);
					 Filtro.setEnabled(true);
					 Pipeta.setEnabled(true);
					 Azucarera.setEnabled(true);
					 empleado.setEnabled(true);
					 Siembra.requestFocus();
		        }
		    });
	}	
	
	public void verificarAsginados(){
		Cursor CursorGuardados =	db.getDguardados("Actividad=" + CodigoOpcion + " AND ActividadFinalizada=0" );
		TotalEmpleadosAsignados = CursorGuardados.getCount();
		TCantidadAsignados.setText(TotalEmpleadosAsignados + "");
		CursorGuardados.close();
	}
	
	public void VerificaSiembra(String dato){	
		 Cursor CursorSemillas = db.getSemillas("Siembra='" + dato + "'");
		 if(CursorSemillas.getCount()>0){
			 Semillas s = db.getSemillasFromCursor(CursorSemillas, 0);
			 Log.d("SEMILLA",s.getSiembra() + ", "+s.getMaterial() + " ," + s.getPolinizacion());
			 validoDescriptivo = true;
			 MaterialS = s.getMaterial();
		 }else{
			 MaterialS = "";
			 validoDescriptivo = false;
		 }
		 CursorSemillas.close();
	}

	public void VerificarFiltro(String dato) {
		Cursor CursorFiltros = db.getDFiltros ("Id='" + dato + "'");
		 if(CursorFiltros != null  && CursorFiltros.getCount()>0){
				 Filtros f = db.getDFiltrosFromCursor(CursorFiltros, 0);
				 Log.d("FILTROS",f.getNoSiembra() + ", "+f.getMaterial() + " ," + f.getFechaSuccion());
				 validoFiltro = true;
				 MaterialF = f.getMaterial();
				 SiembraF = f.getNoSiembra();
				 FechaF = f.getFechaSuccion();
		 }else{
			 SiembraF = "";
			 MaterialF = "";
			 FechaF = "";
			 validoFiltro = false;
			 
		 }
		 CursorFiltros.close();
	}
	
		// Filtro sin Asignar  = 0
		// Filtro Asignado a igual empleado = 2
		// Filtro Asignado diferente empleado = 1
	public void VerificarFiltro2(String dato) {
			validoasignado = 0;
			int emp = 0;
			int emp1 = Integer.parseInt(empleadoEscaneado); 
			String Nemp = null;
			Cursor CursorGuardados = db.getDguardados ("CorrelativoBolsa=" + Integer.parseInt(dato));
			 if(CursorGuardados != null  && CursorGuardados.getCount()>0){
				 Dguardados d = db.getDguardadosFromCursor(CursorGuardados, 0);
				 emp = Integer.parseInt(d.getCodigoEmpleado().toString().trim());
				 String bol = d.getCorrelativoBolsa(); 
				 if(emp1 == emp){
					 validoasignado = 2;
				 }else{
					 Cursor Codigoempleado = this.db.getDempledo("CodigoEmpleado=" + emp);
					 Dempleado DatosEmpleado = this.db.getDempleadoFromCursor(Codigoempleado, 0);
					 if(DatosEmpleado != null){
						 Nemp = DatosEmpleado.getNombre();
					 }else{
						 Nemp = "NOMBRE NO ENCONTRADO";
					 }
				 alertDialogMensaje("DATOS","FILTRO YA FUE ASIGNADO A:  \n\n"+ Nemp + "\n\nFILTRO: " +bol );	 
				 validoasignado = 1;
				 Codigoempleado.close();
				 }
			 }else{  
				 validoasignado = 0;
			}
			CursorGuardados.close();
		}
	

	//retornara un valor dependiendo del estado del empleado
	// 0 = no existe
	// 1 = existe pero esta asignado
	// 2 = existe y no esta asignado
	public int VerificarEmpleado(String empleado){
			int valor = 0;
			Cursor CursorDacceso =	db.getDacceso("CodigoEmpleado='" + empleado + "'");
			if(CursorDacceso != null  && CursorDacceso.getCount()>0){
				Cursor CursorGuardados =	db.getDguardados("CodigoEmpleado=" + empleado + " AND ActividadFinalizada=0");
				if(CursorGuardados.getCount()>0){
					valor = 1;
				}else{
					valor = 2;
				}
				CursorGuardados.close();
			}
			CursorDacceso.close();
			return valor;
	}
	
	public void GuardarDatos(){
    	int existe = VerificarEmpleado(empleadoEscaneado);
    	if(existe == 2){
        		Dguardados dtemp = new Dguardados();
        		dtemp.setCodigoEmpleado(Integer.parseInt(empleadoEscaneado));
        		dtemp.setCodigoDepto(Codigodepartamento);
        		dtemp.setSiembra(Siembra.getText().toString().trim());
        		
        		//-----------------------------------------------------------------------------------
        		String correlativo="";
        		    	//almaceno el empleado en una variable string
        		    	String getEmpleado = empleado.getText().toString().trim();
        		    	// guardo la variable que tiene empleado en un array
        		    	String[]empl = new String[]{getEmpleado};
        		    	Cursor obtenerCorrelativo = db.getCorrelativo(empl);
        		    	//recorro dicho arreglo para obtener el campo
        		    	if (obtenerCorrelativo.moveToFirst()){
        		    		do{
        		    			correlativo = obtenerCorrelativo.getString(0);
        		    		}while(obtenerCorrelativo.moveToNext());
        		    	}
        		//-------------------------------------------------------------------------
        		Calendar c = Calendar.getInstance(); 
        		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
        		String fecha = fdate.format(c.getTime());
        		SimpleDateFormat ftime = new SimpleDateFormat("HH:mm:ss");
        		String horaInicio = ftime.format(c.getTime());
        		dtemp.setFecha(fecha);
        		dtemp.setHoraIncio(horaInicio);
        		dtemp.setActividad(CodigoOpcion);
        		dtemp.setSupervisor(CodigoEmpleado);
        		dtemp.setCorrelativoBolsa(Filtro.getText().toString().trim());
        		dtemp.setActividadFinalizada(0);
        		dtemp.setCorrelativoEmpleado(correlativo);
       
        		Log.d("Dguardados", dtemp.toString().trim());    		
        		db.insertarDguardados(dtemp);
        		
        		//generacion archivo json
        		ObjectMapper mapper = new ObjectMapper();
        		try {
        			String nameJson ="G_" + dtemp.getCodigoEmpleado() + "_"+dtemp.getFecha() + "_"+dtemp.getHoraIncio();
        			nameJson=nameJson.replace(":", "-");	
					mapper.writeValue(new File( Environment.getExternalStorageDirectory()+"/SIM/JsonDescarga/" + nameJson+ ".json"), dtemp);
					
					JsonFiles archivo = new JsonFiles();
					archivo.setName(nameJson+ ".json");
					archivo.setNameFolder("SinFinalizar/");
					archivo.setUpload(0);
					db.insertarJsonFile(archivo);
					
					if(!appState.getSubiendoArchivos()){
						appState.setSubiendoArchivos(true);
						new UptoDropbox().execute(getApplicationContext());
					}
					
        		
        		} catch (JsonGenerationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JsonMappingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        		
        		
        		empleado.setText("");  
        		Siembra.setText("");
        		Filtro.setText("");
        		Pipeta.setText("");
        		Azucarera.setText("");
        		Siembra.setEnabled(true);		
        		Siembra.requestFocus();
            		 		
        		TotalEmpleadosAsignados++;
        		TCantidadAsignados.setText(TotalEmpleadosAsignados + "");
        		
        		
        	}else if(existe == 1){
        		alertDialogMensaje("Asignado", "Este Empleado ya ha sido asignado");
        		empleado.setText("");
        		empleado.requestFocus();
        	}else if(existe == 0){
        		alertDialogMensaje("Marcaje", "Empleado no encontrado \n Descargar D_Acceso");
        		empleado.setText("");
        		empleado.requestFocus();	
    	}
	}
		
	public void alertDialogMensaje(String message1, String mesage2){
		try {
		    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
		    ringtone.play(); 
		} catch (Exception e) {
		    e.printStackTrace();
		}
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(message1);
		alertDialog.setMessage(mesage2);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int which) {
		// here you can add functions
			ringtone.stop();
		}
		});
		alertDialog.show();
	}
		
	
}
