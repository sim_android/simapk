package com.example.sim;

import java.util.ArrayList;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.example.floricultura.R;
import com.example.sim.adapters.DatosLista;
import com.example.sim.adapters.ListAdapter;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.Dempleado;
import com.example.sim.data.Dguardados;

public class LISTADOLIMPIEZA extends ActionBarActivity {
	
	ArrayList<DatosLista> listado = new ArrayList<DatosLista>();
	int CodigoEmpleado;
	
	private MyApp appState;	
	private DBAdapter db;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_listado_limpieza);
		
		Bundle extras = getIntent().getExtras();
	    CodigoEmpleado = extras.getInt("CodigoEmpleado");
	    
	    appState = ((MyApp)getApplicationContext());   
	    db = appState.getDb();
	    db.open();
	    
		ListView listadoVista = (ListView)findViewById(R.id.ListadoLimpieza);
		listado = getDatos();
		
		ListAdapter adaptador = new ListAdapter(this, R.layout.filas_lista, listado);
        listadoVista.setAdapter(adaptador);
        
        Button btnBack = (Button) findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
 
            	onBackPressed();
            }
        });


	}
	
	
	public ArrayList<DatosLista> getDatos(){
		ArrayList<DatosLista> listaTemp = new ArrayList<DatosLista>();
		
		listaTemp.add(new DatosLista("  Nombre\nEmpleado", " Harnero", " Opcion"));

		 Cursor Casignados= null ;
		 Casignados = this.db.getDguardados("Supervisor=" + CodigoEmpleado + " AND ActividadFinalizada=0");
		 
		 if(Casignados.getCount()>0){
			 for(int i=0 ; i<Casignados.getCount();i++){	    
				 
				 Dguardados Dtemp = this.db.getDguardadosFromCursor(Casignados, i);
				 
				 Cursor Codigoempleado = this.db.getDempledo("CodigoEmpleado=" + Dtemp.getCodigoEmpleado().toString().trim());
				 Dempleado DatosEmpleado = this.db.getDempleadoFromCursor(Codigoempleado, 0);
				 if(DatosEmpleado != null){
					 listaTemp.add(new DatosLista("    "+DatosEmpleado.getNombre().toString().trim(),"       " + Dtemp.getSiembra()," " +Dtemp.getActividad()));
				 } else {
					 listaTemp.add(new DatosLista("    "+ " NOMBRE NO ENCONTRADO ","       " + Dtemp.getSiembra()," " +Dtemp.getActividad()));
				 }
				 
				 
				 Codigoempleado.close();
		     } 
		 }
		 
		 Casignados.close();
		
		return listaTemp;
		

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.listado_actividad, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}



}
