package com.example.sim;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.example.floricultura.R;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.Dguardados;
import com.example.sim.data.JsonFiles;
import com.example.sim.data.Modulos;
import com.example.sim.data.Opciones;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class FinalizarActividad extends ActionBarActivity {
	
	private MyApp appState;	
	private DBAdapter db;
	int CodigoModulo;
	int Codigodepartamento;
	int CodigoEmpleado;
	int CodigoOpcion;
	String NombreOpcion;
	int ValorPolinizacion;
	EditText empleado;
	int finalizar;
	Ringtone ringtone;
	EditText EditBancas;
    EditText EditFlores;
	EditText EditPlantas;
	EditText EditHora;
	EditText EditMin;
	int OpcionAsignado = 0;
	int NoModuloAsignado = 0;
	String ModuloAsignado = "";
	int ExcepMadre = 201;
	int ExcepPolen = 202;
	int ExcepLM = 23107;
	String datoPlantaM;
	String datoEditPlantas;
	String hora;
	String min;
	int datoHora = 0;
	int datoMin = 0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_finalizar_actividad);
		
		Bundle extras = getIntent().getExtras();
		CodigoModulo = extras.getInt("CodigoModulo");
	    CodigoEmpleado = extras.getInt("CodigoEmpleado");
	    Codigodepartamento = extras.getInt("Codigodepartamento");
	    CodigoOpcion = extras.getInt("CodigoOpcion");
	    NombreOpcion = extras.getString("NombreOpcion");
	    
	    
	    TextView TNombreCaptura = (TextView)findViewById(R.id.OpcionCaptura);
	    TextView TCodigoEmpleado= (TextView)findViewById(R.id.CodigoEmpleado);
	    TextView TCodigoOpcion= (TextView)findViewById(R.id.codigoOpcion);
	    empleado  = (EditText)findViewById(R.id.editEmpleado);
	    Button btnBack = (Button) findViewById(R.id.btnBack);
	    Button btnListado = (Button) findViewById(R.id.btnListado);
	    Button btnNuevo = (Button) findViewById(R.id.btnNuevo);
	    
	    final CheckBox Check = (CheckBox)findViewById(R.id.checkCorte);
	    TextView TxtBancas = (TextView)findViewById(R.id.textViewBancas);
	    TextView TxtFlores= (TextView)findViewById(R.id.textFlores);
	    TextView TextPlantas= (TextView)findViewById(R.id.textPlantas);
	    
	    TextView TextHora= (TextView)findViewById(R.id.textPlantas);
	    TextView TextMin= (TextView)findViewById(R.id.textPlantas);
	    
	    EditBancas = (EditText)findViewById(R.id.editBancas);
	    EditFlores = (EditText)findViewById(R.id.editFlores);
	    EditPlantas = (EditText)findViewById(R.id.editPlantas);
	    EditHora = (EditText)findViewById(R.id.editHora);
	    EditMin = (EditText)findViewById(R.id.editMin);
	    EditBancas.setInputType(InputType.TYPE_CLASS_NUMBER |InputType.TYPE_NUMBER_FLAG_DECIMAL);
	    EditBancas.setSingleLine(false);
	 
	    if(Codigodepartamento == 1){	
	    	TxtFlores.setVisibility(View.INVISIBLE);
	    	EditFlores.setVisibility(View.INVISIBLE);
	    	EditPlantas.setVisibility(View.INVISIBLE);
	    	TextPlantas.setVisibility(View.INVISIBLE);
	    	//empleado.requestFocus();
	    	EditBancas.requestFocus();
	    	
	    }else{
	    	Check.setVisibility(View.INVISIBLE);
	    	TxtBancas.setVisibility(View.INVISIBLE);
	    	TxtFlores.setVisibility(View.INVISIBLE);
	    	EditBancas.setVisibility(View.INVISIBLE);
	    	EditFlores.setVisibility(View.INVISIBLE);
	    	//empleado.requestFocus();
	    	EditPlantas.requestFocus();
	    }
	    
	    TNombreCaptura.setText(NombreOpcion+ "");
	    TCodigoEmpleado.setText(CodigoEmpleado + "");
	    TCodigoOpcion.setText(CodigoOpcion + "");	    
	    
	    //se valida la opcion finalizar actividad
	    appState = ((MyApp)getApplicationContext());   
	    db = appState.getDb();
	    db.open();
	    
	    Cursor CursorOpciones= null ;
	    CursorOpciones = db.getOpciones("Opcion=" + CodigoOpcion);
	    
	    if(CursorOpciones.getCount()>0){
	    	Opciones op = db.getOpcionesFromCursor(CursorOpciones, 0);
	    	finalizar = op.getFinalizaActividad();
	    }
	    CursorOpciones.close();
	    
	    if (Codigodepartamento == 1){	
		    //Se verfica el input azucarera al persionar enter
		    empleado.setOnKeyListener(new View.OnKeyListener() {
	            public boolean onKey(View v, int keyCode, KeyEvent event) {
	                // If the event is a key-down event on the "enter" button
	                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
	                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
	                	if(Check.isChecked()== true){
	                		Cursor CursorGuardados =	db.getDguardados("CodigoEmpleado=" + Integer.parseInt(empleado.getText().toString().trim())+ " AND ActividadFinalizada=0");
	                		if(CursorGuardados.getCount()>0){
	                			Dguardados dtemp =  (Dguardados)db.getDguardadosFromCursor(CursorGuardados, 0);
	                			String Actividad = dtemp.getActividad().toString().trim();
	                			String Corte = "CORTE";
	                			String ACT = ObtenAct(Actividad);
	                			if(ACT.equals(Corte)){
	                				GuardaVegetativo();
	                				empleado.requestFocus();
	                			}else{
	                				alertDialogMensaje("ERROR","EMPLEADO ASIGNADO EN ACTIVIDAD DIFERENTE A CORTE " +
	                									"QUITAR EL CHECK PARA ESTE EMPLEADO.");
	                				EditBancas.setText("");
	                            	EditFlores.setText("");
	                            	EditPlantas.setText("");
	                            	empleado.setText("");
	                            	EditHora.setText("");
	                            	EditMin.setText("");
	                            	empleado.requestFocus();
	                			}	
	                		} else{
	                			alertDialogMensaje("ERROR","ERROR EN EMPLEADO");
	                			EditBancas.setText("");
	                			EditFlores.setText("");
	                			EditPlantas.setText("");
	                			empleado.setText("");
	                			EditHora.setText("");
	                			EditMin.setText("");
	                			empleado.requestFocus();
	                		}
	                		CursorGuardados.close();
	                	}else {
	                		Cursor CursorGuardados = db.getDguardados("CodigoEmpleado=" + Integer.parseInt(empleado.getText().toString().trim())+ " AND ActividadFinalizada=0");
	                		if(CursorGuardados.getCount()>0){
	                			Dguardados dtemp =  (Dguardados)db.getDguardadosFromCursor(CursorGuardados, 0);
	                			String Actividad = dtemp.getActividad().toString().trim();
	                			String Corte = "CORTE";
	                			String ACT = ObtenAct(Actividad);
	                			if(ACT.equals(Corte)){
	                				alertDialogMensaje("ERROR","EMPLEADO ASIGNADO A CORTE " +
	                									"PONER CHECK EN CORTE.");
	                				EditBancas.setText("");
		                			EditFlores.setText("");
		                			EditPlantas.setText("");
		                			empleado.setText("");
		                			EditHora.setText("");
		                			EditMin.setText("");
		                			empleado.requestFocus();
	                			} else {	
	                				int Size = EditBancas.getText().length();
	                				if(Size > 3 ){
	                					alertDialogMensaje("ERROR","ERROR VERIFICAR BANCAS");
	                					EditBancas.setText("");
			                			EditFlores.setText("");
			                			EditPlantas.setText("");
			                			empleado.setText("");
			                			EditHora.setText("");
			                			EditMin.setText("");
			                			EditBancas.requestFocus();
	                				}else{
	                				String Bancas = EditBancas.getText().toString().trim();
	                				String Pb = "";
	                				if(Bancas.equals(Pb)){
	                					alertDialogMensaje("ERROR","ERROR DEBE INGRESAR BANCAS");
	                					EditBancas.setText("");
	                					empleado.setText("");
	                					EditBancas.requestFocus();
	                				} else {
	                					if(Bancas.contains("0")||Bancas.contains("1")||Bancas.contains("2")||Bancas.contains("3")||Bancas.contains("4")||Bancas.contains("5")||Bancas.contains("6")||Bancas.contains("7")||Bancas.contains("8")||Bancas.contains("9")||Bancas.contains(".")){
	                					 	 if (Bancas.contains(",")){
	                						alertDialogMensaje("ERROR","ERROR AL INGRESAR BANCAS \n VERIFICAR QUE SE INGRESO UN PUNTO  \n Y NO UNA COMA...");
	                					 	 }else{
	                					 		GuardaVegetativo();
	                					 	 } 
	                					}else {
	                					 		 alertDialogMensaje("ERROR","VALOR INVALIDO EN BANCAS \n EMPLEADO NO SE FINALIZO");
	                					 	 }	                					
	                					}
	                				}
	                			}
	                			}else{
	                				alertDialogMensaje("ERROR","EMPLEADO NO ASIGNADO");
                					EditMin.setText("");
                					EditHora.setText("");
	                				EditBancas.setText("");
	                				empleado.setText("");
	                				EditBancas.requestFocus();
	                			}	
	                		CursorGuardados.close();
	                	}
		                return true;
	                }
	                return false;
	                }
	        });
	    	  	
	    }else{    			    	
			    //Se verfica el input empleado al persionar enter
			    empleado.setOnKeyListener(new View.OnKeyListener() {
		            public boolean onKey(View v, int keyCode, KeyEvent event) {		               
		                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
		                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
		                	
		                	int valid = revisa(Integer.parseInt(empleado.getText().toString().trim()));
		                	if(valid == 1){
		                		if(NoModuloAsignado == CodigoModulo || NoModuloAsignado == ExcepMadre || NoModuloAsignado == ExcepPolen ){
		                			if(OpcionAsignado != ExcepLM){
		                		Cursor CursorGuardados = db.getDguardados("CodigoEmpleado = " + Integer.parseInt(empleado.getText().toString().trim()) +" AND ActividadFinalizada = 0");
		                		if(CursorGuardados != null  && CursorGuardados.getCount()>0){
		                			Dguardados dtemp =  db.getDguardadosFromCursor(CursorGuardados, 0);
		                			String Actividad = dtemp.getActividad().toString().trim();
		                			String PolFlor = "POLINIZACION FLOR/FLOR";
		                			String ACT = ObtenAct(Actividad);	          
		                			
		                			
		                			if(ACT.equals(PolFlor)){
		                				//*******************************************************
		                				lanzar(v);		
		                			}else{
		                				
		                				
		                				//-----------------------------------------------------------------------------------
		                				String correlativo="";
		                				    	//almaceno el empleado en una variable string
		                				    	String getEmpleado = empleado.getText().toString().trim();
		                				    	// guardo la variable que tiene empleado en un array
		                				    	String[]empl = new String[]{getEmpleado};
		                				    	Cursor obtenerCorrelativo = db.getCorrelativo(empl);
		                				    	//recorro dicho arreglo para obtener el campo
		                				    	if (obtenerCorrelativo.moveToFirst()){
		                				    		do{
		                				    			correlativo = obtenerCorrelativo.getString(0);
		                				    		}while(obtenerCorrelativo.moveToNext());
		                				    	}
		                				//-------------------------------------------------------------------------
		                    		Calendar c = Calendar.getInstance(); 
		                    		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
		                    		String fecha = fdate.format(c.getTime());
		                    		SimpleDateFormat ftime = new SimpleDateFormat("HH:mm:ss");
		                    		String horaFin = ftime.format(c.getTime());
		                    		
		                    		dtemp.setFecha(fecha);
		                			dtemp.setHoraFin(horaFin);
		                			dtemp.setSupervisor(CodigoEmpleado);
		                			dtemp.setActividadFinalizada(1);
		                			dtemp.setCorrelativoEmpleado(correlativo);
		                					
		                			if(EditFlores.getText().toString().trim().length()>0)
		                				dtemp.setFlores(EditFlores.getText().toString().trim());	
		                			if(EditPlantas.getText().toString().trim().length()>0)
		                				dtemp.setPlantas(EditPlantas.getText().toString().trim());
		                		
		                			
		                			if(EditHora.getText().toString().trim().length()>0)
		                				dtemp.setHora(Integer.parseInt(EditHora.getText().toString().trim()));
		                			
		                			if(EditMin.getText().toString().trim().length()>0)
		                				dtemp.setMin(Integer.parseInt(EditMin.getText().toString().trim()));
		                			
		                			
		                			db.updateDguardados(dtemp);
		                			
		                			empleado.setText("");
		                			EditPlantas.setText("");
		                			EditFlores.setText("");
		                			EditHora.setText("");
		                			EditMin.setText("");
		                			empleado.requestFocus();
		                    		
		                    		ObjectMapper mapper = new ObjectMapper();
		                    		try {
		                    			String nameJson ="G_" + dtemp.getCodigoEmpleado() + "_"+dtemp.getFecha() + "_"+dtemp.getHoraFin();
		                    			nameJson=nameJson.replace(":", "-");
										mapper.writeValue(new File( Environment.getExternalStorageDirectory()+"/SIM/JsonDescarga/Finalizados/" + nameJson+ ".json"), dtemp);
										
										
										JsonFiles archivo = new JsonFiles();
										archivo.setName(nameJson+ ".json");
										archivo.setNameFolder("Finalizados/");
										archivo.setUpload(0);
										db.insertarJsonFile(archivo);
										
										if(!appState.getSubiendoArchivos()){
											appState.setSubiendoArchivos(true);
											new UptoDropboxFin().execute(getApplicationContext());
										}	
									} catch (JsonGenerationException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									} catch (JsonMappingException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									} catch (IOException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
		                			}
		                		}else{
		                			alertDialogMensaje("Invalido", "Usuario invalido");
		                    		empleado.setText("");
		                    		empleado.requestFocus();
		                		}
		                		CursorGuardados.close();
		                			}else{
		                				alertDialogMensaje("OPCION INVALIDA", "Empleado Asignado A LIMPIEZA MANUAL \n Porfavor finalice en: FINALIZA LIMP. MANUAL");
				                		empleado.setText("");
			                    		empleado.requestFocus();
		                			}
		                	}else {
		                		alertDialogMensaje("MODULO INVALIDO", "Empleado Asignado en modulo Diferente \n Porfavor finalice en: "+ ModuloAsignado);
		                		empleado.setText("");
	                    		empleado.requestFocus();
		                	}
		                	} else {
		                		alertDialogMensaje("EMPLEADO NO ECONTRADO","Empleado No Encontrado, favor descargar \n  D_Acceso e intentar de nuevo");
		                		empleado.setText("");
	                    		empleado.requestFocus();
		                	}
			                return true;
		                }
		                return false;
		            }
		        });	    	
	    }
	    
	    //Se verfica el input azucarera al persionar enter
        EditBancas.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	// se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                		empleado.requestFocus();
	                return true;
                }
                return false;
            }
        });
        
	    //Se verfica el input azucarera al persionar enter
        EditPlantas.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	// se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                		EditFlores.requestFocus();
	                return true;
                }
                return false;
            }
        });
               
	    //Se verfica el input azucarera al persionar enter
        EditFlores.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	// se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                		empleado.requestFocus();
	                return true;
                }
                return false;
            }
        });
	    
	    //Se verfica el input azucarera al persionar enter
        EditHora.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	// se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                		EditMin.requestFocus();
	                return true;
                }
                return false;
            }
        });
	    
        EditMin.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                		empleado.requestFocus();
	                return true;
                }
                return false;
            }
        });
	          
        btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
 
            	onBackPressed();
            }
        });
        
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	EditBancas.setText("");
            	EditFlores.setText("");
            	EditPlantas.setText("");
            	empleado.setText("");
            	EditHora.setText("");
            	EditMin.setText("");
            	if(Codigodepartamento == 1){
            		EditBancas.requestFocus();
            	} else {
            		EditPlantas.requestFocus();
            	}
            }
        });
        
        btnListado.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Intent intent = new Intent(getApplicationContext(), LISTADOACTIVIDAD.class);
           	    intent.putExtra("CodigoEmpleado", CodigoEmpleado );
           	    startActivity(intent);
            	
            }
        });

	    EditBancas.setOnFocusChangeListener(new View.OnFocusChangeListener() {
	        @Override
	        public void onFocusChange(View v, boolean hasFocus) {
	        	Log.d("teclado","111111111111111111111");
	            if (hasFocus) {
	            	Log.d("teclado","22222222222222222");
	            	InputMethodManager imm = (InputMethodManager)
                            getSystemService(Context.INPUT_METHOD_SERVICE);
					if(imm != null){
					  imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
					}
	            	hideSoftKeyboard();
	            }
	        }
	    });
        		
	}
		
	public void GuardaVegetativo(){
		Cursor CursorGuardados =	db.getDguardados("CodigoEmpleado=" + Integer.parseInt(empleado.getText().toString().trim())+ " AND ActividadFinalizada=0");
		if(CursorGuardados.getCount()>0){
			Dguardados dtemp =  (Dguardados)db.getDguardadosFromCursor(CursorGuardados, 0);
			
			
			//-----------------------------------------------------------------------------------
			String correlativo="";
			    	//almaceno el empleado en una variable string
			    	String getEmpleado = empleado.getText().toString().trim();
			    	// guardo la variable que tiene empleado en un array
			    	String[]empl = new String[]{getEmpleado};
			    	Cursor obtenerCorrelativo = db.getCorrelativo(empl);
			    	//recorro dicho arreglo para obtener el campo
			    	if (obtenerCorrelativo.moveToFirst()){
			    		do{
			    			correlativo = obtenerCorrelativo.getString(0);
			    		}while(obtenerCorrelativo.moveToNext());
			    	}
			//-------------------------------------------------------------------------
    		Calendar c = Calendar.getInstance(); 
    		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
    		String fecha = fdate.format(c.getTime());
    		SimpleDateFormat ftime = new SimpleDateFormat("HH:mm:ss");
    		String horaFin = ftime.format(c.getTime());
 
    		dtemp.setFecha(fecha);
			dtemp.setHoraFin(horaFin);
			dtemp.setActividadFinalizada(1);
			dtemp.setSupervisor(CodigoEmpleado);
			dtemp.setCorrelativoEmpleado(correlativo);
			
			if(EditBancas.getText().toString().trim().length()>0)
				dtemp.setBancas(EditBancas.getText().toString().trim());

			if(EditHora.getText().toString().trim().length()>0)
				dtemp.setHora(Integer.parseInt(EditHora.getText().toString().trim()));
			
			if(EditMin.getText().toString().trim().length()>0)
				dtemp.setMin(Integer.parseInt(EditMin.getText().toString().trim()));
			
			db.updateDguardados(dtemp);

    		ObjectMapper mapper = new ObjectMapper();
    		try {
    			String nameJson = "V_" +dtemp.getCodigoEmpleado() + "_"+dtemp.getFecha() + "_"+dtemp.getHoraFin();
    			nameJson=nameJson.replace(":", "-");
				mapper.writeValue(new File( Environment.getExternalStorageDirectory()+"/SIM/JsonDescarga/Finalizados/" + nameJson+ ".json"), dtemp);
				
				JsonFiles archivo = new JsonFiles();
				archivo.setName(nameJson+ ".json");
				archivo.setNameFolder("Finalizados/");
				archivo.setUpload(0);
				db.insertarJsonFile(archivo);
				
				if(!appState.getSubiendoArchivos()){
					appState.setSubiendoArchivos(true);
					new UptoDropboxFin().execute(getApplicationContext());
				}
							
			} catch (JsonGenerationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		empleado.setText("");
    		EditBancas.setText("");
    		EditHora.setText("");
    		EditMin.setText("");
    		EditBancas.requestFocus();
    		//empleado.requestFocus();
    		
		}else{
			alertDialogMensaje("Invalido", "ERROR EMPLEADO INVALIDO");
    		empleado.setText("");
    		empleado.requestFocus();
		}
		CursorGuardados.close();
	}
	
	public void hideSoftKeyboard() {
	    if(getCurrentFocus()!=null) {
	        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
	        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),   InputMethodManager.HIDE_NOT_ALWAYS);
	    }
	}
		
	public void comprueba(){
		String Bancas = EditBancas.getText().toString().trim();
		String Pb = "";
		if(Bancas.equals(Pb)){
			alertDialogMensaje("ERROR","ERROR DEBE INGRESAR BANCAS");
			EditBancas.setText("");
			empleado.setText("");
			EditBancas.requestFocus();
		} 
	}
	
	// Revisa el Modulo en el que fue asignado el empleado y retorna un Valor 1 si el empleado fue encontrado
	public int revisa (int codigo){
		int valor = 0;
		int Op = 0;
		int Md = 0;
		Cursor CursorGuarda =	db.getDguardados("CodigoEmpleado=" + codigo + " AND ActividadFinalizada=0");
		 if(CursorGuarda.getCount()>0){
			 Dguardados dgu = db.getDguardadosFromCursor(CursorGuarda, 0);
			 Op = dgu.getActividad();
			 Cursor MouloOpcion = this.db.getOpciones("Opcion=" + Op);
			 Opciones opc = this.db.getOpcionesFromCursor(MouloOpcion, 0);
			 Md = opc.getCodigoDepto();
			 Cursor Modulos = db.getModulos("CodigoModulo=" + Md);
			 Modulos mod = this.db.getModulosFromCursor(Modulos, 0);
			 NoModuloAsignado = mod.getCodigoModulo();
			 ModuloAsignado = mod.getDescripcion().toString().trim();
			 OpcionAsignado = dgu.getActividad();
			 valor = 1;
			 MouloOpcion.close();
			 Modulos.close();
			 } else {
				valor = 0;
			 }
		 CursorGuarda.close();
		 return valor;
	}
	
	
	public String ObtenAct(String actividad)
	{
		String des = "";
		Cursor Opc = db.getOpciones("Opcion=" + actividad);
		if(Opc.getCount() > 0)
		{
			Opciones op = db.getOpcionesFromCursor(Opc, 0);
			des = op.getDescripcion();
		}
		Opc.close();
		return des;
	}
	
	
	public void alertDialogMensaje(String message1, String mesage2){	
		try {
		    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
		    ringtone.play(); 
		} catch (Exception e) {
		    e.printStackTrace();
		}	
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(message1);
		alertDialog.setMessage(mesage2);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int which) {
		// here you can add functions
			ringtone.stop();
		}
		});
		alertDialog.show();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.finalizar_actividad, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void ButtonOnClick(View v) {
	    switch (v.getId()) {
	      case R.id.buttonborra:
		        agregarNumero("1");
	        break;
	      case R.id.btnDos:
	    	  agregarNumero("2");
	        break;
	      case R.id.btnTres:
		        agregarNumero("3");
	        break;
	      case R.id.btnCuatro:
	    	  agregarNumero("4");
	        break;
	      case R.id.btnCinco:
		        agregarNumero("5");
	        break;
	      case R.id.btnSeis:
	    	  agregarNumero("6");
	        break;
	      case R.id.btnSiete:
	    	  agregarNumero("7");
	        break;
	      case R.id.btnOcho:
		        agregarNumero("8");
	        break;
	      case R.id.btnNueve:
	    	  agregarNumero("9");
	        break;
	      case R.id.button0:
		        agregarNumero("0");
	        break;
	      case R.id.btnCero:
	    	  agregarNumero("b");
	        break;
	      case R.id.botonp:
	    	  agregarNumero(".");
	        break;
	      }
	     
	}
	public void lanzar(View view) {
        Intent i = new Intent(this, PlantaMacho.class );
        datoPlantaM =  empleado.getText().toString();
        datoEditPlantas = EditPlantas.getText().toString();
        hora = EditHora.getText().toString().trim();       
        min = EditMin.getText().toString().trim();
        if (hora.equals("") && min.equals("")|| min.equals("")|| hora.equals("")){
       	 i.putExtra("CodigoModulo",CodigoModulo);
            i.putExtra("CodigoEmpleado",CodigoEmpleado);
            i.putExtra("Codigodepartamento",Codigodepartamento);
            i.putExtra("CodigoOpcion",CodigoOpcion);
            i.putExtra("NombreOpcion",NombreOpcion);
            i.putExtra("datoPlantaM",datoPlantaM);
            i.putExtra("datoEditPlantas", datoEditPlantas);
            i.putExtra("datoHora", 0);
            i.putExtra("datoMin", 0);
            startActivity(i);
        }else{
       	 datoHora = Integer.parseInt(hora);
       	 datoMin = Integer.parseInt(min);
       	 i.putExtra("CodigoModulo",CodigoModulo);
            i.putExtra("CodigoEmpleado",CodigoEmpleado);
            i.putExtra("Codigodepartamento",Codigodepartamento);
            i.putExtra("CodigoOpcion",CodigoOpcion);
            i.putExtra("NombreOpcion",NombreOpcion);
            i.putExtra("datoPlantaM",datoPlantaM);
            i.putExtra("datoEditPlantas", datoEditPlantas);
            i.putExtra("datoHora", datoHora);
            i.putExtra("datoMin", datoMin);
            startActivity(i);
        }
       
        //alertDialogMensaje("Edit Plantas son:",""+datoEditPlantas);
        
       // i.putExtra("texto", txtPlantaMacho.getText().toString());
        
    	super.finish();
  
  }
	
	public void agregarNumero(String n){
		
		if(n.contains("b")){
			
			if(EditBancas.isFocused()){
				EditBancas.setText("");
			}else if(EditFlores.isFocused()){
				EditFlores.setText("");
			}else if(EditPlantas.isFocused()){
				EditPlantas.setText("");
			}else if(EditHora.isFocused()){
				EditHora.setText("");
			}else if(EditMin.isFocused()){
				EditMin.setText("");
			}
		}else{
			if(EditBancas.isFocused()){
				EditBancas.setText(EditBancas.getText() + n);
			}else if(EditFlores.isFocused()){
				EditFlores.setText(EditFlores.getText() + n);
			}else if(EditPlantas.isFocused()){
				EditPlantas.setText(EditPlantas.getText() + n);
			}else if(EditHora.isFocused()){
				EditHora.setText(EditHora.getText() + n);
			}else if(EditMin.isFocused()){
				EditMin.setText(EditMin.getText() + n);
			}
		}			
	}

}

//ESTE CODIGO ERA PARA GENERAR LAS ETIQUETAS AL FINALIZAR AL USUARIO
/*
String query = "CodigoEmpleado=" + Integer.parseInt(empleado.getText().toString().trim()) 
		+ " AND finalizado=0";
ArrayList<EtiquetasVegetativo> lista = new ArrayList<EtiquetasVegetativo>();
Cursor etiquetas =	db.getEtiquetas(query);
for(int i=0 ; i<etiquetas.getCount() ; i++ ){
	EtiquetasVegetativo etemp = db.getEtiquetasFromCursor(etiquetas, i);
	lista.add(etemp);
	etemp.setFinalizado(1);
	db.updateEtiquetas(etemp);
			
}


if(lista.size()>0){
	mapper = new ObjectMapper();
	try {
		String nameJson = "E_" +dtemp.getCodigoEmpleado() + "_"+dtemp.getFecha() + "_"+dtemp.getHoraFin();
		nameJson=nameJson.replace(":", "-");
		mapper.writeValue(new File( Environment.getExternalStorageDirectory()+"/Floricultura/JsonDescarga/" + nameJson+ ".json"), lista);
	} catch (JsonGenerationException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (JsonMappingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
}


else {
			if(Bancas.contains("0")||Bancas.contains("1")||Bancas.contains("2")||Bancas.contains("3")||Bancas.contains("4")||Bancas.contains("5")||Bancas.contains("6")||Bancas.contains("7")||Bancas.contains("8")||Bancas.contains("9")||Bancas.contains(".")){
			alertDialogMensaje("ERROR","ERROR DATO DE BANCAS INCORRECTO");
			EditBancas.setText("");
			empleado.setText("");
			EditBancas.requestFocus();
		}
	  }


*/
	
