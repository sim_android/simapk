package com.example.sim;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import com.example.floricultura.R;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.Dacceso;
import com.example.sim.data.Usuario;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.Vibrator;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

public class DetalleConsultaTiempoCambiado extends ActionBarActivity {
	
	//Variables globales
			private MyApp appState;	
			private DBAdapter db;
			int CodigoModulo;
			int Codigodepartamento;
			int CodigoEmpleado;
			int CodigoOpcion;
			long Pos;
			String UsuarioLogueado;
			String Employee;
			String EmpleadoSeleccionado;
			String Desc;
			String Hours;
			String Fecha;
			String Inv;
			String Empleado;
			GridView gridview;
			GridView gridEnc;
			Vibrator mVibrator;
			Ringtone ringtone;
			String Fechade;
			TextView NomActividad;
			TextView Invernadero;
			TextView FechaF;
			TextView siem;
			String tCorrelativo;
			String tNombre;
			String tApellido;
			String tEmpleado;
			String tPlantas;
			ArrayAdapter<String> adapter;
			ArrayList<String> datos;
			ArrayList<String> encabezado;
			ArrayAdapter<String> adapEnc;
			
			private String val;
	

	@SuppressLint("NewApi") @Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.detalle_tiempo_cambiado);
		// para manejar la excepcion de permisos en el API de Android
    	StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

		//se obtienen lo datos de la actividad antetior por medio del intent
		Bundle extras = getIntent().getExtras();
		CodigoModulo = extras.getInt("CodigoModulo");
		Codigodepartamento = extras.getInt("Codigodepartamento");
	    CodigoOpcion = extras.getInt("CodigoOpcion");
	    CodigoEmpleado = extras.getInt("CodigoEmpleado");
	    Employee = extras.getString("Employee");
	    EmpleadoSeleccionado = extras.getString("EmpleadoSeleccionado");
	    Hours = extras.getString("Hours");
	    UsuarioLogueado = extras.getString("UsuarioLogueado");
	   
	    //hacemos referencia a todos los objetos de nuestra vista
	    NomActividad = (TextView)findViewById(R.id.TextNombreActividad);
	    TextView TCodigoEmpleado= (TextView)findViewById(R.id.CodigoEmpleado);		
	    Button btnBack = (Button) findViewById(R.id.btnBack);
       // Button btnOK = (Button) findViewById(R.id.btnAsistencia);
        final EditText editFechade = (EditText) findViewById(R.id.editFechaDe);	        
        siem = (TextView) findViewById(R.id.TextSiembra);
        Invernadero = (TextView) findViewById(R.id.TextInv);
        FechaF = (TextView) findViewById(R.id.TextFecha);
        
        gridEnc = (GridView) findViewById(R.id.gridEnc);
        gridview = (GridView) findViewById(R.id.gridDetalle);
       
        
        datos = new ArrayList<String>();
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, datos);
        encabezado = new ArrayList<String>();
        adapEnc = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, encabezado);
        
	    //hacemos las inicializaciones necesarias a nuestra vista
        NomActividad.setText(Employee);
	    TCodigoEmpleado.setText(CodigoEmpleado + "");
	    siem.setText(Hours);
	    Invernadero.setText(EmpleadoSeleccionado);
	   
	    //incializamos la BD asi como tambien obtenemos la referencia al singleton
	    appState = ((MyApp)getApplicationContext());   
	    db = appState.getDb();
	    db.open();
	    
	    // Se cargar el Grid con la Funci�n Cargar y se inicializan los valores.
	    Cargar();
     	   	   	  
	    btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
 
            	onBackPressed();
            }
        });
    		    		    
	}
	
	public void Cargar(){
    	datos.clear();
    	adapter.clear();
    	encabezado.clear();
    	adapEnc.clear();
    	Cursor CursorUsuario =	db.getUsuario("CodigoEmpleado=" +  "'" + CodigoEmpleado + "'");
        Usuario codigo = db.getUsuarioFromCursor(CursorUsuario,  0);
        String codigoSupervisor = codigo.getUsuario().substring(2,8);
        
        
      //-----------------------------------------------------------------------------------
    	String correlativo="";
    	    	//almaceno el empleado en una variable string
    	    	//String getEmpleado = "0"+EmpleadoSeleccionado;
    	    	String getEmpleado = "01008786";
    	    	// guardo la variable que tiene empleado en un array
    	    	String[]empl = new String[]{getEmpleado};
    	    	Cursor obtenerCorrelativo = db.getCorrelativo(empl);
    	    	//recorro dicho arreglo para obtener el campo
    	    	if (obtenerCorrelativo.moveToFirst()){
    	    		do{
    	    			correlativo = obtenerCorrelativo.getString(0);
    	    		}while(obtenerCorrelativo.moveToNext());
    	    	}
    	//-------------------------------------------------------------------------

    	
    	try {
    	String user = "sim";
    	String pasword = "sim";
    	 Connection connection = null;
    	 ResultSet rs = null;
    	 Statement statement = null;
    	 	
    	 
			Class.forName("com.ibm.as400.access.AS400JDBCDriver");
			connection = DriverManager.getConnection("jdbc:as400://190.190.190.10", user, pasword);	
			statement = connection.createStatement();			     
			rs = statement.executeQuery(
			"SELECT b.fecha,b.codigoempleado,b.correlativoempleado,b.empleado,b.TIPO_HORA,b.Tiempo,b.s_nombre sUPERVISOR FROM SISRRHHF.MEMPLEADO A inner join sisrrhhf.mempleadoestado ee on a.empleadoestado = ee.codigoempleadoestado inner join (SELECT TC.FECHA,TC.SUPERVISOR,(s.nombre||'-'||s.apellido) S_Nombre,TC.CODIGOEMPLEADO,TC.CORRELATIVOEMPLEADO,(e.nombre||'-'||e.apellido) Empleado,TC.ACTIVIDAD,'HORAS TRABAJADAS' TIPO_HORA,(((sum(cast(TC.HORAT as decimal(10,2))) * 60.00) + sum(cast(TC.MINUTOT as decimal(10,2)))) / 60.00)  Tiempo FROM SISAPPF.APP_TMP_TIEMPOCAMBIADO TC inner join sisrrhhf.mempleado s on tc.supervisor = s.correlativo inner join sisrrhhf.mempleado e on tc.correlativoempleado = e.correlativo inner join sisrrhhf.mempleadoestado ee on e.empleadoestado = ee.codigoempleadoestado WHERE TC.ACTIVIDAD IN ('61192315','30692313','21892311','14792309','14692307','20592258') and ee.activo = 'SI' and e.codigoencargado = '"+ codigoSupervisor +"' group by TC.FECHA,TC.SUPERVISOR,s.nombre,s.apellido,TC.CODIGOEMPLEADO,TC.CORRELATIVOEMPLEADO,e.nombre,e.apellido,TC.ACTIVIDAD,e.empleadoestado UNION SELECT TC.FECHA,TC.SUPERVISOR,(s.nombre||'-'||s.apellido) S_Nombre ,TC.CODIGOEMPLEADO,TC.CORRELATIVOEMPLEADO,(e.nombre||'-'||e.apellido) Empleado ,TC.ACTIVIDAD,'HORAS GOZADAS' TIPO_HORA ,(-(((sum(cast(TC.HORAT as decimal(10,2))) * 60.00) + sum(cast(TC.MINUTOT as decimal(10,2)))) / 60.00))  Tiempo FROM SISAPPF.APP_TMP_TIEMPOCAMBIADO TC inner join sisrrhhf.mempleado s on tc.supervisor = s.correlativo inner join sisrrhhf.mempleado e on tc.correlativoempleado = e.correlativo inner join sisrrhhf.mempleadoestado ee on e.empleadoestado = ee.codigoempleadoestado WHERE TC.ACTIVIDAD IN ('61192316','30692314','21892312','14792310','14692308','61092257','30492256','12292255','10892254','20892253') and ee.activo = 'SI' and e.codigoencargado = '"+ codigoSupervisor +"' and tc.estado = 'AC'  group by TC.FECHA,TC.SUPERVISOR,s.nombre,s.apellido,TC.CODIGOEMPLEADO,TC.CORRELATIVOEMPLEADO,e.nombre,e.apellido,TC.ACTIVIDAD,e.empleadoestado ) b on a.correlativo = b.correlativoempleado where ee.activo = 'SI'  AND B.CORRELATIVOEMPLEADO = '"+ EmpleadoSeleccionado +"' ORDER BY B.Empleado,b.fecha");

		    //Agregar encabezados de columnas	
			String Cn1 = rs.getMetaData().getColumnName(1);
			encabezado.add(Cn1);
			gridEnc.setAdapter(adapEnc);
			/*String Cn2 = rs.getMetaData().getColumnName(2);
			encabezado.add(Cn2);
			gridEnc.setAdapter(adapEnc);
			String Cn3 = rs.getMetaData().getColumnName(3);
			encabezado.add(Cn3);
			gridEnc.setAdapter(adapEnc);*/
			String Cn4 = rs.getMetaData().getColumnName(4);
			encabezado.add(Cn4);
			gridEnc.setAdapter(adapEnc);
			String Cn5 = rs.getMetaData().getColumnName(5);
			encabezado.add(Cn5);
			gridEnc.setAdapter(adapEnc);
			String Cn6 = rs.getMetaData().getColumnName(6);
			encabezado.add(Cn6);
			gridEnc.setAdapter(adapEnc);
			String Cn7 = rs.getMetaData().getColumnName(7);
			encabezado.add(Cn7);
			gridEnc.setAdapter(adapEnc);
			// finaliza agregar encabezados de columnas	
			while (rs.next()) {
			//agrega filas de columnas
				 String d1 = rs.getString(1).trim();
				 //String d2 = rs.getString(2).trim();
				 //String d3 = rs.getString(3).trim();
				 String d4 = rs.getString(4).trim();
			     String d5 = rs.getString(5).trim();
				 String d6 = rs.getString(6).trim();
				 String d7 = rs.getString(7).trim();
				 String c1 = comprobar(d1);
				// String c2 = comprobar(d2);
				 //String c3 = comprobar(d3);
				 String c4 = comprobar(d4);
				 String c5 = comprobar(d5);
				 String c6 = comprobar(d6);
				 String c7 = comprobar(d7);
				 datos.add(c1);
				 gridview.setAdapter(adapter);
				 /*datos.add(c2);
				 gridview.setAdapter(adapter);
				 datos.add(c3);
				 gridview.setAdapter(adapter);
				 */
				 datos.add(c4);
				 gridview.setAdapter(adapter);
				 datos.add(c5);
				 gridview.setAdapter(adapter);
				 datos.add(c6);
				 gridview.setAdapter(adapter);
				 datos.add(c7);
				 gridview.setAdapter(adapter);
			}
			connection.close();	
    	}
		catch (Exception e) {
			Toast toast = Toast.makeText(getApplicationContext(), "Verificar Conexi�n.....", Toast.LENGTH_SHORT);
        	toast.show();
			e.printStackTrace();
		} 
	}
	
	public void alertDialogMensaje(String message1, String mesage2){
	
		try {
		    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
		    ringtone.play();	    	    
		} catch (Exception e) {
		    e.printStackTrace();
		}
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(message1);
		alertDialog.setMessage(mesage2);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int which) {
		// here you can add functions
			ringtone.stop();
		}
		});
		alertDialog.show();
	}
	
	public String comprobar(String cad){
		String Pb = "";
		if(cad.equals(Pb)){
			cad = "---";
		}
		return cad;	
	}
	
	public String format (int f){
		val = "";
		if(f<10){
		val = "0"+Integer.toString(f);	
		}
		else{
		val = Integer.toString(f);
		}
		return val;
	}
			
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.soporteit, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	
}  
