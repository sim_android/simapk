package com.example.sim;





import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import com.dropbox.core.DbxException;
import com.dropbox.core.v2.files.FileMetadata;
import com.example.floricultura.R;



import com.example.sim.data.DBAdapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class descargarAS extends ActionBarActivity {
private ProgressDialog loadingDialog;
private MyApp appState;	
private DBAdapter db;
Ringtone ringtone;
String user="";
String password="";
ArrayList<String> listado = new ArrayList<String>();
CheckBox Usr;
ResultSet rs;
ResultSet rs2;
Statement statement;
Connection connection;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.descargar_as);
		Button btnSemillas = (Button) findViewById(R.id.btnSemillas);
		Button btnVegetativo = (Button) findViewById(R.id.btnVegetativo);
		Button btnIT = (Button) findViewById(R.id.btnIT);
		Button btnBack = (Button)findViewById(R.id.btnBack);
		Button btnDescarga = (Button) findViewById(R.id.cargarDatos);
		final CheckBox Acces = (CheckBox) findViewById(R.id.checkAnterior);
		final CheckBox Sem = (CheckBox) findViewById(R.id.checkBox2);
		final CheckBox Mod = (CheckBox) findViewById(R.id.checkBox3);
		final CheckBox Opc = (CheckBox) findViewById(R.id.checkBox4);
		Usr = (CheckBox) findViewById(R.id.checkBox5);
		final CheckBox Veg = (CheckBox) findViewById(R.id.checkBox6);
		final CheckBox Emp = (CheckBox) findViewById(R.id.checkBox7);
		final CheckBox Bol = (CheckBox) findViewById(R.id.checkBox8);
		final CheckBox Ubi = (CheckBox) findViewById(R.id.checkBox9);
		final CheckBox SiemV = (CheckBox) findViewById(R.id.checkBox10);
		final CheckBox Fil = (CheckBox) findViewById(R.id.checkBox11);
		final CheckBox SemAcond = (CheckBox) findViewById(R.id.checkBox12);
		final CheckBox QCPol = (CheckBox) findViewById(R.id.checkBox13);
	 
	
		btnDescarga.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
			try{
				    	 appState = ((MyApp)getApplicationContext());
						 db = appState.getDb();
						 db.open();
				    	 user = "sim";
			        	 password = "sim";
			        	connection = null;
			        	 rs = null;
			        	 statement = null;
							Class.forName("com.ibm.as400.access.AS400JDBCDriver");
							connection = DriverManager.getConnection("jdbc:as400://190.190.190.10", user, password);
							System.setProperty("http.keepAlive", "false");
							statement = connection.createStatement();	
							Toast toast = Toast.makeText(getApplicationContext(), "Conexion Exitosa.....", Toast.LENGTH_SHORT);
				        	toast.show();
				        	loadingDialog = ProgressDialog.show(descargarAS.this,"", "Descargando....", true, false);   
				if(Usr.isChecked()==true){
					try{
							rs = statement.executeQuery("select upper(u.usuario) usuario,upper(u.clave) clave,upper(u.tipo) tipo,case when e.correlativo is null then 0 else e.correlativo end codigoempleado,case when e.codigodepto is null then 0 else e.codigodepto end CodigoDepto,ifnull(opciones_app,'A') opciones_app from siscfgf.usuario u left join sisrrhhf.mempleado e on u.correlativoempleado = e.correlativo where u.estado = 'AC'");
							//rs = statement.executeQuery("select upper(u.usuario) usuario,upper(u.clave) clave,upper(u.tipo) tipo,case when u.codigoempleado is null then 0 else u.codigoempleado end codigoempleado,case when e.codigodepto is null then 0 else e.codigodepto end CodigoDepto,ifnull(opciones_app,'A') opciones_app from siscfgf.usuario u left join sisrrhhf.mempleado e on u.codigoempleado = e.correlativo where u.estado = 'AC'");
							String prueba = String.valueOf(rs);
						
							if(rs.next()==false){
							}else{
							while (rs.next()){
									String Usuario = rs.getString(1);
									String Clave= rs.getString(2);
									String Tipo = rs.getString(3);
									String CodigoEmpleado = rs.getString(4);
									String CodigoDepto = rs.getString(5);
									String CodigoAPP = rs.getString(6);
									
									//db.insertUsuarios(prueba);
									db.insertUsuarios(Clave, CodigoAPP, CodigoDepto, CodigoEmpleado, Tipo, Usuario);
								}
							}
							
								toast = Toast.makeText(getApplicationContext(), "Guardado en el Dispositivo", Toast.LENGTH_SHORT);
				            	toast.show();
				            	db.close();
							
							
							
							 
		            	}
						catch (Exception e) {
							 toast = Toast.makeText(getApplicationContext(), "Verificar Conexi�n.....", Toast.LENGTH_SHORT);
			            	toast.show();
							e.printStackTrace();
					}
					
				}
				//modulo
				if (Mod.isChecked()==true){
					try{
						rs = statement.executeQuery("select codigo_depto,codigo_modulo,upper(descripcion) descripcion,prioridad,cod_familia,opciones_App,Area from siscfgf.seguridad_moduloasp where estado = 'AC' order by codigo_depto,prioridad");
						
						if(rs.next()== false){	
						}
						else{
							while (rs.next()) {
								String CodigoDepto= rs.getString(1);
								String CodigoModulo = rs.getString(2);
								String Descripcion = rs.getString(3);
								String Prioridad = rs.getString(4);
								String Familia	= rs.getString(5);
								String CodigoAPP = rs.getString(6);
								String Area = rs.getString(7);
								db.insertModulos(Area, CodigoAPP, CodigoDepto, CodigoModulo, Descripcion, Familia,Prioridad);
							}
							
		 			    	 toast = Toast.makeText(getApplicationContext(), "Guardado en el Dispositivo", Toast.LENGTH_SHORT);
			            	toast.show();

							 db.close();
						}
						rs2 = statement.executeQuery("Select codigodepto,descripciondepto,Menu from sisrrhhf.mdepartamento where estado = 'AC'");
				        if(rs2.next()==false){
				        }
				        else{
				        	while (rs2.next()) {
								String CodigoDepto= rs2.getString(1);
								String DescripcionDepto = rs2.getString(2);
								String Menu = rs2.getString(3);
							
								db.insertDepartamento(CodigoDepto, DescripcionDepto, Menu);
							}
				        	 toast = Toast.makeText(getApplicationContext(), "Guardado en el Dispositivo", Toast.LENGTH_SHORT);
				            	toast.show();

								 db.close();
				        }  
							
	            	}
					catch (Exception e) {
						 toast = Toast.makeText(getApplicationContext(), "Verificar Conexi�n.....", Toast.LENGTH_SHORT);
		            	toast.show();
						e.printStackTrace();
					
				}
				}
				//opciones
				if (Opc.isChecked()==true){
					try{
						rs = statement.executeQuery("select codigo_modulo,opcion Opcion,upper(descripcion)descripcion,menu,prioridad,tipoopcion,parametro_evaluacion,poliza,(trim(Validacion1)||trim(Parametro1))Parametro1,(trim(Validacion2)||trim(Parametro2))Parametro2,(trim(Validacion3)||trim(Parametro3)) Parametro3,(trim(Validacion4)||trim(Parametro4)) Parametro4,(trim(Validacion5)||trim(Parametro5))Parametro5,opciones_app,parametro_evaluacion2,parametro_evaluacion3 from siscfgf.seguridad_opcion_moduloasp where estado = 'AC' and acceso in ('APP','MIX') order by codigo_modulo,prioridad");
						if(rs.next()== false){	
						}
						else{
							while (rs.next()) {
								String CodigoDepto =rs.getString(1);
								String Opcion =rs.getString(2);
								String Descripcion =rs.getString(3);
								String Menu =rs.getString(4);
								String Prioridad =rs.getString(5);
								String Tipo =rs.getString(6);
								String Poliza =rs.getString(8);
								String Parametro =rs.getString(9);
								
								
								String Parametro2 =rs.getString(10);
								String Parametro3 =rs.getString(11);
								String Parametro4 =rs.getString(12);
								String Parametro5 =rs.getString(13);
								
								String CodigoApp= "";
								String FinalizaActividad = "0";
								String Pureza1 ="";
								String Pureza2 ="";
								String Pureza3 ="";
								String Pureza4 ="";
								String Pureza5 ="";
								
								//String Parametro_Evaluacion =rs.getString(7).trim();
								//String Parametro_Evaluacion2=rs.getString(15).trim();
								//String Parametro_Evaluacion3=rs.getString(16).trim();
								
								
								db.insertOpciones(CodigoApp,CodigoDepto,Descripcion,FinalizaActividad,Menu,Opcion,Parametro,Parametro2,Parametro3,Parametro4,Parametro5,Poliza,Prioridad,Pureza1,Pureza2,Pureza3,Pureza4,Pureza5,Tipo);
							}

		 			    toast = Toast.makeText(getApplicationContext(), "Guardado en el Dispositivo", Toast.LENGTH_SHORT);
			            	toast.show();

							 db.close();
						}
						
	            	}
					catch (Exception e) {
						 toast = Toast.makeText(getApplicationContext(), "Verificar Conexi�n.....", Toast.LENGTH_SHORT);
		            	toast.show();
						e.printStackTrace();
					
				}
				}
				//Acceso
				if (Acces.isChecked()== true){
					try{
						rs = statement.executeQuery("select e.empresa,e.empresa||e.codigoempleado codigoempleado from sisrrhhf.mempleado e inner join sisrrhhf.mempleadoestado ee on e.empleadoestado = ee.codigoempleadoestado where ee.activo = 'SI'union select empresa,empresa||case when length(trim(correlativo))= 2 then '0000' || trim(correlativo) when length(trim(correlativo))= 3 then '000' || trim(correlativo) when length(trim(correlativo))= 4 then '00' || trim(correlativo) end codigoempleado from sisrrhhf.mempleado where fase between '1' and '4' and empleadoestado = '6' and empresa <> '99'");
						if(rs.next()== false){	
						}
						else{
							while (rs.next()) {
								String Empresa= rs.getString(1);
								String CodigoEmpleado = rs.getString(2);
								db.insertDAceso(Empresa,CodigoEmpleado);
							}

		 			    	toast = Toast.makeText(getApplicationContext(), "Guardado en el Dispositivo", Toast.LENGTH_SHORT);
			            	toast.show();

							 db.close();
						}
						
	            	}
					catch (Exception e) {
						toast = Toast.makeText(getApplicationContext(), "Verificar Conexi�n.....", Toast.LENGTH_SHORT);
		            	toast.show();
						e.printStackTrace();
					
				}
				
					
				}
				//Semillas
				if (Sem.isChecked()== true){
					try{
						rs = statement.executeQuery("select nosiembra siembra,material material,case when metodologiap = 1 then 1 when metodologiap = 2 then 0 when metodologiap = 3 then 2 when metodologiap = 8 then 3 else 0 end polinizacion,metodologia mcosecha,plantas,plantasxbolsa from sissemf.mov_defsiembrae where Plantas > 0 union select '1001' siembra,'0001' material,'0' polinizacion,'0' mcosecha,'0' plantas,'0' plantasxbolsa from sisrrhhf.centrocostomov");
						if(rs.next()== false){	
						}
						else{
							while (rs.next()) {
								String MCosecha= rs.getString(4);
								String Material= rs.getString(2);
								String Plantas= rs.getString(5);
								String Polinizacion= rs.getString(3);
								String Postura= rs.getString(6);
								String Produccion= "";
								String Siembra= rs.getString(1).trim();
								db.insertSemillas(MCosecha,Material,Plantas,Polinizacion,Postura,Produccion,Siembra);
							}

		 			    	toast = Toast.makeText(getApplicationContext(), "Guardado en el Dispositivo", Toast.LENGTH_SHORT);
			            	toast.show();

							 db.close();
						}
						
	            	}
					catch (Exception e) {
						toast = Toast.makeText(getApplicationContext(), "Verificar Conexi�n.....", Toast.LENGTH_SHORT);
		            	toast.show();
						e.printStackTrace();
					
				}
				
					
				}
				//dBolsas y azucareras
				if (Bol.isChecked()== true){
					try{
						rs = statement.executeQuery("select empresa,codigo,nomaterial material,nosiembra siembra,fechacosecha from sissemf.mov_etiquetasc where estado = 'BE' and fechacosecha > '2017-06-04'");
						
						if(rs.next()== false){	
							alertDialogMensaje("Mensaje","Vacio de la Bd");
						}
						else{
							while (rs.next()) {
								String Empresa= rs.getString(1);
								String Codigo= rs.getString(2);
								String Material= rs.getString(3);
								String Siembra= rs.getString(4);
								String FechaCosecha= rs.getString(5);

								db.insertDBolsas(Empresa,Codigo,Material,Siembra,FechaCosecha);
							}
		 			    	toast = Toast.makeText(getApplicationContext(), "Guardado en el Dispositivo", Toast.LENGTH_SHORT);
			            	toast.show();

							 db.close();
						}
						rs2 = statement.executeQuery("Select ep.identificador||ep.correlativo Codigo,ep.nosiembra Siembra,m.codigomaterial Material from sispolf.mov_etiquetaspolen as ep left join sissemf.mov_defsiembrae e on ep.nosiembra =e.nosiembra left join sissemf.mmaterialsem m on e.material = m.codigomaterial where ep.estado = 'AC'");
						if(rs2.next()== false){	
							alertDialogMensaje("Mensaje","Vacio de la Bd");
						}
						else{
							while (rs2.next()) {
								String Codigo= rs2.getString(1);
								String Material= rs2.getString(3);
								String Siembra= rs2.getString(2);
								db.insertDAzucarera(Codigo,Material,Siembra);
							}
							toast = Toast.makeText(getApplicationContext(), "Guardado en el Dispositivo", Toast.LENGTH_SHORT);
			            	toast.show();

							 db.close();
						}
	            	}
					catch (Exception e) {
						toast = Toast.makeText(getApplicationContext(), "Verificar Conexi�n.....", Toast.LENGTH_SHORT);
		            	toast.show();
						e.printStackTrace();
					
				}
				
					
				}
				//filtros
				if (Fil.isChecked()== true){
					try{
						rs = statement.executeQuery("Select id,fe.fechasuccion,s.material,s.nosiembra from sissemf.mov_printfiltrosd f inner join sissemf.mov_printfiltrose fe on f.correlativo = fe.correlativo inner join sissemf.mov_defsiembrae s on f.siembra = s.nosiembra where f.estado = 'BE' and fechasuccion > '2018-04-01'");
						if(rs.next()== false){	
						}
						else{
							while (rs.next()) {
								String FechaSuccion= rs.getString(2);
								String Id= rs.getString(1);
								String Material= rs.getString(3);
								String NoSiembra= rs.getString(4);
								db.insertFiltros(FechaSuccion,Id,Material,NoSiembra);
							}

		 			    	toast = Toast.makeText(getApplicationContext(), "Guardado en el Dispositivo", Toast.LENGTH_SHORT);
			            	toast.show();

							 db.close();
						}
						
	            	}
					catch (Exception e) {
						toast = Toast.makeText(getApplicationContext(), "Verificar Conexi�n.....", Toast.LENGTH_SHORT);
		            	toast.show();
						e.printStackTrace();
					
				}
				
					
				}
				//QC_Polen
				if (QCPol.isChecked()== true){
					try{
						rs = statement.executeQuery("Select obscod Codigo,obsdes Descripcion,calificativo from sispolf.observaciones where estado = 'AC' order by calificativo");
						if(rs.next()== false){	
						}
						else{
							while (rs.next()) {
								String Calificativo= rs.getString(3);
								String Codigo= rs.getString(1);
								String Descripcion= rs.getString(2);
								
								db.insertQC(Calificativo,Codigo,Descripcion);
							}

		 			    	toast = Toast.makeText(getApplicationContext(), "Guardado en el Dispositivo", Toast.LENGTH_SHORT);
			            	toast.show();

							 db.close();
						}
						
	            	}
					catch (Exception e) {
						toast = Toast.makeText(getApplicationContext(), "Verificar Conexi�n.....", Toast.LENGTH_SHORT);
		            	toast.show();
						e.printStackTrace();
					
				}
				
					
				}
				//empleados
				if (Emp.isChecked()== true){
					try{
						rs = statement.executeQuery("select e.empresa,e.empresa||case when length(trim(e.codigoempleado))= 3 then '000' || trim(codigoempleado) when length(trim(codigoempleado))= 4 then '00' || trim(codigoempleado)  when length(trim(codigoempleado))= 5 then '0' || trim(codigoempleado) else e.codigoempleado end codigoempleado,e.codigodepto,(E.PRIMERNOMBRE||'-'||E.PRIMERAPELLIDO) Nombre,e.estado,s.responsable from sisrrhhf.mempleado as e inner join sisrrhhf.mempleadoestado ee on e.empleadoestado = ee.codigoempleadoestado inner join sisrrhhf.mseccion s on e.codigoseccion= s.codigoseccion where ee.activo = 'SI' and e.fase = '6' AND E.EMPRESA <> '99' union select empresa,empresa||case when length(trim(correlativo))= 2 then '0000' || trim(correlativo)  when length(trim(correlativo))= 3 then '000' || trim(correlativo) when length(trim(correlativo))= 4 then '00' || trim(correlativo) when length(trim(correlativo))= 5 then '0' || trim(correlativo) end codigoempleado,'1' CodigoDepto,(PRIMERNOMBRE||'-'||PRIMERAPELLIDO) Nombre,estado,'0' Responsable from sisrrhhf.mempleado where fase between '1' and '4' and empleadoestado = '6' and empresa <> '99'");
						if(rs.next()== false){	
						}
						else{
							while (rs.next()) {
								String Empresa = rs.getString(1);
								String CodigoEmpleado = rs.getString(2);
								String CodigoDepto = rs.getString(3);
								String Nombre = rs.getString(4);
								String Estado = rs.getString(5);
								String Responsable = rs.getString(6);
					
								
								db.insertEmpleado(Empresa,CodigoEmpleado,CodigoDepto,Nombre,Estado,Responsable);
							}

		 			    	toast = Toast.makeText(getApplicationContext(), "Guardado en el Dispositivo", Toast.LENGTH_SHORT);
			            	toast.show();

							 db.close();
						}
						
	            	}
					catch (Exception e) {
						toast = Toast.makeText(getApplicationContext(), "Verificar Conexi�n.....", Toast.LENGTH_SHORT);
		            	toast.show();
						e.printStackTrace();
					
				}
				
					
				}
				connection.close();
				loadingDialog.dismiss();
			 }
		    catch (Exception e){
		    	Toast toast = Toast.makeText(getApplicationContext(), "Verificar Conexi�n.....", Toast.LENGTH_SHORT);
	        	toast.show();
				e.printStackTrace();
		    }
			}
			
			
		});
	
	  
		btnSemillas.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
			if(Acces.isChecked()==false){
				Acces.setChecked(true);
				Emp.setChecked(true);
				Sem.setChecked(true);
				Bol.setChecked(true);
				Fil.setChecked(true);
				QCPol.setChecked(true);
				
			}
			else{
				Acces.setChecked(false);
				Emp.setChecked(false);
				Sem.setChecked(false);
				Bol.setChecked(false);
				Fil.setChecked(false);
				QCPol.setChecked(false);
			}
				
			}
		});
		btnBack.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				onBackPressed();
				
			}
		});
		
	}
	public void alertDialogMensaje(String message1, String mesage2){
		
		try {
		    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
		    ringtone.play();	    	    
		} catch (Exception e) {
		    e.printStackTrace();
		}
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(message1);
		alertDialog.setMessage(mesage2);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
		public void onClick(final DialogInterface dialog, final int which) {
		// here you can add functions
			ringtone.stop();
		}
		});
		alertDialog.show();
	}
}