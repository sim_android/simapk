package com.example.sim;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.Vibrator;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.floricultura.R;
import com.example.sim.data.DBAdapter;

public class consulta_genesis extends ActionBarActivity {
	
	//Variables globales
	private MyApp appState;	
	private DBAdapter db;
	int CodigoModulo;
	int Codigodepartamento;
	int CodigoEmpleado;
	int CodigoOpcion;
	long Pos;
	String Inv;
	String Val;
	String NombreOpcion;
	GridView gridview;
	GridView gridEnc;
	Vibrator mVibrator;
	Ringtone ringtone;
	int dia1;
	int mes1;
	int amo1;
	String Fechade;
	String Fechade1;
	String Wk;
	String DiaCorte;
	private String val;
	
	@SuppressLint("NewApi") @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.consulta_genesis);
		
		// para manejar la excepcion de permisos en el API de Android
    	StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

		//se obtienen lo datos de la actividad antetior por medio del intent
		Bundle extras = getIntent().getExtras();
		CodigoModulo = extras.getInt("CodigoModulo");
	    CodigoEmpleado = extras.getInt("CodigoEmpleado");
	    Codigodepartamento = extras.getInt("Codigodepartamento");
	    CodigoOpcion = extras.getInt("CodigoOpcion");
	    NombreOpcion = extras.getString("NombreOpcion");
	    
	    //hacemos referencia a todos los objetos de nuestra vista
	    TextView TNombreCaptura = (TextView)findViewById(R.id.OpcionCaptura);
	    TextView TCodigoEmpleado= (TextView)findViewById(R.id.CodigoEmpleado);
	    TextView TCodigoOpcion= (TextView)findViewById(R.id.codigoOpcion);
	    //Button btnInicio = (Button) findViewById(R.id.btnInicio);
        Button btnBack = (Button) findViewById(R.id.btnBack);
        Button btnOK = (Button) findViewById(R.id.btnAsistencia);
        Button btnfechade = (Button) findViewById(R.id.buttonfechade);
        final EditText editFechade = (EditText) findViewById(R.id.editFechaDe);
        final EditText Week = (EditText) findViewById(R.id.editWeek);
        editFechade.setEnabled(false);
        final Spinner sp = (Spinner) findViewById(R.id.spdia);
        gridview = (GridView) findViewById(R.id.Grid);
        gridEnc = (GridView) findViewById(R.id.GridEnc);
        
        final ArrayList<String> datos = new ArrayList<String>();
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, datos);
        final ArrayList<String> encabezado = new ArrayList<String>();
        final ArrayAdapter<String> adapEnc = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, encabezado);
        final String[] dias = {"DOM","LUN","MAR","MIE","JUE","VIE","SAB"};
        final ArrayAdapter<String> adap = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item, dias);
        sp.setAdapter(adap);
	    //hacemos las inicializaciones necesarias a nuestra vista
	    TNombreCaptura.setText(NombreOpcion+ "");
	    TCodigoEmpleado.setText(CodigoEmpleado + "");
	    TCodigoOpcion.setText(CodigoOpcion + "");
	    //Comentario.requestFocus();

	    //incializamos la BD asi como tambien obtenemos la referencia al singleton
	    appState = ((MyApp)getApplicationContext());   
	    db = appState.getDb();
	    db.open();
	    
	    btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
 
            	onBackPressed();
            }
        });
	    
	    btnfechade.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Calendar mcurrentDate=Calendar.getInstance();
                int mYear = mcurrentDate.get(Calendar.YEAR);
                int mMonth = mcurrentDate.get(Calendar.MONTH);
                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker=new DatePickerDialog(consulta_genesis.this, new OnDateSetListener() {                  
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                    	int datodia = selectedday;
                    	String dia = format(datodia);
                    	int datomes = selectedmonth + 1;
                    	String mes = format(datomes);
                    	String datoanio = Integer.toString(selectedyear);
                    	amo1 = selectedyear;
                    	String dato = datoanio + "-" + mes + "-" +dia;
                    	editFechade.setText(dato);
                    }
                }
                ,mYear, mMonth, mDay);
                mDatePicker.setTitle("Seleccionar Fecha");                
                mDatePicker.show();  
            }         
        });
	    
	    Week.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	final NumberPicker myNumberPicker = new NumberPicker(consulta_genesis.this);
            	myNumberPicker.setMaxValue(53);
            	myNumberPicker.setMinValue(01);

            	NumberPicker.OnValueChangeListener myValChangedListener = new NumberPicker.OnValueChangeListener() {
            	  @Override
            	  public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
            		  int dat = myNumberPicker.getValue();
            		  String Sem = format(dat);
            		  Week.setText(Sem);
            	  }
            	};

            	myNumberPicker.setOnValueChangedListener(myValChangedListener);

            	new AlertDialog.Builder(consulta_genesis.this).setView(myNumberPicker).setPositiveButton(android.R.string.ok, new OnClickListener(){
            		@Override
            		public void onClick(DialogInterface dialog, int which) {
            		myNumberPicker.clearFocus();}
            		}).show(); 	
            }     
        });
	    
	    btnOK.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	datos.clear();
            	adapter.clear();
            	encabezado.clear();
            	adapEnc.clear();
            	DiaCorte = sp.getSelectedItem().toString().trim();
         
            	Fechade = editFechade.getText().toString().trim();
            	Fechade1 = Fechade.replace("-", "");
            	Wk = Week.getText().toString().trim();
            	String dia = sp.getSelectedItem().toString().trim();
            	String Pb = "";
           
            	
            	if(Wk.equals(Pb) || Fechade.equals(Pb)){
            		alertDialogMensaje("ERROR","PORFAVOR INGRESE LA SEMANA Y FECHA");
            		Week.setText("");
            		Week.requestFocus();
            	}
            	else 
            	{
            	try {
            	String user = "sim";
            	String pasword = "sim";
            	 Connection connection = null;
            	 ResultSet rs = null;
            	 Statement statement = null;
            	 
					Class.forName("com.ibm.as400.access.AS400JDBCDriver");
					connection = DriverManager.getConnection("jdbc:as400://190.190.190.10", user, pasword);	
					statement = connection.createStatement();			     
					rs = statement.executeQuery("Select w.codigo_app Invernadero,decimal((sum(w.corte) / 24.00),10,2) Personas,ifnull(sum(w.corte),0) Corte " +
							"from (Select v.bardoc,i.codigo_app,count(v.barcns) Corte from sigvegfgu.vege43 v Left join sisappf.app_invernaderos i on v.barinv = i.codigo_produccion " +
							"where barano = '"+ amo1 +"' and barsem = '"+ Wk +"' and barot3 = '2.00' and barsde like '%"+DiaCorte+"%'  and i.correlativosupervisor = '"+ CodigoEmpleado +"' " +
							"group by v.bardoc,i.codigo_app) w group by w.codigo_app order by w.codigo_app");
	
				    //Agregar encabezados de columnas	
					String Cn1 = rs.getMetaData().getColumnName(1);
					encabezado.add(Cn1);
					gridEnc.setAdapter(adapEnc);
					String Cn2 = rs.getMetaData().getColumnName(2);
					encabezado.add(Cn2);
					gridEnc.setAdapter(adapEnc);
					String Cn3 = rs.getMetaData().getColumnName(3);
					encabezado.add(Cn3);
					gridEnc.setAdapter(adapEnc);
					// finaliza agregar encabezados de columnas	
					while (rs.next()) {
					//agrega filas de columnas
						 String d1 = rs.getString(1).trim();
						 String d2 = rs.getString(2).trim();
						 String d3 = rs.getString(3).trim();
						 String c1 = comprobar(d1);
						 String c2 = comprobar(d2);
						 String c3 = comprobar(d3);
						 datos.add(c1);
						 gridview.setAdapter(adapter);
						 datos.add(c2);
						 gridview.setAdapter(adapter);
						 datos.add(c3);
						 gridview.setAdapter(adapter);
					}
					connection.close();	
            	}
				catch (Exception e) {
					Toast toast = Toast.makeText(getApplicationContext(), "Verificar Conexi�n.....", Toast.LENGTH_SHORT);
	            	toast.show();
					e.printStackTrace();
            } 
           } 	
            }
        });
	    
	    gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id){
            			Pos =  parent.getItemIdAtPosition(position);
            			int ifila = (int)(Pos/3);
            			int icelda = (ifila*3); 
            			Inv = (String) parent.getItemAtPosition(icelda);
	    	        	Intent intent = new Intent(getApplicationContext(), DetalleGenesis.class);
	    	        	intent.putExtra("Inv", Inv);
	    	        	intent.putExtra("amo1", amo1);
	    	        	intent.putExtra("Wk", Wk);
	    	        	intent.putExtra("DiaCorte", DiaCorte);
	    	        	intent.putExtra("CodigoEmpleado", CodigoEmpleado);
	    	        	startActivity(intent);	    	    
	    	            }
	    	    });
	    	      
	}
	
	public void alertDialogMensaje(String message1, String mesage2){
	
		try {
		    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
		    ringtone.play();	    	    
		} catch (Exception e) {
		    e.printStackTrace();
		}
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(message1);
		alertDialog.setMessage(mesage2);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int which) {
		// here you can add functions
			ringtone.stop();
		}
		});
		alertDialog.show();
	}
	
	public String comprobar(String cad){
		String Pb = "";
		if(cad.equals(Pb)){
			cad = "---";
		}
		return cad;	
	}
	
	public String format (int f){
		val = "";
		if(f<10){
		val = "0"+Integer.toString(f);	
		}
		else{
		val = Integer.toString(f);
		}
		return val;
	}
		
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.soporteit, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}  
