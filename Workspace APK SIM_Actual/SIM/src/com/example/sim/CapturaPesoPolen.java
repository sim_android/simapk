package com.example.sim;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Vibrator;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.floricultura.R;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.Dbolsas;
import com.example.sim.data.Dguardados;
import com.example.sim.data.Filtros;
import com.example.sim.data.JsonFiles;
import com.example.sim.data.Semillas;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CapturaPesoPolen extends ActionBarActivity {
	
	//Variables globales
	private MyApp appState;	
	private DBAdapter db;
	int CodigoModulo;
	int Codigodepartamento;
	int CodigoEmpleado;
	int CodigoOpcion;
	int OpcionPolinizacion;
	int OpcionSuccion;
	int ValorPolinizacion = 0;
	int ValorSuccion=0;
	int TotalEmpleadosAsignados;
	int OpcionCosecha;
	String SiembraF;
	boolean validoDescriptivo;
	boolean confirma = true;
	boolean confirma2 = true;
	String NombreOpcion;
	String Val_Bolsa;
	
	LinearLayout validacionesGroup;
	TextView Material;
	TextView Material1;
	TextView TCantidadAsignados;
	EditText azucar;
	EditText bote;
	EditText brocha;
	EditText descriptivo;
	EditText empleado;
	TextView TextviewAzucarera;
	TextView TextviewBrocha;
	TextView TextviewBote;
	Vibrator mVibrator;
	Ringtone ringtone;
	CheckBox checkbox1;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_captura_pesadopolen);
		
		
		//se obtienen lo datos de la actividad antetior por medio del intent
		Bundle extras = getIntent().getExtras();
		CodigoModulo = extras.getInt("CodigoModulo");
	    CodigoEmpleado = extras.getInt("CodigoEmpleado");
	    Codigodepartamento = extras.getInt("Codigodepartamento");
	    CodigoOpcion = extras.getInt("CodigoOpcion");
	    NombreOpcion = extras.getString("NombreOpcion");
	    OpcionPolinizacion = extras.getInt("OpcionPolinizacion");
	    OpcionCosecha= extras.getInt("OpcionCosecha");
	    
	    OpcionSuccion = extras.getInt("OpcionSuccion");
	    
	    //hacemos referencia a todos los objetos de nuestra vista
	    TextView TNombreCaptura = (TextView)findViewById(R.id.OpcionCaptura);
	    TextView TCodigoEmpleado= (TextView)findViewById(R.id.CodigoEmpleado);
	    TextView TCodigoOpcion= (TextView)findViewById(R.id.codigoOpcion);
	    TextviewAzucarera= (TextView)findViewById(R.id.textViewFlores);
	    TextviewBrocha= (TextView)findViewById(R.id.textViewBrocha);
	    TextviewBote= (TextView)findViewById(R.id.textViewBote);
	    
	    
	    
	    TCantidadAsignados = (TextView)findViewById(R.id.textEmpleadosAsginados);
	    validacionesGroup = (LinearLayout)findViewById(R.id.layoutValidaciones);
	    descriptivo = (EditText)findViewById(R.id.editTextDescriptivo);
	    Material = (TextView)findViewById(R.id.textMaterial);
	    Material1 = (TextView)findViewById(R.id.textMaterial1);
	    azucar  = (EditText)findViewById(R.id.editAzucar);
	    bote  = (EditText)findViewById(R.id.editBote);
	    brocha  = (EditText)findViewById(R.id.editBrocha);
	    empleado  = (EditText)findViewById(R.id.editEmpleado);
	    checkbox1 = (CheckBox)findViewById(R.id.checkBox10);
	    Button buttonNuevo = (Button) findViewById(R.id.btnNuevo);
        Button btnBack = (Button) findViewById(R.id.btnBack);
        Button btnInicio = (Button) findViewById(R.id.btnInicio);
	    
	    
	    //hacemos las inicializaciones necesarias a nuestra vista
	    TNombreCaptura.setText(NombreOpcion+ "");
	    TCodigoEmpleado.setText(CodigoEmpleado + "");
	    TCodigoOpcion.setText(CodigoOpcion + "");
	    validoDescriptivo = false;
	    validacionesGroup.setVisibility(View.INVISIBLE);
	    descriptivo.requestFocus();
	    
	    // Agrega CheckBox a La vista si opcion es seleccionada y agrega el texto que corresponde
	    checkbox1.setVisibility(View.VISIBLE);
	  
	   // Se limitan los campos para el tipo de datos con decimal
	    brocha.setInputType(InputType.TYPE_CLASS_NUMBER |InputType.TYPE_NUMBER_FLAG_DECIMAL);
	    brocha.setSingleLine(false);
	    bote.setInputType(InputType.TYPE_CLASS_NUMBER |InputType.TYPE_NUMBER_FLAG_DECIMAL);
	    bote.setSingleLine(false);
	    
	    //incializamos la BD asi como tambien obtenemos la referencia al singleton
	    appState = ((MyApp)getApplicationContext());   
	    db = appState.getDb();
	    db.open();
	    

	    verificarAsginados();
	   
	    
	    //al recibir el enter se verifica si el descriptivo es correcto y lo manda al siguiente input sino le muestra un mensaje al usuario
        descriptivo.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                		VerificarDatos(descriptivo.getText().toString().trim());
	                	if (validoDescriptivo == true){
	                			azucar.requestFocus();
	                		descriptivo.setEnabled(false);
	                	}else{
		    	    		alertDialogMensaje("Azucarera", "Valor de Azucarera Invalido");
		    	    		descriptivo.setText("");
		    	    		descriptivo.requestFocus();
	                	}
	                	return true;  
                }     
                return false;
            }  
        });
        
        
	    //Se verfica el input azucarera al persionar enter
        azucar.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	// se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                	String des = descriptivo.getText().toString().trim();
                	String az = VerificarFiltro(azucar.getText().toString().trim());
                	if (az.equals(des)){
            		if( checkbox1.isChecked() == true){
            			brocha.requestFocus();
            			azucar.setEnabled(false);
            		}else{
            			bote.requestFocus();
            			azucar.setEnabled(false); 
            		}
            		}else{
            				alertDialogMensaje("Filtro", "Valor de Filtro es invalido");
            				azucar.setVisibility(View.VISIBLE);
            				azucar.setEnabled(true);
                			azucar.setText("");
                			azucar.requestFocus();
            			}  			
            			 return true;
            		}		
                return false;
            }
        });
        
        
	    //Se verfica el input azucarera al persionar enter
        bote.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	// se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                	if(bote.getText().toString().trim() == ""){
                		alertDialogMensaje("Peso", "Valor de Peso es invalido");
            			bote.setText("");
            			bote.requestFocus();
            			}else {
            				empleado.requestFocus();
            				//bote.setEnabled(false);
            		}
	                return true;
                }
                return false;
            }
        });
        
        
	    //Se verfica el input azucarera al persionar enter
        brocha.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	// se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                	if(brocha.getText().toString().trim() == ""){
                		alertDialogMensaje("Volumen", "Valor de Volumen es invalido");
            			brocha.setText("");
            			brocha.requestFocus();
            			}else {
            				empleado.requestFocus();
            				//brocha.setEnabled(false);
            		}
	                return true;
                }
                return false;
            }
        });
        
        
        
	    //Se verfica el input empleado al persionar enter
        empleado.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	// se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                	String pb = "";
                	String v1 = brocha.getText().toString().trim();
                	String v2 = bote.getText().toString().trim();
                	
                if(checkbox1.isChecked() == true && v1.equals(pb) ){
                	alertDialogMensaje("ERROR","Debe Ingresar el Peso");
                	empleado.setText("");
                	brocha.requestFocus();
                }else if (checkbox1.isChecked() == false && v2.equals(pb)){
                	alertDialogMensaje("ERROR","Debe Ingresar el Peso");
                	empleado.setText("");
                	bote.requestFocus();
                }else{ 
                if(empleado.getText().toString().trim() == ""){
                	alertDialogMensaje("Empleado", "Ingrese el empleado");
        			brocha.setText("");
        			brocha.requestFocus();
                } else {                
                	GuardarDatosCP();
                } 
                }
                return true; 
            }
            
            return false;
        } 
    });	 
               
        
	    //se limpian los campos y variables para una nueva captura
	    buttonNuevo.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	        	validacionesGroup.setVisibility(View.INVISIBLE);
				 Material.setText(" ");
				 descriptivo.setText("");
				 azucar.setText("");  
				 bote.setText("");  
				 brocha.setText("");  
				 empleado.setText(""); 
				 validoDescriptivo = false;
				 ValorPolinizacion = 0; 
				 descriptivo.setEnabled(true);
				 azucar.setEnabled(true);
				 bote.setEnabled(true);
				 brocha.setEnabled(true);
				 descriptivo.requestFocus();
				 
				 
	        }
	    });
        
        btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
 
            	onBackPressed();
            }
        });
        
        
        /*
        btnInicio.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Intent intent = new Intent(getApplicationContext(), MainActivity.class);
           	 	startActivity(intent);
            }
        });
        
        */
	}
	
	
	public void verificarAsginados(){
		Cursor CursorGuardados =	db.getDguardados("Actividad=" + CodigoOpcion + " AND ActividadFinalizada=0" );
		TotalEmpleadosAsignados = CursorGuardados.getCount();
		TCantidadAsignados.setText(TotalEmpleadosAsignados + "");
		CursorGuardados.close();
	}
	

	//retornara un valor dependiendo del estado del empleado
	// 0 = no existe
	// 1 = existe pero esta asignado
	// 2 = existe y no esta asignado
	public int VerificarEmpleado(String empleado){
		int valor = 0;
		Cursor CursorDacceso =	db.getDacceso("CodigoEmpleado='" + empleado + "'");
		if(CursorDacceso != null  && CursorDacceso.getCount()>0){
			Cursor CursorGuardados =	db.getDguardados("CodigoEmpleado=" + empleado + " AND ActividadFinalizada=0");
			if(CursorGuardados.getCount()>0){
				valor = 1;
			}else{
				valor = 2;
			}
			CursorGuardados.close();
		}
		CursorDacceso.close();
		return valor;
	}
	
	public void alertDialogMensaje(String message1, String mesage2){
		
		//mVibrator.vibrate(300);
		
		try {
		    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
		    ringtone.play();
		    
		    
		} catch (Exception e) {
		    e.printStackTrace();
		}
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(message1);
		alertDialog.setMessage(mesage2);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int which) {
		// here you can add functions
			ringtone.stop();
		}
		});
		alertDialog.show();
	}


	public void VerificarDatos(String dato){	
		 Cursor CursorSemillas = db.getSemillas("Siembra='" + dato + "'");
		 if(CursorSemillas.getCount()>0){
			 Semillas s = db.getSemillasFromCursor(CursorSemillas, 0);
			 Log.d("SEMILLA",s.getSiembra() + ", "+s.getMaterial() + " ," + s.getPolinizacion());
			 
			 if(CodigoOpcion == 20222116){
				 if(checkbox1.isChecked() == true){
				 validacionesGroup.setVisibility(View.VISIBLE);				 
				 azucar.setVisibility(View.VISIBLE);
				 bote.setVisibility(View.INVISIBLE);
				 brocha.setVisibility(View.VISIBLE);
				 TextviewAzucarera.setText("Filtro");
				 TextviewBrocha.setText("Volumen");
				 TextviewBote.setText("");
				 ValorPolinizacion=0;
				 } else {
					 validacionesGroup.setVisibility(View.VISIBLE);				 
					 azucar.setVisibility(View.VISIBLE);
					 bote.setVisibility(View.VISIBLE);
					 brocha.setVisibility(View.INVISIBLE);
					 TextviewAzucarera.setText("Filtro");
					 TextviewBrocha.setText("");
					 TextviewBote.setText("Peso");
					 ValorPolinizacion=0;
				 }
			 }
			 validoDescriptivo = true;
			 Material.setText(s.getMaterial());
			 
		 }else{
			// validacionesGroup.setVisibility(View.INVISIBLE);
			 Material.setText(" ");
			 ValorPolinizacion=0;
			 validoDescriptivo = false;
		 }
		 CursorSemillas.close();
	}
	
	public void GuardarDatosCP(){
		String empleadoEscaneado = empleado.getText().toString().trim();
    	//int existe = VerificarEmpleado( empleadoEscaneado );
        int existe = 2;
        
    	if(existe == 2){
        		Dguardados dtemp = new Dguardados();
        		dtemp.setCodigoEmpleado(Integer.parseInt(empleadoEscaneado));
        		dtemp.setCodigoDepto(Codigodepartamento);
        		dtemp.setSiembra(descriptivo.getText().toString().trim());
        		
        		//-----------------------------------------------------------------------------------
        		String correlativo="";
        		    	//almaceno el empleado en una variable string
        		    	String getEmpleado = empleado.getText().toString().trim();
        		    	// guardo la variable que tiene empleado en un array
        		    	String[]empl = new String[]{getEmpleado};
        		    	Cursor obtenerCorrelativo = db.getCorrelativo(empl);
        		    	//recorro dicho arreglo para obtener el campo
        		    	if (obtenerCorrelativo.moveToFirst()){
        		    		do{
        		    			correlativo = obtenerCorrelativo.getString(0);
        		    		}while(obtenerCorrelativo.moveToNext());
        		    	}
        		//-------------------------------------------------------------------------
        		
        		Calendar c = Calendar.getInstance(); 
        		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
        		String fecha = fdate.format(c.getTime());
        		SimpleDateFormat ftime = new SimpleDateFormat("HH:mm:ss");
        		String horaInicio = ftime.format(c.getTime());
        		//CorrelativoBolsa = azucar.getText().toString().trim();
        		
        		
        		//String fecha = c.get(Calendar.DAY_OF_MONTH) + "-" + (c.get(Calendar.MONTH)+1) + "-"+c.get(Calendar.YEAR);
        		dtemp.setFecha(fecha);
        		//String horaInicio = c.get(Calendar.HOUR) + ":" +c.get(Calendar.MINUTE) + ":"+c.get(Calendar.SECOND);
        		dtemp.setHoraIncio(horaInicio);
           		dtemp.setActividad(CodigoOpcion);
        		dtemp.setSupervisor(CodigoEmpleado);
        		dtemp.setAzucarera(descriptivo.getText().toString().trim());
        		dtemp.setFiltro(azucar.getText().toString().trim());
        		dtemp.setActividadFinalizada(0);
        		dtemp.setCorrelativoEmpleado(correlativo);
        		
        		//Guarda Pesos
        		dtemp.setPesoPolen(bote.getText().toString().trim());
        		dtemp.setVolumenPolen(brocha.getText().toString().trim());
        		//Fin graba bolsas adicionales
        		
        		
        	
        		Log.d("Dguardados", dtemp.toString().trim());
        		
        		//db.insertarDguardados(dtemp);
        		
        		//generacion archivo json
        		ObjectMapper mapper = new ObjectMapper();
        		try {
        			String nameJson ="PP_" + dtemp.getCodigoEmpleado() + "_"+dtemp.getFecha() + "_"+dtemp.getHoraIncio();
        			nameJson=nameJson.replace(":", "-");	
					mapper.writeValue(new File( Environment.getExternalStorageDirectory()+"/SIM/JsonDescarga/" + nameJson+ ".json"), dtemp);
					
					JsonFiles archivo = new JsonFiles();
					archivo.setName(nameJson+ ".json");
					archivo.setNameFolder("SinFinalizar/");
					archivo.setUpload(0);
					db.insertarJsonFile(archivo);
					
					if(!appState.getSubiendoArchivos()){
						appState.setSubiendoArchivos(true);
						new UptoDropbox().execute(getApplicationContext());
					}
					
        		
        		} catch (JsonGenerationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JsonMappingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        		
        		
        		empleado.setText("");  
        		azucar.setText("");
        		bote.setText("");
        		brocha.setText("");
        		azucar.setEnabled(true);		
        		bote.setEnabled(true);
        		brocha.setEnabled(true);               		
        		VerificarDatos(descriptivo.getText().toString().trim());		
            	azucar.requestFocus();    // envio el focus a azucarera  para que escaneen el nuevo Filtro
            		
            	
        		
        		TotalEmpleadosAsignados++;
        		TCantidadAsignados.setText(TotalEmpleadosAsignados + "");
        		
        		
        	}else if(existe == 1){
        		alertDialogMensaje("Asignado", "Este usuario ya ha sido asignado");
        		empleado.setText("");
        		empleado.requestFocus();
        		//VerificarDatos(descriptivo.getText().toString().trim());
        	}else if(existe == 0){
        		alertDialogMensaje("Marcaje", "No existe marcaje para este usuario");
        		empleado.setText("");
        		empleado.requestFocus();
        		 //VerificarDatos(descriptivo.getText().toString().trim());	
    	}
	}
	
	
	public void VerificarDatosbolsa(String dato){	
		
		 Cursor CursorSemillas = db.getDbolsas("Codigo='" + dato + "'");
		 if(CursorSemillas.getCount()>0){
			 Dbolsas s = db.getDbolsasFromCursor(CursorSemillas, 0);
			 Log.d("SEMILLA",s.getSiembra() + ", "+s.getMaterial() + " ," + s.getCodigo());
			 
			 if(OpcionCosecha==1 && checkbox1.isChecked() == true){
				 //si se entra a captura con la opcion de cosechas
				validacionesGroup.setVisibility(View.VISIBLE);		
				bote.setVisibility(View.VISIBLE);
				brocha.setVisibility(View.VISIBLE);
				TextviewAzucarera.setText("Bolsa 1");
				TextviewBote.setText("Bolsa 2");
				TextviewBrocha.setText("Bolsa 3");
			 } else if (OpcionCosecha==1 && checkbox1.isChecked() == false){
				validacionesGroup.setVisibility(View.VISIBLE);		
				bote.setVisibility(View.INVISIBLE);
				brocha.setVisibility(View.INVISIBLE);
				TextviewAzucarera.setText("Bolsa");
				TextviewBote.setText("");;
				TextviewBrocha.setText("");;
				//CODIGO AGREGADO
			 }else if(OpcionSuccion == 1){
				 validacionesGroup.setVisibility(View.VISIBLE);
				 TextviewAzucarera.setText("Filtro");
				 TextviewBote.setText("Pipeta");;
				 brocha.setVisibility(View.INVISIBLE);
				 TextviewBrocha.setText("");;
				 //FIN CODIGO AGREGADO
				 
				
			 }/*else if(s.getPolinizacion() == 1 && OpcionPolinizacion==1){
				 validacionesGroup.setVisibility(View.VISIBLE);				 
				 ValorPolinizacion=1;
			 }*/else{
				 validacionesGroup.setVisibility(View.INVISIBLE);
				 ValorPolinizacion=0;
			 }
			 validoDescriptivo = true;
			 Material1.setText(s.getSiembra());
			 //Val_Bolsa = "";
			 //Val_Bolsa = s.getSiembra();
			 
			 
		 }else{
			 validacionesGroup.setVisibility(View.VISIBLE);
			 
			 //Material.setText(" ");
			 //Material.setText("Menor 0");
			 ValorPolinizacion=0;
			 validoDescriptivo = false;
			// alertDialogMensaje("Mensaje", "EL valor es invalido");
			 
		 }
		 CursorSemillas.close();
	}
		
	public String VerificarFiltro(String dato) {
		Cursor CursorFiltros = db.getDFiltros ("Id='" + dato + "'");
		 if(CursorFiltros != null  && CursorFiltros.getCount()>0){
				 Filtros f = db.getDFiltrosFromCursor(CursorFiltros, 0);
				 Log.d("FILTROS",f.getNoSiembra() + ", "+f.getMaterial() + " ," + f.getFechaSuccion());
				 SiembraF = f.getNoSiembra();
		 }else{
			 SiembraF = ""; 
		 }
		 CursorFiltros.close();
		return SiembraF;
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.captura_pesadopolen, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void ButtonOnClick(View v) {
	    switch (v.getId()) {
	      case R.id.buttonborra:
		        agregarNumero("1");
	        break;
	      case R.id.btnDos:
	    	  agregarNumero("2");
	        break;
	      case R.id.btnTres:
		        agregarNumero("3");
	        break;
	      case R.id.btnCuatro:
	    	  agregarNumero("4");
	        break;
	      case R.id.btnCinco:
		        agregarNumero("5");
	        break;
	      case R.id.btnSeis:
	    	  agregarNumero("6");
	        break;
	      case R.id.btnSiete:
	    	  agregarNumero("7");
	        break;
	      case R.id.btnOcho:
		        agregarNumero("8");
	        break;
	      case R.id.btnNueve:
	    	  agregarNumero("9");
	        break;
	      case R.id.button0:
		        agregarNumero("0");
	        break;
	      case R.id.btnCero:
	    	  agregarNumero("b");
	        break;	        
	      case R.id.Button11:
	    	  agregarNumero(".");
	        break;
	      }
	     
	}
	
	public void agregarNumero(String n){
		
		if(n.contains("b")){
			if(bote.isFocused()){
				bote.setText("");			
			} else if (brocha.isFocused()){
				brocha.setText(""); }
			}else{
			if(bote.isFocused()){
				bote.setText(bote.getText() + n);
			} else if(brocha.isFocused()){
					brocha.setText(brocha.getText() + n);
			}
		}
	}
}