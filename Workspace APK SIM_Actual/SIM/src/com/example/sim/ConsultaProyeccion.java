package com.example.sim;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.Vibrator;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.floricultura.R;
import com.example.sim.data.DBAdapter;

public class ConsultaProyeccion extends ActionBarActivity {
	//Variables globales
	private MyApp appState;	
	private DBAdapter db;
	int CodigoModulo;
	int Codigodepartamento;
	int CodigoEmpleado;
	int CodigoOpcion;
	String Val;
	String NombreOpcion;
	Vibrator mVibrator;
	Ringtone ringtone;
	String FechaDe;
	String FechaAl;
	GridView gridview;
	GridView gridEnc;
	long Pos;
	private String val;
	
	@SuppressLint("NewApi") @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.consulta_proyeccion);
		
		// para manejar la excepcion de permisos en el API de Android
    	StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

		//se obtienen lo datos de la actividad antetior por medio del intent
		Bundle extras = getIntent().getExtras();
		CodigoModulo = extras.getInt("CodigoModulo");
	    CodigoEmpleado = extras.getInt("CodigoEmpleado");
	    Codigodepartamento = extras.getInt("Codigodepartamento");
	    CodigoOpcion = extras.getInt("CodigoOpcion");
	    NombreOpcion = extras.getString("NombreOpcion");
	    
	    //hacemos referencia a todos los objetos de nuestra vista
	    TextView TNombreCaptura = (TextView)findViewById(R.id.OpcionCaptura);
	    TextView TCodigoEmpleado= (TextView)findViewById(R.id.CodigoEmpleado);
	    TextView TCodigoOpcion= (TextView)findViewById(R.id.codigoOpcion);
        Button btnBack = (Button) findViewById(R.id.btnBack);
        Button btnOK = (Button) findViewById(R.id.btnAsistencia);
        Button btnfechade = (Button) findViewById(R.id.buttonfechade);
        Button btnfechaal = (Button) findViewById(R.id.buttonfechaal);
        final EditText editFechade = (EditText) findViewById(R.id.editFechaDe);
        final EditText editFechaal = (EditText) findViewById(R.id.editFechaAl);
        editFechade.setEnabled(false);
        editFechaal.setEnabled(false);
        gridview = (GridView) findViewById(R.id.GridDeta);
        gridEnc = (GridView) findViewById(R.id.GridEnca);
        //final TableLayout TablaEnc = (TableLayout) findViewById(R.id.TablaEnc);
        final ArrayList<String> datos = new ArrayList<String>();
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, datos);
        final ArrayList<String> encabezado = new ArrayList<String>();
        final ArrayAdapter<String> adapEnc = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, encabezado);
        
	    //hacemos las inicializaciones necesarias a nuestra vista
	    TNombreCaptura.setText(NombreOpcion+ "");
	    TCodigoEmpleado.setText(CodigoEmpleado + "");
	    TCodigoOpcion.setText(CodigoOpcion + "");
	  
	    
	    //Comentario.requestFocus();

	    //incializamos la BD asi como tambien obtenemos la referencia al singleton
	    appState = ((MyApp)getApplicationContext());   
	    db = appState.getDb();
	    db.open();

	    btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
 
            	onBackPressed();
            }
        });
	    
	    btnfechade.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Calendar mcurrentDate=Calendar.getInstance();
                int mYear = mcurrentDate.get(Calendar.YEAR);
                int mMonth = mcurrentDate.get(Calendar.MONTH);
                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker=new DatePickerDialog(ConsultaProyeccion.this, new OnDateSetListener() {                  
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                    	int datodia = selectedday;
                    	String dia = format(datodia);
                    	int datomes = selectedmonth + 1;
                    	String mes = format(datomes);
                    	String datoanio = Integer.toString(selectedyear);
                    	String dato = datoanio + "-" + mes + "-" +dia;
                    	editFechade.setText(dato);
                    }
                }
                ,mYear, mMonth, mDay);
                mDatePicker.setTitle("Seleccionar Fecha");                
                mDatePicker.show();  
            }
            
        });
	    
	    btnfechaal.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
			Calendar mcurrentDate = Calendar.getInstance();
			int mYear = mcurrentDate.get(Calendar.YEAR);
			int mMonth = mcurrentDate.get(Calendar.MONTH);
            int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog mDatePicker=new DatePickerDialog(ConsultaProyeccion.this, new OnDateSetListener() {                  
                public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                	int datodia = selectedday;
                	String dia = format(datodia);
                	int datomes = selectedmonth + 1;
                	String mes = format(datomes);
                	String datoanio = Integer.toString(selectedyear);
                	String dato = datoanio + "-" + mes + "-" +dia;
                	editFechaal.setText(dato);
                }
            }
            ,mYear, mMonth, mDay);
            mDatePicker.setTitle("Seleccionar Fecha");                
            mDatePicker.show();  

			}
		});
	    	    
	    btnOK.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	String Pb = "";
            	String es = " | ";
            	DecimalFormat formatea = new DecimalFormat("###,###.##");
            	FechaDe = editFechade.getText().toString().trim();
            	FechaAl = editFechaal.getText().toString().trim();
            	datos.clear();
            	adapter.clear();
            	encabezado.clear();
            	adapEnc.clear();
                          	
            	if(FechaDe.equals(Pb) || FechaAl.equals(Pb)){
            		alertDialogMensaje("ERROR","PORFAVOR SELECCIONE LAS FECHAS");
            		editFechade.setText("");
            		editFechade.requestFocus();
            	}else{
            	try {
            	String user = "sim";
            	String pasword = "sim";
            	 Connection connection = null;
            	 ResultSet rs = null;
            	 Statement statement = null;
            	 
					Class.forName("com.ibm.as400.access.AS400JDBCDriver");
					connection = DriverManager.getConnection("jdbc:as400://190.190.190.10", user, pasword);	
					statement = connection.createStatement();			     
					rs = statement.executeQuery("Select w.fecha Fecha, w.definv inv, w.actividad Activ,ifnull(sum(w.disponibilidad),0) Dispon, ifnull(sum(w.minutos),0) Min, ifnull(decimal((decimal(W.RENDIMIENTOGENERAL,10,2) * 480.00),10,2),0) Tarea, ifnull(decimal((decimal(sum(w.horas),10,2) / 8.00),10,2),0) Personas" +
							" from(Select d.fecha, s.definv, so.descripcion Actividad, s.defban, s.defvar, d.siembra, s.defmat, decimal((sum(decimal(d.conteo,10,2)) / count(decimal(d.conteo,10,2))),10,2) Promedio, " +
							" decimal((decimal((sum(decimal(d.conteo,10,2)) / count(decimal(d.conteo,10,2))),10,2) * decimal(s.defmat,10,2)),10,1) Disponibilidad, so.rendimientogeneral, " +
							" decimal(((decimal((sum(decimal(d.conteo,10,2)) / count(decimal(d.conteo,10,2))),10,2) * decimal(s.defmat,10,2)) / decimal(so.rendimientogeneral,10,2)),10,2) Minutos, " +
							" decimal(decimal(((decimal((sum(decimal(d.conteo,10,2)) / count(decimal(d.conteo,10,2))),10,2) * decimal(s.defmat,10,2)) / decimal(so.rendimientogeneral,10,2)),10,2) / 60.00,10,2) Horas " +
							" from sisappf.app_disponibilidadveg d left join sisvegf.mov_defsiembraveg s on d.siembra = left(s.defsie,4)||s.correlativo left join siscfgf.seguridad_opcion_moduloasp so on d.actividad = so.opcion " +
							" where d.fecha between '"+FechaDe+"' and '"+FechaAl+"' group by so.rendimientogeneral,so.descripcion,d.siembra,d.fecha,d.actividad,s.defvar,s.defban,s.definv,s.defmat " +
							" order by d.fecha,s.definv,s.defban) w group by w.fecha,w.definv,w.actividad, w.RendimientoGeneral order by w.definv,w.actividad");
					//Agregar encabezados de columnas	
					String Cn1 = rs.getMetaData().getColumnName(1);
					encabezado.add(Cn1);
					gridEnc.setAdapter(adapEnc);
					String Cn2 = rs.getMetaData().getColumnName(2);
					encabezado.add(Cn2);
					gridEnc.setAdapter(adapEnc);
					String Cn3 = rs.getMetaData().getColumnName(3);
					encabezado.add(Cn3);
					gridEnc.setAdapter(adapEnc);
					String Cn4 = rs.getMetaData().getColumnName(4);
					encabezado.add(Cn4);
					gridEnc.setAdapter(adapEnc);
					String Cn5 = rs.getMetaData().getColumnName(6);
					encabezado.add(Cn5);
					gridEnc.setAdapter(adapEnc);
					String Cn6 = rs.getMetaData().getColumnName(7);
					encabezado.add(Cn6);
					gridEnc.setAdapter(adapEnc);
					// finaliza agregar encabezados de columnas	
					while (rs.next()) {
					//agrega filas de columnas
						 String d1 = rs.getString(1).trim();
						 String d2 = rs.getString(2).trim();
						 String d3 = rs.getString(3).trim();
						 String d4 = rs.getString(4).trim();
					     String d5 = rs.getString(6).trim();
						 String d6 = rs.getString(7).trim();
						 String c1 = comprobar(d1);
						 String pv = comprobar(d2);
						 String c2 = pv.replace("-","");
						 String c3 = comprobar(d3);
						 String c4 = comprobar(d4);
						 String c5 = comprobar(d5);
						 String r4 = formatea.format(Double.parseDouble(c4));
						 String r5 = formatea.format(Double.parseDouble(c5));
						 String c6 = comprobar(d6);		
						 datos.add(c1);
						 gridview.setAdapter(adapter);
						 datos.add(c2);
						 gridview.setAdapter(adapter);
						 datos.add(c3);
						 gridview.setAdapter(adapter);
						 datos.add(r4);
						 gridview.setAdapter(adapter);
						 datos.add(r5);
						 gridview.setAdapter(adapter);
						 datos.add(c6);
						 gridview.setAdapter(adapter);						
					}
					connection.close();	
            	}
				catch (Exception e) {
					Toast toast = Toast.makeText(getApplicationContext(), "Verificar Conexi�n.....", Toast.LENGTH_SHORT);
	            	toast.show();
					e.printStackTrace();
            } 
           }	
            }
        });
	    
	    gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id){
            			Pos =  parent.getItemIdAtPosition(position);
            			int ifila = (int)(Pos/6);
            			int iceldaInv = (ifila*6)+1; 
            			int iceldaAct = iceldaInv + 1;
            			int iceldaper = iceldaInv + 4;
            			String Invpv = (String) parent.getItemAtPosition(iceldaInv);
            			String Inv =  formatear(Invpv);
            			String Fecha1 = editFechade.getText().toString().trim();
            			String Fecha2 = editFechaal.getText().toString().trim();
            			String Act = (String) parent.getItemAtPosition(iceldaAct);
            			String Per = (String) parent.getItemAtPosition(iceldaper);
	    	        	Intent intent = new Intent(getApplicationContext(), DetalleProyeccion.class);
	    	        	intent.putExtra("Inv", Inv);
	    	        	intent.putExtra("Fecha1", Fecha1);
	    	        	intent.putExtra("Fecha2", Fecha2);
	    	        	intent.putExtra("Act", Act);
	    	        	intent.putExtra("Per", Per);
	    	        	startActivity(intent);	    	    
	    	            }
	    	    });
	    
	}	    

	public void alertDialogMensaje(String message1, String mesage2){
	
		try {
		    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
		    ringtone.play();	    	    
		} catch (Exception e) {
		    e.printStackTrace();
		}
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(message1);
		alertDialog.setMessage(mesage2);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int which) {
		// here you can add functions
			ringtone.stop();
		}
		});
		alertDialog.show();
	}
	
	public String comprobar(String cad){
		String Pb = "";
		if(cad.equals(Pb)){
			cad = "---";
		}
		return cad;	
	}
	
	public String formatear(String ft){
		String f1 = null;
		String f = null;
		String Except = "INV381";
		if(ft.equals(Except)) {
			f = "INV38-1";
		}else{ 
		f1 = ft.replace("INV", "INV-");
		int size = 7;
		int f3 = f1.length();
		if(f3 < size){	
		 while(f3 < size){
			 f = " "+ f1;
			 f3++;
		} 
		}else {
			f = f1;
		}
		}
		return f;	
	}
	
	public String format (int f){
		val = "";
		if(f<10){
		val = "0"+Integer.toString(f);	
		}
		else{
		val = Integer.toString(f);
		}
		return val;
	}
		
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.soporteit, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}  	




/*  *************************** CON TABLA ***************************************
try {
String user = "sim";
String pasword = "sim";
 Connection connection = null;
 ResultSet rs = null;
 Statement statement = null;
 
	Class.forName("com.ibm.as400.access.AS400JDBCDriver");
	connection = DriverManager.getConnection("jdbc:as400://190.190.190.10", user, pasword);	
	statement = connection.createStatement();			     
	rs = statement.executeQuery("Select w.fecha Fecha_Disp, w.definv inv, w.actividad,ifnull(sum(w.disponibilidad),0) Disp, ifnull(sum(w.minutos),0) Min, ifnull(sum(w.horas),0) Horas, ifnull(decimal((decimal(sum(w.horas),10,2) / 8.00),10,2),0) Personas" +
			" from(Select d.fecha, s.definv, so.descripcion Actividad, s.defban, s.defvar, d.siembra, s.defmat, decimal((sum(decimal(d.conteo,10,2)) / count(decimal(d.conteo,10,2))),10,2) Promedio, " +
			" decimal((decimal((sum(decimal(d.conteo,10,2)) / count(decimal(d.conteo,10,2))),10,2) * decimal(s.defmat,10,2)),10,2) Disponibilidad, so.rendimientogeneral, " +
			" decimal(((decimal((sum(decimal(d.conteo,10,2)) / count(decimal(d.conteo,10,2))),10,2) * decimal(s.defmat,10,2)) / decimal(so.rendimientogeneral,10,2)),10,2) Minutos, " +
			" decimal(decimal(((decimal((sum(decimal(d.conteo,10,2)) / count(decimal(d.conteo,10,2))),10,2) * decimal(s.defmat,10,2)) / decimal(so.rendimientogeneral,10,2)),10,2) / 60.00,10,2) Horas " +
			" from sisappf.app_disponibilidadveg d left join sisvegf.mov_defsiembraveg s on d.siembra = left(s.defsie,4)||s.correlativo left join siscfgf.seguridad_opcion_moduloasp so on d.actividad = so.opcion " +
			" where d.fecha between '"+FechaDe+"' and '"+FechaAl+"' group by so.rendimientogeneral,so.descripcion,d.siembra,d.fecha,d.actividad,s.defvar,s.defban,s.definv,s.defmat " +
			" order by d.fecha,s.definv,s.defban) w group by w.fecha,w.definv,w.actividad order by w.definv,w.actividad");
	//Agregar encabezados de columnas	
	 TableRow Erow1 = new TableRow(ConsultaProyeccion.this);
	 TextView E1 = new TextView(ConsultaProyeccion.this);
	 TextView E2 = new TextView(ConsultaProyeccion.this);
	 TextView E3 = new TextView(ConsultaProyeccion.this);
	 TextView E4 = new TextView(ConsultaProyeccion.this);
	 TextView E5 = new TextView(ConsultaProyeccion.this);
	 TextView E6 = new TextView(ConsultaProyeccion.this);
	 TextView E7 = new TextView(ConsultaProyeccion.this);
	 TextView E8 = new TextView(ConsultaProyeccion.this); 
	 TextView E9 = new TextView(ConsultaProyeccion.this); 
	 TextView E10 = new TextView(ConsultaProyeccion.this); 
	 TextView E11 = new TextView(ConsultaProyeccion.this); 
	 TextView E12 = new TextView(ConsultaProyeccion.this); 
	 TextView E13 = new TextView(ConsultaProyeccion.this);
	 E1.setTextSize(15);
	 E1.setTextColor(Color.WHITE);
	 E2.setTextSize(15);
	 E2.setTextColor(Color.WHITE);
	 E3.setTextSize(15);
	 E3.setTextColor(Color.WHITE);
	 E4.setTextSize(15);
	 E4.setTextColor(Color.WHITE);
	 E5.setTextSize(15);
	 E5.setTextColor(Color.WHITE);
	 E6.setTextSize(15);
	 E6.setTextColor(Color.WHITE);
	 E7.setTextSize(15);
	 E7.setTextColor(Color.WHITE);
	 E8.setTextSize(20);
	 E8.setTextColor(Color.BLACK);
	 E9.setTextSize(20);
	 E9.setTextColor(Color.BLACK);
	 E10.setTextSize(20);
	 E10.setTextColor(Color.BLACK);
	 E11.setTextSize(20);
	 E11.setTextColor(Color.BLACK);
	 E12.setTextSize(20);
	 E12.setTextColor(Color.BLACK);
	 E13.setTextSize(20);
	 String Cn1 = rs.getMetaData().getColumnName(1);
	 String Cn2 = rs.getMetaData().getColumnName(2);
	 String Cn3 = rs.getMetaData().getColumnName(3);
	 String Cn4 = rs.getMetaData().getColumnName(4);
	 String Cn5 = rs.getMetaData().getColumnName(5);
	 String Cn6 = rs.getMetaData().getColumnName(6);
	 String Cn7 = rs.getMetaData().getColumnName(7);
	 E1.setText(Cn1);
	 E2.setText(Cn2);
	 E3.setText(Cn3);
	 E4.setText(Cn4);
	 E5.setText(Cn5);
	 E6.setText(Cn6);
	 E7.setText(Cn7);
	 E8.setText(es);
	 E9.setText(es);
	 E10.setText(es);
	 E11.setText(es);
	 E12.setText(es);
	 E13.setText(es);
	 Erow1.addView(E1);
	 Erow1.addView(E8);
	 Erow1.addView(E2);
	 Erow1.addView(E9);
	 Erow1.addView(E3);
	 Erow1.addView(E10);
	 Erow1.addView(E4);
	 Erow1.addView(E11);
	 Erow1.addView(E5);
	 Erow1.addView(E12);
	 Erow1.addView(E6);
	 Erow1.addView(E13);
	 Erow1.addView(E7);
	 E1.setGravity(Gravity.CENTER_HORIZONTAL);
	 E2.setGravity(Gravity.CENTER_HORIZONTAL);
	 E3.setGravity(Gravity.CENTER_HORIZONTAL);
	 E4.setGravity(Gravity.CENTER_HORIZONTAL);
	 E5.setGravity(Gravity.CENTER_HORIZONTAL);
	 E6.setGravity(Gravity.CENTER_HORIZONTAL);
	 E7.setGravity(Gravity.CENTER_HORIZONTAL);
	 E8.setGravity(Gravity.CENTER_HORIZONTAL);
	 E9.setGravity(Gravity.CENTER_HORIZONTAL);
	 E10.setGravity(Gravity.CENTER_HORIZONTAL);
	 E11.setGravity(Gravity.CENTER_HORIZONTAL);
	 E12.setGravity(Gravity.CENTER_HORIZONTAL);
	 E13.setGravity(Gravity.CENTER_HORIZONTAL);
	 Erow1.setGravity(Gravity.CENTER);
	 Erow1.setBackgroundColor(Color.BLUE);
//	 TablaEnc.addView(Erow1);	
	// finaliza agregar encabezados de columnas 
	int sz = rs.getFetchSize();
	int cont = 0;
	while (rs.next()) {
		 String d1 = rs.getString(1).trim();
		 String d2 = rs.getString(2).trim();
		 String d3 = rs.getString(3).trim();
		 String d4 = rs.getString(4).trim();
	     String d5 = rs.getString(5).trim();
		 String d6 = rs.getString(6).trim();
		 String d7 = rs.getString(7).trim();
		 String c1 = comprobar(d1);
		 String c2 = comprobar(d2);
		 String c3 = comprobar(d3);
		 String c4 = comprobar(d4);
		 String c5 = comprobar(d5);
		 String c6 = comprobar(d6);
		 String c7 = comprobar(d7);
		 for (int i = 0; i == sz; i++)
		 {	
			 cont = cont +1;
			 TableRow row1 = new TableRow(ConsultaProyeccion.this);
			 TextView t1 = new TextView(ConsultaProyeccion.this);
			 TextView t2 = new TextView(ConsultaProyeccion.this);
			 TextView t3 = new TextView(ConsultaProyeccion.this);
			 TextView t4 = new TextView(ConsultaProyeccion.this);
			 TextView t5 = new TextView(ConsultaProyeccion.this);
			 TextView t6 = new TextView(ConsultaProyeccion.this);
			 TextView t7 = new TextView(ConsultaProyeccion.this);
			 TextView t8 = new TextView(ConsultaProyeccion.this); 
			 TextView t9 = new TextView(ConsultaProyeccion.this); 
			 TextView t10 = new TextView(ConsultaProyeccion.this); 
			 TextView t11 = new TextView(ConsultaProyeccion.this); 
			 TextView t12 = new TextView(ConsultaProyeccion.this); 
			 TextView t13 = new TextView(ConsultaProyeccion.this);
			 t1.setTextSize(13);
			 t1.setTextColor(Color.BLACK);
			 t2.setTextSize(13);
			 t2.setTextColor(Color.BLACK);
			 t3.setTextSize(13);
			 t3.setTextColor(Color.BLACK);
			 t4.setTextSize(13);
			 t4.setTextColor(Color.BLACK);
			 t5.setTextSize(13);
			 t5.setTextColor(Color.BLACK);
			 t6.setTextSize(13);
			 t6.setTextColor(Color.BLACK);
			 t7.setTextSize(13);
			 t7.setTextColor(Color.BLACK);
			 t8.setTextSize(20);
			 t8.setTextColor(Color.BLACK);
			 t9.setTextSize(20);
			 t9.setTextColor(Color.BLACK);
			 t10.setTextSize(20);
			 t10.setTextColor(Color.BLACK);
			 t11.setTextSize(20);
			 t11.setTextColor(Color.BLACK);
			 t12.setTextSize(20);
			 t12.setTextColor(Color.BLACK);
			 t13.setTextSize(20);
			 t1.setGravity(Gravity.CENTER_HORIZONTAL);
			 t2.setGravity(Gravity.CENTER_HORIZONTAL);
			 t3.setMaxWidth(150);
			 t4.setMaxWidth(150);
			 t5.setMaxWidth(100);
			 t4.setGravity(Gravity.CENTER_HORIZONTAL);
			 t5.setGravity(Gravity.CENTER_HORIZONTAL);
			 t6.setGravity(Gravity.CENTER_HORIZONTAL);
			 t7.setGravity(Gravity.CENTER_HORIZONTAL);
			 t8.setGravity(Gravity.CENTER_HORIZONTAL);
			 t9.setGravity(Gravity.CENTER_HORIZONTAL);
			 t10.setGravity(Gravity.CENTER_HORIZONTAL);
			 t11.setGravity(Gravity.CENTER_HORIZONTAL);
			 t12.setGravity(Gravity.CENTER_HORIZONTAL);
			 t13.setGravity(Gravity.CENTER_HORIZONTAL);
			 row1.setGravity(Gravity.CENTER);
			 row1.setClickable(true);
			 if(cont%2 == 0){
			 row1.setBackgroundColor(Color.LTGRAY);
			 }else {
			 row1.setBackgroundColor(Color.WHITE);	 
			 }
		 t1.setText(c1);
		 t2.setText(c2);
		 t3.setText(c3);
		 t4.setText(c4);
		 t5.setText(c5);
		 t6.setText(c6);
		 t7.setText(c7);
		 t8.setText(es);
		 t9.setText(es);
		 t10.setText(es);
		 t11.setText(es);
		 t12.setText(es);
		 t13.setText(es);
		 row1.addView(t1);
		 row1.addView(t8);
		 row1.addView(t2);
		 row1.addView(t9);
		 row1.addView(t3);
		 row1.addView(t10);
		 row1.addView(t4);
		 row1.addView(t11);
		 row1.addView(t5);
		 row1.addView(t12);
		 row1.addView(t6);
		 row1.addView(t13);
		 row1.addView(t7);
		 TablaEnc.addView(row1);
		 TablaEnc.setClickable(true);
		 }
		 
	}
	connection.close();	
}
catch (Exception e) {
	Toast toast = Toast.makeText(getApplicationContext(), "Verificar Conexi�n.....", Toast.LENGTH_SHORT);
	toast.show();
	e.printStackTrace();
} */