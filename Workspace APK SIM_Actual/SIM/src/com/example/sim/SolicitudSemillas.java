package com.example.sim;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Vibrator;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.floricultura.R;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.Dguardados;
import com.example.sim.data.JsonFiles;
import com.example.sim.data.Semillas;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SolicitudSemillas extends ActionBarActivity {
	
	//Variables globales
	private MyApp appState;	
	private DBAdapter db;
	int CodigoModulo;
	int Codigodepartamento;
	int CodigoEmpleado;
	int CodigoOpcion;
	int OpcionPolinizacion;
	int OpcionSuccion;
	int ValorPolinizacion = 0;
	int ValorSuccion=0;
	int TotalEmpleadosAsignados;
	int OpcionCosecha;
	boolean validoDescriptivo;
	String NombreOpcion;
	String Val_Bolsa;
	String pdes;
	String pflor;
	String pfecha;
	String pgramos;
	String dato;
	
	LinearLayout validacionesGroup;
	TextView TCantidadAsignados;
	EditText descriptivo;
	EditText empleado;	
	EditText Gramos;
	Vibrator mVibrator;
	Ringtone ringtone;

	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_solicitud_semillas);
			
		//se obtienen lo datos de la actividad antetior por medio del intent
		Bundle extras = getIntent().getExtras();
		CodigoModulo = extras.getInt("CodigoModulo");
	    CodigoEmpleado = extras.getInt("CodigoEmpleado");
	    Codigodepartamento = extras.getInt("Codigodepartamento");
	    CodigoOpcion = extras.getInt("CodigoOpcion");
	    NombreOpcion = extras.getString("NombreOpcion");
	    OpcionPolinizacion = extras.getInt("OpcionPolinizacion");
	    OpcionCosecha= extras.getInt("OpcionCosecha");	    
	    OpcionSuccion = extras.getInt("OpcionSuccion");
	    
	    //hacemos referencia a todos los objetos de nuestra vista
	    TextView TNombreCaptura = (TextView)findViewById(R.id.OpcionCaptura);
	    TextView TCodigoEmpleado= (TextView)findViewById(R.id.CodigoEmpleado);
	    TextView TCodigoOpcion= (TextView)findViewById(R.id.codigoOpcion);
	    TextView TGramos = (TextView)findViewById(R.id.textPeso);
	    final TextView Fecha = (TextView)findViewById(R.id.textFecha);
	    
	    TCantidadAsignados = (TextView)findViewById(R.id.textEmpleadosAsginados);
	    validacionesGroup = (LinearLayout)findViewById(R.id.layoutValidaciones);
	    descriptivo = (EditText)findViewById(R.id.editTextDescriptivo);
	    empleado  = (EditText)findViewById(R.id.editEmpleado);
	    Gramos = (EditText)findViewById(R.id.editGramos);
	    Button buttonNuevo = (Button) findViewById(R.id.btnNuevo);
        Button btnBack = (Button) findViewById(R.id.btnBack);
	    Button botonGuarda = (Button) findViewById(R.id.buttonGuarda);
	    Button botonFecha =  (Button) findViewById(R.id.botonFecha);
	    Button botonConsulta = (Button) findViewById(R.id.buttonsConsulta);
	    
	    //hacemos las inicializaciones necesarias a nuestra vista
	    TNombreCaptura.setText(NombreOpcion+ "");
	    TCodigoEmpleado.setText(CodigoEmpleado + "");
	    TCodigoOpcion.setText(CodigoOpcion + "");
	    validoDescriptivo = false;
	    if(CodigoOpcion == 29729701){
	    	botonConsulta.setVisibility(View.VISIBLE);
	    } else {
	    	botonConsulta.setVisibility(View.INVISIBLE);
	    }
	    validacionesGroup.setVisibility(View.VISIBLE);
	    
	    //incializamos la BD asi como tambien obtenemos la referencia al singleton
	    appState = ((MyApp)getApplicationContext());
	    Calendar c = Calendar.getInstance(); 
		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
		final String fecha = fdate.format(c.getTime());
	    db = appState.getDb();
	    db.open();	    
	    
	    
	    botonFecha.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Calendar mcurrentDate=Calendar.getInstance();
                int mYear = mcurrentDate.get(Calendar.YEAR);
                int mMonth = mcurrentDate.get(Calendar.MONTH);
                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker=new DatePickerDialog(SolicitudSemillas.this, new OnDateSetListener() {                  
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                    	int datodia = selectedday;
                    	String dia = format(datodia);
                    	int datomes = selectedmonth + 1;
                    	String mes = format(datomes);
                    	String datoanio = Integer.toString(selectedyear);
                    	dato = datoanio + "-" + mes + "-" +dia;
                    	if(dato.compareTo(fecha) < 0 ){
                     		alertDialogMensaje(" ERROR "," La fecha de Solicitud \n No puede ser \n Menor al dia de Hoy ");
                     		Fecha.setText("");
                     	}else {
                    	Fecha.setText(dato);
                     	}
                    }
                }
                ,mYear, mMonth, mDay);
                mDatePicker.setTitle("Seleccionar Fecha de Solicitud");                
                mDatePicker.show(); 
            }
            
        });
	    
	    
	    //al recibir el enter se verifica si el descriptivo es correcto y lo manda al siguiente input sino le muestra un mensaje al usuario
        descriptivo.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                		VerificarDatos(descriptivo.getText().toString().trim());
	                	if (validoDescriptivo == true){
	                		descriptivo.setEnabled(false);
	                		empleado.requestFocus();
	                	}else{
		    	    		alertDialogMensaje("Desciptivo", "Numero de Siembra Invalido");
		    	    		descriptivo.setText("");
		    	    		descriptivo.requestFocus();
	                	}
	                  return true;
                }
                return false;
            }
        });
        
        

        botonGuarda.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	        	VerificarDatos(descriptivo.getText().toString().trim());
	        	String p1 = "";
	        	pdes = descriptivo.getText().toString().trim();
	        	pflor = empleado.getText().toString().trim();
	        	pfecha = Fecha.getText().toString().trim();
	        	pgramos = Gramos.getText().toString().trim();
	        	if(pdes.equals(p1)){
	        		alertDialogMensaje("Mensaje", "Datos Invalidos");
	        		descriptivo.requestFocus();
	        	} else if(pflor.equals(p1)){
	        		alertDialogMensaje("Mensaje", "Datos Invalidos");
	        		empleado.requestFocus();
	        	}else if(pgramos.equals(p1)){
	        		Gramos.setText("0");
	        	}else if(validoDescriptivo == false) {
	        		alertDialogMensaje("Mensaje", "NUMERO DE SIEMBRA INVALIDO");
	        		descriptivo.setText("");
	        		empleado.setText("");
	        		descriptivo.requestFocus();
	        	}else if(pfecha.equals(p1)){
	        		alertDialogMensaje("Seleccione Fecha", "Debe Seleccionar La Fecha \n Para el Pedido");
	        	}
	        	else
	        	{
	        	Dguardados dtemp = new Dguardados();
        		dtemp.setCodigoDepto(Codigodepartamento);
        		dtemp.setSiembra(descriptivo.getText().toString().trim());
        		dtemp.setPlantas(empleado.getText().toString().trim());
        		dtemp.setPesoPolen(Gramos.getText().toString().trim());
        		
        		Calendar c = Calendar.getInstance(); 
        		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
        		String fecha = fdate.format(c.getTime());
        		SimpleDateFormat ftime = new SimpleDateFormat("HH:mm:ss");
        		String horaInicio = ftime.format(c.getTime());
        		
        		dtemp.setFecha(fecha);
        		dtemp.setHoraIncio(horaInicio);
        		dtemp.setActividad(CodigoOpcion);
        		dtemp.setSupervisor(CodigoEmpleado);
        		dtemp.setActividadFinalizada(1);
        		dtemp.setCorrelativoBolsa(Fecha.getText().toString().trim());
    	
        		Log.d("Dguardados", dtemp.toString().trim());
        		
        		//db.insertarDguardados(dtemp);
        		
        		//generacion archivo json
        		ObjectMapper mapper = new ObjectMapper();
        		try {
        			String nameJson ="S_" + dtemp.getSiembra() + "_"+dtemp.getFecha() + "_"+dtemp.getHoraIncio();
        			nameJson=nameJson.replace(":", "-");	
					mapper.writeValue(new File( Environment.getExternalStorageDirectory()+"/SIM/JsonDescarga/" + nameJson+ ".json"), dtemp);
					
					JsonFiles archivo = new JsonFiles();
					archivo.setName(nameJson+ ".json");
					archivo.setNameFolder("SinFinalizar/");
					archivo.setUpload(0);
					db.insertarJsonFile(archivo);
					
					if(!appState.getSubiendoArchivos()){
						appState.setSubiendoArchivos(true);
						new UptoDropbox().execute(getApplicationContext());
					}
	
        		} catch (JsonGenerationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JsonMappingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        		
        		empleado.setText("");         	
        		empleado.requestFocus();
        		
        		TotalEmpleadosAsignados++;
        		TCantidadAsignados.setText(TotalEmpleadosAsignados + "");       	 
	        }
	        } 
	    });       
               
	    //se limpian los campos y variables para una nueva captura
	    buttonNuevo.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	        	validacionesGroup.setVisibility(View.VISIBLE); 
				 descriptivo.setText("");
				 empleado.setText(""); 
				 TCantidadAsignados.setText("0");
				 validoDescriptivo = false;
				 descriptivo.setEnabled(true);
				 descriptivo.requestFocus();
				 
				 
	        }   
	    });
        
        btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
 
            	onBackPressed();
            }
        
        });
        
        botonConsulta.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Intent intent = new Intent(getApplicationContext(), CONSULTAFILTROS.class);
           	    intent.putExtra("CodigoEmpleado", CodigoEmpleado );
           	    startActivity(intent);
            	
            }
        });
        
	}  
        
	public void alertDialogMensaje(String message1, String mesage2){
		
		//mVibrator.vibrate(300);
		
		try {
		    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
		    ringtone.play();
		    
		    
		} catch (Exception e) {
		    e.printStackTrace();
		}
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(message1);
		alertDialog.setMessage(mesage2);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int which) {
		// here you can add functions
			ringtone.stop();
		}
		});
		alertDialog.show();
	}

	public void VerificarDatos(String dato){	
		 Cursor CursorSemillas = db.getSemillas("Siembra='" + dato + "'");
		 if(CursorSemillas.getCount()>0){
			 Semillas s = db.getSemillasFromCursor(CursorSemillas, 0);
			 Log.d("SEMILLA",s.getSiembra() + ", "+s.getMaterial() + " ," + s.getPolinizacion());
			 validoDescriptivo = true;
			 } else {
				 validoDescriptivo = false;
			 }
		 CursorSemillas.close();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.captura_disponibilidad, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void ButtonOnClick(View v) {
	    switch (v.getId()) {
	      case R.id.buttonborra:
		        agregarNumero("1");
	        break;
	      case R.id.btnDos:
	    	  agregarNumero("2");
	        break;
	      case R.id.btnTres:
		        agregarNumero("3");
	        break;
	      case R.id.btnCuatro:
	    	  agregarNumero("4");
	        break;
	      case R.id.btnCinco:
		        agregarNumero("5");
	        break;
	      case R.id.btnSeis:
	    	  agregarNumero("6");
	        break;
	      case R.id.btnSiete:
	    	  agregarNumero("7");
	        break;
	      case R.id.btnOcho:
		        agregarNumero("8");
	        break;
	      case R.id.btnNueve:
	    	  agregarNumero("9");
	        break;
	      case R.id.button0:
		        agregarNumero("0");
	        break;
	      case R.id.btnCero:
	    	  agregarNumero("b");
	        break;
	      case R.id.buttonP:
	    	  agregarNumero(".");
	        break;
	      }
	     
	}
	
	public String format (int f){
		String val = "";
		if(f<10){
		val = "0"+Integer.toString(f);	
		}
		else{
		val = Integer.toString(f);
		}
		return val;
	}
	
	
	public void agregarNumero(String n){
		
		if(n.contains("b")){
			
			if(empleado.isFocused()){
				empleado.setText("");			
			} else if(Gramos.isFocused()){
				Gramos.setText("");
			}
			
		}else if(empleado.isFocused()){
				empleado.setText(empleado.getText() + n);
			}
		else if(Gramos.isFocused()){
				Gramos.setText(Gramos.getText() + n);
		}
	}

}