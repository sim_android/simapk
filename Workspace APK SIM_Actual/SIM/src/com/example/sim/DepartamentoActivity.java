package com.example.sim;

import java.util.ArrayList;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.example.floricultura.R;
import com.example.sim.adapters.DatosLista;
import com.example.sim.adapters.ListAdapter;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.Mdepartamento;

public class DepartamentoActivity extends ActionBarActivity {

	ArrayList<DatosLista> listado = new ArrayList<DatosLista>();
	int CodigoEmpleado;
	String UsuarioLogueado;
	int Administrador;
	private MyApp appState;	
	private DBAdapter db;
	private String activityString;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_departamento);
		
		 appState = ((MyApp)getApplicationContext());   
		 db = appState.getDb();
		 db.open();
		
		
		Bundle extras = getIntent().getExtras();
	    CodigoEmpleado = extras.getInt("CodigoEmpleado");
	    Administrador = extras.getInt("Administrador");
	    UsuarioLogueado = extras.getString("UsuarioLogueado");

		ListView listadoVista = (ListView)findViewById(R.id.ListDepartamento);
		listado = getDatos();
		
        ListAdapter adaptador = new ListAdapter(this, R.layout.filas_lista, listado);
        listadoVista.setAdapter(adaptador);
        
        

        listadoVista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {   
            	DatosLista dato = (DatosLista) parent.getItemAtPosition(position);
            	 try{
            		 int Depto = Integer.parseInt(dato.getCampo1().trim());
            		 String Menu = getMenu(Depto+"");
           		
            		Class newclass = null;            		
            		activityString = "com.example.sim."+ Menu;
         			try {
       			      newclass = Class.forName(activityString);
       			    } catch (ClassNotFoundException c) {
       			        c.printStackTrace();
       			    } 
            		Intent intent = new Intent(getApplicationContext(), newclass);
	           	 	intent.putExtra("Codigodepartamento", Integer.parseInt(dato.getCampo1().trim()) );
	           	    intent.putExtra("CodigoEmpleado", CodigoEmpleado );
	           	 	intent.putExtra("Administrador", Administrador);
	           	 	intent.putExtra("UsuarioLogueado", UsuarioLogueado);
	           	 	startActivity(intent);
            		 
            	 } catch (Exception e) {
         			e.printStackTrace();
         		} 
            }
        });
        
        
        
        Button btnBack = (Button) findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
 
            	onBackPressed();
            }
        });
        
        
        Button btnInicio = (Button) findViewById(R.id.btnInicio);
        btnInicio.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Intent intent = new Intent(getApplicationContext(), MAINACTIVITY.class);
           	 	startActivity(intent);
            }
        });
        
	}
	
	public String getMenu(String cod){
		Cursor Menu = this.db.getMdepartamento("CodigoDepto=" + cod);
		Mdepartamento men = this.db.getMdepartamentoFromCursor(Menu, 0);
		String Inicio = men.getMenu();	
		Menu.close();
	return Inicio;
	}
	
	
	public ArrayList<DatosLista> getDatos(){
		ArrayList<DatosLista> listaTemp = new ArrayList<DatosLista>();
		listaTemp.add(new DatosLista("CD", "Departamento", ""));
		

		 Cursor Deptos = null ;
		 Deptos = this.db.getMdepartamento("");
		 
		 if(Deptos.getCount()>0){
			 for(int i=0 ; i<Deptos.getCount();i++){	         	
				 Mdepartamento d = db.getMdepartamentoFromCursor(Deptos, i);  				 
				 listaTemp.add(new DatosLista(d.getCodigoDepto()+ " ", d.getDescripcionDepto(), ""));			 
		     } 
		 }

		
		 Deptos.close();
		return listaTemp;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.departamento, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}



}
