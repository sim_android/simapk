package com.example.sim;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.os.Vibrator;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.floricultura.R;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.DUbicaciones;
import com.example.sim.data.ERiego;
import com.example.sim.data.JsonFiles;
import com.example.sim.data.Opciones;
import com.example.sim.data.Semillas;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@SuppressLint("NewApi") public class AsignaRiegoSem extends ActionBarActivity {
	
	//Variables globales
	private MyApp appState;	
	private DBAdapter db;
	int CodigoModulo;
	int Codigodepartamento;
	int CodigoEmpleado;
	int CodigoOpcion;
	String CodEmpleado;
	String NombreEmple;
	int TotalEmpleadosAsignados;
	boolean validoDescriptivo;
	String NombreOpcion;
	TextView TCantidadAsignados;
	Vibrator mVibrator;
	Ringtone ringtone;
	String EmpleadoEscaneado;
	String Ubicacion = "";
	String[] datos = null;
	String[] data = null;
	String ValorUbicacion = "";
	TextView TextActividad;
	String CodigoActividad = "";
	TextView TextUbicacion;
	TextView empleado;
	TextView NombreEmpleado;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.asignacion_riegosem);
	
		//se obtienen lo datos de la actividad antetior por medio del intent
		Bundle extras = getIntent().getExtras();
		CodigoModulo = extras.getInt("CodigoModulo");
	    CodigoEmpleado = extras.getInt("CodigoEmpleado");
	    Codigodepartamento = extras.getInt("Codigodepartamento");
	    CodEmpleado = extras.getString("CodEmpleado");
	    NombreEmple = extras.getString("NombreEmple");
	   
	    //hacemos referencia a todos los objetos de nuestra vista
	    TextView TCodigoEmpleado= (TextView)findViewById(R.id.CodigoEmpleado);
	    TextUbicacion = (TextView)findViewById(R.id.textUbicacion);
	    TextActividad = (TextView)findViewById(R.id.textActividad);
	    empleado = (TextView)findViewById(R.id.textCodigoEmpleado);
	    NombreEmpleado = (TextView)findViewById(R.id.textNombreEmpleado);    
	    Button botonGuarda = (Button) findViewById(R.id.buttonGuarda);
	    Button botonUbicacion =  (Button) findViewById(R.id.botonUbicacion);
	    Button botonActividad = (Button) findViewById(R.id.botonActividad);
	       
	    //hacemos las inicializaciones necesarias a nuestra vista
	    TCodigoEmpleado.setText(CodigoEmpleado + "");
	    empleado.setText(CodEmpleado);
	    NombreEmpleado.setText(NombreEmple);
	    Calendar c = Calendar.getInstance(); 
		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
		String fecha = fdate.format(c.getTime());
	    if (android.os.Build.VERSION.SDK_INT > 9)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
	    
	    
	  //incializa la BD, tambien obtenemos la referencia al singleton
	    appState = ((MyApp)getApplicationContext());   
	    db = appState.getDb();
	    db.open();
	    
	    // Comandos al presionar el boton Seleccionar Actividad
	    botonActividad.setOnClickListener(new View.OnClickListener() {
	    	 public void onClick(View v) {
	    		 getActividad();
	            	//Creando Cuadro de Dialogo para Listado de Personas
	        		class DialogoSeleccion extends DialogFragment {
	        	        @Override
	        	        public Dialog onCreateDialog(Bundle savedInstanceState) {
	        	         	  
	        	            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	        	    
							builder.setTitle("Actividad").setItems(data, new DialogInterface.OnClickListener() {
	        	                    public void onClick(DialogInterface dialog, int item) {
	        	                        Log.i("Dialogos", "Opci�n elegida: " + data[item]);
	        	                        int inicio = data[item].indexOf(" ");
	        	                        CodigoActividad = data[item].substring(0,inicio);
	        	                        TextActividad.setText(data[item].substring(inicio + 1));
	        	                    }
	        	                });
	        	            return builder.create(); }
	        	    }
	        		
	        		FragmentManager fragmentManager = getSupportFragmentManager();
	        		DialogoSeleccion dialogo = new DialogoSeleccion();
	                dialogo.show(fragmentManager, "Alerta");
	            }
            
        });
	         
	    // Comandos al Presionar el Boton Buscar Ubicacion
	    botonUbicacion.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	getDatos();
            	ValorUbicacion = "";
            	TextUbicacion.setText("");
            	//Creando Cuadro de Dialogo para Listado de Personas
        		class DialogoSeleccion extends DialogFragment {
        	        @Override
        	        public Dialog onCreateDialog(Bundle savedInstanceState) {
        	         	  
        	            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        	    
						builder.setTitle("Ubicacion").setItems(datos, new DialogInterface.OnClickListener() {
        	                    public void onClick(DialogInterface dialog, int item) {
        	                        Log.i("Dialogos", "Opci�n elegida: " + datos[item]);
        	                        int inicio = datos[item].indexOf(" ");
        	                        ValorUbicacion = datos[item].substring(0, inicio);
        	                        TextUbicacion.setText(datos[item].substring(inicio + 1));
        	                        //TextUbicacion.setText(datos[item]);
        	                    }
        	                });
        	            return builder.create(); }
        	    }
        		
        		FragmentManager fragmentManager = getSupportFragmentManager();
        		DialogoSeleccion dialogo = new DialogoSeleccion();
                dialogo.show(fragmentManager, "Alerta");
            }
            
        });
	         	    
         // Comandos al Presionar el Boton Guardar
         botonGuarda.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	String prb = ""; //variable para validar no Vacios
            	EmpleadoEscaneado = "0" + empleado.getText().toString().trim();
            	Ubicacion = ValorUbicacion;
            	if(CodigoActividad.equals(prb)){
            		alertDialogMensaje("ERROR DE ASIGNACION","Debe Elegir una ACTIVIDAD");
            	}if(ValorUbicacion.equals(prb)){
            		alertDialogMensaje("ERROR","Debe Elegir una Ubicaci�n");
            		ValorUbicacion = "";
            		
            	} else{
            	GuardarDatos(); 
            	//Se destrulle la pila de Actividades con  .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP 
            	Intent i = new Intent(getApplicationContext(), ModuloActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                i.putExtra("CodigoEmpleado",CodigoEmpleado);
                i.putExtra("Administrador", 0);	
                startActivity(i);
            	}
            }
        });
	    
	}
	
		
	public void alertDialogMensaje(String message1, String mesage2){
		
		//mVibrator.vibrate(300);
		
		try {
		    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
		    ringtone.play();
		    
		    
		} catch (Exception e) {
		    e.printStackTrace();
		}
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(message1);
		alertDialog.setMessage(mesage2);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int which) {
		// here you can add functions
			ringtone.stop();
		}
		});
		alertDialog.show();
	}
		
	public void VerificarDatos(String dato){	
		 Cursor CursorSemillas = db.getSemillas("Siembra='" + dato + "'");
		 if(CursorSemillas.getCount()>0){
			 Semillas s = db.getSemillasFromCursor(CursorSemillas, 0);
			 Log.d("SEMILLA",s.getSiembra() + ", "+s.getMaterial() + " ," + s.getPolinizacion());
			 validoDescriptivo = true;
			 } else {
				 validoDescriptivo = false;
			 }
		 CursorSemillas.close();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.captura_disponibilidad, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void getDatos(){
		 Cursor CUbicaciones = null ;
		 CUbicaciones = this.db.getDUbicaciones("");
		 String tmp = null;
		 int j = 0;
		 datos = new String[CUbicaciones.getCount()];
		 if(CUbicaciones.getCount()>0){
			 for(int i=0 ; i<CUbicaciones.getCount();i++){	 
				 DUbicaciones ub = db.getDUbicacionesFromCursor(CUbicaciones, i);
				 tmp = ub.getCodigo().toString().trim() + " " + ub.getDescripcion().toString().trim();
				datos[j] = tmp;
				j++;
			 	} 
		 	}
		 CUbicaciones.close();
		}
	
	public void getActividad(){
		 int modulo1 = 213;
		 int modulo2 = 214;
		 int modulo3 = 215;
		 Cursor COpciones= null ;
		 COpciones = this.db.getOpciones("CodigoDepto in("+ modulo1 +","+ modulo2 +","+ modulo3 +")");
		 Integer tmp = null;
		 String tmp2 = null;
		 int j = 0;
		 int z = COpciones.getCount();
		 int x = z -6;
		 data = new String[x];
		 if(COpciones.getCount()>0){
			 for(int i=0 ; i<COpciones.getCount();i++){	 
				 Opciones op = db.getOpcionesFromCursor(COpciones, i);
				 tmp = op.getOpcion();
				 tmp2 = op.getDescripcion().toString().trim();
				 if(!tmp2.contains("FINALIZA")){
					 data[j] = tmp + "  " +tmp2;
					 j++;
				 	}
			 	} 
		 	}
		 COpciones.close();
		}
	
	public void GuardarDatos(){
    	int existe = VerificarEmpleado(EmpleadoEscaneado);
    	if(existe == 2){
    			String Vacio = "";
    			ERiego dtemp = new ERiego();
        		dtemp.setCodigoEmpleado(Integer.parseInt(EmpleadoEscaneado));
        		dtemp.setCodigoDepto(Codigodepartamento);
        		
        		//-----------------------------------------------------------------------------------
        		String correlativo="";
        		    	//almaceno el empleado en una variable string
        		    	String getEmpleado = empleado.getText().toString().trim();
        		    	// guardo la variable que tiene empleado en un array
        		    	String[]empl = new String[]{getEmpleado};
        		    	Cursor obtenerCorrelativo = db.getCorrelativo(empl);
        		    	//recorro dicho arreglo para obtener el campo
        		    	if (obtenerCorrelativo.moveToFirst()){
        		    		do{
        		    			correlativo = obtenerCorrelativo.getString(0);
        		    		}while(obtenerCorrelativo.moveToNext());
        		    	}
        		//-------------------------------------------------------------------------
        		
        		Calendar c = Calendar.getInstance(); 
        		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
        		String fecha = fdate.format(c.getTime());
        		SimpleDateFormat ftime = new SimpleDateFormat("HH:mm:ss");
        		String horaInicio = ftime.format(c.getTime());
        		
        		if(Ubicacion.equals(Vacio)){
            		dtemp.setUbicacion("0");
            	} else {
            		dtemp.setUbicacion(Ubicacion);
            	}
        		
        		dtemp.setFP("S");
        		dtemp.setFecha(fecha);
        		dtemp.setHoraIncio(horaInicio);
        		dtemp.setActividad(Integer.parseInt(CodigoActividad));
        		dtemp.setSupervisor(CodigoEmpleado);
        		dtemp.setActividadFinalizada(0);
        		dtemp.setCorrelativoEmpleado(correlativo);
       
        		Log.d("Riego", dtemp.toString().trim());    		
        		db.insertarRiego(dtemp);
        		
        		//generacion archivo json
        		ObjectMapper mapper = new ObjectMapper();
        		try {
        			String nameJson ="RF_" + dtemp.getCodigoEmpleado() + "_"+dtemp.getFecha() + "_"+dtemp.getHoraIncio();
        			nameJson=nameJson.replace(":", "-");	
					mapper.writeValue(new File( Environment.getExternalStorageDirectory()+"/SIM/JsonDescarga/" + nameJson+ ".json"), dtemp);
					
					JsonFiles archivo = new JsonFiles();
					archivo.setName(nameJson+ ".json");
					archivo.setNameFolder("SinFinalizar/");
					archivo.setUpload(0);
					db.insertarJsonFile(archivo);
					
					if(!appState.getSubiendoArchivos()){
						appState.setSubiendoArchivos(true);
						new UptoDropbox().execute(getApplicationContext());
					}
					
        		
        		} catch (JsonGenerationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JsonMappingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        	       		
        	}else if(existe == 1){
        		alertDialogMensaje("Asignado", "Este Empleado ya esta asignado");
        	}else if(existe == 0){
        		alertDialogMensaje("Marcaje", "Empleado no encontrado \n Descargar D_Acceso");
    	}
	}
	
		//retornara un valor dependiendo del estado del empleado
		// 0 = no existe
		// 1 = existe pero esta asignado
		// 2 = existe y no esta asignado
	public int VerificarEmpleado(String empleado){
		int valor = 0;
		Cursor CursorDacceso =	db.getDacceso("CodigoEmpleado='" + empleado + "'");
		if(CursorDacceso != null  && CursorDacceso.getCount()>0){
		Cursor CursorGuardados =	db.getRiego("CodigoEmpleado=" + empleado + " AND ActividadFinalizada=0");
		if(CursorGuardados.getCount()>0){
			valor = 1;
			}else{
			valor = 2;
			}
		CursorGuardados.close();
		 }
		CursorDacceso.close();
		  return valor;
		}
	
	public String format (int f){
		String val = "";
		if(f<10){
		val = "0"+Integer.toString(f);	
		}
		else{
		val = Integer.toString(f);
		}
		return val;
	}
	
}