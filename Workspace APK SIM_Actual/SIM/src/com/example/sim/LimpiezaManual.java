package com.example.sim;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Vibrator;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.example.floricultura.R;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.Dguardados;
import com.example.sim.data.JsonFiles;
import com.example.sim.data.Semillas;
import com.example.sim.data.Usuario;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class LimpiezaManual extends ActionBarActivity {
	
	//Variables globales
		private MyApp appState;	
		private DBAdapter db;
		int CodigoModulo;
		int Codigodepartamento;
		int CodigoEmpleado;
		int CodigoOpcion;
		int TotalEmpleadosAsignados;
		boolean validoDescriptivo = false;
		boolean validoMaterial = false;
		boolean confirma = false; 
		String TextoEtiqueta;
		String Siembra;
		//String Arnero;
		String NombreOpcion;
		String Val_Material;
		int Supervisa;
		String Material;
		String Material1;
		TextView TCantidadAsignados;
		EditText Etiqueta;
		EditText HProceso;
		EditText Bote1;
		EditText Bote2;
		EditText Molde;
		EditText Mueble;
		EditText empleado;
		EditText Supervisor;
		TextView TextEtiqueta;
		TextView TextHProceso;
		TextView TextBote1;
		TextView TextBote2;
		TextView TextMolde;
		TextView TextMueble;
		TextView TextEmpleado;
		Vibrator mVibrator;
		Ringtone ringtone;
		CheckBox checkbox1;
		
		@Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.activity_limpieza_manual);
			
			//se obtienen lo datos de la actividad antetior por medio del intent
			Bundle extras = getIntent().getExtras();
			CodigoModulo = extras.getInt("CodigoModulo");
		    CodigoEmpleado = extras.getInt("CodigoEmpleado");
		    Codigodepartamento = extras.getInt("Codigodepartamento");
		    CodigoOpcion = extras.getInt("CodigoOpcion");
		    NombreOpcion = extras.getString("NombreOpcion");
		  
		    
		    //hacemos referencia a todos los objetos de nuestra vista
		    TextView TNombreCaptura = (TextView)findViewById(R.id.OpcionCaptura);
		    TextView TCodigoEmpleado= (TextView)findViewById(R.id.CodigoEmpleado);
		    TextView TCodigoOpcion= (TextView)findViewById(R.id.codigoOpcion);
		    TextEtiqueta= (TextView)findViewById(R.id.textViewEtiqueta);
		    TextHProceso= (TextView)findViewById(R.id.textViewHProceso);
		    TextBote1= (TextView)findViewById(R.id.textViewBote1);
		    TextBote2= (TextView)findViewById(R.id.textViewBote2);
		    TextMolde= (TextView)findViewById(R.id.TextViewMolde);
		    TextMueble= (TextView)findViewById(R.id.TextViewMueble);
		    TextEmpleado= (TextView)findViewById(R.id.textView29);    
		    Etiqueta = (EditText)findViewById(R.id.editEtiquet);
		    HProceso = (EditText)findViewById(R.id.editHproces);
		    Bote1 = (EditText)findViewById(R.id.EditTextBote1);
		    Bote2 = (EditText)findViewById(R.id.EditTextBote2);
		    Molde = (EditText)findViewById(R.id.EditTextMolde);
		    Mueble = (EditText)findViewById(R.id.EditTextMueble);
		    empleado  = (EditText)findViewById(R.id.editEmpleado);
		    Supervisor = (EditText)findViewById(R.id.editSupervisor);
		    Button buttonNuevo = (Button) findViewById(R.id.btnNuevo);
	        Button btnBack = (Button) findViewById(R.id.btnBack);
	        Button btnListado = (Button) findViewById(R.id.btnListado);
		    
		    
		    //hacemos las inicializaciones necesarias a nuestra vista
		    TNombreCaptura.setText(NombreOpcion+ "");
		    TCodigoEmpleado.setText(CodigoEmpleado + "");
		    TCodigoOpcion.setText(CodigoOpcion + "");
		   // Etiqueta.requestFocus();
		  
		  
		 
		    //incializamos la BD asi como tambien obtenemos la referencia al singleton
		    appState = ((MyApp)getApplicationContext());   
		    db = appState.getDb();
		    db.open();
		    

		 //   verificarAsginados();
		    
	        Etiqueta.setOnKeyListener(new View.OnKeyListener() {
	            public boolean onKey(View v, int keyCode, KeyEvent event) {
	                // If the event is a key-down event on the "enter" button
	                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
	                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
	                	    TextoEtiqueta = Etiqueta.getText().toString().trim();
	                		Siembra = TextoEtiqueta.substring(0,8);
	                	    //alertDialogMensaje("Siembra", Siembra);
	                		VerificarDatos(Siembra);
		                	if (validoDescriptivo == true){	
		               			HProceso.requestFocus();
		               			Etiqueta.setEnabled(false);
		                	}else if(validoDescriptivo == false){
			    	    		alertDialogMensaje("Etiqueta", "ERROR EN ETIQUETA");
			    	    		Etiqueta.setText("");
			    	    		Etiqueta.requestFocus();
		                	}  return true;    	 
	                }   
	                return false; 
	            } 
	        });
	        
	        HProceso.setOnKeyListener(new View.OnKeyListener() {
	            public boolean onKey(View v, int keyCode, KeyEvent event) {
	                // If the event is a key-down event on the "enter" button
	                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
	                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
	                		VerificarDatos(Siembra);
		                	if (validoDescriptivo == true){
		               			Bote1.requestFocus();
		               			HProceso.setEnabled(false);
		                	}else{
			    	    		alertDialogMensaje("Etiqueta", "ERROR EN ETIQUETA");
			    	    		Etiqueta.setText("");
			    	    		Etiqueta.requestFocus();
		                	}
		                	return true;  
	                }     
	                return false;
	            }  
	        });

	        Bote1.setOnKeyListener(new View.OnKeyListener() {
	            public boolean onKey(View v, int keyCode, KeyEvent event) {
	                // If the event is a key-down event on the "enter" button
	                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
	                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
	                		VerificarMaterial(Bote1.getText().toString().trim());
		                	if (validoMaterial == true){
		               			Bote2.requestFocus();
		               			Bote1.setEnabled(false);
		                	}else{
			    	    		alertDialogMensaje("Material", "MATERIAL INCORRECTO");
			    	    		LimpiarError();
		                	}
		                	return true;  
	                }     
	                return false;
	            }  
	        });
	        
	        Bote2.setOnKeyListener(new View.OnKeyListener() {
	            public boolean onKey(View v, int keyCode, KeyEvent event) {
	                // If the event is a key-down event on the "enter" button
	                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
	                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
	                		VerificarMaterial(Bote2.getText().toString().trim());
		                	if (validoMaterial == true){
		               			Molde.requestFocus();
		               			Bote2.setEnabled(false);
		                	}else{
			    	    		alertDialogMensaje("Material", "MATERIAL INCORRECTO");
			    	    		LimpiarError();
		                	}
		                	return true;  
	                }     
	                return false;
	            }  
	        });
	        
	        Molde.setOnKeyListener(new View.OnKeyListener() {
	            public boolean onKey(View v, int keyCode, KeyEvent event) {
	                // If the event is a key-down event on the "enter" button
	                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
	                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
	                		VerificarMaterial(Molde.getText().toString().trim());
		                	if (validoMaterial == true){
		               			Mueble.requestFocus();
		               			Molde.setEnabled(false);
		                	}else{
			    	    		alertDialogMensaje("Material", "MATERIAL INCORRECTO");
			    	    		LimpiarError();
		                	}
		                	return true;  
	                }     
	                return false;
	            }  
	        });
	        
	        Mueble.setOnKeyListener(new View.OnKeyListener() {
	            public boolean onKey(View v, int keyCode, KeyEvent event) {
	                // If the event is a key-down event on the "enter" button
	                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
	                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
	                		VerificarMaterial(Mueble.getText().toString().trim());
		                	if (validoMaterial == true){
		               			Supervisor.requestFocus();
		               			Mueble.setEnabled(false);
		                	}else{
			    	    		alertDialogMensaje("Material", "MATERIAL INCORRECTO");
			    	    		LimpiarError();
		                	}
		                	return true;  
	                }     
	                return false;
	            }  
	        });
	        
	        Supervisor.setOnKeyListener(new View.OnKeyListener() {
	            public boolean onKey(View v, int keyCode, KeyEvent event) {
	                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
	                    (keyCode == KeyEvent.KEYCODE_ENTER)) { 
	                	int valida = VerificarSupervisor(Supervisor.getText().toString().trim());
	                	if(valida == 1){
	                	 Supervisor.setEnabled(false);
	                	 empleado.requestFocus();
	                	}else{
	                		//alertDialogMensaje("Supervisor", "El SUPERVISOR NO EXISTE");
	                		Supervisor.setText("");
	                	}return true; 
	            }
	                return false;
	            }
	        });
	     
	        empleado.setOnKeyListener(new View.OnKeyListener() {
	            public boolean onKey(View v, int keyCode, KeyEvent event) {
	                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
	                    (keyCode == KeyEvent.KEYCODE_ENTER)) {  
	                	 //Validaciones	
	                	ValidaCampos(Etiqueta.getText().toString().trim());
	                	if(confirma == false){
	                		alertDialogMensaje("Mensaje", "Falta Etiqueta");
	                		LimpiarCampos();
	                	} else{
	                		ValidaCampos(HProceso.getText().toString().trim());
	                		if(confirma == false){
		                		alertDialogMensaje("Mensaje", "Faltan Datos");
		                		LimpiarError();
		                	} else{
		                		ValidaCampos(Bote1.getText().toString().trim());
		                		if(confirma == false){
			                		alertDialogMensaje("Mensaje", "Faltan Datos");
			                		LimpiarError();
			                	} else{
			                		ValidaCampos(Bote2.getText().toString().trim());
			                		if(confirma == false){
				                		alertDialogMensaje("Mensaje", "Faltan Datos");
				                		LimpiarError();
				                	} else{
				                		ValidaCampos(Molde.getText().toString().trim());
				                		if(confirma == false){
					                		alertDialogMensaje("Mensaje", "Faltan Datos");
					                		LimpiarError();
					                	} else{
					                		ValidaCampos(Mueble.getText().toString().trim());
					                		if(confirma == false){
						                		alertDialogMensaje("Mensaje", "Faltan Datos");
						                		LimpiarError();
						                	} else{
						                		ValidaCampos(Supervisor.getText().toString().trim());
						                		if(confirma == false){
							                		alertDialogMensaje("Mensaje", "Faltan Datos");
							                		LimpiarError();
							                	} else{
						                		GuardarDatos();
							                }
					                	}
				                	}
			                	} 
		                	} 
	                	} 
	                }return true;  
	            }
	                return false;
	            }
	        });
	        
		 buttonNuevo.setOnClickListener(new View.OnClickListener() {
		        public void onClick(View v) {
					 Material = "";
					 LimpiarCampos();	 
		        }
		    });
	        
	        btnBack.setOnClickListener(new View.OnClickListener() {
	            public void onClick(View v) {
	            	onBackPressed();
	            }
	        });
	        
	        btnListado.setOnClickListener(new View.OnClickListener() {
	            public void onClick(View v) {
	            	Intent intent = new Intent(getApplicationContext(), LISTADOLIMPIEZA.class);
	           	    intent.putExtra("CodigoEmpleado", CodigoEmpleado );
	           	    startActivity(intent);
	            	
	            }
	        });
	        
	        
	        
		}
		
		//retornara un valor dependiendo del estado del empleado
		// 0 = no existe
		// 1 = existe pero esta asignado
		// 2 = existe y no esta asignado
		public int VerificarEmpleado(String empleado){
			int valor = 0;
			Cursor CursorDacceso =	db.getDacceso("CodigoEmpleado='" + empleado + "'");
			if(CursorDacceso != null  && CursorDacceso.getCount()>0){
				Cursor CursorGuardados =	db.getDguardados("CodigoEmpleado=" + empleado + " AND ActividadFinalizada=0");
				if(CursorGuardados.getCount()>0){
					valor = 1;
				}else{
					valor = 2;
				}
				CursorGuardados.close();
			}
			CursorDacceso.close();
			return valor;
		}

		
		public int VerificarSupervisor(String sup){
			int val = 0;
			 Cursor CursorUsuario = db.getUsuario("Usuario='" + sup + "'");
			 if(CursorUsuario.getCount()>0){
				 Usuario u = db.getUsuarioFromCursor(CursorUsuario, 0);
				 try {
				 Log.d("USUARIO",u.getUsuario() + ", "+u.getCodigoEmpleado());
				 } catch(Exception e) {
					 alertDialogMensaje("ERROR", "NO EXISTE USUARIO PARA ESTE SUPERVISOR");
				 }
				 Supervisa =  u.getCodigoEmpleado();
				 val = 1;
			} else {
				alertDialogMensaje("ERROR", "NO EXISTE USUARIO PARA ESTE SUPERVISOR");
			}
			 CursorUsuario.close();
			 return val;
		}
	
		
		public void alertDialogMensaje(String message1, String mesage2){
			
			//mVibrator.vibrate(300);
			
			try {
			    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
			    ringtone.play();
			    
			    
			} catch (Exception e) {
			    e.printStackTrace();
			}
			AlertDialog alertDialog = new AlertDialog.Builder(this).create();
			alertDialog.setTitle(message1);
			alertDialog.setMessage(mesage2);
			alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
			// here you can add functions
				ringtone.stop();
			}
			});
			alertDialog.show();
		}
		
		public void VerificarDatos(String dato){
			 Material = "";
			 Cursor CursorSemillas = db.getSemillas("Siembra='" + dato + "'");
			 if(CursorSemillas != null  && CursorSemillas.getCount()>0){
				 Semillas s = db.getSemillasFromCursor(CursorSemillas, 0);
				 Log.d("SEMILLA",s.getSiembra() + ", "+s.getMaterial() + " ," + s.getPolinizacion()); 
				 validoDescriptivo = true;
				 Material = s.getMaterial().toString().trim(); 
			 }else{
				 Material = "";
				 validoDescriptivo = false;
				//Etiqueta.requestFocus();
			 }
			 CursorSemillas.close();
		}
	
		public void VerificarMaterial(String dato){	 
			if(dato.equals(Material)) {	 	 				
				validoMaterial = true;
			 }else{
				 validoMaterial = false; 
			 }
		}
		
		public void LimpiarCampos(){
			 Etiqueta.setEnabled(true);
			 HProceso.setEnabled(true);
			 Bote1.setEnabled(true);
			 Bote2.setEnabled(true);
			 Molde.setEnabled(true);
			 Mueble.setEnabled(true);
			 Supervisor.setEnabled(true);
			 Etiqueta.setText("");
			 HProceso.setText("");
			 Bote1.setText("");
			 Bote2.setText("");
			 Molde.setText("");
			 Mueble.setText("");
			 empleado.setText("");
			 Supervisor.setText("");
			 Etiqueta.requestFocus();
		}
		
		public void LimpiarError(){
			 Etiqueta.setEnabled(false);
			 HProceso.setEnabled(true);
			 Bote1.setEnabled(true);
			 Bote2.setEnabled(true);
			 Molde.setEnabled(true);
			 Mueble.setEnabled(true);
			 Supervisor.setEnabled(true);
			 HProceso.setText("");
			 Bote1.setText("");
			 Bote2.setText("");
			 Molde.setText("");
			 Mueble.setText("");
			 empleado.setText("");
			 Supervisor.setText("");
			 HProceso.requestFocus();
		}
		
		public void GuardarDatos(){
			String empleadoEscaneado = empleado.getText().toString().trim();
	    	int existe = VerificarEmpleado(empleadoEscaneado);
	        
	    	if(existe == 2){
	        		Dguardados dtemp = new Dguardados();
	        		dtemp.setCodigoEmpleado(Integer.parseInt(empleadoEscaneado));
	        		dtemp.setCodigoDepto(Codigodepartamento);
	        		dtemp.setSiembra(Etiqueta.getText().toString().trim());
	        		
	        		
	        		//-----------------------------------------------------------------------------------
	        		String correlativo="";
	        		    	//almaceno el empleado en una variable string
	        		    	String getEmpleado = empleado.getText().toString().trim();
	        		    	// guardo la variable que tiene empleado en un array
	        		    	String[]empl = new String[]{getEmpleado};
	        		    	Cursor obtenerCorrelativo = db.getCorrelativo(empl);
	        		    	//recorro dicho arreglo para obtener el campo
	        		    	if (obtenerCorrelativo.moveToFirst()){
	        		    		do{
	        		    			correlativo = obtenerCorrelativo.getString(0);
	        		    		}while(obtenerCorrelativo.moveToNext());
	        		    	}
	        		//-------------------------------------------------------------------------
	        		Calendar c = Calendar.getInstance(); 
	        		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
	        		String fecha = fdate.format(c.getTime());
	        		SimpleDateFormat ftime = new SimpleDateFormat("HH:mm:ss");
	        		String horaInicio = ftime.format(c.getTime());
	      		
	        		dtemp.setFecha(fecha);
	        		dtemp.setHoraIncio(horaInicio);
	        		dtemp.setActividad(CodigoOpcion);
	        		dtemp.setSupervisor(CodigoEmpleado);
	        		dtemp.setAzucarera(HProceso.getText().toString().trim());
	        		dtemp.setCorrelativoBolsa(Integer.toString(Supervisa));
	        		dtemp.setActividadFinalizada(0);
	        		dtemp.setCorrelativoEmpleado(correlativo);
	        	        	
	        		Log.d("Dguardados", dtemp.toString().trim());
	        		
	        		db.insertarDguardados(dtemp);
	        		
	        		//generacion archivo json
	        		ObjectMapper mapper = new ObjectMapper();
	        		try {
	        			String nameJson ="LM_" + dtemp.getCodigoEmpleado() + "_"+dtemp.getFecha() + "_"+dtemp.getHoraIncio();
	        			nameJson=nameJson.replace(":", "-");	
						mapper.writeValue(new File( Environment.getExternalStorageDirectory()+"/SIM/JsonDescarga/" + nameJson+ ".json"), dtemp);
						
						JsonFiles archivo = new JsonFiles();
						archivo.setName(nameJson+ ".json");
						archivo.setNameFolder("SinFinalizar/");
						archivo.setUpload(0);
						db.insertarJsonFile(archivo);
						
						if(!appState.getSubiendoArchivos()){
							appState.setSubiendoArchivos(true);
							new UptoDropbox().execute(getApplicationContext());
						}
						
	        		
	        		} catch (JsonGenerationException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (JsonMappingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	        		
	        		
	        		LimpiarCampos();
	        		VerificarDatos(Etiqueta.getText().toString().trim());
	        		//TotalEmpleadosAsignados++;
	        		//TCantidadAsignados.setText(TotalEmpleadosAsignados + "");
	        		
	        	}else if(existe == 1){
	        		alertDialogMensaje("Asignado", "Este Empleado ya ha sido asignado");
	        		empleado.setText("");
	        		empleado.requestFocus();
	        		//VerificarDatos(descriptivo.getText().toString().trim());
	        	}else if(existe == 0){
	        		alertDialogMensaje("Marcaje", "No existe marcaje para este usuario");
	        		empleado.setText("");
	        		empleado.requestFocus();
	        		 //VerificarDatos(descriptivo.getText().toString().trim());	
	    	}
		}
	
		public void ValidaCampos(String vali){
			Material1 = "";
			if(vali.equals(Material1)){
			  confirma = false;
			}
			else{
				confirma = true;
			}
		}
		
		@Override
		public boolean onCreateOptionsMenu(Menu menu) {

			// Inflate the menu; this adds items to the action bar if it is present.
			getMenuInflater().inflate(R.menu.limpieza_manual, menu);
			return true;
		}

		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			// Handle action bar item clicks here. The action bar will
			// automatically handle clicks on the Home/Up button, so long
			// as you specify a parent activity in AndroidManifest.xml.
			int id = item.getItemId();
			if (id == R.id.action_settings) {
				return true;
			}
			return super.onOptionsItemSelected(item);
		}
}