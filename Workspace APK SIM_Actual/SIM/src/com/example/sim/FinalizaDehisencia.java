package com.example.sim;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.floricultura.R;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.Dguardados;
import com.example.sim.data.JsonFiles;
import com.example.sim.data.Opciones;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class FinalizaDehisencia extends ActionBarActivity {
	
	private MyApp appState;	
	private DBAdapter db;
	int CodigoModulo;
	int Codigodepartamento;
	int CodigoEmpleado;
	int CodigoOpcion;
	String NombreOpcion;
	EditText empleado;
	int finalizar;
	Ringtone ringtone;
	EditText EditPeso;
	EditText Sobre;
	EditText EditHora;
	EditText EditMin;
    

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_finaliza_dehisencia);
		
		Bundle extras = getIntent().getExtras();
		CodigoModulo = extras.getInt("CodigoModulo");
	    CodigoEmpleado = extras.getInt("CodigoEmpleado");
	    Codigodepartamento = extras.getInt("Codigodepartamento");
	    CodigoOpcion = extras.getInt("CodigoOpcion");
	    NombreOpcion = extras.getString("NombreOpcion");
	    
	    
	    TextView TNombreCaptura = (TextView)findViewById(R.id.OpcionCaptura);
	    TextView TCodigoEmpleado= (TextView)findViewById(R.id.CodigoEmpleado);
	    TextView TCodigoOpcion= (TextView)findViewById(R.id.codigoOpcion);
	    empleado  = (EditText)findViewById(R.id.editEmpleado);
	    Sobre = (EditText)findViewById(R.id.editSobre);
	    Button btnNuevo = (Button) findViewById(R.id.btnNuevo);
	    Button btnBack = (Button) findViewById(R.id.btnBack);
	    Button btnListado = (Button) findViewById(R.id.btnListado);
	    EditHora = (EditText)findViewById(R.id.editHora);
	    EditMin = (EditText)findViewById(R.id.editMin);
	    
	    TNombreCaptura.setText(NombreOpcion+ "");
	    TCodigoEmpleado.setText(CodigoEmpleado + "");
	    TCodigoOpcion.setText(CodigoOpcion + "");
	    

	    
	    //se valida la opcion finalizar actividad
	    
	    
	    appState = ((MyApp)getApplicationContext());   
	    db = appState.getDb();
	    db.open();
	    
	    Cursor CursorOpciones= null ;
	    CursorOpciones = db.getOpciones("Opcion=" + CodigoOpcion);
	    
	    if(CursorOpciones.getCount()>0){
	    	Opciones op = db.getOpcionesFromCursor(CursorOpciones, 0);
	    	finalizar = op.getFinalizaActividad();
	    }
	    CursorOpciones.close();
	    
			    //Se verfica el input de empleado al persionar enter
	    			Sobre.setOnKeyListener(new View.OnKeyListener() {
	    				public boolean onKey(View v, int keyCode, KeyEvent event) {
	    					// se manda a llamar cuando se presione Enter
	    					if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
	    							(keyCode == KeyEvent.KEYCODE_ENTER)) {
	    						Sobre.setEnabled(false);
	    						empleado.requestFocus();
	    						return true;     
	    					}
	    					return false;
	    				}   
	    			});
			    	
			    empleado.setOnKeyListener(new View.OnKeyListener() {
		            public boolean onKey(View v, int keyCode, KeyEvent event) {
		                // If the event is a key-down event on the "enter" button
		                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
		                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
		                	 	int verifica = 0;
		                	 	String sob = Sobre.getText().toString().trim();
		                	 	int emp = Integer.parseInt(empleado.getText().toString().trim());
		                		Cursor CursorGuardados =	db.getDguardados("CodigoEmpleado=" + Integer.parseInt(empleado.getText().toString().trim())+ " AND ActividadFinalizada=0" );
		                		if(CursorGuardados.getCount()>0){
		                			Dguardados dtemp =  (Dguardados)db.getDguardadosFromCursor(CursorGuardados, 0);
		                			verifica = Verificadatos(sob,emp);
		                			if(verifica == 1)
		                			{
		                			
		                				
		                				//-----------------------------------------------------------------------------------
		                				String correlativo="";
		                				    	//almaceno el empleado en una variable string
		                				    	String getEmpleado = empleado.getText().toString().trim();
		                				    	// guardo la variable que tiene empleado en un array
		                				    	String[]empl = new String[]{getEmpleado};
		                				    	Cursor obtenerCorrelativo = db.getCorrelativo(empl);
		                				    	//recorro dicho arreglo para obtener el campo
		                				    	if (obtenerCorrelativo.moveToFirst()){
		                				    		do{
		                				    			correlativo = obtenerCorrelativo.getString(0);
		                				    		}while(obtenerCorrelativo.moveToNext());
		                				    	}
		                				//-------------------------------------------------------------------------
		                    		Calendar c = Calendar.getInstance(); 
		                    		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
		                    		String fecha = fdate.format(c.getTime());
		                    		SimpleDateFormat ftime = new SimpleDateFormat("HH:mm:ss");
		                    		String horaFin = ftime.format(c.getTime());
		                    		
		                    		dtemp.setFecha(fecha);
		                			dtemp.setHoraFin(horaFin);
		                			dtemp.setSupervisor(CodigoEmpleado);
		                			dtemp.setActividadFinalizada(1);
		                			dtemp.setCorrelativoEmpleado(correlativo);
	              		                			
		                			db.updateDguardados(dtemp);
		                			
		                			empleado.setText("");
		                			Sobre.setText("");
		                			EditHora.setText("");
		                			EditMin.setText("");
		                			Sobre.requestFocus();
		                    		
		                    		ObjectMapper mapper = new ObjectMapper();
		                    		try {
		                    			String nameJson ="G_" + dtemp.getCodigoEmpleado() + "_"+dtemp.getFecha() + "_"+dtemp.getHoraFin();
		                    			nameJson=nameJson.replace(":", "-");
										mapper.writeValue(new File( Environment.getExternalStorageDirectory()+"/SIM/JsonDescarga/Finalizados/" + nameJson+ ".json"), dtemp);
										
										
										JsonFiles archivo = new JsonFiles();
										archivo.setName(nameJson+ ".json");
										archivo.setNameFolder("Finalizados/");
										archivo.setUpload(0);
										db.insertarJsonFile(archivo);
										
										if(!appState.getSubiendoArchivos()){
											appState.setSubiendoArchivos(true);
											new UptoDropboxFin().execute(getApplicationContext());
										}	
									} catch (JsonGenerationException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									} catch (JsonMappingException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									} catch (IOException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
		                			}else{
		                				alertDialogMensaje("ERROR", "SOBRE ESCANEADO NO COINCIDE \n CON EL ASIGNADO AL EMPLEADO \n ASEGURESE  QUE EL SOBRE SEA CORRECTO");
			                    		empleado.setText("");
			                    		Sobre.setText("");
			                    		Sobre.setEnabled(true);
			                    		Sobre.requestFocus();
		                			}	
		                	 }else{
		                	 alertDialogMensaje("Invalido", "Empleado Invalido o No Asignado");
		                	 empleado.setText("");
		                	 empleado.requestFocus();
		                	 }
		                CursorGuardados.close();
		                 return true;
		                }
		                return false;
		            }
		        });
	    
        
        btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
 
            	onBackPressed();
            }
        });
        
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	empleado.setText("");
    			Sobre.setText("");
    			Sobre.setEnabled(true);
    			EditHora.setText("");
    			EditMin.setText("");
    			Sobre.requestFocus();
            	
            }
        });
        
        btnListado.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Intent intent = new Intent(getApplicationContext(), LISTADOACTIVIDAD.class);
           	    intent.putExtra("CodigoEmpleado", CodigoEmpleado );
           	    startActivity(intent);
            	
            }
        });

		
	}
		
	public void hideSoftKeyboard() {
	    if(getCurrentFocus()!=null) {
	        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
	        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),   InputMethodManager.HIDE_NOT_ALWAYS);
	    }
	}
	
	//retornara un valor dependiendo del estado del empleado
			// 0 = no existe
			// 1 = existe pero esta asignado
			// 2 = existe y no esta asignado
	public int VerificarEmpleado(String empleado){
				int valor = 0;
				Cursor CursorDacceso =	db.getDacceso("CodigoEmpleado='" + empleado + "'");
				if(CursorDacceso != null  && CursorDacceso.getCount()>0){
					Cursor CursorGuardados =	db.getDguardados("CodigoEmpleado=" + empleado + " AND ActividadFinalizada=0");
					if(CursorGuardados.getCount()>0){
						valor = 1;
					}else{
						valor = 2;
					}
					CursorGuardados.close();
				}
				CursorDacceso.close();
				return valor;
			}

	public int Verificadatos (String sob, int emp){
		int val = 0;	
		Cursor CursorGuardados = db.getDguardados("CodigoEmpleado=" + emp + " AND Siembra= "+ sob );
		 if(CursorGuardados.getCount()>0){
			 val = 1;
		} else {
			val = 0;
		}
		 CursorGuardados.close();
		 return val;
	}
	
	public void alertDialogMensaje(String message1, String mesage2){
		
		
		try {
		    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
		    ringtone.play();
		    
		    
		} catch (Exception e) {
		    e.printStackTrace();
		}
		
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(message1);
		alertDialog.setMessage(mesage2);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int which) {
		// here you can add functions
			ringtone.stop();
		}
		});
		alertDialog.show();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.finalizar_actividad, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	
	public void ButtonOnClick(View v) {
	    switch (v.getId()) {
	      case R.id.buttonborra:
		        agregarNumero("1");
	        break;
	      case R.id.btnDos:
	    	  agregarNumero("2");
	        break;
	      case R.id.btnTres:
		        agregarNumero("3");
	        break;
	      case R.id.btnCuatro:
	    	  agregarNumero("4");
	        break;
	      case R.id.btnCinco:
		        agregarNumero("5");
	        break;
	      case R.id.btnSeis:
	    	  agregarNumero("6");
	        break;
	      case R.id.btnSiete:
	    	  agregarNumero("7");
	        break;
	      case R.id.btnOcho:
		        agregarNumero("8");
	        break;
	      case R.id.btnNueve:
	    	  agregarNumero("9");
	        break;
	      case R.id.button0:
		        agregarNumero("0");
	        break;
	      case R.id.btnCero:
	    	  agregarNumero("b");
	        break;
	      case R.id.Button11:
	    	  agregarNumero(".");
	        break;
	      }
	     
	}
	
	
	public void agregarNumero(String n){
		
		if(n.contains("b")){
			if(EditHora.isFocused()){
				EditHora.setText("");
			}else if(EditMin.isFocused()){
				EditMin.setText("");
			}
		}else{
			if(EditHora.isFocused()){
				EditHora.setText(EditHora.getText() + n);
			}else if(EditMin.isFocused()){
				EditMin.setText(EditMin.getText() + n);
			}
		}			
	}

}
	
