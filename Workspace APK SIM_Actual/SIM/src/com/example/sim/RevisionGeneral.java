package com.example.sim;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.floricultura.R;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.Dempleado;
import com.example.sim.data.Dguardados;
import com.example.sim.data.Dinvernaderos;
import com.example.sim.data.ERiego;
import com.example.sim.data.EtiquetasVegetativo;
import com.example.sim.data.JSONReader;
import com.example.sim.data.JsonFiles;
import com.example.sim.data.Opciones;
import com.example.sim.data.RangoEtiqueta;
import com.example.sim.data.SiembraVegetativo;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class RevisionGeneral extends ActionBarActivity {
	
	private MyApp appState;	
	private DBAdapter db;
	int CodigoModulo;
	int Codigodepartamento;
	int CodigoEmpleado;
	int CodigoOpcion;
	int EsCorte;
	int TotalEmpleadosAsignados;
	int TotalEtiquetasAsignadas;
	boolean validoInvernadero;
	boolean validoDescriptivo;
	int TipoOpcion;
	int EstadoEtiqueta = 0;
	int CantidadETQ;
	String Hoy;
	
	
	CountDownTimer tim = new CountDownTimer(45000, 1000) {
		
	     public void onTick(long millisUntilFinished){
	     }

	     public void onFinish() {
	         invernadero.setText("");
	         empleado.setText("");
	         etiqueta.setText("");
	         Siembra.setText("");
	         invernadero.setEnabled(true);
	         empleado.setEnabled(true);
	         etiqueta.setEnabled(true);
	         invernadero.requestFocus();
	     }
	  };
	String NombreOpcion;
	EditText empleado;
	EditText invernadero;
	EditText etiqueta;
	EditText Siembra;
	TextView textSiembra;
    TextView textInvernadero; 
	TextView TCantidadAsignados;
	Ringtone ringtone;
	RangoEtiqueta rangos;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_revision_general);
		
		
		Bundle extras = getIntent().getExtras();
		CodigoModulo = extras.getInt("CodigoModulo");
	    CodigoEmpleado = extras.getInt("CodigoEmpleado");
	    Codigodepartamento = extras.getInt("Codigodepartamento");
	    CodigoOpcion = extras.getInt("CodigoOpcion");
	    NombreOpcion = extras.getString("NombreOpcion");
	    EsCorte = extras.getInt("EsCorte");
	    
	    TextView TNombreCaptura = (TextView)findViewById(R.id.OpcionCaptura);
	    TextView TCodigoEmpleado= (TextView)findViewById(R.id.CodigoEmpleado);
	    TextView TCodigoOpcion= (TextView)findViewById(R.id.codigoOpcion);
	    TextView textViewEtiqueta= (TextView)findViewById(R.id.textViewEtiqueta);
	    textSiembra = (TextView)findViewById(R.id.textSiembra);
	    textInvernadero = (TextView)findViewById(R.id.textView20);
	    TCantidadAsignados = (TextView)findViewById(R.id.textEmpleadosAsginados);
	    empleado  = (EditText)findViewById(R.id.editEmpleado);
	    invernadero  = (EditText)findViewById(R.id.editComentario);
	    etiqueta  = (EditText)findViewById(R.id.editEtiqueta);
	    Siembra = (EditText)findViewById(R.id.editSiembra);
	    Button button = (Button) findViewById(R.id.btnNuevo);
	    Button invernaderoBtn = (Button) findViewById(R.id.btnAsistencia);
	    Button etiquetasBtn = (Button) findViewById(R.id.buttonEA);
	    
	    empleado.setInputType(InputType.TYPE_CLASS_NUMBER);
	    empleado.setSingleLine(false);
	    empleado.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
	    etiqueta.setInputType(InputType.TYPE_CLASS_NUMBER);   //Para Asignarle un tipo de dato 
	    etiqueta.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
	    etiqueta.setSingleLine(false);   // se utiliza para que el EditText no haga un Tab Automatico.
	    
	    appState = ((MyApp)getApplicationContext());   
	    db = appState.getDb();
	    db.open();
	    
	    Calendar c = Calendar.getInstance(); 
		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
		Hoy = fdate.format(c.getTime());
	    
	    rangos = new RangoEtiqueta();
	    //etiquetasBtn.setVisibility(View.INVISIBLE);
	    
	    TNombreCaptura.setText(NombreOpcion+ "");
	    TCodigoEmpleado.setText(CodigoEmpleado + "");
	    TCodigoOpcion.setText(CodigoOpcion + "");
	    //etiquetasBtn.setVisibility(View.INVISIBLE);
	    
	    TipoOpcion = ValidaTipo(CodigoOpcion);
	    obtenerRangoEtiqueta();
	    validoInvernadero = false;
	    verificarAsginados();
	    Siembra.setText("");
	    invernadero.setText("");
	    
	    
	    //******************************* devuelve valores para Activar Label y Edit Text dependiendo del Tipo *****************************************
	    
	    //if (CodigoOpcion == 13110 || CodigoOpcion == 16109 || CodigoOpcion == 17109 || CodigoOpcion == 11110 || CodigoOpcion == 12109) { 
	     
	   /* if(NombreOpcion.contains("ASIGNACION_ETIQUETAS")){
	    	etiquetasBtn.setVisibility(View.VISIBLE);
	    	EsCorte = 1;
	    	ObtenerAsignadas();
	    } else {
	    	etiquetasBtn.setVisibility(View.INVISIBLE);
	    } */
	    
	    if(NombreOpcion.startsWith("-")){
	    	etiquetasBtn.setVisibility(View.VISIBLE);
	    	EsCorte = 1;
	    	ObtenerAsignadas();
	    } else {
	    	etiquetasBtn.setVisibility(View.INVISIBLE);
	    }
	    
	    
	    if(TipoOpcion == 0 ) {  // TipoOpcion 0 se utiliza para escaneo Por siembra
	    	textInvernadero.setVisibility(View.INVISIBLE);
	    	invernadero.setVisibility(View.INVISIBLE);
	    	invernadero.setEnabled(false);
	    	Siembra.setText("");
	    	Siembra.requestFocus();
	    	invernaderoBtn.setText("Nueva Siembra");
	    } else {       // Si no es Tipo 0  Muestra el Invernadero y Oculta la Siembra
	    	textSiembra.setVisibility(View.INVISIBLE);
	    	Siembra.setVisibility(View.INVISIBLE);
	    	Siembra.setEnabled(false);
	    	invernadero.setText("");
	    	invernadero.requestFocus();
	    } 
	    
	    if(EsCorte == 0){   // Oculta el TextView y EditText de Etiqueta si no es Corte.
	    	etiqueta.setVisibility(View.INVISIBLE);
	    	textViewEtiqueta.setVisibility(View.INVISIBLE);    	
	    }
	    
	    
	    //se limpian los campos y variables para una nueva captura
	    invernaderoBtn.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	        	empleado.setText("");
	        	etiqueta.setText("");
	        	validoInvernadero = false;
	        	empleado.setEnabled(true);
	        	 if(TipoOpcion == 0 ) {
	     	    	textInvernadero.setVisibility(View.INVISIBLE);
	     	    	invernadero.setVisibility(View.INVISIBLE);
	     	    	invernadero.setEnabled(false);
	     	    	textSiembra.setVisibility(View.VISIBLE);
	     	    	Siembra.setVisibility(View.VISIBLE);
	     	    	Siembra.setEnabled(true);
	     	    	Siembra.setText("");
	     	    	Siembra.requestFocus();
	     	    } else {
	     	    	textSiembra.setVisibility(View.INVISIBLE);
	     	    	Siembra.setVisibility(View.INVISIBLE);
	     	    	Siembra.setEnabled(false);
	     	    	invernadero.setVisibility(View.VISIBLE);
	     	    	invernadero.setVisibility(View.VISIBLE);
	     	    	invernadero.setEnabled(true);
	     	    	invernadero.setText("");
	     	    	invernadero.requestFocus();
	     	    }    	
	        }
	    });
	      
	    //se limpian los campos y variables para una nueva captura Boton Nuevo
	    button.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	        	empleado.setText("");
	        	etiqueta.setText("");
	        	empleado.setEnabled(true);
	        	empleado.requestFocus();
	        	TCantidadAsignados.setText("0");
	        	contador();
	        }
	    });
	    
	    invernadero.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                	VerificarDatos(invernadero.getText().toString().trim());
                if (validoInvernadero == true){	
                	invernadero.setEnabled(false);
                	contador();
                	empleado.requestFocus();
                }else {
                	invernadero.setText("");
            		invernadero.requestFocus();
            		alertDialogMensaje("Mensaje", "Error Invernadero NO Existe");
                }
	             return true;
                }
                return false;
            }
        });
	    
	    
	    Siembra.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                	VerificaSiembra(Siembra.getText().toString().trim());
                if (validoDescriptivo == true){	
                	Siembra.setEnabled(false);
                	contador();
                	empleado.requestFocus();
                }else {
                	Siembra.setText("");
            		Siembra.requestFocus();
            		alertDialogMensaje("Mensaje", "Error Siembra NO Encontrada  \n  Descargar D_SiembrasVeg");
                }
	             return true;
                }
                return false;
            }
        });
	    
	    //al recibir el enter se verifica si el descriptivo es correcto y lo manda al siguiente input sino le muestra un mensaje al usuario
	    empleado.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                	String val = "";
                	String inv = invernadero.getText().toString().trim();
                	String sie = Siembra.getText().toString().trim();
                	if(TipoOpcion == 0 && sie.equals(val)) {
                			Siembra.setText("");
                			Siembra.requestFocus();
                			empleado.setText("");
                			alertDialogMensaje("Mensaje","Error Falta ingresar \n la Siembra"); 
                	} else if(TipoOpcion == 1 && inv.equals(val)){
                			invernadero.setText("");
                			invernadero.requestFocus();
                			empleado.setText("");
                			alertDialogMensaje("Mensaje","Error Falta ingresar \n el Invernadero"); 
                	} else {
                	String empleadoEscaneado = empleado.getText().toString().trim();
                	int existe = VerificarEmpleado( empleadoEscaneado );
                	if(EsCorte == 0){
	                	if(existe != 0){
	                		Dguardados dtemp = new Dguardados();
	                		dtemp.setCodigoEmpleado(Integer.parseInt(empleadoEscaneado));
	                		dtemp.setCodigoDepto(Codigodepartamento);
	                		
	                		//-----------------------------------------------------------------------------------
	                		String correlativo="";
	                		    	//almaceno el empleado en una variable string
	                		    	String getEmpleado = empleado.getText().toString().trim();
	                		    	// guardo la variable que tiene empleado en un array
	                		    	String[]empl = new String[]{getEmpleado};
	                		    	Cursor obtenerCorrelativo = db.getCorrelativo(empl);
	                		    	//recorro dicho arreglo para obtener el campo
	                		    	if (obtenerCorrelativo.moveToFirst()){
	                		    		do{
	                		    			correlativo = obtenerCorrelativo.getString(0);
	                		    		}while(obtenerCorrelativo.moveToNext());
	                		    	}
	                		//-------------------------------------------------------------------------
	                		
                    		Calendar c = Calendar.getInstance(); 
                    		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
                    		String fecha = fdate.format(c.getTime());
                    		SimpleDateFormat ftime = new SimpleDateFormat("HH:mm:ss");
                    		String horaInicio = ftime.format(c.getTime());
	                		
                    		dtemp.setFecha(fecha);
	                		dtemp.setHoraIncio(horaInicio);
	                		dtemp.setActividad(CodigoOpcion);
	                		dtemp.setSupervisor(CodigoEmpleado);
	                		dtemp.setActividadFinalizada(1);
	                		dtemp.setCorrelativoEmpleado(correlativo);
	                		
	                		if(TipoOpcion == 0 ) {
	                			dtemp.setSiembra(Siembra.getText().toString().trim());
	                			dtemp.setInvernadero(null);
	            		    } else {
	            		    	dtemp.setInvernadero(invernadero.getText().toString().trim());
	            		    	dtemp.setSiembra(null);
	            		    }
	                		
	                	
	                		Log.d("Dguardados", dtemp.toString().trim());
	                		
	                		db.insertarDguardados(dtemp);
	                		
 		
	                		//generacion archivo json
	                		ObjectMapper mapper = new ObjectMapper();
	                		try {
	                			String nameJson ="V_" + dtemp.getCodigoEmpleado() + "_"+dtemp.getFecha() + "_"+dtemp.getHoraIncio();
	                			nameJson=nameJson.replace(":", "-");	
								mapper.writeValue(new File( Environment.getExternalStorageDirectory()+"/SIM/JsonDescarga/" + nameJson+ ".json"), dtemp);
								
								JsonFiles archivo = new JsonFiles();
								archivo.setName(nameJson+ ".json");
								archivo.setNameFolder("SinFinalizar/");
								archivo.setUpload(0);
								db.insertarJsonFile(archivo);
								
								if(!appState.getSubiendoArchivos()){
									appState.setSubiendoArchivos(true);
									new UptoDropbox().execute(getApplicationContext());
								}
								
	                		
	                		} catch (JsonGenerationException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (JsonMappingException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
	                		
	                				
	                		TotalEmpleadosAsignados++;
	                		TCantidadAsignados.setText(TotalEmpleadosAsignados + "");
	                		
	                		
	                	    if(EsCorte == 1){
	                	    	//EtiquetasAsginadas();
	                	    	empleado.setEnabled(false);
	                	    	etiqueta.requestFocus();
	                	    	contador();
	                	    }else{
	                	    	empleado.requestFocus();
	                	    	empleado.setText("");
	                	    	contador();
	                	    }
	               		
	                	/*}else if(existe == 1){
	                		alertDialogMensaje("Asignado", "Este usuario ya ha sido asignado");
	                		empleado.setText("");
	                		empleado.requestFocus();
	                		contador();*/
	                	}else if(existe == 0){
	                		alertDialogMensaje("Marcaje", "No existe marcaje para este usuario");
	                		empleado.setText("");
	                		empleado.requestFocus();
	                		contador();
	                	}
		                
	                }else{
	                	if(existe == 0){
	                		empleado.setText("");
	                		alertDialogMensaje("Marcaje", "No existe marcaje para este usuario");
	                		empleado.setText("");
	                		contador();
	                		//empleado.requestFocus();
	                	}else{
	                		//agregado para contar etiquetas por empleado	
	                		empleado.setEnabled(false);            		
	                		etiqueta.requestFocus();
	                		contador();
	                	}
	                }
                	return true;
                	
               }
                	}  
                return false;
            }
        });
	    
	    
	    etiqueta.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                	String val = "";
                	String emp = empleado.getText().toString().trim();
                	if(emp.equals(val)){
                		etiqueta.setText("");
                		empleado.setText("");
                		empleado.setEnabled(true);
                		empleado.requestFocus();
                		alertDialogMensaje("ERROR","Error debe Ingresar \n un empleado");
                	} else {
                		if (ValidarEtiqueta() == true){ 
                			// Devuelve 0 si la etiqueta NO existe 
                			// Devuelve 1 si la etiqueta fue ingresada con mismo empleado y mismo invernadero
                			// Devuelve 2 si la etiqueta fue ingresada con distinto empleado o Invernadero
                			if(EstadoEtiqueta == 1){
                				alertDialogMensaje("Error Etiqueta", "Etiqueta ya ingresada en este Invernadero y mismo Empleado");
                				etiqueta.setText("");
                        		etiqueta.requestFocus();
                        		contador();
                			}else if(EstadoEtiqueta == 2){
                				int ETQ  = 0;
                				try{
                					String etqEscan =  etiqueta.getText().toString().trim();
                					ETQ = Integer.parseInt(etqEscan);
                					} catch(Exception e){
                						ETQ = 0;
                					}
                				Cursor CurEtiqueta = db.getEtiquetas("Etiqueta='" + ETQ + "' AND Fecha = '"+ Hoy +"'");
                				if(CurEtiqueta != null  && CurEtiqueta.getCount()>0){
                					EtiquetasVegetativo UPDATE =  db.getEtiquetasFromCursor(CurEtiqueta, 0);
       		                	 
		                    		Calendar c = Calendar.getInstance(); 
		                    		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
		                    		String fecha = fdate.format(c.getTime());
		                    		SimpleDateFormat ftime = new SimpleDateFormat("HH:mm:ss");
		                    		String hora = ftime.format(c.getTime());
		                    		
		                    		UPDATE.setFecha(fecha);
	    	                		UPDATE.setHora(hora);
	    	                		UPDATE.setSupervisor(CodigoEmpleado);
	    	                		UPDATE.setActividad(CodigoOpcion);
	    	                		String empleadoEscaneado = empleado.getText().toString().trim();
	    	                		UPDATE.setCodigoEmpleado(Integer.parseInt(empleadoEscaneado));
	    	                		UPDATE.setEtiqueta(etiqueta.getText().toString().trim());
	    	                		UPDATE.setFinalizado(1);
	    	                		UPDATE.setInvernadero(invernadero.getText().toString().trim());		
		                    		
		                			
		                			db.updateEtiquetas(UPDATE);
		                					                					                    		
		                			ObjectMapper mapper = new ObjectMapper();
	                        		mapper = new ObjectMapper();
	                        		try {
	                        			String nameJson = "E_" +empleadoEscaneado + "_"+UPDATE.getFecha() + "_"+UPDATE.getHora() + "_" + etiqueta.getText().toString().trim();
	                        			nameJson=nameJson.replace(":", "-");
	    								mapper.writeValue(new File( Environment.getExternalStorageDirectory()+"/SIM/JsonDescarga/" + nameJson+ ".json"), UPDATE);
	    								
	    								
	    								JsonFiles archivo = new JsonFiles();
	    								archivo.setName(nameJson+ ".json");
	    								archivo.setNameFolder("SinFinalizar/");
	    								archivo.setUpload(0);
	    								db.insertarJsonFile(archivo);
	    													
	    								if(!appState.getSubiendoArchivos()){
	    									appState.setSubiendoArchivos(true);
	    									new UptoDropbox().execute(getApplicationContext());
	    								}
	    								
									} catch (JsonGenerationException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									} catch (JsonMappingException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									} catch (IOException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
	                        		
	                        		etiqueta.setText("");
	                        		etiqueta.requestFocus();
	                        		contador();
	                        		ObtenerAsignadas();
	                        		
	                        		//agregado para contar etiquetas por empleado
	                        		//TotalEtiquetasAsignadas++;
	    	                		//TCantidadAsignados.setText(TotalEtiquetasAsignadas + "");
                				
                				}else{
                        			etiqueta.setText("");
                            		etiqueta.requestFocus();
                            		contador();   
                            		ObtenerAsignadas();
                        		}               
                				CurEtiqueta.close();	
                				
                			} else if(EstadoEtiqueta == 0){
                				EtiquetasVegetativo EVtemp = new EtiquetasVegetativo();
                        		
                        		String empleadoEscaneado = empleado.getText().toString().trim();
                        		EVtemp.setCodigoEmpleado(Integer.parseInt(empleadoEscaneado));
                        		EVtemp.setEtiqueta(etiqueta.getText().toString().trim());
                        		EVtemp.setFinalizado(1);
                        		EVtemp.setInvernadero(invernadero.getText().toString().trim());
                        		
                        		Calendar c = Calendar.getInstance(); 
                        		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
                        		String fecha = fdate.format(c.getTime());
                        		SimpleDateFormat ftime = new SimpleDateFormat("HH:mm:ss");
                        		String horaInicio = ftime.format(c.getTime());
    	                		
    	                		
    	                		EVtemp.setFecha(fecha);
    	                		EVtemp.setHora(horaInicio);
    	                		
    	                		//Agrege codigo de Supervisor y Actividad
    	                		EVtemp.setSupervisor(CodigoEmpleado);
    	                		EVtemp.setActividad(CodigoOpcion);
    	                		
                        		db.insertarEtiquetas(EVtemp);
    								                		
    	                		ObjectMapper mapper = new ObjectMapper();
                        		mapper = new ObjectMapper();
                        		try {
                        			String nameJson = "E_" +empleadoEscaneado + "_"+EVtemp.getFecha() + "_"+EVtemp.getHora() + "_"+etiqueta.getText().toString().trim();
                        			nameJson=nameJson.replace(":", "-");
    								mapper.writeValue(new File( Environment.getExternalStorageDirectory()+"/SIM/JsonDescarga/" + nameJson+ ".json"), EVtemp);
    								
    								
    								JsonFiles archivo = new JsonFiles();
    								archivo.setName(nameJson+ ".json");
    								archivo.setNameFolder("SinFinalizar/");
    								archivo.setUpload(0);
    								db.insertarJsonFile(archivo);
    													
    								if(!appState.getSubiendoArchivos()){
    									appState.setSubiendoArchivos(true);
    									new UptoDropbox().execute(getApplicationContext());
    								}
    								
    							} catch (JsonGenerationException e) {
    								// TODO Auto-generated catch block
    								e.printStackTrace();
    							} catch (JsonMappingException e) {
    								// TODO Auto-generated catch block
    								e.printStackTrace();
    							} catch (IOException e) {
    								// TODO Auto-generated catch block
    								e.printStackTrace();
    							}
    	                		
                        		etiqueta.setText("");
                        		etiqueta.requestFocus();
                        		ObtenerAsignadas();
                        		contador();
                        		
                        		//agregado para contar etiquetas por empleado
                        		//TotalEtiquetasAsignadas++;
    	                		//TCantidadAsignados.setText(TotalEtiquetasAsignadas + "");  
                			}
                			                    		
                		}else{
                			etiqueta.setText("");
                    		etiqueta.requestFocus();
                    		contador();                    		
                		}                		
                		//Finaliza codigo de validacion de Invernadero y Empleado               		
	                  return true;
                }
                	}
                return false;
            }
        });
	    
	  
	    // Prueba Boton Etiquetas 
	    // Comandos al Presionar el Boton Mostrar Listado
	    /*etiquetasBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	getListado();
            	//Creando Cuadro de Dialogo para Listado de Personas
        		class DialogoSeleccion extends DialogFragment {
        	        @Override
        	        public Dialog onCreateDialog(Bundle savedInstanceState) {
        	         	  
        	            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        	    
						builder.setTitle("Listado").setItems(data, new DialogInterface.OnClickListener() {
        	                    public void onClick(DialogInterface dialog, int item) {
        	                        Log.i("Dialogos", "Opci�n elegida: " + data[item]);        	                 
        	                    }
        	                });
        	            return builder.create(); }
        	    }
        		
        		FragmentManager fragmentManager = getSupportFragmentManager();
        		DialogoSeleccion dialogo = new DialogoSeleccion();
                dialogo.show(fragmentManager, "Alerta");
            }
            
        });*/
	    
	    // AGREGA BOTON  ETIQUETAS ASIGNADAS  
	    etiquetasBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Intent intent = new Intent(getApplicationContext(), LISTADOETIQUETAS.class);
           	    intent.putExtra("CodigoEmpleado", CodigoEmpleado );
           	    intent.putExtra("CodigoOpcion", CodigoOpcion);
           	    startActivity(intent);
            	
            }
        }); 
	    
        Button btnBack = (Button) findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
 
            	onBackPressed();
            }
        });
         
        Button btnInicio = (Button) findViewById(R.id.btnInicio);
        btnInicio.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Intent intent = new Intent(getApplicationContext(), MAINACTIVITY.class);
           	 	startActivity(intent);
           	 	finish();
            }
        });
	}
	
	public void MuestraCampos(){ 
		if(TipoOpcion == 0 ) {
		    	textInvernadero.setVisibility(View.INVISIBLE);
		    	invernadero.setVisibility(View.INVISIBLE);
		    	invernadero.setEnabled(false);
		    	
		    } else {
		    	textSiembra.setVisibility(View.INVISIBLE);
		    	Siembra.setVisibility(View.INVISIBLE);
		    	Siembra.setEnabled(false);
	    }
	}
	
	public void verificarAsginados(){
		Cursor CursorGuardados =	db.getDguardados("Actividad=" + CodigoOpcion + " AND ActividadFinalizada=0" );
		TotalEmpleadosAsignados = CursorGuardados.getCount();
		TCantidadAsignados.setText(TotalEmpleadosAsignados + "");
		CursorGuardados.close();
	}
	
	public void EtiquetasAsginadas(){
		//Cursor cursorEtiquetaG =	db.getEtiquetas("Actividad=" + CodigoOpcion + " AND CodigoEmpleado=" + empleado +"");
		Cursor cursorEtiquetaG =	db.getEtiquetas("Actividad");
		TotalEtiquetasAsignadas = cursorEtiquetaG.getCount();
		TCantidadAsignados.setText(TotalEtiquetasAsignadas + "");
		cursorEtiquetaG.close();
	}
		
	public void obtenerRangoEtiqueta(){
		
		JSONReader _reader = appState.getReader();
		ArrayList<RangoEtiqueta> r = (ArrayList<RangoEtiqueta>) ( _reader.ReadFromSDCard(6, "SIM/JsonCarga/D_Vegetativo.json"));
		if(r.size()>0){
			this.rangos = r.get(0);
		}
		
	}
			
	public void ObtenerAsignadas() {
		Cursor CurEtiqueta = db.getEtiquetas("Fecha = '"+ Hoy +"'");
		if(CurEtiqueta.getCount() > 0){
			CantidadETQ = CurEtiqueta.getCount();
			TCantidadAsignados.setText("  "+ CantidadETQ + " ");
		}else{
			CantidadETQ = 0;
		}
		CurEtiqueta.close();
	}
	
	
	public boolean ValidarEtiqueta(){
		boolean valido = false;
		int valorEtiqueta;
		String empleadoEscaneado = empleado.getText().toString().trim();
		int empelado  = Integer.parseInt(empleadoEscaneado);
		String invernaderoEscaneado = invernadero.getText().toString().trim();
		String etiquetaEscaneada = etiqueta.getText().toString().trim();
		try{
		 valorEtiqueta = Integer.parseInt(etiquetaEscaneada);
		 Log.d("valorEtiqueta",valorEtiqueta + "");
		 Log.d("Rangos Etiqueta", this.rangos.getInicio() + "  " + this.rangos.getFin());
		
		
		if(valorEtiqueta> this.rangos.getInicio() && valorEtiqueta<this.rangos.getFin()){
			valido = true;
			//se hace la consulta para validar si la etiqueta ya fue ingresada en el sistema
			// Devuelve 0 si la etiqueta NO existe 
			// Devuelve 1 si la etiqueta fue ingresada con mismo empleado y mismo invernadero
			// Devuelve 2 si la etiqueta fue ingresada con distinto empleado o Invernadero
			Cursor CurEtiqueta = db.getEtiquetas("Etiqueta='" + etiquetaEscaneada + "' AND Fecha = '"+ Hoy +"'");
			if(CurEtiqueta.getCount() > 0){
				Cursor cursorEtiqueta = db.getEtiquetas("CodigoEmpleado=" + empelado + " AND Etiqueta='" + etiquetaEscaneada + "' AND Invernadero='" + invernaderoEscaneado + "' AND Fecha = '"+ Hoy +"'");
				if(cursorEtiqueta.getCount() > 0 ){
					EstadoEtiqueta = 1;
				}else{
					EstadoEtiqueta = 2;
				}
				cursorEtiqueta.close();
			}else{
				EstadoEtiqueta = 0;
			}
			CurEtiqueta.close();
	    }else{
			alertDialogMensaje("Error Etiqueta", "Valor de Etiqueta no valido");
			valido = false;
		 }
		} catch(Exception e){
			 valorEtiqueta = 0;
			 alertDialogMensaje("Error Etiqueta", "Valor de Etiqueta no valido");
			 valido = false;
		 	 Log.d("valorEtiqueta",valorEtiqueta + " NO ES VALIDO");
		}
		
		contador();
		Log.d("Valido", "Valor Valido = " + valido);
		Log.d("ESTADO ETQ", "ESTADO ETQ = " + EstadoEtiqueta);
		return valido;
	}
	
	
	public int ValidaGuardarBD(String etiqueta){
	
		//retornara un valor dependiendo de la etiqueta
		// 0 = no existe en la Base de datos (etiqueta nueva)
		// 1 = Existe en la Base de datos (etiqueta asignada)
			int valor = 0;
			Cursor CursorComprueba =	db.getEtiquetas("Etiqueta='" + etiqueta + "'");
			if(CursorComprueba != null  && CursorComprueba.getCount()>0){
					valor = 1;
				}
			CursorComprueba.close();
		return valor;
	}
	
	//Funci�n para obtener el Tipo de Opci�n
	
	public int ValidaTipo(int codigo){
		int tipo = 0;
		Cursor CursorOp = db.getOpciones("Opcion='"+ codigo + "'");
		if(CursorOp != null && CursorOp.getCount()>0){
		Opciones Dato = this.db.getOpcionesFromCursor(CursorOp,0);
			tipo =  Dato.getTipo();
		}
		CursorOp.close();
		return tipo;
	}
	
	
	public void alertDialogMensaje(String message1, String mesage2){	
		try {
		    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
		    ringtone.play();	    	    
		} catch (Exception e) {
		    e.printStackTrace();
		}	
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(message1);
		alertDialog.setMessage(mesage2);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int which) {
		// here you can add functions
			ringtone.stop();
		}
		});
		alertDialog.show();
	}
	
	//retornara un valor dependiendo del estado del empleado
	// 0 = no existe
	// 1 = existe pero esta asignado
	// 2 = existe y no esta asignado
	
	public int VerificarEmpleado(String empleado){
		int valor = 0;
		Cursor CursorDacceso =	db.getDacceso("CodigoEmpleado='" + empleado + "'");
		if(CursorDacceso != null  && CursorDacceso.getCount()>0){
			Cursor CursorGuardados =	db.getDguardados("CodigoEmpleado=" + empleado + " AND ActividadFinalizada=0");
			if(CursorGuardados.getCount()>0){
				valor = 1;
			}else{
				valor = 2;
			}
			CursorGuardados.close();
		}
		CursorDacceso.close();
		contador();
		return valor;
	}
	
	
	public void VerificaSiembra(String dato){	
		 Cursor CursorSiembra = db.getSiembraVegetativo("Siembra='" + dato + "'");
		 if(CursorSiembra != null && CursorSiembra.getCount()>0){
			 SiembraVegetativo s = db.getSiembraVegetativoFromCursor(CursorSiembra, 0);
			 Log.d("SIEMBRAVEGETATIVO",s.getSiembra() + ", "+s.getVariedad() + " ," + s.getPlantas() + s.getInvernadero());
			 validoDescriptivo = true;
			 } else {
				 validoDescriptivo = false;
			 }
		 CursorSiembra.close();
	}
	
	public void VerificarDatos(String dato){
		Log.e("Andy", "Invernadero " + dato);
		validoInvernadero= false;
		 Cursor CursorInv = db.getDInvernaderos("Invernadero='" + dato + "'");
		 if(CursorInv.getCount()>0){
			 Dinvernaderos s = db.getDInvernaderosFromCursor(CursorInv, 0);
			 Log.d("INVERNADERO",s.getInvernadero());			 
			 validoInvernadero = true;
		 }else{
			 validoInvernadero = false;
		 }
		 CursorInv.close();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.captura_vegetativo, menu);
		return true;
	}
	
	public void contador() {
		  tim.cancel();
		  tim.start();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
