package com.example.sim;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.Vibrator;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.floricultura.R;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.DBHelper;

public class ConsultaEtiqueta extends ActionBarActivity {
	
	//Variables globales
		private SQLiteDatabase database;
		private DBHelper dbHelper;
		private MyApp appState;	
		private DBAdapter db;
		int CodigoModulo;
		int Codigodepartamento;
		int CodigoEmpleado;
		int CodigoOpcion;
		String NombreOpcion;
		GridView gridview;
		GridView gridEnc;
		TableLayout tablatest;
		Vibrator mVibrator;
		Ringtone ringtone;
		String Fechade;
		EditText Etiqueta;
		TextView Orden;
		TextView Inv;
		TextView Variedad;
		TextView Nombre;
		TextView Apellido;
		
		@SuppressLint("NewApi") @Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.consulta_etq);
			
			// para manejar la excepcion de permisos en el API de Android
	    	StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	        StrictMode.setThreadPolicy(policy);

			//se obtienen lo datos de la actividad antetior por medio del intent
			Bundle extras = getIntent().getExtras();
			CodigoModulo = extras.getInt("CodigoModulo");
		    CodigoEmpleado = extras.getInt("CodigoEmpleado");
		    Codigodepartamento = extras.getInt("Codigodepartamento");
		    CodigoOpcion = extras.getInt("CodigoOpcion");
		    NombreOpcion = extras.getString("NombreOpcion");
		    
		    //hacemos referencia a todos los objetos de nuestra vista
		    TextView TNombreCaptura = (TextView)findViewById(R.id.OpcionCaptura);
		    TextView TCodigoEmpleado= (TextView)findViewById(R.id.CodigoEmpleado);
		    TextView TCodigoOpcion= (TextView)findViewById(R.id.codigoOpcion);
	        Button btnBack = (Button) findViewById(R.id.btnBack);
	        Button btnOK = (Button) findViewById(R.id.btnOK);
	        TextView Fecha = (TextView) findViewById(R.id.texFecha);
	        Etiqueta = (EditText) findViewById(R.id.editEtiqueta);
	        Orden = (TextView) findViewById(R.id.textOrden);
	        Inv = (TextView) findViewById(R.id.textInvernadero);
	        Variedad = (TextView) findViewById(R.id.textVariedad);
	        Nombre = (TextView) findViewById(R.id.textNombre);
	        Apellido = (TextView) findViewById(R.id.textApellido);
	     	       
	        
		    //hacemos las inicializaciones necesarias a nuestra vista
		    TNombreCaptura.setText(NombreOpcion+ "");
		    TCodigoEmpleado.setText(CodigoEmpleado + "");
		    TCodigoOpcion.setText(CodigoOpcion + "");
		    Calendar c = Calendar.getInstance(); 
			SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
			String fecha = fdate.format(c.getTime());
			Fecha.setText(fecha);
			final String anio = fecha.substring(0, 4);
			
		    if (android.os.Build.VERSION.SDK_INT > 9)
	        {
	            StrictMode.ThreadPolicy poli = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	            StrictMode.setThreadPolicy(poli);
	        }
		    
		    //incializamos la BD asi como tambien obtenemos la referencia al singleton
		    appState = ((MyApp)getApplicationContext());   
		    db = appState.getDb();
		    db.open();
		    
		  		    
		    final String Pb = "";
        	btnBack.setOnClickListener(new View.OnClickListener() {
	            public void onClick(View v) {
	 
	            	onBackPressed();
	            }
	        });
        	
        	btnOK.setOnClickListener(new View.OnClickListener() {
 	            public void onClick(View v) {
        	String ETQ = null;
        	ETQ = Etiqueta.getText().toString().trim();
        	String PRB = "";
        	 Orden.setText(PRB);
			 Inv.setText(PRB);
			 Variedad.setText(PRB);
			 Nombre.setText(PRB);
			 Apellido.setText(PRB);
        	if (ETQ == PRB || ETQ.equals(null)){
        		alertDialogMensaje("ERROR","Debe Ingresar Una Etiqueta");
        		Etiqueta.setText("");
        		Etiqueta.requestFocus();
        	}else{    	
        		try {
            	String user = "sim";
            	String pasword = "sim";
            	 Connection connection = null;
            	 ResultSet rs = null;
            	 Statement statement = null;
            	 
    				Class.forName("com.ibm.as400.access.AS400JDBCDriver");
    				connection = DriverManager.getConnection("jdbc:as400://190.190.190.10", user, pasword);	
    				statement = connection.createStatement();			     
    				rs = statement.executeQuery("select ifnull(v.bardoc, '----') Orden, ifnull(e.invernadero,v.barinv) Inv, " +
    						"v.barvar Variedad, ifnull(m.nombre, ' NO ') Nombre, ifnull(m.apellido, 'ASIGNADA') Apellido from sigvegfgu.vege43 v " +
    						"left join (Select Invernadero,Etiqueta,codigo_empleado from sisappf.app_tmp_etiquetas " +
    						"where invernadero <> 'EMPAQU' and estado = 'A') e on v.barcns = e.etiqueta " +
    						"left join sisrrhhf.mempleado m on e.codigo_empleado = substring(m.empresa,2,1)||m.codigoempleado " +
    						"where v.barcns = '"+ETQ+"'  and v.barano >= '"+anio+"'");
    			
					while (rs.next()) {
						 String d1 = rs.getString(1).trim();
						 String d2 = rs.getString(2).trim();
						 String d3 = rs.getString(3).trim();
						 String d4 = rs.getString(4).trim();
					     String d5 = rs.getString(5).trim();
						 Orden.setText(d1);
						 Inv.setText(d2);
						 Variedad.setText(d3);
						 Nombre.setText(d4);
						 Apellido.setText(d5);
					}
					connection.close();	
           	}
        		catch (Exception e) {
				Toast toast = Toast.makeText(getApplicationContext(), "Verificar Conexi�n.....", Toast.LENGTH_SHORT);
            	toast.show();
				e.printStackTrace();
        		} 
        	}
        }
     });
}
		
		public void alertDialogMensaje(String message1, String mesage2){
		
			try {
			    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
			    ringtone.play();	    	    
			} catch (Exception e) {
			    e.printStackTrace();
			}
			AlertDialog alertDialog = new AlertDialog.Builder(this).create();
			alertDialog.setTitle(message1);
			alertDialog.setMessage(mesage2);
			alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
			// here you can add functions
				ringtone.stop();
			}
			});
			alertDialog.show();
		}
					
		@Override
		public boolean onCreateOptionsMenu(Menu menu) {

			// Inflate the menu; this adds items to the action bar if it is present.
			getMenuInflater().inflate(R.menu.soporteit, menu);
			return true;
		}

		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			// Handle action bar item clicks here. The action bar will
			// automatically handle clicks on the Home/Up button, so long
			// as you specify a parent activity in AndroidManifest.xml.
			int id = item.getItemId();
			if (id == R.id.action_settings) {
				return true;
			}
			return super.onOptionsItemSelected(item);
		}
	
		public void ButtonOnClick(View v) {
		    switch (v.getId()) {
		      case R.id.buttonborra:
			        agregarNumero("1");
		        break;
		      case R.id.btnDos:
		    	  agregarNumero("2");
		        break;
		      case R.id.btnTres:
			        agregarNumero("3");
		        break;
		      case R.id.btnCuatro:
		    	  agregarNumero("4");
		        break;
		      case R.id.btnCinco:
			        agregarNumero("5");
		        break;
		      case R.id.btnSeis:
		    	  agregarNumero("6");
		        break;
		      case R.id.btnSiete:
		    	  agregarNumero("7");
		        break;
		      case R.id.btnOcho:
			        agregarNumero("8");
		        break;
		      case R.id.btnNueve:
		    	  agregarNumero("9");
		        break;
		      case R.id.button0:
			        agregarNumero("0");
		        break;
		      case R.id.btnCero:
		    	  agregarNumero("b");
		        break;
		      }
		     
		}
		
		public void agregarNumero(String n){
			
			if(n.contains("b")){
				
				if(Etiqueta.isFocused()){
					Etiqueta.setText("");
					Orden.setText("");
					Inv.setText("");
					Variedad.setText("");
					Nombre.setText("");
					Apellido.setText("");
				}
				
			}else{
				if(Etiqueta.isFocused()){
					Etiqueta.setText(Etiqueta.getText() + n);
				}
			}
			

		}

}  