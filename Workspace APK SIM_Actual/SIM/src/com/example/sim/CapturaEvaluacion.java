package com.example.sim;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.Vibrator;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400Message;
import com.ibm.as400.access.CommandCall;
import com.ibm.as400.jtopenstubs.javabeans.PropertyVetoException;

import com.example.floricultura.R;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.Semillas;

@SuppressLint("NewApi") public class CapturaEvaluacion extends ActionBarActivity {
	
	//Variables globales
	private MyApp appState;	
	private DBAdapter db;
	int CodigoModulo;
	int Codigodepartamento;
	int CodigoEmpleado;
	int CodigoOpcion;
	int TotalEmpleadosAsignados;
	boolean validoDescriptivo;
	String NombreOpcion;
	TextView TCantidadAsignados;
	EditText descriptivo;
	Vibrator mVibrator;
	Ringtone ringtone;
	String[] datos = null;	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.captura_evaluacion);
	
		//se obtienen lo datos de la actividad antetior por medio del intent
		Bundle extras = getIntent().getExtras();
		CodigoModulo = extras.getInt("CodigoModulo");
	    CodigoEmpleado = extras.getInt("CodigoEmpleado");
	    Codigodepartamento = extras.getInt("Codigodepartamento");
	    CodigoOpcion = extras.getInt("CodigoOpcion");
	    NombreOpcion = extras.getString("NombreOpcion");
	   
	    //hacemos referencia a todos los objetos de nuestra vista
	    TextView TNombreCaptura = (TextView)findViewById(R.id.OpcionCaptura);
	    TextView TCodigoEmpleado= (TextView)findViewById(R.id.CodigoEmpleado);
	    final TextView TCodigoOpcion= (TextView)findViewById(R.id.codigoOpcion);
	    final TextView TextFecha = (TextView)findViewById(R.id.textFecha);
	    final TextView Textempleado = (TextView)findViewById(R.id.textNombre);
	    final TextView Textcodigoempleado = (TextView) findViewById(R.id.textCodigo);
	    
	    TCantidadAsignados = (TextView)findViewById(R.id.textEmpleadosAsginados);
	    descriptivo = (EditText)findViewById(R.id.editTextDescriptivo);
	    Button buttonNuevo = (Button) findViewById(R.id.btnNuevo);
        Button btnBack = (Button) findViewById(R.id.btnBack);
	    Button botonGuarda = (Button) findViewById(R.id.buttonGuarda);
	    Button botonFecha =  (Button) findViewById(R.id.botonFecha);
	    Button botonConsulta = (Button) findViewById(R.id.buttonsConsulta);
	       
	    //hacemos las inicializaciones necesarias a nuestra vista
	    TNombreCaptura.setText(NombreOpcion+ "");
	    TCodigoEmpleado.setText(CodigoEmpleado + "");
	    TCodigoOpcion.setText(CodigoOpcion + "");
	    Calendar c = Calendar.getInstance(); 
		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
		String fecha = fdate.format(c.getTime());
	    TextFecha.setText(fecha);
	    if (android.os.Build.VERSION.SDK_INT > 9)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
	    
	    
	  //incializa la BD, tambien obtenemos la referencia al singleton
	    appState = ((MyApp)getApplicationContext());   
	    db = appState.getDb();
	    db.open();
	    
	    // Comandos al Presionar el Boton Fecha
	    botonFecha.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Calendar mcurrentDate=Calendar.getInstance();
                int mYear = mcurrentDate.get(Calendar.YEAR);
                int mMonth = mcurrentDate.get(Calendar.MONTH);
                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker=new DatePickerDialog(CapturaEvaluacion.this, new OnDateSetListener() {                  
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                    	int datodia = selectedday;
                    	String dia = format(datodia);
                    	int datomes = selectedmonth + 1;
                    	String mes = format(datomes);
                    	String datoanio = Integer.toString(selectedyear);
                    	String dato = datoanio + "-" + mes + "-" +dia;
                    	TextFecha.setText(dato);
                    }

                }
                ,mYear, mMonth, mDay);
                mDatePicker.setTitle("Seleccionar Fecha de Evaluacion");                
                mDatePicker.show();  
            }
            
        });
	    
	    // Comandos al Presionar el Boton Atras
	    btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
 
            	onBackPressed();
            }
        
        });
	
	    // Comandos al recibir un valor en el TextView de Descriptivo seguidos de un "Enter"	
	    descriptivo.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // se llama cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                		VerificarDatos(descriptivo.getText().toString().trim());
	                	if (validoDescriptivo == true){
	                		descriptivo.setEnabled(false);
	                		String Opcion = TCodigoOpcion.getText().toString().trim();
	                		String Fecha = TextFecha.getText().toString().trim();
	                		String siembra = descriptivo.getText().toString().trim();
	                		int size = 0;
	                		
	                		try {
	                        	String user = "sim";
	                        	String pasword = "sim";
	                        	 Connection connection = null;
	                        	 ResultSet rs = null;
	                        	 ResultSet rd = null;
	                        	 Statement statement = null;
	                        	 Statement stat = null;
	                        	 
	            					Class.forName("com.ibm.as400.access.AS400JDBCDriver");
	            					connection = DriverManager.getConnection("jdbc:as400://190.190.190.10", user, pasword);	
	            					stat = connection.createStatement();
	            					rd = stat.executeQuery("select  count(distinct(g.codigoempleado)) from siscfgf.seguridad_opcion_moduloasp s " +
	            							"left join sisappf.app_tmp_general g  on  s.codmantenimiento = g.actividad " +
	            							"left join sisrrhhf.mempleado e on substring(g.codigoempleado,2,6) = e.codigoempleado " +
	            							"where s.opcion = '"+Opcion+"' and g.Fecha = '"+Fecha+"' and g.Siembra = '"+siembra+"'");
	            					
	            					while(rd.next()){
	            						size = rd.getInt(1);
	            					}
	         
	            					statement = connection.createStatement();			     
	            					rs = statement.executeQuery("select distinct(g.codigoempleado), ifnull(e.primernombre ||' '|| e.primerapellido, '---') nombre " +
	            							"from siscfgf.seguridad_opcion_moduloasp s left join sisappf.app_tmp_general g  on  s.codmantenimiento = g.actividad " +
	            							"left join sisrrhhf.mempleado e on substring(g.codigoempleado,2,6) = e.codigoempleado " +
	            							"where s.opcion = '"+Opcion+"' and g.Fecha = '"+Fecha+"' and g.Siembra = '"+siembra+"'");
	           
	            					
	            					datos = new String[size];
	            					int i = 0;
	            					while (rs.next()) {
	            						String tmp = null;
	            						tmp = rs.getString(1) + "  " + rs.getString(2);
	            						datos[i] = tmp;
	            						i++;
	            					}
	            					connection.close();	
	                        	}
	            				catch (Exception e) {
	            					Toast toast = Toast.makeText(getApplicationContext(), "Verificar Conexi�n.....", Toast.LENGTH_SHORT);
	            	            	toast.show();
	            					e.printStackTrace();
	                        } 
	                		               		   		
           					
    	                	//Creando Cuadro de Dialogo para Listado de Personas
    	                		class DialogoSeleccion extends DialogFragment {
    	                	        @Override
    	                	        public Dialog onCreateDialog(Bundle savedInstanceState) {
    	                	         	  
    	                	            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
    	                	    
    									builder.setTitle("Selecci�n").setItems(datos, new DialogInterface.OnClickListener() {
    	                	                    public void onClick(DialogInterface dialog, int item) {
    	                	                        Log.i("Dialogos", "Opci�n elegida: " + datos[item]);
    	                	                        Textempleado.setText(datos[item].substring(9));
    	                	                        Textcodigoempleado.setText(datos[item].substring(0,6));
    	                	                    }
    	                	                });
    	                	            return builder.create(); }
    	                	    }
    	                		
    	                		FragmentManager fragmentManager = getSupportFragmentManager();
    	                		DialogoSeleccion dialogo = new DialogoSeleccion();
    	                        dialogo.show(fragmentManager, "Alerta");
	                			
	                	}else{
		    	    		alertDialogMensaje("Desciptivo", "Numero de Siembra Invalido");
		    	    		descriptivo.setText("");
		    	    		descriptivo.requestFocus();
	                	}
	                  return true;
                }
                return false;
            }
        });
	
	      
	}
	
	public void alertDialogMensaje(String message1, String mesage2){
		
		//mVibrator.vibrate(300);
		
		try {
		    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
		    ringtone.play();
		    
		    
		} catch (Exception e) {
		    e.printStackTrace();
		}
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(message1);
		alertDialog.setMessage(mesage2);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int which) {
		// here you can add functions
			ringtone.stop();
		}
		});
		alertDialog.show();
	}
	
	
	public void VerificarDatos(String dato){	
		 Cursor CursorSemillas = db.getSemillas("Siembra='" + dato + "'");
		 if(CursorSemillas.getCount()>0){
			 Semillas s = db.getSemillasFromCursor(CursorSemillas, 0);
			 Log.d("SEMILLA",s.getSiembra() + ", "+s.getMaterial() + " ," + s.getPolinizacion());
			 validoDescriptivo = true;
			 } else {
				 validoDescriptivo = false;
			 }
		 CursorSemillas.close();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.captura_disponibilidad, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public String format (int f){
		String val = "";
		if(f<10){
		val = "0"+Integer.toString(f);	
		}
		else{
		val = Integer.toString(f);
		}
		return val;
	}
	
}