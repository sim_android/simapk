package com.example.sim;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Properties;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dropbox.core.DbxException;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.FileMetadata;
import com.example.floricultura.R;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.Dguardados;
import com.example.sim.data.JsonFiles;
import com.example.sim.data.Mdepartamento;
import com.example.sim.data.Usuario;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
//import com.example.floricultura.ListadoDescargas.DescargarDatos;
//import com.example.floricultura.ListadoDescargas.GuardarDatos;

 public class MAINACTIVITY extends ActionBarActivity {
	
	public MyApp appState;	
	public DBAdapter db;
	static Properties props;
	String usuario;
	String password;
	EditText Editusuario;
	EditText Editpassword;
	TextView mensaje ;
	String CodigoUsuario;
	int Cantidad;
	int CantidadArchivos;
	Ringtone ringtone;
	Button BotonInicio;
    Button BotonBorrar;
    Button BotonListado;
    Button BotonSincronizar;
    Button descarga;
	
	public ProgressDialog loadingDialog;
	public ProgressDialog loadingDialogDropbox;
	public final String DIR_APP = "/SIM/";
	String FILES_DIR = "/SIM/JsonDescarga/";
	
	private DbxClientV2 dbxClientV2;
	
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        appState = ((MyApp)getApplicationContext());   
      //IVAN: esta verificacion es necesaria por el token
		 if (!tokenExists()) {
			//No token
			//Back to LoginActivity
			Intent intent = new Intent(MAINACTIVITY.this, LOGINACTIVITY.class);
			startActivity(intent);
		} else {
			appState.initDropBoxV2(getToken());
			dbxClientV2 = appState.getDropBoxClient();
		}
        
        
        db = appState.getDb();
        db.open();
         
        
        int CountUsuarios = this.db.getUsuario(null).getCount();
        int Countdepartamentos = this.db.getMdepartamento(null).getCount();
        int CountModulos = this.db.getModulos(null).getCount();
        
        if (CountUsuarios==0 && Countdepartamentos==0 && CountModulos==0){
        	 Log.d("DATOS", "INSERTANDO DATOS");
        	// new GuardarDatos().execute();	
        }

        File mydir = new File(Environment.getExternalStorageDirectory() + "/SIM/JsonDescarga/Finalizados/");
        if(!mydir.exists()){
            mydir.mkdirs();
        }
        else{
            Log.d("error", "dir. already exists");
        }       
        
        File mydir2 = new File(Environment.getExternalStorageDirectory() + "/SIM/JsonCarga/");
        if(!mydir2.exists()){
            mydir2.mkdirs();
        }
        else{
            Log.d("error", "dir. already exists");
        }
        
        //se referencia el boton de la vista
        //descarga = (Button)findViewById(R.id.btnDesc);
        BotonInicio = (Button) findViewById(R.id.btnInicio); 
        BotonBorrar = (Button) findViewById(R.id.buttonborra);
        BotonListado = (Button) findViewById(R.id.cargarDatos);
        BotonSincronizar = (Button) findViewById(R.id.btnSincronizar);
        Editusuario = (EditText)findViewById(R.id.editTextUser);
        Editpassword = (EditText)findViewById(R.id.editTextPass);
        Editpassword.setSingleLine(false);
        mensaje = (TextView)findViewById(R.id.mensajesLogin);
        
        
        Editusuario.requestFocus();
        Editpassword.setEnabled(true);
                
        Editusuario.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                	Log.d("DATOS", Editusuario.getText() + "");
                	Editpassword.setEnabled(true);
                	Editpassword.requestFocus();
                  return true;
                }
                return false;
            }
        });
        
        Editpassword.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                	verificarUsuario();
                  return true;
                }
                return false;
            }
        });
        
       BotonInicio.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                verificarUsuario();
            }
        });
        
       BotonBorrar.setOnClickListener(new View.OnClickListener() {  	
            public void onClick(View v) {
            	final SharedPreferences preferencias=getSharedPreferences("FechaLogin",Context.MODE_PRIVATE);
            	String UltimaFechaLog = preferencias.getString("dia","");
            	
            	if(appState.connectionAvailable() == true){
        		ArrayList<JsonFiles> Test = getDatos();
        		
        		if(Cantidad > 0)
        		{
        			new SincronizarData().execute(getApplicationContext());
        		}else{
        			class DialogoConfirma extends DialogFragment{
                		@Override
                		public Dialog onCreateDialog(Bundle savedInstanceState) {
                				
                			LayoutInflater layoutInflater = LayoutInflater.from(MAINACTIVITY.this);
                			View promptView = layoutInflater.inflate(R.layout.dialogoborrar, null);
                			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MAINACTIVITY.this);
                			alertDialogBuilder.setView(promptView);

                			final EditText USER = (EditText) promptView.findViewById(R.id.username);
                			USER.setInputType(InputType.TYPE_CLASS_NUMBER);   //Para Asignarle un tipo de dato 
                			USER.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
                			USER.setSingleLine(false);   // se utiliza para que el EditText no haga un Tab Automatico.
                			// setup a dialog window
                			alertDialogBuilder.setCancelable(false)
                					.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                						public void onClick(DialogInterface dialog, int id) {           					
                							String User = USER.getText().toString().trim();
                							
                							if(User.equals("") || User.equals(null))
                							{
                								alertDialogMensaje("ERROR", "Se debe Escanear un USUARIO" );
                							}
                							else{
                							
                							
                							CodigoUsuario = USER.getText().toString().trim();
                							Cursor CursorUsuario =	db.getUsuario("Usuario = '"  + CodigoUsuario +"'");
                							if(CursorUsuario.getCount()>0){
                								Usuario usr = db.getUsuarioFromCursor(CursorUsuario, 0);
                								CodigoUsuario = ""+ usr.getCodigoEmpleado();
                								                								
                        		           		class DialogoConfirmacion extends DialogFragment {
                        		                    @Override
                        		                    public Dialog onCreateDialog(Bundle savedInstanceState) {

                        		                        AlertDialog.Builder builder =
                        		                        		new AlertDialog.Builder(getActivity());

                        		                        builder.setMessage("�Confirma Borrar Datos?")
                        		                        .setTitle("Confirmacion")
                        		                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener()  {
                        		                               public void onClick(DialogInterface dialog, int id) {
                        		                            	   Dguardados dtemp = new Dguardados();
                           	            		           		
                                           		           		Calendar c = Calendar.getInstance(); 
                                           		           		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
                                           		           		String fecha = fdate.format(c.getTime());
                                           		           		SimpleDateFormat ftime = new SimpleDateFormat("HH:mm:ss");
                                           		           		String horaInicio = ftime.format(c.getTime());
                                           		           		
                                           		           		dtemp.setCodigoDepto(null);
                                           		           		dtemp.setSiembra(null);
                                           		           		dtemp.setFlores(null);
                                           		           		dtemp.setFecha(fecha);
                                           		           		dtemp.setHoraIncio(horaInicio);
                                           		           		dtemp.setActividad(null);
                                           		           		dtemp.setSupervisor(Integer.parseInt(CodigoUsuario.trim()));
                                           		           		dtemp.setPlantas("" + Cantidad);
                                           		           		dtemp.setActividadFinalizada(1);
                                           		       	
                                           		           		Log.d("Dguardados", dtemp.toString().trim());
                                           		           		
                                           		           		//db.insertarDguardados(dtemp);
                                           		           		
                                           		           		//generacion archivo json
                                           		           		ObjectMapper mapper = new ObjectMapper();
                                           		           		try {
                                           		           			String nameJson ="BR_"+dtemp.getSupervisor()+"_"+dtemp.getFecha() + "_"+dtemp.getHoraIncio();
                                           		           			nameJson=nameJson.replace(":", "-");	
                                           		   					mapper.writeValue(new File( Environment.getExternalStorageDirectory()+"/SIM/JsonDescarga/" + nameJson+ ".json"), dtemp);
                                           		   					
                                           		   					JsonFiles archivo = new JsonFiles();
                                           		   					archivo.setName(nameJson+ ".json");
                                           		   					archivo.setNameFolder("Finalizados/");
                                           		   					archivo.setUpload(0);
                                           		   					db.insertarJsonFile(archivo);
                                           		   					
                                           		   					if(!appState.getSubiendoArchivos()){
                                           		   						appState.setSubiendoArchivos(true);
                                           		   						new UptoDropbox().execute(getApplicationContext());
                                           		   					}
                                           		   					
                                           		           		
                                           		           		} catch (JsonGenerationException e) {
                                           		   					// TODO Auto-generated catch block
                                           		   					e.printStackTrace();
                                           		   				} catch (JsonMappingException e) {
                                           		   					// TODO Auto-generated catch block
                                           		   					e.printStackTrace();
                                           		   				} catch (IOException e) {
                                           		   					// TODO Auto-generated catch block
                                           		   					e.printStackTrace();
                                           		   				}
                        		                            	   
                        		                            	   
                        		                            	   
                        		                            	//deleteWithChildren("/sdcard/SIM/JsonDescarga/Finalizados/");
                        		                            	deleteWithChildren("/sdcard/SIM/BK/");  //borra los archivos de JsonDescarga y la carpeta
                        		                               	Toast toast = Toast.makeText(getApplicationContext(), "Borrando Datos Espere.....", Toast.LENGTH_SHORT);
                        		                               	toast.show();
                        		                                                        		                               	
                        		                               	db.ReiniciarTabla(db.TIPO_DGUARDADOS);
                        		            	    		    db.ReiniciarTabla(db.TIPO_RIEGO);
                        		            	    		    db.ReiniciarTabla(db.TIPO_MANTENIMIENTO);
                        		            	    		    db.ReiniciarTabla(db.TIPO_INVESTIGACION);
                        		            	    			db.ReiniciarTabla(db.TIPO_JSONFILES);
                        		            	    			db.ReiniciarTabla(db.TIPO_ETIQUETAS);
                        		            	    			db.ReiniciarTabla(db.TIPO_DISPONSEM);
                        		            		            Editor editor = preferencias.edit();
                        		            		            editor.putString("dia", fecha);
                        		            		            editor.commit();
                        		            		            Context context = getApplicationContext();
                        		            		            CharSequence text = "Se ha reiniciado los datos para un nuevo dia";
                        		            		            int duration = Toast.LENGTH_LONG;
                        		            		            Toast tost = Toast.makeText(context, text, duration);
                        		            		            tost.show();
                        		                               	                               	
                        		            		            Log.i("Dialogos", "Confirmacion Aceptada.");
                        		                      			dialog.cancel();
                        		                      			CreaDirectorio(); // Crea la carpeta de JsonDescarga
                        		                                   }
                        		                               })
                        		                        .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        		                               public void onClick(DialogInterface dialog, int id) {
                        		                                	   	Log.i("Dialogos", "Confirmacion Cancelada.");
                        		                       					dialog.cancel();
                        		                                   }
                        		                               });

                        		                        return builder.create();
                        		                    }
                        		                }         	
                        		            	FragmentManager fragmentManager = getSupportFragmentManager();
                        		            	DialogoConfirmacion dialogo = new DialogoConfirmacion();
                        		                dialogo.show(fragmentManager, "Alerta");		
                        		           
                        		                                   		           		
                							}else{
                								alertDialogMensaje("ERROR","ERROR EMPLEADO NO EXISTE....");
                							}
                						  }
                						}
                					});
             
                		    return alertDialogBuilder.create();            		    
                		}
                			
                	}
                	FragmentManager fragmentManager = getSupportFragmentManager();
                	DialogoConfirma dialogo = new DialogoConfirma();
                    dialogo.show(fragmentManager, "Alerta");
        			
        			
        		}
        		            	
            }
            else{

            	
            	class DialogoConfirmacion extends DialogFragment {
                    @Override
                    public Dialog onCreateDialog(Bundle savedInstanceState) {

                        AlertDialog.Builder builder =
                        		new AlertDialog.Builder(getActivity());

                        builder.setMessage("        ERROR             \n\n        RED WIFI NO DISPONIBLE ")
                        .setTitle("ERROR")
                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener()  {
                               public void onClick(DialogInterface dialog, int id) {                           	                              	
                             	   	Log.i("Dialogos", "Confirmacion Aceptada.");
                      					dialog.cancel();
                                   }
                               });
                        return builder.create();
                    }
                }
            	FragmentManager fragmentManager = getSupportFragmentManager();
            	DialogoConfirmacion dialogo = new DialogoConfirmacion();
                dialogo.show(fragmentManager, "Alerta");	
                        
            }
           }
        });
  
       BotonListado.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {           	
            	Intent i = new Intent(getApplicationContext(), LISTADODESCARGAS.class);
 	            startActivity(i);            	    
            }
        });
             
        //se referencia el boton de la vista SINCRONIZAR DATOS
        
        BotonSincronizar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
        		
            	
            if(appState.connectionAvailable() == false){
            	
            	class DialogoConfirmacion extends DialogFragment {
                    @Override
                    public Dialog onCreateDialog(Bundle savedInstanceState) {

                        AlertDialog.Builder builder =
                        		new AlertDialog.Builder(getActivity());

                        builder.setMessage("        ERROR DE SINCRONIZACION \n\n        RED WIFI NO DISPONIBLE ")
                        .setTitle("ERROR")
                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener()  {
                               public void onClick(DialogInterface dialog, int id) {                           	                              	
                             	   	Log.i("Dialogos", "Confirmacion Aceptada.");
                      					dialog.cancel();
                                   }
                               });
                        return builder.create();
                    }
                }
            	FragmentManager fragmentManager = getSupportFragmentManager();
            	DialogoConfirmacion dialogo = new DialogoConfirmacion();
                dialogo.show(fragmentManager, "Alerta");	
            
            }else {
            	
            	new SincronizarDatos().execute(getApplicationContext());
          
            }	
            }   
       });
         
        appState.exportDatabse("SIM.db"); //exporta la base de datos a la SD card			
        	
    }
    
    private boolean tokenExists() {
		 SharedPreferences prefs = getSharedPreferences("com.sim.data", Context.MODE_PRIVATE);
		 String accessToken = prefs.getString("access-token", null);
		 return accessToken != null;
	 }

	private String getToken() {
		 SharedPreferences prefs = getSharedPreferences("com.sim.data", Context.MODE_PRIVATE);
		 return prefs.getString("access-token", null);
	 }
    
    
   //***************** Creado para borrar carpeta de JsonDescarga ***********************************
   public boolean deleteWithChildren(String path) {  
    File file = new File(path);  
    if (!file.exists()) {  
        return true;  
    }  
    if (!file.isDirectory()) {  
        return file.delete();  
    }  
    return this.deleteChildren(file) && file.delete();
   }  
  
   private boolean deleteChildren(File dir) {  
    File[] children = dir.listFiles();  
    boolean childrenDeleted = true;  
    for (int i = 0; children != null && i < children.length; i++) {  
        File child = children[i];  
        if (child.isDirectory()) {  
            childrenDeleted = this.deleteChildren(child) && childrenDeleted;  
        }  
        if (child.exists()) {  
            childrenDeleted = child.delete() && childrenDeleted;  
        }  
    }  
    return childrenDeleted;  
   }     
    
    public void CreaDirectorio() {
    	 File Old = new File(Environment.getExternalStorageDirectory() + "/SIM/JsonDescarga/"); 
    	 File New = new File(Environment.getExternalStorageDirectory() + "/SIM/BK/");
    	 Old.renameTo(New);
    	 File mydir = new File(Environment.getExternalStorageDirectory() + "/SIM/JsonDescarga/Finalizados/");
    	 if(!mydir.exists()){
             mydir.mkdirs();
         }
         else{
             Log.d("error", "dir. already exists");
         } 
    }
//************************* Fin de Codigo para borrar contenido de JsonDescarga ******************************    
       
    public void verificarUsuario(){
    	
        this.usuario= Editusuario.getText().toString().trim();
        this.password = Editpassword.getText().toString().trim();
    	
        Cursor CursorUsuario= null ;
        CursorUsuario =	this.db.getUsuario("Usuario=" +  "'" + this.usuario + "' AND Clave= '" +  this.password + "'");
        Log.d("cursor", CursorUsuario.toString().trim() +"   ---" + CursorUsuario.getCount());
        
        
        if(CursorUsuario.getCount()>0){
        	Usuario UsuarioLogeado = this.db.getUsuarioFromCursor(CursorUsuario,  0);
        	int Depto = UsuarioLogeado.getCodigoDepto();
        	Cursor Menu = this.db.getMdepartamento("CodigoDepto=" + Depto);
			Mdepartamento men = this.db.getMdepartamentoFromCursor(Menu, 0);
			String Inicio = men.getMenu();
			Class newclass = null;
			final Activity thisActivity = this;
			String activityString = "com.example.sim."+ Inicio;
			try {
			      newclass = Class.forName(activityString);
			    } catch (ClassNotFoundException c) {
			        c.printStackTrace();
			    }
        	Log.d("DATOS-USUSARIO", " " + UsuarioLogeado.toString().trim());
        	
        	
        	if(UsuarioLogeado.getTipo().equals("AD"))
        	{	      
	            Intent i = new Intent(thisActivity, DepartamentoActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
	            i.putExtra("CodigoEmpleado", UsuarioLogeado.getCodigoEmpleado());
	            i.putExtra("Administrador", 1);
	            i.putExtra("Codigofamilia", 0);
	            i.putExtra("UsuarioLogueado", UsuarioLogeado.getUsuario());
	            Editusuario.setText("");
	            Editpassword.setText("");
	            Editusuario.requestFocus();	            
	           // RevisarFecha();          
	            startActivity(i);

	            
        	}else{       		
        		Intent i = new Intent(thisActivity, newclass).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        		i.putExtra("CodigoEmpleado", UsuarioLogeado.getCodigoEmpleado());
        		i.putExtra("Administrador", 0);
        		i.putExtra("Codigofamilia", 0);
	            Editusuario.setText("");
	            Editpassword.setText("");
	            Editusuario.requestFocus();
	           // RevisarFecha();
	            startActivity(i);        		
        	}
        	Menu.close();
        }else{
        	
        	this.mensaje.setText("Datos incorrectos");
        	Editusuario.setText("");
        	Editusuario.requestFocus();
        	Editpassword.setText("");
        	
        }
        CursorUsuario.close();
    }
   
    /*
    public void RevisarFecha(){
    	
    	
    	SharedPreferences preferencias=getSharedPreferences("FechaLogin",Context.MODE_PRIVATE);
    	String UltimaFechaLog = preferencias.getString("dia","");
    	
        Calendar c = Calendar.getInstance(); 
		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
		String fecha = fdate.format(c.getTime());
    	
    	if(UltimaFechaLog.equals("")){
    		
            Editor editor=preferencias.edit();
            editor.putString("dia", fecha);
            editor.commit();
    		
    	}else{
    		 
    		 String hoy = fecha.substring(fecha.lastIndexOf("-")+1, fecha.length());
    		 String ultima = UltimaFechaLog.substring(UltimaFechaLog.lastIndexOf("-")+1, UltimaFechaLog.length());
    		 		 
    		 Log.d("fechas", "utlima " + ultima + "  hoy" + hoy);
    		 ArrayList<JsonFiles> FIL = getDatos();
    		 
    		 while(FIL.size()>0){
    			 if(appState.connectionAvailable() == false){
    	            	class DialogoConfirmacion extends DialogFragment {
    	                    @Override
    	                    public Dialog onCreateDialog(Bundle savedInstanceState) {
    	                    	AlertDialog.Builder builder =
    	                        		new AlertDialog.Builder(getActivity());
    	                        builder.setMessage("        ERROR DE SINCRONIZACION \n\n        RED WIFI NO DISPONIBLE ")
    	                        .setTitle("ERROR")
    	                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener()  {
    	                               public void onClick(DialogInterface dialog, int id) {                           	                              	
    	                             	   	Log.i("Dialogos", "Confirmacion Aceptada.");
    	                      					dialog.cancel();
    	                                   }
    	                               });
    	                        return builder.create();
    	                    }
    	                }
    	            	FragmentManager fragmentManager = getSupportFragmentManager();
    	            	DialogoConfirmacion dialogo = new DialogoConfirmacion();
    	                dialogo.show(fragmentManager, "Alerta");	
    	            
    	            }else {
    	            new SincronizarDatos().execute(getApplicationContext());
    	            }
    		 }
    		 
    		 if(!hoy.equals(ultima)){
    				if(!appState.getSubiendoArchivos()){
    					appState.setSubiendoArchivos(true);
    					appState.setMostrarDialog(true);
    					new UptoDropbox().execute(getApplicationContext());
    				}
    				/*
	    		    db.ReiniciarTabla(db.TIPO_DGUARDADOS);
	    		    db.ReiniciarTabla(db.TIPO_RIEGO);
	    		    db.ReiniciarTabla(db.TIPO_MANTENIMIENTO);
	    		    db.ReiniciarTabla(db.TIPO_INVESTIGACION);
	    			db.ReiniciarTabla(db.TIPO_JSONFILES);
	    			db.ReiniciarTabla(db.TIPO_ETIQUETAS);	 
		            Editor editor = preferencias.edit();
		            editor.putString("dia", fecha);
		            editor.commit();
		            Context context = getApplicationContext();
		            CharSequence text = "Se ha reiniciado los datos para un nuevo dia";
		            int duration = Toast.LENGTH_LONG;
		            Toast toast = Toast.makeText(context, text, duration);
		            toast.show();
		            
    		 	}
    		 }
    	}
    */
    
    
   
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
    public void insertarDatosdePrueaba(){
    	 /*
    	//USUARIOS
    	  Usuario u = new Usuario();
          u.setUsuario("usuario");
          u.setClave("123");
          u.setTipo("AD");
          u.setCodigoEmpleado(10);
          db.insertarUsuario(u);
          
          Usuario u2 = new Usuario();
          u2.setUsuario("usuario2");
          u2.setClave("123");
          u2.setTipo("OP");
          u2.setCodigoEmpleado(20);
          db.insertarUsuario(u2);
          
          
          //MEMPLEADO
          Mempleado m1 = new Mempleado(u.getCodigoEmpleado(),1 );
          db.insertarMempleado(m1);
          Mempleado m2 = new Mempleado(u2.getCodigoEmpleado(),2 );
          db.insertarMempleado(m2);

         //DEPARTAMENTOS 
        db.insertarMdepartamento(new Mdepartamaento(1,"Vegetativo", ""));
        db.insertarMdepartamento(new Mdepartamaento(2,"Semillas", ""));
        db.insertarMdepartamento(new Mdepartamaento(3,"Mantenimiento", ""));
        db.insertarMdepartamento(new Mdepartamaento(4,"Administracion", ""));
        db.insertarMdepartamento(new Mdepartamaento(5,"Recursos Humanos", ""));
        db.insertarMdepartamento(new Mdepartamaento(6,"Inv. y Desarollo", ""));
        db.insertarMdepartamento(new Mdepartamaento(7,"Finanzas", ""));
        db.insertarMdepartamento(new Mdepartamaento(8,"Sistemas", ""));
        
      
        
        //MODULOS
        /*
        db.insertarModulos(new Modulos(2,	201, 	"MADRES",	1,""));
        db.insertarModulos(new Modulos(2,	202,	"POLEN",	2,""));
        db.insertarModulos(new Modulos(2,	203,	"ACONDICIONAMIENTO",	3,""));
        db.insertarModulos(new Modulos(2,	204,	"RIEGO Y FUMIGACION",	4,""));
        db.insertarModulos(new Modulos(2,	205,	"PRODUCCION",	5,""));
        db.insertarModulos(new Modulos(1,	101,	"NUCLEOS",	1,""));
        db.insertarModulos(new Modulos(1,	102,	"INCREASE BLOCK",	2,""));
        db.insertarModulos(new Modulos(1,	103,	"PRODUCCION",	3,""));
        db.insertarModulos(new Modulos(1,	104,	"CALLOUS",	4,""));
        db.insertarModulos(new Modulos(1,	105,	"SALA EMPAQUE",	5,""));
        db.insertarModulos(new Modulos(1,	106,	"RIEGO Y FUMIGACION",	6,""));
        db.insertarModulos(new Modulos(2,	206,	"PROPAGADOR",	6,""));
        db.insertarModulos(new Modulos(8,	801,	"TEST",	1,""));
        
        */
        
        //OPCONES
       /* 
        db.insertarOpciones(new Opciones	(	201	,	21101	,"	EMASCULACIONES	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	3	,	1	)	);
        db.insertarOpciones(new Opciones	(	201	,	21102	,"	POLINIZACIONES	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	2	,	1	)	);
        db.insertarOpciones(new Opciones	(	201	,	21103	,"	COSECHAS	", "	~/SEED/Produccion/Mov_Cosecha.aspx	",	1	,	1	)	);
        db.insertarOpciones(new Opciones	(	201	,	21104	,"	LABORES CULTURALES	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	4	,	1	)	);
        db.insertarOpciones(new Opciones	(	201	,	21105	,"	DESECHO	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	5	,	1	)	);
        db.insertarOpciones(new Opciones	(	201	,	21106	,"	PLANTADO	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	6	,	1	)	);
        db.insertarOpciones(new Opciones	(	202	,	22101	,"	SUCCION	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	1	,	1	)	);
        db.insertarOpciones(new Opciones	(	202	,	22102	,"	LABORES CULTURALES	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	2	,	1	)	);
        db.insertarOpciones(new Opciones	(	202	,	22103	,"	DESECHO	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	3	,	1	)	);
        db.insertarOpciones(new Opciones	(	202	,	22104	,"	PLANTADO	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	4	,	1	)	);
        db.insertarOpciones(new Opciones	(	101	,	11101	,"	COLOCADO P/CALLOS	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	1	,	1	)	);
        db.insertarOpciones(new Opciones	(	101	,	11102	,"	PLANTADO	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	2	,	1	)	);
        db.insertarOpciones(new Opciones	(	101	,	11103	,"	DESPUNTES	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	3	,	1	)	);
        db.insertarOpciones(new Opciones	(	101	,	11104	,"	DESHOJADO	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	4	,	1	)	);
        db.insertarOpciones(new Opciones	(	101	,	11105	,"	CHEQUEO PUREZA	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	5	,	1	)	);
        db.insertarOpciones(new Opciones	(	101	,	11106	,"	CORTE	", "	~/Vegetativo/Mov_Asigna_Etq.aspx	",	6	,	2	)	);
        db.insertarOpciones(new Opciones	(	102	,	12101	,"	PLANTADO	", "	~/SeedAcond/Bolsa_Llena.aspx	",	1	,	1	)	);
        db.insertarOpciones(new Opciones	(	102	,	12102	,"	DESPUNTES	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	2	,	1	)	);
        db.insertarOpciones(new Opciones	(	102	,	12103	,"	DESHOJADO	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	3	,	1	)	);
        db.insertarOpciones(new Opciones	(	102	,	12104	,"	CHEQUEO PUREZA	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	4	,	1	)	);
        db.insertarOpciones(new Opciones	(	102	,	12105	,"	CORTE	", "	~/Vegetativo/Mov_Asigna_Etq.aspx	",	5	,	2	)	);
        db.insertarOpciones(new Opciones	(	103	,	13101	,"	PLANTADO	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	1	,	1	)	);
        db.insertarOpciones(new Opciones	(	103	,	13102	,"	DESPUNTE	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	2	,	1	)	);
        db.insertarOpciones(new Opciones	(	103	,	13103	,"	DESHOJADO	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	3	,	1	)	);
        db.insertarOpciones(new Opciones	(	103	,	13104	,"	CHEQUEO PUREZA	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	4	,	1	)	);
        db.insertarOpciones(new Opciones	(	103	,	13105	,"	CORTE	", "	~/Vegetativo/Mov_Asigna_Etq.aspx	",	5	,	2	)	);
        db.insertarOpciones(new Opciones	(	103	,	13106	,"	LIMPIEZA PLANTA	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	6	,	1	)	);
        db.insertarOpciones(new Opciones	(	103	,	13107	,"	DESPUNTE SELECTIVO	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	7	,	1	)	);
        db.insertarOpciones(new Opciones	(	103	,	13108	,"	CONTROL CALIDAD	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	8	,	1	)	);
        db.insertarOpciones(new Opciones	(	104	,	14101	,"	COLOCADO ESQUEJES	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	1	,	1	)	);
        db.insertarOpciones(new Opciones	(	104	,	14102	,"	LEVANTADO ESQUEJES	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	2	,	1	)	);
        db.insertarOpciones(new Opciones	(	105	,	15101	,"	CONTEO ESQ/BROTES	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	1	,	1	)	);
        db.insertarOpciones(new Opciones	(	105	,	15102	,"	ACTIVIDADES EMPAQUE	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	2	,	1	)	);
        db.insertarOpciones(new Opciones	(	105	,	15103	,"	CONTROL CALIDAD	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	3	,	1	)	);
        db.insertarOpciones(new Opciones	(	203	,	23101	,"	ENTREGA H_PROCESO	", "	~/SeedAcond/Entrega_HProceso.aspx	",	2	,	1	)	);
        db.insertarOpciones(new Opciones	(	203	,	23102	,"	UNION SEMILLA FRESCA	", "	~/SeedAcond/Mov_UBF.aspx	",	1	,	1	)	);
        db.insertarOpciones(new Opciones	(	203	,	23103	,"	INTEGRACION PACKING LIST	", "	~/SeedAcond/Mov_Integra_PackingList.aspx	",	3	,	1	)	);
        db.insertarOpciones(new Opciones	(	203	,	23104	,"	UNION EMPAQUE	", "	~/SeedAcond/Union_Pkempaque.aspx	",	4	,	1	)	);
        db.insertarOpciones(new Opciones	(	201	,	21107	,"	FINALIZA ACTIVIDAD	", "	~/Pantallas_generales/Mov_FinActividad_General.aspx	",	10	,	1	)	);
        db.insertarOpciones(new Opciones	(	202	,	22105	,"	FINALIZA ACTIVIDAD	", "	~/Pantallas_generales/Mov_FinActividad_General.aspx	",	5	,	0	)	);
        db.insertarOpciones(new Opciones	(	801	,	81101	,"	TEST	", "	~/IT/Test_2.aspx	",	1	,	1	)	);
        db.insertarOpciones(new Opciones	(	201	,	21108	,"	PODAS DE FORMACION	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	7	,	1	)	);
        db.insertarOpciones(new Opciones	(	201	,	21109	,"	ELIMINACION RAMOS O FLORES	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	8	,	1	)	);
        db.insertarOpciones(new Opciones	(	201	,	21110	,"	CAPADOS	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	9	,	1	)	);
        db.insertarOpciones(new Opciones	(	202	,	22106	,"	PODAS DE FORMACION	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	3	,	1	)	);
        db.insertarOpciones(new Opciones	(	202	,	22107	,"	ELIMINACION RAMOS O FLORES	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	4	,	1	)	);
        db.insertarOpciones(new Opciones	(	202	,	22108	,"	ROGUIN	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	5	,	1	)	);
        db.insertarOpciones(new Opciones	(	202	,	22109	,"	MORTALIDAD	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	6	,	1	)	);
        db.insertarOpciones(new Opciones	(	202	,	22110	,"	BORADO	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	7	,	1	)	);
        db.insertarOpciones(new Opciones	(	201	,	21111	,"	ROGUIN	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	10	,	1	)	);
        db.insertarOpciones(new Opciones	(	201	,	21112	,"	MORTALIDAD	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	11	,	1	)	);
        db.insertarOpciones(new Opciones	(	201	,	21113	,"	BOTADO	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	12	,	1	)	);
        db.insertarOpciones(new Opciones	(	103	,	13999	,"	FINALIZA ACTIVIDAD	", "	~/Pantallas_generales/Mov_FinActividad_General.aspx	",	999	,	0	)	);
        db.insertarOpciones(new Opciones	(	103	,	13109	,"	DESBOTONADO	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	9	,	1	)	);
        db.insertarOpciones(new Opciones	(	101	,	11999	,"	FINALIZA ACTIVIDAD	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	999	,	0	)	);
        db.insertarOpciones(new Opciones	(	102	,	12999	,"	FINALIZA ACTIVIDAD	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	999	,	0	)	);
        db.insertarOpciones(new Opciones	(	104	,	14999	,"	FINALIZA ACTIVIDAD	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	999	,	0	)	);
        db.insertarOpciones(new Opciones	(	105	,	15999	,"	FINALIZA ACTIVIDAD	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	999	,	0	)	);
        db.insertarOpciones(new Opciones	(	106	,	16999	,"	FINALIZA ACTIVIDAD	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	999	,	0	)	);
        
        db.insertarSemillas(new Semillas("20083991",	"96811",	0));
        db.insertarSemillas(new Semillas("20120569",	"761090",	1));
        */
    }
    
    public void alertDialogMensaje(String message1, String mesage2){	
		try {
    	    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
    	    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
    	    ringtone.play();
    	    
    	    
    	} catch (Exception e) {
    	    e.printStackTrace();
    	}
    	AlertDialog alertDialog = new AlertDialog.Builder(this).create();
    	alertDialog.setTitle(message1);
    	alertDialog.setMessage(mesage2);
    	alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
    	public void onClick(DialogInterface dialog, int which) {
    	// here you can add functions
    		ringtone.stop();
    	}
    	});
    	alertDialog.show();
    }
    
    private class SincronizarData extends AsyncTask<Context, Integer, Boolean> {   	
		private MyApp appState;	
		private DBAdapter db;
		
		protected void onPreExecute() {
			ArrayList<JsonFiles> prb = getDatos();
			//loadingDialog = new ProgressDialog(MAINACTIVITY.this);
			//loadingDialog.setMax(Cantidad);
			//loadingDialog.setTitle("PORFAVOR ESPERE");
			//loadingDialog.setMessage("Sincronizando Datos.....");
			//loadingDialog.show();
			loadingDialog = ProgressDialog.show(MAINACTIVITY.this,"", "Sincronizando Datos.....", true, false);
				        		        		        	
	        }
    	
		protected Boolean doInBackground(Context... params) {
        	
        	appState = ((MyApp)params[0]);   
            db = appState.getDb();
            db.open();

            ArrayList<JsonFiles> files = getDatos();
            
            if(appState.connectionAvailable()){
            	
    	        while(files.size()>0){    	
    	        	JsonFiles file = files.remove(0);
    	        	Log.d("SubiendoAAAA", file.getName()); 
    		    	File dir = Environment.getExternalStorageDirectory();
    			    File yourFile = new File(dir,"SIM/JsonDescarga/" + file.getName());
    		    	File OtherFile = new File(dir,"SIM/JsonDescarga/Finalizados/" + file.getName());
    		    	FileInputStream inputStream;
    		    	FileInputStream OtherinputStream;
    		    	
    		    	try {
    		    		if(yourFile.exists()){
    		    			
						String path = FILES_DIR + file.getNameFolder() + yourFile.getName();
						inputStream = new FileInputStream(yourFile);
						FileMetadata metadata = dbxClientV2.files().uploadBuilder(path)
								.uploadAndFinish(inputStream);
						
						file.setUpload(1);
						loadingDialog.incrementProgressBy(1);
						db.updateJsonFiles(file);
	
    		    		} else {
      		    			String path2 = FILES_DIR + file.getNameFolder() + OtherFile.getName();
							OtherinputStream = new FileInputStream(OtherFile);
							FileMetadata metadata = dbxClientV2.files().uploadBuilder(path2)
									.uploadAndFinish(OtherinputStream);
							loadingDialog.incrementProgressBy(1);
							file.setUpload(1);
							db.updateJsonFiles(file);
    		    		}
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (DbxException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
    		    	
     				files = getDatos();
    	        }
            	
            } 
            return true;
                	
        }
     
        protected void onPostExecute(Boolean result) {
        	if (result == true){
        		loadingDialog.dismiss();  
        	}
        }
        
       }
	    
    private class SincronizarDatos extends AsyncTask<Context, Integer, Boolean> {   	
		private MyApp appState;	
		private DBAdapter db;
		
		protected void onPreExecute() {
			ArrayList<JsonFiles> prb = getDatos();
			loadingDialog = new ProgressDialog(MAINACTIVITY.this);
			loadingDialog.setMax(Cantidad);
			CantidadArchivos = Cantidad;
			loadingDialog.setTitle("PORFAVOR ESPERE");
			loadingDialog.setMessage("Sincronizando Datos.....");
			loadingDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			loadingDialog.show();
			//loadingDialog = ProgressDialog.show(MainActivity.this,"", "Sincronizando Datos.....", true, false);
			
	        		        		        	
	        }
    	
		protected Boolean doInBackground(Context... params) {
        	
        	appState = ((MyApp)params[0]);   
            db = appState.getDb();
            db.open();

            ArrayList<JsonFiles> files = getDatos();
            
            if(appState.connectionAvailable()){
            	
    	        while(files.size()>0){    	
    	        	JsonFiles file = files.remove(0);
    	        	Log.d("SubiendoAAAA", file.getName()); 
    		    	File dir = Environment.getExternalStorageDirectory();
    			    File yourFile = new File(dir,"SIM/JsonDescarga/" + file.getName());
    		    	File OtherFile = new File(dir,"SIM/JsonDescarga/Finalizados/" + file.getName());
    		    	FileInputStream inputStream;
    		    	FileInputStream OtherinputStream;
    		    	
    		    	try {
    		    		if(yourFile.exists()){
    		    			
						String path = FILES_DIR + file.getNameFolder() + yourFile.getName();
						inputStream = new FileInputStream(yourFile);
						FileMetadata metadata = dbxClientV2.files().uploadBuilder(path)
								.uploadAndFinish(inputStream);
						
						file.setUpload(1);
						loadingDialog.incrementProgressBy(1);
						db.updateJsonFiles(file);
	
    		    		} else {
      		    			String path2 = FILES_DIR + file.getNameFolder() + OtherFile.getName();
							OtherinputStream = new FileInputStream(OtherFile);
							FileMetadata metadata = dbxClientV2.files().uploadBuilder(path2)
									.uploadAndFinish(OtherinputStream);
							loadingDialog.incrementProgressBy(1);
							file.setUpload(1);
							db.updateJsonFiles(file);
    		    		}
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (DbxException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
    		    	
     				files = getDatos();
    	        }
            	
            } 
            return true;
                	
        }
     
        protected void onPostExecute(Boolean result) {
        	if (result == true){
        		loadingDialog.dismiss();  
        		class DialogoConfirma extends DialogFragment{
            		@Override
            		public Dialog onCreateDialog(Bundle savedInstanceState) {
            				
            			LayoutInflater layoutInflater = LayoutInflater.from(MAINACTIVITY.this);
            			View promptView = layoutInflater.inflate(R.layout.dialogo, null);
            			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MAINACTIVITY.this);
            			alertDialogBuilder.setView(promptView);

            			final EditText USER = (EditText) promptView.findViewById(R.id.username);
            			// setup a dialog window
            			alertDialogBuilder.setCancelable(false)
            					.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            						public void onClick(DialogInterface dialog, int id) {           					
            							String User = USER.getText().toString().trim();
            							USER.setInputType(InputType.TYPE_CLASS_NUMBER);   //Para Asignarle un tipo de dato 
                            			USER.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
                            			USER.setSingleLine(false);   // se utiliza para que el EditText no haga un Tab Automatico.
                            			
            							if(User.equals("") || User.equals(null))
            							{
            								alertDialogMensaje("ERROR", "Se debe Escanear un USUARIO" );
            							}
            							else{
            							
            							
            							CodigoUsuario = USER.getText().toString().trim();
            							Cursor CursorUsuario =	db.getUsuario("Usuario = '" + CodigoUsuario +"'");
            							if(CursorUsuario.getCount()>0){
            								Usuario usr = db.getUsuarioFromCursor(CursorUsuario, 0);
            								CodigoUsuario = ""+usr.getCodigoEmpleado();
            								
            								Dguardados dtemp = new Dguardados();
    	            		           		
                    		           		Calendar c = Calendar.getInstance(); 
                    		           		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
                    		           		String fecha = fdate.format(c.getTime());
                    		           		SimpleDateFormat ftime = new SimpleDateFormat("HH:mm:ss");
                    		           		String horaInicio = ftime.format(c.getTime());
                    		           		
                    		           		dtemp.setCodigoDepto(null);
                    		           		dtemp.setSiembra(null);
                    		           		dtemp.setFlores(null);
                    		           		dtemp.setFecha(fecha);
                    		           		dtemp.setHoraIncio(horaInicio);
                    		           		dtemp.setActividad(null);
                    		           		dtemp.setSupervisor(Integer.parseInt(CodigoUsuario.trim()));
                    		           		dtemp.setPlantas("" + CantidadArchivos);
                    		           		dtemp.setActividadFinalizada(1);
                    		       	
                    		           		Log.d("Dguardados", dtemp.toString().trim());
                    		           		
                    		           		//db.insertarDguardados(dtemp);
                    		           		
                    		           		//generacion archivo json
                    		           		ObjectMapper mapper = new ObjectMapper();
                    		           		try {
                    		           			String nameJson ="Z_"+dtemp.getSupervisor()+"_"+dtemp.getFecha() + "_"+dtemp.getHoraIncio();
                    		           			nameJson=nameJson.replace(":", "-");	
                    		   					mapper.writeValue(new File( Environment.getExternalStorageDirectory()+"/SIM/JsonDescarga/" + nameJson+ ".json"), dtemp);
                    		   					
                    		   					JsonFiles archivo = new JsonFiles();
                    		   					archivo.setName(nameJson+ ".json");
                    		   					archivo.setNameFolder("Finalizados/");
                    		   					archivo.setUpload(0);
                    		   					db.insertarJsonFile(archivo);
                    		   					
                    		   					if(!appState.getSubiendoArchivos()){
                    		   						appState.setSubiendoArchivos(true);
                    		   						new UptoDropbox().execute(getApplicationContext());
                    		   					}
                    		   					
                    		           		
                    		           		} catch (JsonGenerationException e) {
                    		   					// TODO Auto-generated catch block
                    		   					e.printStackTrace();
                    		   				} catch (JsonMappingException e) {
                    		   					// TODO Auto-generated catch block
                    		   					e.printStackTrace();
                    		   				} catch (IOException e) {
                    		   					// TODO Auto-generated catch block
                    		   					e.printStackTrace();
                    		   				}
            								
            							}else{
            								alertDialogMensaje("ERROR","ERROR EMPLEADO NO EXISTE....");
            							}
            						  }
            						}
            					});
         
            		    return alertDialogBuilder.create();            		    
            		}
            			
            	}
            	FragmentManager fragmentManager = getSupportFragmentManager();
            	DialogoConfirma dialogo = new DialogoConfirma();
                dialogo.show(fragmentManager, "Alerta");
        		
        	}else{
        		alertDialogMensaje("ERROR","OCURRIO UN ERROR DE SINCRONIZACION FAVOR SINCRONIZAR DE NUEVO");
        		loadingDialog.dismiss();  
        	}       	
        	
        }
        
       }
    
	public ArrayList<JsonFiles> getDatos(){
	ArrayList<JsonFiles> Listtemp = new ArrayList<JsonFiles> ();
	 Cursor filesjson = db.getJsonFiles("Upload=0");
	 Cantidad = filesjson.getCount();
	 if(filesjson.getCount()>0){
		 for(int i=0 ; i<filesjson.getCount();i++){	
			 JsonFiles Dtemp = this.db.getJsonFilesFromCursor(filesjson, i);
			 Listtemp.add(Dtemp);
		 }
	 }
	 filesjson.close();
	 return Listtemp;
	}

}
