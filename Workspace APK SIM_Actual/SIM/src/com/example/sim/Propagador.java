package com.example.sim;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.floricultura.R;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.Dguardados;
import com.example.sim.data.JsonFiles;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Propagador extends ActionBarActivity {
	
	private MyApp appState;	
	private DBAdapter db;
	int CodigoModulo;
	int Codigodepartamento;
	int CodigoEmpleado;
	int CodigoOpcion;
	String NombreOpcion;
	EditText empleado;
	EditText etiqueta;
	Ringtone ringtone;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_propagador);
		
		
		Bundle extras = getIntent().getExtras();
		CodigoModulo = extras.getInt("CodigoModulo");
	    CodigoEmpleado = extras.getInt("CodigoEmpleado");
	    Codigodepartamento = extras.getInt("Codigodepartamento");
	    CodigoOpcion = extras.getInt("CodigoOpcion");
	    NombreOpcion = extras.getString("NombreOpcion");
	   
	    
	    TextView TNombreCaptura = (TextView)findViewById(R.id.OpcionCaptura);
	    TextView TCodigoEmpleado= (TextView)findViewById(R.id.CodigoEmpleado);
	    TextView TCodigoOpcion= (TextView)findViewById(R.id.codigoOpcion);
	    TextView textViewEtiqueta= (TextView)findViewById(R.id.textView2);
	    empleado  = (EditText)findViewById(R.id.editEmpleado);
	    etiqueta  = (EditText)findViewById(R.id.editEtiquet);
	    Button Nuevo = (Button) findViewById(R.id.btnNuevo);
	    
	    appState = ((MyApp)getApplicationContext());   
	    db = appState.getDb();
	    db.open();
	    
	    
	    TNombreCaptura.setText(NombreOpcion+ "");
	    TCodigoEmpleado.setText(CodigoEmpleado + "");
	    TCodigoOpcion.setText(CodigoOpcion + "");
	    
	 
	    
	    // CUANDO SE PULSA EL BOT�N NUEVO
	    Nuevo.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	        	etiqueta.setText("");
	        	empleado.setText("");
	        	etiqueta.requestFocus();	        	
	        }
	    });
	    
	    Button btnBack = (Button) findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
 
            	onBackPressed();
            }
        });
        
        
        Button btnInicio = (Button) findViewById(R.id.btnInicio);
        btnInicio.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Intent intent = new Intent(getApplicationContext(), MAINACTIVITY.class);
           	 	startActivity(intent);
           	 	finish();
            }
        });
	    
        etiqueta.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
         /*       	datos = editetiqueta.getText().toString().trim();
                	String prb = editetqmanual.getText().toString().trim();
                	if(prb.equals(datos)){
                		verificaEtiqueta(datos);
                		if (Validaetiqueta == 1){
                			//alertDialogMensaje("ETIQUETA", "BOLSA CORRECTA");
                			Supervisor.requestFocus();
                		}else {
                			alertDialogMensaje("ETIQUETA", "La Etiqueta es INCORRECTA");
                			Limpiar();
                		} 
                	}else{
                		alertDialogMensaje("ETIQUETA", "La Etiqueta Manual es INCORRECTA");
            			Limpiar();
                	}	
                	return true;      	
               */   }
              return false;
            }   
	    });
	    
	    empleado.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
              /*  	existe = VerificarSupervisor(Supervisor.getText().toString().trim());
                	if (existe == 1){
                		EditPeso.requestFocus();
                	} else {
                		//alertDialogMensaje("SUPERVISOR","EL SUPERVISOR ES INCORRECTO");
                		Limpiar();
                	}
                	return true;      	
            */    }
                return false;
            }   
	    });
	    
	}	  
	
	
//retornara un valor dependiendo del estado del empleado
// 0 = no existe
// 1 = existe pero esta asignado
// 2 = existe y no esta asignado
/*public int verificar (String empleado){
	int valor = 0;
	Cursor CursorDacceso =	db.getDacceso("CodigoEmpleado='" + empleado + "'");
	if(CursorDacceso != null  && CursorDacceso.getCount()>0){
		Cursor CursorGuardados =	db.getDguardados("CodigoEmpleado=" + empleado + " AND ActividadFinalizada=0");
		Dguardados Dat = db.getDguardadosFromCursor(CursorGuardados, 0);
		try{
		 Log.d("GUARDADO",Dat.getCodigoEmpleado() + ", "+Dat.getSiembra() + " ," + Dat.getAzucarera());
		}catch(Exception e) {
			alertDialogMensaje("ERROR", "ERROR NO SE ENCUENTRA EMPLEADO");
		}
		if(CursorGuardados != null  && CursorGuardados.getCount()>0){
			SiembraC = Dat.getSiembra().toString().trim();
			valor = 1;
		}else{
			valor = 2;
		}
	} else {
		alertDialogMensaje("Empleado","Error en el Empleado");
	}
	return valor;  
}*/

	
//retornara un valor dependiendo del estado del empleado
		// 0 = no existe
		// 1 = existe pero esta asignado
		// 2 = existe y no esta asignado
public int VerificarEmpleado(String empleado){
			int valor = 0;
			Cursor CursorDacceso =	db.getDacceso("CodigoEmpleado='" + empleado + "'");
			if(CursorDacceso != null  && CursorDacceso.getCount()>0){
				Cursor CursorGuardados =	db.getDguardados("CodigoEmpleado=" + empleado + " AND ActividadFinalizada=0");
				if(CursorGuardados.getCount()>0){
					valor = 1;
				}else{
					valor = 2;
				}
				CursorGuardados.close();
			}
			CursorDacceso.close();
			return valor;
	}


/*public int VerificarSupervisor(String sup){     // verfica si el codigo se encuentra en la tabla USUARIO y devuelve un valor
	int val = 0;
	 Cursor CursorUsuario = db.getUsuario("Usuario='" + sup + "'");
	 if(CursorUsuario.getCount()>0){
		 Usuario u = db.getUsuarioFromCursor(CursorUsuario, 0);
		 try {
		 Log.d("USUARIO",u.getUsuario() + ", "+u.getCodigoEmpleado());
		 } catch(Exception e) {
			 alertDialogMensaje("ERROR", "NO EXISTE USUARIO PARA ESTE SUPERVISOR");
		 }
		 Supervisa =  u.getCodigoEmpleado();     // Guarda el correlativo del Supervisor en la variable Supervisa
		 val = 1;
	} else {
		alertDialogMensaje("ERROR", "NO EXISTE USUARIO PARA ESTE SUPERVISOR");
	}
	 return val;
} */

/*public int verificaEtiqueta(String dato){
	if (dato.equals(SiembraC)){
		Validaetiqueta = 1;
	}else {
		 Validaetiqueta = 0;
	}	
	return Validaetiqueta;
}*/


public void Limpiar(){
	etiqueta.setText("");
	empleado.setText("");
	etiqueta.requestFocus();
}	

public void GuardarDatos(){
	String empleadoEscaneado = empleado.getText().toString().trim();
	int existe = VerificarEmpleado(empleadoEscaneado);
    
	if(existe == 2){
    		Dguardados dtemp = new Dguardados();
    		dtemp.setCodigoEmpleado(Integer.parseInt(empleadoEscaneado));
    		dtemp.setCodigoDepto(Codigodepartamento);
    		dtemp.setSiembra(etiqueta.getText().toString().trim());
    		
    		//-----------------------------------------------------------------------------------
    		String correlativo="";
    		    	//almaceno el empleado en una variable string
    		    	String getEmpleado = empleado.getText().toString().trim();
    		    	// guardo la variable que tiene empleado en un array
    		    	String[]empl = new String[]{getEmpleado};
    		    	Cursor obtenerCorrelativo = db.getCorrelativo(empl);
    		    	//recorro dicho arreglo para obtener el campo
    		    	if (obtenerCorrelativo.moveToFirst()){
    		    		do{
    		    			correlativo = obtenerCorrelativo.getString(0);
    		    		}while(obtenerCorrelativo.moveToNext());
    		    	}
    		//-------------------------------------------------------------------------
    		
    		Calendar c = Calendar.getInstance(); 
    		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
    		String fecha = fdate.format(c.getTime());
    		SimpleDateFormat ftime = new SimpleDateFormat("HH:mm:ss");
    		String horaInicio = ftime.format(c.getTime());
  		
    		dtemp.setFecha(fecha);
    		dtemp.setHoraIncio(horaInicio);
    		dtemp.setActividad(CodigoOpcion);
    		dtemp.setSupervisor(CodigoEmpleado);
    		//dtemp.setAzucarera(HProceso.getText().toString().trim());
    		//dtemp.setCorrerlativoBolsa(Integer.toString(Supervisa));
    		dtemp.setActividadFinalizada(0);
    		//dtemp.setCorrelativoEmpleado(correlativo);
    	        	
    		Log.d("Dguardados", dtemp.toString().trim());
    		
    		db.insertarDguardados(dtemp);
    		
    		//generacion archivo json
    		ObjectMapper mapper = new ObjectMapper();
    		try {
    			String nameJson ="G_" + dtemp.getCodigoEmpleado() + "_"+dtemp.getFecha() + "_"+dtemp.getHoraIncio();
    			nameJson=nameJson.replace(":", "-");	
				mapper.writeValue(new File( Environment.getExternalStorageDirectory()+"/SIM/JsonDescarga/" + nameJson+ ".json"), dtemp);
				
				JsonFiles archivo = new JsonFiles();
				archivo.setName(nameJson+ ".json");
				archivo.setNameFolder("SinFinalizar/");
				archivo.setUpload(0);
				db.insertarJsonFile(archivo);
				
				if(!appState.getSubiendoArchivos()){
					appState.setSubiendoArchivos(true);
					new UptoDropbox().execute(getApplicationContext());
				}
				
    		
    		} catch (JsonGenerationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		
    	}else if(existe == 1){
    		alertDialogMensaje("Asignado", "Este Empleado ya ha sido asignado");
    		empleado.setText("");
    		empleado.requestFocus();
    		//VerificarDatos(descriptivo.getText().toString().trim());
    	}else if(existe == 0){
    		alertDialogMensaje("Marcaje", "No existe marcaje para este usuario");
    		empleado.setText("");
    		empleado.requestFocus();
    		 //VerificarDatos(descriptivo.getText().toString().trim());	
	}
}


public void alertDialogMensaje(String message1, String mesage2){	
	try {
	    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
	    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
	    ringtone.play();
	    
	    
	} catch (Exception e) {
	    e.printStackTrace();
	}
	AlertDialog alertDialog = new AlertDialog.Builder(this).create();
	alertDialog.setTitle(message1);
	alertDialog.setMessage(mesage2);
	alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
	public void onClick(DialogInterface dialog, int which) {
	// here you can add functions
		ringtone.stop();
	}
	});
	alertDialog.show();
}


@Override
public boolean onCreateOptionsMenu(Menu menu) {

	// Inflate the menu; this adds items to the action bar if it is present.
	getMenuInflater().inflate(R.menu.captura_general, menu);
	return true;
}

@Override
public boolean onOptionsItemSelected(MenuItem item) {
	// Handle action bar item clicks here. The action bar will
	// automatically handle clicks on the Home/Up button, so long
	// as you specify a parent activity in AndroidManifest.xml.
	int id = item.getItemId();
	if (id == R.id.action_settings) {
		return true;
	}
	return super.onOptionsItemSelected(item);
}

}
