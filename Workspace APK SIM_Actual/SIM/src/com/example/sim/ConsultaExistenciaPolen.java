package com.example.sim;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.Vibrator;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.floricultura.R;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.Semillas;

public class ConsultaExistenciaPolen extends ActionBarActivity {
	
	//Variables globales
		private MyApp appState;	
		private DBAdapter db;
		int CodigoModulo;
		int Codigodepartamento;
		int CodigoEmpleado;
		int CodigoOpcion;
		long Pos;
		String Siembr;
		String Act;
		String Desc;
		String NombreOpcion;
		GridView gridview;
		GridView gridEnc;
		Vibrator mVibrator;
		Ringtone ringtone;
		String Fechade;
		EditText Siembra;
		String siem;
		String Inv;
		boolean validoDescriptivo;
		private String val;
		ArrayList<String> datos;
		ArrayAdapter<String> adapter;
		ArrayList<String> encabezado;
		ArrayAdapter<String> adapEnc;
		EditText editFechade;
		
		@SuppressLint("NewApi") @Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.consulta_existenciapolen);
			
			// para manejar la excepcion de permisos en el API de Android
	    	StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	        StrictMode.setThreadPolicy(policy);

			//se obtienen lo datos de la actividad antetior por medio del intent
			Bundle extras = getIntent().getExtras();
			CodigoModulo = extras.getInt("CodigoModulo");
		    CodigoEmpleado = extras.getInt("CodigoEmpleado");
		    Codigodepartamento = extras.getInt("Codigodepartamento");
		    CodigoOpcion = extras.getInt("CodigoOpcion");
		    NombreOpcion = extras.getString("NombreOpcion");
		    
		    //hacemos referencia a todos los objetos de nuestra vista
		    TextView TNombreCaptura = (TextView)findViewById(R.id.OpcionCaptura);
		    TextView TCodigoEmpleado= (TextView)findViewById(R.id.CodigoEmpleado);
		    TextView TCodigoOpcion= (TextView)findViewById(R.id.codigoOpcion);
	        Button btnBack = (Button) findViewById(R.id.btnBack);
	        Button btnOK = (Button) findViewById(R.id.btnAsistencia);
	        editFechade = (EditText) findViewById(R.id.editFechaDe);
	        Siembra = (EditText) findViewById(R.id.TextSiembra);
	        editFechade.setEnabled(false);
	        gridEnc = (GridView) findViewById(R.id.gridEnc);
	        gridview = (GridView) findViewById(R.id.gridDetalle);
	        
	        datos = new ArrayList<String>();
	        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, datos);
	        encabezado = new ArrayList<String>();
	        adapEnc = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, encabezado);
	        
		    //hacemos las inicializaciones necesarias a nuestra vista
		    TNombreCaptura.setText(NombreOpcion+ "");
		    TCodigoEmpleado.setText(CodigoEmpleado + "");
		    TCodigoOpcion.setText(CodigoOpcion + "");
		    
		    //incializamos la BD asi como tambien obtenemos la referencia al singleton
		    appState = ((MyApp)getApplicationContext());   
		    db = appState.getDb();
		    db.open();
		    Calendar c = Calendar.getInstance(); 
    		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
    		String fecha = fdate.format(c.getTime());
    		editFechade.setText(fecha);

		    btnBack.setOnClickListener(new View.OnClickListener() {
	            public void onClick(View v) {
	 
	            	onBackPressed();
	            }
	        });
		    
		  //al recibir el enter se verifica si el descriptivo es correcto y lo manda al siguiente input sino le muestra un mensaje al usuario
	        Siembra.setOnKeyListener(new View.OnKeyListener() {
	            public boolean onKey(View v, int keyCode, KeyEvent event) {
	                // se manda a llamar cuando se presione Enter
	                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
	                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
	                	String Material = VerificarDatos(Siembra.getText().toString().trim());
	                	if (validoDescriptivo == true){
		            	datos.clear();
		            	adapter.clear();
		            	encabezado.clear();
		            	adapEnc.clear();
		            	
		            	siem = Siembra.getText().toString().trim();
		            	Fechade = editFechade.getText().toString().trim();
          			            	
		            	try {
			            	String user = "sim";
			            	String pasword = "sim";
			            	String Fecha = editFechade.getText().toString().trim();
			            	 Connection connection = null;
			            	 ResultSet rs = null;
			            	 Statement statement = null;
			            	 
								Class.forName("com.ibm.as400.access.AS400JDBCDriver");
								connection = DriverManager.getConnection("jdbc:as400://190.190.190.10", user, pasword);	
								statement = connection.createStatement();			     
								rs = statement.executeQuery(
								"Select d.cuota,f.descripcionfase FASE,m.hibrido HIBRIDO,('"+Fecha+"' - max(s.soexfmov)) E_Min,('"+Fecha+"' - min(s.soexfmov)) E_Max,sum(s.soexexi) Existencia " +
								"from sispolf.sobreexiste s left join sissemf.mov_defsiembrae d on s.soexsie = d.nosiembra left join sissemf.mmaterialsem m on d.material = m.codigomaterial " +
								"left join sissemf.mant_fasesproduccion f on d.codigofase = f.codigofase where s.soexexi > 0 and soexempre > 0 " +
								"and soexbod = '01' and s.soexestado = 'A' and s.soexfmov > '2018-01-01' and m.codigomaterial = '"+Material+"' group by f.descripcionfase,m.hibrido,d.cuota order by d.cuota desc,m.hibrido,f.descripcionfase");
				
							    //Agregar encabezados de columnas	
								String Cn1 = rs.getMetaData().getColumnName(1);
								encabezado.add(Cn1);
								gridEnc.setAdapter(adapEnc);
								String Cn2 = rs.getMetaData().getColumnName(2);
								encabezado.add(Cn2);
								gridEnc.setAdapter(adapEnc);
								String Cn3 = rs.getMetaData().getColumnName(3);
								encabezado.add(Cn3);
								gridEnc.setAdapter(adapEnc);
								String Cn4 = rs.getMetaData().getColumnName(4);
								encabezado.add(Cn4);
								gridEnc.setAdapter(adapEnc);
								String Cn5 = rs.getMetaData().getColumnName(5);
								encabezado.add(Cn5);
								gridEnc.setAdapter(adapEnc);
								String Cn6 = rs.getMetaData().getColumnName(6);
								encabezado.add(Cn6);
								gridEnc.setAdapter(adapEnc);
								// finaliza agregar encabezados de columnas	
								while (rs.next()) {
								//agrega filas de columnas
									 String d1 = rs.getString(1).trim();
									 String d2 = rs.getString(2).trim();
									 String d3 = rs.getString(3).trim();
									 String d4 = rs.getString(4).trim();
								     String d5 = rs.getString(5).trim();
								     String d6 = rs.getString(6).trim();
								     String c1 = comprobar(d1);
									 String c2 = comprobar(d2);
									 String c3 = comprobar(d3);
									 String c4 = comprobar(d4);
									 String c5 = comprobar(d5);					
									 String c6 = comprobar(d6);
									 datos.add(c1);
									 gridview.setAdapter(adapter);
									 datos.add(c2);
									 gridview.setAdapter(adapter);
									 datos.add(c3);
									 gridview.setAdapter(adapter);
									 datos.add(c4);
									 gridview.setAdapter(adapter);
									 datos.add(c5);
									 gridview.setAdapter(adapter);
									 datos.add(c6);
									 gridview.setAdapter(adapter);
								}
								connection.close();	
			            	}
						catch (Exception e) {
							Toast toast = Toast.makeText(getApplicationContext(), "Verificar Conexi�n.....", Toast.LENGTH_SHORT);
			            	toast.show();
							e.printStackTrace();
						} 		            	   	
	              }else{
		    	    alertDialogMensaje("Descriptivo", "Descriptivo Invalido \n \n  Revisar Descriptivo");
		    	    Siembra.setText("");
		    	    Siembra.requestFocus();
	              }
	                return true;  
	               }     
	              return false;
	            }  
	        });
		    		    
		    btnOK.setOnClickListener(new View.OnClickListener() {
	            public void onClick(View v) {
	            	datos.clear();
	            	adapter.clear();
	            	encabezado.clear();
	            	adapEnc.clear();
	         
	            	Fechade = editFechade.getText().toString().trim();
	            	String Pb = "";
	            	
	            	if(Fechade.equals(Pb)){
	            		alertDialogMensaje("ERROR","PORFAVOR SELECCIONE UNA FECHA");
	            	}
	            	else 
	            	{
	            	try {
	            	String user = "sim";
	            	String pasword = "sim";
	            	String Fecha = editFechade.getText().toString().trim();
	            	 Connection connection = null;
	            	 ResultSet rs = null;
	            	 Statement statement = null;
	            	 
						Class.forName("com.ibm.as400.access.AS400JDBCDriver");
						connection = DriverManager.getConnection("jdbc:as400://190.190.190.10", user, pasword);	
						statement = connection.createStatement();			     
						rs = statement.executeQuery(
						"Select d.cuota,f.descripcionfase FASE,m.hibrido HIBRIDO,('"+Fecha+"' - max(s.soexfmov)) Edad_Min,('"+Fecha+"' - min(s.soexfmov)) Edad_Max,sum(s.soexexi) Existencia " +
						"from sispolf.sobreexiste s left join sissemf.mov_defsiembrae d on s.soexsie = d.nosiembra left join sissemf.mmaterialsem m on d.material = m.codigomaterial " +
						"left join sissemf.mant_fasesproduccion f on d.codigofase = f.codigofase where s.soexexi > 0 and soexempre > 0 " +
						"and soexbod = '01' and s.soexestado = 'A' and s.soexfmov > '2018-01-01' group by f.descripcionfase,m.hibrido,d.cuota order by d.cuota desc,m.hibrido,f.descripcionfase");
		
					    //Agregar encabezados de columnas	
						String Cn1 = rs.getMetaData().getColumnName(1);
						encabezado.add(Cn1);
						gridEnc.setAdapter(adapEnc);
						String Cn2 = rs.getMetaData().getColumnName(2);
						encabezado.add(Cn2);
						gridEnc.setAdapter(adapEnc);
						String Cn3 = rs.getMetaData().getColumnName(3);
						encabezado.add(Cn3);
						gridEnc.setAdapter(adapEnc);
						String Cn4 = rs.getMetaData().getColumnName(4);
						encabezado.add(Cn4);
						gridEnc.setAdapter(adapEnc);
						String Cn5 = rs.getMetaData().getColumnName(5);
						encabezado.add(Cn5);
						gridEnc.setAdapter(adapEnc);
						String Cn6 = rs.getMetaData().getColumnName(6);
						encabezado.add(Cn6);
						gridEnc.setAdapter(adapEnc);
						
						// finaliza agregar encabezados de columnas	
						while (rs.next()) {
						//agrega filas de columnas
							 String d1 = rs.getString(1).trim();
							 String d2 = rs.getString(2).trim();
							 String d3 = rs.getString(3).trim();
							 String d4 = rs.getString(4).trim();
						     String d5 = rs.getString(5).trim();
						     String d6 = rs.getString(6).trim();
						     String c1 = comprobar(d1);
							 String c2 = comprobar(d2);
							 String c3 = comprobar(d3);
							 String c4 = comprobar(d4);
							 String c5 = comprobar(d5);					
							 String c6 = comprobar(d6);
							 datos.add(c1);
							 gridview.setAdapter(adapter);
							 datos.add(c2);
							 gridview.setAdapter(adapter);
							 datos.add(c3);
							 gridview.setAdapter(adapter);
							 datos.add(c4);
							 gridview.setAdapter(adapter);
							 datos.add(c5);
							 gridview.setAdapter(adapter);
							 datos.add(c6);
							 gridview.setAdapter(adapter);
						}
						connection.close();	
	            	}
					catch (Exception e) {
						Toast toast = Toast.makeText(getApplicationContext(), "Verificar Conexi�n.....", Toast.LENGTH_SHORT);
		            	toast.show();
						e.printStackTrace();
	            } 
	           } 	
	            }
	        });
		    		    	      
		}
		
		public void alertDialogMensaje(String message1, String mesage2){
		
			try {
			    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
			    ringtone.play();	    	    
			} catch (Exception e) {
			    e.printStackTrace();
			}
			AlertDialog alertDialog = new AlertDialog.Builder(this).create();
			alertDialog.setTitle(message1);
			alertDialog.setMessage(mesage2);
			alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
			// here you can add functions
				ringtone.stop();
			}
			});
			alertDialog.show();
		}
		
		public String comprobar(String cad){
			String Pb = "";
			if(cad.equals(Pb)){
				cad = "---";
			}
			return cad;	
		}
		
		public String format (int f){
			val = "";
			if(f<10){
			val = "0"+Integer.toString(f);	
			}
			else{
			val = Integer.toString(f);
			}
			return val;
		}
		
		public String VerificarDatos(String dato){	
			String material = "";
			 Cursor CursorSemillas = db.getSemillas("Siembra='" + dato + "'");
			 if(CursorSemillas.getCount()>0){
				 Semillas s = db.getSemillasFromCursor(CursorSemillas, 0);
				 Log.d("SEMILLA",s.getSiembra() + ", "+s.getMaterial() + " ," + s.getPolinizacion());
				 validoDescriptivo = true;
				 material = s.getMaterial();
				 } else {
					 validoDescriptivo = false;
					 material = "";
				 }
			 CursorSemillas.close();
			return material;
		}
				
		@Override
		public boolean onCreateOptionsMenu(Menu menu) {

			// Inflate the menu; this adds items to the action bar if it is present.
			getMenuInflater().inflate(R.menu.soporteit, menu);
			return true;
		}

		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			// Handle action bar item clicks here. The action bar will
			// automatically handle clicks on the Home/Up button, so long
			// as you specify a parent activity in AndroidManifest.xml.
			int id = item.getItemId();
			if (id == R.id.action_settings) {
				return true;
			}
			return super.onOptionsItemSelected(item);
		}
	}  
