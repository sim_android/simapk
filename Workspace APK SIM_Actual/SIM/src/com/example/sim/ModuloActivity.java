package com.example.sim;

import java.util.ArrayList;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.example.floricultura.R;
import com.example.sim.adapters.DatosLista;
import com.example.sim.adapters.ListAdapter;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.Modulos;
import com.example.sim.data.Usuario;

public class ModuloActivity extends ActionBarActivity {
	
	private MyApp appState;	
	private DBAdapter db;
	ArrayList<DatosLista> listado = new ArrayList<DatosLista>();
	String id;
	int admin;
	int Codigodepartamento;
	int CodigoEmpleado;
	int CodigoFamilia = 0;
	String OPC = "";
	String UsuarioLogueado;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_modulo);
				
	    appState = ((MyApp)getApplicationContext());   
	    db = appState.getDb();
	    db.open();
		
		Bundle extras = getIntent().getExtras();
	    admin = extras.getInt("Administrador");
	    CodigoEmpleado = extras.getInt("CodigoEmpleado");
	    CodigoFamilia = extras.getInt("CodigoFamilia");
	    UsuarioLogueado = extras.getString("UsuarioLogueado");
	    Codigodepartamento = -1;
    
	    //es admin si es igual a 1
	    if(admin == 1){
	    	Codigodepartamento = extras.getInt("Codigodepartamento");
	    }else if (admin == 0){
	    	//se selecciona al usuario logeado de la tabla mempleado para ver a que modulo pertenece	    	
	    	Cursor usuario = this.db.getUsuario("CodigoEmpleado=" + CodigoEmpleado);
	    	Usuario utemp = this.db.getUsuarioFromCursor(usuario, 0);
	    	Codigodepartamento = utemp.getCodigoDepto();
	    	usuario.close();
	    }
	    
	    ListView listadoVista = (ListView)findViewById(R.id.ListModulo);
	    
	    if(Codigodepartamento == -1){
	    	listado.add(new DatosLista("0", "No existe departamento para este usuario", ""));
	    	ListAdapter adaptador = new ListAdapter(this, R.layout.filas_lista, listado);
	        listadoVista.setAdapter(adaptador);
	    	
	    }else{
	    	if(admin ==1){
	    	listado = getDataAdmin();
	    	}else{
			listado = getDatos();
			}
			ListAdapter adaptador = new ListAdapter(this, R.layout.filas_lista, listado);
	        listadoVista.setAdapter(adaptador);
				        
	        listadoVista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
	            @Override
	            public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {   
	            	DatosLista dato = (DatosLista) parent.getItemAtPosition(position);
	            	try {	
	            		Intent intent = new Intent(getApplicationContext(), OPCION.class);
		           	 	intent.putExtra("Codigodepartamento", Codigodepartamento);
		           	    intent.putExtra("CodigoEmpleado", CodigoEmpleado );
		           	    intent.putExtra("CodigoFamilia", CodigoFamilia);
		           	    intent.putExtra("CodigoModulo",  Integer.parseInt(dato.getCampo1().trim()));
		           	    intent.putExtra("UsuarioLogueado", UsuarioLogueado);
		           	 	startActivity(intent);
	            	}
	            	catch (Exception e) {
	        			e.printStackTrace();
	        		} 
	            }
	        });
        
	    }
	    
	    Button btnBack = (Button) findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
 
            	onBackPressed();
            }
        });
	    
        Button btnInicio = (Button) findViewById(R.id.btnInicio);
        btnInicio.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Intent intent = new Intent(getApplicationContext(), MAINACTIVITY.class);
           	 	startActivity(intent);
            }
        });
        

	}
		
	public ArrayList<DatosLista> getDatos(){
		ArrayList<DatosLista> listaTemp = new ArrayList<DatosLista>();
		listaTemp.add(new DatosLista("CD", "Descripcion", ""));
		
		 Cursor Usuario = this.db.getUsuario("CodigoEmpleado = " + CodigoEmpleado);
		 Usuario USR = this.db.getUsuarioFromCursor(Usuario, 0);
		 try{
		 OPC = USR.getCodigoAPP().toString().trim();
		 if(OPC.equals(null)){
			 OPC = "ALL";
		 }
		 }catch (Exception e) {
			OPC = "ALL";
 			e.printStackTrace();
 		} 
		 Cursor Cmodulos= null;
		 Cmodulos = this.db.getModulos("CodigoDepto = " + Codigodepartamento +" AND Familia = " + CodigoFamilia +" AND CodigoAPP IN('" + OPC + "', 'ALL')");
		 
		 if(Cmodulos.getCount()>0){
			 for(int i=0 ; i<Cmodulos.getCount();i++){	         	
				 Modulos m = db.getModulosFromCursor(Cmodulos, i);  				 
				 listaTemp.add(new DatosLista(m.getCodigoModulo()+ "", m.getDescripcion(), ""));			 
		     } 
		 }	
		 Cmodulos.close();
		 Usuario.close();
		return listaTemp;
	}

	public ArrayList<DatosLista> getDataAdmin(){
		ArrayList<DatosLista> listaTemp = new ArrayList<DatosLista>();
		listaTemp.add(new DatosLista("CD", "Descripcion", ""));
		
		 Cursor Cmodulos= null ;
		 Cmodulos = this.db.getModulos("CodigoDepto= " + Codigodepartamento +" AND Familia = " + CodigoFamilia );
		 
		 if(Cmodulos.getCount()>0){
			 for(int i=0 ; i<Cmodulos.getCount();i++){	         	
				 Modulos m = db.getModulosFromCursor(Cmodulos, i);  				 
				 listaTemp.add(new DatosLista(m.getCodigoModulo()+ "", m.getDescripcion(), ""));			 
		     } 
		 }	
		 Cmodulos.close();
		return listaTemp;
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.modulo, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}


}
