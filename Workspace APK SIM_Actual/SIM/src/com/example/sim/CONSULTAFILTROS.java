package com.example.sim;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.DialogInterface;
import android.graphics.Color;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.Vibrator;
import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.floricultura.R;
import com.example.sim.data.DBAdapter;

public class CONSULTAFILTROS extends ActionBarActivity {
	
	//Variables globales
		private MyApp appState;	
		private DBAdapter db;
		int CodigoModulo;
		int Codigodepartamento;
		int CodigoEmpleado;
		int CodigoOpcion;
		long Pos;
		String Act;
		String Ccosto;
		String NombreOpcion;
		GridView gridview;
		GridView gridEnc;
		
		Vibrator mVibrator;
		Ringtone ringtone;
		String Fechade;
		private String val;
		
		@SuppressLint("NewApi") @Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.activity_consultafiltros);
			
			// para manejar la excepcion de permisos en el API de Android
	    	StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	        StrictMode.setThreadPolicy(policy);

			//se obtienen lo datos de la actividad antetior por medio del intent
			Bundle extras = getIntent().getExtras();
			CodigoModulo = extras.getInt("CodigoModulo");
		    CodigoEmpleado = extras.getInt("CodigoEmpleado");
		    Codigodepartamento = extras.getInt("Codigodepartamento");
		    CodigoOpcion = extras.getInt("CodigoOpcion");
		    NombreOpcion = extras.getString("NombreOpcion");
		    Act = extras.getString("Act");
		    Ccosto = extras.getString("Ccosto");
		    Fechade = extras.getString("Fechade");
		    
		    //hacemos referencia a todos los objetos de nuestra vista
		    TextView TNombreCaptura = (TextView)findViewById(R.id.OpcionCaptura);
		    TextView TCodigoEmpleado= (TextView)findViewById(R.id.CodigoEmpleado);
		    TextView TCodigoOpcion= (TextView)findViewById(R.id.codigoOpcion);
	        Button btnBack = (Button) findViewById(R.id.btnBack);
	        Button btnOK = (Button) findViewById(R.id.btnAsistencia);
	        Button btnfechade = (Button) findViewById(R.id.buttonfechade);
	        final EditText editFechade = (EditText) findViewById(R.id.editFechaDe);
	        editFechade.setEnabled(false);
	        final TableLayout tablatest = (TableLayout) findViewById(R.id.TablaTest);
	        gridEnc = (GridView) findViewById(R.id.gridEnc);
	        gridview = (GridView) findViewById(R.id.gridDetalle);
	        
	        final ArrayList<String> datos = new ArrayList<String>();
	        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, datos);
	        final ArrayList<String> encabezado = new ArrayList<String>();
	        final ArrayAdapter<String> adapEnc = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, encabezado);
	        
		    //hacemos las inicializaciones necesarias a nuestra vista
		    TCodigoEmpleado.setText(CodigoEmpleado + "");
		  
		    
		    //incializamos la BD asi como tambien obtenemos la referencia al singleton
		    appState = ((MyApp)getApplicationContext());   
		    db = appState.getDb();
		    db.open();
		    
		
		    final String Pb = "";
        	final String es = " | ";
        	final String Fecha = editFechade.getText().toString().trim();
        	tablatest.removeAllViews();
        	
        	 btnBack.setOnClickListener(new View.OnClickListener() {
 	            public void onClick(View v) {
 	 
 	            	onBackPressed();
 	            }
 	        });
 		    
 		    btnfechade.setOnClickListener(new View.OnClickListener() {
 	            public void onClick(View v) {
 	            	Calendar mcurrentDate=Calendar.getInstance();
 	                int mYear = mcurrentDate.get(Calendar.YEAR);
 	                int mMonth = mcurrentDate.get(Calendar.MONTH);
 	                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

 	                DatePickerDialog mDatePicker=new DatePickerDialog(CONSULTAFILTROS.this, new OnDateSetListener() {                  
 	                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
 	                    	int datodia = selectedday;
 	                    	String dia = format(datodia);
 	                    	int datomes = selectedmonth + 1;
 	                    	String mes = format(datomes);
 	                    	String datoanio = Integer.toString(selectedyear);
 	                    	String dato = datoanio + "-" + mes + "-" +dia;
 	                    	editFechade.setText(dato);
 	                    }
 	                }
 	                ,mYear, mMonth, mDay);
 	                mDatePicker.setTitle("Seleccionar Fecha");                
 	                mDatePicker.show();  
 	            }
 	            
 	        });
 	    
 		    btnOK.setOnClickListener(new View.OnClickListener() {
 	            public void onClick(View v) {
 	            	datos.clear();
 	            	adapter.clear();
 	            	encabezado.clear();
 	            	adapEnc.clear();
 	            	tablatest.removeAllViews();
 	            	Fechade = editFechade.getText().toString().trim();
 	            	String Pb = "";
 	            	
 	            	if(Fechade.equals(Pb)){
 	            		alertDialogMensaje("ERROR","PORFAVOR SELECCIONE UNA FECHA");
 	            	}
 	            	else 
 	            	{
 	            	try {
 	            	
 	            	String user = "sim";
 	            	String pasword = "sim";
 	            	 Connection connection = null;
 	            	 ResultSet rs = null;
 	            	 Statement statement = null;
 	            	 String CodigoEmpleados = "283";
 						Class.forName("com.ibm.as400.access.AS400JDBCDriver");
 						connection = DriverManager.getConnection("jdbc:as400://190.190.190.10", user, pasword);	
 						statement = connection.createStatement();		
 						System.setProperty("http.keepAlive", "false");
 						rs = statement.executeQuery("select s.FechaSuccion, s.Siembra, s.Filtros Solicitados,ifnull(e.enviados,0) Enviados,ifnull(r.pesados,0) Pesados from sisappf.app_solicitudFiltros s LEFT JOIN (Select d.siembra,count(d.id)Enviados from sissemf.mov_printfiltrosd d inner join sissemf.mov_printfiltrose e on d.correlativo = e.correlativo where e.fechasuccion = '"+Fechade+"' and d.estado in ('BE','BR')group by d.siembra,d.estado) E on s.siembra = e.siembra LEFT JOIN (Select d.siembra,count(d.id) Pesados from sissemf.mov_printfiltrosd d inner join sissemf.mov_printfiltrose e on d.correlativo = e.correlativo where e.fechasuccion = '"+Fechade+"'  and d.estado = 'BR' group by d.siembra,d.estado) r on s.siembra = r.siembra where s.FechaSuccion = '"+Fechade+"'  and s.CorrelativoContador = '"+CodigoEmpleado+"'");
 						if(!rs.isBeforeFirst()){
 							alertDialogMensaje("Mensaje","No Existen Datos con esta Fecha: "+Fechade);
 						}
 						else
 						{
 					    //Agregar encabezados de columnas	
 						 TableRow Erow1 = new TableRow(CONSULTAFILTROS.this);
 						 TextView E1 = new TextView(CONSULTAFILTROS.this);
 						 TextView E2 = new TextView(CONSULTAFILTROS.this);
 						 TextView E3 = new TextView(CONSULTAFILTROS.this);
 						 TextView E4 = new TextView(CONSULTAFILTROS.this);
 						 TextView E5 = new TextView(CONSULTAFILTROS.this);
 						 TextView E6 = new TextView(CONSULTAFILTROS.this);
 						 TextView E7 = new TextView(CONSULTAFILTROS.this);
 						 TextView E8 = new TextView(CONSULTAFILTROS.this); 
 						 TextView E9 = new TextView(CONSULTAFILTROS.this); 
 						 TextView E10 = new TextView(CONSULTAFILTROS.this); 
 						 
 						 E1.setTextSize(18);
 						 E1.setTextColor(Color.BLACK);
 						 E2.setTextSize(25);
 						 E2.setTextColor(Color.BLACK);
 						 E3.setTextSize(18);
 						 E3.setTextColor(Color.BLACK);
 						 E4.setTextSize(25);
 						 E4.setTextColor(Color.BLACK);
 						 E5.setTextSize(18);
 						 E5.setTextColor(Color.BLACK);
 						 E6.setTextSize(25);
 						 E6.setTextColor(Color.BLACK);
 						 E7.setTextSize(18);
 						 E7.setTextColor(Color.BLACK);
 						 E8.setTextSize(25);
 						 E8.setTextColor(Color.BLACK);
 						 E9.setTextSize(18);
						 E9.setTextColor(Color.BLACK);
						 E10.setTextSize(25);
						 E10.setTextColor(Color.BLACK);
 						 
 						 String Cn1 = rs.getMetaData().getColumnName(1);
 						 String Cn2 = rs.getMetaData().getColumnName(2);
 						 String Cn3 = rs.getMetaData().getColumnName(3);
 						 String Cn4 = rs.getMetaData().getColumnName(4);
 						 String Cn5 = rs.getMetaData().getColumnName(5);
 						 
 						 E1.setText(Cn1);
 						 E2.setText(es);
 						 
 						 E3.setText(Cn2);
 						 E4.setText(es);
 						 
 						 E5.setText(Cn3);
 						 E6.setText(es);
 						 
 						 E7.setText(Cn4);
 					     E8.setText(es);
 					     
 					     E9.setText(Cn5);
 					     E10.setText(es);
 					     

 						 Erow1.addView(E1);
 						 Erow1.addView(E2);
 						 Erow1.addView(E3);
 						 Erow1.addView(E4);
 						 Erow1.addView(E5);
 						 Erow1.addView(E6);
 						 Erow1.addView(E7);
 						 Erow1.addView(E8);
 						 Erow1.addView(E9);
 						 
 						 E1.setGravity(Gravity.CENTER_HORIZONTAL);
 						 E2.setGravity(Gravity.CENTER_HORIZONTAL);
 						 E3.setGravity(Gravity.CENTER_HORIZONTAL);
 						 E4.setGravity(Gravity.CENTER_HORIZONTAL);
 						 E5.setGravity(Gravity.CENTER_HORIZONTAL);
 						 E6.setGravity(Gravity.CENTER_HORIZONTAL);
 						 E7.setGravity(Gravity.CENTER_HORIZONTAL);
 						 E8.setGravity(Gravity.CENTER_HORIZONTAL);
 						 E9.setGravity(Gravity.CENTER_HORIZONTAL);
 						 Erow1.setGravity(Gravity.CENTER);
 						 Erow1.setBackgroundColor(Color.GREEN);
 						 tablatest.addView(Erow1);	
 						// finaliza agregar encabezados de columnas	
 						 
 						 int sz = rs.getFetchSize();
 							int cont = 0;
 							while (rs.next()) {
 								 String d1 = rs.getString(1);
 								 String d2 = rs.getString(2).trim();
 								 String d3 = rs.getString(3).trim();
 								 String d4 = rs.getString(4).trim();
 								 String d5 = rs.getString(5).trim();
 								 String c1 = comprobar(d1);
 								 String c2 = comprobar(d2);
 								 String c3 = comprobar(d3);
 								 String c4 = comprobar(d4);
 								 String c5 = comprobar(d5);
 								 for (int i = 0; i == sz; i++)
 								 {	
 									 cont = cont +1;
 									 TableRow row1 = new TableRow(CONSULTAFILTROS.this);
 									 TextView t1 = new TextView(CONSULTAFILTROS.this);
 									 TextView t2 = new TextView(CONSULTAFILTROS.this);
 									 TextView t3 = new TextView(CONSULTAFILTROS.this);
 									 TextView t4 = new TextView(CONSULTAFILTROS.this);
 									 TextView t5 = new TextView(CONSULTAFILTROS.this);
 									 TextView t6 = new TextView(CONSULTAFILTROS.this);
 									 TextView t7 = new TextView(CONSULTAFILTROS.this);
 									 TextView t8 = new TextView(CONSULTAFILTROS.this); 
 									 TextView t9 = new TextView(CONSULTAFILTROS.this); 
 			 						 TextView t10 = new TextView(CONSULTAFILTROS.this); 
 			 						 
 									 t1.setTextSize(20);
 									 t1.setTextColor(Color.BLACK);
 									 t2.setTextSize(25);
 									 t2.setTextColor(Color.BLACK);
 									 t3.setTextSize(20);
 									 t3.setTextColor(Color.BLACK);
 									 t4.setTextSize(25);
 									 t4.setTextColor(Color.BLACK);
 									 t5.setTextSize(20);
 									 t5.setTextColor(Color.BLACK);
 									 t6.setTextSize(25);
 									 t6.setTextColor(Color.BLACK);
 									 t7.setTextSize(20);
 									 t7.setTextColor(Color.BLACK);
 									 t8.setTextSize(25);
 									 t8.setTextColor(Color.BLACK);
 									 t9.setTextSize(25);
									 t9.setTextColor(Color.BLACK);
									 t10.setTextSize(25);
 									 t10.setTextColor(Color.BLACK);
 									 
 									 t1.setGravity(Gravity.CENTER_HORIZONTAL);
 									 t2.setGravity(Gravity.CENTER_HORIZONTAL);
 									 t3.setGravity(Gravity.CENTER_HORIZONTAL);
 									 t4.setGravity(Gravity.CENTER_HORIZONTAL);
 									 t5.setGravity(Gravity.CENTER_HORIZONTAL);
 									 t6.setGravity(Gravity.CENTER_HORIZONTAL);
 									 t7.setGravity(Gravity.CENTER_HORIZONTAL);
 									 t8.setGravity(Gravity.CENTER_HORIZONTAL);
 									 t9.setGravity(Gravity.CENTER_HORIZONTAL);
 									 t10.setGravity(Gravity.CENTER_HORIZONTAL);
 									 
 									 row1.setGravity(Gravity.CENTER);
 									 row1.setClickable(true);
 									 if(cont%2 == 0){
 									 row1.setBackgroundColor(Color.LTGRAY);
 									 }else {
 									 row1.setBackgroundColor(Color.WHITE);	 
 									 }
 								 t1.setText(c1);
 								 t2.setText(es);
 								 
 								 t3.setText(c2);
 								 t4.setText(es);
 								 
 								 t5.setText(c3);
 								 t6.setText(es);
 								 
 								 t7.setText(c4);
 								 t8.setText(es);
 								 
 								 t9.setText(c5);
								// t10.setText(es);
								 
 								 row1.addView(t1);
 								 row1.addView(t2);
 								 row1.addView(t3);
 								 row1.addView(t4);
 								 row1.addView(t5);
 								 row1.addView(t6);
 								 row1.addView(t7);
 								 row1.addView(t8);
 								 row1.addView(t9);
 								 //row1.addView(t10);
 								 tablatest.addView(row1);
 								 tablatest.setClickable(true);
 							}
 							}
 						}
 						connection.close();	
 	            	}
 					catch (Exception e) {
 						Toast toast = Toast.makeText(getApplicationContext(), "Verificar Conexi�n.....", Toast.LENGTH_SHORT);
 		            	toast.show();
 						e.printStackTrace();
 	            } 
 	           } 	
 	         } 
 	        });	
 		    
	}
		
		public void alertDialogMensaje(String message1, String mesage2){
		
			try {
			    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
			    ringtone.play();	    	    
			} catch (Exception e) {
			    e.printStackTrace();
			}
			AlertDialog alertDialog = new AlertDialog.Builder(this).create();
			alertDialog.setTitle(message1);
			alertDialog.setMessage(mesage2);
			alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
			// here you can add functions
				ringtone.stop();
			}
			});
			alertDialog.show();
		}
		
		public String comprobar(String cad){
			String Pb = "AP";
			if(cad.equals(Pb)){
				cad = "Procesado";
			}
			return cad;	
		}
		
		public String format (int f){
			val = "";
			if(f<10){
			val = "0"+Integer.toString(f);	
			}
			else{
			val = Integer.toString(f);
			}
			return val;
		}
				
		@Override
		public boolean onCreateOptionsMenu(Menu menu) {

			// Inflate the menu; this adds items to the action bar if it is present.
			getMenuInflater().inflate(R.menu.soporteit, menu);
			return true;
		}

		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			// Handle action bar item clicks here. The action bar will
			// automatically handle clicks on the Home/Up button, so long
			// as you specify a parent activity in AndroidManifest.xml.
			int id = item.getItemId();
			if (id == R.id.action_settings) {
				return true;
			}
			return super.onOptionsItemSelected(item);
		}
	}  		    