 package com.example.sim;


import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.view.LayoutInflater;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;


import com.example.floricultura.R;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.Dguardados;
import com.example.sim.data.JsonFiles;
import com.example.sim.data.Modulos;
import com.example.sim.data.Opciones;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class PlantaMacho extends ActionBarActivity {
private DBAdapter db;
private MyApp appState;	
Ringtone ringtone;

EditText txtPlantaMacho;
EditText emple;

EditText EditFlores;
EditText EditPlantas;
EditText EditHora;
EditText EditMin;

EditText EditBancas;
int cargar = 0;

TextView viewEmpleado;
Button uno;
Button dos;
Button tres;
Button cuatro;
Button cinco;
Button seis;
Button siete;
Button ocho;
Button nueve;
Button cero;
Button btnBorrar;	
Button btnAceptar;
Button botonGuardar;
Button btnBack;
TextView TCodigoEmpleado;
int CodigoModulo;
int Codigodepartamento;
int CodigoEmpleado;
int CodigoOpcion;
int finalizar;
int datoMin;
int datoHora;

String NombreOpcion;
String empleado;
String numeroPlantas;

String obtenerDato;
int datoEntero = 0;
FinalizarActividad obj = new FinalizarActividad();

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.planta_macho);

		txtPlantaMacho = (EditText)findViewById(R.id.txtPlantaM);
		botonGuardar = (Button)findViewById(R.id.botonGuardar);
		btnBack = (Button)findViewById(R.id.btnAtras);
	    
		Bundle extras = getIntent().getExtras();
		CodigoModulo = extras.getInt("CodigoModulo");
	    CodigoEmpleado = extras.getInt("CodigoEmpleado");
	    Codigodepartamento = extras.getInt("Codigodepartamento");
	    CodigoOpcion = extras.getInt("CodigoOpcion");
	    NombreOpcion = extras.getString("NombreOpcion");
	    empleado  = extras.getString("datoPlantaM");
	    numeroPlantas = extras.getString("datoEditPlantas");
	    datoHora = extras.getInt("datoHora");
	    datoMin = extras.getInt("datoMin");
	    
	    
	    TextView TNombreCaptura = (TextView)findViewById(R.id.prueba);
	    TextView TCodigoEmpleado= (TextView)findViewById(R.id.CodigoEmpleado);
	    TextView TCodigoOpcion= (TextView)findViewById(R.id.codigoOpcion); 
	    TextView viewEmpleado= (TextView)findViewById(R.id.viewEmpleado); 
	    		 emple = (EditText)findViewById(R.id.editEmpleado);
	    

	    final CheckBox Check = (CheckBox)findViewById(R.id.checkCorte);
	    //TextView TxtBancas = (TextView)findViewById(R.id.textViewBancas);
	    TextView TxtFlores= (TextView)findViewById(R.id.textFlores);
	    TextView TextPlantas= (TextView)findViewById(R.id.textPlantas);
	    
	    TextView TextHora= (TextView)findViewById(R.id.textPlantas);
	    TextView TextMin= (TextView)findViewById(R.id.textPlantas);
	    
	    EditBancas = (EditText)findViewById(R.id.editBancas);
	    EditFlores = (EditText)findViewById(R.id.editFlores);
	    EditPlantas = (EditText)findViewById(R.id.txtPlantaM);
	    EditHora = (EditText)findViewById(R.id.editHora);
	    EditMin = (EditText)findViewById(R.id.editMin);
	   
	    
	    //EditBancas.setInputType(InputType.TYPE_CLASS_NUMBER |InputType.TYPE_NUMBER_FLAG_DECIMAL);
	    //EditBancas.setSingleLine(false);
	 
	    if(Codigodepartamento == 1){	
	    	TxtFlores.setVisibility(View.INVISIBLE);
	    	EditFlores.setVisibility(View.INVISIBLE);
	    	EditPlantas.setVisibility(View.INVISIBLE);
	    	TextPlantas.setVisibility(View.INVISIBLE);
	    	//empleado.requestFocus();
	    	EditBancas.requestFocus();
	    	
	    
	    }
	 
	    TNombreCaptura.setText(NombreOpcion+ "");
	    TCodigoEmpleado.setText(CodigoEmpleado + "");
	    TCodigoOpcion.setText(CodigoOpcion + "");	    
	    viewEmpleado.setText(empleado + "");
	    
	    appState = ((MyApp)getApplicationContext());   
	    db = appState.getDb();
	    db.open();
	    
	    Cursor CursorOpciones= null ;
	    CursorOpciones = db.getOpciones("Opcion=" + CodigoOpcion);
	    
	    if(CursorOpciones.getCount()>0){
	    	Opciones op = db.getOpcionesFromCursor(CursorOpciones, 0);
	    	finalizar = op.getFinalizaActividad();
	    }
	    CursorOpciones.close();
	    
	    
	    
	    btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
	    	public void onClick(View v) {
            	
            	//onBackPressed();
            	finish();
            	
            }
        });
	    
	}
	
	
	
	 public void guardarDato(View view) {
		 Finaliza();
		 obtenerDato = txtPlantaMacho.getText().toString();
		 datoEntero = Integer.parseInt(obtenerDato);
         Intent i = new Intent(this, FinalizarActividad.class );
         i.putExtra("datoPlantaMacho",datoEntero);
         i.putExtra("CodigoModulo",CodigoModulo);
         i.putExtra("CodigoEmpleado",CodigoEmpleado);
         i.putExtra("Codigodepartamento",Codigodepartamento);
         i.putExtra("CodigoOpcion",CodigoOpcion);
         i.putExtra("NombreOpcion",NombreOpcion);
         
         //i.putExtra("datoPlantaMacho", txtPlantaMacho.getText().toString());
         startActivity(i);
         super.finish();
   
         
   }	

	

	 
	 
	 
	 
	 
	 
	 
	public void hideSoftKeyboard() {
	    if(getCurrentFocus()!=null) {
	        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
	        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),   InputMethodManager.HIDE_NOT_ALWAYS);
	    }
	}
		
	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.planta_macho, menu);
		return true;
	}*/

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	public void alertDialogMensaje(String message1, String mesage2){	
		try {
		    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
		    ringtone.play(); 
		} catch (Exception e) {
		    e.printStackTrace();
		}	
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(message1);
		alertDialog.setMessage(mesage2);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int which) {
		// here you can add functions
			ringtone.stop();
		}
		});
		alertDialog.show();
	}


	public void click(View v) {
	    switch (v.getId()) {
	      case R.id.uno:
		        agregarNumero("1");
		     	
	        break;
	      case R.id.dos:
	    	  agregarNumero("2");
	        break;
	      case R.id.tres:
		        agregarNumero("3");
	        break;
	      case R.id.cuatro:
	    	  agregarNumero("4");
	        break;
	      case R.id.cinco:
		        agregarNumero("5");
	        break;
	      case R.id.seis:
	    	  agregarNumero("6");
	        break;
	      case R.id.siete:
	    	  agregarNumero("7");
	        break;
	      case R.id.ocho:
		        agregarNumero("8");
	        break;
	      case R.id.nueve:
	    	  agregarNumero("9");
	        break;
	      case R.id.borrarBoton:
		        agregarNumero("0");
	        break;
	      case R.id.btnBorrar:
	    	  agregarNumero("b");
	        break;
	      case R.id.botonp:
	    	  agregarNumero(".");
	        break;
	      }
	     
	}
	public void agregarNumero(String n){
	
		if(n.contains("b")){
			
			if(txtPlantaMacho.isFocused()){
				txtPlantaMacho.setText("");
			}
		}else{
			if(txtPlantaMacho.isFocused()){
				txtPlantaMacho.setText(txtPlantaMacho.getText() + n);
		}			
	}
	}

	public void Finaliza(){
		
		Cursor CursorGuardados = db.getDguardados("CodigoEmpleado = " + Integer.parseInt(empleado) +" AND ActividadFinalizada = 0");
		
		obtenerDato = txtPlantaMacho.getText().toString();
		datoEntero = Integer.parseInt(obtenerDato);
	
		Dguardados dtemp =  db.getDguardadosFromCursor(CursorGuardados, 0);
		//Dguardados dtemp = new Dguardados();
		Calendar c = Calendar.getInstance(); 
		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
		String fecha = fdate.format(c.getTime());
		SimpleDateFormat ftime = new SimpleDateFormat("HH:mm:ss");
		String horaFin = ftime.format(c.getTime());
		//dtemp.setPlantaMacho(datoEntero);	
		dtemp.setFecha(fecha);
		dtemp.setHoraFin(horaFin);
		dtemp.setSupervisor(CodigoEmpleado);
		dtemp.setActividadFinalizada(1);
		dtemp.setPlantaMacho(datoEntero);
		
		//se obtienen los datos ingresados en la caja de texsto y si los datos son mayores a 0
		// se almacenan en la variable temporal dtemp		
		
		if(numeroPlantas.length()>0)
			dtemp.setPlantas(numeroPlantas);
		if(datoHora>0)
			dtemp.setHora(datoHora);
		if(datoMin>0)
			dtemp.setMin(datoMin);
		
	
		
		//if(PlantaMachos>0)
			//dtemp.setPlantaMacho(PlantaMachos);
		// se hace un update a la bd
		
		db.updateDguardados(dtemp);
	
		//EditPlantas.setText("");
		//EditFlores.setText("");
		//EditHora.setText("");
		//EditMin.setText("");
		
		
		
		// se crea el json DE SEMILLAS
		ObjectMapper mapper = new ObjectMapper();
		try {
			// se refiere al nombre que se coloca en el bloc de notas
			String nameJson ="G_" + dtemp.getCodigoEmpleado() + "_"+dtemp.getFecha() + "_"+dtemp.getHoraFin();
			nameJson=nameJson.replace(":", "-");
			mapper.writeValue(new File( Environment.getExternalStorageDirectory()+"/SIM/JsonDescarga/Finalizados/" + nameJson+ ".json"), dtemp);
			
			
			JsonFiles archivo = new JsonFiles();
			archivo.setName(nameJson+ ".json");
			archivo.setNameFolder("Finalizados/");
			archivo.setUpload(0);
			db.insertarJsonFile(archivo);
			
			if(!appState.getSubiendoArchivos()){
				appState.setSubiendoArchivos(true);
				new UptoDropboxFin().execute(getApplicationContext());
			}	

		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	 
	
}
