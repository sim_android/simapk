package com.example.sim;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.media.Ringtone;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.dropbox.core.DbxException;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.FileMetadata;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.JsonFiles;

public class UptoDropbox extends AsyncTask<Context, Integer, Boolean> {   	
		
	private MyApp appState;	
	private DBAdapter db;
    Ringtone ringtone;
    private DbxClientV2 dbxClientV2;
    
    
    String FILES_DIR = "/SIM/JsonDescarga/";
     
	
    protected void onPreExecute() {
    	//loadingDialog = ProgressDialog.show(MainActivity.this,"", "Sincronizando datos hacia el servidor", true, false);
    	Log.d("inciando subida","inciando subida");
    }
	 
    @Override 
    protected Boolean doInBackground(Context... params) {
    	
    	appState = ((MyApp)params[0]);   
        db = appState.getDb();
        db.open();
        dbxClientV2 = appState.getDropBoxClient();

                   
        ArrayList<JsonFiles> files = getDatos();
        
        if(appState.connectionAvailable()){
        	
	        while(files.size()>0){    	
	        	JsonFiles file = files.remove(0);
	        	
	        	Log.d("SubiendoAAAA", file.getName()); 
		    	File dir = Environment.getExternalStorageDirectory();
			    File yourFile = new File(dir,"SIM/JsonDescarga/" + file.getName());
		    	
		   
		    	FileInputStream inputStream;
		    	 String path = FILES_DIR + file.getNameFolder() + yourFile.getName();
		    	 try {
	                    inputStream = new FileInputStream(yourFile);

	                    FileMetadata metadata = dbxClientV2.files().uploadBuilder(path)
	                            .uploadAndFinish(inputStream);

	                    file.setUpload(1);
	                    db.updateJsonFiles(file);

	                } catch (FileNotFoundException e) {
	                    e.printStackTrace();
	                } catch (DbxException e) {
	                    e.printStackTrace();
	                } catch (IOException e) {
	                    e.printStackTrace();
	                }
		    	files = getDatos();
	        }
        	
        } else {
        	
        	
        
        }
        return true;
    }
 
    @Override
    protected void onPostExecute(Boolean result) {
    	//loadingDialog.dismiss();
    	// appState.exportDatabse("floricultural.db");//exporta la base de datos a la SD card	
    	
    	boolean mostrarMensaje = appState.getMostrarDialog();
    	
    	if(mostrarMensaje){  		
    		Context context = appState;
            CharSequence text = "Se han sincronizado todos los Datos";
            int duration = Toast.LENGTH_LONG;
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
    	}
    	appState.setSubiendoArchivos(false);
    	appState.setMostrarDialog(false);
    }
    

	public ArrayList<JsonFiles> getDatos(){
    	ArrayList<JsonFiles> Listtemp = new ArrayList<JsonFiles> ();
    	 Cursor filesjson = db.getJsonFiles("Upload=0");
		 if(filesjson.getCount()>0){
			 for(int i=0 ; i<filesjson.getCount();i++){	
				 JsonFiles Dtemp = this.db.getJsonFilesFromCursor(filesjson, i);
				 Listtemp.add(Dtemp);
			 }
		 }
		 filesjson.close();
    	 return Listtemp;
    }
    
}

