package com.example.sim;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.example.floricultura.R;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.Dguardados;
import com.example.sim.data.JsonFiles;
import com.example.sim.data.Modulos;
import com.example.sim.data.Opciones;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class FinalizaInduccion extends ActionBarActivity {
	
	private MyApp appState;	
	private DBAdapter db;
	int CodigoModulo;
	int Codigodepartamento;
	int CodigoEmpleado;
	int CodigoOpcion;
	String NombreOpcion;
	EditText empleado;
	Ringtone ringtone;
	int OpcionAsignado = 0;
	int NoModuloAsignado = 0;
	String ModuloAsignado = "";
	int finalizar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fin_induccion);
		
		Bundle extras = getIntent().getExtras();
		CodigoModulo = extras.getInt("CodigoModulo");
	    CodigoEmpleado = extras.getInt("CodigoEmpleado");
	    Codigodepartamento = extras.getInt("Codigodepartamento");
	    CodigoOpcion = extras.getInt("CodigoOpcion");
	    NombreOpcion = extras.getString("NombreOpcion");
	    
	    
	    TextView TNombreCaptura = (TextView)findViewById(R.id.OpcionCaptura);
	    TextView TCodigoEmpleado= (TextView)findViewById(R.id.CodigoEmpleado);
	    TextView TCodigoOpcion= (TextView)findViewById(R.id.codigoOpcion);
	    empleado  = (EditText)findViewById(R.id.editEmpleado);
	    Button btnBack = (Button) findViewById(R.id.btnBack);
	    Button btnListado = (Button) findViewById(R.id.btnListado);
	    Button btnNuevo = (Button) findViewById(R.id.btnNuevo);
	    
	    TNombreCaptura.setText(NombreOpcion+ "");
	    TCodigoEmpleado.setText(CodigoEmpleado + "");
	    TCodigoOpcion.setText(CodigoOpcion + "");	    
	    
	    //se valida la opcion finalizar actividad
	    appState = ((MyApp)getApplicationContext());   
	    db = appState.getDb();
	    db.open();
	    
	    Cursor CursorOpciones= null ;
	    CursorOpciones = db.getOpciones("Opcion=" + CodigoOpcion);
	    
	    if(CursorOpciones.getCount()>0){
	    	Opciones op = db.getOpcionesFromCursor(CursorOpciones, 0);
	    	finalizar = op.getFinalizaActividad();
	    }
	    CursorOpciones.close();
		
	    empleado.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {		               
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                	
                	int valid = revisa(Integer.parseInt(empleado.getText().toString().trim()));
                	if(valid == 1){
                		Cursor CursorGuardados = db.getDguardados("CodigoEmpleado = " + Integer.parseInt(empleado.getText().toString().trim()) +" AND ActividadFinalizada = 0");
                		if(CursorGuardados != null  && CursorGuardados.getCount()>0){
                			Dguardados dtemp =  db.getDguardadosFromCursor(CursorGuardados, 0);
                			String Actividad = dtemp.getActividad().toString().trim();
                			
                				//-----------------------------------------------------------------------------------
                				String correlativo="";
                				    	//almaceno el empleado en una variable string
                				    	String getEmpleado = empleado.getText().toString().trim();
                				    	// guardo la variable que tiene empleado en un array
                				    	String[]empl = new String[]{getEmpleado};
                				    	Cursor obtenerCorrelativo = db.getCorrelativo(empl);
                				    	//recorro dicho arreglo para obtener el campo
                				    	if (obtenerCorrelativo.moveToFirst()){
                				    		do{
                				    			correlativo = obtenerCorrelativo.getString(0);
                				    		}while(obtenerCorrelativo.moveToNext());
                				    	}
                				//-------------------------------------------------------------------------
                    		Calendar c = Calendar.getInstance(); 
                    		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
                    		String fecha = fdate.format(c.getTime());
                    		SimpleDateFormat ftime = new SimpleDateFormat("HH:mm:ss");
                    		String horaFin = ftime.format(c.getTime());
                    		
                    		dtemp.setFecha(fecha);
                			dtemp.setHoraFin(horaFin);
                			dtemp.setSupervisor(CodigoEmpleado);
                			dtemp.setActividadFinalizada(1);
                			dtemp.setCorrelativoEmpleado(correlativo);
                					
                			
                			
                			
                			db.updateDguardados(dtemp);
                			
                			empleado.setText("");
                			empleado.requestFocus();
                    		
                    		ObjectMapper mapper = new ObjectMapper();
                    		try {
                    			String nameJson ="G_" + dtemp.getCodigoEmpleado() + "_"+dtemp.getFecha() + "_"+dtemp.getHoraFin();
                    			nameJson=nameJson.replace(":", "-");
								mapper.writeValue(new File( Environment.getExternalStorageDirectory()+"/SIM/JsonDescarga/Finalizados/" + nameJson+ ".json"), dtemp);
								
								
								JsonFiles archivo = new JsonFiles();
								archivo.setName(nameJson+ ".json");
								archivo.setNameFolder("Finalizados/");
								archivo.setUpload(0);
								db.insertarJsonFile(archivo);
								
								if(!appState.getSubiendoArchivos()){
									appState.setSubiendoArchivos(true);
									new UptoDropboxFin().execute(getApplicationContext());
								}	
							} catch (JsonGenerationException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (JsonMappingException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
                			
                		}else{
                			alertDialogMensaje("Invalido", "Usuario invalido");
                    		empleado.setText("");
                    		empleado.requestFocus();
                		}
                		CursorGuardados.close();
                		
                	} else {
                		alertDialogMensaje("EMPLEADO NO ECONTRADO","Empleado No Encontrado, favor descargar \n  D_Acceso e intentar de nuevo");
                		empleado.setText("");
                		empleado.requestFocus();
                	}
	                return true;
                }
                return false;
            }
        });	    	
	    
	    btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
 
            	onBackPressed();
            }
        });
        
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	empleado.setText("");
            }
        });
        
        btnListado.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Intent intent = new Intent(getApplicationContext(), LISTADOACTIVIDAD.class);
           	    intent.putExtra("CodigoEmpleado", CodigoEmpleado );
           	    startActivity(intent);
            	
            }
        });
	}
	
	public int revisa (int codigo){
		int valor = 0;
		int Op = 0;
		int Md = 0;
		Cursor CursorGuarda =	db.getDguardados("CodigoEmpleado=" + codigo + " AND ActividadFinalizada=0");
		 if(CursorGuarda.getCount()>0){
			 Dguardados dgu = db.getDguardadosFromCursor(CursorGuarda, 0);
			 Op = dgu.getActividad();
			 Cursor MouloOpcion = this.db.getOpciones("Opcion=" + Op);
			 Opciones opc = this.db.getOpcionesFromCursor(MouloOpcion, 0);
			 Md = opc.getCodigoDepto();
			 Cursor Modulos = db.getModulos("CodigoModulo=" + Md);
			 Modulos mod = this.db.getModulosFromCursor(Modulos, 0);
			 NoModuloAsignado = mod.getCodigoModulo();
			 ModuloAsignado = mod.getDescripcion().toString().trim();
			 OpcionAsignado = dgu.getActividad();
			 valor = 1;
			 MouloOpcion.close();
			 Modulos.close();
			 } else {
				valor = 0;
			 }
		 CursorGuarda.close();
		 return valor;
	}
	public void alertDialogMensaje(String message1, String mesage2){	
		try {
		    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
		    ringtone.play(); 
		} catch (Exception e) {
		    e.printStackTrace();
		}	
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(message1);
		alertDialog.setMessage(mesage2);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int which) {
		// here you can add functions
			ringtone.stop();
		}
		});
		alertDialog.show();
	}
}


