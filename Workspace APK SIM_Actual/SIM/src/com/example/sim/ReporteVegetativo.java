package com.example.sim;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.Vibrator;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.floricultura.R;
import com.example.sim.data.DBAdapter;

public class ReporteVegetativo extends ActionBarActivity  {
	
	private MyApp appState;	
	private DBAdapter db;
	int month;
	int yearL;
	private String val;
	long Pos;
	String weak;
	String year;
	String yearWithOut;
	String yearPicker;
	String day;
	Vibrator mVibrator;
	Ringtone ringtone;
	GridView gridview;
	GridView gridEnc;
	CheckBox monday;
	CheckBox tuesday;
	CheckBox wednesday;
	CheckBox thursday;
	CheckBox friday;
	CheckBox saturday;
	CheckBox sunday;
	CheckBox saturday2;
	CheckBox sunday2;
	GridView gridView;
	ArrayList<String> list;
	ArrayAdapter madapter;
	
	String Day;
	String Greenhouse;
	String Week;
	String Year;
	String YearPicker;


	@SuppressLint("NewApi") @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.reporte_vegetativo);
		
		// para manejar la excepcion de permisos en el API de Android
    	StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        
		Button btnBack = (Button)findViewById(R.id.btnBack);
		Button btnOk = (Button)findViewById(R.id.btnAccept);
		Button btnDatePicker = (Button)findViewById(R.id.btnDatePicker);
		final EditText datePickerField = (EditText)findViewById(R.id.datePickerField);
		final EditText itemYear = (EditText)findViewById(R.id.itemYear);
		final EditText itemWeak = (EditText)findViewById(R.id.itemWeek);
		gridEnc = (GridView) findViewById(R.id.gridEnc);
	    gridview = (GridView) findViewById(R.id.gridDetalle);
	    monday = (CheckBox)findViewById(R.id.monday);
	    tuesday = (CheckBox)findViewById(R.id.tuesday);
	    wednesday = (CheckBox)findViewById(R.id.wednesday);
	    thursday = (CheckBox)findViewById(R.id.thursday);
	    friday = (CheckBox)findViewById(R.id.friday);
	    saturday = (CheckBox)findViewById(R.id.saturday);
	    sunday = (CheckBox)findViewById(R.id.sunday);
	    saturday2 = (CheckBox)findViewById(R.id.saturday2);
	    sunday2 = (CheckBox)findViewById(R.id.sunday2);
	    gridView = (GridView)findViewById(R.id.gridDetalle);
	    list = new ArrayList<String>();
	    
	    final ArrayList<String> datos = new ArrayList<String>();
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, datos);
        final ArrayList<String> encabezado = new ArrayList<String>();
        final ArrayAdapter<String> adapEnc = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, encabezado);

	  //incializamos la BD asi como tambien obtenemos la referencia al singleton
	    appState = ((MyApp)getApplicationContext());   
	    db = appState.getDb();
	    db.open();
		
		btnOk.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				datos.clear();
            	adapter.clear();
            	encabezado.clear();
            	adapEnc.clear();
				year = itemYear.getText().toString().trim();
				weak = itemWeak.getText().toString().trim();
				yearPicker = datePickerField.getText().toString().trim();
				yearWithOut = yearPicker.replace("-", "").toString().trim();
				String Pb = "";
				day = "OR BARSDE LIKE ' '";
				for (String str : list) {
					day = day + " " +str;
					//datePickerField.setText(datePickerField.getText().toString()+ " , "+ str);
				}
				//alertDialogMensaje("Lista",day);
				if(yearPicker.equals(Pb)) {
					alertDialogMensaje("ERROR","PORFAVOR SELECCIONE UNA FECHA");
					datePickerField.setText("");
				} else if(year.equals(Pb)) {
					alertDialogMensaje("ERROR","PORFAVOR INGRESE UN A�O");
					itemYear.setText("");
				} else if(weak.equals(Pb)) {
					alertDialogMensaje("ERROR","PORFAVOR INGRESE UNA SEMANA");
					itemWeak.setText("");
				} else {
					try {
						String user = "sim";
		            	String pasword = "sim";
		            	 Connection connection = null;
		            	 ResultSet rs = null;
		            	 Statement statement = null;
		            	 
							Class.forName("com.ibm.as400.access.AS400JDBCDriver");
							connection = DriverManager.getConnection("jdbc:as400://190.190.190.10", user, pasword);	
							statement = connection.createStatement();	
							rs = statement.executeQuery("Select cor.dia,INV.codigo_app INVERNADERO,sum(ifnull(COR.CORTE,0)) ETQ,sum(IFNULL(IN.IN,0)) IN,sum(IFNULL(OUT.OUT,0)) OUT" +
             ",((sum(ifnull(COR.CORTE,0)) + sum(IFNULL(IN.IN,0))) - sum(IFNULL(OUT.OUT,0))) CORTE " +
             ",sum(IFNULL(ASIG.ASIGNADAS,0)) ASIGNADAS " +
             ",(sum(IFNULL(L1.L1,0)) + sum(IFNULL(EMP.EMP,0))) L1 " +
             ",CASE WHEN (sum(IFNULL(L1.L1,0)) + sum(IFNULL(EMP.EMP,0))) > ((sum(ifnull(COR.CORTE,0)) + sum(IFNULL(IN.IN,0))) - sum(IFNULL(OUT.OUT,0))) THEN " +
             "'0' ELSE " +
             "(((sum(ifnull(COR.CORTE,0)) + sum(IFNULL(IN.IN,0))) - sum(IFNULL(OUT.OUT,0))) - (sum(IFNULL(L1.L1,0)) + sum(IFNULL(EMP.EMP,0)))) END DIFERENCIA " +
             ",case when (CASE WHEN (SUM(IFNULL(L1.L1,0)) + SUM(IFNULL(EMP.EMP,0))) > ((SUM(ifnull(COR.CORTE,0)) + SUM(IFNULL(IN.IN,0))) - SUM(IFNULL(OUT.OUT,0))) THEN " +
             "'0' ELSE " +
             "(((SUM(ifnull(COR.CORTE,0)) + SUM(IFNULL(IN.IN,0))) - SUM(IFNULL(OUT.OUT,0))) - (SUM(IFNULL(L1.L1,0)) + SUM(IFNULL(EMP.EMP,0)))) END) = 0 then 'BR' ELSE 'BE' END ESTADO " +
             "from sisappf.app_invernaderos inv " +
             "left join ( " +
             "Select bardoc,left(BARSDE,3) DIA,replace(replace(barinv,'-',''),' ','') Inv,count(barcns) Corte from sigvegfgu.vege43 " + 
             "where barano = '"+year+"' and barsem = '"+weak+"' and barot3 = '2.00' " +
              "AND (BARSDE LIKE '' "+day+") " +
             "group by bardoc,barinv,left(BARSDE,3) " + 
             "ORDER BY left(BARSDE,3),replace(replace(barinv,'-',''),' ','') " +
             ") cor " +
             "on inv.codigo_app = cor.inv " +
             "left join ( " +
             "select a.bardoc,A.DIA,a.inv_cr,sum(in) in from ( " +
             "Select v43.bardoc,left(BARSDE,3) DIA,ifnull(ifnull(l1.invernadero,asig.invernadero),replace(replace(v43.barinv,'-',''),' ','')) inv_Cr,count(v43.barcns) IN " +
             ",case when replace(replace(v43.barinv,'-',''),' ','') <> ifnull(ifnull(l1.invernadero,asig.invernadero),replace(replace(v43.barinv,'-',''),' ','')) then 1 else 0 end Valida " +
             "from sigvegfgu.vege43 v43 " +
             "left join (Select ordencompra,invernadero,cast(etiqueta as integer) Etiqueta from sisappf.app_tmp_etiquetas where fecha between '"+yearPicker+"'  and '"+yearPicker+"' and estado = 'A' and invernadero <> 'EMPAQU' order by etiqueta) asig on v43.barcns = asig.Etiqueta " +
             "left join (Select ordencompra,invernadero,cast(etiqueta as integer) Etiqueta from sisappf.app_tmp_etiquetasL1 where fecha between '"+yearPicker+"' and '"+yearPicker+"' and estado = 'A' and invernadero <> 'EMPAQU' order by etiqueta) l1 on v43.barcns = l1.Etiqueta " +
             "where barano = '"+year+"' and barsem = '"+weak+"' and barot3 = '2.00' AND (BARSDE LIKE '' "+day+") " +
             "and case when replace(replace(v43.barinv,'-',''),' ','') <> ifnull(ifnull(l1.invernadero,asig.invernadero),replace(replace(v43.barinv,'-',''),' ','')) then 1 else 0 end > 0 " + 
             "group by v43.bardoc,left(BARSDE,3),v43.barinv,l1.invernadero,asig.invernadero) a " +
             "group by a.bardoc,A.DIA,a.inv_cr " +
             ") IN ON COR.BARDOC = IN.BARDOC AND inv.codigo_app = IN.INV_CR AND cor.dia = IN.dia " +
             "left join ( " +
            "select B.bardoc,B.DIA,B.inv_OR,sum(OUT) OUT from ( " +
             "Select v43.bardoc,left(BARSDE,3) DIA, replace(replace(v43.barinv,'-',''),' ','') inv_Or,count(v43.barcns) Out " +
             ",case when replace(replace(v43.barinv,'-',''),' ','') <> ifnull(ifnull(l1.invernadero,asig.invernadero),replace(replace(v43.barinv,'-',''),' ','')) then 1 else 0 end Valida " +
             "from sigvegfgu.vege43 v43 " +
             "left join (Select ordencompra,invernadero,cast(etiqueta as integer) Etiqueta from sisappf.app_tmp_etiquetas where fecha between '"+yearPicker+"' and '"+yearPicker+"' and estado = 'A' and invernadero <> 'EMPAQU' order by etiqueta) asig on v43.barcns = asig.Etiqueta " +
             "left join (Select ordencompra,invernadero,cast(etiqueta as integer) Etiqueta from sisappf.app_tmp_etiquetasL1 where fecha between '"+yearPicker+"' and '"+yearPicker+"' and estado = 'A' and invernadero <> 'EMPAQU' order by etiqueta) l1 on v43.barcns = l1.Etiqueta " +
             "where barano = '"+year+"' and barsem = '"+weak+"' and barot3 = '2.00' AND (BARSDE LIKE '' "+day+") " +
             "and case when replace(replace(v43.barinv,'-',''),' ','') <> ifnull(ifnull(l1.invernadero,asig.invernadero),replace(replace(v43.barinv,'-',''),' ','')) then 1 else 0 end > 0 " + 
             "group by v43.bardoc,left(BARSDE,3),v43.barinv,l1.invernadero,asig.invernadero) B " +
              "            GROUP BY B.BARDOC,B.INV_OR,B.DIA " +
             ") OUT  ON COR.BARDOC = OUT.BARDOC AND INV.codigo_app = OUT.INV_OR AND cor.dia = OUT.dia " +
             "left join ( " +
             "Select left(BARSDE,3) Dia,OrdenCompra,Invernadero,count(etiqueta) Asignadas " + 
             "from sisappf.app_tmp_etiquetas e inner join sigvegfgu.vege43 v on ordencompra = bardoc and etiqueta = barcns " + 
             "where fecha between '"+yearPicker+"' and '"+yearPicker+"' and ESTADO = 'A' and invernadero <> 'EMPAQU' and barot3 = '2.00'  AND (BARSDE LIKE '' "+day+") " +
             "group by left(BARSDE,3),ordencompra,invernadero " +
             ") ASIG ON COR.BARDOC = ASIG.ORDENCOMPRA AND INV.codigo_app = ASIG.INVERNADERO AND cor.dia = asig.dia " +
             "left join ( " +
             "Select left(BARSDE,3) Dia,OrdenCompra,Invernadero,count(etiqueta) L1 from sisappf.app_tmp_etiquetasl1 l1 " + 
             "inner join sigvegfgu.vege43 v on ordencompra = bardoc and etiqueta = barcns where fecha between '"+yearPicker+"' and '"+yearPicker+"' and ESTADO = 'A' and invernadero <> 'EMPAQU' and barot3 = '2.00'  AND (BARSDE LIKE '' "+day+") " +
             "group by left(BARSDE,3),ordencompra,invernadero " +
             ") L1  ON COR.BARDOC = L1.ORDENCOMPRA AND INV.codigo_app = L1.INVERNADERO AND cor.dia = L1.dia " +
             "left join ( " +
             "Select b.bardoc,b.Inv,sum(b.emp) Emp from (Select v.bardoc " +
             ",ifnull(IFNULL(A.INVERNADERO,e.invernadero),replace(replace(v.barinv,'-',''),' ','')) Inv,count(i.temd01) emp " + 
             "from sigipvfgu.incv618 i " +
             "left join sigvegfgu.vege43 v on '"+year+"' = v.barano and i.temd01 = v.barcns " +
             "left join (Select ordencompra,invernadero,cast(etiqueta as integer) Etiqueta from sisappf.app_tmp_etiquetasL1 inner join sigvegfgu.vege43 v on ordencompra = bardoc and etiqueta = barcns where fecha between '"+yearPicker+"' and '"+yearPicker+"' and estado = 'A' and invernadero <> 'EMPAQU'  and barot3 = '2.00' AND  (BARSDE LIKE '' "+day+") order by etiqueta) a on i.temd01 = a.etiqueta  " + 
             "left join (Select ordencompra,invernadero,cast(etiqueta as integer) Etiqueta from sisappf.app_tmp_etiquetas inner join sigvegfgu.vege43 v on ordencompra = bardoc and etiqueta = barcns where fecha between '"+yearPicker+"' and '"+yearPicker+"' and estado = 'A' and invernadero <> 'EMPAQU'  and barot3 = '2.00'  AND (BARSDE LIKE '' "+day+") order by etiqueta) e on i.temd01 = e.etiqueta  " +
             "where temd08 between '"+yearWithOut+"' and '"+yearWithOut+"' and a.etiqueta is null  AND (BARSDE LIKE '' "+day+") " +
             "group by v.bardoc,barinv,A.INVERNADERO,e.invernadero) b group by b.bardoc,b.Inv order by b.inv,b.bardoc " +
             ") EMP ON COR.BARDOC = EMP.BARDOC AND INV.codigo_app = emp.inv " +
             "group by cor.dia,inv.codigo_app " +
             "order by cor.dia,INV.codigo_app ");
							
							String Cn1 = rs.getMetaData().getColumnName(1);
							encabezado.add(Cn1);
							gridEnc.setAdapter(adapEnc);
							String Cn2 = rs.getMetaData().getColumnName(2);
							encabezado.add(Cn2);
							gridEnc.setAdapter(adapEnc);
							String Cn3 = rs.getMetaData().getColumnName(3);
							encabezado.add(Cn3);
							gridEnc.setAdapter(adapEnc);
							String Cn4 = rs.getMetaData().getColumnName(4);
							encabezado.add(Cn4);
							gridEnc.setAdapter(adapEnc);
							String Cn5 = rs.getMetaData().getColumnName(5);
							encabezado.add(Cn5);
							gridEnc.setAdapter(adapEnc);
							String Cn6 = rs.getMetaData().getColumnName(6);
							encabezado.add(Cn6);
							gridEnc.setAdapter(adapEnc);
							String Cn7 = rs.getMetaData().getColumnName(7);
							encabezado.add(Cn7);
							gridEnc.setAdapter(adapEnc);
							String Cn8 = rs.getMetaData().getColumnName(8);
							encabezado.add(Cn8);
							gridEnc.setAdapter(adapEnc);
							String Cn9 = rs.getMetaData().getColumnName(9);
							encabezado.add(Cn9);
							gridEnc.setAdapter(adapEnc);
							String Cn10 = rs.getMetaData().getColumnName(10);
							encabezado.add(Cn10);
							gridEnc.setAdapter(adapEnc);
						
							while (rs.next()) {
								 String d1 = rs.getString(1).trim();
								 String d2 = rs.getString(2).trim();
								 String d3 = rs.getString(3).trim();
								 String d4 = rs.getString(4).trim();
								 String d5 = rs.getString(5).trim();
								 String d6 = rs.getString(6).trim();
								 String d7 = rs.getString(7).trim();
								 String d8 = rs.getString(8).trim();
								 String d9 = rs.getString(9).trim();
								 String d10 = rs.getString(10).trim();
								 String c1 = comprobar(d1);
								 String c2 = comprobar(d2);
								 String c3 = comprobar(d3);
								 String c4 = comprobar(d4);
								 String c5 = comprobar(d5);
								 String c6 = comprobar(d6);
								 String c7 = comprobar(d7);
								 String c8 = comprobar(d8);
								 String c9 = comprobar(d9);
								 String c10 = comprobar(d10);
								 datos.add(c1);
								 gridview.setAdapter(adapter);
								 datos.add(c2);
								 gridview.setAdapter(adapter);
								 datos.add(c3);
								 gridview.setAdapter(adapter);
								 datos.add(c4);
								 gridview.setAdapter(adapter);
								 datos.add(c5);
								 gridview.setAdapter(adapter);
								 datos.add(c6);
								 gridview.setAdapter(adapter);
								 datos.add(c7);
								 gridview.setAdapter(adapter);
								 datos.add(c8);
								 gridview.setAdapter(adapter);
								 datos.add(c9);
								 gridview.setAdapter(adapter);
								 datos.add(c10);
								 gridview.setAdapter(adapter);
							}
							connection.close();	
							
					}catch (Exception e) {
						Toast toast = Toast.makeText(getApplicationContext(), "Conectando.....", Toast.LENGTH_SHORT);
		            	toast.show();
						e.printStackTrace();
					} 
				}
			}
		});
		
		  gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
	            @Override
	            public void onItemClick(AdapterView<?> parent, final View view, int position, long id){
	            			Pos =  parent.getItemIdAtPosition(position);
	            			int ifila = (int)(Pos/10);
	            			int day = (ifila*10); 
	            			int greenhouse = (ifila*10)+1;
	            			Day = (String) parent.getItemAtPosition(day);
	            			Greenhouse = (String) parent.getItemAtPosition(greenhouse);
	            			//alertDialogMensaje("Alert",weak);
	            			Intent intent = new Intent(getApplicationContext(), DetalleGenesis.class);
		    	        	intent.putExtra("Day", Day);
		    	        	intent.putExtra("Greenhouse",Greenhouse);
		    	        	intent.putExtra("Year", year);
		    	        	intent.putExtra("Weak", weak);
		    	        	intent.putExtra("YearPicker", yearPicker );
		    	        	startActivity(intent);    	    
		    	            }
		    	    });
		
		itemWeak.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				final NumberPicker myNumberPicker = new NumberPicker(ReporteVegetativo.this);
            	myNumberPicker.setMaxValue(53);
            	myNumberPicker.setMinValue(01);

            	NumberPicker.OnValueChangeListener myValChangedListener = new NumberPicker.OnValueChangeListener() {
            	  @Override
            	  public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
            		  int dat = myNumberPicker.getValue();
            		  String Sem = format(dat);
            		  itemWeak.setText(Sem);
            	  }
            	};

            	myNumberPicker.setOnValueChangedListener(myValChangedListener);

            	new AlertDialog.Builder(ReporteVegetativo.this).setView(myNumberPicker).setPositiveButton(android.R.string.ok, new OnClickListener(){
            		@Override
            		public void onClick(DialogInterface dialog, int which) {
            		myNumberPicker.clearFocus();}
            		}).show(); 	
			}
		});
		
		btnBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				onBackPressed();
			}
		});
		
		btnDatePicker.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Calendar mcurrentDate=Calendar.getInstance();
                int mYear = mcurrentDate.get(Calendar.YEAR);
                int mMonth = mcurrentDate.get(Calendar.MONTH);
                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker=new DatePickerDialog(ReporteVegetativo.this, new OnDateSetListener() {                  
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                    	int datodia = selectedday;
                    	String dia = format(datodia);
                    	int datomes = selectedmonth + 1;
                    	String mes = format(datomes);
                    	String datoanio = Integer.toString(selectedyear);
                    	yearL = selectedyear;
                    	String dato = datoanio + "-" + mes + "-" +dia;
                    	datePickerField.setText(dato);
                    }
                }
                ,mYear, mMonth, mDay);
                mDatePicker.setTitle("Seleccionar Fecha");                
                mDatePicker.show();  
			}
		});
	}
	
	public void selectedDay(View view) {
		boolean checked = ((CheckBox) view).isChecked();
		
		switch(view.getId()) {
		case R.id.monday:
			if(monday.isChecked() == true) {
				list.add(monday.getContentDescription().toString());	
			} else {
				list.remove("OR BARSDE LIKE 'LUNES%' ");
				
			}
			break;
		case R.id.tuesday:
			if(tuesday.isChecked() == true) {
				list.add(tuesday.getContentDescription().toString());	
			} else {
				list.remove("OR BARSDE LIKE 'MARTES%' ");
			
			}
			break;
		case R.id.wednesday:
			if(wednesday.isChecked() == true) {
				list.add(wednesday.getContentDescription().toString());	
			} else {
				list.remove("OR BARSDE LIKE 'MIERCOLES%' ");
				
			}
			break;
		case R.id.thursday:
			if(thursday.isChecked() == true) {
				list.add(thursday.getContentDescription().toString());	
			} else {
				list.remove("OR BARSDE LIKE 'JUEVES%' ");
			}
			break;
		case R.id.friday:
			if(friday.isChecked() == true) {
				list.add(friday.getContentDescription().toString());	
			} else {
				list.remove("OR BARSDE LIKE 'VIERNES%' ");
			}
			break;
		case R.id.saturday:
			if(saturday.isChecked() == true) {
				list.add(saturday.getContentDescription().toString());	
			} else {
				list.remove("OR BARSDE LIKE 'SABADO%' ");
			}
			break;
		case R.id.sunday:
			if(sunday.isChecked() == true) {
				list.add(sunday.getContentDescription().toString());	
			} else {
				list.remove("OR BARSDE LIKE 'DOMINGO%' ");
			}
			break;
		case R.id.saturday2:
			if(saturday2.isChecked() == true) {
				list.add(saturday2.getContentDescription().toString());	
			} else {
				list.remove("OR BARSDE LIKE '2SABADO%' ");
			}
			break;
		case R.id.sunday2:
			if(sunday2.isChecked() == true) {
				list.add(sunday2.getContentDescription().toString());	
			} else {
				list.remove("OR BARSDE LIKE '2DOMINGO%' ");
			}
			break;
		}
	}
	public String comprobar(String cad){
		String Pb = "";
		if(cad.equals(Pb)){
			cad = "---";
		}
		return cad;	
	}
	
	public String format (int f){
		val = "";
		if(f<10){
		val = "0"+Integer.toString(f);	
		}
		else{
		val = Integer.toString(f);
		}
		return val;
	}
	public void alertDialogMensaje(String message1, String mesage2){
		
		try {
		    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
		    ringtone.play();	    	    
		} catch (Exception e) {
		    e.printStackTrace();
		}
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(message1);
		alertDialog.setMessage(mesage2);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int which) {
		// here you can add functions
			ringtone.stop();
		}
		});
		alertDialog.show();
	}
}
