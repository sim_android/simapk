package com.example.sim;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Vibrator;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.floricultura.R;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.Dazucareras;
import com.example.sim.data.Dguardados;
import com.example.sim.data.JsonFiles;
import com.example.sim.data.Semillas;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class LimpiezaDehisencia extends ActionBarActivity {
	
	//Variables globales
	private MyApp appState;	
	private DBAdapter db;
	int CodigoModulo;
	int Codigodepartamento;
	int CodigoEmpleado;
	int CodigoOpcion;
	int TotalEmpleadosAsignados;
	boolean validoDescriptivo;
	boolean confirma;
	String NombreOpcion;
	LinearLayout validacionesGroup;
	TextView Material;
	TextView TCantidadAsignados;
	EditText Mesa;
	EditText Etiqueta;
	EditText descriptivo;
	EditText empleado;
	EditText Sobre;
	TextView TextviewAzucarera;
	Vibrator mVibrator;
	Ringtone ringtone;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_limpieza_dehisencia);
		
		
		//se obtienen lo datos de la actividad antetior por medio del intent
		Bundle extras = getIntent().getExtras();
		CodigoModulo = extras.getInt("CodigoModulo");
	    CodigoEmpleado = extras.getInt("CodigoEmpleado");
	    Codigodepartamento = extras.getInt("Codigodepartamento");
	    CodigoOpcion = extras.getInt("CodigoOpcion");
	    NombreOpcion = extras.getString("NombreOpcion");
	    
	    
	    //hacemos referencia a todos los objetos de nuestra vista
	    TextView TNombreCaptura = (TextView)findViewById(R.id.OpcionCaptura);
	    TextView TCodigoEmpleado= (TextView)findViewById(R.id.CodigoEmpleado);
	    TextView TCodigoOpcion= (TextView)findViewById(R.id.codigoOpcion);
	    TextviewAzucarera= (TextView)findViewById(R.id.textViewFlores); 
	    
	    TCantidadAsignados = (TextView)findViewById(R.id.textEmpleadosAsginados);
	    validacionesGroup = (LinearLayout)findViewById(R.id.layoutValidaciones);
	    descriptivo = (EditText)findViewById(R.id.editTextDescriptivo);
	    Material = (TextView)findViewById(R.id.textMaterial);
	    Mesa  = (EditText)findViewById(R.id.editMesa);
	    Sobre = (EditText)findViewById(R.id.editSobre);
	    Etiqueta  = (EditText)findViewById(R.id.editEtiqueta);
	    empleado  = (EditText)findViewById(R.id.editEmpleado);
	    Button buttonNuevo = (Button) findViewById(R.id.btnNuevo);
        Button btnBack = (Button) findViewById(R.id.btnBack);
        Button btnInicio = (Button) findViewById(R.id.btnInicio);
	    
	    
	    //hacemos las inicializaciones necesarias a nuestra vista
	    TNombreCaptura.setText(NombreOpcion+ "");
	    TCodigoEmpleado.setText(CodigoEmpleado + "");
	    TCodigoOpcion.setText(CodigoOpcion + "");
	    validoDescriptivo = false;
	    confirma = false;
	    descriptivo.requestFocus();
	
	    
	    //incializamos la BD asi como tambien obtenemos la referencia al singleton
	    appState = ((MyApp)getApplicationContext());   
	    db = appState.getDb();
	    db.open();
	    
	    verificarAsginados();
	    	    
	    //al recibir el enter se verifica si el descriptivo es correcto y lo manda al siguiente input sino le muestra un mensaje al usuario
        descriptivo.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                		VerificarDatos(descriptivo.getText().toString().trim());
	                	if (validoDescriptivo == true){
	                		descriptivo.setEnabled(false);
	                		Etiqueta.requestFocus();
	                	}else{
		    	    		alertDialogMensaje("Bolsa", "Bolsa Invalida");
		    	    		descriptivo.setEnabled(true);
		    	    		descriptivo.setText("");
		    	    		descriptivo.requestFocus();
	                	}
	                	return true;  
                }     
                return false;
            }  
        });
               
	  
           
	    //Se verfica el input azucarera al persionar enter
        Etiqueta.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	// se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
            		confirma = false;
                    ValidaAzucarera(Etiqueta.getText().toString().trim());
                	if(confirma == true){	
            			Etiqueta.setEnabled(false);
            			Mesa.requestFocus();  
            		}else{
            			alertDialogMensaje("Etiqueta", "Error ETIQUETA no Corresponde a BOLSA");
            			Etiqueta.setEnabled(true);
            			Etiqueta.setText("");
            			Etiqueta.requestFocus();
            		}
	                return true;
                }
                return false;
            }
        });
        
      //Se verfica el input azucarera al persionar enter
        Mesa.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	// se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {                	
                	String Ms = Mesa.getText().toString().trim();
                	String Mat = Material.getText().toString().trim();
            		if(Ms.equals(Mat)){
            			Mesa.setEnabled(false);
            			Sobre.requestFocus();  
            		}else{
            			alertDialogMensaje("MESA", "MESA NO corresponde a la Bolsa");
            			Mesa.setEnabled(true);
            			Mesa.setText("");
            			Mesa.requestFocus();
            		}
	                return true;
                }
                return false;
            }
        });
        
        //Se verfica el input azucarera al persionar enter
        Sobre.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	// se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {                	
                	String Bol = descriptivo.getText().toString().trim();
                	String sob = Sobre.getText().toString().trim();
            		if(sob.equals(Bol)){
            			Sobre.setEnabled(false);
            			empleado.requestFocus();  
            		}else{
            			alertDialogMensaje("Sobre", "SOBRE NO corresponde a la Bolsa");
            			Sobre.setEnabled(true);
            			Sobre.setText("");
            			Sobre.requestFocus();
            		}
	                return true;
                }
                return false;
            }
        });
          
        
	    //Se verfica el input empleado al persionar enter
        empleado.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	// se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                	
                	// agrega c�digo de validaci�n 
                validoDescriptivo = false;
                VerificarDatos(descriptivo.getText().toString().trim());
                if (validoDescriptivo == true){
                    confirma = false;
                    ValidaAzucarera(Etiqueta.getText().toString().trim());
                	if(confirma == false){	
                		alertDialogMensaje("Mensaje", "Valor de ETIQUETA es invalido");
                		Etiqueta.setText("");
                		Mesa.setText("");
            			empleado.setText("");
            			Etiqueta.setEnabled(true);
            			Mesa.setEnabled(true);
            			empleado.setEnabled(true);
            			Etiqueta.requestFocus();
                	 }else{
                		String Ms = Mesa.getText().toString().trim();
                        String Mat = Material.getText().toString().trim();
                		if(Ms.equals(Mat)){
                			String Bol = descriptivo.getText().toString().trim();
                        	String sob = Sobre.getText().toString().trim();
                    		if(sob.equals(Bol)){
                    			GuardarDatos();
                	            return true; 	 
                    		}else{
                    			alertDialogMensaje("Sobre", "SOBRE NO corresponde a la Bolsa");
                    			Sobre.setEnabled(true);
                    			Sobre.setText("");
                    			Sobre.requestFocus();	
                    		}
                    		}else{
                    		alertDialogMensaje("Mensaje", "Valor de MESA es invalido");	
                    		Mesa.setText("");
                			empleado.setText("");
                			Mesa.setEnabled(true);
                			empleado.setEnabled(true);
                			Mesa.requestFocus();
                        	}
                    	}
                	}else { 
	                	alertDialogMensaje("ERROR","ERROR EN EL DESCRIPTIVO");
	                	Material.setText("");
	                	descriptivo.setText("");
	                	Mesa.setText("");  
	                	Etiqueta.setText("");        
	   				 	empleado.setText(""); 
	   				 	validoDescriptivo = false; 
	   				 	descriptivo.setEnabled(true);
	   				 	Mesa.setEnabled(true);
	   				 	Etiqueta.setEnabled(true);
	                	descriptivo.requestFocus();
                		} 
                return true; 
            }
            return false;
        } 
    });	 
                     
	    //se limpian los campos y variables para una nueva captura
	    buttonNuevo.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
				 Material.setText(" ");
				 descriptivo.setText("");
				 Mesa.setText("");  
				 Etiqueta.setText("");   
				 empleado.setText(""); 
				 validoDescriptivo = false; 
				 descriptivo.setEnabled(true);
				 Mesa.setEnabled(true);
				 Etiqueta.setEnabled(true);
				 descriptivo.requestFocus();	 
	        }
	    });
        
        btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
 
            	onBackPressed();
            }
        });
                
        btnInicio.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Intent intent = new Intent(getApplicationContext(), MAINACTIVITY.class);
           	 	startActivity(intent);
            }
        });
          
	}
	
	public void verificarAsginados(){
		Cursor CursorGuardados =	db.getDguardados("Actividad=" + CodigoOpcion + " AND ActividadFinalizada=0" );
		TotalEmpleadosAsignados = CursorGuardados.getCount();
		TCantidadAsignados.setText(TotalEmpleadosAsignados + "");
		CursorGuardados.close();
	}
	
	//retornara un valor dependiendo del estado del empleado
	// 0 = no existe
	// 1 = existe pero esta asignado
	// 2 = existe y no esta asignado
	public int VerificarEmpleado(String empleado){
		int valor = 0;
		Cursor CursorDacceso =	db.getDacceso("CodigoEmpleado='" + empleado + "'");
		if(CursorDacceso != null  && CursorDacceso.getCount()>0){
			Cursor CursorGuardados =	db.getDguardados("CodigoEmpleado=" + empleado + " AND ActividadFinalizada=0");
			if(CursorGuardados.getCount()>0){
				valor = 1;
			}else{
				valor = 2;
			}
			CursorGuardados.close();
		}
		CursorDacceso.close();
		return valor;
	}
	
	public void alertDialogMensaje(String message1, String mesage2){
		//mVibrator.vibrate(300);
		try {
		    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
		    ringtone.play(); 
		} catch (Exception e) {
		    e.printStackTrace();
		}
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(message1);
		alertDialog.setMessage(mesage2);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int which) {
		// here you can add functions
			ringtone.stop();
		}
		});
		alertDialog.show();
	}

	public void VerificarDatos(String dato){	
		 Cursor CursorSemillas = db.getSemillas("Siembra='" + dato + "'");
		 if(CursorSemillas.getCount()>0){
			 Semillas s = db.getSemillasFromCursor(CursorSemillas, 0);
			 Log.d("SEMILLA",s.getSiembra() + ", "+s.getMaterial() + " ," + s.getPolinizacion());
			 validoDescriptivo = true;
			 Material.setText(s.getMaterial());
			 Material.setVisibility(View.INVISIBLE);
		 } else {
			 validoDescriptivo = false;
			 Material.setText("");
			 Material.setVisibility(View.INVISIBLE);
		 }
		 CursorSemillas.close();
	}
	
	public void ValidaAzucarera(String vali){
		 Cursor CursorAzucarera = db.getDazucareras("Codigo='" + vali + "'");
		 if(CursorAzucarera.getCount()>0){
			 Dazucareras az = db.getDazucarerasFromCursor(CursorAzucarera, 0);
			 Log.d("AZUCARERA",az.getCodigo() + ", "+az.getSiembra() + " ," + az.getMaterial());
			 String mat = az.getSiembra();
			 String Des = descriptivo.getText().toString().trim();
			 if(mat.equals(Des)){
			 confirma = true;
			 Material.setText(az.getMaterial());
			 }else{
				 confirma = false;
			 }
		   } else {
			 confirma = false;
		 }
		 CursorAzucarera.close();
	}
	

	public void GuardarDatos(){
		String empleadoEscaneado = empleado.getText().toString().trim();
    	int existe = VerificarEmpleado( empleadoEscaneado );
        
    	if(existe == 2){
        		Dguardados dtemp = new Dguardados();
        		dtemp.setCodigoEmpleado(Integer.parseInt(empleadoEscaneado));
        		dtemp.setCodigoDepto(Codigodepartamento);
        		dtemp.setSiembra(descriptivo.getText().toString().trim());
        		
        		//-----------------------------------------------------------------------------------
        		String correlativo="";
        		    	//almaceno el empleado en una variable string
        		    	String getEmpleado = empleado.getText().toString().trim();
        		    	// guardo la variable que tiene empleado en un array
        		    	String[]empl = new String[]{getEmpleado};
        		    	Cursor obtenerCorrelativo = db.getCorrelativo(empl);
        		    	//recorro dicho arreglo para obtener el campo
        		    	if (obtenerCorrelativo.moveToFirst()){
        		    		do{
        		    			correlativo = obtenerCorrelativo.getString(0);
        		    		}while(obtenerCorrelativo.moveToNext());
        		    	}
        		//-------------------------------------------------------------------------
        		
        		Calendar c = Calendar.getInstance(); 
        		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
        		String fecha = fdate.format(c.getTime());
        		SimpleDateFormat ftime = new SimpleDateFormat("HH:mm:ss");
        		String horaInicio = ftime.format(c.getTime());
        		dtemp.setFecha(fecha);
        		dtemp.setHoraIncio(horaInicio);
        		dtemp.setActividad(CodigoOpcion);
        		dtemp.setSupervisor(CodigoEmpleado);
        		dtemp.setCorrelativoBolsa(Etiqueta.getText().toString().trim());	
        		dtemp.setActividadFinalizada(0);
        		dtemp.setCorrelativoEmpleado(correlativo);
        		Log.d("Dguardados", dtemp.toString().trim());
        		
        		db.insertarDguardados(dtemp);
        		
        		//generacion archivo json
        		ObjectMapper mapper = new ObjectMapper();
        		try {
        			String nameJson ="G_" + dtemp.getCodigoEmpleado() + "_"+dtemp.getFecha() + "_"+dtemp.getHoraIncio();
        			nameJson=nameJson.replace(":", "-");	
					mapper.writeValue(new File( Environment.getExternalStorageDirectory()+"/SIM/JsonDescarga/" + nameJson+ ".json"), dtemp);
					
					JsonFiles archivo = new JsonFiles();
					archivo.setName(nameJson+ ".json");
					archivo.setNameFolder("SinFinalizar/");
					archivo.setUpload(0);
					db.insertarJsonFile(archivo);
					
					if(!appState.getSubiendoArchivos()){
						appState.setSubiendoArchivos(true);
						new UptoDropbox().execute(getApplicationContext());
					}
					
        		
        		} catch (JsonGenerationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JsonMappingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        		
        		
        		empleado.setText("");  
        		Mesa.setText("");
        		Etiqueta.setText("");
        		Mesa.setEnabled(true);		
        		Etiqueta.setEnabled(true);
        		Sobre.setEnabled(true);
        		Sobre.setText("");
        	    empleado.requestFocus();
        		TotalEmpleadosAsignados++;
        		TCantidadAsignados.setText(TotalEmpleadosAsignados + "");
        		
        	}else if(existe == 1){
        		alertDialogMensaje("Asignado", "Este usuario ya ha sido asignado");
        		empleado.setText("");
        		empleado.requestFocus();
        	}else if(existe == 0){
        		alertDialogMensaje("Marcaje", "No existe marcaje para este usuario");
        		empleado.setText("");
        		empleado.requestFocus();	
    	}
	}
		
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.captura_general, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}