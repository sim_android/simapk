package com.example.sim;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.Vibrator;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.floricultura.R;
import com.example.sim.data.DBAdapter;

public class DetalleGenesis extends ActionBarActivity {
	
	//Variables globales
		private MyApp appState;	
		private DBAdapter db;
		long Pos;
		String Day;
		String Greenhouse;
		String Year;
		String Week;
		String YearPicker;
		String Empleado;
		GridView gridview;
		GridView gridEnc;
		Vibrator mVibrator;
		Ringtone ringtone;
		TextView day;
		TextView year;
		TextView week;
		TextView greenhouse;
		TextView date;
		Button btnBack;
		Button btnOK;
		Button erase;
		ArrayAdapter<String> adapter;
		ArrayList<String> datos;
		ArrayList<String> encabezado;
		ArrayAdapter<String> adapEnc;
		
		private String val;
		
		@SuppressLint("NewApi") @Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.activity_detalle_genesis);
			
			// para manejar la excepcion de permisos en el API de Android
	    	StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	        StrictMode.setThreadPolicy(policy);

			//se obtienen lo datos de la actividad antetior por medio del intent
			Bundle extras = getIntent().getExtras();
		    Day = extras.getString("Day");
		    Greenhouse = extras.getString("Greenhouse");
		    Year = extras.getString("Year");
		    Week = extras.getString("Weak");  
		    YearPicker = extras.getString("YearPicker");
		   
		    //hacemos referencia a todos los objetos de nuestra vista
		    day = (TextView)findViewById(R.id.day);
		    year = (TextView)findViewById(R.id.year);
		    week = (TextView)findViewById(R.id.week);
		    greenhouse = (TextView)findViewById(R.id.greenhouse);
		    date = (TextView)findViewById(R.id.date);
	        gridEnc = (GridView) findViewById(R.id.gridEnc);
	        gridview = (GridView) findViewById(R.id.gridDetalle);
	        btnBack = (Button)findViewById(R.id.btnBack);
	       
	        
	        datos = new ArrayList<String>();
	        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, datos);
	        encabezado = new ArrayList<String>();
	        adapEnc = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, encabezado);
	        
		    //hacemos las inicializaciones necesarias a nuestra vista
	        day.setText(Day);
	        year.setText(Year);
	        week.setText(Week);
	        greenhouse.setText(Greenhouse);
	        date.setText(YearPicker);
		    
		    //incializamos la BD asi como tambien obtenemos la referencia al singleton
		    appState = ((MyApp)getApplicationContext());   
		    db = appState.getDb();
		    db.open();
		    
		    // Se cargar el Grid con la Funci�n Cargar y se inicializan los valores.
		    Cargar();
         	   	   	  
		    	      
		    btnBack.setOnClickListener(new View.OnClickListener() {
	            public void onClick(View v) {
	 
	            	onBackPressed();
	            }
	        });
	    		    		    
		  	    	      
		}
		public void Cargar() {
			datos.clear();
        	adapter.clear();
        	encabezado.clear();
        	adapEnc.clear();
        	String Pb = "";
        	try {
        		String user = "sim";
            	String pasword = "sim";
            	 Connection connection = null;
            	 ResultSet rs = null;
            	 Statement statement = null;
            	 
					Class.forName("com.ibm.as400.access.AS400JDBCDriver");
					connection = DriverManager.getConnection("jdbc:as400://190.190.190.10", user, pasword);	
					statement = connection.createStatement();	
					rs = statement.executeQuery("Select v43.bardoc,ifnull(ifnull(l1.invernadero,asig.invernadero),replace(replace(v43.barinv,'-',''),' ','')) " +
					"Inv ,v43.barvar,v43.barcns Etiquetas, IFNULL((rtrim(asig.codigo_empleado)||'-'||rtrim(e.nombre)||'-'||rtrim(e.apellido)), '') Empleado," +
					"'BR' ESTADO from sigvegfgu.vege43 v43 left join (Select codigo_empleado,ordencompra,invernadero,cast(etiqueta as integer) Etiqueta from sisappf.app_tmp_etiquetas where fecha between '"+YearPicker+"' AND '"+YearPicker+"'" +
                  "and estado = 'A' and invernadero <> 'EMPAQU' order by etiqueta) asig on v43.barcns = asig.Etiqueta " +
                  "left join (Select ordencompra,invernadero,cast(etiqueta as integer) Etiqueta " + 
                  "from sisappf.app_tmp_etiquetasL1 where fecha  between  '"+YearPicker+"' AND '"+YearPicker+"' and estado = 'A' " + 
                  "and invernadero <> 'EMPAQU' order by etiqueta) l1 on v43.barcns = l1.Etiqueta " +
                  "left join (Select i.temd01 from sigipvfgu.incv618 i left join" +
                  "(Select ordencompra,invernadero,cast(etiqueta as integer) Etiqueta from sisappf.app_tmp_etiquetasL1 " + 
                  " where fecha  between '"+YearPicker+"' AND '"+YearPicker+"' and estado = 'A'" + 
                  " and invernadero <> 'EMPAQU' order by etiqueta) a on i.temd01 = a.etiqueta where temd08 " + 
                  " between  '"+YearPicker+"' AND '"+YearPicker+"' " +
                  " and a.etiqueta is null order by temd01) emp on v43.barcns = emp.temd01 " +
                  " left join sisrrhhf.mempleado e on '0'||asig.codigo_empleado = e.empresa||e.codigoempleado " +
                  " where barano = '"+Year+"' and barsem = '"+Week+"' " +
                  " and barot3 = '2.00' and l1.etiqueta is null and emp.temd01 is null " + 
                  " and ifnull(ifnull(l1.invernadero,asig.invernadero), " +
                  " replace(replace(v43.barinv,'-',''),' ','')) = " + 
                  " replace(replace('"+Greenhouse+"','-',''),' ','') " + 
				  " AND left(BARSDE,3) = '"+Day+"' ");
				
				String Cn1 = rs.getMetaData().getColumnName(1);
				encabezado.add(Cn1);
				gridEnc.setAdapter(adapEnc);
				String Cn2 = rs.getMetaData().getColumnName(2);
				encabezado.add(Cn2);
				gridEnc.setAdapter(adapEnc);
				String Cn3 = rs.getMetaData().getColumnName(3);
				encabezado.add(Cn3);
				gridEnc.setAdapter(adapEnc);
				String Cn4 = rs.getMetaData().getColumnName(4);
				encabezado.add(Cn4);
				gridEnc.setAdapter(adapEnc);
				String Cn5 = rs.getMetaData().getColumnName(5);
				encabezado.add(Cn5);
				gridEnc.setAdapter(adapEnc);
				String Cn6 = rs.getMetaData().getColumnName(6);
				encabezado.add(Cn6);
				
				while (rs.next()) {
					String d1 = rs.getString(1).trim();
					 String d2 = rs.getString(2).trim();
					 String d3 = rs.getString(3).trim();
					 String d4 = rs.getString(4).trim();
					 String d5 = rs.getString(5);
					 String d6 = rs.getString(6).trim();
					 String c1 = comprobar(d1);
					 String c2 = comprobar(d2);
					 String c3 = comprobar(d3);
					 String c4 = comprobar(d4);
					 String c5 = comprobar(d5);
					 String c6 = comprobar(d6);
					 datos.add(c1);
					 gridview.setAdapter(adapter);
					 datos.add(c2);
					 gridview.setAdapter(adapter);
					 datos.add(c3);
					 gridview.setAdapter(adapter);
					 datos.add(c4);
					 gridview.setAdapter(adapter);
					 datos.add(c5);
					 gridview.setAdapter(adapter);
					 datos.add(c6);
					 gridview.setAdapter(adapter);
				}
				
				
				connection.close();	
				
        	}
			catch (Exception e) {
				Toast toast = Toast.makeText(getApplicationContext(), "Problema de Conexi�n.....", Toast.LENGTH_SHORT);
            	toast.show();
				e.printStackTrace();
        } 
       
		}
		
		
		
		
		public void alertDialogMensaje(String message1, String mesage2){
		
			try {
			    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
			    ringtone.play();	    	    
			} catch (Exception e) {
			    e.printStackTrace();
			}
			AlertDialog alertDialog = new AlertDialog.Builder(this).create();
			alertDialog.setTitle(message1);
			alertDialog.setMessage(mesage2);
			alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
			// here you can add functions
				ringtone.stop();
			}
			});
			alertDialog.show();
		}
		
		public String comprobar(String cad){
			String Pb = "";
			if(cad.equals(Pb)){
				cad = "---";
			}
			return cad;	
		}
		
		public String format (int f){
			val = "";
			if(f<10){
			val = "0"+Integer.toString(f);	
			}
			else{
			val = Integer.toString(f);
			}
			return val;
		}
				
		@Override
		public boolean onCreateOptionsMenu(Menu menu) {

			// Inflate the menu; this adds items to the action bar if it is present.
			getMenuInflater().inflate(R.menu.soporteit, menu);
			return true;
		}

		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			// Handle action bar item clicks here. The action bar will
			// automatically handle clicks on the Home/Up button, so long
			// as you specify a parent activity in AndroidManifest.xml.
			int id = item.getItemId();
			if (id == R.id.action_settings) {
				return true;
			}
			return super.onOptionsItemSelected(item);
		}
	
}  
