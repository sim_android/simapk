package com.example.sim.adapters;


import java.util.ArrayList;

import com.example.floricultura.R;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

/**

 */
public class ListAdapter3 extends ArrayAdapter {

    Context context;
    int ViewResourceId;
    ArrayList<DatosLista> Listado;

    public ListAdapter3(Context context, int ViewId, ArrayList<DatosLista> list) {
        super(context, ViewId, list);
        // TODO Auto-generated constructor stub
        this.context = context;
        this.ViewResourceId = ViewId;
        this.Listado = list;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v;
        LayoutInflater inflator = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflator.inflate(ViewResourceId, null, true);

        TextView id= (TextView)v.findViewById(R.id.textId);
        TextView name = (TextView)v.findViewById(R.id.textName);
        TextView opcion = (TextView)v.findViewById(R.id.txtOpcion);
        LinearLayout root= (LinearLayout)v.findViewById(R.id.layoutLista);
        
        if(position == 0){
        	root.setBackgroundColor(Color.parseColor("#2185FF"));
        	id.setTextColor(Color.parseColor("#ffffff"));
        	name.setTextColor(Color.parseColor("#ffffff"));
        	opcion.setTextColor(Color.parseColor("#ffffff"));
        }
        id.setText(Listado.get(position).getCampo1());
        
        name.setText(Listado.get(position).getCampo2());
        opcion.setText(Listado.get(position).getCampo3());

        return v;
    }
}
