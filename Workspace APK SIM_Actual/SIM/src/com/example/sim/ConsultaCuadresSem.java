package com.example.sim;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Calendar;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Color;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.Vibrator;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.floricultura.R;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.Semillas;

public class ConsultaCuadresSem extends ActionBarActivity {
	
	//Variables globales
		private MyApp appState;	
		private DBAdapter db;
		int CodigoModulo;
		int Codigodepartamento;
		int CodigoEmpleado;
		int CodigoOpcion;
		String Siembr;
		GridView gridview;
		GridView gridEnc;
		Vibrator mVibrator;
		Ringtone ringtone;
		String FechaConsulta;
		String Siem;
		EditText Fecha;
		boolean validoDescriptivo;
		private String val;
		
		
		@SuppressLint("NewApi") @Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.consulta_cuadres_sem);  // estas haciendo referencia a otra vista y no encuentra el boton FECHA en esa vista
			
			// para manejar la excepcion de permisos en el API de Android
	    	StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	        StrictMode.setThreadPolicy(policy);

			//se obtienen lo datos de la actividad antetior por medio del intent
			Bundle extras = getIntent().getExtras();
			CodigoModulo = extras.getInt("CodigoModulo");
		    CodigoEmpleado = extras.getInt("CodigoEmpleado");
		    Codigodepartamento = extras.getInt("Codigodepartamento");
		    CodigoOpcion = extras.getInt("CodigoOpcion");
		   		    
		    //hacemos referencia a todos los objetos de nuestra vista
		    TextView TNombreCaptura = (TextView)findViewById(R.id.OpcionCaptura);
		    TextView TCodigoEmpleado= (TextView)findViewById(R.id.CodigoEmpleado);
		    TextView TCodigoOpcion= (TextView)findViewById(R.id.codigoOpcion);
	        Button btnBack = (Button) findViewById(R.id.btnBack);
	        Button btnOK = (Button) findViewById(R.id.btnAsistencia);
	        Button btnFecha = (Button) findViewById(R.id.buttonfechaCons);
	        Button btnBolsas = (Button) findViewById(R.id.buttonBolsas);
	        Button btnRecipientes = (Button) findViewById(R.id.buttonRecipientes);
	        Fecha = (EditText) findViewById(R.id.editFechaCons);
	        Fecha.setEnabled(false);
	        final TableLayout TablaEnc = (TableLayout) findViewById(R.id.TablaDatos);
	          
		    //hacemos las inicializaciones necesarias a nuestra vista		
		    TCodigoEmpleado.setText(CodigoEmpleado + "");
		    TCodigoOpcion.setText(CodigoOpcion + "");
		    
		    //incializamos la BD asi como tambien obtenemos la referencia al singleton
		    appState = ((MyApp)getApplicationContext());   
		    db = appState.getDb();
		    db.open();

		    btnBack.setOnClickListener(new View.OnClickListener() {
	            public void onClick(View v) {
	 
	            	onBackPressed();
	            }
	        });
		    
		    btnFecha.setOnClickListener(new View.OnClickListener() {
	            public void onClick(View v) {
	            	Calendar mcurrentDate=Calendar.getInstance();
	                int mYear = mcurrentDate.get(Calendar.YEAR);
	                int mMonth = mcurrentDate.get(Calendar.MONTH);
	                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

	                DatePickerDialog mDatePicker=new DatePickerDialog(ConsultaCuadresSem.this, new OnDateSetListener() {                  
	                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
	                    	int datodia = selectedday;
	                    	String dia = format(datodia);
	                    	int datomes = selectedmonth + 1;
	                    	String mes = format(datomes);
	                    	String datoanio = Integer.toString(selectedyear);
	                    	String dato = datoanio + "-" + mes + "-" +dia;
	                    	Fecha.setText(dato);
	                    }
	                }
	                ,mYear, mMonth, mDay);
	                mDatePicker.setTitle("Seleccionar Fecha Consulta");                
	                mDatePicker.show();  
	            }
	            
	        });
		   
		    /*
		    btnFechaProy.setOnClickListener(new View.OnClickListener() {
	            public void onClick(View v) {
	            	Calendar mcurrentDate=Calendar.getInstance();
	                int mYear = mcurrentDate.get(Calendar.YEAR);
	                int mMonth = mcurrentDate.get(Calendar.MONTH);
	                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

	                DatePickerDialog mDatePicker=new DatePickerDialog(ConsultaCuadresSem.this, new OnDateSetListener() {                  
	                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
	                    	int datodia = selectedday;
	                    	String dia = format(datodia);
	                    	int datomes = selectedmonth + 1;
	                    	String mes = format(datomes);
	                    	String datoanio = Integer.toString(selectedyear);
	                    	String dato = datoanio + "-" + mes + "-" +dia;
	                    	editFechaProy.setText(dato);
	                    }
	                }
	                ,mYear, mMonth, mDay);
	                mDatePicker.setTitle("Seleccionar Fecha Proyecccion");                
	                mDatePicker.show();  
	            }
	            
	        });
		    */
		    /*
		  //al recibir el enter se verifica si el descriptivo es correcto y lo manda al siguiente input sino le muestra un mensaje al usuario
	        Siembra.setOnKeyListener(new View.OnKeyListener() {
	            public boolean onKey(View v, int keyCode, KeyEvent event) {
	                // se manda a llamar cuando se presione Enter
	                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
	                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
	                	VerificarDatos(Siembra.getText().toString().trim());
	                	if (validoDescriptivo == true){
	                		
	                	TablaEnc.removeAllViews();	
		            	Siem = Siembra.getText().toString().trim();
		            	FechaDisp = editFechaDisp.getText().toString().trim();
		            	FechaProye = editFechaProy.getText().toString().trim();
		            	String Pb = "";
		            	
		            	if(FechaDisp.equals(Pb) ){
		            		alertDialogMensaje("ERROR","PORFAVOR SELECCIONE UNA FECHA DE DISPONIBILIDAD");
		            		Siembra.setText("");
		            		editFechaDisp.setText("");
		            		Siembra.requestFocus();
		            	} else if(FechaProye.equals(Pb)){
		            		alertDialogMensaje("ERROR","PORFAVOR SELECCIONE UNA FECHA DE PROYECCION");
		            		Siembra.setText("");
		            		editFechaProy.setText("");
		            		Siembra.requestFocus();
		            	}
		            	else 
		            	{
		            	try {
		            	String user = "sim";
		            	String pasword = "sim";
		            	 Connection connection = null;
		            	 ResultSet rs = null;
		            	 Statement statement = null;
		            	 String es = " | ";
		            	 
							Class.forName("com.ibm.as400.access.AS400JDBCDriver");
							connection = DriverManager.getConnection("jdbc:as400://190.190.190.10", user, pasword);	
							statement = connection.createStatement();			     
							rs = statement.executeQuery(
							"SELECT G.ACTIVIDAD ACT, SOM.DESCRIPCION DESC,G.SIEMBRA SIEM,(ifnull(sie.plantas,0) + ifnull(d.desecho,0)) PLTS, SUM(ifnull(G.PLANTAS,0)) TRABAJ, " +
							"(SUM(ifnull(G.PLANTAS,0)) - (ifnull(sie.plantas,0) + ifnull(d.desecho,0))) DIF,sie.invernadero INV FROM SISAPPF.APP_TMP_GENERAL g LEFT JOIN " +
							"SISCFGF.SEGURIDAD_OPCION_MODULOASP SOM ON G.ACTIVIDAD = SOM.OPCION LEFT JOIN SISSEMF.MOV_DEFSIEMBRAE SIE  ON G.SIEMBRA = SIE.NOSIEMBRA  inner join " +
							"(Select correlativoempleado,invernadero from sisappf.app_seguridad_consulta_sem where estado = 'AC') sg on sie.invernadero = '' || sg.invernadero " +
							"left join (select empresa,siembra,sum(plantas) Desecho from sissemf.mov_desechoplantasem where " +
							"fecha > '' group by empresa,siembra) d on g.siembra = d.siembra where (G.ESTADO IS NULL OR G.ESTADO IN ('AC','CO')) AND " +
							"G.FECHA = '' AND  G.actividadfinalizada = 1 AND SOM.CONTROL_PLANTAS = 'SI'  and  " +
							"sg.correlativoempleado = '"+CodigoEmpleado+"' AND G.SIEMBRA = ''  GROUP BY G.ACTIVIDAD,SOM.DESCRIPCION,G.SIEMBRA,SIE.PLANTAS,sie.invernadero,d.desecho " +
							"ORDER BY G.ACTIVIDAD,g.siembra,sie.plantas");
			
							//Agregar encabezados de columnas	
							 TableRow Erow1 = new TableRow(ConsultaCuadresSem.this);
							 TextView E1 = new TextView(ConsultaCuadresSem.this);
							 TextView E2 = new TextView(ConsultaCuadresSem.this);
							 TextView E3 = new TextView(ConsultaCuadresSem.this);
							 TextView E4 = new TextView(ConsultaCuadresSem.this);
							 TextView E5 = new TextView(ConsultaCuadresSem.this);
							 TextView E6 = new TextView(ConsultaCuadresSem.this);
							 TextView E7 = new TextView(ConsultaCuadresSem.this);
							 TextView E8 = new TextView(ConsultaCuadresSem.this); 
							 TextView E9 = new TextView(ConsultaCuadresSem.this); 
							 TextView E10 = new TextView(ConsultaCuadresSem.this); 
							 TextView E11 = new TextView(ConsultaCuadresSem.this); 
							 TextView E12 = new TextView(ConsultaCuadresSem.this); 
							 TextView E13 = new TextView(ConsultaCuadresSem.this);
							 E1.setTextSize(15);
							 E1.setTextColor(Color.BLACK);
							 E2.setTextSize(15);
							 E2.setTextColor(Color.BLACK);
							 E3.setTextSize(15);
							 E3.setTextColor(Color.BLACK);
							 E4.setTextSize(15);
							 E4.setTextColor(Color.BLACK);
							 E5.setTextSize(15);
							 E5.setTextColor(Color.BLACK);
							 E6.setTextSize(15);
							 E6.setTextColor(Color.BLACK);
							 E7.setTextSize(15);
							 E7.setTextColor(Color.BLACK);
							 E8.setTextSize(20);
							 E8.setTextColor(Color.BLACK);
							 E9.setTextSize(20);
							 E9.setTextColor(Color.BLACK);
							 E10.setTextSize(20);
							 E10.setTextColor(Color.BLACK);
							 E11.setTextSize(20);
							 E11.setTextColor(Color.BLACK);
							 E12.setTextSize(20);
							 E12.setTextColor(Color.BLACK);
							 E13.setTextSize(20);
							 String Cn1 = rs.getMetaData().getColumnName(1);
							 String Cn2 = rs.getMetaData().getColumnName(2);
							 String Cn3 = rs.getMetaData().getColumnName(3);
							 String Cn4 = rs.getMetaData().getColumnName(4);
							 String Cn5 = rs.getMetaData().getColumnName(5);
							 String Cn6 = rs.getMetaData().getColumnName(6);
							 String Cn7 = rs.getMetaData().getColumnName(7);
							 E1.setText(Cn1);
							 E2.setText(Cn2);
							 E3.setText(Cn3);
							 E4.setText(Cn4);
							 E5.setText(Cn5);
							 E6.setText(Cn6);
							 E7.setText(Cn7);
							 E8.setText(es);
							 E9.setText(es);
							 E10.setText(es);
							 E11.setText(es);
							 E12.setText(es);
							 E13.setText(es);
							 Erow1.addView(E1);
							 Erow1.addView(E8);
							 Erow1.addView(E2);
							 Erow1.addView(E9);
							 Erow1.addView(E3);
							 Erow1.addView(E10);
							 Erow1.addView(E4);
							 Erow1.addView(E11);
							 Erow1.addView(E5);
							 Erow1.addView(E12);
							 Erow1.addView(E6);
							 Erow1.addView(E13);
							 Erow1.addView(E7);
							 E1.setGravity(Gravity.CENTER_HORIZONTAL);
							 E2.setGravity(Gravity.CENTER_HORIZONTAL);
							 E3.setGravity(Gravity.CENTER_HORIZONTAL);
							 E4.setGravity(Gravity.CENTER_HORIZONTAL);
							 E5.setGravity(Gravity.CENTER_HORIZONTAL);
							 E6.setGravity(Gravity.CENTER_HORIZONTAL);
							 E7.setGravity(Gravity.CENTER_HORIZONTAL);
							 E8.setGravity(Gravity.CENTER_HORIZONTAL);
							 E9.setGravity(Gravity.CENTER_HORIZONTAL);
							 E10.setGravity(Gravity.CENTER_HORIZONTAL);
							 E11.setGravity(Gravity.CENTER_HORIZONTAL);
							 E12.setGravity(Gravity.CENTER_HORIZONTAL);
							 E13.setGravity(Gravity.CENTER_HORIZONTAL);
							 Erow1.setGravity(Gravity.CENTER);
							 Erow1.setBackgroundColor(Color.GREEN);
							 TablaEnc.addView(Erow1);	
							// finaliza agregar encabezados de columnas 
							int sz = rs.getFetchSize();
							int cont = 0;
							while (rs.next()) {
								 String d1 = rs.getString(1).trim();
								 String d2 = rs.getString(2).trim();
								 String d3 = rs.getString(3).trim();
								 String d4 = rs.getString(4).trim();
							     String d5 = rs.getString(5).trim();
								 String d6 = rs.getString(6).trim();
								 String d7 = rs.getString(7).trim();
								 String c1 = comprobar(d1);
								 String c2 = comprobar(d2);
								 String c3 = comprobar(d3);
								 String c4 = comprobar(d4);
								 String c5 = comprobar(d5);
								 String c6 = comprobar(d6);
								 String c7 = comprobar(d7);
								 for (int i = 0; i == sz; i++)
								 {	
									 cont = cont +1;
									 TableRow row1 = new TableRow(ConsultaCuadresSem.this);
									 TextView t1 = new TextView(ConsultaCuadresSem.this);
									 TextView t2 = new TextView(ConsultaCuadresSem.this);
									 TextView t3 = new TextView(ConsultaCuadresSem.this);
									 TextView t4 = new TextView(ConsultaCuadresSem.this);
									 TextView t5 = new TextView(ConsultaCuadresSem.this);
									 TextView t6 = new TextView(ConsultaCuadresSem.this);
									 TextView t7 = new TextView(ConsultaCuadresSem.this);
									 TextView t8 = new TextView(ConsultaCuadresSem.this); 
									 TextView t9 = new TextView(ConsultaCuadresSem.this); 
									 TextView t10 = new TextView(ConsultaCuadresSem.this); 
									 TextView t11 = new TextView(ConsultaCuadresSem.this); 
									 TextView t12 = new TextView(ConsultaCuadresSem.this); 
									 TextView t13 = new TextView(ConsultaCuadresSem.this);
									 t1.setTextSize(13);
									 t1.setTextColor(Color.BLACK);
									 t2.setTextSize(13);
									 t2.setTextColor(Color.BLACK);
									 t3.setTextSize(13);
									 t3.setTextColor(Color.BLACK);
									 t4.setTextSize(13);
									 t4.setTextColor(Color.BLACK);
									 t5.setTextSize(13);
									 t5.setTextColor(Color.BLACK);
									 t6.setTextSize(13);
									 t6.setTextColor(Color.BLACK);
									 t7.setTextSize(13);
									 t7.setTextColor(Color.BLACK);
									 t8.setTextSize(20);
									 t8.setTextColor(Color.BLACK);
									 t9.setTextSize(20);
									 t9.setTextColor(Color.BLACK);
									 t10.setTextSize(20);
									 t10.setTextColor(Color.BLACK);
									 t11.setTextSize(20);
									 t11.setTextColor(Color.BLACK);
									 t12.setTextSize(20);
									 t12.setTextColor(Color.BLACK);
									 t13.setTextSize(20);
									 t1.setGravity(Gravity.CENTER_HORIZONTAL);
									 t2.setGravity(Gravity.CENTER_HORIZONTAL);
									 t3.setMaxWidth(150);
									 t4.setMaxWidth(150);
									 t5.setMaxWidth(100);
									 t4.setGravity(Gravity.CENTER_HORIZONTAL);
									 t5.setGravity(Gravity.CENTER_HORIZONTAL);
									 t6.setGravity(Gravity.CENTER_HORIZONTAL);
									 t7.setGravity(Gravity.CENTER_HORIZONTAL);
									 t8.setGravity(Gravity.CENTER_HORIZONTAL);
									 t9.setGravity(Gravity.CENTER_HORIZONTAL);
									 t10.setGravity(Gravity.CENTER_HORIZONTAL);
									 t11.setGravity(Gravity.CENTER_HORIZONTAL);
									 t12.setGravity(Gravity.CENTER_HORIZONTAL);
									 t13.setGravity(Gravity.CENTER_HORIZONTAL);
									 row1.setGravity(Gravity.CENTER);
									 row1.setClickable(true);
									 if(cont%2 == 0){
									 row1.setBackgroundColor(Color.LTGRAY);
									 }else {
									 row1.setBackgroundColor(Color.WHITE);	 
									 }
								 t1.setText(c1);
								 t2.setText(c2);
								 t3.setText(c3);
								 t4.setText(c4);
								 t5.setText(c5);
								 t6.setText(c6);
								 t7.setText(c7);
								 t8.setText(es);
								 t9.setText(es);
								 t10.setText(es);
								 t11.setText(es);
								 t12.setText(es);
								 t13.setText(es);
								 row1.addView(t1);
								 row1.addView(t8);
								 row1.addView(t2);
								 row1.addView(t9);
								 row1.addView(t3);
								 row1.addView(t10);
								 row1.addView(t4);
								 row1.addView(t11);
								 row1.addView(t5);
								 row1.addView(t12);
								 row1.addView(t6);
								 row1.addView(t13);
								 row1.addView(t7);
								 TablaEnc.addView(row1);
								 TablaEnc.setClickable(true);
								 }
								 
							}
							connection.close();	
		            	}
						catch (Exception e) {
							Toast toast = Toast.makeText(getApplicationContext(), "Verificar Conexi�n.....", Toast.LENGTH_SHORT);
			            	toast.show();
							e.printStackTrace();
						} 
		            } 	   	
	              }else{
		    	    alertDialogMensaje("Descriptivo", "Descriptivo Invalido \n \n  Revisar Descriptivo");
		    	    Siembra.setText("");
		    	    Siembra.requestFocus();
	              }
	                return true;  
	               }     
	              return false;
	            }  
	        });
		    */	
	        
		    btnBolsas.setOnClickListener(new View.OnClickListener() {
	            public void onClick(View v) {
	            	TablaEnc.removeAllViews();	
	            	// en la variable se esta guardando el valor que se observa en la tab (fecha)
	            	FechaConsulta = Fecha.getText().toString().trim();
	            	//FechaProye = editFechaProy.getText().toString().trim();
	            	String Pb = "";
	            	
	            	if(FechaConsulta.equals(Pb) ){
	            		alertDialogMensaje("ERROR","PORFAVOR SELECCIONE UNA FECHA DE CONSULTA");
	            		Fecha.setText("");
	            	}
	            	else 
	            	{
	            		try {
			            	String user = "sim";
			            	String pasword = "sim";
			            	 Connection connection = null;
			            	 ResultSet rs = null;
			            	 Statement statement = null;
			            	 String es = " | ";
			            	 
								Class.forName("com.ibm.as400.access.AS400JDBCDriver");
								connection = DriverManager.getConnection("jdbc:as400://190.190.190.10", user, pasword);	
								statement = connection.createStatement();			     
								rs = statement.executeQuery(
								//aca es el qry de bolsas
										
										"Select i.nombrecortoinvernadero inv,b.siembra" +
             ",b.EtiquetasEnviadas Bolsas,sum(a.noetiquetas) as R_Llenas,sum(a.EtiquetasVacias) as R_Vacias" +
             ",b.etiquetasenviadas - sum(a.noetiquetas) - sum(a.EtiquetasVacias) as Diferencia,sum(a.pesofresco) pesofresco " +
             " from " +
             "(Select et.NoSiembra,et.empresa" +
             ",case when et.estado = 'BR' and et.llena = 'S' then count(et.codigo) else 0 end as NoEtiquetas " +
             ",case when et.llena = 'N' then count(et.codigo) else 0 end as EtiquetasVacias,sum(et.pesofresco) pesofresco " +
             " from sissemf.mov_etiquetasc et " +
             " where et.empresa = '01' " +
             " and et.fechacosecha =  '"+FechaConsulta+"' " +
             " group by et.nosiembra,et.estado,et.llena,et.empresa) a " +
             " inner Join " +
             " (select m.siembra,m.invernadero,m.empresa,m.estado,case when m.estado = 'BE' then sum(m.Cantidad) end as EtiquetasEnviadas " +
             " from sissemf.Mov_bolsaslec1 m " + 
             " where m.empresa = '01' and m.fechacosecha =  '"+FechaConsulta+"' " +
             " and m.estado = 'BE' " +
             " group by m.estado,m.siembra,m.empresa,m.invernadero) b " +
             " on a.empresa = b.empresa and a.nosiembra = b.siembra " +
" LEFT JOIN sismcsf.MINVERNADEROS I ON b.INVERNADERO = I.CODIGOINVERNADERO " +
" inner join (Select correlativoempleado,invernadero from sisappf.app_seguridad_consulta_sem where estado = 'AC') s on i.codigoinvernadero = s.invernadero " +
" where s.correlativoempleado = '"+CodigoEmpleado+"' " +
             " group by b.siembra,b.estado,b.EtiquetasEnviadas,i.nombrecortoinvernadero " + 
" order by i.nombrecortoinvernadero,b.siembra");
										
										
								//		"SELECT G.ACTIVIDAD ACT, SOM.DESCRIPCION DESC,G.SIEMBRA SIEM,(ifnull(sie.plantas,0) + ifnull(d.desecho,0)) PLTS, SUM(ifnull(G.PLANTAS,0)) TRABAJ, " +
								//"(SUM(ifnull(G.PLANTAS,0)) - (ifnull(sie.plantas,0) + ifnull(d.desecho,0))) DIF,sie.invernadero INV FROM SISAPPF.APP_TMP_GENERAL g LEFT JOIN " +
								//"SISCFGF.SEGURIDAD_OPCION_MODULOASP SOM ON G.ACTIVIDAD = SOM.OPCION LEFT JOIN SISSEMF.MOV_DEFSIEMBRAE SIE  ON G.SIEMBRA = SIE.NOSIEMBRA  inner join " +
								//"(Select correlativoempleado,invernadero from sisappf.app_seguridad_consulta_sem where estado = 'AC') sg on sie.invernadero = '' || sg.invernadero " +
								//"left join (select empresa,siembra,sum(plantas) Desecho from sissemf.mov_desechoplantasem where " +
								//"fecha > '' group by empresa,siembra) d on g.siembra = d.siembra where (G.ESTADO IS NULL OR G.ESTADO IN ('AC','CO')) AND " +
								//"G.FECHA = '' AND  G.actividadfinalizada = 1 AND SOM.CONTROL_PLANTAS = 'SI'  and  " +
								//"sg.correlativoempleado = '"+CodigoEmpleado+"' AND G.SIEMBRA = ''  GROUP BY G.ACTIVIDAD,SOM.DESCRIPCION,G.SIEMBRA,SIE.PLANTAS,sie.invernadero,d.desecho " +
								//"ORDER BY G.ACTIVIDAD,g.siembra,sie.plantas");
				
								//Agregar encabezados de columnas
								// aca se definen las columnas en este caso se definieron 13 porque cada columna lleva una que se usa como separador a un costado
								// realmente el query solo tiene 7 entonces esta as�  col1 '|'  col2 | col3 |    el '|' es el separador que es una columna 
								 TableRow Erow1 = new TableRow(ConsultaCuadresSem.this);
								 TextView E1 = new TextView(ConsultaCuadresSem.this);
								 TextView E2 = new TextView(ConsultaCuadresSem.this);
								 TextView E3 = new TextView(ConsultaCuadresSem.this);
								 TextView E4 = new TextView(ConsultaCuadresSem.this);
								 TextView E5 = new TextView(ConsultaCuadresSem.this);
								 TextView E6 = new TextView(ConsultaCuadresSem.this);
								 //TextView E7 = new TextView(ConsultaCuadresSem.this);
								 TextView E8 = new TextView(ConsultaCuadresSem.this); 
								 TextView E9 = new TextView(ConsultaCuadresSem.this); 
								 TextView E10 = new TextView(ConsultaCuadresSem.this); 
								 TextView E11 = new TextView(ConsultaCuadresSem.this); 
								 TextView E12 = new TextView(ConsultaCuadresSem.this); 
								 //TextView E13 = new TextView(ConsultaCuadresSem.this);
								 E1.setTextSize(15);
								 E1.setTextColor(Color.BLACK);
								 E2.setTextSize(15);
								 E2.setTextColor(Color.BLACK);
								 E3.setTextSize(15);
								 E3.setTextColor(Color.BLACK);
								 E4.setTextSize(15);
								 E4.setTextColor(Color.BLACK);
								 E5.setTextSize(15);
								 E5.setTextColor(Color.BLACK);
								E6.setTextSize(15);
								 E6.setTextColor(Color.BLACK);
								 //E7.setTextSize(15);
								 //E7.setTextColor(Color.BLACK);
								 E8.setTextSize(20);
								 E8.setTextColor(Color.BLACK);
								 E9.setTextSize(20);
								 E9.setTextColor(Color.BLACK);
								 E10.setTextSize(20);
								 E10.setTextColor(Color.BLACK);
								 E11.setTextSize(20);
								 E11.setTextColor(Color.BLACK);
								 E12.setTextSize(20);
								 E12.setTextColor(Color.BLACK);
								 //E13.setTextSize(20);
								 // Se obtienen del Query los nombres de las columnas y se agregan a una variable Cn1,2,3...  Column Name1 ...
								 String Cn1 = rs.getMetaData().getColumnName(1);
								 String Cn2 = rs.getMetaData().getColumnName(2);
								 String Cn3 = rs.getMetaData().getColumnName(3);
								 String Cn4 = rs.getMetaData().getColumnName(4);
								 String Cn5 = rs.getMetaData().getColumnName(5);
								 String Cn6 = rs.getMetaData().getColumnName(6);
								 //String Cn7 = rs.getMetaData().getColumnName(7);
								 E1.setText(Cn1);
								 E2.setText(Cn2);
								 E3.setText(Cn3);
								 E4.setText(Cn4);
								 E5.setText(Cn5);
								 E6.setText(Cn6);
								 //E7.setText(Cn7);
								 // la variable 'es' contiene el separador, en este caso el '|'  que se puede personalizar al gusto
								 E8.setText(es);
								 E9.setText(es);
								 E10.setText(es);
								 E11.setText(es);
								 E12.setText(es);
								 //E13.setText(es);
								 Erow1.addView(E1);
								 Erow1.addView(E8);
								 Erow1.addView(E2);
								 Erow1.addView(E9);
								 Erow1.addView(E3);
								 Erow1.addView(E10);
								 Erow1.addView(E4);
								 Erow1.addView(E11);
								 Erow1.addView(E5);
								 Erow1.addView(E12);
								 Erow1.addView(E6);
								 //Erow1.addView(E13);
								 //Erow1.addView(E7);
								 E1.setGravity(Gravity.CENTER_HORIZONTAL);
								 E2.setGravity(Gravity.CENTER_HORIZONTAL);
								 E3.setGravity(Gravity.CENTER_HORIZONTAL);
								 E4.setGravity(Gravity.CENTER_HORIZONTAL);
								 E5.setGravity(Gravity.CENTER_HORIZONTAL);
								 E6.setGravity(Gravity.CENTER_HORIZONTAL);
								 //E7.setGravity(Gravity.CENTER_HORIZONTAL);
								 E8.setGravity(Gravity.CENTER_HORIZONTAL);
								 E9.setGravity(Gravity.CENTER_HORIZONTAL);
								 E10.setGravity(Gravity.CENTER_HORIZONTAL);
								 E11.setGravity(Gravity.CENTER_HORIZONTAL);
								 E12.setGravity(Gravity.CENTER_HORIZONTAL);
								 //E13.setGravity(Gravity.CENTER_HORIZONTAL);
								 Erow1.setGravity(Gravity.CENTER);
								 Erow1.setBackgroundColor(Color.GREEN);
								 TablaEnc.addView(Erow1);	
								// finaliza agregar encabezados de columnas 
								int sz = rs.getFetchSize();
								int cont = 0;
								while (rs.next()) {  // rs.next  se encarga de hacer un recorrido en cada linea devuelta por el query
									// se agregan las columnas para el cuerpo de la tabla... mismo proceso que los encabezados  
									 String d1 = rs.getString(1).trim();
									 String d2 = rs.getString(2).trim();
									 String d3 = rs.getString(3).trim();
									 String d4 = rs.getString(4).trim();
								     String d5 = rs.getString(5).trim();
									 String d6 = rs.getString(6).trim();
									 //String d7 = rs.getString(7).trim();
									 String c1 = comprobar(d1);
									 String c2 = comprobar(d2);
									 String c3 = comprobar(d3);
									 String c4 = comprobar(d4);
									 String c5 = comprobar(d5);
									 String c6 = comprobar(d6);
									 //String c7 = comprobar(d7);
									 for (int i = 0; i == sz; i++)
									 {	
										 cont = cont +1;
										 TableRow row1 = new TableRow(ConsultaCuadresSem.this);
										 TextView t1 = new TextView(ConsultaCuadresSem.this);
										 TextView t2 = new TextView(ConsultaCuadresSem.this);
										 TextView t3 = new TextView(ConsultaCuadresSem.this);
										 TextView t4 = new TextView(ConsultaCuadresSem.this);
										 TextView t5 = new TextView(ConsultaCuadresSem.this);
										 TextView t6 = new TextView(ConsultaCuadresSem.this);
										 //TextView t7 = new TextView(ConsultaCuadresSem.this);
										 TextView t8 = new TextView(ConsultaCuadresSem.this); 
										 TextView t9 = new TextView(ConsultaCuadresSem.this); 
										 TextView t10 = new TextView(ConsultaCuadresSem.this); 
										 TextView t11 = new TextView(ConsultaCuadresSem.this); 
										 TextView t12 = new TextView(ConsultaCuadresSem.this); 
										 //TextView t13 = new TextView(ConsultaCuadresSem.this);
										 t1.setTextSize(13);
										 t1.setTextColor(Color.BLACK);
										 t2.setTextSize(13);
										 t2.setTextColor(Color.BLACK);
										 t3.setTextSize(13);
										 t3.setTextColor(Color.BLACK);
										 t4.setTextSize(13);
										 t4.setTextColor(Color.BLACK);
										 t5.setTextSize(13);
										 t5.setTextColor(Color.BLACK);
										 t6.setTextSize(13);
										 t6.setTextColor(Color.BLACK);
										 //t7.setTextSize(13);
										 //t7.setTextColor(Color.BLACK);
										 t8.setTextSize(20);
										 t8.setTextColor(Color.BLACK);
										 t9.setTextSize(20);
										 t9.setTextColor(Color.BLACK);
										 t10.setTextSize(20);
										 t10.setTextColor(Color.BLACK);
										 t11.setTextSize(20);
										 t11.setTextColor(Color.BLACK);
										 t12.setTextSize(20);
										 t12.setTextColor(Color.BLACK);
										 //t13.setTextSize(20);
										 t1.setGravity(Gravity.CENTER_HORIZONTAL);
										 t2.setGravity(Gravity.CENTER_HORIZONTAL);
										 t3.setMaxWidth(150);
										 t4.setMaxWidth(150);
										 t5.setMaxWidth(100);
										 t4.setGravity(Gravity.CENTER_HORIZONTAL);
										 t5.setGravity(Gravity.CENTER_HORIZONTAL);
										 t6.setGravity(Gravity.CENTER_HORIZONTAL);
										 //t7.setGravity(Gravity.CENTER_HORIZONTAL);
										 t8.setGravity(Gravity.CENTER_HORIZONTAL);
										 t9.setGravity(Gravity.CENTER_HORIZONTAL);
										 t10.setGravity(Gravity.CENTER_HORIZONTAL);
										 t11.setGravity(Gravity.CENTER_HORIZONTAL);
										 t12.setGravity(Gravity.CENTER_HORIZONTAL);
										 //t13.setGravity(Gravity.CENTER_HORIZONTAL);
										 row1.setGravity(Gravity.CENTER);
										 row1.setClickable(true);
										 if(cont%2 == 0){
										 row1.setBackgroundColor(Color.LTGRAY);
										 }else {
										 row1.setBackgroundColor(Color.WHITE);	 
										 }
									 t1.setText(c1);
									 t2.setText(c2);
									 t3.setText(c3);
									 t4.setText(c4);
									 t5.setText(c5);
									 t6.setText(c6);
									 //t7.setText(c7);
									 t8.setText(es);
									 t9.setText(es);
									 t10.setText(es);
									 t11.setText(es);
									 t12.setText(es);
									 //t13.setText(es);
									 row1.addView(t1);
									 row1.addView(t8);
									 row1.addView(t2);
									 row1.addView(t9);
									 row1.addView(t3);
									 row1.addView(t10);
									 row1.addView(t4);
									 row1.addView(t11);
									 row1.addView(t5);
									 row1.addView(t12);
									 row1.addView(t6);
									 //row1.addView(t13);
									 //row1.addView(t7);
									 TablaEnc.addView(row1);
									 TablaEnc.setClickable(true);
									 }
									 
								}
						connection.close();	
	            	}
					catch (Exception e) {
						Toast toast = Toast.makeText(getApplicationContext(), "Verificar Conexi�n.....", Toast.LENGTH_SHORT);
		            	toast.show();
						e.printStackTrace();
	            } 
	           } 	
	            }
	        });
		       
		
			
		 btnRecipientes.setOnClickListener(new View.OnClickListener() {
	            public void onClick(View v) {
	            	TablaEnc.removeAllViews();	      
	            	FechaConsulta = Fecha.getText().toString().trim();
	            	//FechaProye = editFechaProy.getText().toString().trim();
	            	String Pb = "";
	            	
	            	if(FechaConsulta.equals(Pb) ){
	            		alertDialogMensaje("ERROR","PORFAVOR SELECCIONE UNA FECHA DE CONSULTA");
	            		Fecha.setText("");
	            	}
	            	else 
	            	{
	            		try {
			            	String user = "sim";
			            	String pasword = "sim";
			            	 Connection connection = null;
			            	 ResultSet rs = null;
			            	 Statement statement = null;
			            	 String es = " | ";
			            	 
								Class.forName("com.ibm.as400.access.AS400JDBCDriver");
								connection = DriverManager.getConnection("jdbc:as400://190.190.190.10", user, pasword);	
								statement = connection.createStatement();			     
								rs = statement.executeQuery(
								//aca es el qry de bolsas
										
										
										" select distinct si.invernadero Inv,en.nosiembra Siembra" +
										" ,e.enviadas Recipientes" +
										 " ,case when r.recibidas is null then 0 else r.recibidas end as Recibidos " +
										         " ,(e.enviadas) - (case when r.recibidas is null then 0 else r.recibidas end) as Pendiente " +
										        " , si.Descripciontipoproduccion T_Produccion " +
										         " from sispolf.mov_enviorecipiente as en " +
										         " INNER JOIN (SELECT s.invernadero inv,S.NOSIEMBRA,I.NOMBRECORTOINVERNADERO INVERNADERO,M.DESCRIPCIONMATERIAL VARIEDAD,tp.Descripciontipoproduccion,m.hibrido " +
										         " FROM sissemf.MOV_DEFSIEMBRAE S " +
										         " LEFT JOIN sissemf.MMATERIALSEM M ON S.MATERIAL = M.CODIGOMATERIAL " +
										         " left join sissemf.mtipoproduccion tp on s.tiposiembra = tp.CodigoTipoProduccion " +
										         " LEFT JOIN sismcsf.MINVERNADEROS I ON S.INVERNADERO = I.CODIGOINVERNADERO) SI ON EN.NOSIEMBRA = SI.NOSIEMBRA " +
										         " left join (select en.nosiembra,COUNT(en.CORRELATIVO) AS Recibidas,en.tipoprueba " +
										         " from sispolf.mov_enviorecipiente en " +
										         " Where en.fechasuccion = '"+FechaConsulta+"' and en.estado = 'BR' and EN.Empresa = '01' " +
										         " GROUP BY EN.NOSIEMBRA,EN.TIPOPRUEBA " +
										         " ) as r on en.nosiembra = r.nosiembra " +
										         " left join (select en.nosiembra,COUNT(en.CORRELATIVO) AS Enviadas,en.tipoprueba " +
										         " from sispolf.mov_enviorecipiente en " +
										         " Where en.fechasuccion = '"+FechaConsulta+"' and en.estado in ('BE','BR') and EN.Empresa = '01' " +
										         " GROUP BY EN.NOSIEMBRA,EN.TIPOPRUEBA " +
										         " ) as e on en.nosiembra = e.nosiembra " +
										" inner join (Select correlativoempleado,invernadero from sisappf.app_seguridad_consulta_sem where estado = 'AC') s on ''||si.inv = ''||s.invernadero " +
										         " Where en.fechasuccion = '"+FechaConsulta+"' and en.estado IN ('BE','BR') and EN.Empresa = '01' " +
										" and s.correlativoempleado = '"+CodigoEmpleado+"' " +
										         " Order by si.invernadero,EN.nosiembra desc");
										
										
										//"SELECT G.ACTIVIDAD ACT, SOM.DESCRIPCION DESC,G.SIEMBRA SIEM,(ifnull(sie.plantas,0) + ifnull(d.desecho,0)) PLTS, SUM(ifnull(G.PLANTAS,0)) TRABAJ, " +
								//"(SUM(ifnull(G.PLANTAS,0)) - (ifnull(sie.plantas,0) + ifnull(d.desecho,0))) DIF,sie.invernadero INV FROM SISAPPF.APP_TMP_GENERAL g LEFT JOIN " +
								//"SISCFGF.SEGURIDAD_OPCION_MODULOASP SOM ON G.ACTIVIDAD = SOM.OPCION LEFT JOIN SISSEMF.MOV_DEFSIEMBRAE SIE  ON G.SIEMBRA = SIE.NOSIEMBRA  inner join " +
								//"(Select correlativoempleado,invernadero from sisappf.app_seguridad_consulta_sem where estado = 'AC') sg on sie.invernadero = '' || sg.invernadero " +
								//"left join (select empresa,siembra,sum(plantas) Desecho from sissemf.mov_desechoplantasem where " +
								//"fecha > '' group by empresa,siembra) d on g.siembra = d.siembra where (G.ESTADO IS NULL OR G.ESTADO IN ('AC','CO')) AND " +
								//"G.FECHA = '' AND  G.actividadfinalizada = 1 AND SOM.CONTROL_PLANTAS = 'SI'  and  " +
								//"sg.correlativoempleado = '"+CodigoEmpleado+"' AND G.SIEMBRA = ''  GROUP BY G.ACTIVIDAD,SOM.DESCRIPCION,G.SIEMBRA,SIE.PLANTAS,sie.invernadero,d.desecho " +
								//"ORDER BY G.ACTIVIDAD,g.siembra,sie.plantas");
				
								//Agregar encabezados de columnas	
								 TableRow Erow1 = new TableRow(ConsultaCuadresSem.this);
								 TextView E1 = new TextView(ConsultaCuadresSem.this);
								 TextView E2 = new TextView(ConsultaCuadresSem.this);
								 TextView E3 = new TextView(ConsultaCuadresSem.this);
								 TextView E4 = new TextView(ConsultaCuadresSem.this);
								 TextView E5 = new TextView(ConsultaCuadresSem.this);
								 TextView E6 = new TextView(ConsultaCuadresSem.this);
								// TextView E7 = new TextView(ConsultaCuadresSem.this);
								 TextView E8 = new TextView(ConsultaCuadresSem.this); 
								 TextView E9 = new TextView(ConsultaCuadresSem.this); 
								 TextView E10 = new TextView(ConsultaCuadresSem.this); 
								 TextView E11 = new TextView(ConsultaCuadresSem.this); 
								 TextView E12 = new TextView(ConsultaCuadresSem.this); 
								// TextView E13 = new TextView(ConsultaCuadresSem.this);
								 E1.setTextSize(15);
								 E1.setTextColor(Color.BLACK);
								 E2.setTextSize(15);
								 E2.setTextColor(Color.BLACK);
								 E3.setTextSize(15);
								 E3.setTextColor(Color.BLACK);
								 E4.setTextSize(15);
								 E4.setTextColor(Color.BLACK);
								 E5.setTextSize(15);
								 E5.setTextColor(Color.BLACK);
								 E6.setTextSize(15);
								 E6.setTextColor(Color.BLACK);
								// E7.setTextSize(15);
								// E7.setTextColor(Color.BLACK);
								 E8.setTextSize(20);
								 E8.setTextColor(Color.BLACK);
								 E9.setTextSize(20);
								 E9.setTextColor(Color.BLACK);
								 E10.setTextSize(20);
								 E10.setTextColor(Color.BLACK);
								 E11.setTextSize(20);
								 E11.setTextColor(Color.BLACK);
								 E12.setTextSize(20);
								 E12.setTextColor(Color.BLACK);
								// E13.setTextSize(20);
								 String Cn1 = rs.getMetaData().getColumnName(1);
								 String Cn2 = rs.getMetaData().getColumnName(2);
								 String Cn3 = rs.getMetaData().getColumnName(3);
								 String Cn4 = rs.getMetaData().getColumnName(4);
								 String Cn5 = rs.getMetaData().getColumnName(5);
								 String Cn6 = rs.getMetaData().getColumnName(6);
								// String Cn7 = rs.getMetaData().getColumnName(7);
								 E1.setText(Cn1);
								 E2.setText(Cn2);
								 E3.setText(Cn3);
								 E4.setText(Cn4);
								 E5.setText(Cn5);
								 E6.setText(Cn6);
								// E7.setText(Cn7);
								 E8.setText(es);
								 E9.setText(es);
								 E10.setText(es);
								 E11.setText(es);
								 E12.setText(es);
							//	 E13.setText(es);
								 Erow1.addView(E1);
								 Erow1.addView(E8);
								 Erow1.addView(E2);
								 Erow1.addView(E9);
								 Erow1.addView(E3);
								 Erow1.addView(E10);
								 Erow1.addView(E4);
								 Erow1.addView(E11);
								 Erow1.addView(E5);
								 Erow1.addView(E12);
								 Erow1.addView(E6);
							//	 Erow1.addView(E13);
							//	 Erow1.addView(E7);
								 E1.setGravity(Gravity.CENTER_HORIZONTAL);
								 E2.setGravity(Gravity.CENTER_HORIZONTAL);
								 E3.setGravity(Gravity.CENTER_HORIZONTAL);
								 E4.setGravity(Gravity.CENTER_HORIZONTAL);
								 E5.setGravity(Gravity.CENTER_HORIZONTAL);
								 E6.setGravity(Gravity.CENTER_HORIZONTAL);
							//	 E7.setGravity(Gravity.CENTER_HORIZONTAL);
								 E8.setGravity(Gravity.CENTER_HORIZONTAL);
								 E9.setGravity(Gravity.CENTER_HORIZONTAL);
								 E10.setGravity(Gravity.CENTER_HORIZONTAL);
								 E11.setGravity(Gravity.CENTER_HORIZONTAL);
								 E12.setGravity(Gravity.CENTER_HORIZONTAL);
							//	 E13.setGravity(Gravity.CENTER_HORIZONTAL);
								 Erow1.setGravity(Gravity.CENTER);
								 Erow1.setBackgroundColor(Color.GREEN);
								 TablaEnc.addView(Erow1);	
								// finaliza agregar encabezados de columnas 
								int sz = rs.getFetchSize();
								int cont = 0;
								while (rs.next()) {
									 String d1 = rs.getString(1).trim();
									 String d2 = rs.getString(2).trim();
									 String d3 = rs.getString(3).trim();
									 String d4 = rs.getString(4).trim();
								     String d5 = rs.getString(5).trim();
									 String d6 = rs.getString(6).trim();
								//	 String d7 = rs.getString(7).trim();
									 String c1 = comprobar(d1);
									 String c2 = comprobar(d2);
									 String c3 = comprobar(d3);
									 String c4 = comprobar(d4);
									 String c5 = comprobar(d5);
									 String c6 = comprobar(d6);
									// String c7 = comprobar(d7);
									 for (int i = 0; i == sz; i++)
									 {	
										 cont = cont +1;
										 TableRow row1 = new TableRow(ConsultaCuadresSem.this);
										 TextView t1 = new TextView(ConsultaCuadresSem.this);
										 TextView t2 = new TextView(ConsultaCuadresSem.this);
										 TextView t3 = new TextView(ConsultaCuadresSem.this);
										 TextView t4 = new TextView(ConsultaCuadresSem.this);
										 TextView t5 = new TextView(ConsultaCuadresSem.this);
										 TextView t6 = new TextView(ConsultaCuadresSem.this);
										// TextView t7 = new TextView(ConsultaCuadresSem.this);
										 TextView t8 = new TextView(ConsultaCuadresSem.this); 
										 TextView t9 = new TextView(ConsultaCuadresSem.this); 
										 TextView t10 = new TextView(ConsultaCuadresSem.this); 
										 TextView t11 = new TextView(ConsultaCuadresSem.this); 
										 TextView t12 = new TextView(ConsultaCuadresSem.this); 
									//	 TextView t13 = new TextView(ConsultaCuadresSem.this);
										 t1.setTextSize(13);
										 t1.setTextColor(Color.BLACK);
										 t2.setTextSize(13);
										 t2.setTextColor(Color.BLACK);
										 t3.setTextSize(13);
										 t3.setTextColor(Color.BLACK);
										 t4.setTextSize(13);
										 t4.setTextColor(Color.BLACK);
										 t5.setTextSize(13);
										 t5.setTextColor(Color.BLACK);
										 t6.setTextSize(13);
										 t6.setTextColor(Color.BLACK);
									//	 t7.setTextSize(13);
									//	 t7.setTextColor(Color.BLACK);
										 t8.setTextSize(20);
										 t8.setTextColor(Color.BLACK);
										 t9.setTextSize(20);
										 t9.setTextColor(Color.BLACK);
										 t10.setTextSize(20);
										 t10.setTextColor(Color.BLACK);
										 t11.setTextSize(20);
										 t11.setTextColor(Color.BLACK);
										 t12.setTextSize(20);
										 t12.setTextColor(Color.BLACK);
									//	 t13.setTextSize(20);
										 t1.setGravity(Gravity.CENTER_HORIZONTAL);
										 t2.setGravity(Gravity.CENTER_HORIZONTAL);
										 t3.setMaxWidth(150);
										 t4.setMaxWidth(150);
										 t5.setMaxWidth(100);
										 t4.setGravity(Gravity.CENTER_HORIZONTAL);
										 t5.setGravity(Gravity.CENTER_HORIZONTAL);
										 t6.setGravity(Gravity.CENTER_HORIZONTAL);
									//	 t7.setGravity(Gravity.CENTER_HORIZONTAL);
										 t8.setGravity(Gravity.CENTER_HORIZONTAL);
										 t9.setGravity(Gravity.CENTER_HORIZONTAL);
										 t10.setGravity(Gravity.CENTER_HORIZONTAL);
										 t11.setGravity(Gravity.CENTER_HORIZONTAL);
										 t12.setGravity(Gravity.CENTER_HORIZONTAL);
									//	 t13.setGravity(Gravity.CENTER_HORIZONTAL);
										 row1.setGravity(Gravity.CENTER);
										 row1.setClickable(true);
										 if(cont%2 == 0){
										 row1.setBackgroundColor(Color.LTGRAY);
										 }else {
										 row1.setBackgroundColor(Color.WHITE);	 
										 }
									 t1.setText(c1);
									 t2.setText(c2);
									 t3.setText(c3);
									 t4.setText(c4);
									 t5.setText(c5);
									 t6.setText(c6);
								//	 t7.setText(c7);
									 t8.setText(es);
									 t9.setText(es);
									 t10.setText(es);
									 t11.setText(es);
									 t12.setText(es);
								//	 t13.setText(es);
									 row1.addView(t1);
									 row1.addView(t8);
									 row1.addView(t2);
									 row1.addView(t9);
									 row1.addView(t3);
									 row1.addView(t10);
									 row1.addView(t4);
									 row1.addView(t11);
									 row1.addView(t5);
									 row1.addView(t12);
									 row1.addView(t6);
								//	 row1.addView(t13);
								//	 row1.addView(t7);
									 TablaEnc.addView(row1);
									 TablaEnc.setClickable(true);
									 }
									 
								}
						connection.close();	
	            	}
					catch (Exception e) {
						Toast toast = Toast.makeText(getApplicationContext(), "Verificar Conexi�n.....", Toast.LENGTH_SHORT);
		            	toast.show();
						e.printStackTrace();
	            } 
	           } 	
	            }
	        });
		    
		   
		}
		
		public void alertDialogMensaje(String message1, String mesage2){
		
			try {
			    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
			    ringtone.play();	    	    
			} catch (Exception e) {
			    e.printStackTrace();
			}
			AlertDialog alertDialog = new AlertDialog.Builder(this).create();
			alertDialog.setTitle(message1);
			alertDialog.setMessage(mesage2);
			alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
			// here you can add functions
				ringtone.stop();
			}
			});
			alertDialog.show();
		}
		
		public String comprobar(String cad){
			String Pb = "";
			if(cad.equals(Pb)){
				cad = "---";
			}
			return cad;	
		}
		
		public String format (int f){
			val = "";
			if(f<10){
			val = "0"+Integer.toString(f);	
			}
			else{
			val = Integer.toString(f);
			}
			return val;
		}
		
		public void VerificarDatos(String dato){	
			 Cursor CursorSemillas = db.getSemillas("Siembra='" + dato + "'");
			 if(CursorSemillas.getCount()>0){
				 Semillas s = db.getSemillasFromCursor(CursorSemillas, 0);
				 Log.d("SEMILLA",s.getSiembra() + ", "+s.getMaterial() + " ," + s.getPolinizacion());
				 validoDescriptivo = true;
				 } else {
					 validoDescriptivo = false;
				 }
			 CursorSemillas.close();
		}
				
		@Override
		public boolean onCreateOptionsMenu(Menu menu) {

			// Inflate the menu; this adds items to the action bar if it is present.
			getMenuInflater().inflate(R.menu.soporteit, menu);
			return true;
		}

		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			// Handle action bar item clicks here. The action bar will
			// automatically handle clicks on the Home/Up button, so long
			// as you specify a parent activity in AndroidManifest.xml.
			int id = item.getItemId();
			if (id == R.id.action_settings) {
				return true;
			}
			return super.onOptionsItemSelected(item);
		}
	}  
