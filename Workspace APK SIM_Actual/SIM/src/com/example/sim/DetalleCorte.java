package com.example.sim;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.Vibrator;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.floricultura.R;

public class DetalleCorte extends ActionBarActivity {
	
	//Variables globales
	private MyApp appState;
	int valor;
	String NombreOpcion;
	GridView gridet;
	GridView gridEnc;
	Vibrator mVibrator;
	Ringtone ringtone;
	String Inv;
	String Fechade;
	String Fechaal;
	int dia1;
	int mes1;
	int amo1;
	String Fechade1;
	String Fechaal1;
	int dia2;
	int mes2;
	int amo2;
	String Wk;
	String DiaCorte;
	String Dia;
	
	@SuppressLint("NewApi") @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.detalle_corte);
		
		// para manejar la excepcion de permisos en el API de Android
    	StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

		//se obtienen lo datos de la actividad antetior por medio del intent
		Bundle extras = getIntent().getExtras();
	    NombreOpcion = "Detalle Corte";
	    Inv = extras.getString("Inv");
	    Fechade = extras.getString("Fechade");
	    Fechaal = extras.getString("Fechaal");
	    dia1 = extras.getInt("dia1");
	    mes1 = extras.getInt("mes1");
	    amo1 = extras.getInt("amo1");
	    Fechade1 = extras.getString("Fechade1");
	    Fechaal1 = extras.getString("Fechaal1");
	    dia2 = extras.getInt("dia2");
	    mes2 = extras.getInt("mes2");
	    amo2 = extras.getInt("amo2");
	    Wk = extras.getString("Wk");
	    DiaCorte = extras.getString("DiaCorte");

	    
	    
	    //hacemos referencia a todos los objetos de nuestra vista
	    TextView TNombreCaptura = (TextView)findViewById(R.id.OpcionCaptura);
        Button btnBack = (Button) findViewById(R.id.btnBack);
        
        gridet = (GridView) findViewById(R.id.Gridet);
        gridEnc = (GridView) findViewById(R.id.GridEncDet);
        
        final ArrayList<String> datos = new ArrayList<String>();
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, datos);
        final ArrayList<String> encabezado = new ArrayList<String>();
        final ArrayAdapter<String> adapEnc = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, encabezado);
        
	    //hacemos las inicializaciones necesarias a nuestra vista
	    TNombreCaptura.setText(NombreOpcion+ ""); 
	    
	    datos.clear();
    	adapter.clear();
    	encabezado.clear();
    	adapEnc.clear();
    	
    	try {
    	String user = "sim";
    	String pasword = "sim";
    	 Connection connection = null;
    	 ResultSet rs = null;
    	 Statement statement = null;
    	 
			Class.forName("com.ibm.as400.access.AS400JDBCDriver");
			connection = DriverManager.getConnection("jdbc:as400://190.190.190.10", user, pasword);	
			statement = connection.createStatement();
			rs = statement.executeQuery("Select v43.bardoc ORDEN,ifnull(ifnull(l1.invernadero,asig.invernadero),replace(replace(v43.barinv,'-',''),' ','')) Inv," +
					"v43.barvar VARIEDAD,v43.barcns Etiquetas,ifnull((rtrim(asig.codigo_empleado)||'-'||rtrim(e.nombre)||'-'||rtrim(e.apellido)),'---') Empleado " +
					"from sigvegfgu.vege43 v43 left join (Select codigo_empleado,ordencompra,invernadero,cast(etiqueta as integer) Etiqueta " +
					"from sisappf.app_tmp_etiquetas where fecha between '"+Fechade+"' and '"+Fechaal+"' and estado = 'A' and invernadero <> 'EMPAQU' " +
					"order by etiqueta) asig on v43.barcns = asig.Etiqueta left join (Select ordencompra,invernadero,cast(etiqueta as integer) Etiqueta " +
					"from sisappf.app_tmp_etiquetasL1 where fecha  between '"+Fechade+"' and '"+Fechaal+"' and estado = 'A' and invernadero <> 'EMPAQU' " +
					"order by etiqueta) l1 on v43.barcns = l1.Etiqueta left join (Select i.temd01 from sigipvfgu.incv611 i left join " +
					"(Select ordencompra,invernadero,cast(etiqueta as integer) Etiqueta from sisappf.app_tmp_etiquetasL1 where " +
					"fecha between '"+Fechade+"' and '"+Fechaal+"' and estado = 'A' and invernadero <> 'EMPAQU' order by etiqueta) a on i.temd01 = a.etiqueta " +
					"where temd08 between '"+Fechade1+"' and '"+Fechaal1+"' and a.etiqueta is null order by temd01) emp on v43.barcns = emp.temd01 " +
					"left join sisrrhhf.mempleado e on '0'||asig.codigo_empleado = e.empresa||e.codigoempleado left join sisvegf.membarques emb " +
					"on right(v43.bardoc,1) = emb.letraorden where barano = '"+amo1+"' and barsem = '"+Wk+"' and barot3 <> '2.00' and l1.etiqueta is null " +
					"and emp.temd01 is null and ifnull(ifnull(l1.invernadero,asig.invernadero),replace(replace(v43.barinv,'-',''),' ','')) = '"+Inv+"' AND LEFT(EMB.DIA,3) = '"+DiaCorte+"'" +
					"and case when emb.dia = 'LUNES' THEN 2 when emb.dia = 'MARTES' THEN 3 when emb.dia = 'MIERCOLES' THEN 4 when emb.dia = 'JUEVES' THEN 5 when emb.dia = 'VIERNES' THEN 6 when emb.dia = 'SABADO' THEN 7 when emb.dia = 'DOMINGO' THEN 1 END = case when '" +DiaCorte+ "' = 'LUN' THEN 2 when '" +DiaCorte+ "' = 'MAR' THEN 3 when '" +DiaCorte+ "' = 'MIE' THEN 4 when '" +DiaCorte+ "' = 'JUE' THEN 5 when '" +DiaCorte+ "' = 'VIE' THEN 6 when '" +DiaCorte+ "' = 'SAB' THEN 7 when '" +DiaCorte+ "' = 'DOM' THEN 1 END");
			
		    //Agregar encabezados de columnas	
			String Cn1 = rs.getMetaData().getColumnName(1);
			encabezado.add(Cn1);
			gridEnc.setAdapter(adapEnc);
			String Cn2 = rs.getMetaData().getColumnName(2);
			encabezado.add(Cn2);
			gridEnc.setAdapter(adapEnc);
			String Cn3 = rs.getMetaData().getColumnName(3);
			encabezado.add(Cn3);
			gridEnc.setAdapter(adapEnc);
			String Cn4 = rs.getMetaData().getColumnName(4);
			encabezado.add(Cn4);
			gridEnc.setAdapter(adapEnc);
			String Cn5 = rs.getMetaData().getColumnName(5);
			encabezado.add(Cn5);
			gridEnc.setAdapter(adapEnc);
						
			// finaliza agregar encabezados de columnas	
			while (rs.next()) {
			//agrega filas de columnas
				 String d1 = rs.getString(1).trim();
				 String d2 = rs.getString(2).trim();
				 String d3 = rs.getString(3).trim();
				 String d4 = rs.getString(4).trim();
			     String d5 = rs.getString(5).trim();
				 String c1 = comprobar(d1);
				 String c2 = comprobar(d2);
				 String c3 = comprobar(d3);
				 String c4 = comprobar(d4);
				 String c5 = comprobar(d5);
				 datos.add(c1);
				 gridet.setAdapter(adapter);
				 datos.add(c2);
				 gridet.setAdapter(adapter);
				 datos.add(c3);
				 gridet.setAdapter(adapter);
				 datos.add(c4);
				 gridet.setAdapter(adapter);
				 datos.add(c5);
				 gridet.setAdapter(adapter);
				 
			//finaliza agregar columnas	 
			}
			connection.close();	
    	}
		catch (Exception e) {
			Toast toast = Toast.makeText(getApplicationContext(), "Verificar Conexi�n.....", Toast.LENGTH_SHORT);
        	toast.show();
			//alertDialogMensaje("CONEXION","VERIFIQUE SU CONEXION");
			// TODO Auto-generated catch block
			e.printStackTrace();
    }  
	     
	    
	    btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	onBackPressed();
            }
        });   
	}
	    
	public void alertDialogMensaje(String message1, String mesage2){
		
		//mVibrator.vibrate(300);
		
		try {
		    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
		    ringtone.play();
		    
		    
		} catch (Exception e) {
		    e.printStackTrace();
		}
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(message1);
		alertDialog.setMessage(mesage2);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int which) {
		// here you can add functions
			ringtone.stop();
		}
		});
		alertDialog.show();
	}

	public String comprobar(String cad){
		String Pb = "";
		if(cad.equals(Pb)){
			cad = "---";
		}
		return cad;	
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.soporteit, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}  


/*
SELECT a.orden,a.invernadero,a.etiqueta,a.ult_empleado,a.NombreEmp,v.barvar codigo,v.barvde nombre,'' ESTADO " +
					"FROM (select a.bardoc Orden,a.codigo_produccion Invernadero,a.etiqueta,a.codigo_empleado Ult_Empleado,a.NombreEmp " +
					"from (select v.bardoc,e.etiqueta,i.codigo_produccion,e.invernadero,e.codigo_empleado,emp.nombre || ' ' || emp.apellido NombreEmp " +
					"from SISAPPF.APP_TMP_ETIQUETAS e inner join sisappf.app_invernaderos i on e.invernadero = i.codigo_app inner join sigvegfgu.vege43 v " +
					"on e.etiqueta = v.barcns and '"+a�o1+"' = v.barano left join sisrrhhf.mempleado emp on '0' || e.codigo_empleado = emp.empresa || emp.codigoempleado " +
					"where e.fecha BETWEEN '"+Fechade+"' and '"+Fechaal+"' and V.BAROT3 <> '2.00' union select v.bardoc,l.etiqueta,i.codigo_produccion,l.invernadero, " +
					"l.codigo_empleado,emp.nombre || ' ' || emp.apellido NombreEmp from SISAPPF.APP_TMP_ETIQUETASL1 L left join SISAPPF.APP_TMP_ETIQUETAS e " +
					"on l.etiqueta = e.etiqueta inner join sisappf.app_invernaderos i on l.invernadero = i.codigo_app inner join sigvegfgu.vege43 v on l.etiqueta = v.barcns " +
					"and '"+a�o1+"' = v.barano left join sisrrhhf.mempleado emp on '0' || l.codigo_empleado = emp.empresa || emp.codigoempleado where l.fecha " +
					"BETWEEN '"+Fechade+"' and '"+Fechaal+"' and e.etiqueta is null and V.BAROT3 <> '2.00' ) a ) A  LEFT JOIN (SELECT W.BARDOC,I.CODIGO_PRODUCCION,L1 L1 " +
					"FROM (SELECT CASE WHEN INV.CODIGO_PRODUCCION IS NULL THEN ATE.INVERNADERO ELSE INV.CODIGO_PRODUCCION END INV ,CASE WHEN INV.CODIGO_APP IS NULL " +
					"THEN ATE.INVERNADERO ELSE INV.CODIGO_APP END CODIGO_APP,V.BARDOC,ATE.ETIQUETA L1 FROM SISAPPF.APP_TMP_ETIQUETASL1 ATE " +
					"LEFT JOIN (SELECT DISTINCT ETIQUETA,INVERNADERO,MAX(HORA) FROM SISAPPF.APP_TMP_ETIQUETAS WHERE FECHA BETWEEN '"+Fechade+"' and '"+Fechaal+"' " +
					"GROUP BY ETIQUETA,INVERNADERO) E ON ATE.ETIQUETA = E.ETIQUETA INNER JOIN SIGVEGFGU.VEGE43 V ON ATE.ETIQUETA = V.BARCNS  AND '"+a�o1+"' = V.BARANO " +
					"left join sisappf.app_invernaderos inv on e.invernadero = inv.codigo_app WHERE ATE.FECHA BETWEEN '"+Fechade+"' and '"+Fechaal+"'   AND V.BAROT3 <> '2.00' " +
					"GROUP BY V.BARDOC,INV.CODIGO_PRODUCCION,INV.CODIGO_APP,ATE.INVERNADERO,ATE.ETIQUETA UNION SELECT INV.CODIGO_PRODUCCION INV,inv.codigo_app," +
					"V.BARDOC,I.TEMD01 L1 FROM SISAPPF.APP_TMP_ETIQUETAS ATE INNER JOIN SIGVEGFGU.VEGE43 V ON ATE.ETIQUETA = V.BARCNS  AND '"+a�o1+"' = V.BARANO " +
					"LEFT JOIN SISAPPF.APP_TMP_ETIQUETASL1 L ON ATE.ETIQUETA = L.ETIQUETA left join sisappf.app_invernaderos inv on ate.invernadero = inv.codigo_app " +
					"LEFT JOIN SIGIPVFGU.INCV611 I ON ATE.ETIQUETA = I.TEMD01 WHERE ATE.FECHA BETWEEN '"+Fechade+"' and '"+Fechaal+"'  AND V.BAROT3 <> '2.00' " +
					"AND L.ETIQUETA IS NULL AND I.TEMD01 IS NOT NULL GROUP BY INV.CODIGO_PRODUCCION,V.BARDOC,inv.codigo_app,I.TEMD01) W " +
					"INNER JOIN SISAPPF.APP_INVERNADEROS I ON W.CODIGO_APP = I.CODIGO_APP GROUP BY W.BARDOC,I.CODIGO_PRODUCCION,L1 ) B " +
					"ON A.ETIQUETA = B.L1  INNER JOIN SIGVEGFGU.VEGE43 V ON A.ETIQUETA = V.BARCNS AND '"+a�o1+"' = V.BARANO INNER JOIN SISVEGF.MEMBARQUES EMB " +
					"ON RIGHT(V.BARDOC,1) = EMB.LETRAORDEN WHERE B.L1 IS NULL AND A.INVERNADERO = '"+Inv+"' AND LEFT(EMB.DIA,3) = '"+DiaCorte+"'


SELECT V.BARDOC ORDEN,V.BARINV INV,V.BARCNS ETIQUETA,'' ULT_EMPLEADO,'' NombreEmp,v.barvar CODIGO,V.BARVDE NOMBRE " +
					"FROM SIGVEGFGU.VEGE43 v LEFT JOIN (select A.ETIQUETA,a.bardoc,a.codigo_produccion,a.invernadero,count(distinct a.etiqueta) Asignadas " +
					"from (select v.bardoc,e.etiqueta,i.codigo_produccion,e.invernadero,e.codigo_empleado from SISAPPF.APP_TMP_ETIQUETAS e inner join " +
					"sisappf.app_invernaderos i on e.invernadero = i.codigo_app inner join sigvegfgu.vege43 v on e.etiqueta = v.barcns and '"+a�o1+"' = v.barano " +
					"where e.fecha BETWEEN '"+Fechade+"' and '"+Fechaal+"' and V.BAROT3 <> '2.00' union select v.bardoc,l.etiqueta,i.codigo_produccion,l.invernadero,l.codigo_empleado " +
					"from SISAPPF.APP_TMP_ETIQUETASL1 L left join SISAPPF.APP_TMP_ETIQUETAS e on l.etiqueta = e.etiqueta inner join sisappf.app_invernaderos i on l.invernadero = i.codigo_app " +
					"inner join sigvegfgu.vege43 v on l.etiqueta = v.barcns and '"+a�o1+"' = v.barano where l.fecha BETWEEN '"+Fechade+"' and '"+Fechaal+"' and e.etiqueta is null and V.BAROT3 <> '2.00' ) a " +
					"group by a.bardoc,a.codigo_produccion,a.invernadero,A.ETIQUETA order by a.codigo_Produccion ) E ON V.BARCNS = E.ETIQUETA AND '"+a�o1+"' = V.BARANO INNER JOIN " +
					"SISVEGF.MEMBARQUES EMB ON RIGHT(V.BARDOC,1) = EMB.LETRAORDEN WHERE V.BARANO = '"+a�o1+"' AND V.BARSEM = '"+Wk+"' AND V.BAROT3 <> '2.00' " +
					"AND V.BARINV = ' "+Inv+"'  AND E.ETIQUETA IS NULL AND LEFT(EMB.DIA,3) = '"+DiaCorte+"'
*/