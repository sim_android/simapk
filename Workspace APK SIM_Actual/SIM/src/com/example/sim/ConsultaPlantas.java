package com.example.sim;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.Vibrator;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.floricultura.R;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.Semillas;

public class ConsultaPlantas extends ActionBarActivity {
	
	//Variables globales
		private MyApp appState;	
		private DBAdapter db;
		int CodigoModulo;
		int Codigodepartamento;
		int CodigoEmpleado;
		int CodigoOpcion;
		long Pos;
		String Siembr;
		String Act;
		String Desc;
		String NombreOpcion;
		GridView gridview;
		GridView gridEnc;
		Vibrator mVibrator;
		Ringtone ringtone;
		String Fechade;
		EditText Siembra;
		String siem;
		String Inv;
		boolean validoDescriptivo;
		private String val;
		ArrayList<String> datos;
		ArrayAdapter<String> adapter;
		ArrayList<String> encabezado;
		ArrayAdapter<String> adapEnc;
		EditText editFechade;
		
		@SuppressLint("NewApi") @Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.consulta_plantas);
			
			// para manejar la excepcion de permisos en el API de Android
	    	StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	        StrictMode.setThreadPolicy(policy);

			//se obtienen lo datos de la actividad antetior por medio del intent
			Bundle extras = getIntent().getExtras();
			CodigoModulo = extras.getInt("CodigoModulo");
		    CodigoEmpleado = extras.getInt("CodigoEmpleado");
		    Codigodepartamento = extras.getInt("Codigodepartamento");
		    CodigoOpcion = extras.getInt("CodigoOpcion");
		    NombreOpcion = extras.getString("NombreOpcion");
		    
		    //hacemos referencia a todos los objetos de nuestra vista
		    TextView TNombreCaptura = (TextView)findViewById(R.id.OpcionCaptura);
		    TextView TCodigoEmpleado= (TextView)findViewById(R.id.CodigoEmpleado);
		    TextView TCodigoOpcion= (TextView)findViewById(R.id.codigoOpcion);
	        Button btnBack = (Button) findViewById(R.id.btnBack);
	        Button btnOK = (Button) findViewById(R.id.btnAsistencia);
	        Button btnfechade = (Button) findViewById(R.id.buttonfechade);
	        editFechade = (EditText) findViewById(R.id.editFechaDe);
	        Siembra = (EditText) findViewById(R.id.TextSiembra);
	        editFechade.setEnabled(false);
	        gridEnc = (GridView) findViewById(R.id.gridEnc);
	        gridview = (GridView) findViewById(R.id.gridDetalle);
	        
	        datos = new ArrayList<String>();
	        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, datos);
	        encabezado = new ArrayList<String>();
	        adapEnc = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, encabezado);
	        
		    //hacemos las inicializaciones necesarias a nuestra vista
		    TNombreCaptura.setText(NombreOpcion+ "");
		    TCodigoEmpleado.setText(CodigoEmpleado + "");
		    TCodigoOpcion.setText(CodigoOpcion + "");
		    
		    //incializamos la BD asi como tambien obtenemos la referencia al singleton
		    appState = ((MyApp)getApplicationContext());   
		    db = appState.getDb();
		    db.open();

		    btnBack.setOnClickListener(new View.OnClickListener() {
	            public void onClick(View v) {
	 
	            	onBackPressed();
	            }
	        });
		    
		    btnfechade.setOnClickListener(new View.OnClickListener() {
	            public void onClick(View v) {
	            	Calendar mcurrentDate=Calendar.getInstance();
	                int mYear = mcurrentDate.get(Calendar.YEAR);
	                int mMonth = mcurrentDate.get(Calendar.MONTH);
	                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

	                DatePickerDialog mDatePicker=new DatePickerDialog(ConsultaPlantas.this, new OnDateSetListener() {                  
	                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
	                    	int datodia = selectedday;
	                    	String dia = format(datodia);
	                    	int datomes = selectedmonth + 1;
	                    	String mes = format(datomes);
	                    	String datoanio = Integer.toString(selectedyear);
	                    	String dato = datoanio + "-" + mes + "-" +dia;
	                    	editFechade.setText(dato);
	                    }
	                }
	                ,mYear, mMonth, mDay);
	                mDatePicker.setTitle("Seleccionar Fecha");                
	                mDatePicker.show();  
	            }
	            
	        });
		    
		  //al recibir el enter se verifica si el descriptivo es correcto y lo manda al siguiente input sino le muestra un mensaje al usuario
	        Siembra.setOnKeyListener(new View.OnKeyListener() {
	            public boolean onKey(View v, int keyCode, KeyEvent event) {
	                // se manda a llamar cuando se presione Enter
	                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
	                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
	                	VerificarDatos(Siembra.getText().toString().trim());
	                	if (validoDescriptivo == true){
		            	datos.clear();
		            	adapter.clear();
		            	encabezado.clear();
		            	adapEnc.clear();
		            	
		            	siem = Siembra.getText().toString().trim();
		            	Fechade = editFechade.getText().toString().trim();
		            	String Pb = "";
		            	
		            	if(Fechade.equals(Pb)){
		            		alertDialogMensaje("ERROR","PORFAVOR SELECCIONE UNA FECHA");
		            		Siembra.setText("");
		            		editFechade.setText("");
		            		Siembra.requestFocus();
		            	}
		            	else 
		            	{
		            	try {
		            	String user = "sim";
		            	String pasword = "sim";
		            	 Connection connection = null;
		            	 ResultSet rs = null;
		            	 Statement statement = null;
		            	 
							Class.forName("com.ibm.as400.access.AS400JDBCDriver");
							connection = DriverManager.getConnection("jdbc:as400://190.190.190.10", user, pasword);	
							statement = connection.createStatement();			     
							rs = statement.executeQuery(
							"SELECT G.ACTIVIDAD ACT, SOM.DESCRIPCION DESC,G.SIEMBRA SIEM,(ifnull(sie.plantas,0) + ifnull(d.desecho,0)) PLTS, SUM(ifnull(G.PLANTAS,0)) TRABAJ, " +
							"(SUM(ifnull(G.PLANTAS,0)) - (ifnull(sie.plantas,0) + ifnull(d.desecho,0))) DIF,sie.invernadero INV FROM SISAPPF.APP_TMP_GENERAL g LEFT JOIN " +
							"SISCFGF.SEGURIDAD_OPCION_MODULOASP SOM ON G.ACTIVIDAD = SOM.OPCION LEFT JOIN SISSEMF.MOV_DEFSIEMBRAE SIE  ON G.SIEMBRA = SIE.NOSIEMBRA  inner join " +
							"(Select correlativoempleado,invernadero from sisappf.app_seguridad_consulta_sem where estado = 'AC') sg on sie.invernadero = '' || sg.invernadero " +
							"left join (select empresa,siembra,sum(plantas) Desecho from sissemf.mov_desechoplantasem where " +
							"fecha > '"+Fechade+"' group by empresa,siembra) d on g.siembra = d.siembra where (G.ESTADO IS NULL OR G.ESTADO IN ('AC','CO')) AND " +
							"G.FECHA = '"+Fechade+"' AND  G.actividadfinalizada = 1 AND SOM.CONTROL_PLANTAS = 'SI'  and  " +
							"sg.correlativoempleado = '"+CodigoEmpleado+"' AND G.SIEMBRA = '"+siem+"'  GROUP BY G.ACTIVIDAD,SOM.DESCRIPCION,G.SIEMBRA,SIE.PLANTAS,sie.invernadero,d.desecho " +
							"ORDER BY G.ACTIVIDAD,g.siembra,sie.plantas");
			
						    //Agregar encabezados de columnas	
							String Cn1 = rs.getMetaData().getColumnName(1);
							encabezado.add(Cn1);
							gridEnc.setAdapter(adapEnc);
							String Cn2 = rs.getMetaData().getColumnName(2);
							encabezado.add(Cn2);
							gridEnc.setAdapter(adapEnc);
							String Cn3 = rs.getMetaData().getColumnName(3);
							encabezado.add(Cn3);
							gridEnc.setAdapter(adapEnc);
							String Cn4 = rs.getMetaData().getColumnName(4);
							encabezado.add(Cn4);
							gridEnc.setAdapter(adapEnc);
							String Cn5 = rs.getMetaData().getColumnName(5);
							encabezado.add(Cn5);
							gridEnc.setAdapter(adapEnc);
							String Cn6 = rs.getMetaData().getColumnName(6);
							encabezado.add(Cn6);
							gridEnc.setAdapter(adapEnc);
							String Cn7 = rs.getMetaData().getColumnName(7);
							encabezado.add(Cn7);
							gridEnc.setAdapter(adapEnc);
							// finaliza agregar encabezados de columnas	
							while (rs.next()) {
							//agrega filas de columnas
								 String d1 = rs.getString(1).trim();
								 String d2 = rs.getString(2).trim();
								 String d3 = rs.getString(3).trim();
								 String d4 = rs.getString(4).trim();
							     String d5 = rs.getString(5).trim();
								 String d6 = rs.getString(6).trim();
								 String d7 = rs.getString(7).trim();
								 String c1 = comprobar(d1);
								 String c2 = comprobar(d2);
								 String c3 = comprobar(d3);
								 String c4 = comprobar(d4);
								 String c5 = comprobar(d5);
								 String c6 = comprobar(d6);
								 String c7 = comprobar(d7);
								 datos.add(c1);
								 gridview.setAdapter(adapter);
								 datos.add(c2);
								 gridview.setAdapter(adapter);
								 datos.add(c3);
								 gridview.setAdapter(adapter);
								 datos.add(c4);
								 gridview.setAdapter(adapter);
								 datos.add(c5);
								 gridview.setAdapter(adapter);
								 datos.add(c6);
								 gridview.setAdapter(adapter);
								 datos.add(c7);
								 gridview.setAdapter(adapter);
							}
							connection.close();	
		            	}
						catch (Exception e) {
							Toast toast = Toast.makeText(getApplicationContext(), "Verificar Conexi�n.....", Toast.LENGTH_SHORT);
			            	toast.show();
							e.printStackTrace();
						} 
		            } 	   	
	              }else{
		    	    alertDialogMensaje("Descriptivo", "Descriptivo Invalido \n \n  Revisar Descriptivo");
		    	    Siembra.setText("");
		    	    Siembra.requestFocus();
	              }
	                return true;  
	               }     
	              return false;
	            }  
	        });
		    		    
		    btnOK.setOnClickListener(new View.OnClickListener() {
	            public void onClick(View v) {
	            	datos.clear();
	            	adapter.clear();
	            	encabezado.clear();
	            	adapEnc.clear();
	         
	            	Fechade = editFechade.getText().toString().trim();
	            	String Pb = "";
	            	
	            	if(Fechade.equals(Pb)){
	            		alertDialogMensaje("ERROR","PORFAVOR SELECCIONE UNA FECHA");
	            	}
	            	else 
	            	{
	            	try {
	            	String user = "sim";
	            	String pasword = "sim";
	            	 Connection connection = null;
	            	 ResultSet rs = null;
	            	 Statement statement = null;
	            	 
						Class.forName("com.ibm.as400.access.AS400JDBCDriver");
						connection = DriverManager.getConnection("jdbc:as400://190.190.190.10", user, pasword);	
						statement = connection.createStatement();			     
						rs = statement.executeQuery(
						"SELECT G.ACTIVIDAD ACT,SOM.DESCRIPCION DES,G.SIEMBRA,(ifnull(sie.plantas,0) + ifnull(d.desecho,0)) Plantas," +
						"SUM(ifnull(G.PLANTAS,0)) TRABAJADAS, (SUM(ifnull(G.PLANTAS,0)) - (ifnull(sie.plantas,0) + ifnull(d.desecho,0))) DIF, " +
						"sie.invernadero INV FROM SISAPPF.APP_TMP_GENERAL g LEFT JOIN SISCFGF.SEGURIDAD_OPCION_MODULOASP SOM ON G.ACTIVIDAD = SOM.OPCION LEFT JOIN " +
						"SISSEMF.MOV_DEFSIEMBRAE SIE  ON G.SIEMBRA = SIE.NOSIEMBRA inner join (Select correlativoempleado,invernadero from sisappf.app_seguridad_consulta_sem where estado = 'AC') sg " +
						"on sie.invernadero = ''||sg.invernadero left join (select empresa,siembra,sum(plantas) Desecho from sissemf.mov_desechoplantasem " +
						"where fecha > '"+ Fechade +"' group by empresa,siembra) d on g.siembra  = d.siembra  where (G.ESTADO IS NULL OR G.ESTADO IN ('AC','CO')) AND " +
						"G.FECHA = '"+ Fechade +"' AND  G.actividadfinalizada = 1 AND SOM.CONTROL_PLANTAS = 'SI'  and " +
						"sg.correlativoempleado = '"+ CodigoEmpleado +"' GROUP BY G.ACTIVIDAD,SOM.DESCRIPCION,G.SIEMBRA,SIE.PLANTAS,sie.invernadero,d.desecho " +
						"HAVING (SUM(ifnull(G.PLANTAS,0)) - (ifnull(sie.plantas,0) + ifnull(d.desecho,0))) <> 0 ORDER BY sie.invernadero,G.ACTIVIDAD,g.siembra,sie.plantas");
		
					    //Agregar encabezados de columnas	
						String Cn1 = rs.getMetaData().getColumnName(1);
						encabezado.add(Cn1);
						gridEnc.setAdapter(adapEnc);
						String Cn2 = rs.getMetaData().getColumnName(2);
						encabezado.add(Cn2);
						gridEnc.setAdapter(adapEnc);
						String Cn3 = rs.getMetaData().getColumnName(3);
						encabezado.add(Cn3);
						gridEnc.setAdapter(adapEnc);
						String Cn4 = rs.getMetaData().getColumnName(4);
						encabezado.add(Cn4);
						gridEnc.setAdapter(adapEnc);
						String Cn5 = rs.getMetaData().getColumnName(5);
						encabezado.add(Cn5);
						gridEnc.setAdapter(adapEnc);
						String Cn6 = rs.getMetaData().getColumnName(6);
						encabezado.add(Cn6);
						gridEnc.setAdapter(adapEnc);
						String Cn7 = rs.getMetaData().getColumnName(7);
						encabezado.add(Cn7);
						gridEnc.setAdapter(adapEnc);
						// finaliza agregar encabezados de columnas	
						while (rs.next()) {
						//agrega filas de columnas
							 String d1 = rs.getString(1).trim();
							 String d2 = rs.getString(2).trim();
							 String d3 = rs.getString(3).trim();
							 String d4 = rs.getString(4).trim();
						     String d5 = rs.getString(5).trim();
							 String d6 = rs.getString(6).trim();
							 String d7 = rs.getString(7).trim();
							 String c1 = comprobar(d1);
							 String c2 = comprobar(d2);
							 String c3 = comprobar(d3);
							 String c4 = comprobar(d4);
							 String c5 = comprobar(d5);
							 String c6 = comprobar(d6);
							 String c7 = comprobar(d7);
							 datos.add(c1);
							 gridview.setAdapter(adapter);
							 datos.add(c2);
							 gridview.setAdapter(adapter);
							 datos.add(c3);
							 gridview.setAdapter(adapter);
							 datos.add(c4);
							 gridview.setAdapter(adapter);
							 datos.add(c5);
							 gridview.setAdapter(adapter);
							 datos.add(c6);
							 gridview.setAdapter(adapter);
							 datos.add(c7);
							 gridview.setAdapter(adapter);
						}
						connection.close();	
	            	}
					catch (Exception e) {
						Toast toast = Toast.makeText(getApplicationContext(), "Verificar Conexi�n.....", Toast.LENGTH_SHORT);
		            	toast.show();
						e.printStackTrace();
	            } 
	           } 	
	            }
	        });
		    
		    gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
	            @Override
	            public void onItemClick(AdapterView<?> parent, final View view, int position, long id){
	            			Pos =  parent.getItemIdAtPosition(position);
	            			int ifila = (int)(Pos/7);
	            			int iact = (ifila*7); 
	            			int icosto = (ifila*7)+2;
	            			int idesc = (ifila*7)+1;
	            			int iinv = (ifila*7)+6;
	            			
	            			Act = (String) parent.getItemAtPosition(iact);
	            			Desc = (String) parent.getItemAtPosition(idesc);
	            			Siembr = (String) parent.getItemAtPosition(icosto);
	            			Inv = (String) parent.getItemAtPosition(iinv);

	            			Intent intent = new Intent(getApplicationContext(), DetalleConsultaPlantas.class);
		    	        	intent.putExtra("Act", Act);
		    	        	intent.putExtra("Desc",Desc);
		    	        	intent.putExtra("Siembra", Siembr);
		    	        	intent.putExtra("Inv", Inv);
		    	        	intent.putExtra("CodigoEmpleado", CodigoEmpleado );
		    	        	intent.putExtra("Fecha", Fechade);
		    	        	startActivity(intent);    	    
		    	            }
		    	    });
		    	      
		}
		
		public void Cargar(){

        	datos.clear();
        	adapter.clear();
        	encabezado.clear();
        	adapEnc.clear();
     
        	Fechade = editFechade.getText().toString().trim();
        	String Pb = "";
        	
        	if(Fechade.equals(Pb)){
        		alertDialogMensaje("ERROR","PORFAVOR SELECCIONE UNA FECHA");
        	}
        	else 
        	{
        	try {
        	String user = "sim";
        	String pasword = "sim";
        	 Connection connection = null;
        	 ResultSet rs = null;
        	 Statement statement = null;
        	 
				Class.forName("com.ibm.as400.access.AS400JDBCDriver");
				connection = DriverManager.getConnection("jdbc:as400://190.190.190.10", user, pasword);	
				statement = connection.createStatement();			     
				rs = statement.executeQuery(
				"SELECT G.ACTIVIDAD ACT,SOM.DESCRIPCION DES,G.SIEMBRA,(ifnull(sie.plantas,0) + ifnull(d.desecho,0)) Plantas," +
				"SUM(ifnull(G.PLANTAS,0)) TRABAJADAS, (SUM(ifnull(G.PLANTAS,0)) - (ifnull(sie.plantas,0) + ifnull(d.desecho,0))) DIF, " +
				"sie.invernadero INV FROM SISAPPF.APP_TMP_GENERAL g LEFT JOIN SISCFGF.SEGURIDAD_OPCION_MODULOASP SOM ON G.ACTIVIDAD = SOM.OPCION LEFT JOIN " +
				"SISSEMF.MOV_DEFSIEMBRAE SIE  ON G.SIEMBRA = SIE.NOSIEMBRA inner join (Select correlativoempleado,invernadero from sisappf.app_seguridad_consulta_sem where estado = 'AC') sg " +
				"on sie.invernadero = ''||sg.invernadero left join (select empresa,siembra,sum(plantas) Desecho from sissemf.mov_desechoplantasem " +
				"where fecha > '"+ Fechade +"' group by empresa,siembra) d on g.siembra  = d.siembra  where (G.ESTADO IS NULL OR G.ESTADO IN ('AC','CO')) AND " +
				"G.FECHA = '"+ Fechade +"' AND  G.actividadfinalizada = 1 AND SOM.CONTROL_PLANTAS = 'SI'  and " +
				"sg.correlativoempleado = '"+ CodigoEmpleado +"' GROUP BY G.ACTIVIDAD,SOM.DESCRIPCION,G.SIEMBRA,SIE.PLANTAS,sie.invernadero,d.desecho " +
				"HAVING (SUM(ifnull(G.PLANTAS,0)) - (ifnull(sie.plantas,0) + ifnull(d.desecho,0))) <> 0 ORDER BY sie.invernadero,G.ACTIVIDAD,g.siembra,sie.plantas");

			    //Agregar encabezados de columnas	
				String Cn1 = rs.getMetaData().getColumnName(1);
				encabezado.add(Cn1);
				gridEnc.setAdapter(adapEnc);
				String Cn2 = rs.getMetaData().getColumnName(2);
				encabezado.add(Cn2);
				gridEnc.setAdapter(adapEnc);
				String Cn3 = rs.getMetaData().getColumnName(3);
				encabezado.add(Cn3);
				gridEnc.setAdapter(adapEnc);
				String Cn4 = rs.getMetaData().getColumnName(4);
				encabezado.add(Cn4);
				gridEnc.setAdapter(adapEnc);
				String Cn5 = rs.getMetaData().getColumnName(5);
				encabezado.add(Cn5);
				gridEnc.setAdapter(adapEnc);
				String Cn6 = rs.getMetaData().getColumnName(6);
				encabezado.add(Cn6);
				gridEnc.setAdapter(adapEnc);
				String Cn7 = rs.getMetaData().getColumnName(7);
				encabezado.add(Cn7);
				gridEnc.setAdapter(adapEnc);
				// finaliza agregar encabezados de columnas	
				while (rs.next()) {
				//agrega filas de columnas
					 String d1 = rs.getString(1).trim();
					 String d2 = rs.getString(2).trim();
					 String d3 = rs.getString(3).trim();
					 String d4 = rs.getString(4).trim();
				     String d5 = rs.getString(5).trim();
					 String d6 = rs.getString(6).trim();
					 String d7 = rs.getString(7).trim();
					 String c1 = comprobar(d1);
					 String c2 = comprobar(d2);
					 String c3 = comprobar(d3);
					 String c4 = comprobar(d4);
					 String c5 = comprobar(d5);
					 String c6 = comprobar(d6);
					 String c7 = comprobar(d7);
					 datos.add(c1);
					 gridview.setAdapter(adapter);
					 datos.add(c2);
					 gridview.setAdapter(adapter);
					 datos.add(c3);
					 gridview.setAdapter(adapter);
					 datos.add(c4);
					 gridview.setAdapter(adapter);
					 datos.add(c5);
					 gridview.setAdapter(adapter);
					 datos.add(c6);
					 gridview.setAdapter(adapter);
					 datos.add(c7);
					 gridview.setAdapter(adapter);
				}
				connection.close();	
        	}
			catch (Exception e) {
				Toast toast = Toast.makeText(getApplicationContext(), "Verificar Conexi�n.....", Toast.LENGTH_SHORT);
            	toast.show();
				e.printStackTrace();
        } 
       } 	
        
		}
		
		public void alertDialogMensaje(String message1, String mesage2){
		
			try {
			    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
			    ringtone.play();	    	    
			} catch (Exception e) {
			    e.printStackTrace();
			}
			AlertDialog alertDialog = new AlertDialog.Builder(this).create();
			alertDialog.setTitle(message1);
			alertDialog.setMessage(mesage2);
			alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
			// here you can add functions
				ringtone.stop();
			}
			});
			alertDialog.show();
		}
		
		public String comprobar(String cad){
			String Pb = "";
			if(cad.equals(Pb)){
				cad = "---";
			}
			return cad;	
		}
		
		public String format (int f){
			val = "";
			if(f<10){
			val = "0"+Integer.toString(f);	
			}
			else{
			val = Integer.toString(f);
			}
			return val;
		}
		
		public void VerificarDatos(String dato){	
			 Cursor CursorSemillas = db.getSemillas("Siembra='" + dato + "'");
			 if(CursorSemillas.getCount()>0){
				 Semillas s = db.getSemillasFromCursor(CursorSemillas, 0);
				 Log.d("SEMILLA",s.getSiembra() + ", "+s.getMaterial() + " ," + s.getPolinizacion());
				 validoDescriptivo = true;
				 } else {
					 validoDescriptivo = false;
				 }
			 CursorSemillas.close();
		}
				
		@Override
		public boolean onCreateOptionsMenu(Menu menu) {

			// Inflate the menu; this adds items to the action bar if it is present.
			getMenuInflater().inflate(R.menu.soporteit, menu);
			return true;
		}

		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			// Handle action bar item clicks here. The action bar will
			// automatically handle clicks on the Home/Up button, so long
			// as you specify a parent activity in AndroidManifest.xml.
			int id = item.getItemId();
			if (id == R.id.action_settings) {
				return true;
			}
			return super.onOptionsItemSelected(item);
		}
	}  
