package com.example.sim;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.os.Vibrator;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;


import com.example.floricultura.R;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.Dguardados;
import com.example.sim.data.JsonFiles;
import com.example.sim.data.Semillas;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DatosDGuardados extends ActionBarActivity {
	
	//Variables globales
		private MyApp appState;	
		private DBAdapter db;
		int CodigoModulo;
		int Codigodepartamento;
		int CodigoEmpleado;
		int CodigoOpcion;
		long Pos;
		String Siembr;
		String Act;
		String Desc;
		String HoraInicio;
		String HoraFinal;
		String NombreOpcion;
		GridView gridview;
		GridView gridEnc;
		Vibrator mVibrator;
		Ringtone ringtone;
		String Fechade;
		EditText Siembra;
		String siem;
		String Inv;
		int cargar = 0;
		
		boolean validoDescriptivo;
		private String val;
		ArrayList<String> datos;
		ArrayAdapter<String> adapter;
		ArrayList<String> encabezado;
		ArrayAdapter<String> adapEnc;
		EditText editFechade;
		
		@SuppressLint("NewApi") @Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.datos_dguardados);
			
			// para manejar la excepcion de permisos en el API de Android
	    	StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	        StrictMode.setThreadPolicy(policy);

			//se obtienen lo datos de la actividad antetior por medio del intent
			Bundle extras = getIntent().getExtras();
			CodigoModulo = extras.getInt("CodigoModulo");
		    CodigoEmpleado = extras.getInt("CodigoEmpleado");
		    Codigodepartamento = extras.getInt("Codigodepartamento");
		    CodigoOpcion = extras.getInt("CodigoOpcion");
		    NombreOpcion = extras.getString("NombreOpcion");
		    
		    //hacemos referencia a todos los objetos de nuestra vista
		    TextView TNombreCaptura = (TextView)findViewById(R.id.pruebas);
		    TextView TCodigoEmpleado= (TextView)findViewById(R.id.CodigoEmpleado);
		    TextView TCodigoOpcion= (TextView)findViewById(R.id.codigoOpcion);
	        Button btnBack = (Button) findViewById(R.id.btnBack);
	        Button btnOK = (Button) findViewById(R.id.btnOk);
	        Button btnfechade = (Button) findViewById(R.id.buttonfechade);
	        editFechade = (EditText) findViewById(R.id.editFechaDe);
	        Siembra = (EditText) findViewById(R.id.TextSiembra);
	        editFechade.setEnabled(false);
	        gridEnc = (GridView) findViewById(R.id.gridEnc);
	        gridview = (GridView) findViewById(R.id.gridDetalle);
	        
	        datos = new ArrayList<String>();
	        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, datos);
	        encabezado = new ArrayList<String>();
	        adapEnc = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, encabezado);
	        
		    //hacemos las inicializaciones necesarias a nuestra vista
		    TNombreCaptura.setText(NombreOpcion+ "");
		    TCodigoEmpleado.setText(CodigoEmpleado + "");
		    TCodigoOpcion.setText(CodigoOpcion + "");
		   
		    
		    //incializamos la BD asi como tambien obtenemos la referencia al singleton
		    appState = ((MyApp)getApplicationContext());   
		    db = appState.getDb();
		    db.open();

		    btnBack.setOnClickListener(new View.OnClickListener() {
	            public void onClick(View v) {
	 
	            	onBackPressed();
	            }
	        });
		    
		    btnfechade.setOnClickListener(new View.OnClickListener() {
	            public void onClick(View v) {
	            	Calendar mcurrentDate=Calendar.getInstance();
	                int mYear = mcurrentDate.get(Calendar.YEAR);
	                int mMonth = mcurrentDate.get(Calendar.MONTH);
	                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

	                DatePickerDialog mDatePicker=new DatePickerDialog(DatosDGuardados.this, new OnDateSetListener() {                  
	                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
	                    	int datodia = selectedday;
	                    	String dia = format(datodia);
	                    	int datomes = selectedmonth + 1;
	                    	String mes = format(datomes);
	                    	String datoanio = Integer.toString(selectedyear);
	                    	String dato = datoanio + "-" + mes + "-" +dia;
	                    	editFechade.setText(dato);
	                    }
	                }
	                ,mYear, mMonth, mDay);
	                mDatePicker.setTitle("Seleccionar Fecha");                
	                mDatePicker.show();  
	            }
	            
	        });
		    
		  //al recibir el enter se verifica si el descriptivo es correcto y lo manda al siguiente input sino le muestra un mensaje al usuario
	   
		    		    
		    btnOK.setOnClickListener(new View.OnClickListener() {
	            public void onClick(View v) {
	            	datos.clear();
	            	adapter.clear();
	            	encabezado.clear();
	            	adapEnc.clear();
	         
	            	Fechade = editFechade.getText().toString().trim();
	            	String Pb = "";
	            	
	            	if(Fechade.equals(Pb)){
	            		alertDialogMensaje("ERROR","PORFAVOR SELECCIONE UNA FECHA");
	            	}
	            	else 
	            	{
	            	try {
	            		appState = ((MyApp)getApplicationContext());
	     			    db = appState.getDb();
	     			    db.open();
	     			     String fecha = editFechade.getText().toString().trim();
	            		
	            		 String[]datoFecha= new String[]{fecha};
	 					 Cursor s = db.getDguardados(datoFecha);
	 					 int datosBd = s.getCount();
	 					 if(datosBd == 0){
	 						alertDialogMensaje("Mensaje","No Existen Datos con esta Fecha: "+Fechade);
	 					 }
	 					 else{
					    //Agregar encabezados de columnas	
						String Cn1 = s.getColumnName(0);
						encabezado.add(Cn1);
						
						gridEnc.setAdapter(adapEnc);
						String Cn2 = s.getColumnName(1);
						encabezado.add(Cn2);
						gridEnc.setAdapter(adapEnc);
						String Cn3 = s.getColumnName(2);
						encabezado.add(Cn3);
						gridEnc.setAdapter(adapEnc);
						String Cn4 = s.getColumnName(3);
						encabezado.add(Cn4);
						gridEnc.setAdapter(adapEnc);
						String Cn5 = s.getColumnName(4);
						encabezado.add(Cn5);
						gridEnc.setAdapter(adapEnc);
						String Cn6 = s.getColumnName(5);
						encabezado.add(Cn6);
						gridEnc.setAdapter(adapEnc);
						//String Cn7 = s.getColumnName(6);
						//encabezado.add(Cn7);
						//gridEnc.setAdapter(adapEnc);
						//String Cn8 = s.getColumnName(7);
						//encabezado.add(Cn8);
						//gridEnc.setAdapter(adapEnc);
						// finaliza agregar encabezados de columnas	
						
						while (s.moveToNext()) {
							
						//agrega filas de columnas
							 String d1 = s.getString(0).trim();
							 String d2 = s.getString(1).trim();
							 String d3 = s.getString(2).trim();
							 String d4 = s.getString(3).trim();
						     String d5 = s.getString(4).trim();
							 String d6 = s.getString(5).trim();
							 //String d7 = s.getString(6).trim();
							// String d8 = s.getString(7).trim();
							 String c1 = comprobar(d1);
							 String c2 = comprobar(d2);
							 String c3 = comprobar(d3);
							 String c4 = comprobar(d4);
							 String c5 = comprobar(d5);
							 String c6 = comprobar(d6);
							 //String c7 = comprobar(d7);
							// String c8 = comprobar(d8);
							 
							 
							 datos.add(c1);
							 gridview.setAdapter(adapter);
							 
							 datos.add(c2);
							 gridview.setAdapter(adapter);
							 datos.add(c3);
							 gridview.setAdapter(adapter);
							 datos.add(c4);
							 gridview.setAdapter(adapter);
							 datos.add(c5);
							 gridview.setAdapter(adapter);
							 datos.add(c6);
							 gridview.setAdapter(adapter);
						
							
							// datos.add(c7);
							// gridview.setAdapter(adapter);
							 //datos.add(c8);
							 //gridview.setAdapter(adapter);
						}
						
						db.close();	
	            	}
	            	}
					catch (Exception e) {
						Toast toast = Toast.makeText(getApplicationContext(), "Verificar Conexi�n.....", Toast.LENGTH_SHORT);
		            	toast.show();
						e.printStackTrace();
	            }
	            	
	           } 	
	            }
	        });
		    
		    gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
	            @Override
	            public void onItemClick(AdapterView<?> parent, final View view, int position, long id){
        			try{
        			Pos =  parent.getItemIdAtPosition(position);
        			int ifila = (int)(Pos/8);
        			int horaInicio = (ifila*8)+3;
        			int horaFin = (ifila*8)+4;
        			HoraInicio = (String) parent.getItemAtPosition(horaInicio);
        			HoraFinal = (String) parent.getItemAtPosition(horaFin);
        			db.open();
        		    
        		    Cursor d = db.getDguardados("HoraFin=" + HoraFinal.toString().trim() +" AND HoraIncio="+ HoraInicio.toString().trim());
        		  
        		    Cursor cursorDGuardados = db.getDGuardadosF(HoraInicio,HoraFinal);
        		    if(cursorDGuardados != null  && cursorDGuardados.getCount()>0){
        		    Dguardados dtemp =  db.getDguardadosFromCursor(cursorDGuardados, 0);
        		    ObjectMapper mapper = new ObjectMapper();
        			try {
        				// se refiere al nombre que se coloca en el bloc de notas
        				String nameJson ="G_" + dtemp.getCodigoEmpleado() + "_"+dtemp.getFecha() + "_"+dtemp.getHoraFin();
        				nameJson=nameJson.replace(":", "-");
        				mapper.writeValue(new File( Environment.getExternalStorageDirectory()+"/SIM/JsonDescarga/Finalizados/" + nameJson+ ".json"), dtemp);
        				JsonFiles archivo = new JsonFiles();
						archivo.setName(nameJson+ ".json");
						archivo.setNameFolder("Finalizados/");
						archivo.setUpload(0);
						db.insertarJsonFile(archivo);
						
						if(!appState.getSubiendoArchivos()){
							appState.setSubiendoArchivos(true);
							new UptoDropboxFin().execute(getApplicationContext());
							alertDialogMensaje("Mensaje","Archivo Enviado");
						}		
        			} catch (JsonGenerationException e) {
        				// TODO Auto-generated catch block
        				e.printStackTrace();
        			} catch (JsonMappingException e) {
        				// TODO Auto-generated catch block
        				e.printStackTrace();
        			} catch (IOException e) {
        				// TODO Auto-generated catch block
        				e.printStackTrace();
        			}
        			
        			
        		    }
        			db.close();
        			}
        			catch (Exception e) {
        			    e.printStackTrace();
        			}	
        		
        			/*Intent intent = new Intent(getApplicationContext(), detalleGrid.class);
        			intent.putExtra("HoraInicio", HoraInicio);
        			intent.putExtra("HoraFin", HoraFinal);
        			startActivity(intent);*/
    	            }
		    	    });
		    	      
		}
		
		/*public void crearJson(){

			
			Cursor CursorGuardados = db.getDguardados("CodigoEmpleado = " + Integer.parseInt(empleado) +" AND ActividadFinalizada = 0");
			
			//obtenerDato = txtPlantaMacho.getText().toString();
			//datoEntero = Integer.parseInt(obtenerDato);
		
			Dguardados dtemp =  db.getDguardadosFromCursor(CursorGuardados, 0);
			//Dguardados dtemp = new Dguardados();
			Calendar c = Calendar.getInstance(); 
			SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
			String fecha = fdate.format(c.getTime());
			SimpleDateFormat ftime = new SimpleDateFormat("HH:mm:ss");
			String horaFin = ftime.format(c.getTime());
			//dtemp.setPlantaMacho(datoEntero);	
			dtemp.setFecha(fecha);
			dtemp.setHoraFin(horaFin);
			dtemp.setSupervisor(CodigoEmpleado);
			dtemp.setActividadFinalizada(1);
			//dtemp.setPlantaMacho(datoEntero);
			
			//se obtienen los datos ingresados en la caja de texsto y si los datos son mayores a 0
			// se almacenan en la variable temporal dtemp		
			
			if(numeroPlantas.length()>0)
				dtemp.setPlantas(numeroPlantas);
			if(datoHora>0)
				dtemp.setHora(datoHora);
			if(datoMin>0)
				dtemp.setMin(datoMin);
			
		
			
			//if(PlantaMachos>0)
				//dtemp.setPlantaMacho(PlantaMachos);
			// se hace un update a la bd
			
			db.updateDguardados(dtemp);
		
			//EditPlantas.setText("");
			//EditFlores.setText("");
			//EditHora.setText("");
			//EditMin.setText("");
			
			
			
			// se crea el json DE SEMILLAS
			ObjectMapper mapper = new ObjectMapper();
			try {
				// se refiere al nombre que se coloca en el bloc de notas
				String nameJson ="G_" + dtemp.getCodigoEmpleado() + "_"+dtemp.getFecha() + "_"+dtemp.getHoraFin();
				nameJson=nameJson.replace(":", "-");
				mapper.writeValue(new File( Environment.getExternalStorageDirectory()+"/SIM/JsonDescarga/Finalizados/" + nameJson+ ".json"), dtemp);
				
				
				JsonFiles archivo = new JsonFiles();
				archivo.setName(nameJson+ ".json");
				archivo.setNameFolder("Finalizados/");

				if(appState.connectionAvailable() == false){
					archivo.setUpload(0);
					db.insertarJsonFile(archivo);
				} else{
					// se manda a llamar la clase Cargar y su m�todo CargaFin
					// si resulta correcta la carga se registra el archivo como Cargado, si no, se registra como pendiente
				    cargar = new Cargar().CargaFin(nameJson);
				  if(cargar == 1){
					  archivo.setUpload(1);
					  db.insertarJsonFile(archivo);
				  } else{
					  archivo.setUpload(0);
					   db.insertarJsonFile(archivo);
				  }
				
				}
				
			
				
			/*	
				if(!appState.getSubiendoArchivos()){
					appState.setSubiendoArchivos(true);
					new UptoDropbox().execute(getApplicationContext());
				}
				
			*
			} catch (JsonGenerationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		
		}*/
		public void alertDialogMensaje(String message1, String mesage2){
		
			try {
			    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
			    ringtone.play();	    	    
			} catch (Exception e) {
			    e.printStackTrace();
			}
			AlertDialog alertDialog = new AlertDialog.Builder(this).create();
			alertDialog.setTitle(message1);
			alertDialog.setMessage(mesage2);
			alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
			// here you can add functions
				ringtone.stop();
			}
			});
			alertDialog.show();
		}
		
		public String comprobar(String cad){
			String Pb = "";
			if(cad.equals(Pb)){
				cad = "---";
			}
			return cad;	
		}
		
		public String format (int f){
			val = "";
			if(f<10){
			val = "0"+Integer.toString(f);	
			}
			else{
			val = Integer.toString(f);
			}
			return val;
		}
		
		public void VerificarDatos(String dato){	
			 Cursor CursorSemillas = db.getSemillas("Siembra='" + dato + "'");
			 if(CursorSemillas.getCount()>0){
				 Semillas s = db.getSemillasFromCursor(CursorSemillas, 0);
				 Log.d("SEMILLA",s.getSiembra() + ", "+s.getMaterial() + " ," + s.getPolinizacion());
				 validoDescriptivo = true;
				 } else {
					 validoDescriptivo = false;
				 }
			 CursorSemillas.close();
		}
				
		@Override
		public boolean onCreateOptionsMenu(Menu menu) {

			// Inflate the menu; this adds items to the action bar if it is present.
			getMenuInflater().inflate(R.menu.soporteit, menu);
			return true;
		}

		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			// Handle action bar item clicks here. The action bar will
			// automatically handle clicks on the Home/Up button, so long
			// as you specify a parent activity in AndroidManifest.xml.
			int id = item.getItemId();
			if (id == R.id.action_settings) {
				return true;
			}
			return super.onOptionsItemSelected(item);
		}
	}  



//Cursor c = db.getDGuardadosF(HoraInicio,HoraFinal);
/*String Actividad;
String ActividadFinalizada;
String Azucarera;
String Bancas;
String CodigoDepto;
String CodigoEmpleado="";
String CorrelativoBolsa;
String CorrelativoBolsa2;
String CorrelativoBolsa3;
String CorrelativoBolsa4;
String CorrelativoBolsa5;
String Fecha;
String Filtro;
String Flores;
String Hora;
String HoraFin;
String HoraIncio;
String Invernadero;
String Min;
String PesoPolen;
String PlantaMacho;
String Plantas;
String Siembra;
String Supervisor;
String VolumenPolen;


/* if (c.moveToFirst()) {
    //Recorremos el cursor hasta que no haya m�s registros
//	do {
		Actividad = c.getString(0);
    	ActividadFinalizada  = c.getString(1);
    	Azucarera = c.getString(2);
    	Bancas = c.getString(3);
    	CodigoDepto = c.getString(4);
    	CodigoEmpleado = c.getString(5);
    	CorrelativoBolsa = c.getString(6);
    	CorrelativoBolsa2 = c.getString(7);
    	CorrelativoBolsa3 = c.getString(8);
    	CorrelativoBolsa4 = c.getString(9);
    	CorrelativoBolsa5 = c.getString(10);
    	Fecha = c.getString(11);
    	Filtro = c.getString(12);
    	Flores = c.getString(13);
    	Hora = c.getString(14);
    	HoraFin = c.getString(15);
    	HoraIncio = c.getString(16);
    	Invernadero = c.getString(17);
    	Min = c.getString(18);
    	PesoPolen = c.getString(19);
    	PlantaMacho = c.getString(20);
    	Plantas = c.getString(21);
    	Siembra = c.getString(22);
    	Supervisor = c.getString(23);
    	VolumenPolen = c.getString(24);

  // } while(c.moveToNext());
	
    
    
}*/
