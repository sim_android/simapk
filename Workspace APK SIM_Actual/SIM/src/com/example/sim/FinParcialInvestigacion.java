package com.example.sim;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.floricultura.R;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.Dempleado;
import com.example.sim.data.EInvestigacion;
import com.example.sim.data.JsonFiles;
import com.example.sim.data.Opciones;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class FinParcialInvestigacion extends ActionBarActivity {
	
  	private MyApp appState;	
	private DBAdapter db;
	int CodigoModulo;
	int Codigodepartamento;
	int CodigoEmpleado;
	int CodigoOpcion;
	String NombreOpcion;
	Ringtone ringtone;
	EditText Parametro;
    TextView TParametro;
    EditText Parametro2;
    TextView TParametro2;
    EditText Parametro3;
    TextView TParametro3;
	TextView Empleado;
	String NombreEmple;
	Button BuscaEmpleado;
	Button Guardar;
	String CodEmpleado;
	String[] datos = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fin_parcial_investigacion);
		
		Bundle extras = getIntent().getExtras();
		CodigoModulo = extras.getInt("CodigoModulo");
	    CodigoEmpleado = extras.getInt("CodigoEmpleado");
	    Codigodepartamento = extras.getInt("Codigodepartamento");
	    CodigoOpcion = extras.getInt("CodigoOpcion");
	    NombreOpcion = extras.getString("NombreOpcion");
	     
	    TextView TNombreCaptura = (TextView)findViewById(R.id.OpcionCaptura);
	    TextView TCodigoEmpleado= (TextView)findViewById(R.id.CodigoEmpleado);
	    //TextView TCodigoOpcion= (TextView)findViewById(R.id.codigoOpcion);
	    TParametro = (TextView) findViewById(R.id.TextParametro);
	    TParametro2 = (TextView) findViewById(R.id.TextParametro2);
	    TParametro3 = (TextView) findViewById(R.id.TextParametro3);
	    Empleado  = (TextView)findViewById(R.id.textEmpleado);
	    Button btnBack = (Button) findViewById(R.id.btnBack);
	    Button btnNuevo = (Button) findViewById(R.id.btnNuevo);
	    BuscaEmpleado = (Button) findViewById(R.id.botonBuscaEmpleado);
	    Guardar = (Button) findViewById(R.id.buttonGuarda);
	    
	    Parametro = (EditText) findViewById (R.id.editParametro);  
	    Parametro.setInputType(InputType.TYPE_CLASS_NUMBER |InputType.TYPE_NUMBER_FLAG_DECIMAL);
	    Parametro.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
	    Parametro.setSingleLine(false);
	    Parametro2 = (EditText) findViewById (R.id.editParametro2);  
	    Parametro2.setInputType(InputType.TYPE_CLASS_NUMBER |InputType.TYPE_NUMBER_FLAG_DECIMAL);
	    Parametro2.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
	    Parametro2.setSingleLine(false);
	    Parametro3 = (EditText) findViewById (R.id.editParametro3);  
	    Parametro3.setInputType(InputType.TYPE_CLASS_NUMBER |InputType.TYPE_NUMBER_FLAG_DECIMAL);
	    Parametro3.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
	    Parametro3.setSingleLine(false);
	      
	    TNombreCaptura.setText(NombreOpcion+ "");
	    TCodigoEmpleado.setText(CodigoEmpleado + "");
	    //TCodigoOpcion.setText(CodigoOpcion + "");
	    TParametro.setVisibility(View.INVISIBLE);
	    Parametro.setVisibility(View.INVISIBLE);
	    TParametro2.setVisibility(View.INVISIBLE);
	    Parametro2.setVisibility(View.INVISIBLE);
	    TParametro3.setVisibility(View.INVISIBLE);
	    Parametro3.setVisibility(View.INVISIBLE);
	    Guardar.setEnabled(false);
	    
	    //se valida la opcion finalizar actividad
	    appState = ((MyApp)getApplicationContext());   
	    db = appState.getDb();
	    db.open();
	      
	 // Comandos al Presionar el Boton Buscar Empleado
	    BuscaEmpleado.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	getDatos();
            	TParametro.setVisibility(View.INVISIBLE);
        	    Parametro.setVisibility(View.INVISIBLE);
        	    TParametro2.setVisibility(View.INVISIBLE);
        	    Parametro2.setVisibility(View.INVISIBLE);
        	    TParametro3.setVisibility(View.INVISIBLE);
        	    Parametro3.setVisibility(View.INVISIBLE);   
        	    Parametro.setText("");   
        	    Parametro2.setText("");
        	    Parametro3.setText("");
        	    Empleado.setText("");
        	    Guardar.setEnabled(false);
            	//Creando Cuadro de Dialogo para Listado de Personas
        		class DialogoSeleccion extends DialogFragment {
        	        @Override
        	        public Dialog onCreateDialog(Bundle savedInstanceState) {
        	         	  
        	            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        	    
						builder.setTitle("EMPLEADOS").setItems(datos, new DialogInterface.OnClickListener() {
        	                    public void onClick(DialogInterface dialog, int item) {
        	                        Log.i("Dialogos", "Opci�n elegida: " + datos[item]);
        	                        Empleado.setText(datos[item]);
        	                        int inicio = datos[item].indexOf(" ");
        	                        NombreEmple = datos[item].substring(inicio +2);
        	                        CodEmpleado =  datos[item].substring(0,inicio);
        	                        getParametros(CodEmpleado);
        	                    }
        	                });
        	            return builder.create(); }
        	    }
        		
        		FragmentManager fragmentManager = getSupportFragmentManager();
        		DialogoSeleccion dialogo = new DialogoSeleccion();
                dialogo.show(fragmentManager, "Alerta");
            }
            
        });
	    
	    Guardar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	String Val = "";
            	String Par = Parametro.getText().toString().trim();
            	if(CodEmpleado.equals(Val)){
            		alertDialogMensaje("ERROR"," Debe Seleccionar un Empleado ");
            	} else if(Par.equals(Val)){
            		alertDialogMensaje("ERROR"," Debe Ingresar el Parametro de Evaluacion ");
            		Parametro.requestFocus();
            	}  
            	else {
            		GuardarDatos();   
            		}                
            }
        });
	      
        btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	onBackPressed();
            }
        });
        
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	TParametro.setVisibility(View.INVISIBLE);
        	    Parametro.setVisibility(View.INVISIBLE);       	    
        	    Parametro.setText(""); 
        	    TParametro2.setVisibility(View.INVISIBLE);
        	    Parametro2.setVisibility(View.INVISIBLE);       	    
        	    Parametro2.setText(""); 
        	    TParametro3.setVisibility(View.INVISIBLE);
        	    Parametro3.setVisibility(View.INVISIBLE);       	    
        	    Parametro3.setText(""); 
        	    Empleado.setText("");
        	    Guardar.setEnabled(false);
            }
        });
     		
	}
	
	// Este metodo se utiliza para Buscar el Parametro de Evaluaci�n y Tipo Poliza a partir del C�digo de Empleado que recibe
	// Activa los Parametros y asigna el texto dependiendo de la Actividad a la que el empleado est� asignado
	public void getParametros(String Codigo){
		String Op;
		String par;
		String par2;
		String par3;
		Cursor ID = null;
		ID = this.db.getEInvestigacion("CodigoEmpleado= " + Codigo + " AND ActividadFinalizada=0");
		 for(int i=0 ; i < ID.getCount(); i++){	
			 EInvestigacion dgu = db.getEInvestigacionFromCursor(ID, i);
			 Op = dgu.getActividad().toString().trim();
			 Log.d("Asignado", dgu.getActividad() + ", " + dgu.getCodigoEmpleado());
			 Cursor TipoOpcion = this.db.getOpciones("Opcion=" + Op);
			 Opciones opc = this.db.getOpcionesFromCursor(TipoOpcion, 0);
			 Log.d("OPCION", opc.getDescripcion() + ", "+ opc.getCodigoDepto() + " ," +opc.getOpcion() + " ,"  + opc.getParametro());
			 par = opc.getParametro();
			 par2 = opc.getParametro2();
			 par3 = opc.getParametro3();
			 TParametro.setText(par);
			 TParametro2.setText(par2);
			 TParametro3.setText(par3);
			 Parametro.requestFocus();
			 TParametro.setVisibility(View.VISIBLE);
			 Parametro.setVisibility(View.VISIBLE);
			 TParametro2.setVisibility(View.VISIBLE);
			 Parametro2.setVisibility(View.VISIBLE);
			 TParametro3.setVisibility(View.VISIBLE);
			 Parametro3.setVisibility(View.VISIBLE);
			 Guardar.setEnabled(true);
			 TipoOpcion.close();
		 }
	}
	
	// Se obtiene el listado de Empleados Asignados que no han sido finalizados por medio del metodo getDatos()
	public void getDatos(){
		 Cursor Inves = null ;
		 Inves = this.db.getEInvestigacion("Supervisor=" + CodigoEmpleado + " AND ActividadFinalizada=0");
		 String tmp = null;
		 String dato = null;
		 int j = 0;
		 datos = new String[Inves.getCount()];
		 if(Inves.getCount()>0){
			 for(int i=0 ; i<Inves.getCount();i++){	 
				 EInvestigacion dg = db.getEInvestigacionFromCursor(Inves, i);
				 Cursor Codigoempleado = this.db.getDempledo("CodigoEmpleado=" + dg.getCodigoEmpleado().toString().trim());
				 Dempleado DatosEmpleado = this.db.getDempleadoFromCursor(Codigoempleado, 0);
				 if(DatosEmpleado != null){
					 dato = dg.getCodigoEmpleado().toString().trim() + "  " + DatosEmpleado.getNombre().toString().trim();
				 } else {
					 dato = dg.getCodigoEmpleado().toString().trim() + "  " + " NOMBRE NO ENCONTRADO ";
				 } 
				tmp = dato;
				datos[j] = tmp;
				j++;
				Codigoempleado.close();
			 	} 
		 	}
		 Inves.close();
		}
	
	public void GuardarDatos(){	
		String Tpa1 = TParametro.getText().toString().trim();
		String Tpa2 = TParametro2.getText().toString().trim();
		String Tpa3 = TParametro3.getText().toString().trim();
    	String Par = Parametro.getText().toString().trim();
    	String Par2 = Parametro2.getText().toString().trim();
    	String Par3 = Parametro3.getText().toString().trim();
		Cursor CursorGuardados = db.getEInvestigacion("CodigoEmpleado = " + Integer.parseInt(CodEmpleado) +" AND ActividadFinalizada = 0");
		if(CursorGuardados != null  && CursorGuardados.getCount()>0){
			EInvestigacion dtemp =  db.getEInvestigacionFromCursor(CursorGuardados, 0);
			
			if(Tpa1.equals(""))
				Tpa1 = "0";
			if(Tpa2.equals(""))
				Tpa2 = "0";
			if(Tpa3.equals(""))
				Tpa3 = "0";
			if(Par.equals(""))
				Par = "0";
			if(Par2.equals(""))
				Par2 = "0";
			if(Par3.equals(""))
				Par3 = "0";			
    		Calendar c = Calendar.getInstance(); 
    		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
    		String fecha = fdate.format(c.getTime());
    		SimpleDateFormat ftime = new SimpleDateFormat("HH:mm:ss");
    		String horaFin = ftime.format(c.getTime());
    		dtemp.setHora(0);
    		dtemp.setMin(0);
    		dtemp.setFecha(fecha);
			dtemp.setHoraFin(horaFin);
			dtemp.setTextoP1(Tpa1);
			dtemp.setTextoP2(Tpa2);
			dtemp.setTextoP3(Tpa3);
			dtemp.setParametro(Par);
			dtemp.setParametro2(Par2);
			dtemp.setParametro3(Par3);
			dtemp.setSupervisor(CodigoEmpleado);
			dtemp.setActividadFinalizada(1);
		
			db.updateEInvestigacion(dtemp);
			

    		ObjectMapper mapper = new ObjectMapper();
    		try {
    			String nameJson ="ID_" + dtemp.getCodigoEmpleado() + "_"+dtemp.getFecha() + "_"+dtemp.getHoraFin();
    			nameJson=nameJson.replace(":", "-");
				mapper.writeValue(new File( Environment.getExternalStorageDirectory()+"/SIM/JsonDescarga/Finalizados/" + nameJson+ ".json"), dtemp);
				
				
				JsonFiles archivo = new JsonFiles();
				archivo.setName(nameJson+ ".json");
				archivo.setNameFolder("Finalizados/");
				archivo.setUpload(0);
				db.insertarJsonFile(archivo);
				
				if(!appState.getSubiendoArchivos()){
					appState.setSubiendoArchivos(true);
					new UptoDropboxFin().execute(getApplicationContext());
				}	
			} catch (JsonGenerationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		
    	    Parametro.setText("");
    	    Parametro2.setText(""); 
    	    Parametro3.setText(""); 
    	    TParametro.setVisibility(View.INVISIBLE);
    	    Parametro.setVisibility(View.INVISIBLE);
    	    TParametro2.setVisibility(View.INVISIBLE);
    	    Parametro2.setVisibility(View.INVISIBLE);
    	    TParametro3.setVisibility(View.INVISIBLE);
    	    Parametro3.setVisibility(View.INVISIBLE);
    	    Empleado.setText("");
    	    Guardar.setEnabled(false);
    		Intent intent = new Intent(getApplicationContext(), AsignaInvestigacion.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        	intent.putExtra("CodEmpleado", CodEmpleado);
        	intent.putExtra("NombreEmple", NombreEmple);
        	intent.putExtra("CodigoEmpleado",CodigoEmpleado);
        	intent.putExtra("CodigoModulo", CodigoModulo);
        	intent.putExtra("Codigodepartamento", Codigodepartamento);
        	startActivity(intent);	
		}else{
			alertDialogMensaje("Invalido", "Empleado invalido");
			TParametro.setVisibility(View.INVISIBLE);
    	    Parametro.setVisibility(View.INVISIBLE);  	    
    	    Empleado.setText("");
    	    Guardar.setEnabled(false);
		}
		CursorGuardados.close();
	}
	
	public void hideSoftKeyboard() {
	    if(getCurrentFocus()!=null) {
	        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
	        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),   InputMethodManager.HIDE_NOT_ALWAYS);
	    }
	}
		
	public void alertDialogMensaje(String message1, String mesage2){	
		try {
		    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
		    ringtone.play(); 
		} catch (Exception e) {
		    e.printStackTrace();
		}	
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(message1);
		alertDialog.setMessage(mesage2);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int which) {
		// here you can add functions
			ringtone.stop();
		}
		});
		alertDialog.show();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.finalizar_actividad, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void ButtonOnClick(View v) {
	    switch (v.getId()) {
	      case R.id.buttonborra:
		        agregarNumero("1");
	        break;
	      case R.id.btnDos:
	    	  agregarNumero("2");
	        break;
	      case R.id.btnTres:
		        agregarNumero("3");
	        break;
	      case R.id.btnCuatro:
	    	  agregarNumero("4");
	        break;
	      case R.id.btnCinco:
		        agregarNumero("5");
	        break;
	      case R.id.btnSeis:
	    	  agregarNumero("6");
	        break;
	      case R.id.btnSiete:
	    	  agregarNumero("7");
	        break;
	      case R.id.btnOcho:
		        agregarNumero("8");
	        break;
	      case R.id.btnNueve:
	    	  agregarNumero("9");
	        break;
	      case R.id.button0:
		        agregarNumero("0");
	        break;
	      case R.id.btnCero:
	    	  agregarNumero("b");
	        break;
	      case R.id.botonp:
	    	  agregarNumero(".");
	        break;
	      }
	     
	}
	
	public void agregarNumero(String n){
		
		if(n.contains("b")){
			if(Parametro.isFocused()){
				Parametro.setText("");
			} else if(Parametro2.isFocused()){
				Parametro2.setText("");
			} else if(Parametro3.isFocused()){
				Parametro3.setText("");
			}
		}else{
			if(Parametro.isFocused()){
				Parametro.setText(Parametro.getText() + n);
			} else if(Parametro2.isFocused()){
				Parametro2.setText(Parametro2.getText() + n);
			} else if(Parametro3.isFocused()){
				Parametro3.setText(Parametro3.getText() + n);
			}
		}			
	}
}
