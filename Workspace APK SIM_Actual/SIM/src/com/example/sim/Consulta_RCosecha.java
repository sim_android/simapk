package com.example.sim;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;


import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;


import java.util.Calendar;
//import java.util.Date;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Color;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.os.Vibrator;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.dropbox.core.DbxException;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.FileMetadata;
import com.example.floricultura.R;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.DisponibilidadSemillas;

public class Consulta_RCosecha extends ActionBarActivity {
	//Variables globales
	private MyApp appState;	
	private DBAdapter db;
	int CodigoModulo;
	int numeroDeVeces = 0;
	int numeroRepite = 0;
	int Codigodepartamento;
	int CodigoEmpleado;
	int CodigoOpcion;
	String Val;
	String NombreOpcion;
	Vibrator mVibrator;
	Ringtone ringtone;
	String Fecha;	
	String FechaA;
	TableLayout tabla;
	String caracter = "AND";
	String palabra = "BETWEEN";
	private ProgressDialog loadingDialogDropbox;
	private final String DIR_APP = "/prueba/";
	private String val;
	String user ="";
	String password = "";
	String URL = "";
	CheckBox CheckLocal;
	String listadoJson = "query.txt";
	String texto;
	private DbxClientV2 dbxClientV2;
	@SuppressLint("NewApi") @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.consulta_rcosecha);
		int currentOrientation = this.getResources().getConfiguration().orientation;
	    if (currentOrientation == Configuration.ORIENTATION_PORTRAIT){
	        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	    }
	    else{
	        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
	    }
		
		// para manejar la excepcion de permisos en el API de Android
    	StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

		//se obtienen lo datos de la actividad antetior por medio del intent
		Bundle extras = getIntent().getExtras();
		CodigoModulo = extras.getInt("CodigoModulo");
	    CodigoEmpleado = extras.getInt("CodigoEmpleado");
	    Codigodepartamento = extras.getInt("Codigodepartamento");
	    CodigoOpcion = extras.getInt("CodigoOpcion");
	    NombreOpcion = extras.getString("NombreOpcion");
	    
	    //hacemos referencia a todos los objetos de nuestra vista
	    TextView TNombreCaptura = (TextView)findViewById(R.id.OpcionCaptura);
	    TextView TCodigoEmpleado= (TextView)findViewById(R.id.CodigoEmpleado);
	    TextView TCodigoOpcion= (TextView)findViewById(R.id.codigoOpcion);
        Button btnBack = (Button) findViewById(R.id.btnBack);
        Button btnOK = (Button) findViewById(R.id.btnAsistencia);
        Button btnfechade = (Button) findViewById(R.id.buttonfechade);
        Button btnFechaA = (Button)findViewById(R.id.fechaA);
        CheckLocal = (CheckBox) findViewById(R.id.CheckLocal);
        final EditText editFechade = (EditText) findViewById(R.id.editFechaDe);
        final TextView twFechaA = (TextView)findViewById(R.id.twFechaA);
        editFechade.setEnabled(false);
        final TableLayout tabla = (TableLayout) findViewById(R.id.tabla);     
        //final TableLayout tabla2 = (TableLayout)findViewById(R.id.tabla2);
	    //hacemos las inicializaciones necesarias a nuestra vista
	    TNombreCaptura.setText(NombreOpcion+ "");
	    TCodigoEmpleado.setText(CodigoEmpleado + "");
	    TCodigoOpcion.setText(CodigoOpcion + "");
	  
	    
	    //Comentario.requestFocus();

	    //incializamos la BD asi como tambien obtenemos la referencia al singleton
	    appState = ((MyApp)getApplicationContext());   
	    db = appState.getDb();
	    db.open();
	    dbxClientV2 = appState.getDropBoxClient();
	    btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
 
            	onBackPressed();
            }
        });
	    
	    btnfechade.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Calendar mcurrentDate=Calendar.getInstance();
                int mYear = mcurrentDate.get(Calendar.YEAR);
                int mMonth = mcurrentDate.get(Calendar.MONTH);
                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);
             
                DatePickerDialog mDatePicker = new DatePickerDialog(Consulta_RCosecha.this, new OnDateSetListener() {                  
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                    	int datodia = selectedday;
                    	String dia = format(datodia);
                    	int datomes = selectedmonth + 1;
                    	String mes = format(datomes);
                    	String datoanio = Integer.toString(selectedyear);
                    	// se formatea la fecha a conveniencia en este caso YYY - MM - DD  pudiera ser YYYY '/' '-' etc...
                    	String dato = datoanio + "-" + mes + "-" +dia;
                    	editFechade.setText(dato);
                    	twFechaA.setText(dato);
                    }
                }
                ,mYear, mMonth, mDay);
                mDatePicker.setTitle("Seleccionar Fecha");                
                mDatePicker.show();  
            }
            
        });
	    
	    btnFechaA.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

            	Calendar mcurrentDate=Calendar.getInstance();
                int mYear = mcurrentDate.get(Calendar.YEAR);
                int mMonth = mcurrentDate.get(Calendar.MONTH);
                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);
             
                DatePickerDialog mDatePicker = new DatePickerDialog(Consulta_RCosecha.this, new OnDateSetListener() {                  
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                    	int datodia = selectedday;
                    	String dia = format(datodia);
                    	int datomes = selectedmonth + 1;
                    	String mes = format(datomes);
                    	String datoanio = Integer.toString(selectedyear);
                    	// se formatea la fecha a conveniencia en este caso YYY - MM - DD  pudiera ser YYYY '/' '-' etc...
                    	String dato = datoanio + "-" + mes + "-" +dia;
                    	twFechaA.setText(dato);
                    }
                }
                ,mYear, mMonth, mDay);
                mDatePicker.setTitle("Seleccionar Fecha");                
                mDatePicker.show();  
            
				
			}
		});
	    	    
	    btnOK.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	
            	String Pb = "";
            	String es = " | ";
            	Fecha = editFechade.getText().toString().trim();
            	FechaA = twFechaA.getText().toString().trim();
            	tabla.removeAllViews();
            	if(Fecha.equals(Pb) && FechaA.equals(Pb) || (Fecha.equals(Pb) || FechaA.equals(Pb))){
            		alertDialogMensaje("ERROR","PORFAVOR SELECCIONE AMBAS FECHAS");
            		editFechade.setText("");
            		editFechade.requestFocus();
            	}else{
            		leer();	
            	try {
            		
            	 user = "sim";
            	 password = "sim";
            	 Connection connection = null;
            	 ResultSet rs = null;
            	 Statement statement = null;
            	 PreparedStatement pstmt;
					Class.forName("com.ibm.as400.access.AS400JDBCDriver");
					connection = DriverManager.getConnection("jdbc:as400://190.190.190.10", user, password);
					System.setProperty("http.keepAlive", "false");
					pstmt = connection.prepareStatement(texto); 
					
				          
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
					//String Fechita = "2018-07-02";
					Date fechDe = new java.sql.Date(format.parse(Fecha).getTime());
					Date fechA = new java.sql.Date(format.parse(FechaA).getTime());
				//	numeroDeVeces = contarCaracteres(texto, caracter)+1;
				    numeroDeVeces=	contarString(texto,caracter);
				    numeroRepite = contarString(texto,palabra);
					for(int i=1;i<=numeroDeVeces*2;i+=2){
						pstmt.setDate(i, fechDe);
					}
					for(int i=1;i<=numeroRepite*2;i+=1){
						pstmt.setDate(i, fechA);
					}
					/*pstmt.setDate(1, fech);
					pstmt.setDate(2, fech);
					pstmt.setDate(3, fech);
					pstmt.setDate(4, fech);
					pstmt.setDate(5, fech);
					pstmt.setDate(6, fech);
					pstmt.setDate(7, fech);
					pstmt.setDate(8, fech);
					*/
					//pstmt.setDate(9, fech);
					pstmt.setInt(9,CodigoEmpleado);
					
					//statement = connection.createStatement();		
					
					
					rs = pstmt.executeQuery();
					if (!rs.isBeforeFirst()){
						alertDialogMensaje("Error","No Existen Datos");
					}
					else
					{
						//Agregar encabezados de columnas	
						
						 TableRow Erow1 = new TableRow(Consulta_RCosecha.this);
						 TextView E1 = new TextView(Consulta_RCosecha.this);
						 TextView E2 = new TextView(Consulta_RCosecha.this);
						 TextView E3 = new TextView(Consulta_RCosecha.this);
						 TextView E4 = new TextView(Consulta_RCosecha.this);
						 TextView E5 = new TextView(Consulta_RCosecha.this);
						 TextView E6 = new TextView(Consulta_RCosecha.this);
						 TextView E7 = new TextView(Consulta_RCosecha.this);
						 TextView E8 = new TextView(Consulta_RCosecha.this); 
						 TextView E9 = new TextView(Consulta_RCosecha.this); 
						 TextView E10 = new TextView(Consulta_RCosecha.this); 
						 TextView E11 = new TextView(Consulta_RCosecha.this); 
						 TextView E12 = new TextView(Consulta_RCosecha.this); 
						 TextView E13 = new TextView(Consulta_RCosecha.this);
						 TextView E14 = new TextView(Consulta_RCosecha.this);
						 TextView E15 = new TextView(Consulta_RCosecha.this);
						 TextView E16 = new TextView(Consulta_RCosecha.this);
						 TextView E17 = new TextView(Consulta_RCosecha.this);
						 TextView E18 = new TextView(Consulta_RCosecha.this);
						 TextView E19 = new TextView(Consulta_RCosecha.this);
						 TextView E20 = new TextView(Consulta_RCosecha.this);
						 TextView E21 = new TextView(Consulta_RCosecha.this);
						 TextView E22 = new TextView(Consulta_RCosecha.this);
						 TextView E23 = new TextView(Consulta_RCosecha.this);
						 TextView E24 = new TextView(Consulta_RCosecha.this);
						 TextView E25 = new TextView(Consulta_RCosecha.this);
						 TextView E26 = new TextView(Consulta_RCosecha.this);
						 TextView E27 = new TextView(Consulta_RCosecha.this);
						 TextView E28 = new TextView(Consulta_RCosecha.this);
						 TextView E29 = new TextView(Consulta_RCosecha.this);
						 TextView E30 = new TextView(Consulta_RCosecha.this);
						 TextView E31 = new TextView(Consulta_RCosecha.this);
						 TextView E32 = new TextView(Consulta_RCosecha.this);
						 TextView E33 = new TextView(Consulta_RCosecha.this);
						 
						 
						 E1.setTextSize(20);
						 E1.setTextColor(Color.BLACK);

						 E2.setTextSize(20);
						 E2.setTextColor(Color.BLACK);
						 E3.setTextSize(20);
						 E3.setTextColor(Color.BLACK);
						 E4.setTextSize(20);
						 E4.setTextColor(Color.BLACK);
						 E5.setTextSize(20);
						 E5.setTextColor(Color.BLACK);
						 E6.setTextSize(20);
						 E6.setTextColor(Color.BLACK);
						 E7.setTextSize(20);
						 E7.setTextColor(Color.BLACK);
						 E8.setTextSize(20);
						 E8.setTextColor(Color.BLACK);
						 E9.setTextSize(20);
						 E9.setTextColor(Color.BLACK);
						 E10.setTextSize(20);
						 E10.setTextColor(Color.BLACK);
						 E11.setTextSize(20);
						 E11.setTextColor(Color.BLACK);
						 E12.setTextSize(20);
						 E12.setTextColor(Color.BLACK);
						 E13.setTextSize(20);
						 E13.setTextColor(Color.BLACK);
						 E14.setTextSize(20);
						 E14.setTextColor(Color.BLACK);
						 E15.setTextSize(20);
						 E15.setTextColor(Color.BLACK);
						 E16.setTextSize(20);
						 E16.setTextColor(Color.BLACK);
						 E17.setTextSize(20);
						 E17.setTextColor(Color.BLACK);
						 E18.setTextSize(20);
						 E18.setTextColor(Color.BLACK);
						 E19.setTextSize(20);
						 E19.setTextColor(Color.BLACK);
						 E20.setTextSize(20);
						 E20.setTextColor(Color.BLACK);
						 E21.setTextSize(20);
						 E21.setTextColor(Color.BLACK);
						 E22.setTextSize(20);
						 E22.setTextColor(Color.BLACK);
						 E23.setTextSize(20);
						 E23.setTextColor(Color.BLACK);
						 E24.setTextSize(20);
						 E24.setTextColor(Color.BLACK);
						 E25.setTextSize(20);
						 E25.setTextColor(Color.BLACK);
						 E26.setTextSize(20);
						 E26.setTextColor(Color.BLACK);
						 E27.setTextSize(20);
						 E27.setTextColor(Color.BLACK);
						 E28.setTextSize(20);
						 E28.setTextColor(Color.BLACK);
						 E29.setTextSize(20);
						 E29.setTextColor(Color.BLACK);
						 E30.setTextSize(20);
						 E30.setTextColor(Color.BLACK);
						 E31.setTextSize(20);
						 E31.setTextColor(Color.BLACK);
						 E32.setTextSize(20);
						 E32.setTextColor(Color.BLACK);
						 E33.setTextSize(20);
						 E33.setTextColor(Color.BLACK);
						 String Cn1 = rs.getMetaData().getColumnName(1);
						 String Cn2 = rs.getMetaData().getColumnName(2);
						 String Cn3 = rs.getMetaData().getColumnName(3);
						 String Cn4 = rs.getMetaData().getColumnName(4);
						 String Cn5 = rs.getMetaData().getColumnName(5);
						 String Cn6 = rs.getMetaData().getColumnName(6);
						 String Cn7 = rs.getMetaData().getColumnName(7);
						 String Cn8 = rs.getMetaData().getColumnName(8);
						 String Cn9 = rs.getMetaData().getColumnName(9);
						 String Cn10 = rs.getMetaData().getColumnName(10);
						 String Cn11 = rs.getMetaData().getColumnName(11);
						 String Cn12 = rs.getMetaData().getColumnName(12);
						 String Cn13 = rs.getMetaData().getColumnName(13);
						 String Cn14 = rs.getMetaData().getColumnName(14);
						 String Cn15 = rs.getMetaData().getColumnName(15);
						 String Cn16 = rs.getMetaData().getColumnName(16);
						 String Cn17 = rs.getMetaData().getColumnName(17);
						 
						 
						 E1.setText(Cn1);
						 E2.setText(Cn2);
						 E3.setText(Cn3);
						 E4.setText(Cn4);
						 E5.setText(Cn5);
						 E6.setText(Cn6);
						 E7.setText(Cn7);
						 E8.setText(Cn8);
						 E9.setText(Cn9);
						 E10.setText(Cn10);
						 E11.setText(Cn11);
						 E12.setText(Cn12);
						 E13.setText(Cn13);
						 E14.setText(Cn14);
						 E15.setText(Cn15);
						 E16.setText(Cn16);
						 E17.setText(Cn17);
						
						 
						 E18.setText(es);
						 E19.setText(es);
						 E20.setText(es);
						 E21.setText(es);
						 E22.setText(es);
						 E23.setText(es);
						 E24.setText(es);
						 E25.setText(es);
						 E26.setText(es);
						 E27.setText(es);
						 E28.setText(es);
						 E29.setText(es);
						 E30.setText(es);
						 E31.setText(es);
						 E32.setText(es);
						 E33.setText(es);
					
						 
						 Erow1.addView(E1);
						 Erow1.addView(E18);
						 
						 Erow1.addView(E2);
						 Erow1.addView(E19);
						 
						 Erow1.addView(E3);
						 Erow1.addView(E20);
						 
						 Erow1.addView(E4);
						 Erow1.addView(E21);
						 
						 Erow1.addView(E5);
						 Erow1.addView(E22);
						 
						 Erow1.addView(E6);
						 Erow1.addView(E23);
						 
						 Erow1.addView(E7);
						 Erow1.addView(E24);
						 
						 Erow1.addView(E8);
						 Erow1.addView(E25);
						 
						 Erow1.addView(E9);
						 Erow1.addView(E26);
						 
						 Erow1.addView(E10);
						 Erow1.addView(E27);
						 
						 Erow1.addView(E11);
						 Erow1.addView(E28);
						 
						 Erow1.addView(E12);
						 Erow1.addView(E29);
						 
						 Erow1.addView(E13);
						 Erow1.addView(E30);
						 
						 Erow1.addView(E14);
						 Erow1.addView(E31);
						 
						 Erow1.addView(E15);
						 Erow1.addView(E32);
						 
						 Erow1.addView(E16);
						 Erow1.addView(E33);
						 
						 Erow1.addView(E17);
						
						 
						 
						 E1.setGravity(Gravity.CENTER_HORIZONTAL);
						 E2.setGravity(Gravity.CENTER_VERTICAL);
						 E3.setGravity(Gravity.CENTER_VERTICAL);
						 E4.setGravity(Gravity.CENTER_HORIZONTAL);
						 E5.setGravity(Gravity.CENTER_HORIZONTAL);
						 E6.setGravity(Gravity.CENTER_HORIZONTAL);
						 E7.setGravity(Gravity.CENTER_HORIZONTAL);
						 E8.setGravity(Gravity.CENTER_HORIZONTAL);
						 E9.setGravity(Gravity.CENTER_HORIZONTAL);
						 E10.setGravity(Gravity.CENTER_HORIZONTAL);
						 E11.setGravity(Gravity.CENTER_HORIZONTAL);
						 E12.setGravity(Gravity.CENTER_HORIZONTAL);
						 E13.setGravity(Gravity.CENTER_HORIZONTAL);
						 E14.setGravity(Gravity.CENTER_HORIZONTAL);
						 E15.setGravity(Gravity.CENTER_HORIZONTAL);
						 E16.setGravity(Gravity.CENTER_HORIZONTAL);
						 E17.setGravity(Gravity.CENTER_HORIZONTAL);
						 E18.setGravity(Gravity.CENTER_HORIZONTAL);
						 E19.setGravity(Gravity.CENTER_HORIZONTAL);
						 E20.setGravity(Gravity.CENTER_HORIZONTAL);
						 E21.setGravity(Gravity.CENTER_HORIZONTAL);
						 E22.setGravity(Gravity.CENTER_HORIZONTAL);
						 E23.setGravity(Gravity.CENTER_HORIZONTAL);
						 E24.setGravity(Gravity.CENTER_HORIZONTAL);
						 E25.setGravity(Gravity.CENTER_HORIZONTAL);
						 E26.setGravity(Gravity.CENTER_HORIZONTAL);
						 E27.setGravity(Gravity.CENTER_HORIZONTAL);
						 E28.setGravity(Gravity.CENTER_HORIZONTAL);
						 E29.setGravity(Gravity.CENTER_HORIZONTAL);
						 E30.setGravity(Gravity.CENTER_HORIZONTAL);
						 E31.setGravity(Gravity.CENTER_HORIZONTAL);
						 E32.setGravity(Gravity.CENTER_HORIZONTAL);
						 E33.setGravity(Gravity.CENTER_HORIZONTAL);
						 
						 
						 Erow1.setGravity(Gravity.CENTER);
						 Erow1.setBackgroundColor(Color.GREEN);
						 tabla.addView(Erow1);	
						
						// finaliza agregar encabezados de columnas 
						int sz = rs.getFetchSize();
						int cont = 0;
						while (rs.next()) {
							
							 String d1 = rs.getString(1);
							 String d2 = rs.getString(2);
							 String d3 = rs.getString(3);
							 String d4 = rs.getString(4);
						     String d5 = rs.getString(5);
							 String d6 = rs.getString(6);
							 String d7 = rs.getString(7);
							 String d8 = rs.getString(8);
							 String d9 = rs.getString(9);
							 String d10 = rs.getString(10);
							 String d11 = rs.getString(11);
						     String d12 = rs.getString(12);
							 String d13 = rs.getString(13);
							 String d14 = rs.getString(14);
							 String d15 = rs.getString(15);
							 String d16 = rs.getString(16);
							 String d17 = rs.getString(17);

							/* String c1 = comprobar(d1);
							 String c2 = comprobar(d2);
							 String c3 = comprobar(d3);
							 String c4 = comprobar(d4);
							 String c5 = comprobar(d5);
							 String c6 = comprobar(d6);
							 String c7 = comprobar(d7);
							 String c8 = comprobar(d8);
							 String c9 = comprobar(d9);
							 String c10 = comprobar(d10);
							 */
							 //String c11 = comprobar(d11);
							// String c12 = comprobar(d12);
							// String c13 = comprobar(d13);
							// String c14 = comprobar(d14);
							// String c15 = comprobar(d15);
							// String c16 = comprobar(d16);
							 //String c17 = comprobar(d17);
							 //int i = 0; i == sz; i++
							 for (int i = 0; i == sz; i++)
							 {	
								 cont = cont +1;
								 TableRow row1 = new TableRow(Consulta_RCosecha.this);
								 TextView t1 = new TextView(Consulta_RCosecha.this);
								 TextView t2 = new TextView(Consulta_RCosecha.this);
								 TextView t3 = new TextView(Consulta_RCosecha.this);
								 TextView t4 = new TextView(Consulta_RCosecha.this);
								 TextView t5 = new TextView(Consulta_RCosecha.this);
								 TextView t6 = new TextView(Consulta_RCosecha.this);
								 TextView t7 = new TextView(Consulta_RCosecha.this);
								 TextView t8 = new TextView(Consulta_RCosecha.this); 
								 TextView t9 = new TextView(Consulta_RCosecha.this); 
								 TextView t10 = new TextView(Consulta_RCosecha.this); 
								 TextView t11 = new TextView(Consulta_RCosecha.this); 
								 TextView t12 = new TextView(Consulta_RCosecha.this); 
								 TextView t13 = new TextView(Consulta_RCosecha.this);
								 TextView t14 = new TextView(Consulta_RCosecha.this); 
								 TextView t15 = new TextView(Consulta_RCosecha.this); 
								 TextView t16 = new TextView(Consulta_RCosecha.this); 
								 TextView t17 = new TextView(Consulta_RCosecha.this); 
								 TextView t18 = new TextView(Consulta_RCosecha.this); 
								 TextView t19 = new TextView(Consulta_RCosecha.this);
								 TextView t20 = new TextView(Consulta_RCosecha.this);
								 TextView t21 = new TextView(Consulta_RCosecha.this);
								 TextView t22 = new TextView(Consulta_RCosecha.this);
								 TextView t23 = new TextView(Consulta_RCosecha.this);
								 TextView t24 = new TextView(Consulta_RCosecha.this);
								 TextView t25 = new TextView(Consulta_RCosecha.this);
								 TextView t26 = new TextView(Consulta_RCosecha.this);
								 TextView t27 = new TextView(Consulta_RCosecha.this);
								 TextView t28 = new TextView(Consulta_RCosecha.this);
								 TextView t29 = new TextView(Consulta_RCosecha.this);
								 TextView t30 = new TextView(Consulta_RCosecha.this);
								 TextView t31 = new TextView(Consulta_RCosecha.this);
								 TextView t32 = new TextView(Consulta_RCosecha.this);
								 TextView t33 = new TextView(Consulta_RCosecha.this);
						
								 
								 t1.setTextSize(20);
								 t1.setTextColor(Color.BLACK);
								 
								 t2.setTextSize(20);
								 t2.setTextColor(Color.BLACK);
								 
								 t3.setTextSize(20);
								 t3.setTextColor(Color.BLACK);
								 
								 t4.setTextSize(20);
								 t4.setTextColor(Color.BLACK);
								 
								 t5.setTextSize(20);
								 t5.setTextColor(Color.BLACK);
								 
								 t6.setTextSize(20);
								 t6.setTextColor(Color.BLACK);
								 
								 t7.setTextSize(20);
								 t7.setTextColor(Color.BLACK);
								 
								 t8.setTextSize(20);
								 t8.setTextColor(Color.BLACK);
								 
								 t9.setTextSize(20);
								 t9.setTextColor(Color.BLACK);
								 
								 t10.setTextSize(20);
								 t10.setTextColor(Color.BLACK);
								 
								 t11.setTextSize(20);
								 t11.setTextColor(Color.BLACK);
								 
								 t12.setTextSize(20);
								 t12.setTextColor(Color.BLACK);
								 
								 t13.setTextSize(20);
								 t13.setTextColor(Color.BLACK);
								 
								 t14.setTextColor(Color.BLACK);
								 t14.setTextSize(20);
								 
								 t15.setTextColor(Color.BLACK);
								 t15.setTextSize(20);
								 
								 t16.setTextColor(Color.BLACK);
								 t16.setTextSize(20);
								 
								 t16.setTextColor(Color.BLACK);
								 t16.setTextSize(20);
								 
								 t16.setTextColor(Color.BLACK);
								 t16.setTextSize(20);
								 
								 t17.setTextColor(Color.BLACK);
								 t17.setTextSize(20);
								 
								 t18.setTextColor(Color.BLACK);
								 t18.setTextSize(20);
								 
								 t19.setTextColor(Color.BLACK);
								 t19.setTextSize(20);
								 
								 t20.setTextColor(Color.BLACK);
								 t20.setTextSize(20);
								 
								 t21.setTextColor(Color.BLACK);
								 t21.setTextSize(20);
								 
								 t22.setTextColor(Color.BLACK);
								 t23.setTextSize(20);
								 
								 t24.setTextColor(Color.BLACK);
								 t24.setTextSize(20);

								 t25.setTextColor(Color.BLACK);
								 t25.setTextSize(20);
								 
								 t26.setTextColor(Color.BLACK);
								 t26.setTextSize(20);
								 
								 t27.setTextColor(Color.BLACK);
								 t27.setTextSize(20);
								 
								 t28.setTextColor(Color.BLACK);
								 t28.setTextSize(20);
								 
								 t29.setTextColor(Color.BLACK);
								 t29.setTextSize(20);
								 
								 t30.setTextColor(Color.BLACK);
								 t30.setTextSize(20);
								 
								 t31.setTextColor(Color.BLACK);
								 t31.setTextSize(20);
								 
								 t32.setTextColor(Color.BLACK);
								 t32.setTextSize(20);
								 
								 t33.setTextColor(Color.BLACK);
								 t33.setTextSize(20);
								 
								 
								 
								 t1.setGravity(Gravity.CENTER_HORIZONTAL);
								 t2.setGravity(Gravity.CENTER_HORIZONTAL);
								 t3.setGravity(Gravity.CENTER_HORIZONTAL);
								 t4.setGravity(Gravity.CENTER_HORIZONTAL);
								 t5.setGravity(Gravity.CENTER_HORIZONTAL);
								 t6.setGravity(Gravity.CENTER_HORIZONTAL);
								 t7.setGravity(Gravity.CENTER_HORIZONTAL);
								 t8.setGravity(Gravity.CENTER_HORIZONTAL);
								 t9.setGravity(Gravity.CENTER_HORIZONTAL);
								 t10.setGravity(Gravity.CENTER_HORIZONTAL);
								 t11.setGravity(Gravity.CENTER_HORIZONTAL);
								 t12.setGravity(Gravity.CENTER_HORIZONTAL);
								 t13.setGravity(Gravity.CENTER_HORIZONTAL);
								 t14.setGravity(Gravity.CENTER_HORIZONTAL);
								 t15.setGravity(Gravity.CENTER_HORIZONTAL);
								 t16.setGravity(Gravity.CENTER_HORIZONTAL);
								 t17.setGravity(Gravity.CENTER_HORIZONTAL);
								 t18.setGravity(Gravity.CENTER_HORIZONTAL);
								 t19.setGravity(Gravity.CENTER_HORIZONTAL);
								 t20.setGravity(Gravity.CENTER_HORIZONTAL);
								 t21.setGravity(Gravity.CENTER_HORIZONTAL);
								 t22.setGravity(Gravity.CENTER_HORIZONTAL);
								 t23.setGravity(Gravity.CENTER_HORIZONTAL);
								 t24.setGravity(Gravity.CENTER_HORIZONTAL);
								 t25.setGravity(Gravity.CENTER_HORIZONTAL);
								 t26.setGravity(Gravity.CENTER_HORIZONTAL);
								 t27.setGravity(Gravity.CENTER_HORIZONTAL);
								 t28.setGravity(Gravity.CENTER_HORIZONTAL);
								 t28.setGravity(Gravity.CENTER_HORIZONTAL);
								 t30.setGravity(Gravity.CENTER_HORIZONTAL);
								 t31.setGravity(Gravity.CENTER_HORIZONTAL);
								 t32.setGravity(Gravity.CENTER_HORIZONTAL);
								 t33.setGravity(Gravity.CENTER_HORIZONTAL);
								 row1.setGravity(Gravity.CENTER);
								 row1.setClickable(true);
								 if(cont%2 == 0){
								 row1.setBackgroundColor(Color.LTGRAY);
								 }else {
								 row1.setBackgroundColor(Color.WHITE);	 
								 }
								 
							 t1.setText(d1);
							 t2.setText(d2);
							 t3.setText(d3);
							 t4.setText(d4);
							 t5.setText(d5);
							 t6.setText(d6);
							 t7.setText(d7);
							 t8.setText(d8);
							 t9.setText(d9);
							 t10.setText(d10);
							 t11.setText(d11);
							 t12.setText(d12);
							 t13.setText(d13);
							 t14.setText(d14);
							 t15.setText(d15);
							 t16.setText(d16);
							 t17.setText(d17);
							 
							 t18.setText(es);
							 t19.setText(es);
							 t20.setText(es);
							 t21.setText(es);
							 t22.setText(es);
							 t23.setText(es);
							 t24.setText(es);
							 t25.setText(es);
							 t26.setText(es);
							 t27.setText(es);
							 t28.setText(es);
							 t29.setText(es);
							 t30.setText(es);
							 t31.setText(es);
							 t32.setText(es);
							 t33.setText(es);
							 
							 row1.addView(t1);
							 row1.addView(t18);
							 
							 row1.addView(t2);
							 row1.addView(t19);
							 
							 row1.addView(t3);
							 row1.addView(t20);
							 
							 row1.addView(t4);
							 row1.addView(t21);
							 
							 row1.addView(t5);
							 row1.addView(t22);
							 
							 row1.addView(t6);
							 row1.addView(t23);
							 
							 row1.addView(t7);
							 row1.addView(t24);
							 
							 row1.addView(t8);
							 row1.addView(t25);
							 
							 row1.addView(t9);
							 row1.addView(t26);
							 
							 row1.addView(t10);
							 row1.addView(t27);
							 
							 row1.addView(t11);
							 row1.addView(t28);
							 
							 row1.addView(t12);
							 row1.addView(t29);
							 
							 row1.addView(t13);
							 row1.addView(t30);
							 
							 row1.addView(t14);
							 row1.addView(t31);
							 
							 row1.addView(t15);
							 row1.addView(t32);
							 
							 row1.addView(t16);
							 row1.addView(t33);
							 
							 row1.addView(t17);

							 
							 tabla.addView(row1);
							 tabla.setClickable(true);
							 
							 }
							 
						}
						
						
						//}
						connection.close();	
					}
					
            	}
				  catch (Exception e) {
					Toast toast = Toast.makeText(getApplicationContext(), "Verificar Conexión.....", Toast.LENGTH_SHORT);
	            	toast.show();
					e.printStackTrace();
                }
            	
               
             }
            }
        });
	}	    
	
	public void descargar(View view){
		switch(view.getId()){
		case R.id.btnDesc:
			if(appState.connectionAvailable() == true){
            	new DescargarDatos().execute();
            	} else {
            		alertDialogMensaje("ERROR DE CONEXION","NO HAY CONEXION WIFI");
            	
            	}
			break;
		}
	}
private class DescargarDatos extends AsyncTask<Void, Integer, Boolean> {   	
    	
        @Override
	        protected void onPreExecute() {
	        	loadingDialogDropbox = ProgressDialog.show(Consulta_RCosecha.this,"", "Descargando Datos.....", true, false);
	        }
    	
        @Override 
        protected Boolean doInBackground(Void... params) {
        	
        	
        	//for(int i =0;i<listadoJson.size();i++){
        			Log.d("descargando ", listadoJson + "");
        		
		        	String nameJson =listadoJson;
		        	File file = new File( Environment.getExternalStorageDirectory()+"/prueba/JsonCarga/" + nameJson);
		        	FileOutputStream outputStream;
		        	String directorioDropbox = DIR_APP + "JsonCarga/" + nameJson;
		        	try {

						outputStream = new FileOutputStream(file);
						//IVAN: esta es la nueva forma de descargar datos de Dropbox
						FileMetadata fileMetadata = dbxClientV2.files().download(directorioDropbox).download(outputStream);
						Log.i("DbExampleLog", "The file's rev is: " + fileMetadata.getRev());
						mHandler.sendEmptyMessage(0);
						
					} catch (DbxException e) {
						e.printStackTrace();
					}  catch (IllegalStateException e) {
						mHandler.sendEmptyMessage(-1);
						e.printStackTrace();
						return false;
					} catch (IOException e) {
						e.printStackTrace();
					}
	        	//}    	
		     return true;
        }
     

        @Override
        protected void onPostExecute(Boolean result) {
        	if (result == true){
        	loadingDialogDropbox.dismiss();
        	//new GuardarDatos().execute();
        	} 
        }
    }

private Handler mHandler = new Handler() {
	public void handleMessage(android.os.Message msg) {
		if(msg.what == -1){ // Error en la Conexión o Descarga
			Toast.makeText(Consulta_RCosecha.this, "ERROR EN LA CONEXION NO SE HA PODIDO DESCARGAR.....", Toast.LENGTH_SHORT).show();
			loadingDialogDropbox.dismiss();
			//startActivity(new Intent(ListadoDescargas.this, ListadoDescargas.class));
		}
		else if(msg.what == 0){ // Descargado con Exito
			Toast.makeText(Consulta_RCosecha.this, "Descargando exitosamente.....", Toast.LENGTH_SHORT).show();
		}
		
	}
	
};	
	public void leer(){
		try
		{
		    File ruta_sd = Environment.getExternalStorageDirectory();
		 
		    File f = new File(ruta_sd.getAbsolutePath(), "/prueba/JsonCarga/"+listadoJson);
		 
		    BufferedReader fin =
		    		new BufferedReader(
		            new InputStreamReader(
		                new FileInputStream(f)));
		 
		    texto = fin.readLine();
		    
		    fin.close();
		}
		catch (Exception ex)
		{
		    Log.e("Ficheros", "Error al leer fichero desde tarjeta SD");
		}
	}
	public void alertDialogMensaje(String message1, String mesage2){
	
		try {
		    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
		    ringtone.play();	    	    
		} catch (Exception e) {
		    e.printStackTrace();
		}
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(message1);
		alertDialog.setMessage(mesage2);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
		public void onClick(final DialogInterface dialog, final int which) {
		// here you can add functions
			ringtone.stop();
		}
		});
		alertDialog.show();
	}
	
	public String comprobar(String cad){
		String Pb = "";
		if(cad.equals(Pb)){
			cad = "---";
		}
		return cad;	
	}
	
	public String format (int f){
		val = "";
		if(f<10){
		val = "0"+Integer.toString(f);	
		}
		else{
		val = Integer.toString(f);
		}
		return val;
	}
	
	public void Calcular () {
		String Siembra = "";
		int Actividad = 0;
		int Muestras = 0;
		Cursor Disponibilidades = db.getDispSemillas("Fecha = '"+ Fecha +"'");
		 DisponibilidadSemillas DS = db.getDispSemillasFromCursor(Disponibilidades, 0);
		 Siembra = DS.getSiembra();
		 Actividad = DS.getActividad();
		 
		 Cursor Dispon2 = db.getDispSemillas("Fecha = '"+ Fecha +"' AND Siembra='" + Siembra + "' AND Actividad = "+ Actividad );
		 if(Dispon2.getCount()>0){
			 for(int i=0 ; i<Dispon2.getCount();i++){
				 
			 }
		 }
		 Dispon2.close();
		 Disponibilidades.close();
		 
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.soporteit, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	 public static int contarCaracteres(String cadena, String palabra) {
	        int posicion, contador = 0;
	        //se busca la primera vez que aparece
	        posicion = cadena.indexOf(palabra);
	        while (posicion != -1) { //mientras se encuentre el caracter
	            contador++;           //se cuenta
	            //se sigue buscando a partir de la posición siguiente a la encontrada
	            posicion = cadena.indexOf(palabra, posicion + 1);
	        }
	        return contador;
	   }

	 public static int contarString(String cadena,String palabra){
		 //String sTexto = "palabra o palabra y palabra";
		    // Texto que vamos a buscar
		   // String sTextoBuscado = "palabra";
		    // Contador de ocurrencias 
		    int contador = 0;

		    while (cadena.indexOf(palabra) > -1) {
		    	cadena = cadena.substring(cadena.indexOf(
		    			palabra)+palabra.length(),cadena.length());
		      contador++; 
		    }
		    return contador;
	 }
}  	

