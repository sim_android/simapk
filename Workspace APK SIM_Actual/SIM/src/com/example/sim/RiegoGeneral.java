package com.example.sim;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.os.Vibrator;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.floricultura.R;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.DUbicaciones;
import com.example.sim.data.Dempleado;
import com.example.sim.data.ERiego;
import com.example.sim.data.JsonFiles;
import com.example.sim.data.Semillas;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@SuppressLint("NewApi") 
public class RiegoGeneral extends ActionBarActivity {
	
	//Variables globales
	private MyApp appState;	
	private DBAdapter db;
	int CodigoModulo;
	int Codigodepartamento;
	int CodigoEmpleado;
	int CodigoOpcion;
	int TotalEmpleadosAsignados;
	int CodigoFamilia = 0;
	boolean validoDescriptivo;
	LinearLayout Encabezado;
	LinearLayout Cuerpo;
	String NombreOpcion;
	TextView TCantidadAsignados;
	Vibrator mVibrator;
	Ringtone ringtone;
	String EmpleadoEscaneado;
	String[] datos = null;
	String[] data = null;
	TextView TextUbicacion;
	EditText empleado;
	String Ubica = "";;
	TextView TextEmpleado;
	int count = 0;
	String pivote = "";
	String dat = "";
	String[] data2 = null;
	String[] seleccionados = null;
	String[] ubicaciones = null;
	String[] descripciones = null;
	ArrayList<String> seletedItems = new ArrayList<String>();
	ArrayList<String> descrip = new ArrayList<String>();
	String Ubicacion;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.riego_general);
	
		//se obtienen lo datos de la actividad antetior por medio del intent
		Bundle extras = getIntent().getExtras();
		CodigoModulo = extras.getInt("CodigoModulo");
	    CodigoEmpleado = extras.getInt("CodigoEmpleado");
	    Codigodepartamento = extras.getInt("Codigodepartamento");
	    CodigoOpcion = extras.getInt("CodigoOpcion");
	    NombreOpcion = extras.getString("NombreOpcion");
	    CodigoFamilia = extras.getInt("CodigoFamilia");
	   
	    //hacemos referencia a todos los objetos de nuestra vista
	    TextView TNombreCaptura = (TextView)findViewById(R.id.OpcionCaptura);
	    TextView TCodigoEmpleado= (TextView)findViewById(R.id.CodigoEmpleado);
	    TextView TCodigoOpcion= (TextView)findViewById(R.id.codigoOpcion);
	    TextUbicacion = (TextView)findViewById(R.id.textUbicacion);
	    empleado = (EditText)findViewById(R.id.editEmpleado);
	    
	    TCantidadAsignados = (TextView)findViewById(R.id.textEmpleadosAsginados);
	    final Button botonNuevo = (Button) findViewById(R.id.btnNuevo);
        Button btnBack = (Button) findViewById(R.id.btnBack);
	    final Button botonUbicacion =  (Button) findViewById(R.id.botonUbicacion);
	    Button botonListado =  (Button) findViewById(R.id.btnListado);
	    Button botonEmpleado = (Button) findViewById(R.id.botonEmpleado);
	    Button botonGuardar = (Button) findViewById(R.id.buttonGuarda);
	    TextEmpleado = (TextView) findViewById(R.id.textEmpleado);
	    Encabezado = (LinearLayout)findViewById(R.id.Encabezado);
	    Cuerpo = (LinearLayout)findViewById(R.id.Cuerpo);
	       
	    //hacemos las inicializaciones necesarias a nuestra vista
	    TNombreCaptura.setText(NombreOpcion+ "");
	    TCodigoEmpleado.setText(CodigoEmpleado + "");
	    TCodigoOpcion.setText(CodigoOpcion + "");
	    Calendar c = Calendar.getInstance(); 
		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
		String fecha = fdate.format(c.getTime());
		empleado.setInputType(InputType.TYPE_CLASS_NUMBER);   //Para Asignarle un tipo de dato 
		empleado.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
		empleado.setSingleLine(false);   // se utiliza para que el EditText no haga un Tab Automatico.
	    if (android.os.Build.VERSION.SDK_INT > 9)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
	  
	  //incializa la BD, tambien obtenemos la referencia al singleton
	    appState = ((MyApp)getApplicationContext());   
	    db = appState.getDb();
	    db.open();
	    getAsignados();
	    
	    // Comandos al Presionar el Boton Buscar Ubicacion
	    botonUbicacion.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {         
            	count = 0;
            	descripciones = new String[10];
            	descrip = new ArrayList<String>();
            	seleccionados = new String[10];
            	seletedItems = new ArrayList<String>();
            	dat = "";
            	pivote = "";
            	getDatos();
            	//Creando Cuadro de Dialogo para Listado de Personas
        		class DialogoSeleccion extends DialogFragment {
        	        @Override
        	        public Dialog onCreateDialog(Bundle savedInstanceState) {
        	         	  
        	            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        	            
        	            builder.setTitle("Ubicacion").setMultiChoiceItems(datos, null, new DialogInterface.OnMultiChoiceClickListener() {
        	            	public void onClick(DialogInterface dialog, int item, boolean isChecked) {         						
                 					Log.i("Dialogos", "Opci�n elegida: " + datos[item]);
                 					String Codigo = "";
                 					String Descripcion = "";
                 					int inicio = 0;
                 					inicio = datos[item].indexOf(" ");
             						Codigo = datos[item].substring(0, inicio);
             						Descripcion = datos[item].substring(inicio + 1);
                 					if (isChecked) {
                 						seletedItems.add(Codigo);
                 						descrip.add(Descripcion);
                 			        } else if (seletedItems.contains(Codigo)) {
                 			        	seletedItems.remove(Codigo);
                 			        	descrip.remove(Descripcion);
                 			        }                	                					
           						 }
    							}).setPositiveButton("Aceptar", new DialogInterface.OnClickListener()  {
    								public void onClick(DialogInterface dialog,int item) {
    									Log.i("Dialogos", "Confirmacion Aceptada.");
    									Log.i("Dialogos", "Confirmacion " + dat );
    									seleccionados = seletedItems.toArray(new String[seletedItems.size()]);
    									descripciones = descrip.toArray(new String[descrip.size()]);
    									count = seleccionados.length;
    									int z = 0;
    									for(int i=0; i<count; i++){
    										dat = descripciones[z];
    										pivote = pivote + ", "+ dat;
    										dat = pivote;
    										z++;
    									}
    									TextUbicacion.setText(dat);
                      					dialog.dismiss();
    								}
           
            	                });
        	            return builder.create(); }
        	    }
        	               	 
        		FragmentManager fragmentManager = getSupportFragmentManager();
        		DialogoSeleccion dialogo = new DialogoSeleccion();
                dialogo.show(fragmentManager, "Alerta");
            }
            
        });
	    
	    botonEmpleado.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	getEmpleados();
            	empleado.setEnabled(true);
            	empleado.setText("");
            	TextEmpleado.setText("");
            	//Creando Cuadro de Dialogo para Listado de Personas
        		class DialogoSeleccion extends DialogFragment {
        	        @Override
        	        public Dialog onCreateDialog(Bundle savedInstanceState) {
        	         	  
        	            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        	    
						builder.setTitle("Empleados").setItems(datos, new DialogInterface.OnClickListener() {
        	                    public void onClick(DialogInterface dialog, int item) {
        	                        Log.i("Dialogos", "Opci�n elegida: " + datos[item]);
        	                        int inicio = datos[item].indexOf(" ");
        	                        TextEmpleado.setText(datos[item].substring(inicio +2));
        	                        empleado.setEnabled(false);
        	                        empleado.setText(datos[item].substring(0,inicio));
        	                        Log.i("Dialogos", "Codigo Empleado: " + datos[item].substring(0,inicio));
        	                    }
        	                });
        	            return builder.create(); }
        	    }
        		
        		FragmentManager fragmentManager = getSupportFragmentManager();
        		DialogoSeleccion dialogo = new DialogoSeleccion();
                dialogo.show(fragmentManager, "Alerta");
            }
            
        });
	    
	    // Comandos al Presionar el Boton Mostrar Listado
	    botonListado.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	getListado();
            	//Creando Cuadro de Dialogo para Listado de Personas
        		class DialogoSeleccion extends DialogFragment {
        	        @Override
        	        public Dialog onCreateDialog(Bundle savedInstanceState) {
        	         	  
        	            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        	    
						builder.setTitle("Listado").setItems(data, new DialogInterface.OnClickListener() {
        	                    public void onClick(DialogInterface dialog, int item) {
        	                        Log.i("Dialogos", "Opci�n elegida: " + data[item]);        	                 
        	                    }
        	                });
        	            return builder.create(); }
        	    }
        		
        		FragmentManager fragmentManager = getSupportFragmentManager();
        		DialogoSeleccion dialogo = new DialogoSeleccion();
                dialogo.show(fragmentManager, "Alerta");
            }
            
        });
	        
	    // Comandos al Presionar el Boton Atras
	    btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
 
            	onBackPressed();
            }
        
        });
	
	    // Comandos al Presionar el Boton Nuevo
	    botonNuevo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	TextUbicacion.setText("");
            	TextEmpleado.setText("");       
            	empleado.setText("");
            	Ubica = "";
            	count = 0;
            	descripciones = new String[6];
            	descrip = new ArrayList<String>();
            	seleccionados = new String[6];
            	seletedItems = new ArrayList<String>();
            	empleado.requestFocus();
            	empleado.setEnabled(true);
            }
        
        });
	    
	    // Comando para el boton Guardar
	    botonGuardar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	String Valida = "";
            	String ubi = TextUbicacion.getText().toString().trim();
            	EmpleadoEscaneado = empleado.getText().toString().trim();
            	if(count == 0 || ubi.equals("")){
            		alertDialogMensaje("ERROR","ERROR DEBE SELECCIONAR AL MENOS UNA UBICACION");
            		TextUbicacion.setText("");
                	empleado.setText("");
                	empleado.requestFocus();
                	count = 0;
                	seleccionados = new String[6];
                	seletedItems = new ArrayList<String>();
                	dat = "";
                	pivote = "";
            	}
            	else if(EmpleadoEscaneado.equals(Valida))
            	{
            		alertDialogMensaje("ERROR"," DEBE SELECCIONAR UN EMPLEADO");
            	}
            	else 
            	{
            		GuardarDatos();
            	}
            }
        });
	    
	    //Se verfica el input empleado al persionar enter
        empleado.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	// se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                	String Valida = "";
                	String ubi = TextUbicacion.getText().toString().trim();
                	EmpleadoEscaneado = empleado.getText().toString().trim();
                	if(ubi.equals(Valida))
                	{
                		alertDialogMensaje("ERROR"," DEBE SELECCIONAR LA UBICACION");
                	} 
                	else if(EmpleadoEscaneado.equals(Valida))
                	{
                		alertDialogMensaje("ERROR"," DEBE SELECCIONAR UN EMPLEADO");
                	}
                	else 
                	{
                		GuardarDatos();
                	}
                }
				return false;
                }	
                }) ;
   	       
	}
	
	public void alertDialogMensaje(String message1, String mesage2){
		
		//mVibrator.vibrate(300);
		
		try {
		    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
		    ringtone.play();
		    
		    
		} catch (Exception e) {
		    e.printStackTrace();
		}
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(message1);
		alertDialog.setMessage(mesage2);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int which) {
		// here you can add functions
			ringtone.stop();
		}
		});
		alertDialog.show();
	}
		
	public void VerificarDatos(String dato){	
		 Cursor CursorSemillas = db.getSemillas("Siembra='" + dato + "'");
		 if(CursorSemillas.getCount()>0){
			 Semillas s = db.getSemillasFromCursor(CursorSemillas, 0);
			 Log.d("SEMILLA",s.getSiembra() + ", "+s.getMaterial() + " ," + s.getPolinizacion());
			 validoDescriptivo = true;
			 } else {
				 validoDescriptivo = false;
			 }
		 CursorSemillas.close();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.captura_disponibilidad, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void getDatos(){
		 Cursor CUbicaciones = null ;
		 CUbicaciones = this.db.getDUbicaciones("");
		 String tmp = null;
		 int j = 0;
		 datos = new String[CUbicaciones.getCount()];
		 if(CUbicaciones.getCount()>0){
			 for(int i=0 ; i<CUbicaciones.getCount();i++){	 
				 DUbicaciones ub = db.getDUbicacionesFromCursor(CUbicaciones, i);
				 tmp = ub.getCodigo().toString().trim() + " " + ub.getDescripcion().toString().trim();
				datos[j] = tmp;
				j++;
			 	} 
		 	}
		 CUbicaciones.close();
		}
	
	public void getEmpleados(){
		 String Encargado = "7";
		 Cursor Emp = this.db.getDempledo("Responsable = '" + Encargado + "'" );
		 String tmp = null;
		 int j = 0;
		 datos = new String[Emp.getCount()];
		 if(Emp.getCount()>0){
			 for(int i=0 ; i<Emp.getCount();i++){	 
				 Dempleado in = db.getDempleadoFromCursor(Emp, i);
					 tmp = "0" + in.getCodigoEmpleado() + "  " + in.getNombre();
				datos[j] = tmp;
				j++;
			 	} 
		 	}
		 Emp.close();
		}
	
	public void getListado(){
		int sup = CodigoEmpleado;
		int Opc =  CodigoOpcion;
		Cursor Casignados = null ;
		 Casignados = this.db.getRiego("Supervisor=" + sup + " AND Actividad=" + Opc +" AND ActividadFinalizada=0");
		 String tmp = null;
		 int j = 0;
		 data2 = new String[Casignados.getCount()];
		 if(Casignados.getCount()>0){
			 for(int i=0 ; i<Casignados.getCount();i++){	 
				 ERiego r = db.getRiegoFromCursor(Casignados, i);
				 Cursor Codigoempleado = this.db.getDempledo("CodigoEmpleado=" + r.getCodigoEmpleado().toString().trim());
				 Dempleado DE = this.db.getDempleadoFromCursor(Codigoempleado, 0);
				 if(DE != null){
					 tmp =  DE.getNombre().toString().trim() +"         "+ r.getHoraIncio().toString().trim();
				 }else{
					 tmp =  " NOMBRE NO ENCONTRADO " +"         "+ r.getHoraIncio().toString().trim();
				 }
				 Log.i("Dialogos", "Opci�n elegida: " + tmp +" - " + data2[j] + "- " + j );
				 if(j != 0){
					 if(!tmp.equals(data2[j]) && !tmp.equals(data2[j-1]) ){
						 Log.i("Dialogos", "Opci�n  " + tmp +" - " + data2[j] + " - "+ j );
						 data2[j] = tmp;
						 j++;
				 	} 
				 }else{
					 data2[j] = tmp;
					 j++;
				 }
				 Codigoempleado.close();
			 	} 
		 	}
		 int z = 0;
		 data = new String[j];
		 for(int i=0; i<j; i++){
			 data[z] = data2[z];
			 z++;
		 }
		 Casignados.close();
		}
	
	public void GuardarDatos(){	
    	int existe = VerificarEmpleado(EmpleadoEscaneado);
    	if(existe == 2){
    			int z = 0;
    			
    			//-----------------------------------------------------------------------------------
    			String correlativo="";
    			    	//almaceno el empleado en una variable string
    			    	String getEmpleado = empleado.getText().toString().trim();
    			    	// guardo la variable que tiene empleado en un array
    			    	String[]empl = new String[]{getEmpleado};
    			    	Cursor obtenerCorrelativo = db.getCorrelativo(empl);
    			    	//recorro dicho arreglo para obtener el campo
    			    	if (obtenerCorrelativo.moveToFirst()){
    			    		do{
    			    			correlativo = obtenerCorrelativo.getString(0);
    			    		}while(obtenerCorrelativo.moveToNext());
    			    	}
    			//-------------------------------------------------------------------------
    			    	
    			Calendar c = Calendar.getInstance(); 
        		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
        		String fecha = fdate.format(c.getTime());
        		SimpleDateFormat ftime = new SimpleDateFormat("HH:mm:ss");
        		String horaInicio = ftime.format(c.getTime());
    			for(int i=0; i<count; i++){
    			Ubicacion = seleccionados[z];
    			ERiego dtemp = new ERiego();
        		dtemp.setCodigoEmpleado(Integer.parseInt(EmpleadoEscaneado));
        		dtemp.setCodigoDepto(Codigodepartamento);
        		dtemp.setUbicacion(Ubicacion);
        		dtemp.setFecha(fecha);
        		dtemp.setSiembra("0");
        		dtemp.setHoraIncio(horaInicio);
        		dtemp.setActividad(CodigoOpcion);
        		dtemp.setSupervisor(CodigoEmpleado);
        		dtemp.setActividadFinalizada(0);
        		dtemp.setCorrelativoEmpleado(correlativo);
        		dtemp.setFP("N");
       
        		Log.d("RiegoSem", dtemp.toString().trim());    		
        		db.insertarRiego(dtemp);
        		
        		//generacion archivo json
        		ObjectMapper mapper = new ObjectMapper();
        		try {
        			String nameJson ="RF_" + dtemp.getCodigoEmpleado() + "_"+dtemp.getFecha() + "_"+dtemp.getHoraIncio() + " "+ z;
        			nameJson=nameJson.replace(":", "-");	
					mapper.writeValue(new File( Environment.getExternalStorageDirectory()+"/SIM/JsonDescarga/" + nameJson+ ".json"), dtemp);
					
					JsonFiles archivo = new JsonFiles();
					archivo.setName(nameJson+ ".json");
					archivo.setNameFolder("SinFinalizar/");
					archivo.setUpload(0);
					db.insertarJsonFile(archivo);
					
					if(!appState.getSubiendoArchivos()){
						appState.setSubiendoArchivos(true);
						new UptoDropbox().execute(getApplicationContext());
					}
					
        		
        		} catch (JsonGenerationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JsonMappingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        	    z++;
    			}
    			
    			//count = 0;
    			//seletedItems = new ArrayList<String>();
            	//seleccionados = new String[6];
            	//dat = "";
            	//pivote = "";
        		empleado.setText("");
        		//TextUbicacion.setText("");
        		empleado.requestFocus();
        		TotalEmpleadosAsignados++;
        		TCantidadAsignados.setText(TotalEmpleadosAsignados + "");
        		    			
        	}else if(existe == 1){
        		alertDialogMensaje("Asignado", "Este Empleado ya esta asignado");
        		empleado.setText("");
        		empleado.requestFocus();
        	}else if(existe == 0){
        		alertDialogMensaje("Marcaje", "Empleado no encontrado \n Descargar D_Acceso");
        		empleado.setText("");
        		empleado.requestFocus();	
    	}
	}
	
		//retornara un valor dependiendo del estado del empleado
		// 0 = no existe
		// 1 = existe pero esta asignado
		// 2 = existe y no esta asignado
	public int VerificarEmpleado(String empleado){
		int valor = 0;
		Cursor CursorDacceso =	db.getDacceso("CodigoEmpleado='" + empleado + "'");
		if(CursorDacceso != null  && CursorDacceso.getCount()>0){
		Cursor CRiego =	db.getRiego("CodigoEmpleado=" + empleado + " AND ActividadFinalizada=0");
		if(CRiego.getCount()>0){
			valor = 1;
			}else{
			valor = 2;
			}
		 CRiego.close();
		 }
		CursorDacceso.close();
		  return valor;
		}
	
	public void getAsignados(){
		int sup = CodigoEmpleado;
		int Opc =  CodigoOpcion;
		Cursor Casignados = null ;
		 Casignados = this.db.getRiego("Supervisor=" + sup + " AND Actividad=" + Opc +" AND ActividadFinalizada=0");
		 int j = Casignados.getCount();
		TotalEmpleadosAsignados = j;
 		TCantidadAsignados.setText(TotalEmpleadosAsignados + "");
 		Casignados.close();
		}
	
	public String format (int f){
		String val = "";
		if(f<10){
		val = "0"+Integer.toString(f);	
		}
		else{
		val = Integer.toString(f);
		}
		return val;
	}
	
}
