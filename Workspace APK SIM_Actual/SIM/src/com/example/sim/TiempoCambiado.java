package com.example.sim;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.R.string;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.floricultura.R;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.Dguardados;
import com.example.sim.data.JsonFiles;
import com.example.sim.data.Opciones;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TiempoCambiado extends ActionBarActivity {
	
	private MyApp appState;	
	private DBAdapter db;
	int CodigoModulo;
	int Codigodepartamento;
	int CodigoEmpleado;
	int CodigoOpcion;
	String NombreOpcion;
	String Emp;
	EditText empleado;
	int finalizar;
	Ringtone ringtone;
	EditText EditBancas;
	EditText EditPlantas;
	Button btnMostrar;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tiempo_cambiado);
		
		Bundle extras = getIntent().getExtras();
		CodigoModulo = extras.getInt("CodigoModulo");
	    CodigoEmpleado = extras.getInt("CodigoEmpleado");
	    Codigodepartamento = extras.getInt("Codigodepartamento");
	    CodigoOpcion = extras.getInt("CodigoOpcion");
	    NombreOpcion = extras.getString("NombreOpcion");    	    
	    TextView TNombreCaptura = (TextView)findViewById(R.id.OpcionCaptura);
	    TextView TCodigoEmpleado= (TextView)findViewById(R.id.CodigoEmpleado);
	    TextView TCodigoOpcion= (TextView)findViewById(R.id.codigoOpcion);
	    empleado  = (EditText)findViewById(R.id.editEmpleado);
	    Button btnBack = (Button) findViewById(R.id.btnBack);
	    Button botonGuarda = (Button) findViewById(R.id.buttonGuarda);
	    Button botonNuevo =  (Button) findViewById(R.id.btnNuevo);
	    TextView TxtBancas = (TextView)findViewById(R.id.textViewBancas);
	    TextView TxtFlores= (TextView)findViewById(R.id.textFlores);
	    TextView TextPlantas= (TextView)findViewById(R.id.textPlantas);
	    EditBancas = (EditText)findViewById(R.id.editBancas);
	    EditPlantas = (EditText)findViewById(R.id.editPlantas);   
	    TxtBancas.setVisibility(View.VISIBLE);
	    TextPlantas.setVisibility(View.VISIBLE);
	    EditBancas.requestFocus();      	    
	    TNombreCaptura.setText(NombreOpcion+ "");
	    TCodigoEmpleado.setText(CodigoEmpleado + "");
	    TCodigoOpcion.setText(CodigoOpcion + "");
	    EditBancas.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
	    EditBancas.setInputType(InputType.TYPE_CLASS_NUMBER |InputType.TYPE_NUMBER_FLAG_DECIMAL);
	    EditBancas.setSingleLine(false);
	    EditPlantas.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
	    EditPlantas.setInputType(InputType.TYPE_CLASS_NUMBER |InputType.TYPE_NUMBER_FLAG_DECIMAL);
	    EditPlantas.setSingleLine(false);
	    empleado.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
	    empleado.setInputType(InputType.TYPE_CLASS_NUMBER |InputType.TYPE_NUMBER_FLAG_DECIMAL);
	    empleado.setSingleLine(false);
	    
	    


	    
	    appState = ((MyApp)getApplicationContext());   
	    db = appState.getDb();
	    db.open();
	    
	    Cursor CursorOpciones= null ;
	    CursorOpciones = db.getOpciones("Opcion=" + CodigoOpcion);
	    
	    if(CursorOpciones.getCount()>0){
	    	Opciones op = db.getOpcionesFromCursor(CursorOpciones, 0);
	    	finalizar = op.getFinalizaActividad();
	    }
	    CursorOpciones.close();
	    	
		    //Se verfica el input azucarera al persionar enter
		empleado.setOnKeyListener(new View.OnKeyListener() {
	            public boolean onKey(View v, int keyCode, KeyEvent event) {
	                // If the event is a key-down event on the "enter" button
	                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
	                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
	                	Emp = empleado.getText().toString().trim();
	                	if (Emp.length() < 8)
	                	{
	                		alertDialogMensaje("Error", "ERROR EN EL EMPLEADO");
	                		empleado.setText("");
	                		empleado.requestFocus();
	                	}
	                	else
	                    {
	                	int valor = VerificarEmpleado(Emp);	
	                	if(valor == 0) {
	                		alertDialogMensaje("Error", "ERROR EMPLEADO NO EXISTE");
	                		empleado.setText("");
	                		empleado.requestFocus();
	                	} else if (valor == 1) {
	                		alertDialogMensaje("Error", "ERROR EMPLEADO YA ASIGNADO");
	                		empleado.setText("");
	                		empleado.requestFocus(); }	
	                    else
	                	{}	
		                return true;
	                    }
	                }
	                return false;
	            }
	        });
	    	    	  
	    //Se verfica el input azucarera al persionar enter
        EditBancas.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	// se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                		EditPlantas.requestFocus();
	                return true;
                }
                return false;
            }
        });
        
        //Se verfica el input azucarera al persionar enter
        EditPlantas.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	// se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                	empleado.requestFocus();	
                	//EditFlores.requestFocus();
	                return true;
                }
                return false;
            }
        });
        
	    btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	onBackPressed();
            }
        });
   
        botonGuarda.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Emp = empleado.getText().toString().trim();
            	String hr = EditBancas.getText().toString().trim();
            	String Min = EditPlantas.getText().toString().trim();
            	if (Emp.length() < 8)
            	{
            		alertDialogMensaje("Error", "ERROR EN EL EMPLEADO");
            		empleado.setText("");
            		empleado.requestFocus();
            	}
            	else
                {
            	int valor = VerificarEmpleado(Emp);	
            	if(valor == 0) {
            		alertDialogMensaje("Error", "ERROR EMPLEADO NO EXISTE");
            		empleado.setText("");
            		empleado.requestFocus();
            	} else if (valor == 1) {
            		alertDialogMensaje("Error", "ERROR EMPLEADO YA ASIGNADO");
            		empleado.setText("");
            		empleado.requestFocus(); }	
                else
            	{
                 if(hr.equals("") && Min.equals("")){
                	 alertDialogMensaje("ERROR","DEBE INGRESAR HORAS O MINUTOS!!!");
                	 EditBancas.setText("");
                	 EditPlantas.setText("");
                	 empleado.setText("");
                	 
                 }
                 else{
            		GuardarDatos();
                  }	
                }
            }
            }
        });
      
	      	
	}
	
	//retornara un valor dependiendo del estado del empleado
		// 0 = no existe
		// 1 = existe pero esta asignado
		// 2 = existe y no esta asignado	
	public int VerificarEmpleado(String empleado){
			int valor = 0;
			Cursor CursorDacceso =	db.getDacceso("CodigoEmpleado='" + empleado + "'");
			if(CursorDacceso != null  && CursorDacceso.getCount()>0){
				Cursor CursorGuardados =	db.getDguardados("CodigoEmpleado=" + empleado + " AND ActividadFinalizada=0");
				if(CursorGuardados != null){
				if(CursorGuardados.getCount()>0){
					valor = 1;
				}else{
					valor = 2;
				}
				}else{
					alertDialogMensaje("ERROR","ERROR EMPLEADO NO ENCONTRADO");
				}
				CursorGuardados.close();
			}
			CursorDacceso.close();
			return valor;
		}
	
	public void hideSoftKeyboard() {
	    if(getCurrentFocus()!=null) {
	        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
	        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),   InputMethodManager.HIDE_NOT_ALWAYS);
	    }
	}
	
	public void GuardarDatos()
	{
		
    	Dguardados dtemp = new Dguardados();
    	dtemp.setCodigoEmpleado(Integer.parseInt(empleado.getText().toString().trim()));
    	String correlativo="";
    	//almaceno el empleado en una variable string
    	String getEmpleado = empleado.getText().toString().trim();
    	// guardo la variable que tiene empleado en un array
    	String[]empl = new String[]{getEmpleado};
    	Cursor obtenerCorrelativo = db.getCorrelativo(empl);
    	//recorro dicho arreglo para obtener el campo
    	if (obtenerCorrelativo.moveToFirst()){
    		do{
    			correlativo = obtenerCorrelativo.getString(0);
    		}while(obtenerCorrelativo.moveToNext());
    	}
    			
        		Calendar c = Calendar.getInstance(); 
        		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
        		String fecha = fdate.format(c.getTime());
        		SimpleDateFormat ftime = new SimpleDateFormat("HH:mm:ss");
        		String horaFin = ftime.format(c.getTime());
        		             
        		
        		dtemp.setFecha(fecha);
    			dtemp.setHoraFin(horaFin);
    			dtemp.setActividadFinalizada(0); // se cambio a Actividad finalizada '0'  porque quitaba los empleados del listado  13/03/2015
    			dtemp.setSupervisor(CodigoEmpleado);
    			dtemp.setActividad(CodigoOpcion);
    			dtemp.setCorrelativoEmpleado(correlativo);
    			
    			if(EditBancas.getText().toString().trim().length()>0)
    				dtemp.setBancas(EditBancas.getText().toString().trim());
    			
    			if(EditPlantas.getText().toString().trim().length()>0)
    				dtemp.setPlantas(EditPlantas.getText().toString().trim());
    			
    			//db.updateDguardados(dtemp);
    			
        		ObjectMapper mapper = new ObjectMapper();
        		try {
        			String nameJson = "TC_" +dtemp.getCodigoEmpleado() + "_"+dtemp.getFecha() + "_"+dtemp.getHoraFin();
        			nameJson=nameJson.replace(":", "-");
					mapper.writeValue(new File( Environment.getExternalStorageDirectory()+"/SIM/JsonDescarga/" + nameJson+ ".json"), dtemp);
					
					JsonFiles archivo = new JsonFiles();
					archivo.setName(nameJson+ ".json");
					archivo.setNameFolder("Finalizados/");
					archivo.setUpload(0);
					db.insertarJsonFile(archivo);
					
					if(!appState.getSubiendoArchivos()){
						appState.setSubiendoArchivos(true);
						new UptoDropbox().execute(getApplicationContext());
					}
										
				} catch (JsonGenerationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JsonMappingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}                			
        		empleado.setText("");
        		EditBancas.setText("");
        		EditPlantas.setText("");
        		EditBancas.requestFocus();
	}
	
	public void alertDialogMensaje(String message1, String mesage2){
		
		
		try {
		    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
		    ringtone.play();
		    
		    
		} catch (Exception e) {
		    e.printStackTrace();
		}
		
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(message1);
		alertDialog.setMessage(mesage2);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int which) {
		// here you can add functions
			ringtone.stop();
		}
		});
		alertDialog.show();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.finalizar_actividad, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void ButtonOnClick(View v) {
	    switch (v.getId()) {
	      case R.id.buttonborra:
		        agregarNumero("1");
	        break;
	      case R.id.btnDos:
	    	  agregarNumero("2");
	        break;
	      case R.id.btnTres:
		        agregarNumero("3");
	        break;
	      case R.id.btnCuatro:
	    	  agregarNumero("4");
	        break;
	      case R.id.btnCinco:
		        agregarNumero("5");
	        break;
	      case R.id.btnSeis:
	    	  agregarNumero("6");
	        break;
	      case R.id.btnSiete:
	    	  agregarNumero("7");
	        break;
	      case R.id.btnOcho:
		        agregarNumero("8");
	        break;
	      case R.id.btnNueve:
	    	  agregarNumero("9");
	        break;
	      case R.id.button0:
		        agregarNumero("0");
	        break;
	      case R.id.btnCero:
	    	  agregarNumero("b");
	        break;
	      }
	     
	}
		
	public void agregarNumero(String n){
		
		if(n.contains("b")){
			
			if(EditBancas.isFocused()){
				EditBancas.setText("");
			}else if(EditPlantas.isFocused()){
				EditPlantas.setText("");
			}else if(empleado.isFocused()){
				empleado.setText("");
			}
			
		}else{
			if(EditBancas.isFocused()){
				EditBancas.setText(EditBancas.getText() + n);
			}else if(EditPlantas.isFocused()){
				EditPlantas.setText(EditPlantas.getText() + n);
			}else if(empleado.isFocused()){
				empleado.setText(empleado.getText() + n);
			}
		}
		

	}
	



}