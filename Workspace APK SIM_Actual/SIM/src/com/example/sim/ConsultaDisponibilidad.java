package com.example.sim;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Color;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.Vibrator;
import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.floricultura.R;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.DisponibilidadSemillas;

public class ConsultaDisponibilidad extends ActionBarActivity {
	//Variables globales
	private MyApp appState;	
	private DBAdapter db;
	int CodigoModulo;
	int Codigodepartamento;
	int CodigoEmpleado;
	int CodigoOpcion;
	String Val;
	String NombreOpcion;
	Vibrator mVibrator;
	Ringtone ringtone;
	String Fecha;	
	TableLayout TablaEnc;
	private String val;
	String user ="";
	String password = "";
	String URL = "";
	CheckBox CheckLocal;
	
	@SuppressLint("NewApi") @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.consulta_disponibilidad);
		
		// para manejar la excepcion de permisos en el API de Android
    	StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

		//se obtienen lo datos de la actividad antetior por medio del intent
		Bundle extras = getIntent().getExtras();
		CodigoModulo = extras.getInt("CodigoModulo");
	    CodigoEmpleado = extras.getInt("CodigoEmpleado");
	    Codigodepartamento = extras.getInt("Codigodepartamento");
	    CodigoOpcion = extras.getInt("CodigoOpcion");
	    NombreOpcion = extras.getString("NombreOpcion");
	    
	    //hacemos referencia a todos los objetos de nuestra vista
	    TextView TNombreCaptura = (TextView)findViewById(R.id.OpcionCaptura);
	    TextView TCodigoEmpleado= (TextView)findViewById(R.id.CodigoEmpleado);
	    TextView TCodigoOpcion= (TextView)findViewById(R.id.codigoOpcion);
        Button btnBack = (Button) findViewById(R.id.btnBack);
        Button btnOK = (Button) findViewById(R.id.btnAsistencia);
        Button btnfechade = (Button) findViewById(R.id.buttonfechade);
        CheckLocal = (CheckBox) findViewById(R.id.CheckLocal);
        final EditText editFechade = (EditText) findViewById(R.id.editFechaDe);
        editFechade.setEnabled(false);
        final TableLayout TablaEnc = (TableLayout) findViewById(R.id.TablaEnc);     
        
	    //hacemos las inicializaciones necesarias a nuestra vista
	    TNombreCaptura.setText(NombreOpcion+ "");
	    TCodigoEmpleado.setText(CodigoEmpleado + "");
	    TCodigoOpcion.setText(CodigoOpcion + "");
	  
	    
	    //Comentario.requestFocus();

	    //incializamos la BD asi como tambien obtenemos la referencia al singleton
	    appState = ((MyApp)getApplicationContext());   
	    db = appState.getDb();
	    db.open();

	    btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
 
            	onBackPressed();
            }
        });
	    
	    btnfechade.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Calendar mcurrentDate=Calendar.getInstance();
                int mYear = mcurrentDate.get(Calendar.YEAR);
                int mMonth = mcurrentDate.get(Calendar.MONTH);
                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker = new DatePickerDialog(ConsultaDisponibilidad.this, new OnDateSetListener() {                  
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                    	int datodia = selectedday;
                    	String dia = format(datodia);
                    	int datomes = selectedmonth + 1;
                    	String mes = format(datomes);
                    	String datoanio = Integer.toString(selectedyear);
                    	// se formatea la fecha a conveniencia en este caso YYY - MM - DD  pudiera ser YYYY '/' '-' etc...
                    	String dato = datoanio + "-" + mes + "-" +dia;
                    	editFechade.setText(dato);
                    }
                }
                ,mYear, mMonth, mDay);
                mDatePicker.setTitle("Seleccionar Fecha");                
                mDatePicker.show();  
            }
            
        });
	    	    
	    btnOK.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	String Pb = "";
            	String es = " | ";
            	Fecha = editFechade.getText().toString().trim();
            	TablaEnc.removeAllViews();
            	
            	
            	if(Fecha.equals(Pb)){
            		alertDialogMensaje("ERROR","PORFAVOR SELECCIONE UNA FECHA");
            		editFechade.setText("");
            		editFechade.requestFocus();
            	}else{
            	// Cuando el CheckBox LOCAL esta seleccionado
            	if(CheckLocal.isChecked() == true)
            	{
            		   //Cuando se hace una consulta con LOCAL
            		
                	try {
                		
                		//Cuando se hace una consulta con LOCAL
                	
                		appState = ((MyApp)getApplicationContext());
         			    db = appState.getDb();
         			    db.open();
         			     String fecha = editFechade.getText().toString().trim();
                		
                		 String[]datos = new String[]{fecha};
     					 Cursor s = db.getDatos(datos);
     					 
     					 int datosBd = s.getCount();
     					 if(datosBd==0){
     						alertDialogMensaje("Mensaje","No Existen Datos con esta Fecha: "+Fecha);
     					 }
     					 else{
     						//Agregar encabezados de columnas	
     						 TableRow Erow1 = new TableRow(ConsultaDisponibilidad.this);
     						 TextView E1 = new TextView(ConsultaDisponibilidad.this);
     						 TextView E2 = new TextView(ConsultaDisponibilidad.this);
     						 TextView E3 = new TextView(ConsultaDisponibilidad.this);
     						 TextView E4 = new TextView(ConsultaDisponibilidad.this);
     						 TextView E5 = new TextView(ConsultaDisponibilidad.this);
     						 TextView E6 = new TextView(ConsultaDisponibilidad.this);
     						 TextView E7 = new TextView(ConsultaDisponibilidad.this);
     						 TextView E8 = new TextView(ConsultaDisponibilidad.this); 
     						 TextView E9 = new TextView(ConsultaDisponibilidad.this); 
     						 TextView E10 = new TextView(ConsultaDisponibilidad.this); 
     						 TextView E11 = new TextView(ConsultaDisponibilidad.this); 
     						 TextView E12 = new TextView(ConsultaDisponibilidad.this); 
     						 TextView E13 = new TextView(ConsultaDisponibilidad.this);
     						 E1.setTextSize(15);
     						 E1.setTextColor(Color.BLACK);
     						 E2.setTextSize(15);
     						 E2.setTextColor(Color.BLACK);
     						 E3.setTextSize(15);
     						 E3.setTextColor(Color.BLACK);
     						 E4.setTextSize(15);
     						 E4.setTextColor(Color.BLACK);
     						 E5.setTextSize(15);
     						 E5.setTextColor(Color.BLACK);
     						 E6.setTextSize(15);
     						 E6.setTextColor(Color.BLACK);
     						 E7.setTextSize(15);
     						 E7.setTextColor(Color.BLACK);
     						 E8.setTextSize(20);
     						 E8.setTextColor(Color.BLACK);
     						 E9.setTextSize(20);
     						 E9.setTextColor(Color.BLACK);
     						 E10.setTextSize(20);
     						 E10.setTextColor(Color.BLACK);
     						 E11.setTextSize(20);
     						 E11.setTextColor(Color.BLACK);
     						 E12.setTextSize(20);
     						 E12.setTextColor(Color.BLACK);
     						 E13.setTextSize(20);
     						
     						 String Cn1 = s.getColumnName(0);
     						 String Cn2 = s.getColumnName(1);
     						 String Cn3 = s.getColumnName(2);
     						 String Cn4 = s.getColumnName(3);
     	  					 String Cn5 = s.getColumnName(4);
     						 String Cn6 = s.getColumnName(5);
     						// String Cn7 = s.getColumnName(5);
     						 //String Cn7 ="prueba";
     						 E1.setText(Cn1);
     						 E2.setText(Cn2);
     						 E3.setText(Cn3);
     						 E4.setText(Cn4);
     						 E5.setText(Cn5);
     						 E6.setText(Cn6);
     						 //E7.setText(Cn7);
     						 E8.setText(es);
     						 E9.setText(es);
     						 E10.setText(es);
     						 E11.setText(es);
     						 E12.setText(es);
     						 E13.setText(es);
     						 Erow1.addView(E1);
     						 Erow1.addView(E8);
     						 Erow1.addView(E2);
     						 Erow1.addView(E9);
     						 Erow1.addView(E3);
     						 Erow1.addView(E10);
     						 Erow1.addView(E4);
     						 Erow1.addView(E11);
     						 Erow1.addView(E5);
     						 Erow1.addView(E12);
     						 Erow1.addView(E6);
     						 Erow1.addView(E13);
     						// Erow1.addView(E7);
     						 E1.setGravity(Gravity.CENTER_HORIZONTAL);
     						 E2.setGravity(Gravity.CENTER_HORIZONTAL);
     						 E3.setGravity(Gravity.CENTER_HORIZONTAL);
     						 E4.setGravity(Gravity.CENTER_HORIZONTAL);
     						 E6.setGravity(Gravity.CENTER_HORIZONTAL);
     						// E7.setGravity(Gravity.CENTER_HORIZONTAL);
     						 E8.setGravity(Gravity.CENTER_HORIZONTAL);
     						 E9.setGravity(Gravity.CENTER_HORIZONTAL);
     						 E10.setGravity(Gravity.CENTER_HORIZONTAL);
     						 E11.setGravity(Gravity.CENTER_HORIZONTAL);
     						 E12.setGravity(Gravity.CENTER_HORIZONTAL);
     						 E13.setGravity(Gravity.CENTER_HORIZONTAL);
     						 Erow1.setGravity(Gravity.CENTER);
     						 Erow1.setBackgroundColor(Color.GREEN);
     						 TablaEnc.addView(Erow1);	
     						// finaliza agregar encabezados de columnas 
     						int sz = s.getColumnCount();
     						int cont = 0;
     					
     					    String[] fech =new String[]{"2018-06-08"};
     						//Cursor s = db.getDispSemillas(fech);
     					    String datoFecha = editFechade.getText().toString().trim();
     					    
     						//String[]datos = new String[]{datoFecha};
     						//Cursor s = db.getDatos(datos);
     					  
     						
     						//while (s.moveToNext()){
     					//	while (s.moveToNext()) {
     						
     							while(s.moveToNext()){
     					     String d1 = s.getString(0);
     						 String d2 = s.getString(1);
     						 String d3 = s.getString(2);
     						 String d4 = s.getString(3);
     					     String d5 = s.getString(4);
     					     String d6 = s.getString(5);
     						 String c1 = comprobar(d1);
     						 String c2 = comprobar(d2);
     						 String c3 = comprobar(d3);
     						 String c4 = comprobar(d4);
     						 String c5 = comprobar(d5);
     						 String c6 = comprobar(d6);
     							for (int i = 1; i <2; i++)
     							 {	
     								 cont = cont +1;
     								 TableRow row1 = new TableRow(ConsultaDisponibilidad.this);
     								TextView t1 = new TextView(ConsultaDisponibilidad.this);
     								TextView t2 = new TextView(ConsultaDisponibilidad.this);
     								TextView t3 = new TextView(ConsultaDisponibilidad.this);
     								TextView t4 = new TextView(ConsultaDisponibilidad.this);
     								TextView t5 = new TextView(ConsultaDisponibilidad.this);
     								TextView t6 = new TextView(ConsultaDisponibilidad.this);
     								TextView t7 = new TextView(ConsultaDisponibilidad.this);
     								TextView t8 = new TextView(ConsultaDisponibilidad.this); 
     								TextView t9 = new TextView(ConsultaDisponibilidad.this); 
     								TextView t10 = new TextView(ConsultaDisponibilidad.this); 
     								TextView t11 = new TextView(ConsultaDisponibilidad.this); 
     								TextView t12 = new TextView(ConsultaDisponibilidad.this); 
     								TextView t13 = new TextView(ConsultaDisponibilidad.this);
     								 t1.setTextSize(13);
     								 t1.setTextColor(Color.BLACK);
     								t2.setTextSize(13);
     								 t2.setTextColor(Color.BLACK);
     								 t3.setTextSize(13);
     								 t3.setTextColor(Color.BLACK);
     								 t4.setTextSize(13);
     								 t4.setTextColor(Color.BLACK);
     								 t5.setTextSize(13);
     								 t5.setTextColor(Color.BLACK);
     								t6.setTextSize(13);
     								 t6.setTextColor(Color.BLACK);
     								 t7.setTextSize(13);
     								 t7.setTextColor(Color.BLACK);
     								 
     								 t8.setTextSize(20);
     								 t8.setTextColor(Color.BLACK);
     								 t9.setTextSize(20);
     								 t9.setTextColor(Color.BLACK);
     								 t10.setTextSize(20);
     								 t10.setTextColor(Color.BLACK);
     								 t11.setTextSize(20);
     								 t11.setTextColor(Color.BLACK);
     								 t12.setTextSize(20);
     								 t12.setTextColor(Color.BLACK);
     								t13.setTextSize(20);
     								 t1.setGravity(Gravity.CENTER_HORIZONTAL);
     								 t2.setGravity(Gravity.CENTER_HORIZONTAL);
     								 t3.setMaxWidth(150);
     								 t4.setMaxWidth(150);
     								 t5.setMaxWidth(100);
     								 t4.setGravity(Gravity.CENTER_HORIZONTAL);
     								 t5.setGravity(Gravity.CENTER_HORIZONTAL);
     								t6.setGravity(Gravity.CENTER_HORIZONTAL);
     								t7.setGravity(Gravity.CENTER_HORIZONTAL);
     								 t8.setGravity(Gravity.CENTER_HORIZONTAL);
     								 t9.setGravity(Gravity.CENTER_HORIZONTAL);
     								 t10.setGravity(Gravity.CENTER_HORIZONTAL);
     								 t11.setGravity(Gravity.CENTER_HORIZONTAL);
     								 t12.setGravity(Gravity.CENTER_HORIZONTAL);
     								t13.setGravity(Gravity.CENTER_HORIZONTAL);
     								 row1.setGravity(Gravity.CENTER);
     								 row1.setClickable(true);
     								 if(cont%2 == 0){
     								 row1.setBackgroundColor(Color.LTGRAY);
     								 }else {
     								 row1.setBackgroundColor(Color.WHITE);	 
     								 }
     								 
     							 t1.setText(c1);
     							 t2.setText(c2);
     							 t3.setText(c3);
     							 t4.setText(c4);
     							 t5.setText(c5);
     							 t6.setText(c6);
     							
     							 t8.setText(es);
     							 t9.setText(es);
     							 t10.setText(es);
     							 t11.setText(es);
     							 t12.setText(es);
     							 t13.setText(es);
     							 row1.addView(t1);
     							 row1.addView(t8);
     							 row1.addView(t2);
     							 row1.addView(t9);
     							 row1.addView(t3);
     							 row1.addView(t10);
     							 row1.addView(t4);
     							 row1.addView(t11);
     							 row1.addView(t5);
     							 row1.addView(t12);
     							 
     							 row1.addView(t6);
     							 row1.addView(t13);
     							 row1.addView(t7);
     							 TablaEnc.addView(row1);
     							 TablaEnc.setClickable(true);
     							 }
     							}
     	 					 } 
    					db.close();
  
                	} 					 
                	
                	
    				  catch (Exception e) {
    					Toast toast = Toast.makeText(getApplicationContext(), "Verificar Conexi�n.....", Toast.LENGTH_SHORT);
    	            	toast.show();
    					e.printStackTrace();
                    } 
                   
            		
            	} else {   //Cuando se hace una consulta con Conexi�n directa al servidor.
            		
            	try {
            		
            	 user = "sim";
            	 password = "sim";
            	 Connection connection = null;
            	 ResultSet rs = null;
            	 Statement statement = null;
            	 
					Class.forName("com.ibm.as400.access.AS400JDBCDriver");
					connection = DriverManager.getConnection("jdbc:as400://190.190.190.10", user, password);
					System.setProperty("http.keepAlive", "false");
				
					statement = connection.createStatement();		
					
					rs = statement.executeQuery("select s.invernadero INV, sop.descripcion ACT, m.descripcionmaterial MATERIAL, " +
							"d.siembra SIEMBRA, s.plantas PLANTAS, ((sum(decimal(d.flores,10,2)) / s.plantasxbolsa) / count(decimal(d.flores,10,2))) DISP, " +
							"(((sum(decimal(d.flores,10,2)) / s.plantasxbolsa) / count(decimal(d.flores,10,2))) * s.plantas ) Flor_Disp " +
							"from (select distinct d.siembra,d.flores,d.fecha,d.actividad,d.horainicio from sisappf.app_disponibilidadflores d " +
							"where fecha = '"+Fecha+"' and estado = 'AC') d LEFT JOIN SISSEMF.MOV_DEFSIEMBRAE S ON D.SIEMBRA = S.NOSIEMBRA " +
							"left join siscfgf.seguridad_opcion_moduloasp sop on d.actividad = sop.opcion left join sissemf.mmaterialsem m " +
							"on s.material = m.codigomaterial inner join (SELECT DISTINCT S.INVERNADERO FROM SISAPPF.APP_TMP_GENERAL G " +
							"LEFT JOIN SISSEMF.MOV_DEFSIEMBRAE S ON G.SIEMBRA = S.NOSIEMBRA WHERE FECHA = '"+Fecha+"' AND " +
							"G.SUPERVISOR = '"+CodigoEmpleado+"') seg ON S.INVERNADERO = SEG.INVERNADERO group by s.invernadero,d.siembra,d.fecha, " +
							"d.actividad,sop.descripcion,m.descripcionmaterial,s.plantas,s.plantasxbolsa order by s.invernadero, " +
							"sop.descripcion,m.descripcionmaterial,d.siembra");
				
				
					if(!rs.isBeforeFirst()){
						alertDialogMensaje("Mensaje","No Existen Datos con esta Fecha: "+Fecha);
					}
					else{
					//Agregar encabezados de columnas	
					 TableRow Erow1 = new TableRow(ConsultaDisponibilidad.this);
					 TextView E1 = new TextView(ConsultaDisponibilidad.this);
					 TextView E2 = new TextView(ConsultaDisponibilidad.this);
					 TextView E3 = new TextView(ConsultaDisponibilidad.this);
					 TextView E4 = new TextView(ConsultaDisponibilidad.this);
					 TextView E5 = new TextView(ConsultaDisponibilidad.this);
					 TextView E6 = new TextView(ConsultaDisponibilidad.this);
					 TextView E7 = new TextView(ConsultaDisponibilidad.this);
					 TextView E8 = new TextView(ConsultaDisponibilidad.this); 
					 TextView E9 = new TextView(ConsultaDisponibilidad.this); 
					 TextView E10 = new TextView(ConsultaDisponibilidad.this); 
					 TextView E11 = new TextView(ConsultaDisponibilidad.this); 
					 TextView E12 = new TextView(ConsultaDisponibilidad.this); 
					 TextView E13 = new TextView(ConsultaDisponibilidad.this);
					 E1.setTextSize(15);
					 E1.setTextColor(Color.BLACK);
					 E2.setTextSize(15);
					 E2.setTextColor(Color.BLACK);
					 E3.setTextSize(15);
					 E3.setTextColor(Color.BLACK);
					 E4.setTextSize(15);
					 E4.setTextColor(Color.BLACK);
					 E5.setTextSize(15);
					 E5.setTextColor(Color.BLACK);
					 E6.setTextSize(15);
					 E6.setTextColor(Color.BLACK);
					 E7.setTextSize(15);
					 E7.setTextColor(Color.BLACK);
					 E8.setTextSize(20);
					 E8.setTextColor(Color.BLACK);
					 E9.setTextSize(20);
					 E9.setTextColor(Color.BLACK);
					 E10.setTextSize(20);
					 E10.setTextColor(Color.BLACK);
					 E11.setTextSize(20);
					 E11.setTextColor(Color.BLACK);
					 E12.setTextSize(20);
					 E12.setTextColor(Color.BLACK);
					 E13.setTextSize(20);
					 String Cn1 = rs.getMetaData().getColumnName(1);
					 String Cn2 = rs.getMetaData().getColumnName(2);
					 String Cn3 = rs.getMetaData().getColumnName(3);
					 String Cn4 = rs.getMetaData().getColumnName(4);
					 String Cn5 = rs.getMetaData().getColumnName(5);
					 String Cn6 = rs.getMetaData().getColumnName(6);
					 String Cn7 = rs.getMetaData().getColumnName(7);
					 E1.setText(Cn1);
					 E2.setText(Cn2);
					 E3.setText(Cn3);
					 E4.setText(Cn4);
					 E5.setText(Cn5);
					 E6.setText(Cn6);
					 E7.setText(Cn7);
					 E8.setText(es);
					 E9.setText(es);
					 E10.setText(es);
					 E11.setText(es);
					 E12.setText(es);
					 E13.setText(es);
					 Erow1.addView(E1);
					 Erow1.addView(E8);
					 Erow1.addView(E2);
					 Erow1.addView(E9);
					 Erow1.addView(E3);
					 Erow1.addView(E10);
					 Erow1.addView(E4);
					 Erow1.addView(E11);
					 Erow1.addView(E5);
					 Erow1.addView(E12);
					 Erow1.addView(E6);
					 Erow1.addView(E13);
					 Erow1.addView(E7);
					 E1.setGravity(Gravity.CENTER_HORIZONTAL);
					 E2.setGravity(Gravity.CENTER_HORIZONTAL);
					 E3.setGravity(Gravity.CENTER_HORIZONTAL);
					 E4.setGravity(Gravity.CENTER_HORIZONTAL);
					 E5.setGravity(Gravity.CENTER_HORIZONTAL);
					 E6.setGravity(Gravity.CENTER_HORIZONTAL);
					 E7.setGravity(Gravity.CENTER_HORIZONTAL);
					 E8.setGravity(Gravity.CENTER_HORIZONTAL);
					 E9.setGravity(Gravity.CENTER_HORIZONTAL);
					 E10.setGravity(Gravity.CENTER_HORIZONTAL);
					 E11.setGravity(Gravity.CENTER_HORIZONTAL);
					 E12.setGravity(Gravity.CENTER_HORIZONTAL);
					 E13.setGravity(Gravity.CENTER_HORIZONTAL);
					 Erow1.setGravity(Gravity.CENTER);
					 Erow1.setBackgroundColor(Color.GREEN);
					 TablaEnc.addView(Erow1);	
					// finaliza agregar encabezados de columnas 
					int sz = rs.getFetchSize();
					int cont = 0;
					while (rs.next()) {
						 String d1 = rs.getString(1).trim();
						 String d2 = rs.getString(2).trim();
						 String d3 = rs.getString(3).trim();
						 String d4 = rs.getString(4).trim();
					     String d5 = rs.getString(5).trim();
						 String d6 = rs.getString(6).trim();
						 String d7 = rs.getString(7).trim();
						 String c1 = comprobar(d1);
						 String c2 = comprobar(d2);
						 String c3 = comprobar(d3);
						 String c4 = comprobar(d4);
						 String c5 = comprobar(d5);
						 String c6 = comprobar(d6);
						 String c7 = comprobar(d7);
						 for (int i = 0; i == sz; i++)
						 {	
							 cont = cont +1;
							 TableRow row1 = new TableRow(ConsultaDisponibilidad.this);
							 TextView t1 = new TextView(ConsultaDisponibilidad.this);
							 TextView t2 = new TextView(ConsultaDisponibilidad.this);
							 TextView t3 = new TextView(ConsultaDisponibilidad.this);
							 TextView t4 = new TextView(ConsultaDisponibilidad.this);
							 TextView t5 = new TextView(ConsultaDisponibilidad.this);
							 TextView t6 = new TextView(ConsultaDisponibilidad.this);
							 TextView t7 = new TextView(ConsultaDisponibilidad.this);
							 TextView t8 = new TextView(ConsultaDisponibilidad.this); 
							 TextView t9 = new TextView(ConsultaDisponibilidad.this); 
							 TextView t10 = new TextView(ConsultaDisponibilidad.this); 
							 TextView t11 = new TextView(ConsultaDisponibilidad.this); 
							 TextView t12 = new TextView(ConsultaDisponibilidad.this); 
							 TextView t13 = new TextView(ConsultaDisponibilidad.this);
							 t1.setTextSize(13);
							 t1.setTextColor(Color.BLACK);
							 t2.setTextSize(13);
							 t2.setTextColor(Color.BLACK);
							 t3.setTextSize(13);
							 t3.setTextColor(Color.BLACK);
							 t4.setTextSize(13);
							 t4.setTextColor(Color.BLACK);
							 t5.setTextSize(13);
							 t5.setTextColor(Color.BLACK);
							 t6.setTextSize(13);
							 t6.setTextColor(Color.BLACK);
							 t7.setTextSize(13);
							 t7.setTextColor(Color.BLACK);
							 t8.setTextSize(20);
							 t8.setTextColor(Color.BLACK);
							 t9.setTextSize(20);
							 t9.setTextColor(Color.BLACK);
							 t10.setTextSize(20);
							 t10.setTextColor(Color.BLACK);
							 t11.setTextSize(20);
							 t11.setTextColor(Color.BLACK);
							 t12.setTextSize(20);
							 t12.setTextColor(Color.BLACK);
							 t13.setTextSize(20);
							 t1.setGravity(Gravity.CENTER_HORIZONTAL);
							 t2.setGravity(Gravity.CENTER_HORIZONTAL);
							 t3.setMaxWidth(150);
							 t4.setMaxWidth(150);
							 t5.setMaxWidth(100);
							 t4.setGravity(Gravity.CENTER_HORIZONTAL);
							 t5.setGravity(Gravity.CENTER_HORIZONTAL);
							 t6.setGravity(Gravity.CENTER_HORIZONTAL);
							 t7.setGravity(Gravity.CENTER_HORIZONTAL);
							 t8.setGravity(Gravity.CENTER_HORIZONTAL);
							 t9.setGravity(Gravity.CENTER_HORIZONTAL);
							 t10.setGravity(Gravity.CENTER_HORIZONTAL);
							 t11.setGravity(Gravity.CENTER_HORIZONTAL);
							 t12.setGravity(Gravity.CENTER_HORIZONTAL);
							 t13.setGravity(Gravity.CENTER_HORIZONTAL);
							 row1.setGravity(Gravity.CENTER);
							 row1.setClickable(true);
							 if(cont%2 == 0){
							 row1.setBackgroundColor(Color.LTGRAY);
							 }else {
							 row1.setBackgroundColor(Color.WHITE);	 
							 }
						 t1.setText(c1);
						 t2.setText(c2);
						 t3.setText(c3);
						 t4.setText(c4);
						 t5.setText(c5);
						 t6.setText(c6);
						 t7.setText(c7);
						 t8.setText(es);
						 t9.setText(es);
						 t10.setText(es);
						 t11.setText(es);
						 t12.setText(es);
						 t13.setText(es);
						 row1.addView(t1);
						 row1.addView(t8);
						 row1.addView(t2);
						 row1.addView(t9);
						 row1.addView(t3);
						 row1.addView(t10);
						 row1.addView(t4);
						 row1.addView(t11);
						 row1.addView(t5);
						 row1.addView(t12);
						 row1.addView(t6);
						 row1.addView(t13);
						 row1.addView(t7);
						 TablaEnc.addView(row1);
						 TablaEnc.setClickable(true);
						 }
					}
					}
					connection.close();	
            	}
				  catch (Exception e) {
					Toast toast = Toast.makeText(getApplicationContext(), "Verificar Conexi�n.....", Toast.LENGTH_SHORT);
	            	toast.show();
					e.printStackTrace();
                } 
               }
             }
            }
        });
	}	    
		
	public void alertDialogMensaje(String message1, String mesage2){
	
		try {
		    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
		    ringtone.play();	    	    
		} catch (Exception e) {
		    e.printStackTrace();
		}
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(message1);
		alertDialog.setMessage(mesage2);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
		public void onClick(final DialogInterface dialog, final int which) {
		// here you can add functions
			ringtone.stop();
		}
		});
		alertDialog.show();
	}
	
	public String comprobar(String cad){
		String Pb = "";
		if(cad.equals(Pb)){
			cad = "---";
		}
		return cad;	
	}
	
	public String format (int f){
		val = "";
		if(f<10){
		val = "0"+Integer.toString(f);	
		}
		else{
		val = Integer.toString(f);
		}
		return val;
	}
	
	public void Calcular () {
		String Siembra = "";
		int Actividad = 0;
		int Muestras = 0;
		Cursor Disponibilidades = db.getDispSemillas("Fecha = '"+ Fecha +"'");
		 DisponibilidadSemillas DS = db.getDispSemillasFromCursor(Disponibilidades, 0);
		 Siembra = DS.getSiembra();
		 Actividad = DS.getActividad();
		 
		 Cursor Dispon2 = db.getDispSemillas("Fecha = '"+ Fecha +"' AND Siembra='" + Siembra + "' AND Actividad = "+ Actividad );
		 if(Dispon2.getCount()>0){
			 for(int i=0 ; i<Dispon2.getCount();i++){
				 
			 }
		 }
		 Dispon2.close();
		 Disponibilidades.close();
		 
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.soporteit, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}  	

