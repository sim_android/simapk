package com.example.sim;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import com.example.floricultura.R;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.Dguardados;
import com.example.sim.data.Dinvernaderos;
import com.example.sim.data.EtiquetasVegetativo;
import com.example.sim.data.JSONReader;
import com.example.sim.data.JsonFiles;
import com.example.sim.data.RangoEtiqueta;
import com.example.sim.data.Usuario;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.os.Build;

public class CapturaEtiquetas extends ActionBarActivity {
	
	private MyApp appState;	
	private DBAdapter db;
	int CodigoModulo;
	int Codigodepartamento;
	int CodigoEmpleado;
	int CodigoOpcion;
	int EsCorte;
	int TotalEmpleadosAsignados;
	int TotalEtiquetasAsignadas;
	boolean validoInvernadero;
	String NombreOpcion;
	EditText invernadero;
	EditText etiqueta;
	TextView TCantidadAsignados;
	Ringtone ringtone;
	RangoEtiqueta rangos;
	CountDownTimer tim = new CountDownTimer(45000, 1000) {
		
	     public void onTick(long millisUntilFinished){
	     }

	     public void onFinish() {
	         invernadero.setText("");
	         etiqueta.setText("");
	         
	         invernadero.setEnabled(true);
	         etiqueta.setEnabled(true);
	         invernadero.requestFocus();
	     }
	  } ;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_captura_etiquetas);
		
		
		Bundle extras = getIntent().getExtras();
		CodigoModulo = extras.getInt("CodigoModulo");
	    CodigoEmpleado = extras.getInt("CodigoEmpleado");
	    Codigodepartamento = extras.getInt("Codigodepartamento");
	    CodigoOpcion = extras.getInt("CodigoOpcion");
	    NombreOpcion = extras.getString("NombreOpcion");
	    
	    TextView TNombreCaptura = (TextView)findViewById(R.id.OpcionCaptura);
	    TextView TCodigoEmpleado= (TextView)findViewById(R.id.CodigoEmpleado);
	    TextView TCodigoOpcion= (TextView)findViewById(R.id.codigoOpcion);
	    TextView textViewEtiqueta= (TextView)findViewById(R.id.textViewEtiqueta);
	    TCantidadAsignados = (TextView)findViewById(R.id.textEmpleadosAsginados);
	    invernadero  = (EditText)findViewById(R.id.editComentario);
	    etiqueta  = (EditText)findViewById(R.id.editEtiqueta);
	    etiqueta.setInputType(InputType.TYPE_CLASS_NUMBER);   //Para Asignarle un tipo de dato 
	    etiqueta.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
	    etiqueta.setSingleLine(false);   // se utiliza para que el EditText no haga un Tab Automatico.
	    
	    Button invernaderoBtn = (Button) findViewById(R.id.btnAsistencia);
	    Button btnBack = (Button) findViewById(R.id.btnBack);
	    
	    appState = ((MyApp)getApplicationContext());   
	    db = appState.getDb();
	    db.open();
	    
	    rangos = new RangoEtiqueta();
	    
	    
	    TNombreCaptura.setText(NombreOpcion+ "");
	    TCodigoEmpleado.setText(CodigoEmpleado + "");
	    TCodigoOpcion.setText(CodigoOpcion + "");
	    validoInvernadero = false;
	    obtenerRangoEtiqueta();
	    
	    verificarAsginados();
	    
	    //se limpian los campos y variables para una nueva captura
	    invernaderoBtn.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	        	invernadero.setText("");
	        	validoInvernadero = false;
	        	etiqueta.setText("");
	        	invernadero.setEnabled(true);
	        	invernadero.requestFocus();
	        	contador();	        	
	        }
	    });
	    
	    
	   	invernadero.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                	VerificarDatos(invernadero.getText().toString().trim());
                if (validoInvernadero == true){	
                	invernadero.setEnabled(false);
                	etiqueta.requestFocus();
                	contador();
                	//alertDialogMensaje("Mensaje", "Valido Bien");
                	}else {
                		invernadero.setText("");
                		invernadero.requestFocus();
                		alertDialogMensaje("Mensaje", "Error Invernadero NO Existe");
                		contador();
                	}
                return true;
                }
                return false;
            }
        });
	    
	    etiqueta.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                	String val = ("");
                	String inv = invernadero.getText().toString().trim();
                	String et = etiqueta.getText().toString().trim();
                	if(inv.equals(val)){
                	 invernadero.requestFocus();
                	 etiqueta.setText("");
               		 alertDialogMensaje("Mensaje", "Error Falta Invernadero");	
                	}else if(et.equals(inv)){
                		 etiqueta.setText("");
                  		 alertDialogMensaje("Mensaje", "Error de Etiqueta");
                  		 etiqueta.requestFocus();
                	}else{
                		if (ValidarEtiqueta()){                			
                    		EtiquetasVegetativo EVtemp = new EtiquetasVegetativo();
                    		
                    		String empleadoEscaneado = "999";
                    		EVtemp.setCodigoEmpleado(Integer.parseInt(empleadoEscaneado));
                    		EVtemp.setEtiqueta(etiqueta.getText().toString().trim());
                    		EVtemp.setFinalizado(0);
                    		EVtemp.setInvernadero(invernadero.getText().toString().trim());
                    		
                    		Calendar c = Calendar.getInstance(); 
                    		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
                    		String fecha = fdate.format(c.getTime());
                    		SimpleDateFormat ftime = new SimpleDateFormat("HH:mm:ss");
                    		String horaInicio = ftime.format(c.getTime());
	                			                		
	                		EVtemp.setFecha(fecha);
	                		EVtemp.setHora(horaInicio);
	                		
	                		//Agrege codigo de Supervisor y Actividad
	                		EVtemp.setSupervisor(CodigoEmpleado);
	                		EVtemp.setActividad(CodigoOpcion);	                		
	                		
	                		ObjectMapper mapper = new ObjectMapper();
                    		mapper = new ObjectMapper();
                    		try {
                    			String nameJson = "L1_" +empleadoEscaneado + "_"+EVtemp.getFecha() + "_"+EVtemp.getHora() + "_"+etiqueta.getText().toString().trim();
                    			nameJson=nameJson.replace(":", "-");
								mapper.writeValue(new File( Environment.getExternalStorageDirectory()+"/SIM/JsonDescarga/" + nameJson+ ".json"), EVtemp);
																
								JsonFiles archivo = new JsonFiles();
								archivo.setName(nameJson+ ".json");
								archivo.setNameFolder("SinFinalizar/");
								archivo.setUpload(0);
								db.insertarJsonFile(archivo);
								
								if(!appState.getSubiendoArchivos()){
									appState.setSubiendoArchivos(true);
									new UptoDropbox().execute(getApplicationContext());
								}
								
							} catch (JsonGenerationException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (JsonMappingException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
	                		                 		
                    		db.insertarEtiquetas(EVtemp);
                    		etiqueta.setText("");
                    		etiqueta.requestFocus();                   		
                    		
                    		//agregado para contar etiquetas por empleado
                    		contador();
                    		TotalEtiquetasAsignadas++;
	                		TCantidadAsignados.setText(TotalEtiquetasAsignadas + "");
                    		
                		}else{
                			etiqueta.setText("");
                    		etiqueta.requestFocus();
                    		contador();                   		
                		}
                	} 		               		              	
	                  return true;
                }
                return false;
            }
        });
	         
       
        btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
 
            	onBackPressed();
            }
        });
               
	}
	
	
	public void verificarAsginados(){
		Cursor CursorGuardados =	db.getDguardados("Actividad=" + CodigoOpcion + " AND ActividadFinalizada=0" );
		TotalEmpleadosAsignados = CursorGuardados.getCount();
		TCantidadAsignados.setText(TotalEmpleadosAsignados + "");
		CursorGuardados.close();
	}
	
	public void EtiquetasAsginadas(){
		//Cursor cursorEtiquetaG =	db.getEtiquetas("Actividad=" + CodigoOpcion + " AND CodigoEmpleado=" + empleado +"");
		Cursor cursorEtiquetaG =	db.getEtiquetas("Actividad");
		TotalEtiquetasAsignadas = cursorEtiquetaG.getCount();
		TCantidadAsignados.setText(TotalEtiquetasAsignadas + "");
		cursorEtiquetaG.close();
	}
		
	public void obtenerRangoEtiqueta(){
		
		JSONReader _reader = appState.getReader();
		ArrayList<RangoEtiqueta> r = (ArrayList<RangoEtiqueta>) ( _reader.ReadFromSDCard(6, "SIM/JsonCarga/D_Vegetativo.json"));
		if(r.size()>0){
			this.rangos = r.get(0);
		}
		
	}
	
	public boolean ValidarEtiqueta(){
		boolean valido = false;	
		String empleadoEscaneado = "999";
		int empelado  = Integer.parseInt(empleadoEscaneado);
		String invernaderoEscaneado = invernadero.getText().toString().trim();
		String etiquetaEscaneada = etiqueta.getText().toString().trim();  	
		//se hace la consulta para validar si la etiqueta ya fue ingresada en el sistema
		Cursor cursorEtiqueta = db.getEtiquetas("CodigoEmpleado=" + empelado + " AND Etiqueta='" + etiquetaEscaneada + "' AND Invernadero='" + invernaderoEscaneado + "'");
		if(cursorEtiqueta.getCount() > 0 ){
			valido = false;
			alertDialogMensaje("Error Etiqueta", "Etiqueta ya Escaneada");
		}else{
			//se valida el rango de la etiqueta
			int valorEtiqueta = Integer.parseInt(etiquetaEscaneada);
			Log.d("valorEtiqueta",valorEtiqueta + "");
			Log.d("valorEtiqueta", this.rangos.getInicio() + "" + this.rangos.getFin());
			if(valorEtiqueta> this.rangos.getInicio() && valorEtiqueta<this.rangos.getFin()){
				valido = true;
			}else{
				alertDialogMensaje("Error Etiqueta", "Valor de Etiqueta no valido");
			}	
		}
		cursorEtiqueta.close();
		return valido;
	}
	
	public void alertDialogMensaje(String message1, String mesage2){
		

		
		try {
		    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
		    ringtone.play();
		    
		    
		} catch (Exception e) {
		    e.printStackTrace();
		}
		
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(message1);
		alertDialog.setMessage(mesage2);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int which) {
		// here you can add functions
			ringtone.stop();
		}
		});
		alertDialog.show();
	}
		
	//retornara un valor dependiendo del estado del empleado
	// 0 = no existe
	// 1 = existe pero esta asignado
	// 2 = existe y no esta asignado
	public int VerificarEmpleado(String empleado){
		int valor = 0;
		Cursor CursorDacceso =	db.getDacceso("CodigoEmpleado='" + empleado + "'");
		if(CursorDacceso != null  && CursorDacceso.getCount()>0){
			Cursor CursorGuardados =	db.getDguardados("CodigoEmpleado=" + empleado + " AND ActividadFinalizada=0");
			if(CursorGuardados.getCount()>0){
				//validarActividadCorte();
				valor = 1;
			}else{
				valor = 2;
			}
			CursorGuardados.close();
		}
		CursorDacceso.close();
		return valor;
	}
	
	public void VerificarDatos(String dato){
		validoInvernadero= false;
		 Cursor CursorInv = db.getDInvernaderos("Invernadero='" + dato + "'");
		 if(CursorInv.getCount()>0){
			 Dinvernaderos s = db.getDInvernaderosFromCursor(CursorInv, 0);
			 Log.d("INVERNADERO",s.getInvernadero());			 
			 validoInvernadero = true;
			 contador();
		 }else{
			
			 validoInvernadero = false;
		 }
		 CursorInv.close();
	}
	
	public void contador() {
		  tim.cancel();
		  tim.start();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.captura_vegetativo, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}



}