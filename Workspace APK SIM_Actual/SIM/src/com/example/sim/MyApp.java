package com.example.sim;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.util.Log;

import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.example.sim.data.DBAdapter;
import com.example.sim.data.JSONReader;

public class MyApp  extends Application {
	
	 ///////////////////////////////////////////////////////////////////////////
    //                      Your app-specific settings.                      //
    ///////////////////////////////////////////////////////////////////////////

    // Replace this with your app key and secret assigned by Dropbox.
    // Note that this is a really insecure way to do this, and you shouldn't
    // ship code which contains your key & secret in such an obvious way.
    // Obfuscation is good.
    String APP_KEY = "ibljq149f1cavdu";
    String APP_SECRET = "b9793fprwih6x0z";

    ///////////////////////////////////////////////////////////////////////////
    //                      End app-specific settings.                       //
    ///////////////////////////////////////////////////////////////////////////
    String ACCOUNT_PREFS_NAME = "prefs";
    String ACCESS_KEY_NAME = "ACCESS_KEY";
    String ACCESS_SECRET_NAME = "ACCESS_SECRET";


	private static DbxClientV2 dbxClientV2;
	
	private DBAdapter db;
	private JSONReader reader;	
	
	boolean subiendoArchivos;
	boolean mostrarDialog;

	public void onCreate(){
        super.onCreate();
        subiendoArchivos = false;
        mostrarDialog= false;
        setDB();
        setJSONReader();   
        //DropBoxInit();
	}
	
	
	public boolean getMostrarDialog() {
		return mostrarDialog;
	}


	public void setMostrarDialog(boolean mostrarDialog) {
		this.mostrarDialog = mostrarDialog;
	}
	
	
	public boolean getSubiendoArchivos() {
		return subiendoArchivos;
	}


	public void setSubiendoArchivos(boolean subiendoArchivos) {
		this.subiendoArchivos = subiendoArchivos;
	}

	public DbxClientV2 getDropBoxClient () {
		return dbxClientV2;
	}
	
	public void initDropBoxV2(String ACCESS_TOKEN) {
		DbxRequestConfig config = new DbxRequestConfig("dropbox/sim", "en_ES");
		dbxClientV2 = new DbxClientV2(config, ACCESS_TOKEN);
	}
	
	

	
	public String getAKey(){
		return this.APP_KEY;
	}
	
	public String getASecret(){
		return this.APP_SECRET;
	}
	

	
    /**
     * Shows keeping the access keys returned from Trusted Authenticator in a local
     * store, rather than storing user name & password, and re-authenticating each
     * time (which is not to be done, ever).
     *//*
    private void loadAuth(AndroidAuthSession session) {
        SharedPreferences prefs = getSharedPreferences(ACCOUNT_PREFS_NAME, 0);
        String key = prefs.getString(ACCESS_KEY_NAME, null);
        String secret = prefs.getString(ACCESS_SECRET_NAME, null);
        if (key == null || secret == null || key.length() == 0 || secret.length() == 0) return;

        if (key.equals("oauth2:")) {
            // If the key is set to "oauth2:", then we can assume the token is for OAuth 2.
            session.setOAuth2AccessToken(secret);
        } else {
            // Still support using old OAuth 1 tokens.
            session.setAccessTokenPair(new AccessTokenPair(key, secret));
        }
    }
	
	
    private AndroidAuthSession buildSession() {
        AppKeyPair appKeyPair = new AppKeyPair(APP_KEY, APP_SECRET);

        AndroidAuthSession session = new AndroidAuthSession(appKeyPair);
        loadAuth(session);
        return session;
    }
	*/

	public void setDB() {
		if (db == null) {
			db = new DBAdapter(getApplicationContext());
			//exportDatabse("guatetravel.db");
		}
	}
	
	public DBAdapter getDb(){
		if (this.db == null) {
			this.db = new DBAdapter(getApplicationContext());	
		}		
		return this.db;
	}
	
	
	public void setJSONReader(){
		if (reader == null) {
			reader = new JSONReader(getApplicationContext());
		}
	}
	
	public JSONReader getReader(){
		return this.reader;
	}

	
    public void exportDatabse(String databaseName) {
        try {
        	Log.d("ESCRIBIENDO-DB", "escribiendo");
            File sd = Environment.getExternalStorageDirectory();
            File data = Environment.getDataDirectory();

            if (sd.canWrite()) {
                String currentDBPath = "//data//"+getPackageName()+"//databases//"+databaseName+"";
                String backupDBPath = "SIM.db";
                File currentDB = new File(data, currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            }
        } catch (Exception e) {

        }
    }
    

	
	public boolean connectionAvailable() {
		boolean connection = true;
		NetworkInfo info = (NetworkInfo) ((ConnectivityManager) 				
				getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();	    		
	    if (info == null || !info.isConnected() ) {
	    	connection = false;
	    } else if (info.isRoaming()){
	    	connection = false;
	    }
	    return connection;
	} 
	

}
