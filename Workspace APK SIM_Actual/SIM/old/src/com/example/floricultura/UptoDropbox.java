package com.example.floricultura;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.DropboxAPI.Entry;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.exception.DropboxException;
import com.example.floricultura.data.DBAdapter;
import com.example.floricultura.data.Dguardados;
import com.example.floricultura.data.JsonFiles;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

public class UptoDropbox extends AsyncTask<Context, Integer, Boolean> {   	
	
	private MyApp appState;	
	private DBAdapter db;
    DropboxAPI<AndroidAuthSession> mApi;
    
    String FILES_DIR = "/Floricultura/JsonDescarga/";
	
    @Override
    protected void onPreExecute() {
    	

    	//loadingDialog = ProgressDialog.show(MainActivity.this,"", "Sincronizando datos hacia el servidor", true, false);
    	Log.d("inciando subida","inciando subida");
    }
	 
    @Override 
    protected Boolean doInBackground(Context... params) {
    	
    	appState = ((MyApp)params[0]);   
        db = appState.getDb();
        db.open();
        this.mApi = appState.getMAPI();

        ArrayList<JsonFiles> files = getDatos();
        
        if(appState.connectionAvailable()){
        	
	        while(files.size()>0){    	
	        	JsonFiles file = files.remove(0);
	        	
	        	Log.d("SubiendoAAAA", file.getName()); 
		    	File dir = Environment.getExternalStorageDirectory();
			    File yourFile = new File(dir,"Floricultura/JsonDescarga/" + file.getName());
		    	
		   
		    	FileInputStream inputStream;
				try {
					
					String path = FILES_DIR + file.getNameFolder() + yourFile.getName();
					inputStream = new FileInputStream(yourFile);
		        	Entry response = mApi.putFile(path, inputStream,
		        			yourFile.length(), null, null);
		        	Log.i("DbExampleLog", "The uploaded file's rev is: " + response.rev);
					file.setUpload(1);
					db.updateJsonFiles(file);
					
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (DropboxException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				files = getDatos();
	        }
        	
        }
	
        return true;
    }
 

    @Override
    protected void onPostExecute(Boolean result) {
    	// loadingDialog.dismiss();
    	// appState.exportDatabse("floricultural.db");//exporta la base de datos a la SD card	
    	
    	boolean mostrarMensaje = appState.getMostrarDialog();
    	
    	if(mostrarMensaje){
    		Context context = appState;
            CharSequence text = "Se han sincronizado todos los archivos al dropbox";
            int duration = Toast.LENGTH_LONG;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
    	}
    	
    	appState.setSubiendoArchivos(false);
    	appState.setMostrarDialog(false);

    }
    
    
    public ArrayList<JsonFiles> getDatos(){
    	ArrayList<JsonFiles> Listtemp = new ArrayList<JsonFiles> ();
    	 Cursor filesjson = db.getJsonFiles("Upload=0");
		 if(filesjson.getCount()>0){
			 for(int i=0 ; i<filesjson.getCount();i++){	
				 JsonFiles Dtemp = this.db.getJsonFilesFromCursor(filesjson, i);
				 Listtemp.add(Dtemp);
			 }
		 }
    	 
    	 return Listtemp;
    }

}