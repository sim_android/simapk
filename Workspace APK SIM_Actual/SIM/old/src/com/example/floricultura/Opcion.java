package com.example.floricultura;

import java.util.ArrayList;

import com.example.floricultura.adapters.DatosLista;
import com.example.floricultura.adapters.ListAdapter;
import com.example.floricultura.data.DBAdapter;
import com.example.floricultura.data.Modulos;
import com.example.floricultura.data.Opciones;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.os.Build;


public class Opcion extends ActionBarActivity {
	
	private MyApp appState;	
	private DBAdapter db;
	ArrayList<DatosLista> listado = new ArrayList<DatosLista>();
	String id;
	int CodigoModulo;
	int Codigodepartamento;
	int CodigoEmpleado;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_opcion);
		
		
	    appState = ((MyApp)getApplicationContext());   
	    db = appState.getDb();
	    db.open();
		
		Bundle extras = getIntent().getExtras();
		CodigoModulo = extras.getInt("CodigoModulo");
	    CodigoEmpleado = extras.getInt("CodigoEmpleado");
	    Codigodepartamento = extras.getInt("Codigodepartamento");
	    
	    ListView listadoVista = (ListView)findViewById(R.id.ListOpciones);
		listado = getDatos();
					
		ListAdapter adaptador = new ListAdapter(this, R.layout.filas_lista, listado);
        listadoVista.setAdapter(adaptador);
        
        
        listadoVista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {   
            	DatosLista dato = (DatosLista) parent.getItemAtPosition(position);
     	    
            	if(CodigoModulo == 108 || CodigoModulo == 208){
            		
            		Intent intent = new Intent(getApplicationContext(), Ausencias.class);
	           	 	intent.putExtra("Codigodepartamento", Codigodepartamento);
	           	    intent.putExtra("CodigoEmpleado", CodigoEmpleado );
	           	    intent.putExtra("CodigoModulo",  CodigoModulo);
	           	    intent.putExtra("CodigoOpcion", Integer.parseInt(dato.getCampo1().trim()));
	           	    intent.putExtra("NombreOpcion", dato.getCampo2());
	           	    startActivity(intent);
	           	    
            		
            	}else if(dato.getCampo2().contains("FINALIZA ACTIVIDAD")){
            			
            			Intent intent = new Intent(getApplicationContext(), FinalizarActividad.class);
		           	 	intent.putExtra("Codigodepartamento", Codigodepartamento);
		           	    intent.putExtra("CodigoEmpleado", CodigoEmpleado );
		           	    intent.putExtra("CodigoModulo",  CodigoModulo);
		           	    intent.putExtra("CodigoOpcion", Integer.parseInt(dato.getCampo1().trim()));
		           	    intent.putExtra("NombreOpcion", dato.getCampo2());
		           	    startActivity(intent);
            			
            		} else if(Codigodepartamento == 1){
            			
            			if(dato.getCampo2().contains("LECTURA UNO")) {
            				Intent intent = new Intent(getApplicationContext(), CapturaEtiquetas.class);
    		           	 	intent.putExtra("Codigodepartamento", Codigodepartamento);
    		           	    intent.putExtra("CodigoEmpleado", CodigoEmpleado );
    		           	    intent.putExtra("CodigoModulo",  CodigoModulo);
    		           	    intent.putExtra("CodigoOpcion", Integer.parseInt(dato.getCampo1().trim()));
    		           	    intent.putExtra("NombreOpcion", dato.getCampo2());
    		           	 intent.putExtra("EsCorte", 1);
    		           	startActivity(intent);	
            				
            			}else{
    		           	

    	           	    	Intent intent = new Intent(getApplicationContext(), CapturaVegetativo.class);
    		           	 	intent.putExtra("Codigodepartamento", Codigodepartamento);
    		           	    intent.putExtra("CodigoEmpleado", CodigoEmpleado );
    		           	    intent.putExtra("CodigoModulo",  CodigoModulo);
    		           	    intent.putExtra("CodigoOpcion", Integer.parseInt(dato.getCampo1().trim()));
    		           	    intent.putExtra("NombreOpcion", dato.getCampo2());
    		           	    
    			           	 if(dato.getCampo2().contains("ASIGNACION_ETIQUETAS")){
    			           		intent.putExtra("EsCorte", 1); 
    			           	 }else{
    			           		intent.putExtra("EsCorte", 0); 
    			           	 }
    		           	    
    		           	    startActivity(intent);
    	           	    	
            			}
            			
            			
	           	    	
	           	    }else if(Codigodepartamento == 2){
	           	    	Intent intent = new Intent(getApplicationContext(), CapturaGeneral.class);
		           	 	intent.putExtra("Codigodepartamento", Codigodepartamento);
		           	    intent.putExtra("CodigoEmpleado", CodigoEmpleado );
		           	    intent.putExtra("CodigoModulo",  CodigoModulo);
		           	    intent.putExtra("CodigoOpcion", Integer.parseInt(dato.getCampo1().trim()));
		           	    intent.putExtra("NombreOpcion", dato.getCampo2());
		           	    
		           	    
		           	    
		           	    if(dato.getCampo2().contains("POLINIZACIONES")){
			           		intent.putExtra("OpcionPolinizacion", 1); 
			           	 }else{
			           		intent.putExtra("OpcionPolinizacion", 0); 
			           	 }
		           	    //CODIGO AGREGADO
		           	    if(dato.getCampo2().contains("SUCCION")){
		           	    	intent.putExtra("OpcionSuccion", 1);
		           	    }else{
		           	    	intent.putExtra("OpcionSuccion", 0);
		           	    }
		           	    //FIN CODIGO AGREGADO
		           	    
		           	    
		           	    if(dato.getCampo2().contains("COSECHAS")){
			           		intent.putExtra("OpcionCosecha", 1); 
			           	 }else{
			           		intent.putExtra("OpcionCosecha", 0); 
			           	 }
		           	 
		           	    
		           	 
		           	    startActivity(intent);
	    	
	           	    }

            }
        });
        
        
        Button btnBack = (Button) findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
 
            	onBackPressed();
            }
        });
        
        
        Button btnInicio = (Button) findViewById(R.id.btnInicio);
        btnInicio.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Intent intent = new Intent(getApplicationContext(), MainActivity.class);
           	 	startActivity(intent);
            }
        });


	}
	
	
	
	public ArrayList<DatosLista> getDatos(){
		ArrayList<DatosLista> listaTemp = new ArrayList<DatosLista>();
		listaTemp.add(new DatosLista("Opcion", "Descripcion", ""));
		

		 Cursor COpciones= null ;
		 COpciones = this.db.getOpciones("CodigoDepto=" + CodigoModulo);
		 
		 if(COpciones.getCount()>0){
			 for(int i=0 ; i<COpciones.getCount();i++){	         	
				 Opciones m = db.getOpcionesFromCursor(COpciones, i);  				 
				 listaTemp.add(new DatosLista(m.getOpcion()+ "", m.getDescripcion().trim(), ""));			 
		     } 
		 }

		
		
		return listaTemp;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.opciones, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}



}
