package com.example.floricultura;

import java.util.ArrayList;

import com.example.floricultura.adapters.DatosLista;
import com.example.floricultura.adapters.ListAdapter;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.os.Build;

public class DepartamentoActivity extends ActionBarActivity {

	ArrayList<DatosLista> listado = new ArrayList<DatosLista>();
	int CodigoEmpleado;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_departamento);
		
		Bundle extras = getIntent().getExtras();
	    CodigoEmpleado = extras.getInt("CodigoEmpleado");

		ListView listadoVista = (ListView)findViewById(R.id.ListDepartamento);
		listado = getDatos();
		
        ListAdapter adaptador = new ListAdapter(this, R.layout.filas_lista, listado);
        listadoVista.setAdapter(adaptador);
        
        

        listadoVista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {   
            	DatosLista dato = (DatosLista) parent.getItemAtPosition(position);
            	
            		Intent intent = new Intent(getApplicationContext(), ModuloActivity.class);
	           	 	intent.putExtra("Codigodepartamento", Integer.parseInt(dato.getCampo1().trim()) );
	           	    intent.putExtra("CodigoEmpleado", CodigoEmpleado );
	           	 	intent.putExtra("Administrador", 1);
	           	 	
	           	 	startActivity(intent);

            }
        });
        
        
        
        Button btnBack = (Button) findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
 
            	onBackPressed();
            }
        });
        
        
        Button btnInicio = (Button) findViewById(R.id.btnInicio);
        btnInicio.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Intent intent = new Intent(getApplicationContext(), MainActivity.class);
           	 	startActivity(intent);
            }
        });
        
	}
	
	
	public ArrayList<DatosLista> getDatos(){
		ArrayList<DatosLista> listaTemp = new ArrayList<DatosLista>();
		
		listaTemp.add(new DatosLista("CD", "Departamento", ""));
		listaTemp.add(new DatosLista("1 ", "Vegetativo", ""));
		listaTemp.add(new DatosLista("2 ", "Semillas", ""));
		listaTemp.add(new DatosLista("3 ", "Mantenimiento", ""));
		listaTemp.add(new DatosLista("4 ", "Administracion", ""));
		listaTemp.add(new DatosLista("5 ", "Recursos Humanos", ""));
		listaTemp.add(new DatosLista("6 ", "Inv. y Desarollo", ""));
		listaTemp.add(new DatosLista("7 ", "Finanzas", ""));
		listaTemp.add(new DatosLista("8 ", "Sistemas", ""));
		
		
		return listaTemp;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.departamento, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}



}
