package com.example.floricultura;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import com.example.floricultura.data.DBAdapter;
import com.example.floricultura.data.Dguardados;
import com.example.floricultura.data.EtiquetasVegetativo;
import com.example.floricultura.data.JsonFiles;
import com.example.floricultura.data.Opciones;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.os.Build;

public class FinalizarActividad extends ActionBarActivity {
	
	private MyApp appState;	
	private DBAdapter db;
	int CodigoModulo;
	int Codigodepartamento;
	int CodigoEmpleado;
	int CodigoOpcion;
	String NombreOpcion;
	EditText empleado;
	int finalizar;
	Ringtone ringtone;
	EditText EditBancas;
    EditText EditFlores;
	EditText EditPlantas;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_finalizar_actividad);
		
		Bundle extras = getIntent().getExtras();
		CodigoModulo = extras.getInt("CodigoModulo");
	    CodigoEmpleado = extras.getInt("CodigoEmpleado");
	    Codigodepartamento = extras.getInt("Codigodepartamento");
	    CodigoOpcion = extras.getInt("CodigoOpcion");
	    NombreOpcion = extras.getString("NombreOpcion");
	    
	    
	    TextView TNombreCaptura = (TextView)findViewById(R.id.OpcionCaptura);
	    TextView TCodigoEmpleado= (TextView)findViewById(R.id.CodigoEmpleado);
	    TextView TCodigoOpcion= (TextView)findViewById(R.id.codigoOpcion);
	    empleado  = (EditText)findViewById(R.id.editEmpleado);
	    Button btnBack = (Button) findViewById(R.id.btnBack);
	    Button btnListado = (Button) findViewById(R.id.btnListado);
	    
	    TextView TxtBancas = (TextView)findViewById(R.id.textViewBancas);
	    TextView TxtFlores= (TextView)findViewById(R.id.textFlores);
	    TextView TextPlantas= (TextView)findViewById(R.id.textPlantas);
	    EditBancas = (EditText)findViewById(R.id.editBancas);
	    EditFlores = (EditText)findViewById(R.id.editFlores);
	    EditPlantas = (EditText)findViewById(R.id.editPlantas);
	    
	  //  EditBancas.setText("0");
	   // EditFlores.setText("0");
	  //  EditPlantas.setText("0");
	    
	    
	    if(Codigodepartamento == 1){
	    	TxtFlores.setVisibility(View.INVISIBLE);
	    	EditFlores.setVisibility(View.INVISIBLE);
	    	EditPlantas.setVisibility(View.INVISIBLE);
	    	TextPlantas.setVisibility(View.INVISIBLE);
	    	
	    	empleado.requestFocus();
	    	//EditBancas.requestFocus();
	    	
	    }else if(Codigodepartamento == 2){
	    	TxtBancas.setVisibility(View.INVISIBLE);
	    	EditBancas.setVisibility(View.INVISIBLE);
	    	
	    	empleado.requestFocus();
	    	//EditPlantas.requestFocus();
	    }
	    
	    
	    
	    
	    TNombreCaptura.setText(NombreOpcion+ "");
	    TCodigoEmpleado.setText(CodigoEmpleado + "");
	    TCodigoOpcion.setText(CodigoOpcion + "");
	    

	    
	    //se valida la opcion finalizar actividad
	    
	    
	    appState = ((MyApp)getApplicationContext());   
	    db = appState.getDb();
	    db.open();
	    
	    Cursor CursorOpciones= null ;
	    CursorOpciones = db.getOpciones("Opcion=" + CodigoOpcion);
	    
	    if(CursorOpciones.getCount()>0){
	    	Opciones op = db.getOpcionesFromCursor(CursorOpciones, 0);
	    	finalizar = op.getFinalizaActividad();
	    }
	    
	    if(finalizar>0){
	    	//validacionesGroup.setVisibility(View.VISIBLE);
	    }
	    
	    if (Codigodepartamento == 1){
	    	
	    	
	    	
		    //Se verfica el input azucarera al persionar enter
		    empleado.setOnKeyListener(new View.OnKeyListener() {
	            public boolean onKey(View v, int keyCode, KeyEvent event) {
	                // If the event is a key-down event on the "enter" button
	                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
	                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
	                		Cursor CursorGuardados =	db.getDguardados("CodigoEmpleado=" + Integer.parseInt(empleado.getText().toString().trim())+ " AND ActividadFinalizada=0");
	                		if(CursorGuardados.getCount()>0){
	                			Dguardados dtemp =  (Dguardados)db.getDguardadosFromCursor(CursorGuardados, 0);
	                			
	                    		Calendar c = Calendar.getInstance(); 
	                    		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
	                    		String fecha = fdate.format(c.getTime());
	                    		SimpleDateFormat ftime = new SimpleDateFormat("HH:mm:ss");
	                    		String horaFin = ftime.format(c.getTime());
	                    		
	                    		
	                    		dtemp.setFecha(fecha);
	                			dtemp.setHoraFin(horaFin);
	                			dtemp.setActividadFinalizada(1);
	                			dtemp.setSupervisor(CodigoEmpleado);
	                			
	                			if(EditBancas.getText().toString().trim().length()>0)
	                				dtemp.setBancas(Integer.parseInt(EditBancas.getText().toString().trim()));

	                			db.updateDguardados(dtemp);

	                    		ObjectMapper mapper = new ObjectMapper();
	                    		try {
	                    			String nameJson = "V_" +dtemp.getCodigoEmpleado() + "_"+dtemp.getFecha() + "_"+dtemp.getHoraFin();
	                    			nameJson=nameJson.replace(":", "-");
									mapper.writeValue(new File( Environment.getExternalStorageDirectory()+"/Floricultura/JsonDescarga/" + nameJson+ ".json"), dtemp);
									
									JsonFiles archivo = new JsonFiles();
									archivo.setName(nameJson+ ".json");
									archivo.setNameFolder("Finalizados/");
									archivo.setUpload(0);
									db.insertarJsonFile(archivo);
									
									if(!appState.getSubiendoArchivos()){
										appState.setSubiendoArchivos(true);
										new UptoDropbox().execute(getApplicationContext());
									}
									
									
									
								} catch (JsonGenerationException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (JsonMappingException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
	                    		
	                    		
	                    		//ESTE CODIGO ERA PARA GENERAR LAS ETIQUETAS AL FINALIZAR AL USUARIO
	                    		/*
	                    		String query = "CodigoEmpleado=" + Integer.parseInt(empleado.getText().toString().trim()) 
	                    				+ " AND finalizado=0";
	                    		ArrayList<EtiquetasVegetativo> lista = new ArrayList<EtiquetasVegetativo>();
		                		Cursor etiquetas =	db.getEtiquetas(query);
		                		for(int i=0 ; i<etiquetas.getCount() ; i++ ){
		                			EtiquetasVegetativo etemp = db.getEtiquetasFromCursor(etiquetas, i);
		                			lista.add(etemp);
		                			etemp.setFinalizado(1);
		                			db.updateEtiquetas(etemp);
		                					
		                		}
		                	
		                		
		                		if(lista.size()>0){
		                    		mapper = new ObjectMapper();
		                    		try {
		                    			String nameJson = "E_" +dtemp.getCodigoEmpleado() + "_"+dtemp.getFecha() + "_"+dtemp.getHoraFin();
		                    			nameJson=nameJson.replace(":", "-");
										mapper.writeValue(new File( Environment.getExternalStorageDirectory()+"/Floricultura/JsonDescarga/" + nameJson+ ".json"), lista);
									} catch (JsonGenerationException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									} catch (JsonMappingException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									} catch (IOException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
		                			
		                		}
		                		*/
		                			
		                		empleado.setText("");
		                		EditBancas.setText("");
		                		//EditBancas.requestFocus();
		                		empleado.requestFocus();
	                    		
	                		}else{
	                			alertDialogMensaje("Invalido", "Usuario invalido");
	                    		empleado.setText("");
	                    		empleado.requestFocus();
	                		}
		                return true;
	                }
	                return false;
	            }
	        });
	    	
	    	
	    	
	    	
	    }else{
	    	
		    	
			    //Se verfica el input azucarera al persionar enter
			    empleado.setOnKeyListener(new View.OnKeyListener() {
		            public boolean onKey(View v, int keyCode, KeyEvent event) {
		                // If the event is a key-down event on the "enter" button
		                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
		                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
		                		Cursor CursorGuardados =	db.getDguardados("CodigoEmpleado=" + Integer.parseInt(empleado.getText().toString().trim())+ " AND ActividadFinalizada=0" );
		                		if(CursorGuardados.getCount()>0){
		                			Dguardados dtemp =  (Dguardados)db.getDguardadosFromCursor(CursorGuardados, 0);
		                			
		                    		Calendar c = Calendar.getInstance(); 
		                    		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
		                    		String fecha = fdate.format(c.getTime());
		                    		SimpleDateFormat ftime = new SimpleDateFormat("HH:mm:ss");
		                    		String horaFin = ftime.format(c.getTime());
		                    		
		                    		dtemp.setFecha(fecha);
		                			dtemp.setHoraFin(horaFin);
		                			dtemp.setSupervisor(CodigoEmpleado);
		                			dtemp.setActividadFinalizada(1);
		                			
		                			if(EditFlores.getText().toString().trim().length()>0)
		                				dtemp.setFlores( Integer.parseInt(EditFlores.getText().toString().trim()));	
		                			if(EditPlantas.getText().toString().trim().length()>0)
		                				dtemp.setPlantas(Integer.parseInt(EditPlantas.getText().toString().trim()));
		                			db.updateDguardados(dtemp);
		                			
		                			empleado.setText("");
		                			EditPlantas.setText("");
		                			EditFlores.setText("");
		                			//EditPlantas.requestFocus();
		                			empleado.requestFocus();
		                    		
		                    		ObjectMapper mapper = new ObjectMapper();
		                    		try {
		                    			String nameJson ="G_" + dtemp.getCodigoEmpleado() + "_"+dtemp.getFecha() + "_"+dtemp.getHoraFin();
		                    			nameJson=nameJson.replace(":", "-");
										mapper.writeValue(new File( Environment.getExternalStorageDirectory()+"/Floricultura/JsonDescarga/" + nameJson+ ".json"), dtemp);
										
										
										JsonFiles archivo = new JsonFiles();
										archivo.setName(nameJson+ ".json");
										archivo.setNameFolder("Finalizados/");
										archivo.setUpload(0);
										db.insertarJsonFile(archivo);
										
										if(!appState.getSubiendoArchivos()){
											appState.setSubiendoArchivos(true);
											new UptoDropbox().execute(getApplicationContext());
										}
										
										
									} catch (JsonGenerationException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									} catch (JsonMappingException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									} catch (IOException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
		                    		
		                		}else{
		                			alertDialogMensaje("Invalido", "Usuario invalido");
		                    		empleado.setText("");
		                    		empleado.requestFocus();
		                		}
			                return true;
		                }
		                return false;
		            }
		        });
			    	
	    }
	    
	    
	    //Se verfica el input azucarera al persionar enter
        EditBancas.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	// se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                		empleado.requestFocus();
	                return true;
                }
                return false;
            }
        });
        
	    //Se verfica el input azucarera al persionar enter
        EditPlantas.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	// se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                		EditFlores.requestFocus();
	                return true;
                }
                return false;
            }
        });
        
        
	    //Se verfica el input azucarera al persionar enter
        EditFlores.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	// se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                		empleado.requestFocus();
	                return true;
                }
                return false;
            }
        });
	    
	    
        btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
 
            	onBackPressed();
            }
        });
        
        
        btnListado.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Intent intent = new Intent(getApplicationContext(), ListadoActividad.class);
           	    intent.putExtra("CodigoEmpleado", CodigoEmpleado );
           	    startActivity(intent);
            	
            }
        });


        
	    EditBancas.setOnFocusChangeListener(new View.OnFocusChangeListener() {
	        @Override
	        public void onFocusChange(View v, boolean hasFocus) {
	        	Log.d("teclado","111111111111111111111");
	            if (hasFocus) {
	            	Log.d("teclado","22222222222222222");
	            	
	            	InputMethodManager imm = (InputMethodManager)
                            getSystemService(Context.INPUT_METHOD_SERVICE);
					if(imm != null){
					  imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
					}
	            	
	            	
	            	hideSoftKeyboard();
	            }
	        }
	    });
        

		
	}
	
	
	public void hideSoftKeyboard() {
	    if(getCurrentFocus()!=null) {
	        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
	        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),   InputMethodManager.HIDE_NOT_ALWAYS);
	    }
	}
	
	
	
	public void alertDialogMensaje(String message1, String mesage2){
		
		
		try {
		    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
		    ringtone.play();
		    
		    
		} catch (Exception e) {
		    e.printStackTrace();
		}
		
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(message1);
		alertDialog.setMessage(mesage2);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int which) {
		// here you can add functions
			ringtone.stop();
		}
		});
		alertDialog.show();
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.finalizar_actividad, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void ButtonOnClick(View v) {
	    switch (v.getId()) {
	      case R.id.button1:
		        agregarNumero("1");
	        break;
	      case R.id.button2:
	    	  agregarNumero("2");
	        break;
	      case R.id.button3:
		        agregarNumero("3");
	        break;
	      case R.id.button4:
	    	  agregarNumero("4");
	        break;
	      case R.id.button5:
		        agregarNumero("5");
	        break;
	      case R.id.button6:
	    	  agregarNumero("6");
	        break;
	      case R.id.button7:
	    	  agregarNumero("7");
	        break;
	      case R.id.button8:
		        agregarNumero("8");
	        break;
	      case R.id.button9:
	    	  agregarNumero("9");
	        break;
	      case R.id.button0:
		        agregarNumero("0");
	        break;
	      case R.id.button10:
	    	  agregarNumero("b");
	        break;
	      }
	     
	}
	
	
	public void agregarNumero(String n){
		
		if(n.contains("b")){
			
			if(EditBancas.isFocused()){
				EditBancas.setText("0");
			}else if(EditFlores.isFocused()){
				EditFlores.setText("0");
			}else if(EditPlantas.isFocused()){
				EditPlantas.setText("0");
			}
			
		}else{
			if(EditBancas.isFocused()){
				EditBancas.setText(EditBancas.getText() + n);
			}else if(EditFlores.isFocused()){
				EditFlores.setText(EditFlores.getText() + n);
			}else if(EditPlantas.isFocused()){
				EditPlantas.setText(EditPlantas.getText() + n);
			}
		}
		

	}



}
