package com.example.floricultura;

import java.util.ArrayList;

import com.example.floricultura.adapters.DatosLista;
import com.example.floricultura.adapters.ListAdapter;
import com.example.floricultura.data.DBAdapter;
import com.example.floricultura.data.Mempleado;
import com.example.floricultura.data.Modulos;
import com.example.floricultura.data.Usuario;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.os.Build;

public class ModuloActivity extends ActionBarActivity {
	
	private MyApp appState;	
	private DBAdapter db;
	ArrayList<DatosLista> listado = new ArrayList<DatosLista>();
	String id;
	int admin;
	int Codigodepartamento;
	int CodigoEmpleado;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_modulo);
		
		
	    appState = ((MyApp)getApplicationContext());   
	    db = appState.getDb();
	    db.open();
		
		Bundle extras = getIntent().getExtras();
	    admin = extras.getInt("Administrador");
	    CodigoEmpleado = extras.getInt("CodigoEmpleado");
	    Codigodepartamento=-1;
	    
	    //es admin si es igual a 1
	    if(admin == 1){
	    	Codigodepartamento = extras.getInt("Codigodepartamento");
	    }else if (admin == 0){
	    	//se selecciona al usuario logeado de la tabla mempleado para ver a que modulo pertenece	    	
	    	Cursor usuario = this.db.getUsuario("CodigoEmpleado=" + CodigoEmpleado);
	    	Usuario utemp = this.db.getUsuarioFromCursor(usuario, 0);
	    	Codigodepartamento = utemp.getCodigoDepto();

	    }

	    
	    ListView listadoVista = (ListView)findViewById(R.id.ListModulo);
	    
	    if(Codigodepartamento == -1){
	    	listado.add(new DatosLista("0", "No existe departamento para este usuario", ""));
	    	ListAdapter adaptador = new ListAdapter(this, R.layout.filas_lista, listado);
	        listadoVista.setAdapter(adaptador);
	    	
	    }else{

			listado = getDatos();
						
			ListAdapter adaptador = new ListAdapter(this, R.layout.filas_lista, listado);
	        listadoVista.setAdapter(adaptador);
			
	        
	        listadoVista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
	            @Override
	            public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {   
	            	DatosLista dato = (DatosLista) parent.getItemAtPosition(position);
	
	            		Intent intent = new Intent(getApplicationContext(), Opcion.class);
		           	 	intent.putExtra("Codigodepartamento", Codigodepartamento);
		           	    intent.putExtra("CodigoEmpleado", CodigoEmpleado );
		           	    intent.putExtra("CodigoModulo",  Integer.parseInt(dato.getCampo1().trim()));	
		           	 	startActivity(intent);
	            	
	            }
	        });
        
	    }
	    
	    
        Button btnBack = (Button) findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
 
            	onBackPressed();
            }
        });
        
        
        Button btnInicio = (Button) findViewById(R.id.btnInicio);
        btnInicio.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Intent intent = new Intent(getApplicationContext(), MainActivity.class);
           	 	startActivity(intent);
            }
        });
        

	}
	
	
	public ArrayList<DatosLista> getDatos(){
		ArrayList<DatosLista> listaTemp = new ArrayList<DatosLista>();
		listaTemp.add(new DatosLista("CD", "Descripcion", ""));
		

		 Cursor Cmodulos= null ;
		 Cmodulos = this.db.getModulos("CodigoDepto=" + Codigodepartamento);
		 
		 if(Cmodulos.getCount()>0){
			 for(int i=0 ; i<Cmodulos.getCount();i++){	         	
				 Modulos m = db.getModulosFromCursor(Cmodulos, i);  				 
				 listaTemp.add(new DatosLista(m.getCodigoModulo()+ "", m.getDescripcion(), ""));			 
		     } 
		 }

		
		
		return listaTemp;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.modulo, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}


}
