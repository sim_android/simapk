package com.example.floricultura;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import com.example.floricultura.data.DBAdapter;
import com.example.floricultura.data.Dguardados;
import com.example.floricultura.data.JsonFiles;
import com.example.floricultura.data.Semillas;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Vibrator;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.os.Build;

public class CapturaGeneral extends ActionBarActivity {
	
	//Variables globales
	private MyApp appState;	
	private DBAdapter db;
	int CodigoModulo;
	int Codigodepartamento;
	int CodigoEmpleado;
	int CodigoOpcion;
	int OpcionPolinizacion;
	int OpcionSuccion;
	int ValorPolinizacion=0;
	int ValorSuccion=0;
	int TotalEmpleadosAsignados;
	int OpcionCosecha;
	boolean validoDescriptivo;
	String NombreOpcion;
	LinearLayout validacionesGroup;
	TextView Material;
	TextView TCantidadAsignados;
	EditText azucar;
	EditText bote;
	EditText brocha;
	EditText descriptivo;
	EditText empleado;
	TextView TextviewAzucarera;
	TextView TextviewBrocha;
	TextView TextviewBote;
	Vibrator mVibrator;
	Ringtone ringtone;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_captura_general);
		
		
		//se obtienen lo datos de la actividad antetior por medio del intent
		Bundle extras = getIntent().getExtras();
		CodigoModulo = extras.getInt("CodigoModulo");
	    CodigoEmpleado = extras.getInt("CodigoEmpleado");
	    Codigodepartamento = extras.getInt("Codigodepartamento");
	    CodigoOpcion = extras.getInt("CodigoOpcion");
	    NombreOpcion = extras.getString("NombreOpcion");
	    OpcionPolinizacion = extras.getInt("OpcionPolinizacion");
	    OpcionCosecha= extras.getInt("OpcionCosecha");
	    
	    OpcionSuccion = extras.getInt("OpcionSuccion");
	    
	    
	    //hacemos referencia a todos los objetos de nuestra vista
	    TextView TNombreCaptura = (TextView)findViewById(R.id.OpcionCaptura);
	    TextView TCodigoEmpleado= (TextView)findViewById(R.id.CodigoEmpleado);
	    TextView TCodigoOpcion= (TextView)findViewById(R.id.codigoOpcion);
	    TextviewAzucarera= (TextView)findViewById(R.id.textViewAzucarera);
	    TextviewBrocha= (TextView)findViewById(R.id.textViewBrocha);
	    TextviewBote= (TextView)findViewById(R.id.textViewBote);
	    
	    
	    
	    TCantidadAsignados = (TextView)findViewById(R.id.textEmpleadosAsginados);
	    validacionesGroup = (LinearLayout)findViewById(R.id.layoutValidaciones);
	    descriptivo = (EditText)findViewById(R.id.editTextDescriptivo);
	    Material = (TextView)findViewById(R.id.textMaterial);
	    azucar  = (EditText)findViewById(R.id.editAzucar);
	    bote  = (EditText)findViewById(R.id.editBote);
	    brocha  = (EditText)findViewById(R.id.editBrocha);
	    empleado  = (EditText)findViewById(R.id.editEmpleado);
	    Button buttonNuevo = (Button) findViewById(R.id.btnNuevo);
        Button btnBack = (Button) findViewById(R.id.btnBack);
        Button btnInicio = (Button) findViewById(R.id.btnInicio);
	    
	    
	    //hacemos las inicializaciones necesarias a nuestra vista
	    TNombreCaptura.setText(NombreOpcion+ "");
	    TCodigoEmpleado.setText(CodigoEmpleado + "");
	    TCodigoOpcion.setText(CodigoOpcion + "");
	    validacionesGroup.setVisibility(View.INVISIBLE);
	    
	   
	    //incializamos la BD asi como tambien obtenemos la referencia al singleton
	    appState = ((MyApp)getApplicationContext());   
	    db = appState.getDb();
	    db.open();
	    

	    verificarAsginados();
	    

	    
	    //al recibir el enter se verifica si el descriptivo es correcto y lo manda al siguiente input sino le muestra un mensaje al usuario
        descriptivo.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                		VerificarDatos(descriptivo.getText().toString().trim());
	                	if (validoDescriptivo){
	                		if(OpcionCosecha == 1){
	                			azucar.requestFocus();
	                		}else if(ValorPolinizacion==1){
	                			azucar.requestFocus();
	                		}else if (OpcionSuccion==1){
	                			azucar.requestFocus();
	                		}else{
	                			empleado.requestFocus();
	                		}
	                		
	                	}else{
		    	    		alertDialogMensaje("Descriptivo", "Descriptivo Invalido");
		    	    		descriptivo.setText("");
		    	    		descriptivo.requestFocus();
	                	}
	                  return true;
                }
                return false;
            }
        });
        
        
	    //Se verfica el input azucarera al persionar enter
        azucar.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	// se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
            		String des = descriptivo.getText().toString().trim();
            		String az = azucar.getText().toString().trim();
            		if(des.equals(az)){
            			bote.requestFocus();
            			
            			if(OpcionCosecha == 1){
            				empleado.requestFocus();
            			}else{
            				bote.requestFocus();
            			}
            			
            			
            		}else{
            			
            			if(OpcionCosecha == 1){
            				alertDialogMensaje("Bolsa", "Valor de Bolsa es invalido");
            				//CODIGO AGREGADO
            			}else if (OpcionSuccion == 1){
            				alertDialogMensaje("Filtro", "Valor de Filtro es invalido");
            				//FIN CODIGO AGREGADO
            			}else{
            				alertDialogMensaje("Azucarera", "Valor de Azucarera es invalido");
            			}
            				
            				
            			azucar.setText("");
            			azucar.requestFocus();
            			

            		}
	                return true;
                }
                return false;
            }
        });
        
        
	    //Se verfica el input azucarera al persionar enter
        bote.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	// se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
            		String bot = bote.getText().toString().trim();
            		String mat = Material.getText().toString().trim();	
            		if(bot.equals(mat)){
            			brocha.requestFocus();
            			
            			//CODIGO AGREGADO
            			if(OpcionSuccion == 1){
            				empleado.requestFocus();
            			}else{
            				brocha.requestFocus();            				
            			}
            			//FIN CODIGO AGREGADO
            			
            		}else{
            			//CODIGO AGREGADO
            			if(OpcionSuccion == 1){
            				alertDialogMensaje("Pepeta", "Valor de Pipeta es invalido");
            			}else{
            				alertDialogMensaje("Bote", "Valor de Bote es invalido");
            			}
            			//alertDialogMensaje("Bote", "Valor de Bote es invalido");
            			bote.setText("");
            			bote.requestFocus();
            		}
	                return true;
                }
                return false;
            }
        });
        
        
	    //Se verfica el input azucarera al persionar enter
        brocha.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	// se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                	String bro = brocha.getText().toString().trim();
            		String mat = Material.getText().toString().trim();	
            		if(bro.equals(mat)){
            			empleado.requestFocus();
            		}else{
            			alertDialogMensaje("Brocha", "Valor de Brocha es invalido");
            			brocha.setText("");
            			brocha.requestFocus();
            		}
	                return true;
                }
                return false;
            }
        });
        
        
	    //Se verfica el input azucarera al persionar enter
        empleado.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	// se manda a llamar cuando se presione Enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                	
                	String empleadoEscaneado = empleado.getText().toString().trim();
                	int existe = VerificarEmpleado( empleadoEscaneado );
                	
                	if(existe == 2){
                		Dguardados dtemp = new Dguardados();
                		dtemp.setCodigoEmpleado(Integer.parseInt(empleadoEscaneado));
                		dtemp.setCodigoDepto(Codigodepartamento);
                		dtemp.setSiembra(Integer.parseInt(descriptivo.getText().toString().trim()));
                		
                		Calendar c = Calendar.getInstance(); 
                		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
                		String fecha = fdate.format(c.getTime());
                		SimpleDateFormat ftime = new SimpleDateFormat("HH:mm:ss");
                		String horaInicio = ftime.format(c.getTime());
                		
                		
                		//String fecha = c.get(Calendar.DAY_OF_MONTH) + "-" + (c.get(Calendar.MONTH)+1) + "-"+c.get(Calendar.YEAR);
                		dtemp.setFecha(fecha);
                		//String horaInicio = c.get(Calendar.HOUR) + ":" +c.get(Calendar.MINUTE) + ":"+c.get(Calendar.SECOND);
                		dtemp.setHoraIncio(horaInicio);
                		dtemp.setActividad(CodigoOpcion);
                		dtemp.setSupervisor(CodigoEmpleado);
                		dtemp.setActividadFinalizada(0);
                	
                		Log.d("Dguardados", dtemp.toString().trim());
                		
                		db.insertarDguardados(dtemp);
                		
                		//generacion archivo json
                		ObjectMapper mapper = new ObjectMapper();
                		try {
                			String nameJson ="G_" + dtemp.getCodigoEmpleado() + "_"+dtemp.getFecha() + "_"+dtemp.getHoraIncio();
                			nameJson=nameJson.replace(":", "-");	
							mapper.writeValue(new File( Environment.getExternalStorageDirectory()+"/Floricultura/JsonDescarga/" + nameJson+ ".json"), dtemp);
							
							JsonFiles archivo = new JsonFiles();
							archivo.setName(nameJson+ ".json");
							archivo.setNameFolder("SinFinalizar/");
							archivo.setUpload(0);
							db.insertarJsonFile(archivo);
							
							if(!appState.getSubiendoArchivos()){
								appState.setSubiendoArchivos(true);
								new UptoDropbox().execute(getApplicationContext());
							}
							
                		
                		} catch (JsonGenerationException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (JsonMappingException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
                		
                		
                		
                		empleado.setText("");
                		empleado.requestFocus();
                		
                		TotalEmpleadosAsignados++;
                		TCantidadAsignados.setText(TotalEmpleadosAsignados + "");
                		
                	}else if(existe == 1){
                		alertDialogMensaje("Asignado", "Este usuario ya ha sido asignado");
                		empleado.setText("");
                		empleado.requestFocus();
                	}else if(existe == 0){
                		alertDialogMensaje("Marcaje", "No existe marcaje para este usuario");
                		empleado.setText("");
                		empleado.requestFocus();
                	}
	                return true;
                }
                return false;
            }
        });
        
 	   /* 
	    //listener para verificar el campo descriptivo
	    descriptivo.addTextChangedListener(new TextWatcher() {

	           public void afterTextChanged(Editable s) {

	        	   	Log.d("TEXTOAPP", descriptivo.getText().toString().trim());
	        	   	VerificarDatos(descriptivo.getText().toString().trim());
	           }

	           public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

	           public void onTextChanged(CharSequence s, int start, int before, int count) {}
	        });
	    */
        
        
        
	    //se limpian los campos y variables para una nueva captura
	    buttonNuevo.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	        	validacionesGroup.setVisibility(View.INVISIBLE);
				 Material.setText(" ");
				 ValorPolinizacion=0;
				 validoDescriptivo = false;
				 descriptivo.setText("");
				 azucar.setText("");  
				 bote.setText("");  
				 brocha.setText("");  
				 empleado.setText(""); 
				 descriptivo.requestFocus();
	        }
	    });
        
        btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
 
            	onBackPressed();
            }
        });
        
        
        
        btnInicio.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Intent intent = new Intent(getApplicationContext(), MainActivity.class);
           	 	startActivity(intent);
            }
        });
        
        
	}
	
	
	public void verificarAsginados(){
		Cursor CursorGuardados =	db.getDguardados("Actividad=" + CodigoOpcion + " AND ActividadFinalizada=0" );
		TotalEmpleadosAsignados = CursorGuardados.getCount();
		TCantidadAsignados.setText(TotalEmpleadosAsignados + "");
		
	}
	

	//retornara un valor dependiendo del estado del empleado
	// 0 = no existe
	// 1 = existe pero esta asignado
	// 2 = existe y no esta asignado
	public int VerificarEmpleado(String empleado){
		int valor = 0;
		Cursor CursorDacceso =	db.getDacceso("CodigoEmpleado='" + empleado + "'");
		if(CursorDacceso != null  && CursorDacceso.getCount()>0){
			Cursor CursorGuardados =	db.getDguardados("CodigoEmpleado=" + empleado + " AND ActividadFinalizada=0");
			if(CursorGuardados.getCount()>0){
				valor = 1;
			}else{
				valor = 2;
			}
		}
		
		return valor;
	}
	
	public void alertDialogMensaje(String message1, String mesage2){
		
		//mVibrator.vibrate(300);
		
		try {
		    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
		    ringtone.play();
		    
		    
		} catch (Exception e) {
		    e.printStackTrace();
		}
		
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(message1);
		alertDialog.setMessage(mesage2);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int which) {
		// here you can add functions
			ringtone.stop();
		}
		});
		alertDialog.show();
	}
	
	
	public void VerificarDatos(String dato){	
		
		 Cursor CursorSemillas = db.getSemillas("Siembra='" + dato + "'");
		 if(CursorSemillas.getCount()>0){
			 Semillas s = db.getSemillasFromCursor(CursorSemillas, 0);
			 Log.d("SEMILLA",s.getSiembra() + ", "+s.getMaterial() + " ," + s.getPolinizacion());
			 
			 if(OpcionCosecha==1){
				 //si se entra a captura con la opcion de cosechas
				validacionesGroup.setVisibility(View.VISIBLE);		
				bote.setVisibility(View.INVISIBLE);
				brocha.setVisibility(View.INVISIBLE);
				TextviewAzucarera.setText("Bolsa");
				TextviewBrocha.setText("");;
				TextviewBote.setText("");;
				 
				//CODIGO AGREGADO
			 }else if(OpcionSuccion == 1){
				 validacionesGroup.setVisibility(View.VISIBLE);
				 TextviewAzucarera.setText("Filtro");
				 TextviewBote.setText("Pipeta");;
				 brocha.setVisibility(View.INVISIBLE);
				 TextviewBrocha.setText("");;
				 //FIN CODIGO AGREGADO
				 
				
			 }else if(s.getPolinizacion() == 1 && OpcionPolinizacion==1){
				 validacionesGroup.setVisibility(View.VISIBLE);				 
				 ValorPolinizacion=1;
			 }else{
				 validacionesGroup.setVisibility(View.INVISIBLE);
				 ValorPolinizacion=0;
			 }
			 validoDescriptivo = true;
			 Material.setText(s.getMaterial());
			 
		 }else{
			 validacionesGroup.setVisibility(View.INVISIBLE);
			 Material.setText(" ");
			 ValorPolinizacion=0;
			 validoDescriptivo = false;
		 }
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.captura_general, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}



}
