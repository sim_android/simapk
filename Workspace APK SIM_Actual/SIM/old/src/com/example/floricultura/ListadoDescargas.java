package com.example.floricultura;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.DropboxAPI.DropboxFileInfo;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.exception.DropboxException;
import com.example.floricultura.data.DBAdapter;
import com.example.floricultura.data.Dacceso;
import com.example.floricultura.data.Dempleado;
import com.example.floricultura.data.JSONReader;
import com.example.floricultura.data.Modulos;
import com.example.floricultura.data.Opciones;
import com.example.floricultura.data.Semillas;
import com.example.floricultura.data.Usuario;

import android.support.v7.app.ActionBarActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;


public class ListadoDescargas extends ActionBarActivity {
	
	
	private ProgressDialog loadingDialogDropbox;
	private final String DIR_APP = "/Floricultura/";
	DropboxAPI<AndroidAuthSession> mApi;
	private ProgressDialog loadingDialog;
	private MyApp appState;	
	private DBAdapter db;
	ArrayList<String> listadoJson = new ArrayList<String>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_listado_descargas);
		
		
        appState = ((MyApp)getApplicationContext());   
        db = appState.getDb();
        db.open();
        this.mApi = appState.getMAPI();
		
		
		
        Button btnBack = (Button) findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
 
            	onBackPressed();
            }
        });

        
        
        Button btnDescarga = (Button) findViewById(R.id.cargarDatos);
        btnDescarga.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	new DescargarDatos().execute();
            	
            }
        });

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.listado_descargas, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	
	public void onCheckboxClicked(View view) {
	    // Is the view now checked?
	    boolean checked = ((CheckBox) view).isChecked();
	    boolean contains = false;
	    // Check which checkbox was clicked
	    switch(view.getId()) {
	    
	        case R.id.checkBox1:
	        	contains = listadoJson.contains("D_Acceso.json");
	            if (checked){
	            	if (contains == false)
	            	listadoJson.add("D_Acceso.json");
	            }else{
	            	if (contains == true)
	            	listadoJson.remove("D_Acceso.json");
	            }
	            break;
	            
	        case R.id.checkBox2:	            
	        	contains = listadoJson.contains("D_Semillas.json");
	            if (checked){
	            	if (contains == false)
	            	listadoJson.add("D_Semillas.json");
	            }else{
	            	if (contains == true)
	            	listadoJson.remove("D_Semillas.json");
	            }
	            break;
	            
	        case R.id.checkBox3:
	        	contains = listadoJson.contains("Modulos.json");
	            if (checked){
	            	if (contains == false)
	            	listadoJson.add("Modulos.json");
	            }else{
	            	if (contains == true)
	            	listadoJson.remove("Modulos.json");
	            }
	            break;
	            
	        case R.id.checkBox4:
	        	contains = listadoJson.contains("Opciones.json");
	            if (checked){
	            	if (contains == false)
	            	listadoJson.add("Opciones.json");
	            }else{
	            	if (contains == true)
	            	listadoJson.remove("Opciones.json");
	            }
	            break;
	            
	        case R.id.checkBox5:
	        	contains = listadoJson.contains("Usuarios.json");
	            if (checked){
	            	if (contains == false)
	            	listadoJson.add("Usuarios.json");
	            }else{
	            	if (contains == true)
	            	listadoJson.remove("Usuarios.json");
	            }
	            break;
	            
	        case R.id.checkBox6:
	        	contains = listadoJson.contains("D_Vegetativo.json");
	            if (checked){
	            	if (contains == false)
	            	listadoJson.add("D_Vegetativo.json");
	            }else{
	            	if (contains == true)
	            	listadoJson.remove("D_Vegetativo.json");
	            }
	            break;
	            
	        case R.id.checkBox7:
	        	contains = listadoJson.contains("D_Empleados.json");
	            if (checked){
	            	if (contains == false)
	            	listadoJson.add("D_Empleados.json");
	            }else{
	            	if (contains == true)
	            	listadoJson.remove("D_Empleados.json");
	            }
	            break;

	    }
	}

	
    private class DescargarDatos extends AsyncTask<Void, Integer, Boolean> {   	
    	
        @Override
        protected void onPreExecute() {
        	loadingDialogDropbox = ProgressDialog.show(ListadoDescargas.this,"", "Descargando de Dropbox", true, false);
        }
    	 
        @Override 
        protected Boolean doInBackground(Void... params) {
        	
        	
        	for(int i =0;i<listadoJson.size();i++){
        			Log.d("descargando ", listadoJson.get(i) + "");
        		
		        	String nameJson =listadoJson.get(i);
		        	File file = new File( Environment.getExternalStorageDirectory()+"/Floricultura/JsonCarga/" + nameJson);
		        	FileOutputStream outputStream;
		        	String directorioDropbox = DIR_APP + "JsonCarga/" + nameJson;
					try {
						
						outputStream = new FileOutputStream(file);
			        	DropboxFileInfo info = mApi.getFile(directorioDropbox, null, outputStream, null);
			        	Log.i("DbExampleLog", "The file's rev is: " + info.getMetadata().rev);
						
						
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (DropboxException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
        	}    	
            return true;
        }
     

        @Override
        protected void onPostExecute(Boolean result) {
        	loadingDialogDropbox.dismiss();
        	new GuardarDatos().execute();
        }

    }
    
    
    
    public void InsertarDatosPrueba2(){
    	
    	JSONReader _reader = appState.getReader();	
    	for(int j =0;j<listadoJson.size();j++){
    		if(listadoJson.get(j).equals("D_Acceso.json")){
    			db.ReiniciarTabla(db.TIPO_DACCESO);
    	    	ArrayList<Dacceso> d = (ArrayList<Dacceso>) ( _reader.ReadFromSDCard(5, "Floricultura/JsonCarga/D_Acceso.json"));
    	    	for(int i =0; i<d.size() ; i++){
    	    		db.insertarDacceso(d.get(i));}
    		}else if(listadoJson.get(j).equals("D_Semillas.json")){
    			db.ReiniciarTabla(db.TIPO_SEMILLAS);
    	    	ArrayList<Semillas> s = (ArrayList<Semillas>) ( _reader.ReadFromSDCard(2, "Floricultura/JsonCarga/D_Semillas.json"));
    	    	for(int i =0; i<s.size() ; i++){
    	    		db.insertarSemillas(s.get(i));}
    		}else if(listadoJson.get(j).equals("Modulos.json")){
    			db.ReiniciarTabla(db.TIPO_MODULOS);
    	    	ArrayList<Modulos> m = (ArrayList<Modulos>) ( _reader.ReadFromSDCard(3, "Floricultura/JsonCarga/Modulos.json"));
    	    	for(int i =0; i<m.size() ; i++){
    	    		db.insertarModulos(m.get(i));}	
    		}else if(listadoJson.get(j).equals("Opciones.json")){
    			db.ReiniciarTabla(db.TIPO_OPCIONES);
    	    	ArrayList<Opciones> o = (ArrayList<Opciones>) ( _reader.ReadFromSDCard(4, "Floricultura/JsonCarga/Opciones.json"));
    	    	for(int i =0; i<o.size() ; i++){
    	    		db.insertarOpciones(o.get(i));}
    	    	
    		}else if(listadoJson.get(j).equals("Usuarios.json")){
    			db.ReiniciarTabla(db.TIPO_USUARIO);
    			ArrayList<Usuario> u = (ArrayList<Usuario>) ( _reader.ReadFromSDCard(1, "Floricultura/JsonCarga/Usuarios.json"));
    	    	for(int i =0; i<u.size() ; i++){
    	    		db.insertarUusario(u.get(i));}   		
    		}
    		else if(listadoJson.get(j).equals("D_Empleados.json")){
    			db.ReiniciarTabla(db.TIPO_DEMPLEADO);
    			ArrayList<Dempleado> d = (ArrayList<Dempleado>) ( _reader.ReadFromSDCard(7, "Floricultura/JsonCarga/D_Empleados.json"));
    	    	for(int i =0; i<d.size() ; i++){
    	    		db.insertDEmpleado(d.get(i));}   		
    		}
    	}
    	
    }
    

    
    
    private class GuardarDatos extends AsyncTask<Void, Integer, Boolean> {   	
    	
        @Override
        protected void onPreExecute() {
        	loadingDialog = ProgressDialog.show(ListadoDescargas.this,"", "Guardando al dispositivo", true, false);
        }
    	 
        @Override 
        protected Boolean doInBackground(Void... params) {
        	
        	InsertarDatosPrueba2();
        	
            return true;
        }
     

        @Override
        protected void onPostExecute(Boolean result) {
        	 loadingDialog.dismiss();
        	 appState.exportDatabse("floricultural.db");//exporta la base de datos a la SD card		
        	
        	 Intent i = new Intent(getApplicationContext(), MainActivity.class);
	         startActivity(i);
	         finish();
        }

    }
	


}
