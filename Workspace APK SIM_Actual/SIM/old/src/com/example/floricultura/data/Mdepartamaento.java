package com.example.floricultura.data;

public class Mdepartamaento {
	
	Integer CodigoDepto;
	String DescripcionDepto;
	String Estado;
	
	
	public Mdepartamaento() {
		CodigoDepto = 0;
		DescripcionDepto = "";
		Estado = "";
	}
	
	public Mdepartamaento(Integer codigoDepto, String descripcionDepto,
			String estado) {
		CodigoDepto = codigoDepto;
		DescripcionDepto = descripcionDepto;
		Estado = estado;
	}
	
	public Integer getCodigoDepto() {
		return CodigoDepto;
	}
	public void setCodigoDepto(Integer codigoDepto) {
		CodigoDepto = codigoDepto;
	}
	public String getDescripcionDepto() {
		return DescripcionDepto;
	}
	public void setDescripcionDepto(String descripcionDepto) {
		DescripcionDepto = descripcionDepto;
	}
	public String getEstado() {
		return Estado;
	}
	public void setEstado(String estado) {
		Estado = estado;
	}
	

	
	


}
