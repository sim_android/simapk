package com.example.floricultura.data;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import  com.example.floricultura.MyApp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DBAdapter {
	
	private SQLiteDatabase database;
	private DBHelper dbHelper;
	public static int TIPO_USUARIO = 1;
	public static int TIPO_MEMPLEADO = 2;
	public static int TIPO_MDEPARTAMENTO = 3;
	public static int TIPO_MODULOS = 4;
	public static int TIPO_OPCIONES = 5;
	public static int TIPO_SEMILLAS = 6;
	public static int TIPO_DACCESO = 7;
	public static int TIPO_DGUARDADOS = 8;
	public static int TIPO_ETIQUETAS = 9;
	public static int TIPO_JSONFILES = 10;
	public static int TIPO_DEMPLEADO = 11;
	
	public DBAdapter(Context context) {
		dbHelper = new DBHelper(context);
	}
	

	//abrir una conexion a la base de datos
	public synchronized void open() throws SQLException {
			try {
				database = dbHelper.getWritableDatabase();
			} catch (SQLiteException ex) {
				try {
					Log.e("error", "DB abierta RO " + ex.getMessage());
					database = dbHelper.getReadableDatabase();				
				} catch (SQLiteException ex1) {
					Log.e("error", "Imposible abrir la base de datos " + ex1.getMessage());
				}
			}			
	}
	
	public void ReinicarDB(){
		dbHelper.onUpgrade(database, 0, 1);
	}
	
	public void ReiniciarTabla(int tipoTabla){
		
		switch(tipoTabla){
		
		case 1:
			dbHelper.NuevaTabla(database, dbHelper.TABLE_USUARIO);
			break;
		case 2:
			dbHelper.NuevaTabla(database, dbHelper.TABLE_MEMPLEADO);
			break;
		case 3:
			dbHelper.NuevaTabla(database, dbHelper.TABLE_MEMDEPARTAMENTO);
			break;
		case 4:
			dbHelper.NuevaTabla(database, dbHelper.TABLE_MODULOS);
			break;
		case 5:
			dbHelper.NuevaTabla(database, dbHelper.TABLE_OPCIONES);
			break;
		case 6:
			dbHelper.NuevaTabla(database, dbHelper.TABLE_SEMILLAS);
			break;
		case 7:
			dbHelper.NuevaTabla(database, dbHelper.TABLE_DACCESO);
			break;
		case 8:
			dbHelper.NuevaTabla(database, dbHelper.TABLE_DGUARDADOS);
			break;
		case 9:
			dbHelper.NuevaTabla(database, dbHelper.TABLE_ETIQUETAS);
			break;
		case 10:
			dbHelper.NuevaTabla(database, dbHelper.TABLE_JSONFILES);
			break;
		case 11:
			dbHelper.NuevaTabla(database, dbHelper.TABLE_DEMPLEADO);
			break;
		default:
			break;
		}
		
	}
	
	//cerrar conexion a la base de datos
	public void close() {
		try {
			if (database.isOpen()) {
				database.close();
			}
			if (dbHelper != null) {
				dbHelper.close();	
			}
		} catch (Exception e){}
	}
	
	
	//************** Acciones a la BD para Elementos ********************
	//inserta objetos tipo Elementos a la BD
	public synchronized long insertarUusario(Usuario u) {	   		
		ContentValues newUsuario = buildFromObject(u,TIPO_USUARIO);
		
		if (!database.isOpen()) {
			open();
		}		
		long rowId = -1;
		try {
			rowId = database.insert(DBHelper.TABLE_USUARIO, null, newUsuario);
			Log.d("rowId", rowId + "");
		} catch (Exception e){
			Log.e("InsertErrorDBApder", e.toString().trim());
		}
		Log.d("return", rowId + "");
		return rowId;		
	}
	
	
	
	//modifica elementos en la BD
	public synchronized boolean updateElement(Usuario u) {
		ContentValues updatedUsuario = buildFromObject(u,this.TIPO_USUARIO);		
		if (!database.isOpen()) {
			open();
		}		
		boolean result = false;
		try {
			result = database.update(DBHelper.TABLE_USUARIO, updatedUsuario,  "CodigoEmpleado =" + u.getCodigoEmpleado(), null) > 0;
		} catch (Exception e){}
	    return result;
	}
	
	
	//elimina elementos en la BD
	public synchronized boolean removeUsario(Usuario u) {
		return removeElement(u.getCodigoEmpleado());
	}
	
	public synchronized boolean removeElement(int id) {
		if (!database.isOpen()) {
			this.open();
		}		
		boolean result = false;
		try {		
			result = database.delete(DBHelper.TABLE_USUARIO, "CodigoEmpleado =" + id, null) > 0;
		} catch (Exception e){}

		return result;
	}
	
	//retorna un curosr con elementos a partir de un where	
	public synchronized Cursor getUsuario(String whereClause) {
		if (!database.isOpen()) {
			this.open();
		}

		Cursor _data = new MatrixCursor(new String[]{""});
		try {
			_data = database.query(DBHelper.TABLE_USUARIO, getKeysFromClass(Usuario.class), 
								   whereClause, null, null, null, null);
		} catch (Exception e) {
			Log.e("ErrorIngetElements", e.toString().trim());
		}
			
		return _data;
	}
		
	
	//retorna un elemento a partir de un cursor
		public synchronized Usuario getUsuarioFromCursor (Cursor _c, int index) {	 
			Usuario e = (Usuario)getObjectFromCursor(_c,TIPO_USUARIO,index);
			_c.close();
			return e;
		}

		
		
//************** Acciones a la BD para Mempleado ********************
		//inserta objetos tipo Elementos a la BD
		public synchronized long insertarMempleado(Mempleado u) {	   		
			ContentValues newContent = buildFromObject(u,TIPO_MEMPLEADO);
			
			if (!database.isOpen()) {
				open();
			}		
			long rowId = -1;
			try {
				rowId = database.insert(DBHelper.TABLE_MEMPLEADO, null, newContent);
			} catch (Exception e){
			}
			Log.d("return", rowId + "");
			return rowId;		
		}
		
		//retorna un curosr con elementos a partir de un where	
		public synchronized Cursor getMempleado(String whereClause) {
			if (!database.isOpen()) {
				this.open();
			}

			Cursor _data = new MatrixCursor(new String[]{""});
			try {
				_data = database.query(DBHelper.TABLE_MEMPLEADO, getKeysFromClass(Mempleado.class), 
									   whereClause, null, null, null, null);
			} catch (Exception e) {
				Log.e("ErrorIngetElements", e.toString().trim());
			}
				
			return _data;
		}
			
		
		//retorna un elemento a partir de un cursor
			public synchronized Mempleado getMempleadoFromCursor (Cursor _c, int index) {	 
				Mempleado e = (Mempleado)getObjectFromCursor(_c,TIPO_MEMPLEADO,index);
				_c.close();
				return e;
			}
		
			
			
			
//************** Acciones a la BD para Memdepartemento ********************
			//inserta objetos tipo Elementos a la BD
			public synchronized long insertarMdepartamento(Mdepartamaento u) {	   		
				ContentValues newContent = buildFromObject(u,TIPO_MDEPARTAMENTO);
				
				if (!database.isOpen()) {
					open();
				}		
				long rowId = -1;
				try {
					rowId = database.insert(DBHelper.TABLE_MEMDEPARTAMENTO, null, newContent);
				} catch (Exception e){
				}
				Log.d("return", rowId + "");
				return rowId;		
			}
			
			//retorna un curosr con elementos a partir de un where	
			public synchronized Cursor getMdepartamento(String whereClause) {
				if (!database.isOpen()) {
					this.open();
				}

				Cursor _data = new MatrixCursor(new String[]{""});
				try {
					_data = database.query(DBHelper.TABLE_MEMDEPARTAMENTO, getKeysFromClass(Mdepartamaento.class), 
										   whereClause, null, null, null, null);
				} catch (Exception e) {
					Log.e("ErrorIngetElements", e.toString().trim());
				}
					
				return _data;
			}
				
			
			//retorna un elemento a partir de un cursor
				public synchronized Mdepartamaento getMdepartamentooFromCursor (Cursor _c, int index) {	 
					Mdepartamaento e = (Mdepartamaento)getObjectFromCursor(_c,TIPO_MDEPARTAMENTO,index);
					_c.close();
					return e;
				}
				
				
				
				
//************** Acciones a la BD para Modulos ********************
			//inserta objetos tipo Elementos a la BD
			public synchronized long insertarOpciones(Opciones u) {	   		
				ContentValues newContent = buildFromObject(u,TIPO_OPCIONES);
				
				if (!database.isOpen()) {
					open();
				}		
				long rowId = -1;
				try {
					rowId = database.insert(DBHelper.TABLE_OPCIONES, null, newContent);
				} catch (Exception e){
				}
				Log.d("return", rowId + "");
				return rowId;		
			}
			
			//retorna un curosr con elementos a partir de un where	
			public synchronized Cursor getOpciones(String whereClause) {
				if (!database.isOpen()) {
					this.open();
				}
	
				Cursor _data = new MatrixCursor(new String[]{""});
				try {
					_data = database.query(DBHelper.TABLE_OPCIONES, getKeysFromClass(Opciones.class), 
										   whereClause, null, null, null, null);
				} catch (Exception e) {
					Log.e("ErrorIngetElements", e.toString().trim());
				}
					
				return _data;
			}
				
			
			//retorna un elemento a partir de un cursor
				public synchronized Opciones getOpcionesFromCursor (Cursor _c, int index) {	 
					Opciones e = (Opciones)getObjectFromCursor(_c,TIPO_OPCIONES,index);
					_c.close();
					return e;
				}
						
			
				
				
				
//************** Acciones a la BD para Modulos ********************
			//inserta objetos tipo Elementos a la BD
			public synchronized long insertarModulos(Modulos u) {	   		
				ContentValues newContent = buildFromObject(u,TIPO_MODULOS);
				
				if (!database.isOpen()) {
					open();
				}		
				long rowId = -1;
				try {
					rowId = database.insert(DBHelper.TABLE_MODULOS, null, newContent);
				} catch (Exception e){
				}
				Log.d("return", rowId + "");
				return rowId;		
			}
			
			//retorna un curosr con elementos a partir de un where	
			public synchronized Cursor getModulos(String whereClause) {
				if (!database.isOpen()) {
					this.open();
				}
	
				Cursor _data = new MatrixCursor(new String[]{""});
				try {
					_data = database.query(DBHelper.TABLE_MODULOS, getKeysFromClass(Modulos.class), 
										   whereClause, null, null, null, null);
				} catch (Exception e) {
					Log.e("ErrorIngetElements", e.toString().trim());
				}
					
				return _data;
			}
				
			
			//retorna un elemento a partir de un cursor
				public synchronized Modulos getModulosFromCursor (Cursor _c, int index) {	 
					Modulos e = (Modulos)getObjectFromCursor(_c,TIPO_MODULOS,index);
					_c.close();
					return e;
				}
					
							
			
			
			
//************** Acciones a la BD para Semillas ********************
			//inserta objetos tipo Elementos a la BD
			public synchronized long insertarSemillas(Semillas u) {	   		
				ContentValues newContent = buildFromObject(u,TIPO_SEMILLAS);
				
				if (!database.isOpen()) {
					open();
				}		
				long rowId = -1;
				try {
					rowId = database.insert(DBHelper.TABLE_SEMILLAS, null, newContent);
				} catch (Exception e){
				}
				Log.d("return", rowId + "");
				return rowId;		
			}
			
			//retorna un curosr con elementos a partir de un where	
			public synchronized Cursor getSemillas(String whereClause) {
				if (!database.isOpen()) {
					this.open();
				}
	
				Cursor _data = new MatrixCursor(new String[]{""});
				try {
					_data = database.query(DBHelper.TABLE_SEMILLAS, getKeysFromClass(Semillas.class), 
										   whereClause, null, null, null, null);
				} catch (Exception e) {
					Log.e("ErrorIngetElements", e.toString().trim());
				}
					
				return _data;
			}
				
			
			//retorna un elemento a partir de un cursor
				public synchronized Semillas getSemillasFromCursor (Cursor _c, int index) {	 
					Semillas e = (Semillas)getObjectFromCursor(_c,TIPO_SEMILLAS,index);
					_c.close();
					return e;
				}
				
				
				
				
//************** Acciones a la BD para Dacceso ********************
				//inserta objetos tipo Elementos a la BD
				public synchronized long insertarDacceso(Dacceso u) {	   		
					ContentValues newContent = buildFromObject(u,TIPO_DACCESO);
					
					if (!database.isOpen()) {
						open();
					}		
					long rowId = -1;
					try {
						rowId = database.insert(DBHelper.TABLE_DACCESO, null, newContent);
					} catch (Exception e){
					}
					Log.d("return", rowId + "");
					return rowId;		
				}
				
				//retorna un curosr con elementos a partir de un where	
				public synchronized Cursor getDacceso(String whereClause) {
					if (!database.isOpen()) {
						this.open();
					}
		
					Cursor _data = new MatrixCursor(new String[]{""});
					try {
						_data = database.query(DBHelper.TABLE_DACCESO, getKeysFromClass(Dacceso.class), 
											   whereClause, null, null, null, null);
					} catch (Exception e) {
						Log.e("ErrorIngetElements", e.toString().trim());
					}
						
					return _data;
				}
					
				
				//retorna un elemento a partir de un cursor
					public synchronized Dacceso getDaccesoFromCursor (Cursor _c, int index) {	 
						Dacceso e = (Dacceso)getObjectFromCursor(_c,TIPO_DACCESO,index);
						_c.close();
						return e;
					}
					
					
					
//************** Acciones a la BD para Dacceso ********************
					//inserta objetos tipo Elementos a la BD
					public synchronized long insertarDguardados(Dguardados u) {	   		
						ContentValues newContent = buildFromObject(u,TIPO_DGUARDADOS);
						
						if (!database.isOpen()) {
							open();
						}		
						long rowId = -1;
						try {
							rowId = database.insert(DBHelper.TABLE_DGUARDADOS, null, newContent);
						} catch (Exception e){
						}
						Log.d("return", rowId + "");
						return rowId;		
					}
					
					//retorna un curosr con elementos a partir de un where	
					public synchronized Cursor getDguardados(String whereClause) {
						if (!database.isOpen()) {
							this.open();
						}
			
						Cursor _data = new MatrixCursor(new String[]{""});
						try {
							_data = database.query(DBHelper.TABLE_DGUARDADOS, getKeysFromClass(Dguardados.class), 
												   whereClause, null, null, null, null);
						} catch (Exception e) {
							Log.e("ErrorIngetElements", e.toString().trim());
						}
							
						return _data;
					}
						
					
					//retorna un elemento a partir de un cursor
						public synchronized Dguardados getDguardadosFromCursor (Cursor _c, int index) {	 
							Dguardados e = (Dguardados)getObjectFromCursor(_c,TIPO_DGUARDADOS,index);
							_c.close();
							return e;
						}
						
						//modifica elementos en la BD
						public synchronized boolean updateDguardados(Dguardados u) {
							ContentValues updatedUsuario = buildFromObject(u,this.TIPO_DGUARDADOS);		
							if (!database.isOpen()) {
								open();
							}		
							boolean result = false;
							try {
								result = database.update(DBHelper.TABLE_DGUARDADOS, updatedUsuario,  "CodigoEmpleado =" + u.getCodigoEmpleado(), null) > 0;
							} catch (Exception e){}
						    return result;
						}
					
						
						
				//************** Acciones a la BD para Dacceso ********************
				//inserta objetos tipo Elementos a la BD
				public synchronized long insertarJsonFile(JsonFiles u) {	   		
					ContentValues newContent = buildFromObject(u,TIPO_JSONFILES);
					
					if (!database.isOpen()) {
						open();
					}		
					long rowId = -1;
					try {
						rowId = database.insert(DBHelper.TABLE_JSONFILES, null, newContent);
					} catch (Exception e){
					}
					Log.d("return", rowId + "");
					return rowId;		
				}
				
				//retorna un curosr con elementos a partir de un where	
				public synchronized Cursor getJsonFiles(String whereClause) {
					if (!database.isOpen()) {
						this.open();
					}
		
					Cursor _data = new MatrixCursor(new String[]{""});
					try {
						_data = database.query(DBHelper.TABLE_JSONFILES, getKeysFromClass(JsonFiles.class), 
											   whereClause, null, null, null, null);
					} catch (Exception e) {
						Log.e("ErrorIngetElements", e.toString().trim());
					}
						
					return _data;
				}
					
				
				//retorna un elemento a partir de un cursor
					public synchronized JsonFiles getJsonFilesFromCursor (Cursor _c, int index) {	 
						JsonFiles e = (JsonFiles)getObjectFromCursor(_c,TIPO_JSONFILES,index);
						_c.close();
						return e;
					}
					
					//modifica elementos en la BD
					public synchronized boolean updateJsonFiles(JsonFiles u) {
						ContentValues updatedUsuario = buildFromObject(u,this.TIPO_JSONFILES);		
						if (!database.isOpen()) {
							open();
						}		
						boolean result = false;
						try {
							result = database.update(DBHelper.TABLE_JSONFILES, updatedUsuario,  "Name ='" + u.getName() + "'", null) > 0;
						} catch (Exception e){}
					    return result;
					}
			
					
					
					
					
					
					//************** Acciones a la BD para Dacceso ********************
					//inserta objetos tipo Elementos a la BD
					public synchronized long insertarEtiquetas(EtiquetasVegetativo u) {	   		
						ContentValues newContent = buildFromObject(u,TIPO_ETIQUETAS);
						
						if (!database.isOpen()) {
							open();
						}		
						long rowId = -1;
						try {
							rowId = database.insert(DBHelper.TABLE_ETIQUETAS, null, newContent);
						} catch (Exception e){
						}
						Log.d("return", rowId + "");
						return rowId;		
					}
					
					//retorna un curosr con elementos a partir de un where	
					public synchronized Cursor getEtiquetas(String whereClause) {
						if (!database.isOpen()) {
							this.open();
						}
			
						Cursor _data = new MatrixCursor(new String[]{""});
						try {
							_data = database.query(DBHelper.TABLE_ETIQUETAS, getKeysFromClass(EtiquetasVegetativo.class), 
												   whereClause, null, null, null, null);
						} catch (Exception e) {
							Log.e("ErrorIngetElements", e.toString().trim());
						}
							
						return _data;
					}
						
					
					//retorna un elemento a partir de un cursor
						public synchronized EtiquetasVegetativo getEtiquetasFromCursor (Cursor _c, int index) {	 
							EtiquetasVegetativo e = (EtiquetasVegetativo)getObjectFromCursor(_c,TIPO_ETIQUETAS,index);
							_c.close();
							return e;
						}
						
						//modifica elementos en la BD
						public synchronized boolean updateEtiquetas(EtiquetasVegetativo u) {
							ContentValues updatedUsuario = buildFromObject(u,this.TIPO_ETIQUETAS);		
							if (!database.isOpen()) {
								open();
							}		
							boolean result = false;
							try {
								result = database.update(DBHelper.TABLE_ETIQUETAS, updatedUsuario,  "CodigoEmpleado =" + u.getCodigoEmpleado(), null) > 0;
							} catch (Exception e){}
						    return result;
						}
							

						//agrege porfy
						//************** Acciones a la BD para Dacceso ********************
						//inserta objetos tipo Elementos a la BD
						public synchronized long insertDEmpleado(Dempleado d) {	   		
							ContentValues newContent = buildFromObject(d,TIPO_DEMPLEADO);
							
							if (!database.isOpen()) {
								open();
							}		
							long rowId = -1;
							try {
								rowId = database.insert(DBHelper.TABLE_DEMPLEADO, null, newContent);
							} catch (Exception e){
							}
							Log.d("return", rowId + "");
							return rowId;		
						}	
													
							
						//retorna un curosr con elementos a partir de un where	
						public synchronized Cursor getDempledo(String whereClause) {
							if (!database.isOpen()) {
								this.open();
							}
				
							Cursor _data = new MatrixCursor(new String[]{""});
							try {
								_data = database.query(DBHelper.TABLE_DEMPLEADO, getKeysFromClass(Dempleado.class), 
													   whereClause, null, null, null, null);
							} catch (Exception e) {
								Log.e("ErrorIngetElements", e.toString().trim());
							}
								
							return _data;
						}
							
						
						//retorna un elemento a partir de un cursor
							public synchronized Dempleado getDempleadoFromCursor (Cursor _c, int index) {	 
								Dempleado d = (Dempleado)getObjectFromCursor(_c,TIPO_DEMPLEADO,index);
								_c.close();
								return d;
							}
//Fin Porfy
							
							
							
							
							
							
						
						
						
						
			public synchronized void setLastUpdated(Date d) {
						SimpleDateFormat _format = new SimpleDateFormat("ddMMyyyy'T'HH:mm:ssZ");
						String str_date = _format.format(d);		
						ContentValues newElement = new ContentValues();
						newElement.put("last_updated",str_date);
						
						if (!database.isOpen()) {
							open();
						}
						
						Cursor _data = database.query( DBHelper.TABLE_INFO, null, "id = 1", null, null, null, "");
						try {
							if (_data.moveToFirst()) {		
								database.update(DBHelper.TABLE_INFO, newElement,  "id = 1", null);
							} else {
								database.insert(DBHelper.TABLE_INFO, null, newElement);
							}
						} catch (Exception e){}
						_data.close();
					}
			
	
			
			public synchronized Date getLastUpdated(){
				Date last_updated = null;
				
				if (!database.isOpen()) {
					this.open();
				}				
				Cursor _data = database.query( DBHelper.TABLE_INFO, new String[]{"last_updated"}, "id = 1", null, null, null, "");
				if (_data.moveToFirst()) {
					String str_last_updated = _data.getString(_data.getColumnIndex("last_updated"));
					_data.close();
					SimpleDateFormat _format = new SimpleDateFormat("ddMMyyyy'T'HH:mm:ssZ");
			    	try {
						last_updated = _format.parse(str_last_updated);
					} catch (Exception e) {}
				}
				return last_updated;
			}
	
	//elimina todas las tablas de la BD
	public synchronized void deleteAllElements() {
		if (!database.isOpen()) {
			open();
		}		
		try {
			database.delete(DBHelper.TABLE_USUARIO, null, null);


		} catch (Exception e) {}
	}
	
	public String[] getKeysFromClass(Class<?> cl) {
		ArrayList<String> keys = new ArrayList<String>();		
		Field[] fl = cl.getDeclaredFields();
		for (Field f : fl) {
			if (  f.getType().equals(String.class)
				||f.getType().equals(Bitmap.class)
				|| f.getType().equals(Integer.class)
				) {
				keys.add(f.getName());				
			}
		}
		return keys.toArray(new String[keys.size()]);
	}
	
	
	private ContentValues buildFromObject(Object Ob, int type) {		
	    ContentValues newElement = new ContentValues();
	    	    
		if(type == TIPO_USUARIO){	
			Usuario u = (Usuario)Ob;
			
			newElement.put("Usuario", u.getUsuario());
			newElement.put("Clave", u.getClave());
			newElement.put("Tipo", u.getTipo());
			newElement.put("CodigoEmpleado", u.getCodigoEmpleado());		
			newElement.put("CodigoDepto", u.getCodigoDepto());	
			
		}else if(type == TIPO_MEMPLEADO){	
			Mempleado u = (Mempleado)Ob;
			
			newElement.put("CodigoEmpleado", u.getCodigoEmpleado());
			newElement.put("CodigoDepto", u.getCodigoDepto());
	
		}else if(type == TIPO_MDEPARTAMENTO){
			Mdepartamaento u = (Mdepartamaento)Ob;	
			
			newElement.put("CodigoDepto", u.getCodigoDepto());
			newElement.put("DescripcionDepto", u.getDescripcionDepto());
			newElement.put("Estado", u.getEstado());
			
		}else if(type == TIPO_MODULOS){
			Modulos u = (Modulos)Ob;	
						
			newElement.put("CodigoDepto", u.getCodigoDepto());
			newElement.put("CodigoModulo", u.getCodigoModulo());
			newElement.put("Descripcion", u.getDescripcion());
			newElement.put("Prioridad", u.getPrioridad());
			
		}else if(type == TIPO_OPCIONES){
			Opciones u = (Opciones)Ob;	
			
			newElement.put("CodigoDepto", u.getCodigoDepto());
			newElement.put("Opcion", u.getOpcion());
			newElement.put("Descripcion", u.getDescripcion());
			newElement.put("Menu", u.getMenu());
			newElement.put("Prioridad", u.getPrioridad());
			newElement.put("FinalizaActividad", u.getFinalizaActividad());
			
			
		}else if(type == TIPO_SEMILLAS){
			Semillas u = (Semillas)Ob;
			
			newElement.put("Siembra", u.getSiembra());
			newElement.put("Material", u.getMaterial());
			newElement.put("Polinizacion", u.getPolinizacion());
			
		}else if(type == TIPO_DACCESO){
			Dacceso u = (Dacceso)Ob;

			newElement.put("Empresa", u.getEmpresa());
			newElement.put("CodigoEmpleado", u.getCodigoEmpleado());

		}else if(type == TIPO_DGUARDADOS){
			Dguardados u = (Dguardados)Ob;


			newElement.put("CodigoEmpleado", u.getCodigoEmpleado());
			newElement.put("CodigoDepto", u.getCodigoDepto());
			newElement.put("Actividad",   u.getActividad());
			newElement.put("Siembra",  u.getSiembra() );
			newElement.put("Fecha",  u.getFecha() );
			newElement.put("HoraIncio",  u.getHoraIncio() );
			newElement.put("HoraFin",   u.getHoraFin());
			newElement.put("Supervisor",   u.getSupervisor());
			newElement.put("ActividadFinalizada",   u.getActividadFinalizada());
			newElement.put("Invernadero",   u.getInvernadero());
			newElement.put("Plantas",   u.getPlantas());
			newElement.put("Flores",   u.getFlores());
			newElement.put("Bancas",   u.getBancas());
			
					

		}else if(type == TIPO_ETIQUETAS){
			EtiquetasVegetativo u = (EtiquetasVegetativo)Ob;
			newElement.put("CodigoEmpleado", u.getCodigoEmpleado());
			newElement.put("Etiqueta", u.getEtiqueta());
			newElement.put("Finalizado", u.getFinalizado());
			newElement.put("Invernadero", u.getInvernadero());
			newElement.put("Fecha", u.getFecha());
			newElement.put("Hora", u.getHora());

		}else if(type == TIPO_JSONFILES){
			JsonFiles u = (JsonFiles)Ob;
			newElement.put("Name", u.getName());
			newElement.put("Upload", u.getUpload());
			newElement.put("NameFolder", u.getNameFolder());
			
			

		}else if(type == TIPO_DEMPLEADO){
			Dempleado d = (Dempleado)Ob;
			newElement.put("Empresa", d.getEmpresa());
			newElement.put("CodigoEmpleado", d.getCodigoEmpleado());
			newElement.put("CodigoDepto", d.getCodigoDepto());
			newElement.put("Nombre", d.getNombre());
			newElement.put("Estado", d.getEstado());
			//newElement.put("Hora", u.getHora());

		}
		
			
		
		
		return newElement;		
	}
	

	//retorna un objeto a partir de un cursor
		public synchronized Object getObjectFromCursor (Cursor _c, int type, int index) {
			Object objecto= null;
			
			if (_c.moveToPosition(index)) {
				Class<?> cl =  getClass();
				
	   			 switch (type) {
				 	case 1:	
				 		    cl = Usuario.class;
				 		    objecto = new Usuario();
				 			break;
				 	case 2:	
			 		    cl = Mempleado.class;
			 		    objecto = new Mempleado();
			 			break;
				 	case 3:	
			 		    cl = Mdepartamaento.class;
			 		    objecto = new Mdepartamaento();
			 			break;
				 	case 4:	
			 		    cl = Modulos.class;
			 		    objecto = new Modulos();
			 			break;
				 	case 5:	
			 		    cl = Opciones.class;
			 		    objecto = new Opciones();
			 			break;
				 	case 6:	
			 		    cl = Semillas.class;
			 		    objecto = new Semillas();
			 			break;
				 	case 7:	
			 		    cl = Dacceso.class;
			 		    objecto = new Dacceso();
			 			break;
				 	case 8:	
			 		    cl = Dguardados.class;
			 		    objecto = new Dguardados();
			 			break;
				 	case 9:	
			 		    cl = EtiquetasVegetativo.class;
			 		    objecto = new EtiquetasVegetativo();
			 			break;
				 	case 10:	
			 		    cl = JsonFiles.class;
			 		    objecto = new JsonFiles();
			 			break;

				 	case 11:	
			 		    cl = Dempleado.class;
			 		    objecto = new Dempleado();
			 			break;
				 }
				

				Field[] fl = cl.getDeclaredFields();
				for (Field f : fl) {				
					try {
						String _name = f.getName();
						if (f.getType().equals(String.class)) {
							String _value = _c.getString(_c.getColumnIndex(_name));
							Method method = (objecto.getClass()).getMethod("set" + _name, String.class);
							method.invoke(objecto,_value);
						} else if (f.getType().equals(Integer.class)) {
							Integer _value = _c.getInt(_c.getColumnIndex(_name));
							Method method = (objecto.getClass()).getMethod("set" + _name, Integer.class);
							method.invoke(objecto,_value); 
										
						}
					} catch (SecurityException e) {
					} catch (NoSuchMethodException e) {
					} catch (IllegalArgumentException e) {
					} catch (IllegalAccessException e) {
					} catch (IllegalStateException e) {
					} catch (InvocationTargetException e) {						
					}					
				}
			}
			return objecto;
		}

	
	
	


}
