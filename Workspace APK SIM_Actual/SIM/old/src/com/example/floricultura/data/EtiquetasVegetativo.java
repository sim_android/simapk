package com.example.floricultura.data;

public class EtiquetasVegetativo {
	
	Integer CodigoEmpleado;
	String Etiqueta;
	String Invernadero;
	String Fecha;
	String Hora;
	Integer Finalizado;
	Integer Supervisor;
	Integer Actividad;
	
		
	
	public String getFecha() {
		return Fecha;
	}
	public void setFecha(String fecha) {
		Fecha = fecha;
	}
		
	public String getHora() {
		return Hora;
	}
	public void setHora(String hora) {
		Hora = hora;
	}
	
	
	public String getInvernadero() {
		return Invernadero;
	}
	public void setInvernadero(String invernadero) {
		Invernadero = invernadero;
	}
	

	public Integer getFinalizado() {
		return Finalizado;
	}
	public void setFinalizado(Integer finalizado) {
		this.Finalizado = finalizado;
	}
	public Integer getCodigoEmpleado() {
		return CodigoEmpleado;
	}
	public void setCodigoEmpleado(Integer codigoEmpleado) {
		CodigoEmpleado = codigoEmpleado;
	}
	public String getEtiqueta() {
		return Etiqueta;
	}
	public void setEtiqueta(String etiqueta) {
		this.Etiqueta = etiqueta;
	}
	
	
	public Integer getSupervisor() {
		return Supervisor;
	}
	public void setSupervisor(Integer supervisor) {
		Supervisor = supervisor;
	}
	public Integer getActividad() {
		return Actividad;
	}
	public void setActividad(Integer actividad) {
		Actividad = actividad;
	}
	

}
