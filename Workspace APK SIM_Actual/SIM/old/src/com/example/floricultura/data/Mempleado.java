package com.example.floricultura.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Mempleado {
	
	private Integer CodigoEmpleado;
	private Integer CodigoDepto;
	
	
	public Mempleado() {
		CodigoEmpleado = 0;
		CodigoDepto = 0;
	}
	
	
	public Mempleado(Integer codigoEmpleado, Integer codigoDepto) {
		CodigoEmpleado = codigoEmpleado;
		CodigoDepto = codigoDepto;
	}
	
	@JsonProperty("EMPRESA")
	public Integer getCodigoEmpleado() {
		return CodigoEmpleado;
	}
	public void setCodigoEmpleado(Integer codigoEmpleado) {
		CodigoEmpleado = codigoEmpleado;
	}
	

	public Integer getCodigoDepto() {
		return CodigoDepto;
	}
	public void setCodigoDepto(Integer codigoDepto) {
		CodigoDepto = codigoDepto;
	}
	

	
	
}
