package com.example.floricultura.data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;





import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;



public class JSONReader {

	private Context ctx;
	public final static int URL = 1;
	public final static int FILE = 2;
	
	public JSONReader(Context ctx){
		this.ctx = ctx;
	}
	
	/*
	public ArrayList ReadFromSDCard(int clase, String file){
		
		JSONArray _json = null;
		ArrayList _data = new ArrayList();
		try {
			
			_json = readJSONArrayFromInputStream((ctx.getAssets()).open(file));
			//Log.d("JSON:", _json.toString().trim());
			ObjectMapper mapper = new ObjectMapper();

			 switch (clase) {
			 	case 1:	_data = mapper.readValue(_json.toString().trim(), new TypeReference<ArrayList<Pantallas>>(){});
			 			break;
			 	case 2: _data = mapper.readValue(_json.toString().trim(), new TypeReference<ArrayList<Elemento>>(){});
	 					break;
			 	case 3: _data = mapper.readValue(_json.toString().trim(), new TypeReference<ArrayList<Phones>>(){});
					break;
			 	case 4: _data = mapper.readValue(_json.toString().trim(), new TypeReference<ArrayList<Comments>>(){});
					break;
			 	case 5: _data = mapper.readValue(_json.toString().trim(), new TypeReference<ArrayList<Place>>(){});
					break;
			 	case 6: _data = mapper.readValue(_json.toString().trim(), new TypeReference<ArrayList<Subcategories>>(){});
					break;
					
			 }

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return _data;
	}
	*/
	
	public ArrayList ReadFromSDCard(int clase, String file){
		
		JSONArray _json = null;
		ArrayList _data = new ArrayList();
				
		
		try {

			 File dir = Environment.getExternalStorageDirectory();
		     File yourFile = new File(dir,file);
		     
		     if(yourFile.exists())
		     {
		     
			     FileInputStream stream = new FileInputStream(yourFile);  
			     _json = readJSONArrayFromInputStream(stream);
				 ObjectMapper mapper = new ObjectMapper();
				 switch (clase) {
				 	case 1:	_data = mapper.readValue(_json.toString().trim(), new TypeReference<ArrayList<Usuario>>(){});
				 			break;		
				 	case 2:	_data = mapper.readValue(_json.toString().trim(), new TypeReference<ArrayList<Semillas>>(){});
		 					break;	
				 	case 3:	_data = mapper.readValue(_json.toString().trim(), new TypeReference<ArrayList<Modulos>>(){});
 							break;	
				 	case 4:	_data = mapper.readValue(_json.toString().trim(), new TypeReference<ArrayList<Opciones>>(){});
 							break;	
				 	case 5:	_data = mapper.readValue(_json.toString().trim(), new TypeReference<ArrayList<Dacceso>>(){});
 							break;	
				 	case 6:	_data = mapper.readValue(_json.toString().trim(), new TypeReference<ArrayList<RangoEtiqueta>>(){});
						break;
				 	case 7:	_data = mapper.readValue(_json.toString().trim(), new TypeReference<ArrayList<Dempleado>>(){});
					break;
				 }
				 stream.close();
		     }else{
		    	 Log.d("no existe", "no existe");
		     }

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		return _data;
		
	}
	
	
	
	public JSONArray readJSONArrayFromInputStream(InputStream is) {		
		String result="";
		JSONArray json = null;
				
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
	        StringBuilder sb = new StringBuilder();
	        String line = null;
	        while ((line = reader.readLine()) != null) {
	            sb.append(line + "\n");
	        }
	        is.close();
	        result=sb.toString().trim();
	    }catch(Exception e){
	        //Log.e("log_tag", "Error converting result "+e.toString().trim());
	    }
		
		try{	
			json = new JSONArray(result);			  
		}catch(JSONException e){
			//Log.e("log_tag", "Error parsing data " + e.toString().trim());
		}
		return json;		
		
	}
	
	
}
