package com.example.floricultura.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Opciones {
	
	Integer CodigoDepto;
	Integer Opcion;
	String Descripcion;
	String Menu;
	Integer Prioridad;
	Integer FinalizaActividad;
	
	
	


	public Opciones() {
		CodigoDepto = 0;
		Opcion = 0;
		Descripcion = "";
		Menu = "";
		Prioridad = 0;
		FinalizaActividad=0;
	}
	
	public Opciones(Integer codigoDepto, Integer opcion, String descripcion,
			String menu, Integer prioridad, Integer finalizaActividad) {
		CodigoDepto = codigoDepto;
		Opcion = opcion;
		Descripcion = descripcion;
		Menu = menu;
		Prioridad = prioridad;
		FinalizaActividad = finalizaActividad;
	}
	

	public Integer getFinalizaActividad() {
		return FinalizaActividad;
	}

	public void setFinalizaActividad(Integer finalizaActividad) {
		FinalizaActividad = finalizaActividad;
	}
	
	@JsonProperty("CODIGO_MODULO")
	public Integer getCodigoDepto() {
		return CodigoDepto;
	}
	
	public void setCodigoDepto(Integer codigoDepto) {
		CodigoDepto = codigoDepto;
	}
	
	@JsonProperty("OPCION")
	public Integer getOpcion() {
		return Opcion;
	}
	public void setOpcion(Integer opcion) {
		Opcion = opcion;
	}
	
	@JsonProperty("DESCRIPCION")
	public String getDescripcion() {
		return Descripcion;
	}
	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}
	
	@JsonProperty("MENU")
	public String getMenu() {
		return Menu;
	}
	public void setMenu(String menu) {
		Menu = menu;
	}
	
	@JsonProperty("PRIORIDAD")
	public Integer getPrioridad() {
		return Prioridad;
	}
	public void setPrioridad(Integer prioridad) {
		Prioridad = prioridad;
	}
	
	


}
