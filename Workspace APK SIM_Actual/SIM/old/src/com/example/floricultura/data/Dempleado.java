package com.example.floricultura.data;

import com.fasterxml.jackson.annotation.JsonProperty;



public class Dempleado {
	
	String Empresa;
	Integer CodigoEmpleado;
	Integer CodigoDepto;
	String Nombre;
	String Estado;
	

	
	public String getEmpresa() {
		return Empresa;
	}
	public void setEmpresa(String empresa) {
		Empresa = empresa;
	}
	
	public Integer getCodigoEmpleado() {
		return CodigoEmpleado;
	}
	public void setCodigoEmpleado(Integer codigoempleado) {
		CodigoEmpleado = codigoempleado;
	}
	
	
	public Integer getCodigoDepto() {
		return CodigoDepto;
	}
	public void setCodigoDepto(Integer codigoDepto) {
		CodigoDepto = codigoDepto;
	}
	
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	
	public String getEstado() {
		return Estado;
	}
	public void setEstado(String estado) {
		Estado = estado;
	}
	
}
