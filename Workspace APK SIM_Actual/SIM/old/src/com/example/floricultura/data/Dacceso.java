package com.example.floricultura.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Dacceso {
	
	
	private String Empresa;
	private String CodigoEmpleado;
	
	@JsonProperty("EMPRESA")
	public String getEmpresa() {
		return Empresa;
	}
	public void setEmpresa(String empresa) {
		Empresa = empresa;
	}
	
	@JsonProperty("CODIGOEMPLEADO")
	public String getCodigoEmpleado() {
		return CodigoEmpleado;
	}
	public void setCodigoEmpleado(String codigoEmpleado) {
		CodigoEmpleado = codigoEmpleado;
	}
	
	

}
