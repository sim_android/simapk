package com.example.floricultura.data;

public class Dguardados {
	

	Integer CodigoEmpleado;
	Integer CodigoDepto;
	Integer Actividad;
	Integer Siembra;
	String Fecha;
	String HoraIncio;
	String HoraFin;
	Integer Supervisor;
	Integer ActividadFinalizada;
	String Invernadero;
	Integer Plantas;
	Integer Flores;
	Integer Bancas;
	
	
	public Integer getPlantas() {
		return Plantas;
	}
	public void setPlantas(Integer plantas) {
		Plantas = plantas;
	}
	
	public Integer getFlores() {
		return Flores;
	}
	public void setFlores(Integer flores) {
		Flores = flores;
	}
	
	public Integer getBancas() {
		return Bancas;
	}
	public void setBancas(Integer bancas) {
		Bancas = bancas;
	}
	
	
	public String getInvernadero() {
		return Invernadero;
	}
	public void setInvernadero(String invernadero) {
		Invernadero = invernadero;
	}
	public Integer getActividadFinalizada() {
		return ActividadFinalizada;
	}
	public void setActividadFinalizada(Integer actividadFinalizada) {
		ActividadFinalizada = actividadFinalizada;
	}
	public Integer getSupervisor() {
		return Supervisor;
	}
	public void setSupervisor(Integer supervisor) {
		Supervisor = supervisor;
	}
	public Integer getCodigoEmpleado() {
		return CodigoEmpleado;
	}
	public void setCodigoEmpleado(Integer codigoEmpleado) {
		CodigoEmpleado = codigoEmpleado;
	}
	public Integer getCodigoDepto() {
		return CodigoDepto;
	}
	public void setCodigoDepto(Integer codigoDepto) {
		CodigoDepto = codigoDepto;
	}
	public Integer getActividad() {
		return Actividad;
	}
	public void setActividad(Integer actividad) {
		Actividad = actividad;
	}
	public Integer getSiembra() {
		return Siembra;
	}
	public void setSiembra(Integer siembra) {
		Siembra = siembra;
	}
	public String getFecha() {
		return Fecha;
	}
	public void setFecha(String fecha) {
		this.Fecha = fecha;
	}
	public String getHoraIncio() {
		return HoraIncio;
	}
	public void setHoraIncio(String horaIncio) {
		HoraIncio = horaIncio;
	}
	public String getHoraFin() {
		return HoraFin;
	}
	public void setHoraFin(String horaFin) {
		HoraFin = horaFin;
	}
	
	public String toString(){
		String s ="";
				
		s = this.CodigoEmpleado + "\n" + this.CodigoDepto + "\n" + this.Actividad + "\n" + this.Siembra  + "\n" + this.Fecha
				+ "\n" + this.HoraIncio + "\n" + this.HoraFin;
		
		return s;
	}
	

}
