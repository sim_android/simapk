package com.example.floricultura.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Modulos {
	
	Integer CodigoDepto;
	Integer CodigoModulo;
	String Descripcion;
	Integer Prioridad;
	
	
	
	public Modulos(Integer codigoDepto, Integer codigoModulo,
			String descripcion, Integer prioridad) {
		CodigoDepto = codigoDepto;
		CodigoModulo = codigoModulo;
		Descripcion = descripcion;
		Prioridad = prioridad;
	}
	
	public Modulos() {

		CodigoDepto = 0;
		CodigoModulo = 0;
		Descripcion = "";
		Prioridad = 0;
	}
	
	
	
	@JsonProperty("CODIGO_DEPTO")
	public Integer getCodigoDepto() {
		return CodigoDepto;
	}
	public void setCodigoDepto(Integer codigoDepto) {
		CodigoDepto = codigoDepto;
	}
	
	@JsonProperty("CODIGO_MODULO")
	public Integer getCodigoModulo() {
		return CodigoModulo;
	}
	public void setCodigoModulo(Integer codigoModulo) {
		CodigoModulo = codigoModulo;
	}
	
	@JsonProperty("DESCRIPCION")
	public String getDescripcion() {
		return Descripcion;
	}
	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}
	
	@JsonProperty("PRIORIDAD")
	public Integer getPrioridad() {
		return Prioridad;
	}
	public void setPrioridad(Integer prioridad) {
		Prioridad = prioridad;
	}
	

	

}
