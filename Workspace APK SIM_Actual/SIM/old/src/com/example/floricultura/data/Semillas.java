package com.example.floricultura.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Semillas {
	
	String Siembra;
	String Material;
	Integer Polinizacion;
	
	
	
	public Semillas() {
		Siembra = "";
		Material = "";
		Polinizacion = 0;
	}
	
	public Semillas(String siembra, String material, Integer polinizacion) {
		Siembra = siembra;
		Material = material;
		Polinizacion = polinizacion;
	}
	
	@JsonProperty("SIEMBRA")
	public String getSiembra() {
		return Siembra;
	}
	public void setSiembra(String siembra) {
		Siembra = siembra;
	}
	
	@JsonProperty("MATERIAL")
	public String getMaterial() {
		return Material;
	}
	public void setMaterial(String material) {
		Material = material;
	}
	
	@JsonProperty("POLINIZACION")
	public Integer getPolinizacion() {
		return Polinizacion;
	}
	public void setPolinizacion(Integer polinizacion) {
		Polinizacion = polinizacion;
	}
	
	

}
