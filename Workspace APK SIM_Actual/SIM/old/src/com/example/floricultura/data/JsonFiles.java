package com.example.floricultura.data;

public class JsonFiles {
	
	String Name;
	String NameFolder;
	Integer Upload;
	
	
	public String getNameFolder() {
		return NameFolder;
	}
	public void setNameFolder(String nameFolder) {
		NameFolder = nameFolder;
	}
	
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		this.Name = name;
	}
	public Integer getUpload() {
		return Upload;
	}
	public void setUpload(Integer upload) {
		this.Upload = upload;
	}
	
	

}
