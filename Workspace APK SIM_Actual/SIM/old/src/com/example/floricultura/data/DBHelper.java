package com.example.floricultura.data;

import java.lang.reflect.Field;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.util.Log;


class DBHelper extends SQLiteOpenHelper {
	
	private static final int DATABASE_VERSION = 1;
	
	static final String TABLE_USUARIO = "usuario";
	static final String TABLE_MEMPLEADO = "mempleado";
	static final String TABLE_MEMDEPARTAMENTO = "memdepartamento";
	static final String TABLE_MODULOS = "modulos";
	static final String TABLE_OPCIONES = "opciones";
	static final String TABLE_SEMILLAS = "semillas";
	static final String TABLE_DGUARDADOS = "dguardados";
	static final String TABLE_DACCESO = "dacceso";
	static final String TABLE_ETIQUETAS = "etiquetavegetatvio";
	static final String TABLE_JSONFILES = "jsonfiles";
	static final String TABLE_INFO = "info";
	static final String TABLE_DEMPLEADO = "dempleado";
	private static final String DATABASE_NAME = "floricultural.db";	
				
	public DBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	public DBHelper(Context context, int version) {
		super(context, DATABASE_NAME, null, version);
	}
	
	@Override
	public void onCreate(SQLiteDatabase database) {
		
		/*
		String usuario_sql = "";
		String mempleado_sql = "";
		String mdepartamento_sql = "";
		String modulos_sql = "";
		String opciones_sql = "";
		String semillas_sql = "";
		*/
		
		String usuario_sql = getSqlQueryText(Usuario.class);
		String mempleado_sql = getSqlQueryText(Mempleado.class);
		String mdepartamento_sql = getSqlQueryText(Mdepartamaento.class);
		String modulos_sql = getSqlQueryText(Modulos.class);
		String opciones_sql = getSqlQueryText(Opciones.class);
		String semillas_sql = getSqlQueryText(Semillas.class);
		String dacceso_sql = getSqlQueryText(Dacceso.class);
		String dguardados_sql = getSqlQueryText(Dguardados.class);
		String etiquetas_sql = getSqlQueryText(EtiquetasVegetativo.class);
		String jsonfiles_sql = getSqlQueryText(JsonFiles.class);
		String dempleado_sql = getSqlQueryText(Dempleado.class);
						
		
		usuario_sql = "CREATE TABLE " + TABLE_USUARIO + " (" + usuario_sql.substring(0, usuario_sql.length()-1) + ")";
		mempleado_sql = "CREATE TABLE " + TABLE_MEMPLEADO + " (" + mempleado_sql.substring(0, mempleado_sql.length()-1) + ")";
		mdepartamento_sql = "CREATE TABLE " + TABLE_MEMDEPARTAMENTO + " (" + mdepartamento_sql.substring(0, mdepartamento_sql.length()-1) + ")";
		modulos_sql = "CREATE TABLE " + TABLE_MODULOS + " (" + modulos_sql.substring(0, modulos_sql.length()-1) + ")";
		opciones_sql = "CREATE TABLE " + TABLE_OPCIONES + " (" + opciones_sql.substring(0, opciones_sql.length()-1) + ")";
		semillas_sql = "CREATE TABLE " + TABLE_SEMILLAS + " (" + semillas_sql.substring(0, semillas_sql.length()-1) + ")";
		dguardados_sql = "CREATE TABLE " + TABLE_DGUARDADOS + " (" + dguardados_sql.substring(0, dguardados_sql.length()-1) + ")";
		dacceso_sql = "CREATE TABLE " + TABLE_DACCESO + " (" + dacceso_sql.substring(0, dacceso_sql.length()-1) + ")";
		etiquetas_sql = "CREATE TABLE " + TABLE_ETIQUETAS + " (" + etiquetas_sql.substring(0, etiquetas_sql.length()-1) + ")";
		jsonfiles_sql = "CREATE TABLE " + TABLE_JSONFILES + " (" + jsonfiles_sql.substring(0, jsonfiles_sql.length()-1) + ")";
		String info_sql = "CREATE TABLE " + TABLE_INFO + " (id integer primary key, last_updated text, check_count integer, agencia integer, producto integer, promocion integer, carrera integer)";
		dempleado_sql = "CREATE TABLE " + TABLE_DEMPLEADO + " (" + dempleado_sql.substring(0, dempleado_sql.length()-1) + ")";
		
		database.execSQL(usuario_sql);
		database.execSQL(mempleado_sql);
		database.execSQL(mdepartamento_sql);
		database.execSQL(modulos_sql);
		database.execSQL(opciones_sql);
		database.execSQL(info_sql);
		database.execSQL(semillas_sql);
		database.execSQL(dguardados_sql);
		database.execSQL(dacceso_sql);
		database.execSQL(etiquetas_sql);
		database.execSQL(jsonfiles_sql);
		database.execSQL(dempleado_sql);

		
		Log.d("se han creado las tablas", "se han creado las tablas");
		Log.d("tablaUsuario", usuario_sql);
	}

	
	public String getSqlQueryText( Class<?> clase)
	{
		String query="";
		Field[] fl = clase.getDeclaredFields();
		for (Field f : fl) {
			try {
				String _name = f.getName();
				if (f.getType().equals(Bitmap.class)) {
					query += _name + " blob,";
				} else if (f.getType().equals(Integer.class)) {
					query += _name + " integer,";
				} else if (f.getType().equals(String.class)) {
					if (_name.equals("id")) {
						query += "id integer primary key, type integer,";	
					} else {
						query += _name + " text,";	
					}						
				}
			} catch (SecurityException e) {
			} catch (IllegalArgumentException e) {						
			}
		}
		
		return query;
	}
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(DBHelper.class.getName(),
				"Upgrading database from version " + oldVersion + " to "
						+ newVersion + ", which will destroy all old data");
				
					
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USUARIO);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_MEMPLEADO);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_MEMDEPARTAMENTO);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_MODULOS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_OPCIONES);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SEMILLAS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_INFO);
		//db.execSQL("DROP TABLE IF EXISTS " + TABLE_DGUARDADOS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_DACCESO);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ETIQUETAS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_JSONFILES);
		
		
		restaurarTablas(db);

	}
	
	
	public void restaurarTablas(SQLiteDatabase database) {
		
		String usuario_sql = getSqlQueryText(Usuario.class);
		String mempleado_sql = getSqlQueryText(Mempleado.class);
		String mdepartamento_sql = getSqlQueryText(Mdepartamaento.class);
		String modulos_sql = getSqlQueryText(Modulos.class);
		String opciones_sql = getSqlQueryText(Opciones.class);
		String semillas_sql = getSqlQueryText(Semillas.class);
		String dacceso_sql = getSqlQueryText(Dacceso.class);
		//String dguardados_sql = getSqlQueryText(Dguardados.class);
		String etiquetas_sql = getSqlQueryText(EtiquetasVegetativo.class);
		String jsonfiles_sql = getSqlQueryText(JsonFiles.class);
						
		usuario_sql = "CREATE TABLE " + TABLE_USUARIO + " (" + usuario_sql.substring(0, usuario_sql.length()-1) + ")";
		mempleado_sql = "CREATE TABLE " + TABLE_MEMPLEADO + " (" + mempleado_sql.substring(0, mempleado_sql.length()-1) + ")";
		mdepartamento_sql = "CREATE TABLE " + TABLE_MEMDEPARTAMENTO + " (" + mdepartamento_sql.substring(0, mdepartamento_sql.length()-1) + ")";
		modulos_sql = "CREATE TABLE " + TABLE_MODULOS + " (" + modulos_sql.substring(0, modulos_sql.length()-1) + ")";
		opciones_sql = "CREATE TABLE " + TABLE_OPCIONES + " (" + opciones_sql.substring(0, opciones_sql.length()-1) + ")";
		semillas_sql = "CREATE TABLE " + TABLE_SEMILLAS + " (" + semillas_sql.substring(0, semillas_sql.length()-1) + ")";
		//dguardados_sql = "CREATE TABLE " + TABLE_DGUARDADOS + " (" + dguardados_sql.substring(0, dguardados_sql.length()-1) + ")";
		dacceso_sql = "CREATE TABLE " + TABLE_DACCESO + " (" + dacceso_sql.substring(0, dacceso_sql.length()-1) + ")";
		etiquetas_sql = "CREATE TABLE " + TABLE_ETIQUETAS + " (" + etiquetas_sql.substring(0, etiquetas_sql.length()-1) + ")";
		jsonfiles_sql = "CREATE TABLE " + TABLE_JSONFILES + " (" + jsonfiles_sql.substring(0, jsonfiles_sql.length()-1) + ")";
		String info_sql = "CREATE TABLE " + TABLE_INFO + " (id integer primary key, last_updated text, check_count integer, agencia integer, producto integer, promocion integer, carrera integer)"; 
		
		database.execSQL(usuario_sql);
		database.execSQL(mempleado_sql);
		database.execSQL(mdepartamento_sql);
		database.execSQL(modulos_sql);
		database.execSQL(opciones_sql);
		database.execSQL(info_sql);
		database.execSQL(semillas_sql);
		//database.execSQL(dguardados_sql);
		database.execSQL(dacceso_sql);
		database.execSQL(etiquetas_sql);
		database.execSQL(jsonfiles_sql);

		
		Log.d("se han creado las tablas", "se han creado las tablas");
		Log.d("tablaUsuario", usuario_sql);
		
	}
	
	
	public void NuevaTabla(SQLiteDatabase database , String tabla){
		
		if(tabla.equals(TABLE_USUARIO)){
			database.execSQL("DROP TABLE IF EXISTS " + TABLE_USUARIO);
			String usuario_sql = getSqlQueryText(Usuario.class);
			usuario_sql = "CREATE TABLE " + TABLE_USUARIO + " (" + usuario_sql.substring(0, usuario_sql.length()-1) + ")";
			database.execSQL(usuario_sql);
		}else if(tabla.equals(TABLE_MEMPLEADO)){
			database.execSQL("DROP TABLE IF EXISTS " + TABLE_MEMPLEADO);
			String mempleado_sql = getSqlQueryText(Mempleado.class);
			mempleado_sql = "CREATE TABLE " + TABLE_MEMPLEADO + " (" + mempleado_sql.substring(0, mempleado_sql.length()-1) + ")";
			database.execSQL(mempleado_sql);
		}else if(tabla.equals(TABLE_MEMDEPARTAMENTO)){
			database.execSQL("DROP TABLE IF EXISTS " + TABLE_MEMDEPARTAMENTO);
			String mdepartamento_sql = getSqlQueryText(Mdepartamaento.class);
			mdepartamento_sql = "CREATE TABLE " + TABLE_MEMDEPARTAMENTO + " (" + mdepartamento_sql.substring(0, mdepartamento_sql.length()-1) + ")";
			database.execSQL(mdepartamento_sql);
		}else if(tabla.equals(TABLE_MODULOS)){
			database.execSQL("DROP TABLE IF EXISTS " + TABLE_MODULOS);
			String modulos_sql = getSqlQueryText(Modulos.class);
			modulos_sql = "CREATE TABLE " + TABLE_MODULOS + " (" + modulos_sql.substring(0, modulos_sql.length()-1) + ")";
			database.execSQL(modulos_sql);			
		}else if(tabla.equals(TABLE_OPCIONES)){
			database.execSQL("DROP TABLE IF EXISTS " + TABLE_OPCIONES);
			String opciones_sql = getSqlQueryText(Opciones.class);
			opciones_sql = "CREATE TABLE " + TABLE_OPCIONES + " (" + opciones_sql.substring(0, opciones_sql.length()-1) + ")";
			database.execSQL(opciones_sql);
		}else if(tabla.equals(TABLE_SEMILLAS)){
			database.execSQL("DROP TABLE IF EXISTS " + TABLE_SEMILLAS);
			String semillas_sql = getSqlQueryText(Semillas.class);
			semillas_sql = "CREATE TABLE " + TABLE_SEMILLAS + " (" + semillas_sql.substring(0, semillas_sql.length()-1) + ")";
			database.execSQL(semillas_sql);
		}else if(tabla.equals(TABLE_DGUARDADOS)){
			database.execSQL("DROP TABLE IF EXISTS " + TABLE_DGUARDADOS);
			String dguardados_sql = getSqlQueryText(Dguardados.class);
			dguardados_sql = "CREATE TABLE " + TABLE_DGUARDADOS + " (" + dguardados_sql.substring(0, dguardados_sql.length()-1) + ")";
			database.execSQL(dguardados_sql);	
		}else if(tabla.equals(TABLE_DACCESO)){
			database.execSQL("DROP TABLE IF EXISTS " + TABLE_DACCESO);
			String dacceso_sql = getSqlQueryText(Dacceso.class);
			dacceso_sql = "CREATE TABLE " + TABLE_DACCESO + " (" + dacceso_sql.substring(0, dacceso_sql.length()-1) + ")";
			database.execSQL(dacceso_sql);
		}else if(tabla.equals(TABLE_ETIQUETAS)){
			database.execSQL("DROP TABLE IF EXISTS " + TABLE_ETIQUETAS);
			String etiquetas_sql = getSqlQueryText(EtiquetasVegetativo.class);
			etiquetas_sql = "CREATE TABLE " + TABLE_ETIQUETAS + " (" + etiquetas_sql.substring(0, etiquetas_sql.length()-1) + ")";
			database.execSQL(etiquetas_sql);
		}else if(tabla.equals(TABLE_JSONFILES)){
			database.execSQL("DROP TABLE IF EXISTS " + TABLE_JSONFILES);
			String jsonfiles_sql = getSqlQueryText(JsonFiles.class);
			jsonfiles_sql = "CREATE TABLE " + TABLE_JSONFILES + " (" + jsonfiles_sql.substring(0, jsonfiles_sql.length()-1) + ")";
			database.execSQL(jsonfiles_sql);
		}	
		
		else if(tabla.equals(TABLE_DEMPLEADO)){
			database.execSQL("DROP TABLE IF EXISTS " + TABLE_DEMPLEADO);
			String dempleado_sql = getSqlQueryText(Dempleado.class);
			dempleado_sql = "CREATE TABLE " + TABLE_DEMPLEADO + " (" + dempleado_sql.substring(0, dempleado_sql.length()-1) + ")";
			database.execSQL(dempleado_sql);
		}	
	}

}