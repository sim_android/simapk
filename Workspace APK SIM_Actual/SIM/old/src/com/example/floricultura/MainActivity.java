package com.example.floricultura;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.DropboxAPI.DropboxFileInfo;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.exception.DropboxException;
import com.example.floricultura.adapters.DatosLista;
import com.example.floricultura.data.DBAdapter;
import com.example.floricultura.data.Dacceso;
import com.example.floricultura.data.JSONReader;
import com.example.floricultura.data.Mdepartamaento;
import com.example.floricultura.data.Mempleado;
import com.example.floricultura.data.Modulos;
import com.example.floricultura.data.Opciones;
import com.example.floricultura.data.Semillas;
import com.example.floricultura.data.Usuario;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Build;

public class MainActivity extends ActionBarActivity {
	
	private MyApp appState;	
	private DBAdapter db;
	String usuario;
	String password;
	EditText Editusuario;
	EditText Editpassword;
	TextView mensaje ;
	private ProgressDialog loadingDialog;
	private ProgressDialog loadingDialogDropbox;
	private final String DIR_APP = "/Floricultura/";
	 DropboxAPI<AndroidAuthSession> mApi;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        appState = ((MyApp)getApplicationContext());   
        db = appState.getDb();
        db.open();
        this.mApi = appState.getMAPI();
        
        
      
        int CountUsuarios = this.db.getUsuario(null).getCount();
        int Countdepartamentos = this.db.getMdepartamento(null).getCount();
        int CountModulos = this.db.getModulos(null).getCount();
        
        if (CountUsuarios==0 && Countdepartamentos==0 && CountModulos==0){
        	 Log.d("DATOS", "INSERTANDO DATOS");
        	// new GuardarDatos().execute();	
        }
        
        
       
        File mydir = new File(Environment.getExternalStorageDirectory() + "/Floricultura/JsonDescarga/");
        if(!mydir.exists()){
            mydir.mkdirs();
        }
        else{
            Log.d("error", "dir. already exists");
        }
        
        
        File mydir2 = new File(Environment.getExternalStorageDirectory() + "/Floricultura/JsonCarga/");
        if(!mydir2.exists()){
            mydir2.mkdirs();
        }
        else{
            Log.d("error", "dir. already exists");
        }
        
        /*
        
        Usuario u = new Usuario();
        u.setUsuario("Teemo");
        u.setCodigoEmpleado(85);
        u.setTipo("AD");
        u.setCodigoEmpleado(102);
        
        ObjectMapper mapper = new ObjectMapper();
        try {
			mapper.writeValue(new File(Environment.getExternalStorageDirectory() + "/Floricultura/JsonDescarga/user.json"), u);
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       
        */
        
        Editusuario = (EditText)findViewById(R.id.editTextUser);
        Editpassword = (EditText)findViewById(R.id.editTextPass);
        mensaje = (TextView)findViewById(R.id.mensajesLogin);
        
        
        Editusuario.requestFocus();
        
        


        //se referencia el boton de la vista
        Button button = (Button) findViewById(R.id.btnInicio);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
 
                verificarUsuario();
            }
        });
        
       
        
        Editusuario.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                	Log.d("DATOS", Editusuario.getText() + "");
                	Editpassword.requestFocus();
                  return true;
                }
                return false;
            }
        });
        
        Editpassword.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                	verificarUsuario();
                  return true;
                }
                return false;
            }
        });
        
        
        
        
        //se referencia el boton de la vista
        Button button2 = (Button) findViewById(R.id.cargarDatos);
        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
 
            	//new GuardarDatos().execute();
            	//new DescargarDatos().execute();
            	
            	Intent i = new Intent(getApplicationContext(), ListadoDescargas.class);
 	            startActivity(i);
            	
            
            }
        });
        
        
        
        //se referencia el boton de la vista
        Button buttonSincronizar = (Button) findViewById(R.id.btnSincronizar);
        buttonSincronizar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
 
				if(!appState.getSubiendoArchivos()){
					appState.setSubiendoArchivos(true);
					appState.setMostrarDialog(true);
					new UptoDropbox().execute(getApplicationContext());
				}
            
            }
        });
        
        
        appState.exportDatabse("floricultural.db");//exporta la base de datos a la SD card			
        	
    }
    
    
    


    public void verificarUsuario(){
    	
        this.usuario= Editusuario.getText().toString().trim();
        this.password = Editpassword.getText().toString().trim();
    	
        Cursor CursorUsuario= null ;
        CursorUsuario =	this.db.getUsuario("Usuario=" +  "'" + this.usuario + "' AND Clave= '" +  this.password + "'");
        Log.d("cursor", CursorUsuario.toString().trim() +"   ---" + CursorUsuario.getCount());
        
        
        if(CursorUsuario.getCount()>0){
        	Usuario UsuarioLogeado =this.db.getUsuarioFromCursor(CursorUsuario,  0);
        	Log.d("DATOS-USUSARIO", " " + UsuarioLogeado.toString().trim());
        	
        	
        	if(UsuarioLogeado.getTipo().equals("AD"))
        	{
	            Intent i = new Intent(getApplicationContext(), DepartamentoActivity.class);
	            i.putExtra("CodigoEmpleado", UsuarioLogeado.getCodigoEmpleado());
	            Editusuario.setText("");
	            Editpassword.setText("");
	            Editusuario.requestFocus();	            
	            RevisarFecha();          
	            startActivity(i);

	            
        	}else{
        		Intent i = new Intent(getApplicationContext(), ModuloActivity.class);
        		i.putExtra("CodigoEmpleado", UsuarioLogeado.getCodigoEmpleado());
        		i.putExtra("Administrador", 0);
	            Editusuario.setText("");
	            Editpassword.setText("");
	            Editusuario.requestFocus();
	            RevisarFecha();
	            startActivity(i);
        	}
        	


        }else{
        	
        	this.mensaje.setText("Datos incorrectos");
        	Editusuario.setText("");
        	Editusuario.requestFocus();
        	Editpassword.setText("");
        	
        }
    }
    
    
    
    public void RevisarFecha(){
    	
    	
    	SharedPreferences preferencias=getSharedPreferences("FechaLogin",Context.MODE_PRIVATE);
    	String UltimaFechaLog = preferencias.getString("dia","");
    	
        Calendar c = Calendar.getInstance(); 
		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
		String fecha = fdate.format(c.getTime());
    	
    	if(UltimaFechaLog.equals("")){
    		
            Editor editor=preferencias.edit();
            editor.putString("dia", fecha);
            editor.commit();
    		
    	}else{
    		 
    		 String hoy = fecha.substring(fecha.lastIndexOf("-")+1, fecha.length());
    		 String ultima = UltimaFechaLog.substring(UltimaFechaLog.lastIndexOf("-")+1, UltimaFechaLog.length());
    		 		 
    		 Log.d("fechas", "utlima " + ultima + "  hoy" + hoy);
    		 
    		 if(!hoy.equals(ultima)){
	    		    db.ReiniciarTabla(db.TIPO_DGUARDADOS);
	    			db.ReiniciarTabla(db.TIPO_JSONFILES);
	    			db.ReiniciarTabla(db.TIPO_ETIQUETAS);
    			 
		            Editor editor=preferencias.edit();
		            editor.putString("dia", fecha);
		            editor.commit();
		            
		            
		            Context context = getApplicationContext();
		            CharSequence text = "Se ha reiniciado los datos para un nuevo dia";
		            int duration = Toast.LENGTH_LONG;

		            Toast toast = Toast.makeText(context, text, duration);
		            toast.show();
    		 }
		 
    	}
    	
    	
    	/*
    	
    	SharedPreferences preferencias=getSharedPreferences("FechaLogin",Context.MODE_PRIVATE);
        Editor editor=preferencias.edit();
        Calendar c = Calendar.getInstance(); 
		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
		String fecha = fdate.format(c.getTime());
		
        editor.putString("mail", fecha);
        editor.commit();
        */
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
    
    public void insertarDatosdePrueaba(){
    	
    	//USUARIOS
    	  Usuario u = new Usuario();
          u.setUsuario("usuario");
          u.setClave("123");
          u.setTipo("AD");
          u.setCodigoEmpleado(10);
          db.insertarUusario(u);
          
          Usuario u2 = new Usuario();
          u2.setUsuario("usuario2");
          u2.setClave("123");
          u2.setTipo("OP");
          u2.setCodigoEmpleado(20);
          db.insertarUusario(u2);
          
          
          //MEMPLEADO
          Mempleado m1 = new Mempleado(u.getCodigoEmpleado(),1 );
          db.insertarMempleado(m1);
          Mempleado m2 = new Mempleado(u2.getCodigoEmpleado(),2 );
          db.insertarMempleado(m2);

         //DEPARTAMENTOS 
        db.insertarMdepartamento(new Mdepartamaento(1,"Vegetativo", ""));
        db.insertarMdepartamento(new Mdepartamaento(2,"Semillas", ""));
        db.insertarMdepartamento(new Mdepartamaento(3,"Mantenimiento", ""));
        db.insertarMdepartamento(new Mdepartamaento(4,"Administracion", ""));
        db.insertarMdepartamento(new Mdepartamaento(5,"Recursos Humanos", ""));
        db.insertarMdepartamento(new Mdepartamaento(6,"Inv. y Desarollo", ""));
        db.insertarMdepartamento(new Mdepartamaento(7,"Finanzas", ""));
        db.insertarMdepartamento(new Mdepartamaento(8,"Sistemas", ""));
        
        
        //MODULOS
        
        db.insertarModulos(new Modulos(2,	201	, "MADRES",	1));
        db.insertarModulos(new Modulos(2,	202,	"POLEN",	2));
        db.insertarModulos(new Modulos(2,	203,	"ACONDICIONAMIENTO",	3));
        db.insertarModulos(new Modulos(2,	204,	"RIEGO Y FUMIGACION",	4));
        db.insertarModulos(new Modulos(2,	205,	"PRODUCCION",	5));
        db.insertarModulos(new Modulos(1,	101,	"NUCLEOS",	1));
        db.insertarModulos(new Modulos(1,	102,	"INCREASE BLOCK",	2));
        db.insertarModulos(new Modulos(1,	103,	"PRODUCCION",	3));
        db.insertarModulos(new Modulos(1,	104,	"CALLOUS",	4));
        db.insertarModulos(new Modulos(1,	105,	"SALA EMPAQUE",	5));
        db.insertarModulos(new Modulos(1,	106,	"RIEGO Y FUMIGACION",	6));
        db.insertarModulos(new Modulos(2,	206,	"PROPAGADOR",	6));
        db.insertarModulos(new Modulos(8,	801,	"TEST",	1));
        
        
        //OPCONES
        
        db.insertarOpciones(new Opciones	(	201	,	21101	,"	EMASCULACIONES	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	3	,	1	)	);
        db.insertarOpciones(new Opciones	(	201	,	21102	,"	POLINIZACIONES	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	2	,	1	)	);
        db.insertarOpciones(new Opciones	(	201	,	21103	,"	COSECHAS	", "	~/SEED/Produccion/Mov_Cosecha.aspx	",	1	,	1	)	);
        db.insertarOpciones(new Opciones	(	201	,	21104	,"	LABORES CULTURALES	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	4	,	1	)	);
        db.insertarOpciones(new Opciones	(	201	,	21105	,"	DESECHO	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	5	,	1	)	);
        db.insertarOpciones(new Opciones	(	201	,	21106	,"	PLANTADO	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	6	,	1	)	);
        db.insertarOpciones(new Opciones	(	202	,	22101	,"	SUCCION	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	1	,	1	)	);
        db.insertarOpciones(new Opciones	(	202	,	22102	,"	LABORES CULTURALES	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	2	,	1	)	);
        db.insertarOpciones(new Opciones	(	202	,	22103	,"	DESECHO	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	3	,	1	)	);
        db.insertarOpciones(new Opciones	(	202	,	22104	,"	PLANTADO	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	4	,	1	)	);
        db.insertarOpciones(new Opciones	(	101	,	11101	,"	COLOCADO P/CALLOS	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	1	,	1	)	);
        db.insertarOpciones(new Opciones	(	101	,	11102	,"	PLANTADO	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	2	,	1	)	);
        db.insertarOpciones(new Opciones	(	101	,	11103	,"	DESPUNTES	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	3	,	1	)	);
        db.insertarOpciones(new Opciones	(	101	,	11104	,"	DESHOJADO	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	4	,	1	)	);
        db.insertarOpciones(new Opciones	(	101	,	11105	,"	CHEQUEO PUREZA	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	5	,	1	)	);
        db.insertarOpciones(new Opciones	(	101	,	11106	,"	CORTE	", "	~/Vegetativo/Mov_Asigna_Etq.aspx	",	6	,	2	)	);
        db.insertarOpciones(new Opciones	(	102	,	12101	,"	PLANTADO	", "	~/SeedAcond/Bolsa_Llena.aspx	",	1	,	1	)	);
        db.insertarOpciones(new Opciones	(	102	,	12102	,"	DESPUNTES	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	2	,	1	)	);
        db.insertarOpciones(new Opciones	(	102	,	12103	,"	DESHOJADO	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	3	,	1	)	);
        db.insertarOpciones(new Opciones	(	102	,	12104	,"	CHEQUEO PUREZA	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	4	,	1	)	);
        db.insertarOpciones(new Opciones	(	102	,	12105	,"	CORTE	", "	~/Vegetativo/Mov_Asigna_Etq.aspx	",	5	,	2	)	);
        db.insertarOpciones(new Opciones	(	103	,	13101	,"	PLANTADO	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	1	,	1	)	);
        db.insertarOpciones(new Opciones	(	103	,	13102	,"	DESPUNTE	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	2	,	1	)	);
        db.insertarOpciones(new Opciones	(	103	,	13103	,"	DESHOJADO	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	3	,	1	)	);
        db.insertarOpciones(new Opciones	(	103	,	13104	,"	CHEQUEO PUREZA	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	4	,	1	)	);
        db.insertarOpciones(new Opciones	(	103	,	13105	,"	CORTE	", "	~/Vegetativo/Mov_Asigna_Etq.aspx	",	5	,	2	)	);
        db.insertarOpciones(new Opciones	(	103	,	13106	,"	LIMPIEZA PLANTA	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	6	,	1	)	);
        db.insertarOpciones(new Opciones	(	103	,	13107	,"	DESPUNTE SELECTIVO	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	7	,	1	)	);
        db.insertarOpciones(new Opciones	(	103	,	13108	,"	CONTROL CALIDAD	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	8	,	1	)	);
        db.insertarOpciones(new Opciones	(	104	,	14101	,"	COLOCADO ESQUEJES	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	1	,	1	)	);
        db.insertarOpciones(new Opciones	(	104	,	14102	,"	LEVANTADO ESQUEJES	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	2	,	1	)	);
        db.insertarOpciones(new Opciones	(	105	,	15101	,"	CONTEO ESQ/BROTES	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	1	,	1	)	);
        db.insertarOpciones(new Opciones	(	105	,	15102	,"	ACTIVIDADES EMPAQUE	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	2	,	1	)	);
        db.insertarOpciones(new Opciones	(	105	,	15103	,"	CONTROL CALIDAD	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	3	,	1	)	);
        db.insertarOpciones(new Opciones	(	203	,	23101	,"	ENTREGA H_PROCESO	", "	~/SeedAcond/Entrega_HProceso.aspx	",	2	,	1	)	);
        db.insertarOpciones(new Opciones	(	203	,	23102	,"	UNION SEMILLA FRESCA	", "	~/SeedAcond/Mov_UBF.aspx	",	1	,	1	)	);
        db.insertarOpciones(new Opciones	(	203	,	23103	,"	INTEGRACION PACKING LIST	", "	~/SeedAcond/Mov_Integra_PackingList.aspx	",	3	,	1	)	);
        db.insertarOpciones(new Opciones	(	203	,	23104	,"	UNION EMPAQUE	", "	~/SeedAcond/Union_Pkempaque.aspx	",	4	,	1	)	);
        db.insertarOpciones(new Opciones	(	201	,	21107	,"	FINALIZA ACTIVIDAD	", "	~/Pantallas_generales/Mov_FinActividad_General.aspx	",	10	,	1	)	);
        db.insertarOpciones(new Opciones	(	202	,	22105	,"	FINALIZA ACTIVIDAD	", "	~/Pantallas_generales/Mov_FinActividad_General.aspx	",	5	,	0	)	);
        db.insertarOpciones(new Opciones	(	801	,	81101	,"	TEST	", "	~/IT/Test_2.aspx	",	1	,	1	)	);
        db.insertarOpciones(new Opciones	(	201	,	21108	,"	PODAS DE FORMACION	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	7	,	1	)	);
        db.insertarOpciones(new Opciones	(	201	,	21109	,"	ELIMINACION RAMOS O FLORES	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	8	,	1	)	);
        db.insertarOpciones(new Opciones	(	201	,	21110	,"	CAPADOS	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	9	,	1	)	);
        db.insertarOpciones(new Opciones	(	202	,	22106	,"	PODAS DE FORMACION	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	3	,	1	)	);
        db.insertarOpciones(new Opciones	(	202	,	22107	,"	ELIMINACION RAMOS O FLORES	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	4	,	1	)	);
        db.insertarOpciones(new Opciones	(	202	,	22108	,"	ROGUIN	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	5	,	1	)	);
        db.insertarOpciones(new Opciones	(	202	,	22109	,"	MORTALIDAD	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	6	,	1	)	);
        db.insertarOpciones(new Opciones	(	202	,	22110	,"	BORADO	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	7	,	1	)	);
        db.insertarOpciones(new Opciones	(	201	,	21111	,"	ROGUIN	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	10	,	1	)	);
        db.insertarOpciones(new Opciones	(	201	,	21112	,"	MORTALIDAD	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	11	,	1	)	);
        db.insertarOpciones(new Opciones	(	201	,	21113	,"	BOTADO	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	12	,	1	)	);
        db.insertarOpciones(new Opciones	(	103	,	13999	,"	FINALIZA ACTIVIDAD	", "	~/Pantallas_generales/Mov_FinActividad_General.aspx	",	999	,	0	)	);
        db.insertarOpciones(new Opciones	(	103	,	13109	,"	DESBOTONADO	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	9	,	1	)	);
        db.insertarOpciones(new Opciones	(	101	,	11999	,"	FINALIZA ACTIVIDAD	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	999	,	0	)	);
        db.insertarOpciones(new Opciones	(	102	,	12999	,"	FINALIZA ACTIVIDAD	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	999	,	0	)	);
        db.insertarOpciones(new Opciones	(	104	,	14999	,"	FINALIZA ACTIVIDAD	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	999	,	0	)	);
        db.insertarOpciones(new Opciones	(	105	,	15999	,"	FINALIZA ACTIVIDAD	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	999	,	0	)	);
        db.insertarOpciones(new Opciones	(	106	,	16999	,"	FINALIZA ACTIVIDAD	", "	~/Pantallas_generales/Mov_Captura_General.aspx	",	999	,	0	)	);

        db.insertarSemillas(new Semillas("20083991",	"96811",	0));
        db.insertarSemillas(new Semillas("20120569",	"761090",	1));
    
    }
    
    
    

    
    
    
    
    
    

    


}
