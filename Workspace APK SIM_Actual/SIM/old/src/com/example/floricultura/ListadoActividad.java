package com.example.floricultura;

import java.util.ArrayList;

import com.example.floricultura.adapters.DatosLista;
import com.example.floricultura.adapters.ListAdapter;
import com.example.floricultura.data.DBAdapter;
import com.example.floricultura.data.Dempleado;
import com.example.floricultura.data.Dguardados;
import com.example.floricultura.data.Opciones;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.os.Build;

public class ListadoActividad extends ActionBarActivity {
	
	ArrayList<DatosLista> listado = new ArrayList<DatosLista>();
	int CodigoEmpleado;
	
	private MyApp appState;	
	private DBAdapter db;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_listado_actividad);
		
		Bundle extras = getIntent().getExtras();
	    CodigoEmpleado = extras.getInt("CodigoEmpleado");
	    
	    appState = ((MyApp)getApplicationContext());   
	    db = appState.getDb();
	    db.open();
	    
		ListView listadoVista = (ListView)findViewById(R.id.ListadoAsignados);
		listado = getDatos();
		
		ListAdapter adaptador = new ListAdapter(this, R.layout.filas_lista, listado);
        listadoVista.setAdapter(adaptador);
        
        Button btnBack = (Button) findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
 
            	onBackPressed();
            }
        });


	}
	
	
	public ArrayList<DatosLista> getDatos(){
		ArrayList<DatosLista> listaTemp = new ArrayList<DatosLista>();
		
		listaTemp.add(new DatosLista("  Codigo\nEmpleado", " Hora Inicio", " Opcion"));

		 Cursor Casignados= null ;
		 Casignados = this.db.getDguardados("Supervisor=" + CodigoEmpleado + " AND ActividadFinalizada=0");
		 
		 if(Casignados.getCount()>0){
			 for(int i=0 ; i<Casignados.getCount();i++){	    
				 
				 Dguardados Dtemp = this.db.getDguardadosFromCursor(Casignados, i);
				 
				 Cursor Codigoempleado = this.db.getDempledo("CodigoEmpleado=" + Dtemp.getCodigoEmpleado().toString().trim());
				 Dempleado DatosEmpleado = this.db.getDempleadoFromCursor(Codigoempleado, 0);
				 
				 
				 listaTemp.add(new DatosLista("    "+DatosEmpleado.getNombre().toString().trim(),"       " + Dtemp.getHoraIncio()," " +Dtemp.getActividad()));
			 
				 
				 
				 
		     } 
		 }

		
		
		return listaTemp;
		

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.listado_actividad, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}



}
