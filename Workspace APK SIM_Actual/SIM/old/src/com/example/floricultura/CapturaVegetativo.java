package com.example.floricultura;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import com.example.floricultura.data.DBAdapter;
import com.example.floricultura.data.Dguardados;
import com.example.floricultura.data.EtiquetasVegetativo;
import com.example.floricultura.data.JSONReader;
import com.example.floricultura.data.JsonFiles;
import com.example.floricultura.data.RangoEtiqueta;
import com.example.floricultura.data.Usuario;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.os.Build;

public class CapturaVegetativo extends ActionBarActivity {
	
	private MyApp appState;	
	private DBAdapter db;
	int CodigoModulo;
	int Codigodepartamento;
	int CodigoEmpleado;
	int CodigoOpcion;
	int EsCorte;
	int TotalEmpleadosAsignados;
	int TotalEtiquetasAsignadas;
	String NombreOpcion;
	EditText empleado;
	EditText invernadero;
	EditText etiqueta;
	TextView TCantidadAsignados;
	Ringtone ringtone;
	RangoEtiqueta rangos;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_captura_vegetativo);
		
		
		Bundle extras = getIntent().getExtras();
		CodigoModulo = extras.getInt("CodigoModulo");
	    CodigoEmpleado = extras.getInt("CodigoEmpleado");
	    Codigodepartamento = extras.getInt("Codigodepartamento");
	    CodigoOpcion = extras.getInt("CodigoOpcion");
	    NombreOpcion = extras.getString("NombreOpcion");
	    EsCorte = extras.getInt("EsCorte");
	    
	    TextView TNombreCaptura = (TextView)findViewById(R.id.OpcionCaptura);
	    TextView TCodigoEmpleado= (TextView)findViewById(R.id.CodigoEmpleado);
	    TextView TCodigoOpcion= (TextView)findViewById(R.id.codigoOpcion);
	    TextView textViewEtiqueta= (TextView)findViewById(R.id.textViewEtiqueta);
	    TCantidadAsignados = (TextView)findViewById(R.id.textEmpleadosAsginados);
	    empleado  = (EditText)findViewById(R.id.editEmpleado);
	    invernadero  = (EditText)findViewById(R.id.editInvernadero);
	    etiqueta  = (EditText)findViewById(R.id.editEtiqueta);
	    Button button = (Button) findViewById(R.id.btnNuevo);
	    Button invernaderoBtn = (Button) findViewById(R.id.btnNuevoInvernadero);
	    
	    appState = ((MyApp)getApplicationContext());   
	    db = appState.getDb();
	    db.open();
	    
	    rangos = new RangoEtiqueta();
	    
	    
	    TNombreCaptura.setText(NombreOpcion+ "");
	    TCodigoEmpleado.setText(CodigoEmpleado + "");
	    TCodigoOpcion.setText(CodigoOpcion + "");
	    
	    obtenerRangoEtiqueta();
	    
	    verificarAsginados();
	    
	        
	    
	    
	    
	    //se limpian los campos y variables para una nueva captura
	    invernaderoBtn.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	        	invernadero.setText("");
	        	empleado.setText("");
	        	etiqueta.setText("");
	        	invernadero.setEnabled(true);
	        	empleado.setEnabled(true);
	        	invernadero.requestFocus();
	        		        	
	        }
	    });
	    
	    
	    //se limpian los campos y variables para una nueva captura
	    button.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	        	empleado.setText("");
	        	etiqueta.setText("");
	        	empleado.setEnabled(true);
	        	//Coloco a 0 las Etiquetas
	        	TCantidadAsignados.setText("0");
	        	empleado.requestFocus();
	        }
	    });
	    
	    

	    invernadero.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                		
                	invernadero.setEnabled(false);
                	empleado.requestFocus();
	                  return true;
                }
                return false;
            }
        });
	    
	    
	    //al recibir el enter se verifica si el descriptivo es correcto y lo manda al siguiente input sino le muestra un mensaje al usuario
	    empleado.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                	                	
                	String empleadoEscaneado = empleado.getText().toString().trim();
                	int existe = VerificarEmpleado( empleadoEscaneado );
                	
                	if(EsCorte == 0){
	                	if(existe == 2){
	                		Dguardados dtemp = new Dguardados();
	                		dtemp.setCodigoEmpleado(Integer.parseInt(empleadoEscaneado));
	                		dtemp.setCodigoDepto(Codigodepartamento);
	                		
                    		Calendar c = Calendar.getInstance(); 
                    		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
                    		String fecha = fdate.format(c.getTime());
                    		SimpleDateFormat ftime = new SimpleDateFormat("HH:mm:ss");
                    		String horaInicio = ftime.format(c.getTime());
	                		
                    		dtemp.setFecha(fecha);
	                		dtemp.setHoraIncio(horaInicio);
	                		dtemp.setActividad(CodigoOpcion);
	                		dtemp.setSupervisor(CodigoEmpleado);
	                		dtemp.setActividadFinalizada(0);
	                		dtemp.setInvernadero(invernadero.getText().toString().trim());
	                	
	                		Log.d("Dguardados", dtemp.toString().trim());
	                		
	                		db.insertarDguardados(dtemp);
	                		
 		
	                		//generacion archivo json
	                		ObjectMapper mapper = new ObjectMapper();
	                		try {
	                			String nameJson ="V_" + dtemp.getCodigoEmpleado() + "_"+dtemp.getFecha() + "_"+dtemp.getHoraIncio();
	                			nameJson=nameJson.replace(":", "-");	
								mapper.writeValue(new File( Environment.getExternalStorageDirectory()+"/Floricultura/JsonDescarga/" + nameJson+ ".json"), dtemp);
								
								JsonFiles archivo = new JsonFiles();
								archivo.setName(nameJson+ ".json");
								archivo.setNameFolder("SinFinalizar/");
								archivo.setUpload(0);
								db.insertarJsonFile(archivo);
								
								if(!appState.getSubiendoArchivos()){
									appState.setSubiendoArchivos(true);
									new UptoDropbox().execute(getApplicationContext());
								}
								
	                		
	                		} catch (JsonGenerationException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (JsonMappingException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
	                		
	                		
	                		
	                		TotalEmpleadosAsignados++;
	                		TCantidadAsignados.setText(TotalEmpleadosAsignados + "");
	                		
	                		
	                	    if(EsCorte == 1){
	                	    	//EtiquetasAsginadas();
	                	    	empleado.setEnabled(false);
	                	    	etiqueta.requestFocus();
	                	    }else{
	                	    	empleado.requestFocus();
	                	    	empleado.setText("");
	                	    }
	
	                		
	                		
	                	}else if(existe == 1){
	                		alertDialogMensaje("Asignado", "Este usuario ya ha sido asignado");
	                		empleado.setText("");
	                		empleado.requestFocus();
	                	}else if(existe == 0){
	                		alertDialogMensaje("Marcaje", "No existe marcaje para este usuario");
	                		empleado.setText("");
	                		empleado.requestFocus();
	                	}
		                
	                }else{
	                	if(existe == 0){
	                		empleado.setText("");
	                		alertDialogMensaje("Marcaje", "No existe marcaje para este usuario");
	                		empleado.setText("");
	                		//empleado.requestFocus();
	                	}else{
	                		//agregado para contar etiquetas por empleado
	                		
	                		empleado.setEnabled(false);            		
	                		etiqueta.requestFocus();
	                	}
	                }
                	return true;
                	
                }
                	
                	
                return false;
            }
        });
	    
	    
	    
	
	    etiqueta.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                	//Codigo para validar invernadero y empleado
                	/*
                	String invernaderoEscaneado1 = invernadero.getText().toString().trim();
                	alertDialogMensaje("Ver",invernaderoEscaneado1);
                	                	
                	if (invernaderoEscaneado1 == "1"){
                		//Mensaje de Invernadero o Empleado Vacio
                		alertDialogMensaje("Campo_Vacio", "Invernadero o Empleado Vacio");
                		invernadero.setText("");
                		empleado.setText("");
                		etiqueta.setText("");
                		invernadero.requestFocus();
                		
                	}else{
                	*/	
                		if (ValidarEtiqueta()){                			
                    		EtiquetasVegetativo EVtemp = new EtiquetasVegetativo();
                    		
                    		String empleadoEscaneado = empleado.getText().toString().trim();
                    		EVtemp.setCodigoEmpleado(Integer.parseInt(empleadoEscaneado));
                    		EVtemp.setEtiqueta(etiqueta.getText().toString().trim());
                    		EVtemp.setFinalizado(0);
                    		EVtemp.setInvernadero(invernadero.getText().toString().trim());
                    		
                    		Calendar c = Calendar.getInstance(); 
                    		SimpleDateFormat fdate = new SimpleDateFormat("yyyy-MM-dd");
                    		String fecha = fdate.format(c.getTime());
                    		SimpleDateFormat ftime = new SimpleDateFormat("HH:mm:ss");
                    		String horaInicio = ftime.format(c.getTime());
	                		
	                		
	                		EVtemp.setFecha(fecha);
	                		EVtemp.setHora(horaInicio);
	                		
	                		//Agrege codigo de Supervisor y Actividad
	                		EVtemp.setSupervisor(CodigoEmpleado);
	                		EVtemp.setActividad(CodigoOpcion);
	                		
	                		
	                		ObjectMapper mapper = new ObjectMapper();
                    		mapper = new ObjectMapper();
                    		try {
                    			String nameJson = "E_" +empleadoEscaneado + "_"+EVtemp.getFecha() + "_"+EVtemp.getHora();
                    			nameJson=nameJson.replace(":", "-");
								mapper.writeValue(new File( Environment.getExternalStorageDirectory()+"/Floricultura/JsonDescarga/" + nameJson+ ".json"), EVtemp);
								
								
								JsonFiles archivo = new JsonFiles();
								archivo.setName(nameJson+ ".json");
								archivo.setNameFolder("SinFinalizar/");
								archivo.setUpload(0);
								db.insertarJsonFile(archivo);
								
								if(!appState.getSubiendoArchivos()){
									appState.setSubiendoArchivos(true);
									new UptoDropbox().execute(getApplicationContext());
								}
								
							} catch (JsonGenerationException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (JsonMappingException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
	                		
                    		
                    		
                    		
                    		
                    		db.insertarEtiquetas(EVtemp);
                    		etiqueta.setText("");
                    		etiqueta.requestFocus();
                    		
                    		
                    		//agregado para contar etiquetas por empleado
                    		TotalEtiquetasAsignadas++;
	                		TCantidadAsignados.setText(TotalEtiquetasAsignadas + "");
                    		

                		}else{
                			etiqueta.setText("");
                    		etiqueta.requestFocus();
                    		
                		}
                		
                		
                	//}
                	
                	
                		
                		//Finaliza codigo de validacion de Invernadero y Empleado
                		
	                  return true;
                }
                return false;
            }
        });
	    
	    
	    if(EsCorte == 0){
	    	etiqueta.setVisibility(View.INVISIBLE);
	    	textViewEtiqueta.setVisibility(View.INVISIBLE);
	    	
	    	
	    }
	    
	    
	    
        Button btnBack = (Button) findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
 
            	onBackPressed();
            }
        });
        
        
        Button btnInicio = (Button) findViewById(R.id.btnInicio);
        btnInicio.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Intent intent = new Intent(getApplicationContext(), MainActivity.class);
           	 	startActivity(intent);
           	 	finish();
            }
        });
	    
	    


	}
	
	
	public void verificarAsginados(){
		Cursor CursorGuardados =	db.getDguardados("Actividad=" + CodigoOpcion + " AND ActividadFinalizada=0" );
		TotalEmpleadosAsignados = CursorGuardados.getCount();
		TCantidadAsignados.setText(TotalEmpleadosAsignados + "");
		
	}
	
	public void EtiquetasAsginadas(){
		//Cursor cursorEtiquetaG =	db.getEtiquetas("Actividad=" + CodigoOpcion + " AND CodigoEmpleado=" + empleado +"");
		Cursor cursorEtiquetaG =	db.getEtiquetas("Actividad");
		TotalEtiquetasAsignadas = cursorEtiquetaG.getCount();
		TCantidadAsignados.setText(TotalEtiquetasAsignadas + "");
		
	}
	
	
	public void obtenerRangoEtiqueta(){
		
		JSONReader _reader = appState.getReader();
		ArrayList<RangoEtiqueta> r = (ArrayList<RangoEtiqueta>) ( _reader.ReadFromSDCard(6, "Floricultura/JsonCarga/D_Vegetativo.json"));
		if(r.size()>0){
			this.rangos = r.get(0);
		}
		
	}
	
	public boolean ValidarEtiqueta(){
		boolean valido = false;
		
		String empleadoEscaneado = empleado.getText().toString().trim();
		int empelado  = Integer.parseInt(empleadoEscaneado);
		String invernaderoEscaneado = invernadero.getText().toString().trim();
		String etiquetaEscaneada = etiqueta.getText().toString().trim();  
		
		//se hace la consulta para validar si la etiqueta ya fue ingresada en el sistema
		Cursor cursorEtiqueta = db.getEtiquetas("CodigoEmpleado=" + empelado + " AND Etiqueta='" + etiquetaEscaneada + "' AND Invernadero='" + invernaderoEscaneado + "'");
		if(cursorEtiqueta.getCount() > 0 ){
			valido = false;
			alertDialogMensaje("Error Etiqueta", "Etiqueta ya ingresada para este Invernadero y Usuario");
		}else{
			//se valida el rango de la etiqueta
			int valorEtiqueta = Integer.parseInt(etiquetaEscaneada);
			
			Log.d("valorEtiqueta",valorEtiqueta + "");
			Log.d("valorEtiqueta", this.rangos.getInicio() + "" + this.rangos.getFin());
			
			
			if(valorEtiqueta> this.rangos.getInicio() && valorEtiqueta<this.rangos.getFin()){
				valido = true;
			}else{
				alertDialogMensaje("Error Etiqueta", "Valor de Etiqueta no valido");
			}
			
		}
			
		
		return valido;
	}
	
	public void alertDialogMensaje(String message1, String mesage2){
		

		
		try {
		    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		    ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
		    ringtone.play();
		    
		    
		} catch (Exception e) {
		    e.printStackTrace();
		}
		
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(message1);
		alertDialog.setMessage(mesage2);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int which) {
		// here you can add functions
			ringtone.stop();
		}
		});
		alertDialog.show();
	}
	
	
	
	
	//retornara un valor dependiendo del estado del empleado
	// 0 = no existe
	// 1 = existe pero esta asignado
	// 2 = existe y no esta asignado
	public int VerificarEmpleado(String empleado){
		int valor = 0;
		Cursor CursorDacceso =	db.getDacceso("CodigoEmpleado='" + empleado + "'");
		if(CursorDacceso != null  && CursorDacceso.getCount()>0){
			Cursor CursorGuardados =	db.getDguardados("CodigoEmpleado=" + empleado + " AND ActividadFinalizada=0");
			if(CursorGuardados.getCount()>0){
				//validarActividadCorte();
				valor = 1;
			}else{
				valor = 2;
			}
		}
		
		return valor;
	}
	
	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.captura_vegetativo, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}



}
