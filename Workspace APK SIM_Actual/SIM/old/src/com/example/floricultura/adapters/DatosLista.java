package com.example.floricultura.adapters;

public class DatosLista {
	
	String Campo1;
	String Campo2;
	String Campo3;
	
	
	public DatosLista(){
		this.Campo1 = "";
		this.Campo2 = "";
		this.Campo3 = "";
	}
	
	public DatosLista(String C1, String C2, String C3){
		this.Campo1 = C1;
		this.Campo2 = C2;
		this.Campo3 = C3;
	}
	
	
	public String getCampo1() {
		return Campo1;
	}
	public void setCampo1(String campo1) {
		Campo1 = campo1;
	}
	public String getCampo2() {
		return Campo2;
	}
	public void setCampo2(String campo2) {
		Campo2 = campo2;
	}
	public String getCampo3() {
		return Campo3;
	}
	public void setCampo3(String campo3) {
		Campo3 = campo3;
	}


}
