package com.example.floricultura.adapters;


import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import com.example.floricultura.R;

/**
 * Created by Byron on 29/05/2014.
 */
public class ListAdapter extends ArrayAdapter {

    Context context;
    int ViewResourceId;
    ArrayList<DatosLista> Listado;

    public ListAdapter(Context context, int ViewId, ArrayList<DatosLista> list) {
        super(context, ViewId, list);
        // TODO Auto-generated constructor stub
        this.context = context;
        this.ViewResourceId = ViewId;
        this.Listado = list;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v;
        LayoutInflater inflator = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflator.inflate(ViewResourceId, null, true);

        TextView id= (TextView)v.findViewById(R.id.textId);
        TextView name = (TextView)v.findViewById(R.id.textName);
        LinearLayout root= (LinearLayout)v.findViewById(R.id.layoutLista);
        
        if(position == 0){
        	root.setBackgroundColor(Color.parseColor("#2185FF"));
        	id.setTextColor(Color.parseColor("#ffffff"));
        	name.setTextColor(Color.parseColor("#ffffff"));
        }
        id.setText(Listado.get(position).getCampo1());
        name.setText(Listado.get(position).getCampo2());

        return v;
    }
}
